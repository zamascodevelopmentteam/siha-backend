INSERT INTO "non_arv_medicines"
    (id,medicine_id,is_r1,is_r2,is_r3,is_sifilis,is_anak,is_io_ims,is_cd_vl,is_preventif,is_alkes)
VALUES
    (default, 16.0, 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'True'),
    (default, 17.0, 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'True'),
    (default, 18.0, 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False'),
    (default, 19.0, 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False'),
    (default, 20.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 21.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 22.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 23.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 24.0, 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False', 'False'),
    (default, 25.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 26.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 27.0, 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False'),
    (default, 28.0, 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False'),
    (default, 29.0, 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'True'),
    (default, 30.0, 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'True'),
    (default, 31.0, 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False'),
    (default, 32.0, 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False'),
    (default, 33.0, 'False', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False'),
    (default, 34.0, 'False', 'False', 'True', 'False', 'False', 'False', 'False', 'False', 'False'),
    (default, 35.0, 'False', 'False', 'False', 'True', 'False', 'False', 'False', 'False', 'False'),
    (default, 36.0, 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'True'),
    (default, 37.0, 'False', 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False'),
    (default, 38.0, 'False', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False');
