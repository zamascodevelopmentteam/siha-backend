INSERT INTO "arv_medicines"
    (id,medicine_id,is_lini_1,is_lini_2)
VALUES
    (default, 1.0, 'True', 'False'),
    (default, 2.0, 'True', 'False'),
    (default, 3.0, 'True', 'False'),
    (default, 4.0, 'True', 'False'),
    (default, 5.0, 'True', 'False'),
    (default, 6.0, 'True', 'False'),
    (default, 7.0, 'True', 'False'),
    (default, 8.0, 'True', 'True'),
    (default, 9.0, 'False', 'True'),
    (default, 10.0, 'False', 'True'),
    (default, 11.0, 'True', 'False'),
    (default, 12.0, 'True', 'False'),
    (default, 13.0, 'True', 'False'),
    (default, 14.0, 'True', 'False'),
    (default, 15.0, 'True', 'False');
