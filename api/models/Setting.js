const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const ENUM = require("../../config/enum");

const tableName = "settings";

const Setting = sequelize.define(
  "setting",
  {
    name: {
      type: Sequelize.STRING,
      primaryKey: true,
      field: "name",
      set(val) {
        this.setDataValue("name", val.toUpperCase());
      }
    },
    meta: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },
    isActive: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

module.exports = Setting;
