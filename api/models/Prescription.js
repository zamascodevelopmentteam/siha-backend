const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Patient = require("./Patient");
const Visit = require("./Visit");
const Medicine = require("./Medicine");
const Regiment = require("./Regiment");
// const PrescriptionMedicine = require("./PrescriptionMedicine");

const tableName = "prescriptions";

const Prescription = sequelize.define(
  "prescription",
  {
    prescriptionNumber: Sequelize.STRING,
    paduanObatIds: Sequelize.STRING,
    paduanObatStr: Sequelize.STRING,
    notes: Sequelize.STRING,
    isDraft: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
      allowNull: true
    },
    orderId: Sequelize.INTEGER,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    regimentId: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Patient.hasMany(Prescription);
Prescription.belongsTo(Patient);

Regiment.hasMany(Prescription);
Prescription.belongsTo(Regiment);

Visit.hasOne(Prescription);
Prescription.belongsTo(Visit);

// Prescription.belongsToMany(Medicine, { through: PrescriptionMedicine });
// Medicine.belongsToMany(Prescription, { through: PrescriptionMedicine });

module.exports = Prescription;
