const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const tableName = "alkes_medicines";

const AlkesMedicine = sequelize.define(
  "alkesMedicine",
  {
    medicineId: Sequelize.INTEGER,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    deletedBy: Sequelize.INTEGER,
  },
  { tableName, underscored: true, paranoid: true }
);

Medicine.hasOne(AlkesMedicine);
AlkesMedicine.belongsTo(Medicine);

module.exports = AlkesMedicine;
