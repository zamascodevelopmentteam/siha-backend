const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const tableName = "arv_medicines";

const ArvMedicine = sequelize.define(
  "arvMedicine",
  {
    isLini1: {
      type: Sequelize.BOOLEAN,
      field: "is_lini_1"
    },
    isLini2: {
      type: Sequelize.BOOLEAN,
      field: "is_lini_2"
    },
    isIms: Sequelize.BOOLEAN,
    medicineId: Sequelize.INTEGER,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Medicine.hasOne(ArvMedicine);
ArvMedicine.belongsTo(Medicine);

module.exports = ArvMedicine;
