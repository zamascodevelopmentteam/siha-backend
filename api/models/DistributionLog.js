const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const DistributionPlan = require("./DistributionPlan");

const tableName = "distribution_logs";

const DistributionLog = sequelize.define(
  "distributionLog",
  {
    activity: Sequelize.STRING,
    activityDate: Sequelize.DATEONLY,
    actorId: {
      type : Sequelize.INTEGER,
      allowNull : true
    },
    prevOrderId: {
      type : Sequelize.INTEGER,
      allowNull : true
    },
    prevPicId: {
      type : Sequelize.INTEGER,
      allowNull : true
    },
    prevPlanStatus: Sequelize.STRING,

    createdBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

DistributionPlan.hasMany(DistributionLog);
DistributionLog.belongsTo(DistributionPlan);

module.exports = DistributionLog;
