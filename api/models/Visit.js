const Sequelize = require("sequelize");
const sequelize = require("../../config/database");

const User = require("./User");
const Upk = require("./Upk");
const Patient = require("./Patient");

const tableName = "visits";

const Visit = sequelize.define(
  "visit",
  {
    ordinal: Sequelize.INTEGER,
    isGenerated: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    visitType: Sequelize.ENUM({
      values: ["TEST", "TREATMENT", "CONSULTATION", "PROFILAKSIS"]
    }),
    visitDate: Sequelize.DATE,
    checkOutDate: Sequelize.DATE,

    userRrId: {
      field: "user_rr_id",
      type: Sequelize.INTEGER
    },

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

// User.hasMany(Visit, { as: "user_rr_id" });
// Visit.belongsTo(User);
User.hasMany(Visit, { foreignKey: "id" });
Visit.belongsTo(User, { foreignKey: "userRrId", as: "petugasRR" });

Upk.hasMany(Visit);
Visit.belongsTo(Upk);

Patient.hasMany(Visit);
Visit.belongsTo(Patient);

module.exports = Visit;
