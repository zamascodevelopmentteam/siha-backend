const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const SudinKabKota = require("./SudinKabKota");
const Province = require("./Province");

const tableName = "upk";

const Upk = sequelize.define(
  "upk",
  {
    name: {
      type: Sequelize.STRING,
      required: true,
      field: "name",
      set(val) {
        this.setDataValue("name", val.toUpperCase());
      }
    },
    codeId: Sequelize.STRING,
    orderOrd: Sequelize.INTEGER,
    distOrd: Sequelize.INTEGER,
    orderOrdReg: Sequelize.INTEGER,
    distOrdReg: Sequelize.INTEGER,
    distOrdKhusus: Sequelize.INTEGER,
    orderMultiplier: {
      type: Sequelize.INTEGER,
      required: true,
      defaultValue: 3
    },

    sudinKabKotaId: {
      field: "sudin_kab_kota_id",
      type: Sequelize.INTEGER
    },

    isActive: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    provinceId: Sequelize.INTEGER,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    address: Sequelize.TEXT
  },
  { tableName, underscored: true, paranoid: true }
);

SudinKabKota.hasMany(Upk, { foreignKey: "id" });
Upk.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaId",
  as: "sudinKabKota"
});

Province.hasMany(Upk);
Upk.belongsTo(Province);

module.exports = Upk;
