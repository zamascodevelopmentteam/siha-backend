const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const CentralMinistry = require("./CentralMinistry");

const tableName = "provinces";

const Province = sequelize.define(
  "province",
  {
    name: {
      type: Sequelize.STRING,
      set(val) {
        this.setDataValue("name", val.toUpperCase());
      }
    },
    codeId: Sequelize.STRING,
    orderOrd: Sequelize.INTEGER,
    distOrd: Sequelize.INTEGER,
    orderOrdReg: Sequelize.INTEGER,
    distOrdReg: Sequelize.INTEGER,
    distOrdKhusus: Sequelize.INTEGER,

    orderMultiplier: {
      type: Sequelize.INTEGER,
      defaultValue: 9
    },

    centralMinistryId: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },

    isActive: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    address: Sequelize.TEXT
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

CentralMinistry.hasMany(Province);
Province.belongsTo(CentralMinistry);

module.exports = Province;
