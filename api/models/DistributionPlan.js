const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Order = require("./Order");
const User = require("./User");
const Province = require("./Province");
const Upk = require("./Upk");
const SudinKabKota = require("./SudinKabKota");

const ENUM = require("./../../config/enum");

const tableName = "distribution_plans";

const DistributionPlan = sequelize.define(
  "distributionPlan",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    droppingNumber: { type: Sequelize.STRING, unique: true, notNull: true },
    picId: Sequelize.INTEGER,
    fundSource: Sequelize.ENUM({
      values: ["APBN", "NON_APBN"]
    }),
    planStatus: Sequelize.ENUM({
      values: [
        ENUM.PLAN_STATUS.DRAFT,
        ENUM.PLAN_STATUS.EVALUATION,
        ENUM.PLAN_STATUS.PROCESSED,
        ENUM.PLAN_STATUS.ACCEPTED_FULLY,
        ENUM.PLAN_STATUS.ACCEPTED_DIFFERENT,
        ENUM.PLAN_STATUS.LOST,
        ENUM.PLAN_STATUS.REJECTED
      ]
    }),
    distType: Sequelize.STRING,
    totalPrice: {
      type: Sequelize.REAL,
      defaultValue: 0
    },
    approvalStatus: Sequelize.ENUM({
      values: ["DRAFT", "IN_EVALUATION", "APPROVED_FULL", "REJECTED"]
    }),
    notes: Sequelize.STRING,
    approvalNotes: Sequelize.STRING,
    entityName: Sequelize.STRING,
    logisticRole: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    provinceId: Sequelize.INTEGER,
    sudinKabKotaId: Sequelize.INTEGER,
    upkId: Sequelize.INTEGER,

    logisticRoleSender: Sequelize.STRING,
    upkIdSender: Sequelize.INTEGER,
    provinceIdSender: Sequelize.INTEGER,
    sudinKabKotaIdSender: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Order.hasMany(DistributionPlan, {
  foreignKey: {
    allowNull: true
  }
});
DistributionPlan.belongsTo(Order);

User.hasMany(DistributionPlan, { foreignKey: "id" });
DistributionPlan.belongsTo(User, {
  foreignKey: "createdBy",
  as: "createdByData"
});

User.hasMany(DistributionPlan, { foreignKey: "id" });
DistributionPlan.belongsTo(User, {
  foreignKey: "updatedBy",
  as: "updatedByData"
});

Province.hasMany(DistributionPlan);
DistributionPlan.belongsTo(Province);

Upk.hasMany(DistributionPlan);
DistributionPlan.belongsTo(Upk);

SudinKabKota.hasMany(DistributionPlan, { foreignKey: "id" });
DistributionPlan.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaId"
});

Upk.hasMany(DistributionPlan, { foreignKey: "id" });
DistributionPlan.belongsTo(Upk, {
  foreignKey: "upkIdSender",
  as: "upkSenderData"
});

Province.hasMany(DistributionPlan, { foreignKey: "id" });
DistributionPlan.belongsTo(Province, {
  foreignKey: "provinceIdSender",
  as: "provinceSenderData"
});

SudinKabKota.hasMany(DistributionPlan, { foreignKey: "id" });
DistributionPlan.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaIdSender",
  as: "sudinKabKotaSenderData"
});

module.exports = DistributionPlan;
