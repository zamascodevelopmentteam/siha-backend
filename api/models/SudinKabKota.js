const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Province = require("./Province");

const tableName = "sudin_kab_kota";

const SudinKabKota = sequelize.define(
  "sudinKabKota",
  {
    name: {
      type: Sequelize.STRING,
      set(val) {
        this.setDataValue("name", val.toUpperCase());
      }
    },
    codeId: Sequelize.STRING,
    orderOrd: Sequelize.INTEGER,
    distOrd: Sequelize.INTEGER,
    orderOrdReg: Sequelize.INTEGER,
    distOrdReg: Sequelize.INTEGER,
    distOrdKhusus: Sequelize.INTEGER,
    orderMultiplier: {
      type: Sequelize.INTEGER,
      defaultValue: 6
    },
    isActive: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    address: Sequelize.TEXT
  },
  { tableName, underscored: true, paranoid: true }
);

Province.hasMany(SudinKabKota);
SudinKabKota.belongsTo(Province);

module.exports = SudinKabKota;
