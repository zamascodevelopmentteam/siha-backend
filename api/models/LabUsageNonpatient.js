const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Upk = require("./Upk");
// const Brand = require("./Brand");
const Medicine = require("./Medicine");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const tableName = "lab_usage_nonpatient";

const TestLabUsage = sequelize.define(
  "labUsageNonpatient",
  {
    testType: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.REAGEN_TYPE, [null, ""])]
      }
    },
    usageStock: Sequelize.INTEGER,
    usageControl: Sequelize.INTEGER,
    usageError: Sequelize.INTEGER,
    usageOthers: Sequelize.INTEGER,
    usageWasted: Sequelize.INTEGER,
    dateUsage: Sequelize.DATEONLY,
    isDbs: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    medicineId: Sequelize.INTEGER,
    toolId: Sequelize.INTEGER,

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Upk.hasOne(TestLabUsage);
TestLabUsage.belongsTo(Upk);

// Brand.hasOne(TestLabUsage);
// TestLabUsage.belongsTo(Brand);

Medicine.hasMany(TestLabUsage, { foreignKey: "id" });
TestLabUsage.belongsTo(Medicine, {
  foreignKey: "medicineId",
  as: "medicineData"
});

Medicine.hasMany(TestLabUsage, { foreignKey: "id" });
TestLabUsage.belongsTo(Medicine, {
  foreignKey: "toolId",
  as: "toolData"
});
module.exports = TestLabUsage;
