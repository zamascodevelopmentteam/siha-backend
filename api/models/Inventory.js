const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Brand = require("./Brand");
const Medicine = require("./Medicine");
const ReceiptItem = require("./ReceiptItem");
const Province = require("./Province");
const Upk = require("./Upk");
const SudinKabKota = require("./SudinKabKota");
const User = require("./User");

const tableName = "inventories";

const Inventory = sequelize.define(
  "inventory",
  {
    batchCode: Sequelize.STRING,
    expiredDate: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    packageUnitType: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    packageQuantity: {
      field: "package_quantity",
      type: Sequelize.INTEGER,
      notNull: true,
      defaultValue: 0
    },
    stockUnit: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    stockQty: {
      field: "stock_qty",
      type: Sequelize.INTEGER,
      notNull: true,
      defaultValue: 0
    },
    packagePrice: {
      type: Sequelize.REAL,
      defaultValue: 0
    },
    fundSource: Sequelize.STRING,
    manufacture: Sequelize.STRING,
    isAdjustment: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    logisticRole: Sequelize.ENUM({
      values: [
        "LAB_ENTITY",
        "PHARMA_ENTITY", // unused for now, next development will use this entity
        "UPK_ENTITY",
        "SUDIN_ENTITY",
        "PROVINCE_ENTITY",
        "MINISTRY_ENTITY"
      ]
    }),
    provinceId: Sequelize.INTEGER,
    sudinKabKotaId: Sequelize.INTEGER,
    upkId: Sequelize.INTEGER,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    deletedAt: Sequelize.DATEONLY,
    receiptItemId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Brand.hasMany(Inventory);
Inventory.belongsTo(Brand);

Medicine.hasMany(Inventory);
Inventory.belongsTo(Medicine);

ReceiptItem.hasMany(Inventory);
Inventory.belongsTo(ReceiptItem);

Province.hasMany(Inventory);
Inventory.belongsTo(Province);

SudinKabKota.hasMany(Inventory, { foreignKey: "id" });
Inventory.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaId"
});

Upk.hasMany(Inventory);
Inventory.belongsTo(Upk);

User.hasMany(Inventory, { foreignKey: "id" });
Inventory.belongsTo(User, {
  foreignKey: "createdBy",
  as: "createdByData"
});

User.hasMany(Inventory, { foreignKey: "id" });
Inventory.belongsTo(User, {
  foreignKey: "updatedBy",
  as: "updatedByData"
});

module.exports = Inventory;
