const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Prescription = require("./Prescription");
const Medicine = require("./Medicine");

const tableName = "prescription_medicines";

const PrescriptionMedicine = sequelize.define(
  "prescriptionMedicine",
  {
    amount: Sequelize.INTEGER,
    jumlahHari: Sequelize.INTEGER,
    notes: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { timestamps: true, tableName, underscored: true, paranoid: true }
);

Prescription.hasMany(PrescriptionMedicine);
PrescriptionMedicine.belongsTo(Prescription);

Medicine.hasMany(PrescriptionMedicine);
PrescriptionMedicine.belongsTo(Medicine);

module.exports = PrescriptionMedicine;
