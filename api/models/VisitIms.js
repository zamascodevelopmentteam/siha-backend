const Sequelize = require("sequelize");
const sequelize = require("../../config/database");

const Visit = require("./Visit");
const Upk = require("./Upk");

const tableName = "ims_visits";

const VisitIms = sequelize.define(
  "visitIms",
  {
    visitId: {
      field: "visit_id",
      type: Sequelize.INTEGER
    },
    reasonVisit: {
      field: "reason_visit",
      type: Sequelize.ENUM({
        values: [
          "SKRINING",
          "SAKIT",
          "FOLLOWUP"
        ]
      })
    },
    complaint: {
      type: Sequelize.ENUM({
        values: [
          "DUH_TUBUH",
          "GATAL",
          "KENCING_SAKIT",
          "NYERI_PERUT",
          "LECET",
          "BINTIL_SAKIT",
          "LUKA_ULKUS",
          "JENGGER",
          "BENJOLAN",
          "TIDAK_ADA"
        ]
      })
    },
    isPregnant: {
      field: "is_pregnant",
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    pregnancyStage: {
      field: "pregnancy_stage",
      type: Sequelize.ENUM({
        values: [
          "TRIMESTER_1",
          "TRIMESTER_2",
          "TRIMESTER_3"
        ]
      })
    },
    upkId: {
      field: "upk_id",
      type: Sequelize.INTEGER,
    },
    physicalExamination: {
      // fields:
      // DTV =>
      // DTS  => Female
      // Nyeri Perut =>
      // Lecet => Male / Female
      // Bintil Sakit => Male / Female
      // Luka/Ulkus => Male / Female
      // Jengger => Male / Female
      // Bubo => Male / Female
      // Nyeri Goyang Serviks => Male / Female
      // DTU =>  Female
      // Scortum Bengkak => Male
      // DTA => Male
      // DTM => Male / Female
      // Menstruasi => Female
      // Tidak Ada => Male / Female
      field: "physical_examination",
      type: Sequelize.JSON,
      defaultValue: {}
    },
    reasonReferral: Sequelize.STRING,
    ivaTest: Sequelize.BOOLEAN,
    isIvaTested: Sequelize.BOOLEAN,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    tesLab: Sequelize.BOOLEAN
  },
  {
    tableName,
    underscored: true,
    paranoid: true,
    name: {
      singular: "visitIms",
      plural: "visitIms"
    }
  }
);

Visit.hasOne(VisitIms, { foreignKey: "visitId", as: "testIms" });
VisitIms.belongsTo(Visit);

Upk.hasMany(VisitIms);
VisitIms.belongsTo(Upk);

module.exports = VisitIms;
