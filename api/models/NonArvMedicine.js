const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const tableName = "non_arv_medicines";

const NonArvMedicine = sequelize.define(
  "nonArvMedicine",
  {
    isR1: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isR2: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isR3: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isSifilis: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isAnak: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isAlkes: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isIoIms: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isIms: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isVl: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isCd4: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isPreventif: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isEid: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    isMachine: {
      type: Sequelize.BOOLEAN,
      defaultValue: null,
      allowNull: true
    },
    testType: {
      type: Sequelize.STRING,
      defaultValue: null,
      allowNull: true,
      set(val) {
        if (val) this.setDataValue("testType", val.toUpperCase());
      }
    },
    vlcd4Category: {
      type: Sequelize.STRING,
      defaultValue: null,
      allowNull: true
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Medicine.hasOne(NonArvMedicine);
NonArvMedicine.belongsTo(Medicine);

module.exports = NonArvMedicine;
