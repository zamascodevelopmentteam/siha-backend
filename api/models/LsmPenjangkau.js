const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const SudinKabKota = require("./SudinKabKota");

const tableName = "lsm_penjangkau";

const LsmPenjangkau = sequelize.define(
  "lsmPenjangkau",
  {
    name: Sequelize.STRING,

    created_by: Sequelize.INTEGER,
    updated_by: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

SudinKabKota.hasMany(LsmPenjangkau);
LsmPenjangkau.belongsTo(SudinKabKota);

module.exports = LsmPenjangkau;
