const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Patient = require("./Patient");
const Inventory = require("./Inventory");
const Upk = require("./Upk");
const Visit = require("./Visit");
const Brand = require("./Brand");

const tableName = "inventory_usage";

const InventoryUsage = sequelize.define(
  "inventoryUsage",
  {
    usedStockUnit: { type: Sequelize.INTEGER, notNull: true },
    treatmentId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    testHivId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    testImsId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    testVlCd4Id: {
      field: "test_vl_cd4_id",
      type: Sequelize.INTEGER,
      allowNull: true
    },
    profilaksisId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    labUsageNonpatientId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    stockBefore: Sequelize.INTEGER,
    stockAfter: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Inventory.hasMany(InventoryUsage);
InventoryUsage.belongsTo(Inventory);

Patient.hasMany(InventoryUsage, {
  foreignKey: {
    allowNull: true
  }
});
InventoryUsage.belongsTo(Patient);

Upk.hasMany(InventoryUsage);
InventoryUsage.belongsTo(Upk);

Visit.hasMany(InventoryUsage, {
  foreignKey: {
    allowNull: true
  }
});
InventoryUsage.belongsTo(Visit);

Brand.hasMany(InventoryUsage);
InventoryUsage.belongsTo(Brand);

module.exports = InventoryUsage;
