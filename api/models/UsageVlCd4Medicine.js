const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const UsageVlCd4 = require("./UsageVlCd4");
const Medicine = require("./Medicine");
const Inventory = require("./Inventory");

// const ENUM = require("../../config/enum");
// const varService = require("../services/variable.service")();

// const Visit = require("./Visit");

const tableName = "usage_vl_cd4_medicines";

const UsageVlCd4Medicine = sequelize.define(
  "usageVlCd4Medicines",
  {
    usageVlCd4Id: Sequelize.INTEGER,
    medicineId: Sequelize.INTEGER,
    amount: Sequelize.INTEGER,
    inventoryId: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

// UsageVlCd4.hasMany(UsageVlCd4Medicine);
// UsageVlCd4Medicine.belongsTo(UsageVlCd4);

Medicine.hasMany(UsageVlCd4Medicine);
UsageVlCd4Medicine.belongsTo(Medicine);
UsageVlCd4Medicine.belongsTo(Inventory);

// Visit.hasOne(TestVl);
// TestVl.belongsTo(Visit);

// Medicine.hasMany(TestVl, {
//   foreignKey: "namaReagen",
//   as: "tesReagenData"
// });
// TestVl.belongsTo(Medicine, {
//   foreignKey: "namaReagen",
//   as: "namaReagenData"
// });

module.exports = UsageVlCd4Medicine;
