const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Inventory = require("./Inventory");
const User = require("./User");

const tableName = "inventory_adjustment";

const InventoryAdjustment = sequelize.define(
  "inventoryAdjustment",
  {
    adjustmentType: Sequelize.ENUM({
      values: ["ADDITION", "DEDUCTION"]
    }),
    packageUnitType: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    packageQuantity: {
      field: "package_quantity",
      type: Sequelize.INTEGER,
      notNull: true,
      defaultValue: 0
    },
    stockUnit: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    stockQty: {
      field: "stock_qty",
      type: Sequelize.INTEGER,
      notNull: true,
      defaultValue: 0
    },
    notes: Sequelize.STRING,
    reportCode: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    deletedAt: Sequelize.DATEONLY,
    inventoryId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Inventory.hasMany(InventoryAdjustment);
InventoryAdjustment.belongsTo(Inventory);

User.hasMany(InventoryAdjustment, { foreignKey: "id" });
InventoryAdjustment.belongsTo(User, {
  foreignKey: "createdBy",
  as: "createdByData"
});

User.hasMany(InventoryAdjustment, { foreignKey: "id" });
InventoryAdjustment.belongsTo(User, {
  foreignKey: "updatedBy",
  as: "updatedByData"
});

module.exports = InventoryAdjustment;
