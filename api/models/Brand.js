const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const User = require("./User");

const tableName = "brands";

const Brand = sequelize.define(
  "brand",
  {
    name: Sequelize.STRING,
    isGeneric: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    // isActive: {
    //   type: Sequelize.BOOLEAN,
    //   defaultValue: true
    // },
    notes: Sequelize.STRING,
    packageUnitType: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    packageMultiplier: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    unitPrice: {
      type: Sequelize.REAL,
      defaultValue: 0
    },
    fundSource: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Medicine.hasMany(Brand);
Brand.belongsTo(Medicine);

User.hasMany(Brand, { foreignKey: "id" });
Brand.belongsTo(User, {
  foreignKey: "createdBy",
  as: "createdByData"
});

Brand.belongsTo(User, {
  foreignKey: "updatedBy",
  as: "updatedByData"
});

module.exports = Brand;
