const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Inventory = require("./Inventory");
const User = require("./User");

const tableName = "inventory_logs";

const InventoryLog = sequelize.define(
  "inventoryLog",
  {
    activity: Sequelize.STRING,
    activityDate: Sequelize.DATEONLY,
    actorId: Sequelize.INTEGER,
    isAdjustment: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    prevExpiredDate: Sequelize.DATEONLY,
    prevPkgUnitType: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    prevPkgQuantity: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    pkgQtyChanges: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    afterPkgQuantity: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    prevStockUnit: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    prevStockQty: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    stockQtyChanges: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    afterStockQty: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    inventoryId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Inventory.hasMany(InventoryLog);
InventoryLog.belongsTo(Inventory);

User.hasMany(InventoryLog, { foreignKey: "id" });
InventoryLog.belongsTo(User, {
  foreignKey: "createdBy",
  as: "createdByData"
});

User.hasMany(InventoryLog, { foreignKey: "id" });
InventoryLog.belongsTo(User, {
  foreignKey: "updatedBy",
  as: "updatedByData"
});
module.exports = InventoryLog;
