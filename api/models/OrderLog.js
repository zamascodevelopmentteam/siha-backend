const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Order = require("./Order");
const User = require("./User");

const tableName = "order_logs";

const OrderLog = sequelize.define(
  "orderLog",
  {
    orderStatus: Sequelize.STRING,
    approvalStatus: Sequelize.STRING,
    activityType: Sequelize.STRING,
    notes: Sequelize.STRING,
    prevOrderStatus: Sequelize.STRING,
    prevApprovalStatus: Sequelize.STRING,
    prevNotes: Sequelize.STRING,

    actorId: Sequelize.INTEGER,
    activityDate: Sequelize.DATEONLY,
    createdBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Order.hasMany(OrderLog);
OrderLog.belongsTo(Order);

User.hasMany(OrderLog, { foreignKey: "id" });
OrderLog.belongsTo(User, { foreignKey: "actorId", as: "actorData" });

module.exports = OrderLog;
