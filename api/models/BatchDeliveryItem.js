const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const DistributionPlan = require("./DistributionPlan");
const Inventory = require("./Inventory");

const tableName = "batch_delivery_items";

const BatchDeliveryItem = sequelize.define(
  "batchDeliveryItem", {
    requiredPackageQuantity: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    notes: Sequelize.STRING,
    orderItemId : {
      type : Sequelize.INTEGER,
      allowNull : true
    },
    inventoryId : {
      field: 'inventory_id',
      type : Sequelize.INTEGER,
      allowNull : true
    },
    createdBy: Sequelize.INTEGER
  }, {
    tableName,
    underscored: true,
    paranoid: true
  }
);

DistributionPlan.hasMany(BatchDeliveryItem);
BatchDeliveryItem.belongsTo(DistributionPlan);

Inventory.hasMany(BatchDeliveryItem, {foreignKey:'id'});
BatchDeliveryItem.belongsTo(Inventory, {foreignKey:'inventoryId'});

module.exports = BatchDeliveryItem;
