const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const Treatment = require("./Treatment");

const tableName = "couples";

const Couple = sequelize.define(
  "couple",
  {
    name: {
      type: Sequelize.STRING,
      set(val) {
        this.setDataValue("name", val.toUpperCase());
      }
    },
    age: Sequelize.INTEGER,
    gender: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.GENDER, [null, ""])]
      },
      set(val) {
        this.setDataValue("gender", val.toUpperCase());
      }
    },
    relationship: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.HUBUNGAN_NOTIF_COUPLE, [null, ""])]
      }
    }
  },
  { tableName, underscored: true, paranoid: true }
);

Treatment.hasMany(Couple);
Couple.belongsTo(Treatment);

module.exports = Couple;
