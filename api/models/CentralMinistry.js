const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const tableName = "central_ministry";

const CentralMinistry = sequelize.define(
  "centralMinistry",
  {
    name: Sequelize.STRING,
    distOrd: Sequelize.INTEGER,
    distOrdReg: Sequelize.INTEGER,
    distOrdKhusus: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true
  }
);

module.exports = CentralMinistry;
