const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const Joi = require("@hapi/joi");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const User = require("./User");
const Upk = require("./Upk");
const tableName = "lbpha2";

const Lbpha2 = sequelize.define(
  "lbpha2",
  {
    tanggalLaporan: Sequelize.DATEONLY,

    upkId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    sudinKabKotaId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    provinceId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },

    tabelRejimenStandar: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },
    tabelRejimenLain: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },
    tabelFdcJunior: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },
    tabelStok: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Lbpha2.getValidationConstraint = () => {};

Upk.hasMany(Lbpha2);
Lbpha2.belongsTo(Upk);

module.exports = Lbpha2;
