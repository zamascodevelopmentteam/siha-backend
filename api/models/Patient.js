const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const Joi = require("@hapi/joi");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const User = require("./User");
const Upk = require("./Upk");
const tableName = "patients";

const Patient = sequelize.define(
  "patient",
  {
    nik: Sequelize.STRING,
    name: {
      type: Sequelize.STRING,
      field: "fullname",
      set(val) {
        if (val) this.setDataValue("name", val.toUpperCase());
      }
    },
    addressKTP: {
      type: Sequelize.STRING,
      field: "address_KTP",
      set(val) {
        if (val) this.setDataValue("addressKTP", val.toUpperCase());
      }
    },
    addressDomicile: {
      type: Sequelize.STRING,
      set(val) {
        if (val) this.setDataValue("addressDomicile", val.toUpperCase());
      }
    },
    dateBirth: Sequelize.DATEONLY,
    tglMeninggal: Sequelize.DATEONLY,

    email: {
      type: Sequelize.STRING,
      set(val) {
        if (val) this.setDataValue("email", val.toUpperCase());
      }
    },

    gender: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.GENDER, [null, ""])]
      },
      set(val) {
        if (val) this.setDataValue("gender", val.toUpperCase());
      }
    },
    phone: Sequelize.STRING,
    statusPatient: Sequelize.ENUM({
      values: Object.values(ENUM.STATUS_PATIENT),
      defaultValue: ENUM.STATUS_PATIENT.HIV_NEGATIF
    }),
    weight: {
      type: Sequelize.INTEGER,
      allowNull: true,
      set(val) {
        if (val === "") {
          this.setDataValue("weight", null);
        } else {
          this.setDataValue("weight", val);
        }
      }
    },
    height: {
      type: Sequelize.INTEGER,
      allowNull: true,
      set(val) {
        if (val === "") {
          this.setDataValue("height", null);
        } else {
          this.setDataValue("height", val);
        }
      }
    },
    namaPmo: {
      type: Sequelize.STRING,
      set(val) {
        if (val) this.setDataValue("namaPmo", val.toUpperCase());
      }
    },
    hubunganPmo: {
      type: Sequelize.STRING,
      set(val) {
        if (val) this.setDataValue("hubunganPmo", val.toUpperCase());
      }
    },
    noHpPmo: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    upkId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      field: "user_id"
    },
    meta: {
      // fields:
      // convertToODHADate -> String timestamp,
      // tglKonfirmasiHivPos -> String timestamp,
      // firstTreatmentDate -> String timestamp
      // lastTreatmentDate -> String timestamp
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },
    lsmPenjangkau: Sequelize.ENUM({
      values: [
        'SPIRITIA',
        'IAC',
        'UNFPA',
        'IPPI',
        'YPI',
        'PKVHI',
        'PKBI',
        'KADER KESEHATAN',
        'DOTS/TB',
        'LAPAS RUTAN',
        'HEPATITIS',
        'TB',
        'KIA',
        'POLI_LAINNYA'
      ]
    }),
    kelompokPopulasi: Sequelize.ENUM({
      values: [
        'WPS',
        'Waria',
        'Penasun',
        'LSL',
        'Bumil',
        'Pasien TB',
        'Pasien IMS',
        'Pasien Hepatitis',
        'Pasangan ODHA',
        'Anak ODHA',
        'WBP',
        'Lainnya'
      ]
    }),
    motherNik: Sequelize.INTEGER,
    odhaLama: Sequelize.BOOLEAN,
  },
  { tableName, underscored: true, paranoid: true }
);

Patient.getValidationConstraint = () => {
  return Joi.object({
    name: Joi.string().required(),
    nik: Joi.string()
      .pattern(/^[0-9]+$/)
      .required(),
    addressKTP: Joi.string(),
    addressDomicile: Joi.string(),
    dateBirth: Joi.date().max("now"),
    gender: Joi.string().valid(
      ...varService.transformEnum(ENUM.GENDER, [null, ""])
    ),
    phone: Joi.number()
      .allow(null)
      .allow(""),
    statusPatient: Joi.string().valid(
      ...varService.transformEnum(ENUM.STATUS_PATIENT)
    ),
    weight: Joi.number()
      .allow(null)
      .allow("")
      .integer(),
    height: Joi.number()
      .allow(null)
      .allow("")
      .integer(),
    namaPmo: Joi.string(),
    hubunganPmo: Joi.string(),
    noHpPmo: Joi.number()
      .integer()
      .allow(null)
      .allow(""),
    upkId: Joi.number()
      .allow(null)
      .allow("")
      .integer(),
    kelompokPopulasi: Joi.string().valid(...ENUM.KELOMPOK_POPULASI).required(),
    lsmPenjangkau: Joi.string().valid(...ENUM.LSM_PENJANGKAU).required(),
  }).unknown();
};

User.hasOne(Patient, { foreignKey: "id" });
Patient.belongsTo(User, { foreignKey: "userId", as: "user" });

User.hasOne(Patient, { foreignKey: "id" });
Patient.belongsTo(User, { foreignKey: "createdBy", as: "createdByUser" });

Upk.hasMany(Patient);
Patient.belongsTo(Upk);

module.exports = Patient;
