const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const tableName = "producers";

const User = require("./User");

const Producers = sequelize.define(
    "producer",
    {
        name: Sequelize.STRING,
        medicineType: Sequelize.STRING,
        createdBy: Sequelize.INTEGER,
        updatedBy: Sequelize.INTEGER
    },
    { tableName, underscored: true, paranoid: true }
);

User.hasMany(Producers, { foreignKey: "id" });
Producers.belongsTo(User, { foreignKey: "createdBy", as: "createdByData" });
Producers.belongsTo(User, { foreignKey: "updatedBy", as: "updatedByData" });

module.exports = Producers;
