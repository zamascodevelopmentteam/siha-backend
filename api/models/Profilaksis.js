const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const Visit = require("./Visit");
const tablename = "profilaksis";

const Profilaksis = sequelize.define(
  "profilaksis",
  {
    ordinal: Sequelize.INTEGER,
    tglProfilaksis: Sequelize.DATEONLY,
    statusProfilaksis: Sequelize.STRING,
    obatDiberikan: Sequelize.INTEGER,
    qtyObat: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  {
    tablename,
    underscored: true,
    paranoid: true,
    freezeTableName: true
  }
);

Medicine.hasMany(Profilaksis, { foreignKey: "id" });
Profilaksis.belongsTo(Medicine, {
  foreignKey: "obatDiberikan"
});

Visit.hasOne(Profilaksis);
Profilaksis.belongsTo(Visit);

module.exports = Profilaksis;
