const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const User = require("./User");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const tableName = "notifications";

const Notification = sequelize.define(
  "notification",
  {
    userId: {
      type: Sequelize.INTEGER
    },
    fcm: {
      type: Sequelize.STRING,
      allowNull: true
    },
    sms: {
      type: Sequelize.STRING,
      allowNull: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: true
    },
    title: {
      type: Sequelize.STRING,
      allowNull: true
    },
    message: {
      type: Sequelize.STRING,
      allowNull: true
    },
    notificationType: Sequelize.STRING,

    sentAt: {
      type: Sequelize.DATE
    },
    receivedAt: Sequelize.DATE,
    readAt: Sequelize.DATE,

    meta: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

User.hasMany(Notification);
Notification.belongsTo(User);

module.exports = Notification;
