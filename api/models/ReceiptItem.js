const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Receipt = require("./Receipt");
const Brand = require("./Brand");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const tableName = "receipt_items";

const ReceiptItem = sequelize.define(
  "receiptItem",
  {
    batchCode: Sequelize.STRING,
    receivedExpiredDate: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    receiptItemStatus: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.RECEIPT_ITEM_STATUS, [null, ""])]
      }
    },
    receivedPkgUnitType: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.PKG_UNIT_TYPE, [null, ""])]
      }
    },
    receivedPkgQuantity: {
      type: Sequelize.INTEGER,
      notNull: true,
      defaultValue: 0
    },
    notes: Sequelize.STRING,
    inventoryId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Receipt.hasMany(ReceiptItem);
ReceiptItem.belongsTo(Receipt);

Brand.hasMany(ReceiptItem);
ReceiptItem.belongsTo(Brand);

module.exports = ReceiptItem;
