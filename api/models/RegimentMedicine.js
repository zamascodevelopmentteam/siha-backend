const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const Regiment = require("./Regiment");

const tableName = "regiment_medicines";

const RegimentMedicine = sequelize.define(
  "regimentMedicine",
  {
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

// Medicine.hasMany(RegimentMedicine);
// RegimentMedicine.belongsTo(Medicine);
//
// Regiment.hasMany(RegimentMedicine);
// RegimentMedicine.belongsTo(Regiment);

module.exports = RegimentMedicine;
