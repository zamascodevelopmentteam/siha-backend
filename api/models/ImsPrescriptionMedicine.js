const Sequelize = require("sequelize");
const sequelize = require("../../config/database");

const tableName = "ims_prescription_medicines";
const ImsPrescription = require("./ImsPrescription");

const ImsPrescriptionMedicine = sequelize.define(
  "imsPrescriptionMedicine",
  {
    prescriptionId: Sequelize.INTEGER,
    medicineId: Sequelize.INTEGER,
    medicineName: Sequelize.STRING,
    totalQty: Sequelize.INTEGER,
    totalDays: Sequelize.INTEGER,
    note: Sequelize.STRING,
    updatedAt: Sequelize.DATE,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    deletedBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true,
  }
);

ImsPrescription.hasMany(ImsPrescriptionMedicine, { foreignKey: "prescriptionId", as: "medicines" });
ImsPrescriptionMedicine.belongsTo(ImsPrescription, {
  foreignKey: {
    fieldName: 'prescriptionId'
  }
});

module.exports = ImsPrescriptionMedicine;
