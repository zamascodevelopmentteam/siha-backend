const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const tableName = "report_logs";

const ReportLogs = sequelize.define(
    "reportLog",
    {
        upkId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        sudinKabKotaId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        provinceId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        report: Sequelize.STRING,
        year: Sequelize.STRING,
        month: Sequelize.STRING,
        data: {
            type: Sequelize.JSON,
            allowNull: true,
            defaultValue: {}
        },
        createdBy: Sequelize.INTEGER,
        updatedBy: Sequelize.INTEGER
    },
    { tableName, underscored: true, paranoid: true }
);

// ReportLogs.getValidationConstraint = () => {};

module.exports = ReportLogs;
