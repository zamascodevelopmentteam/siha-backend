const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Order = require("./Order");
const Medicine = require("./Medicine");

const tableName = "order_items";

const OrderItem = sequelize.define(
  "orderItem",
  {
    packageUnitType: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    packageQuantity: {
      type : Sequelize.INTEGER,
      allowNull : false,
    },
    expiredDate: {
      type : Sequelize.DATEONLY,
      allowNull : true,
    },
    notes: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    totalPatient: Sequelize.INTEGER,
    reason: Sequelize.STRING,
    type: Sequelize.STRING,
    rAmount: Sequelize.INTEGER,
  },
  { tableName, underscored: true, paranoid: true }
);

Order.hasMany(OrderItem);
OrderItem.belongsTo(Order);

Medicine.hasMany(OrderItem);
OrderItem.belongsTo(Medicine);

module.exports = OrderItem;
