const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Medicine = require("./Medicine");
const RegimentMedicine = require("./RegimentMedicine");
const User = require("./User");

const tableName = "regiments";

const Regiment = sequelize.define(
  "regiment",
  {
    name: {
      type: Sequelize.STRING,
      set(val) {
        this.setDataValue("name", val.toUpperCase());
      },
      required: true
    },
    combinationIds: Sequelize.STRING,
    combinationStrInfo: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Regiment.belongsToMany(Medicine, { through: RegimentMedicine });
Medicine.belongsToMany(Regiment, { through: RegimentMedicine });
Regiment.belongsTo(User, { foreignKey: "createdBy", as: "createdByData" });

module.exports = Regiment;
