const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const DistributionPlan = require("./DistributionPlan");
const User = require("./User");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const tableName = "receipts";

const Receipt = sequelize.define(
  "receipt",
  {
    receiptNumber: {
      type: Sequelize.INTEGER,
      unique: true,
      allowNull: true
    },
    receiptStatus: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.RECEIPT_STATUS, [null, ""])]
      }
    },
    logisticRole: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.LOGISTIC_ROLE, [null, ""])]
      }
    },
    notes: Sequelize.STRING,
    provinceId: Sequelize.INTEGER,
    sudinKabKotaId: Sequelize.INTEGER,
    upkId: Sequelize.INTEGER,
    deletedAt: Sequelize.DATEONLY,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

DistributionPlan.hasMany(Receipt);
Receipt.belongsTo(DistributionPlan);

User.hasMany(Receipt, { foreignKey: "id" });
Receipt.belongsTo(User, {
  foreignKey: "createdBy",
  as: "createdByData"
});

User.hasMany(Receipt, { foreignKey: "id" });
Receipt.belongsTo(User, {
  foreignKey: "updatedBy",
  as: "updatedByData"
});

module.exports = Receipt;
