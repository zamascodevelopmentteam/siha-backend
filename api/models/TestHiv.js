const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Visit = require("./Visit");
const LsmPenjangkau = require("./LsmPenjangkau");
const Medicine = require("./Medicine");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const tableName = "test_hiv";

const TestHiv = sequelize.define(
  "testHiv",
  {
    ordinal: Sequelize.INTEGER,
    lsmPenjangkau: {
      type: Sequelize.STRING,
      allowNull: true
    },
    kelompokPopulasi: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.KEL_POPULASI, [null, ""])]
      }
    },
    alasanTest: Sequelize.STRING,
    tanggalTest: Sequelize.DATEONLY,
    jenisTest: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.HIV_TEST_TYPE, [null, ""])]
      }
    },
    namaReagenR1: Sequelize.INTEGER,
    hasilTestR1: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenR1: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    namaReagenR2: Sequelize.INTEGER,
    hasilTestR2: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenR2: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    namaReagenR3: Sequelize.INTEGER,
    hasilTestR3: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenR3: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    namaReagenNat: Sequelize.INTEGER,
    hasilTestNat: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenNat: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    namaReagenDna: Sequelize.INTEGER,
    hasilTestDna: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenDna: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    namaReagenRna: Sequelize.INTEGER,
    hasilTestRna: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenRna: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    namaReagenElisa: Sequelize.INTEGER,
    hasilTestElisa: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenElisa: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    rujukanLabLanjut: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.RUJUK_LAB_LANJUT, [null, ""])]
      }
    },
    namaReagenWb: Sequelize.INTEGER,
    qtyReagenWb: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    hasilTestWb: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    kesimpulanHiv: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    // ---------------
    namaReagenR12: Sequelize.INTEGER,
    hasilTestR12: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenR12: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    // ---------------
    namaReagenR22: Sequelize.INTEGER,
    hasilTestR22: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.TEST_RESULT, [null, ""])]
      }
    },
    qtyReagenR22: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    typeReagenR12: Sequelize.STRING,
    typeReagenR22: Sequelize.STRING
  },
  { tableName, underscored: true, paranoid: true }
);

Visit.hasOne(TestHiv);
TestHiv.belongsTo(Visit);
//
// LsmPenjangkau.hasMany(TestHiv);
// TestHiv.belongsTo(LsmPenjangkau);

// Medicine.hasMany(TestHiv, { foreignKey: "id" });
Medicine.hasMany(TestHiv, {
  foreignKey: "namaReagenR1",
  as: "tesReagenR1Data"
});
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenR1",
  as: "namaReagenR1Data"
});

// Medicine.hasMany(TestHiv, { foreignKey: "id" });
Medicine.hasMany(TestHiv, {
  foreignKey: "namaReagenR2",
  as: "tesReagenR2Data"
});
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenR2",
  as: "namaReagenR2Data"
});

// Medicine.hasMany(TestHiv, { foreignKey: "id" });
Medicine.hasMany(TestHiv, {
  foreignKey: "namaReagenR3",
  as: "tesReagenR3Data"
});
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenR3",
  as: "namaReagenR3Data"
});

Medicine.hasMany(TestHiv, {
  foreignKey: "namaReagenR12",
  as: "tesReagenR12Data"
});
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenR12",
  as: "namaReagenR12Data"
});

Medicine.hasMany(TestHiv, {
  foreignKey: "namaReagenR22",
  as: "tesReagenR22Data"
});
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenR22",
  as: "namaReagenR22Data"
});

// Medicine.hasMany(TestHiv, { foreignKey: "id" });
Medicine.hasMany(TestHiv, {
  foreignKey: "namaReagenDna",
  as: "tesReagenDnaData"
});
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenDna",
  as: "namaReagenDnaData"
});

Medicine.hasMany(TestHiv, { foreignKey: "id" });
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenNat",
  as: "namaReagenNatData"
});

Medicine.hasMany(TestHiv, { foreignKey: "id" });
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenRna",
  as: "namaReagenRnaData"
});

Medicine.hasMany(TestHiv, { foreignKey: "id" });
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenElisa",
  as: "namaReagenElisaData"
});

Medicine.hasMany(TestHiv, { foreignKey: "id" });
TestHiv.belongsTo(Medicine, {
  foreignKey: "namaReagenWb",
  as: "namaReagenWbData"
});

module.exports = TestHiv;
