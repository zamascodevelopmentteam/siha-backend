const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const Joi = require("@hapi/joi");
const bcryptService = require("../services/bcrypt.service");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const hooks = {
  beforeCreate(user) {
    if (user && user.password) {
      user.password = bcryptService() // eslint-disable-line no-param-reassign
        .password(user.password);
    }
  }
  // beforeUpdate(user) {
  //   if (user && user.password) {
  //     user.password = bcryptService() // eslint-disable-line no-param-reassign
  //       .password(user.password);
  //   }
  // }
};

const Province = require("./Province");
const Upk = require("./Upk");
const SudinKabKota = require("./SudinKabKota");

const tableName = "users";

const User = sequelize.define(
  "User",
  {
    nik: {
      type: Sequelize.STRING,
      unique: true,
      notNull: true
      // set(val) {
      //   if (val) this.setDataValue("nik", val.toUpperCase());
      // }
    },
    isActive: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    password: Sequelize.STRING,
    name: {
      type: Sequelize.STRING,
      set(val) {
        if (val) this.setDataValue("name", val.toUpperCase());
      }
    },
    role: {
      type: Sequelize.STRING,
      required: true,
      validate: {
        isIn: [varService.transformEnum(ENUM.USER_ROLE)]
      }
    },
    logisticrole: {
      field: "logistic_role",
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.LOGISTIC_ROLE, [null, ""])]
      }
    },
    avatar: {
      type: Sequelize.STRING,
      defaultValue:
        "https://hi-studio-public.s3-ap-southeast-1.amazonaws.com/assets/img/frame.png"
    },
    email: {
      type: Sequelize.STRING,
      set(val) {
        if (val) this.setDataValue("email", val.toUpperCase());
      }
    },
    phone: Sequelize.STRING,
    refreshToken: Sequelize.STRING,
    fcmToken: Sequelize.STRING,
    provinceId: Sequelize.INTEGER,
    upkId: Sequelize.INTEGER,

    sudinKabKotaId: {
      field: "sudin_kab_kota_id",
      type: Sequelize.INTEGER
    },

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    address: Sequelize.TEXT
  },
  {
    hooks,
    tableName,
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ["password", "refreshToken"] }
    }
  }
);

User.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());

  delete values.password;
  delete values.refreshToken;

  return values;
};

User.getValidationConstraint = () => {
  return Joi.object({
    name: Joi.string().required(),
    nik: Joi.string()
      .pattern(/^[\w\-]+$/)
      .required(),
    isActive: Joi.boolean().required(),
    password: Joi.string().required(),
    role: Joi.string()
      .valid(...varService.transformEnum(ENUM.USER_ROLE))
      .required(),
    // logisticrole: Joi.string()
    //   .allow(null)
    //   .allow("")
    //   .valid(...varService.transformEnum(ENUM.LOGISTIC_ROLE)),
    email: Joi.string()
      .email()
      .allow(null)
      .allow(""),
    phone: Joi.number()
      .allow(null)
      .allow(""),
    provinceId: Joi.number()
      .allow(null)
      .allow("")
      .integer(),
    upkId: Joi.number()
      .allow(null)
      .allow("")
      .integer(),
    sudinKabKotaId: Joi.number()
      .allow(null)
      .allow("")
      .integer()
  }).unknown();
};

Province.hasMany(User);
User.belongsTo(Province);

Upk.hasMany(User);
User.belongsTo(Upk);

SudinKabKota.hasMany(User, { foreignKey: "id" });
User.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaId",
  as: "sudinKabKota"
});

module.exports = User;
