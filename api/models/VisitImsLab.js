const Sequelize = require("sequelize");
const sequelize = require("../../config/database");
const Medicine = require("./Medicine");

const Visit = require("./Visit");

const tableName = "ims_visit_labs";

const VisitImsLab = sequelize.define(
  "visitImsLab",
  {
    visitId: {
      field: "visit_id",
      type: Sequelize.INTEGER
    },
    pmnUretraServiks: {
      field: "pmn_uretra_serviks",
      type: Sequelize.BOOLEAN
    },
    diplokokusIntraselUretraServiks: {
      field: "diplokokus_intrasel_uretra_serviks",
      type: Sequelize.BOOLEAN
    },
    pmnAnus: {
      field: "pmn_anus",
      type: Sequelize.BOOLEAN
    },
    diplokokusIntraselAnus: {
      field: "diplokokus_intrasel_anus",
      type: Sequelize.BOOLEAN
    },
    tVaginalis: {
      field: "t_vaginalis",
      type: Sequelize.BOOLEAN
    },
    kandida: {
      field: "kandida",
      type: Sequelize.BOOLEAN
    },
    ph: {
      field: "ph",
      type: Sequelize.BOOLEAN
    },
    sniffTest: {
      field: "sniff_test",
      type: Sequelize.BOOLEAN
    },
    clueCells: {
      field: "clue_cells",
      type: Sequelize.BOOLEAN
    },
    rprVdrl: {
      field: "rpr_vdrl",
      type: Sequelize.JSON,
      defaultValue: {}
    },
    tphaTppa: {
      field: "tpha_tppa",
      type: Sequelize.JSON,
      defaultValue: {}
    },
    rprVdrlTiter: {
      field: "rpr_vdrl_titer",
      type: Sequelize.ENUM({
        values: [
          '0',
          '1/2',
          '1/4',
          '1/8',
          '1/16',
          '1/32'
        ]
      })
    },
    isTested: Sequelize.BOOLEAN,
    etc: Sequelize.STRING,
    diagnosisSyndrome: Sequelize.JSON,
    diagnosisKlinis: Sequelize.JSON,
    diagnosisLab: Sequelize.JSON,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    // -----------------------------
    pmnVagina: Sequelize.BOOLEAN,
    pmnUretra: Sequelize.BOOLEAN,
    pmnMata: Sequelize.BOOLEAN,
    trichomonasVag: Sequelize.BOOLEAN,
    diplokokusIntraselVagina: Sequelize.BOOLEAN,
    diplokokusIntraselUretra: Sequelize.BOOLEAN,
    diplokokusIntraselMata: Sequelize.BOOLEAN,
    pseudohifaBlastospora: Sequelize.BOOLEAN,
    pemeriksaan: Sequelize.STRING,
    // -----------------------------
    realRprVdrl: {
      field: "real_rpr_vdrl",
      type: Sequelize.JSON,
      defaultValue: {}
    },
  },
  {
    tableName,
    underscored: true,
    paranoid: true,
  }
);

Visit.hasOne(VisitImsLab, { foreignKey: "visitId", as: "visitImsLab" });
VisitImsLab.belongsTo(Visit);
// Medicine.hasMany(VisitImsLab, {
//   foreignKey: `rpr_vdrl->>'reagent'`,
//   // as: "tesReagenR3Data"
// });
// VisitImsLab.belongsTo(Medicine, {
//   foreignKey: `rpr_vdrl->>'reagent'`,
// });

module.exports = VisitImsLab;
