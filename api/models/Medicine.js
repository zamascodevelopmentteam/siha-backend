const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const Joi = require("@hapi/joi");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const User = require("./User");

const tableName = "medicines";

const Medicine = sequelize.define(
  "medicine",
  {
    name: {
      type: Sequelize.STRING,
      // set(val) {
      //   this.setDataValue("name", val.toUpperCase());
      // }
    },
    codeName: {
      type: Sequelize.STRING,
      // set(val) {
      //   this.setDataValue("codeName", val.toUpperCase());
      // }
    },
    medicineType: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.MEDICINE_TYPE, [null, ""])]
      }
    },
    stockUnitType: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.PKG_UNIT_TYPE, [null, ""])]
      }
    },
    packageUnitType: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.PKG_UNIT_TYPE, [null, ""])]
      }
    },
    packageMultiplier: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    unitPrice: {
      type: Sequelize.REAL,
      defaultValue: 0
    },
    fundSource: Sequelize.STRING,
    ingredients: Sequelize.STRING,
    sediaan: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.MEDICINE_SEDIAAN, [null, ""])]
      }
    },
    imageUrl: Sequelize.STRING,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    category: Sequelize.STRING,
  },
  { tableName, underscored: true, paranoid: true }
);

Medicine.getValidationConstraint = () => {
  return Joi.object({
    name: Joi.string().required(),
    codeName: Joi.string().required(),
    unitPrice: Joi.number().required(),
    medicineType: Joi.string().required(),
    stockUnitType: Joi.string()
      .valid(...varService.transformEnum(ENUM.PKG_UNIT_TYPE))
      .required(),
    packageUnitType: Joi.string()
      .valid(...varService.transformEnum(ENUM.PKG_UNIT_TYPE))
      .required(),
    packageMultiplier: Joi.number()
      .integer()
      .required(),
    sediaan: Joi.string().valid(
      ...varService.transformEnum(ENUM.MEDICINE_SEDIAAN)
    ),
    imageUrl: Joi.string()
      .allow(null)
      .allow("")
      .uri()
  }).unknown();
};

User.hasMany(Medicine, { foreignKey: "id" });
Medicine.belongsTo(User, { foreignKey: "createdBy", as: "createdByData" });
Medicine.belongsTo(User, { foreignKey: "updatedBy", as: "updatedByData" });

module.exports = Medicine;
