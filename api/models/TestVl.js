const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const Visit = require("./Visit");
const Medicine = require("./Medicine");

const tableName = "test_vl_cd4";

const TestVl = sequelize.define(
  "testVL",
  {
    ordinal: Sequelize.INTEGER,
    namaReagen: {
      type: Sequelize.INTEGER
    },
    testVlCd4Type: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.VL_TEST_TYPE, [null, ""])]
      }
    },
    qtyReagen: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    hasilTestVlCd4: Sequelize.STRING,
    hasilTestInNumber: Sequelize.INTEGER,
    hasilLabSatuan: {
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.HASIL_LAB_SATUAN, [null, ""])]
      }
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    reason: Sequelize.STRING,
    date: Sequelize.DATE,
    reagentType: Sequelize.STRING
  },
  { tableName, underscored: true, paranoid: true }
);

Visit.hasOne(TestVl);
TestVl.belongsTo(Visit);

Medicine.hasMany(TestVl, {
  foreignKey: "namaReagen",
  as: "tesReagenData"
});
TestVl.belongsTo(Medicine, {
  foreignKey: "namaReagen",
  as: "namaReagenData"
});

module.exports = TestVl;
