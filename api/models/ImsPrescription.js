const Sequelize = require("sequelize");
const sequelize = require("../../config/database");

const tableName = "ims_prescriptions";
const Visit = require("./Visit");

const ImsPrescription = sequelize.define(
  "imsPrescription",
  {
    patientId: Sequelize.INTEGER,
    orderId: Sequelize.INTEGER,
    visitId: Sequelize.INTEGER,
    note: Sequelize.STRING,
    dateGivenMedicine: Sequelize.DATE,
    isDraft: Sequelize.BOOLEAN,
    condom: Sequelize.INTEGER,
    lubricant: Sequelize.INTEGER,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    counseling: Sequelize.STRING,
    rencanaKunjungan: Sequelize.DATE,
  },
  {
    tableName,
    underscored: true,
    paranoid: true,
  }
);

Visit.hasOne(ImsPrescription, { foreignKey: "visitId", as: "visitPrescription" });
ImsPrescription.belongsTo(Visit);

ImsPrescription.showSpecAttr = [
  'patientId',
  'visitId',
  'note',
  'dateGivenMedicine',
  'isDraft',
  'condom',
  'lubricant',
  'counseling',
];

module.exports = ImsPrescription;
