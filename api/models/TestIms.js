const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Visit = require("./Visit");
const Medicine = require("./Medicine");

const tableName = "test_ims";

const TestIms = sequelize.define(
  "testIms",
  {
    ordinal: Sequelize.INTEGER,
    ditestIms: Sequelize.BOOLEAN,
    namaReagen: Sequelize.INTEGER,
    qtyReagen: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    hasilTestIms: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null
    },
    ditestSifilis: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: null
    },
    hasilTestSifilis: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null
    },
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true,
    name: {
      singular: "testIms",
      plural: "testIms"
    }
  }
);

Visit.hasOne(TestIms, { foreignKey: "visitId", as: "testImsOld" });
TestIms.belongsTo(Visit);

Medicine.hasMany(TestIms, { foreignKey: "id" });
TestIms.belongsTo(Medicine, { foreignKey: "namaReagen", as: "namaReagenData" });

module.exports = TestIms;
