const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const Joi = require("@hapi/joi");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const User = require("./User");
const Upk = require("./Upk");
const Medicine = require("./Medicine");

const tableName = "lbpha2_medicine_usage";

const Lbpha2View = sequelize.define(
  "lbpha2View",
  {
    upkId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    medicineId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    codeName: {
      type: Sequelize.STRING,
      allowNull: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    stokAwal: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokDiterima: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokKeluar: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokKeluarReguler: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokKadaluarsa: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokSelisihFisik: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokAkhir: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stokAkhirBotol: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stockOnHand: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    packageOnHand: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    packageMultiplier: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    packageUnitType: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    stockUnitType: {
      type: Sequelize.INTEGER,
      allowNull: true
    },

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Lbpha2View.getValidationConstraint = () => {};

Upk.hasMany(Lbpha2View);
Lbpha2View.belongsTo(Upk);

Medicine.hasMany(Lbpha2View);
Lbpha2View.belongsTo(Medicine);

module.exports = Lbpha2View;
