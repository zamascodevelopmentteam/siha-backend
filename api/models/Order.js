const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Province = require("./Province");
const Upk = require("./Upk");
const SudinKabKota = require("./SudinKabKota");
const User = require("./User");

const tableName = "orders";

const Order = sequelize.define(
  "order",
  {
    orderCode: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    orderNumber: {
      type: Sequelize.INTEGER
    },
    lastOrderStatus: Sequelize.STRING,
    lastApprovalStatus: Sequelize.STRING,
    orderNotes: Sequelize.STRING,
    approvalNotes: Sequelize.STRING,
    isRegular: Sequelize.BOOLEAN,
    picId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null
    },
    logisticRole: Sequelize.STRING,

    upkIdOwner: Sequelize.INTEGER,
    sudinKabKotaIdOwner: Sequelize.INTEGER,
    provinceIdOwner: Sequelize.INTEGER,

    upkIdFor: Sequelize.INTEGER,
    sudinKabKotaIdFor: Sequelize.INTEGER,
    provinceIdFor: Sequelize.INTEGER,

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    deletedAt: Sequelize.DATEONLY,
    createdAt: {
      type: Sequelize.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    generalOrderId: Sequelize.INTEGER,
    totalPatient: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

Province.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(Province, {
  foreignKey: "provinceIdOwner",
  as: "provinceOwnerData"
});

Province.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(Province, {
  foreignKey: "provinceIdFor",
  as: "provinceForData"
});

SudinKabKota.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaIdOwner",
  as: "sudinKabKotaOwnerData"
});

SudinKabKota.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(SudinKabKota, {
  foreignKey: "sudinKabKotaIdFor",
  as: "sudinKabKotaForData"
});

Upk.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(Upk, { foreignKey: "upkIdOwner", as: "upkOwnerData" });

Upk.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(Upk, { foreignKey: "upkIdFor", as: "upkForData" });

User.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(User, { foreignKey: "createdBy", as: "createdByData" });

User.hasMany(Order, { foreignKey: "id" });
Order.belongsTo(User, { foreignKey: "updatedBy", as: "updatedByData" });

module.exports = Order;
