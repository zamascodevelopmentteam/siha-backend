const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const UsageVlCd4Medicine = require("./UsageVlCd4Medicine");
const Upk = require("./Upk");

// const ENUM = require("../../config/enum");
// const varService = require("../services/variable.service")();

// const Visit = require("./Visit");
// const Medicine = require("./Medicine");

const tableName = "usage_vl_cd4";

const UsageVlCd4 = sequelize.define(
  "usageVlCd4",
  {
    testType: Sequelize.INTEGER,
    date: Sequelize.DATE,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    upkId: Sequelize.INTEGER,
    reagentType: Sequelize.STRING,
    examination: Sequelize.INTEGER,
  },
  { tableName, underscored: true, paranoid: true }
);

UsageVlCd4.hasMany(UsageVlCd4Medicine);
UsageVlCd4Medicine.belongsTo(UsageVlCd4);

Upk.hasMany(UsageVlCd4);
UsageVlCd4.belongsTo(Upk);

// Visit.hasOne(TestVl);
// TestVl.belongsTo(Visit);

// Medicine.hasMany(TestVl, {
//   foreignKey: "namaReagen",
//   as: "tesReagenData"
// });
// TestVl.belongsTo(Medicine, {
//   foreignKey: "namaReagen",
//   as: "namaReagenData"
// });

module.exports = UsageVlCd4;
