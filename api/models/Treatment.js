const Sequelize = require("sequelize");
const sequelize = require("../../config/database");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();

const Visit = require("./Visit");
const Medicine = require("./Medicine");
const Prescription = require("./Prescription");
const Upk = require("./Upk");
const tableName = "treatments";

const Treatment = sequelize.define(
  "treatment",
  {
    prescriptionId: {
      // on prescription given
      field: "prescription_id",
      type: Sequelize.INTEGER
    },
    ordinal: Sequelize.INTEGER, // on visit created
    isGenerated: {
      // on medicine given
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    treatmentStartDate: Sequelize.DATEONLY, // on update treatment
    artStartDate: Sequelize.DATEONLY, // on update treatment
    noRegNas: Sequelize.STRING, // on update treatment
    tglKonfirmasiHivPos: Sequelize.DATEONLY, // on update treatment
    tglKunjungan: Sequelize.DATEONLY, // on update treatment
    tglRujukMasuk: Sequelize.DATEONLY, // on visit created
    upkSebelumnya: Sequelize.INTEGER, // on visit created
    kelompokPopulasi: {
      // on update treatment
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.KEL_POPULASI, [null, ""])]
      }
    },
    statusTb: {
      // on update treatment
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.STATUS_TB, [null, ""])]
      }
    },
    tglPengobatanTb: Sequelize.DATEONLY, // on update treatment
    statusFungsional: {
      // on update treatment
      type: Sequelize.STRING,
      defaultValue: ENUM.STATUS_FUNGSIONAL.KERJA,
      validate: {
        isIn: [varService.transformEnum(ENUM.STATUS_FUNGSIONAL, [null, ""])]
      }
    },
    stadiumKlinis: {
      // on update treatment
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.STADIUM_KLINIS, [null, ""])]
      }
    },
    ppk: Sequelize.BOOLEAN, // on update treatment
    tglPemberianPpk: Sequelize.DATEONLY, // on update treatment
    statusTbTpt: {
      // on update treatment
      type: Sequelize.STRING,
      defaultValue: ENUM.STATUS_TB_TPT.TDK_DIBERIKAN,
      validate: {
        isIn: [varService.transformEnum(ENUM.STATUS_TB_TPT, [null, ""])]
      }
    },
    tglPemberianTbTpt: Sequelize.DATEONLY, // on update treatment
    paduanObatArv: Sequelize.STRING, // on prescription given
    statusPaduan: {
      // on prescription given
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.STATUS_PADUAN, [null, ""])]
      }
    },
    tglPemberianObat: Sequelize.DATEONLY, // on prescription given
    tglPemberianObatSelesai: Sequelize.DATEONLY, // on prescription given
    bulanPemeriksaan: Sequelize.STRING,
    obatArv1: {
      // on prescription given
      field: "obat_arv_1",
      type: Sequelize.INTEGER
    },
    jmlHrObatArv1: {
      // on prescription given
      field: "jml_hr_obat_arv_1",
      type: Sequelize.INTEGER
    },
    sisaObatArv1: {
      // on update sisa obat
      field: "sisa_obat_arv_1",
      type: Sequelize.INTEGER
    },
    obatArv2: {
      // on prescription given
      field: "obat_arv_2",
      type: Sequelize.INTEGER
    },
    jmlHrObatArv2: {
      // on prescription given
      field: "jml_hr_obat_arv_2",
      type: Sequelize.INTEGER
    },
    sisaObatArv2: {
      // on update sisa obat
      field: "sisa_obat_arv_2",
      type: Sequelize.INTEGER
    },
    obatArv3: {
      // on prescription given
      field: "obat_arv_3",
      type: Sequelize.INTEGER
    },
    jmlHrObatArv3: {
      // on prescription given
      field: "jml_hr_obat_arv_3",
      type: Sequelize.INTEGER
    },
    sisaObatArv3: {
      // on update sisa obat
      field: "sisa_obat_arv_3",
      type: Sequelize.INTEGER
    },
    // tesLab: {
    //   type: Sequelize.STRING,
    //   validate: {
    //     isIn: [varService.transformEnum(ENUM.TES_LAB, [null, ""])]
    //   }
    // },
    // namaReagen: Sequelize.INTEGER,
    // jmlReagen: Sequelize.INTEGER,
    // hasilLab: Sequelize.INTEGER,
    // hasilLabSatuan: Sequelize.STRING,
    notifikasiPasangan: {
      // on update treatment
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.NOTIF_COUPLE, [null, ""])]
      }
    },
    akhirFollowUp: {
      // on update treatment
      type: Sequelize.STRING,
      validate: {
        isIn: [varService.transformEnum(ENUM.AKHIR_FOLLOW_UP, [null, ""])]
      }
    },
    isDiberiOAT: {
      type: Sequelize.BOOLEAN,
      field: "is_diberi_oat",
      defaultValue: false
    },
    periksaTB: {
      type: Sequelize.STRING,
      field: "periksa_tb",
      validate: {
        isIn: [varService.transformEnum(ENUM.PERIKSA_TB, [null, ""])]
      }
    },
    tglRujukKeluar: Sequelize.DATEONLY, // on update treatment
    tglMeninggal: Sequelize.DATEONLY, // on update treatment
    tglBerhentiArv: Sequelize.DATEONLY, // on update treatment
    tglRencanaKunjungan: Sequelize.DATEONLY, // on medicine given
    tglPermintaanKunjungan: Sequelize.DATEONLY, // patient apps
    bulanPeriodeLaporan: Sequelize.INTEGER, // on medicine given
    weight: Sequelize.INTEGER,
    height: Sequelize.INTEGER,
    isOnTransit: Sequelize.BOOLEAN, // on visit created
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER,
    lsmPenjangkau: {
      // on update treatment
      type: Sequelize.STRING,
      allowNull: true
    },
    skoring: Sequelize.TEXT,
    didampingi: Sequelize.STRING,
    upkId: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Visit.hasOne(Treatment);
Treatment.belongsTo(Visit);

Prescription.hasMany(Treatment);
Treatment.belongsTo(Prescription);

Medicine.hasMany(Treatment, { foreignKey: "id" });
Treatment.belongsTo(Medicine, { foreignKey: "obatArv1", as: "obatArv1Data" });

Medicine.hasMany(Treatment, { foreignKey: "id" });
Treatment.belongsTo(Medicine, { foreignKey: "obatArv2", as: "obatArv2Data" });

Medicine.hasMany(Treatment, { foreignKey: "id" });
Treatment.belongsTo(Medicine, { foreignKey: "obatArv3", as: "obatArv3Data" });

Upk.hasMany(Treatment, { foreignKey: "id" });
Treatment.belongsTo(Upk, {
  foreignKey: "upkSebelumnya",
  as: "upkSebelumnyaData"
});

module.exports = Treatment;
