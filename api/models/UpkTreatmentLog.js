const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Upk = require("./Upk");
const Patient = require("./Patient");
const tableName = "upk_treatment_logs";

const UpkTreatmentLog = sequelize.define(
  "upkTreatmentLog",
  {
    treatmentStartDate: Sequelize.DATEONLY,
    treatmentEndDate: Sequelize.DATEONLY,
    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  { tableName, underscored: true, paranoid: true }
);

Upk.hasMany(UpkTreatmentLog);
UpkTreatmentLog.belongsTo(Upk);

Patient.hasMany(UpkTreatmentLog);
UpkTreatmentLog.belongsTo(Patient);

module.exports = UpkTreatmentLog;
