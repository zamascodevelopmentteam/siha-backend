const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Brand = require("./Brand");
const DistributionPlan = require("./DistributionPlan");
const OrderItem = require("./OrderItem");
const Medicine = require("./Medicine");
const Inventory = require("./Inventory");

const tableName = "distribution_plans_items";

const DistributionPlanItem = sequelize.define(
  "distribution_plans_items",
  {
    actualRecievedPackageQuantity: Sequelize.INTEGER,
    packageUnitType: Sequelize.ENUM({
      values: [
        "TABLET",
        "TEST",
        "KAPSUL",
        "PAKET",
        "VIAL",
        "PCS",
        "BUAH",
        "KIT",
        "BOTOL"
      ]
    }),
    expiredDate: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    notes: Sequelize.STRING,
    fundSource: Sequelize.STRING,
    packageQuantity: {
      type: Sequelize.INTEGER,
      notNull: true
    },
    unitPrice: {
      type: Sequelize.REAL,
      defaultValue: 0
    },
    totalPrice: {
      type: Sequelize.REAL,
      defaultValue: 0
    },
    manufactureCode: Sequelize.STRING,
    inventoryIdFor: Sequelize.INTEGER,

    createdBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true
  }
);

DistributionPlan.hasMany(DistributionPlanItem);
DistributionPlanItem.belongsTo(DistributionPlan);

Medicine.hasMany(DistributionPlanItem);
DistributionPlanItem.belongsTo(Medicine);

OrderItem.hasMany(DistributionPlanItem);
DistributionPlanItem.belongsTo(OrderItem);

Inventory.hasMany(DistributionPlanItem);
DistributionPlanItem.belongsTo(Inventory);

Inventory.hasMany(DistributionPlanItem, {
  foreignKey: {
    allowNull: true
  }
});
DistributionPlanItem.belongsTo(Inventory, {
  foreignKey: "inventoryIdFor",
  as: "inventoryForData"
});

Brand.hasMany(DistributionPlanItem);
DistributionPlanItem.belongsTo(Brand);

module.exports = DistributionPlanItem;
