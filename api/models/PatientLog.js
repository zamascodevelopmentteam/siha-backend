const sequelize = require("../../config/database");
const Sequelize = require("sequelize");

const Patient = require("./Patient");
const Upk = require("./Upk");

const tableName = "patient_logs";

const PatientLog = sequelize.define(
  "patientLog",
  {
    startDate: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    endDate: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    tglRujukMasuk: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    tglRujukKeluar: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },

    visitId: Sequelize.INTEGER,

    createdBy: Sequelize.INTEGER,
    updatedBy: Sequelize.INTEGER
  },
  {
    tableName,
    underscored: true,
    paranoid: true,
    hooks: {
      beforeCreate: (patientLog, options) => {
        if (!patientLog.endDate) {
          patientLog.endDate = new Date(
            new Date().setFullYear(new Date().getFullYear() + 50)
          );
        }
      }
    }
  }
);

Patient.hasMany(PatientLog);
PatientLog.belongsTo(Patient);

Upk.hasMany(PatientLog);
PatientLog.belongsTo(Upk);

module.exports = PatientLog;
