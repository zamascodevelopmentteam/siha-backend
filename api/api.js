/**
 * third party libraries
 */
const bodyParser = require("body-parser");
const express = require("express");
const path = require("path");
const helmet = require("helmet");
const http = require("http");
const mapRoutes = require("express-routes-mapper");
const Sentry = require("@sentry/node");
const cors = require("cors");
const morgan = require("morgan");
const rfs = require("rotating-file-stream");
const dotenv = require("dotenv").config();

/**
 * server configuration
 */
const config = require("../config/");
const generalConfig = require("../config/general");
const dbService = require("./services/db.service");
const errorService = require("./services/error.service")();
const notificationService = require("./services/notification.service")();
const LoggerServive = require("./services/logger.service")();
const auth = require("./policies/auth.policy");

// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */
const app = express();
// new
// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   next();
// });
// end new
const server = http.Server(app);
app.use('/excel', express.static(__dirname + '/excel'));

// The request handler must be the first middleware on the app
if (environment == "production") {
  Sentry.init({
    dsn: generalConfig.SENTRY.DNS,
    environment: generalConfig.SENTRY.ENVIRONMENT
  });
  app.use(Sentry.Handlers.requestHandler());
}

var accessLogStream = rfs.createStream("access.log", {
  interval: "1d", // rotate daily
  path: path.join("request-logs")
});

// setup the logger
app.use(morgan("combined", { stream: accessLogStream }));

const mappedOpenRoutes = mapRoutes(config.publicRoutes, "api/controllers/");
const mappedAuthRoutes = mapRoutes(config.privateRoutes, "api/controllers/");

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

// secure express app
app.use(
  helmet({
    dnsPrefetchControl: false,
    frameguard: false,
    ieNoOpen: false
  })
);

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// secure your private routes with jwt authentication middleware
app.all("/private/*", (req, res, next) => auth(req, res, next));

// fill routes for express application
app.use("/public", mappedOpenRoutes);
app.use("/private", mappedAuthRoutes);

notificationService.initiateFirebaseConfig();
const DB = dbService(environment, config.migrate).start();

// The error handler must be before any other error middleware and after all controllers
if (environment == "production") {
  app.use(Sentry.Handlers.errorHandler());
}

app.use(errorService.errorHandler);

server.listen(config.port, () => {
  LoggerServive.success(`Server is running on port: ${config.port}`);
  LoggerServive.success(
    `Server is running on Node Version: ${process.version}`
  );
  if (
    environment !== "production" &&
    environment !== "development" &&
    environment !== "testing"
  ) {
    console.error(
      `NODE_ENV is set to ${environment}, but only production and development are valid.`
    );
    process.exit(1);
  }
  return DB;
});
