const errorService = require("../services/error.service")();
const Medicine = require("../models/Medicine");

module.exports = () => (req, res, next) => {
  const medicineId = req.body.medicineId || req.params.medicineId;
  if (medicineId) {
    Medicine.findOne({
      where: {
        id: medicineId
      }
    })
      .then(medicine => {
        if (medicine) {
          req.medicine = medicine;
          next();
        } else {
          const err = new Error("medicine_not_found");
          err.status = 400;
          throw err;
        }
        return medicine;
      })
      .catch(err => {
        errorService.errorHandler(err, req, res, next);
      });
  } else {
    res.status(400).json({
      status: 400,
      message: "medicine_not_found",
      data: null
    });
  }
};
