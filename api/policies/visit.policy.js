const errorService = require("../services/error.service")();
const Visit = require("../models/Visit");

module.exports = () => (req, res, next) => {
  const { visitId } = req.params;
  if (visitId) {
    Visit.findOne({
      where: {
        id: visitId
        // upkId: req.user.upkId
      }
    })
      .then(visit => {
        if (visit) {
          req.visit = visit;
          next();
        } else {
          const err = new Error("visit_not_found");
          err.status = 400;
          throw err;
        }
        return visit;
      })
      .catch(err => {
        errorService.errorHandler(err, req, res, next);
      });
  } else {
    res.status(400).json({
      status: 400,
      message: "visit_not_found",
      data: null
    });
  }
};
