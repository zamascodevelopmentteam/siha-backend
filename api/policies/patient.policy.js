const errorService = require("../services/error.service")();
const Patient = require("../models/Patient");

module.exports = () => (req, res, next) => {
  const patientId = req.body.patientId || req.params.patientId;
  if (patientId) {
    Patient.findOne({
      where: {
        id: patientId
      }
    })
      .then(patient => {
        if (patient) {
          req.patient = patient;
          next();
        } else {
          const err = new Error("patient_not_found");
          err.status = 400;
          throw err;
        }
        return patient;
      })
      .catch(err => {
        errorService.errorHandler(err, req, res, next);
      });
  } else {
    res.status(400).json({
      status: 400,
      message: "patient_not_found",
      data: null
    });
  }
};
