const _ = require("lodash");

const ErrorService = require("../../services/error.service")();
const VarService = require("../../services/variable.service")();

const Medicine = require("../../models/Medicine");

const middleware = () => {
  const add = () => async (req, res, next) => {
    const body = req.body;
    let medicineArr = body.medicines;
    try {
      if (!_.isArray(medicineArr)) {
        throw "invalid_list_medicine";
      }

      const medicineIdsArr = _.uniq(medicineArr.map(item => item.medicineId));

      if (medicineIdsArr.length !== medicineArr.length) {
        throw "invalid_list_medicine";
      }

      let medicineList = [];
      for (let i = 0; i < medicineArr.length; i++) {
        const medicine = await Medicine.findByPk(
          parseInt(medicineArr[i].medicineId)
        );
        if (medicine) {
          medicineArr[i].medicine = medicine.toJSON();
          medicineList.push(medicineArr[i]);
        } else {
          throw "invalid_list_medicine";
        }
      }
      req.medicineList = medicineList;

      if (req.medicineList.length == 0) {
        throw "invalid_list_medicine";
      }

      next();
    } catch (err) {
      res.status(422).json({
        data: err || err.message
      });
    }
  };

  const update = () => (req, res, next) => {
    next();
  };

  return {
    add,
    update
  };
};

module.exports = middleware;
