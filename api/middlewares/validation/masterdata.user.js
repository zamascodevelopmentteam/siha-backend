const VarService = require("../../services/variable.service")();

const ENUM = require("../../../config/enum");

const User = require("../../models/User");
const Upk = require("../../models/Upk");
const SudinKabKota = require("../../models/SudinKabKota");
const Province = require("../../models/Province");

const middleware = () => {
  const add = () => async (req, res, next) => {
    try {
      const body = req.body;
      let dataParams = {
        name: body.name,
        nik: body.nik,
        isActive: body.isActive,
        password: body.password,
        role: body.role,
        logisticrole: body.logisticrole,
        email: body.email,
        phone: body.phone,
        provinceId: body.provinceId,
        upkId: body.upkId,
        sudinKabKotaId: body.sudinKabKotaId
      };
      let { error } = User.getValidationConstraint().validate(dataParams);

      if (error) {
        throw error;
      }

      if (dataParams.role === ENUM.USER_ROLE.MINISTRY_STAFF) {
        req.body.logisticrole = ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY;
        req.body.upkId = null;
        req.body.sudinKabKotaId = null;
        req.body.provinceId = null;
      }

      if (dataParams.role === ENUM.USER_ROLE.PROVINCE_STAFF) {
        if (!dataParams.provinceId) {
          throw "province_not_found";
        } else {
          const province = await Province.findByPk(dataParams.provinceId);
          if (!province) {
            throw "province_not_found";
          }
        }
        req.body.logisticrole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        req.body.upkId = null;
        req.body.sudinKabKotaId = null;
      }

      if (dataParams.role === ENUM.USER_ROLE.SUDIN_STAFF) {
        if (!dataParams.sudinKabKotaId) {
          throw "sudin_not_found";
        } else {
          const sudin = await SudinKabKota.findByPk(dataParams.sudinKabKotaId);
          if (!sudin) {
            throw "sudin_not_found";
          }
          // req.body.provinceId = sudin.provinceId; RR
          req.body.provinceId = sudin.provinceId;
        }
        req.body.logisticrole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        req.body.upkId = null;
      }

      if (
        [
          ENUM.USER_ROLE.PATIENT,
          ENUM.USER_ROLE.LAB_STAFF,
          ENUM.USER_ROLE.RR_STAFF,
          ENUM.USER_ROLE.PHARMA_STAFF,
          ENUM.USER_ROLE.DOCTOR,
        ].includes(dataParams.role)
      ) {
        if (!dataParams.upkId) {
          throw "upk_not_found";
        } else {
          const upk = await Upk.findByPk(dataParams.upkId, {
            include: [
              {
                model: SudinKabKota,
                as: "sudinKabKota",
                required: true,
                include: [{ model: Province, required: true }]
              }
            ]
          });
          if (!upk) {
            throw "upk_not_found";
          }
          req.body.sudinKabKotaId = upk.sudinKabKotaId;
          req.body.provinceId = upk.sudinKabKota.provinceId;
        }
        req.body.logisticrole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        if (
          ENUM.USER_ROLE.PHARMA_STAFF === dataParams.role &&
          [
            ENUM.LOGISTIC_ROLE.PHARMA_ENTITY,
            ENUM.LOGISTIC_ROLE.UPK_ENTITY
          ].includes(dataParams.logisticrole)
        ) {
          req.body.logisticrole = dataParams.logisticrole;
        } else if (ENUM.USER_ROLE.LAB_STAFF === dataParams.role) {
          req.body.logisticrole = ENUM.LOGISTIC_ROLE.LAB_ENTITY;
        }
      }

      const user = await User.findOne({
        where: {
          nik: dataParams.nik
        },
        paranoid: false
      });
      if (user) {
        if (
          (req.params.userId && user.id != req.params.userId) ||
          !req.params.userId
        ) {
          throw "nik_already_exists";
        }
      }

      next();
    } catch (e) {
      next({
        status: 422,
        message: e
      });
    }
  };

  const update = () => (req, res, next) => {
    next();
  };

  return {
    add,
    update
  };
};

module.exports = middleware;
