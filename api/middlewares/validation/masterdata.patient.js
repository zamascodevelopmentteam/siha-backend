const VarService = require("../../services/variable.service")();

const ENUM = require("../../../config/enum");

const User = require("../../models/User");
const Patient = require("../../models/Patient");
const Upk = require("../../models/Upk");
const SudinKabKota = require("../../models/SudinKabKota");
const Province = require("../../models/Province");

const middleware = () => {
  const add = () => async (req, res, next) => {
    try {
      const {
        name,
        nik,
        addressKTP,
        addressDomicile,
        dateBirth,
        gender,
        phone,
        statusPatient,
        weight,
        height,
        namaPmo,
        hubunganPmo,
        noHpPmo,
        upkId,
        lsmPenjangkau,
        kelompokPopulasi
      } = req.body;
      let dataParams = {
        name,
        nik,
        addressKTP,
        addressDomicile,
        dateBirth,
        gender,
        phone,
        statusPatient,
        weight,
        height,
        namaPmo,
        hubunganPmo,
        noHpPmo,
        upkId,
        lsmPenjangkau,
        kelompokPopulasi
      };

      let { error } = Patient.getValidationConstraint().validate(dataParams);
      if (error) {
        throw error;
      }

      if (!dataParams.upkId) {
        throw "upk_not_found";
      } else {
        const upk = await Upk.findByPk(dataParams.upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (!upk) {
          throw "upk_not_found";
        }
        req.body.sudinKabKotaId = upk.sudinKabKotaId;
        req.body.provinceId = upk.sudinKabKota.provinceId;
      }

      const user = await User.findOne({
        where: {
          nik: dataParams.nik
        },
        paranoid: false
      });
      if (user) {
        if (!req.params.patientId) {
          throw "nik_already_exists";
        } else {
          if (req.params.patientId && req.patient.userId != user.id) {
            throw "nik_already_exists";
          }
        }
      }

      next();
    } catch (e) {
      next({
        status: 422,
        message: e
      });
    }
  };

  const update = () => (req, res, next) => {
    next();
  };

  return {
    add,
    update
  };
};

module.exports = middleware;
