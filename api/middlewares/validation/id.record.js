const { Op } = require("sequelize");
const moment = require("moment");

const ENUM = require("../../../config/enum");

const ErrorService = require("../../services/error.service")();

module.exports = (modelInstanceName, recordIdName) => async (
  req,
  res,
  next
) => {
  try {
    const modelInstance = require(`../../models/${modelInstanceName}`);
    const recordId =
      req.params[recordIdName || "id"] || req.body[recordIdName || "id"];

    const record = await modelInstance.findByPk(recordId);

    if (!record) {
      throw ErrorService.throwRecordNotFound("record_not_found");
    }

    req[modelInstanceName] = record;

    return next();
  } catch (e) {
    return ErrorService.errorHandler(e, req, res, next);
  }
};
