const errorService = require("../../services/error.service")();
const Medicine = require("../../models/Medicine");

const middleware = () => {
  const add = () => (req, res, next) => {
    const body = req.body;
    let dataParams = {
      name: body.name,
      codeName: body.codeName,
      unitPrice: body.unitPrice,
      fundSource: body.fundSource,
      medicineType: body.medicineType,
      stockUnitType: body.stockUnitType,
      packageUnitType: body.packageUnitType,
      packageMultiplier: body.packageMultiplier,
      ingredients: body.ingredients,
      sediaan: body.sediaan,
      imageUrl: body.imageUrl
    };
    const { error } = Medicine.getValidationConstraint().validate(dataParams);

    if (error) {
      res.status(422).json({
        data: error
      });
    } else {
      next();
    }
  };

  const update = () => (req, res, next) => {
    next();
  };

  return {
    add,
    update
  };
};

module.exports = middleware;
