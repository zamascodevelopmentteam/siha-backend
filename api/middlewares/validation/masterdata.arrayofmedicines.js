const _ = require("lodash");

const ErrorService = require("../../services/error.service")();
const VarService = require("../../services/variable.service")();

const Medicine = require("../../models/Medicine");

const middleware = () => {
  const add = () => async (req, res, next) => {
    const body = req.body;
    let medicineIdsArr = body.medicineIds;
    try {
      if (!_.isArray(medicineIdsArr)) {
        throw "invalid_list_medicine_id";
      }

      medicineIdsArr = _.uniq(medicineIdsArr);

      let medicineList = [];
      for (let i = 0; i < medicineIdsArr.length; i++) {
        const medicine = await Medicine.findByPk(parseInt(medicineIdsArr[i]));
        if (medicine) {
          medicineList.push(medicine.toJSON());
        } else {
          throw "invalid_list_medicine_id";
        }
      }
      req.medicineList = medicineList;

      if (req.medicineList.length == 0) {
        throw "invalid_list_medicine_id";
      }

      next();
    } catch (err) {
      res.status(422).json({
        data: err || err.message
      });
    }
  };

  const update = () => (req, res, next) => {
    next();
  };

  return {
    add,
    update
  };
};

module.exports = middleware;
