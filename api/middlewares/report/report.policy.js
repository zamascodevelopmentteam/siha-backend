const errorService = require("../../services/error.service")();

const middleware = () => {
  const defaultPolicy = () => async (req, res, next) => {
    next();
  };

  return {
    defaultPolicy
  };
};

module.exports = middleware;
