const moment = require("moment");
const Agenda = require("agenda");
const Sentry = require("@sentry/node");

const CONFIG = require("../../config/general");

const LoggerServive = require("./logger.service")();

let agenda;
const agendaService = () => {
  const start = async () => {
    try {
      if (process.env.AGENDA && process.env.AGENDA !== "FALSE") {
        agenda = new Agenda({
          db: {
            address: CONFIG.MONGODB.CONNECTION_STRING,
            collection: process.env.AGENDA_COLLECTION || "agendaJobs",
            options: {
              useUnifiedTopology: true,
              useNewUrlParser: true
            }
          }
        });
        await agenda.start();
        await agenda.purge();

        require("./jobs/test-logging")(agenda);
        require("./jobs/01_permintaanReguler")(agenda);
        require("./jobs/02_notifyPatientForVisit")(agenda);

        agenda.on("start", job => {
          console.log("Job %s starting", job.attrs.name);
        });
        agenda.on("complete", job => {
          console.log(`Job ${job.attrs.name} finished`);
        });

        runJobs();
        LoggerServive.success(
          "Agenda jobs have been executed at " +
            moment()
              .local()
              .format()
        );
      }
    } catch (e) {
      console.log("Error: ", e);
      // Sentry.captureException(e);
    }
  };

  const runJobs = () => {
    if (!process.env.AGENDA || process.env.AGENDA === "FALSE") {
      return;
    }
    // run every month on day 28th
    // await agenda.every("* * */28 * *", "generatePermintaanRegulerUPK");
    // await agenda.every("* * */28 * *", "generatePermintaanRegulerSudin");
    // await agenda.every("* * */28 * *", "generatePermintaanRegulerProvince");

    // run every day at 06.30am
    // await agenda.every("30 6 * * *", "notifyPatientsAWeekBefore");
    // await agenda.every("30 6 * * *", "notifyPatientsADayBefore");
    // await agenda.every("30 6 * * *", "notifyPatientsAtTheDay");
  };

  const getAgendaInstance = () => {
    return agenda;
  };

  return { start, runJobs, getAgendaInstance };
};

module.exports = agendaService;
