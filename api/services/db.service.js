const moment = require("moment");
const database = require("../../config/database");
const VariableService = require("./variable.service")();
const AgendaService = require("./agenda.service")();
const LoggerServive = require("./logger.service")();

const dbService = (environment, migrate) => {
  const authenticateDB = () => database.authenticate();

  const dropDB = () => database.drop();

  const syncDB = () => database.sync();

  const successfulDBStart = () =>
    LoggerServive.success(
      "Connection to the database has been established successfully at " +
        moment()
          .local()
          .format()
    );
  LoggerServive.success(`DB Host: ${process.env.DB_HOST}`);

  const errorDBStart = err => {
    LoggerServive.error("unable to connect to the database:", err);
    console.log(err);
  }

  const wrongEnvironment = () => {
    console.warn(
      `only development, staging, test and production are valid NODE_ENV variables but ${environment} is specified`
    );
    return process.exit(1);
  };

  const dbDataInitialization = async () => {
    try {
      await VariableService.loadSetting();
      AgendaService.start();
    } catch (err) {
      errorDBStart(err);
      // process.exit(1);
    }
  };

  const startMigrateTrue = async () => {
    try {
      // await syncDB();
      await dbDataInitialization();
      successfulDBStart();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startMigrateFalse = async () => {
    try {
      // await dropDB();
      // await database.dropAllSchemas();
      // await syncDB();
      await dbDataInitialization();
      successfulDBStart();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startDev = async () => {
    try {
      await authenticateDB();

      if (migrate) {
        return await startMigrateTrue();
      }

      return await startMigrateFalse();
    } catch (err) {
      return errorDBStart(err);
    }
  };

  const startStage = async () => {
    try {
      await authenticateDB();

      if (migrate) {
        return await startMigrateTrue();
      }

      return await startMigrateFalse();
    } catch (err) {
      return errorDBStart(err);
    }
  };

  const startTest = async () => {
    try {
      await authenticateDB();
      await startMigrateFalse();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startProd = async () => {
    try {
      await authenticateDB();
      await startMigrateFalse();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const start = async () => {
    switch (environment) {
      case "development":
        await startDev();
        break;
      case "staging":
        await startStage();
        break;
      case "testing":
        await startTest();
        break;
      case "production":
        await startProd();
        break;
      default:
        await wrongEnvironment();
    }
  };

  return {
    start
  };
};

module.exports = dbService;
