const fs = require("fs");

const ErrorService = require("../services/error.service")();

const responseService = () => {
  const download = async (res, filepath, filename, removeAfterDownload) => {
    if (filename && fs.existsSync(filepath)) {
      res.download(filepath, filename, function(err) {
        if (!err) {
          if (removeAfterDownload) {
            fs.unlink(filepath, function() {});
          }
        }
      });
    } else {
      throw ErrorService.throwRecordNotFound();
    }
  };

  return {
    download
  };
};

module.exports = responseService;
