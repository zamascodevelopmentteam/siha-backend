const firebase = require("firebase-admin");
const _ = require("lodash");
const axios = require("axios");
const sgMail = require("@sendgrid/mail");
const validator = require("validator");
const querystring = require("querystring");
const Sentry = require("@sentry/node");

const GENERAL_CONFIG = require("../../config/general");
const serviceAccount = require("../../config/serviceAccount.json");
const ENUM = require("../../config/enum");

const LoggerServive = require("./logger.service")();
const DateService = require("./date.service")();

const User = require("../models/User");
const Upk = require("../models/Upk");
const Notification = require("../models/Notification");

const service = () => {
  const sendFirebaseMessage = async payload => {
    const { fcmToken, title, message, fcmNotifType, meta } = payload;
    if (fcmToken && title && message) {
      await firebase
        .messaging()
        .send({
          data: {
            title: title,
            message: message,
            content: message,
            meta: meta ? JSON.stringify(meta) : null,
            type: fcmNotifType || "GENERAL"
          },
          notification: {
            title: title,
            body: message
          },
          token: fcmToken
        })
        .then(response => {
          return response;
        })
        .catch(error => {
          LoggerServive.error(error);
          // Sentry.captureException(error);
        });
    } else {
      LoggerServive.error(new Error("no_fcm_token_found"));
    }
  };

  const sendSMS = async payload => {
    const { phoneNumber, message, meta } = payload;

    if (!phoneNumber.toString().match(/^(\+62|0)(\d+)+$/)) {
      LoggerServive.error(
        "Phone number does not match with indonesia phone format!"
      );
      return;
    }
    LoggerServive.info(`Sending sms to ${phoneNumber}`);
    // LoggerServive.info(`Webhook: ${GENERAL_CONFIG.RAJASMS.URL_CALLBACK}`);

    const encodedParams = querystring.encode({
      username: GENERAL_CONFIG.RAJASMS.USERNAME,
      key: GENERAL_CONFIG.RAJASMS.API_KEY,
      number: phoneNumber,
      message: message
    });

    // const params = {
    //   callbackurl: GENERAL_CONFIG.RAJASMS.URL_CALLBACK,
    //   apikey: GENERAL_CONFIG.RAJASMS.API_KEY,
    //   datapacket: [{ number: phoneNumber, message: message }]
    // };

    axios
      .post(`${GENERAL_CONFIG.RAJASMS.URL_MASKING_OTP}?${encodedParams}`)
      // .post(`${GENERAL_CONFIG.RAJASMS.URL_REGULER_JSON}`, params)
      .then(function (response) {
        if (response.data && response.data.toString().includes("0|")) {
          LoggerServive.success(phoneNumber, response.data);
        } else {
          LoggerServive.error(phoneNumber, response.data);
        }
        return response;
      })
      .catch(function (error) {
        LoggerServive.error(error);
        Sentry.captureException(error);
      });
  };

  const sendEmail = async (to, text, subject) => {
    if (!validator.isEmail(to)) {
      LoggerServive.error("Email address is invalid!");
      return;
    }
    sgMail.setApiKey(GENERAL_CONFIG.SENDGRID.API_KEY);
    const msg = {
      to: to.toString().toLowerCase(),
      from: "msiha@kemkes.go.id",
      subject,
      text,
      html: text
    };
    sgMail.send(msg);
  };

  const sendNotification = async payload => {
    // notificationType is null or {sms: true, fcm: true, email:true}
    // recipient is {sms: phone_number, fcm: fcmToken, email:email address} or array of them
    let {
      recipient,
      recipientList,
      title,
      message,
      notificationType,
      meta,
      createdBy
    } = payload;
    recipientList =
      recipientList && _.isArray(recipientList) ? recipientList : [recipient];

    if (recipientList.length === 0) {
      LoggerServive.error("recipient list can not be empty!");
      return;
    }

    for (var i = 0; i < recipientList.length; i++) {
      const { userId, sms, fcm, email } = recipientList[i];

      // sending fcm message
      if (fcm) {
        sendFirebaseMessage({
          fcmToken: fcm,
          title,
          message,
          notificationType,
          meta
        });
      }
      // sending sms
      if (sms) {
        sendSMS({
          phoneNumber: sms,
          message,
          meta
        });
      }
      if (sms || fcm || email) {
        const notifToLogParams = {
          userId,
          sms,
          fcm,
          email,
          title,
          message,
          notificationType,
          meta,
          createdBy,
          sentAt: DateService.now()
        };
        Notification.create(notifToLogParams)
          .then(resp => {
            return resp;
          })
          .catch(e => {
            LoggerServive.error(e);
          });
      }
    }
  };

  const initiateFirebaseConfig = () => {
    firebase.initializeApp({
      credential: firebase.credential.cert(serviceAccount),
      databaseURL: "https://siha-14051.firebaseio.com"
    });
  };

  const notifyUPKBaseRRUsers = async (patient, notification) => {
    const users = await User.findAll({
      where: {
        upkId: patient.upkId,
        role: ENUM.USER_ROLE.RR_STAFF
      },
      attributes: ["id", "fcmToken"]
    });

    const notificationToSend = notification || ENUM.NOTIFICATION.C_SCH_CHANGED;

    if (users) {
      sendNotification({
        recipientList: users.map(i => ({
          userId: i.id,
          fcm: i.fcmToken
        })),
        title: notificationToSend.title,
        message: notificationToSend.message,
        notificationType: notificationToSend.type,
        meta: {}
      });
    }
  };

  return {
    sendFirebaseMessage,
    sendNotification,
    initiateFirebaseConfig,
    notifyUPKBaseRRUsers
  };
};

module.exports = service;
