const sequelize = require("../../config/database");
const moment = require("moment");
const Sequelize = require("sequelize");
const _ = require("lodash");
const Op = Sequelize.Op;

const Inventory = require("../models/Inventory");
const Medicine = require("../models/Medicine");
const Brand = require("../models/Brand");
const InventoryUsage = require("../models/InventoryUsage");

const LoggerServive = require("./logger.service")();
const QueryService = require("../services/query.service")();

const LogisticRole = require("../../config/logistic_role");
const ENUM = require("../../config/enum");

const InventoryService = () => {
  const getInventoryStock = async (field, id, keyword) => {
    var whereQuery =
      field && id
        ? " WHERE inventories." + field + " = " + id
        : " WHERE inventories.logistic_role = 'MINISTRY_ENTITY' ";
    if (keyword) {
      whereQuery += whereQuery ? " AND " : " WHERE ";
      whereQuery += " medicines.code_name ILIKE '%" + keyword + "%' ";
    }
    let query =
      "SELECT medicines.id,medicines.code_name as medicineName, inventories.package_unit_type as packageUnit, sum(inventories.package_quantity) packageQuantity " +
      "FROM inventories " +
      "LEFT JOIN brands ON brands.id = inventories.brand_id " +
      "LEFT JOIN medicines ON medicines.id = brands.medicine_id " +
      whereQuery +
      " GROUP BY medicines.id,medicineName,packageUnit";

    const InventoryList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return InventoryList;
  };

  const getMedicineStock = async (
    user,
    medicineType,
    medicineId,
    medicineParams,
    customQuery,
    orderBy
  ) => {
    let nonArvMedicineTypeParams = medicineParams
      ? _.pick(medicineParams, [
        "is_io_ims",
        "is_alkes",
        "is_anak",
        "is_cd_vl",
        "is_vl",
        "is_cd4",
        "is_machine",
        "is_preventif",
        "is_eid",
        "is_sifilis",
        "is_r1",
        "is_r2",
        "is_r3",
        "is_profilaksis"
      ])
      : null;

    let whereString = "";

    if (!customQuery) {
      switch (user.logisticrole) {
        case LogisticRole.LAB_ENTITY:
          id = parseInt(user.upkId);
          whereString = id
            ? " where upk_id = " +
            id +
            " AND logistic_role = '" +
            LogisticRole.LAB_ENTITY +
            "' "
            : "";
          break;
        case LogisticRole.PHARMA_ENTITY:
          id = parseInt(user.upkId);
          whereString = id
            ? " where upk_id = " +
            id +
            " AND logistic_role = '" +
            LogisticRole.PHARMA_ENTITY +
            "' "
            : "";
          break;
        case LogisticRole.UPK_ENTITY:
          id = parseInt(user.upkId);
          whereString = id
            ? " where upk_id = " +
            id +
            " AND logistic_role = '" +
            LogisticRole.UPK_ENTITY +
            "' "
            : "";
          break;
        case LogisticRole.SUDIN_ENTITY:
          id = parseInt(user.sudinKabKotaId);
          whereString = id
            ? " where sudin_kab_kota_id = " +
            id +
            " AND logistic_role = '" +
            LogisticRole.SUDIN_ENTITY +
            "' "
            : "";
          break;
        case LogisticRole.PROVINCE_ENTITY:
          id = parseInt(user.provinceId);

          whereString = id
            ? " where province_id = " +
            id +
            " AND logistic_role = '" +
            LogisticRole.PROVINCE_ENTITY +
            "' "
            : "";
          break;
        case LogisticRole.MINISTRY_ENTITY:
          whereString =
            " where logistic_role = '" + LogisticRole.MINISTRY_ENTITY + "' ";
          break;
        default:
          id = parseInt(user.upkId);
          whereString = id ? " where upk_id = " + id : "";
          if (user.role && user.role === ENUM.USER_ROLE.RR_STAFF) {
            if (medicineType === "ARV") {
              whereString = id
                ? " where upk_id = " +
                id +
                " AND logistic_role = '" +
                LogisticRole.PHARMA_ENTITY +
                "' "
                : "";
            } else if (medicineType === "NON_ARV") {
              whereString = id
                ? " where upk_id = " +
                id +
                " AND logistic_role = '" +
                LogisticRole.LAB_ENTITY +
                "' "
                : "";
            }
          }
      }
    } else {
      whereString = customQuery
    }

    let whereStringMedicineId = "";
    let whereStringMedicineFilter = "";
    if (medicineId) {
      whereStringMedicineId += " WHERE medicines.id =  " + medicineId;
    }
    if (nonArvMedicineTypeParams && !_.isEmpty(nonArvMedicineTypeParams)) {
      let nonArvMedicineTypeParamWhere = [];
      if (nonArvMedicineTypeParams.is_cd_vl) {
        nonArvMedicineTypeParams.is_vl = "true";
        nonArvMedicineTypeParams.is_cd4 = "true";
        delete nonArvMedicineTypeParams.is_cd_vl;
      }
      _.keys(nonArvMedicineTypeParams).forEach(item => {
        if (
          nonArvMedicineTypeParams[item] === "true" ||
          nonArvMedicineTypeParams[item] === "TRUE"
        ) {
          nonArvMedicineTypeParamWhere.push(` ${item} = true `);
        } else if (
          nonArvMedicineTypeParams[item] === "false" ||
          nonArvMedicineTypeParams[item] === "FALSE"
        ) {
          nonArvMedicineTypeParamWhere.push(` ${item} = false `);
        }
      });
      whereStringMedicineFilter =
        nonArvMedicineTypeParamWhere.length > 0
          ? "WHERE " + nonArvMedicineTypeParamWhere.join(" AND ")
          : "";
    } else if (medicineParams && medicineParams.is_reagen) {
      whereStringMedicineFilter =
        " WHERE is_r1 = true OR is_r2 = true OR is_r3 = true OR is_anak = true ";
    }

    let query =
      "SELECT medicines.id as id, medicines.name as name, medicines.code_name as codeName, medicines.medicine_type, medicines.category," +
      " arv_medicines.is_lini_1, arv_medicines.is_lini_2, " +
      " non_arv_medicines.is_alkes, non_arv_medicines.is_anak, non_arv_medicines.is_vl, non_arv_medicines.is_cd4, non_arv_medicines.is_io_ims, " +
      " non_arv_medicines.is_preventif, non_arv_medicines.is_r1, non_arv_medicines.is_r2, non_arv_medicines.is_r3, non_arv_medicines.is_sifilis, non_arv_medicines.is_eid, non_arv_medicines.vlcd4_category, " +
      // " non_arv_medicines.is_preventif, non_arv_medicines.is_r1, non_arv_medicines.is_r2, non_arv_medicines.is_r3, non_arv_medicines.is_sifilis, non_arv_medicines.is_eid, " +
      " medicines.package_multiplier,  " +
      " coalesce(inventories.package_quantity, 0) as package_quantity, coalesce(inventories.stock_qty, 0) as stock_qty, " +
      " medicines.stock_unit_type, medicines.package_unit_type " +
      " FROM (SELECT * FROM medicines " +
      whereStringMedicineId +
      ") AS medicines LEFT JOIN non_arv_medicines on medicines.id=non_arv_medicines.medicine_id " +
      " LEFT JOIN arv_medicines on medicines.id=arv_medicines.medicine_id  " +
      " LEFT JOIN  " +
      " ( SELECT brands.medicine_id, sum(inventories.stock_qty) as stock_qty, sum(inventories.package_quantity) as package_quantity  " +
      " FROM inventories JOIN brands on brands.id = inventories.brand_id" +
      whereString +
      " AND inventories.deleted_at IS NULL " +
      " group by brands.medicine_id ) as inventories  " +
      " ON medicines.id = inventories.medicine_id " +
      whereStringMedicineFilter +
      " ORDER BY " + (orderBy ? orderBy : "package_quantity DESC, medicines.id ASC");

    const MedicineWithStockList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results.map(item => {
          item.package_quantity = parseInt(item.package_quantity);
          item.stock_qty = parseInt(item.stock_qty);
          return item;
        });
      });

    let result;
    if (medicineType && (medicineType === "ARV" || medicineType === "NON_ARV")) {
      // console.log(MedicineWithStockList);
      if (medicineType === "ARV") {
        result = MedicineWithStockList.filter(data => {
          // console.log(data.category, medicineType);
          // return data.medicine_type === medicineType || data.category === "IO_IMS";
          return data.medicine_type === medicineType;
        });
      } else {
        result = MedicineWithStockList.filter(data => {
          if (user.role === "PHARMA_STAFF" && user.logisticrole !== "UPK_ENTITY") {
            return data.medicine_type === medicineType && ["IO", "IMS", "IO (TB)", "HIV", "Alkes"].includes(data.category);
          } else if (user.role === "LAB_STAFF") {
            return data.medicine_type === medicineType && ["Reagen IMS", "Reagen HIV"].includes(data.category);
          } else {
            return data.medicine_type === medicineType;
          }
        });
      }
    } else {
      if (medicineId) {
        result =
          MedicineWithStockList && MedicineWithStockList.length > 0
            ? MedicineWithStockList[0]
            : null;
      } else {
        result = MedicineWithStockList;
      }
    }

    return result;
  };

  const getInventoryPickingList = async (field, id, medicineId) => {
    let query =
      "SELECT inventories.id as inventory_id, " +
      "inventories.batch_code, " +
      "inventories.brand_id," +
      "brands.name as brand_name, " +
      "medicines.code_name as medicine_code, " +
      "inventories.expired_date, " +
      "inventories.package_unit_type, " +
      "inventories.package_quantity " +
      "FROM inventories " +
      "LEFT JOIN brands ON brands.id = inventories.brand_id " +
      "LEFT JOIN medicines ON medicines.id = brands.medicine_id " +
      " WHERE brands.medicine_id = " +
      medicineId +
      " AND inventories." +
      field +
      " = " +
      id;

    const PickingList = await sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT
    });

    return PickingList;
  };

  const logInventoryUsage = async (visitObject, inventory, staff) => {
    if (!visitObject || !visitObject.patientId || !visitObject.visitId) {
      LoggerServive.error("Visit object is not assigned");
      return false;
    }

    let medicineStock;
    if (inventory.medicineId) { 
      medicineStock = await getMedicineStock(
        staff,
        null,
        inventory.medicineId,
        null,
        null
      );
    }

    let inventoryUsageParams = {
      patientId: visitObject.patientId,
      visitId: visitObject.visitId,
      upkId: staff.upkId,
      brandId: inventory.brandId,
      usedStockUnit: inventory.usedStockUnit,
      createdBy: staff.id,
      inventoryId: inventory.inventoryId || undefined,
      stockBefore: medicineStock ? medicineStock.stock_qty + inventory.usedStockUnit : 0,
      stockAfter: medicineStock ? medicineStock.stock_qty : 0
    };

    switch (visitObject.visitActivityType) {
      case ENUM.VISIT_ACT_TYPE.HIV:
        inventoryUsageParams.testHivId = visitObject.visitActivityId;
        break;
      case ENUM.VISIT_ACT_TYPE.IMS:
        inventoryUsageParams.testImsId = visitObject.visitActivityId;
        break;
      case ENUM.VISIT_ACT_TYPE.VL_CD4:
        inventoryUsageParams.testVlCd4Id = visitObject.visitActivityId;
        break;
      case ENUM.VISIT_ACT_TYPE.TREATMENT:
        inventoryUsageParams.treatmentId = visitObject.visitActivityId;
        break;
      case ENUM.VISIT_ACT_TYPE.LAB_USAGE_NONPATIENT:
        inventoryUsageParams.labUsageNonpatientId = visitObject.visitActivityId;
        break;
      default:
        break;
    }
    LoggerServive.info(
      "inventoryUsageParams: ",
      JSON.stringify(inventoryUsageParams)
    );
    await InventoryUsage.create(inventoryUsageParams);
  };

  // staff should have upkId field
  const updateInventoryUPKByUsage = async (
    medicineId,
    quantity,
    staff,
    visitObject,
    inserted = false
  ) => {
    if (!staff || !staff.upkId) {
      return {
        quantityAffected: null,
        error: new Error("user_role_should_be_staff")
      };
    }

    const medicine = await Medicine.findOne({
      where: {
        id: medicineId
      }
    });

    const brandList = await Brand.findAll({
      where: {
        medicineId: medicineId
      }
    });
    const brandListId = brandList.map((data, idx) => {
      return data.id;
    });

    let whereObj = {
      brandId: brandListId,
      upkId: staff.upkId,
      logisticRole: staff.logisticrole || "UPK_ENTITY",
      expiredDate: {
        [Op.gt]: moment()
          .local()
          .endOf("day")
          .add(1, 'day')
          .format()
      },
      stockQty: {
        [Op.gt]: 0
      },
      // tidak perlu
      // packageQuantity: {
      //   [Op.gt]: 0
      // }
    };

    if (medicine) {
      if (staff.role === ENUM.USER_ROLE.RR_STAFF) {
        if (medicine.medicineType === ENUM.MEDICINE_TYPE.ARV) {
          whereObj.logisticRole = ENUM.LOGISTIC_ROLE.PHARMA_ENTITY;
        } else if (medicine.medicineType === ENUM.MEDICINE_TYPE.NON_ARV) {
          whereObj.logisticRole = ENUM.LOGISTIC_ROLE.LAB_ENTITY;
        }
      }
    }

    let orderObj = [
      ["expiredDate", "ASC"],
      ["createdAt", "ASC"]
    ];

    // const existingStockQty = await Inventory.sum("stockQty", {
    //   where: whereObj
    // });

    const med = await getMedicineStock(staff, 'NON_ARV', medicineId);
    let existingStockQty;
    if (med[0]) {
      existingStockQty = med[0].stock_qty;
    } else {
      existingStockQty = null;
    }

    // console.log(`... ${existingStockQty} ...`);

    if (!existingStockQty || existingStockQty < quantity) {
      LoggerServive.error(
        "Update Canceled! Existing Stock for Medicine ID (",
        medicineId,
        ") with Stock (",
        isNaN(existingStockQty) ? 0 : existingStockQty,
        ") ",
        " < ",
        "Requested Stock (",
        quantity,
        ")"
      );
      return {
        quantityAffected: null,
        error: new Error("stock_is_not_sufficient")
      };
    }

    LoggerServive.info(
      `Affecting on Medicine ID: ${medicineId} with Stock ( ${existingStockQty} ) Requested Stock: ${quantity} - Using Logistic Role: ${whereObj.logisticRole} at UPK ID: ${whereObj.upkId}`
    );
    LoggerServive.info(
      `Staff Role: ${staff.role} and medicine type: ${medicine.medicineType}`
    );
    LoggerServive.info(`Begin updating inventory...`);

    // console.log('........ where pickingList', whereObj);

    const pickingList = await Inventory.findAll({
      where: whereObj,
      order: orderObj,
      include: [
        {
          model: Brand,
          // required: true
        }
      ]
    });

    // console.log('........... pickingList', inserted, pickingList);

    let quantityAffected = quantity;
    // let usedPkgUnit = 0;
    if (!inserted) {
      for (i = 0; i < pickingList.length; i++) {
        const inventoryItem = pickingList[i];
        // console.log('... Before', inventoryItem);
        if (inventoryItem.stockQty >= quantityAffected) {
          inventoryItem.stockQty -= quantityAffected;
          inventoryItem.packageQuantity = Math.floor(
            inventoryItem.stockQty / inventoryItem.brand.packageMultiplier
          );
          // usedPkgUnit += Math.ceil(quantityAffected / inventoryItem.brand.packageMultiplier);
          quantityAffected -= quantityAffected;
        } else {
          quantityAffected -= inventoryItem.stockQty;
          // usedPkgUnit += Math.ceil(inventoryItem.stockQty / inventoryItem.brand.packageMultiplier);
          inventoryItem.stockQty = 0;
          inventoryItem.packageQuantity = 0;
        }
        // console.log('... After', inventoryItem);
        await inventoryItem.save();
        await logInventoryUsage(
          visitObject,
          {
            usedStockUnit: quantity,
            brandId: inventoryItem.brandId,
            inventoryId: inventoryItem.id,
            medicineId: medicine.id
          },
          staff
        );
        if (quantityAffected <= 0) {
          break;
        }
      }
    } else {
      const inventoryItem = pickingList[0];
      inventoryItem.stockQty += quantity;
      inventoryItem.packageQuantity = Math.floor(
        inventoryItem.stockQty / inventoryItem.brand.packageMultiplier
      );

      // console.log('... Else', inventoryItem);

      await inventoryItem.save();
    }
    return {
      quantityAffected: quantity,
      medicineId: medicineId
    };
  };

  const getPergerakanStok = async params => {
    let {
      page,
      limit,
      keyword,
      upkId,
      sudinKabKotaId,
      provinceId,
      medicineId,
      monthStart,
      monthEnd
    } = params;
    const offset = limit * page;
    var whereParams =
      monthStart && monthEnd
        ? QueryService.queryPeriodeByMonth(
          `"inventoryLog"."activity_date"`,
          monthStart,
          monthEnd
        )
        : "";
    let whereQuery = whereParams ? " AND" + whereParams + " " : "";
    whereQuery += medicineId
      ? ` AND "inventoryLog".medicine_id = ` + medicineId + " "
      : "";

    if (upkId) {
      whereQuery += whereQuery
        ? ` AND "inventory".upk_id = ` + upkId + ` `
        : "";
    } else if (sudinKabKotaId) {
      whereQuery += whereQuery
        ? ` AND "inventory".sudin_kab_kota_id = ` + sudinKabKotaId + ` `
        : "";
    } else if (provinceId) {
      whereQuery += whereQuery
        ? ` AND "inventory".province_id = ` + provinceId + ` `
        : "";
    }

    let query =
      `SELECT to_char("inventoryLog"."activity_date", 'YYYY-MM') AS "activityDate", ` +
      '"inventoryLog"."prev_stock_unit" AS "prevStockUnit", ' +
      'sum("after_stock_qty") AS "stockTotal", ' +
      '"inventory->brand->medicine"."id" AS "medicineId", ' +
      '"inventory->brand->medicine"."code_name" AS "codeName" ' +
      'FROM "inventory_logs" AS "inventoryLog" ' +
      'INNER JOIN "inventories" AS "inventory" ON "inventoryLog"."inventory_id" = "inventory"."id" AND "inventory"."deleted_at" IS NULL ' +
      'LEFT OUTER JOIN "brands" AS "inventory->brand" ON "inventory"."brand_id" = "inventory->brand"."id" AND("inventory->brand"."deleted_at" IS NULL) ' +
      'LEFT OUTER JOIN "medicines" AS "inventory->brand->medicine" ON "inventory->brand"."medicine_id" = "inventory->brand->medicine"."id" ' +
      'AND("inventory->brand->medicine"."deleted_at" IS NULL) ' +
      'WHERE "inventoryLog"."deleted_at" IS NULL ' +
      whereQuery +
      'GROUP BY "activityDate", "prevStockUnit", "medicineId", "codeName" ' +
      'ORDER BY "medicineId" ';

    const InventoryList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return InventoryList;
  };

  return {
    getInventoryStock,
    getMedicineStock,
    getInventoryPickingList,
    updateInventoryUPKByUsage,
    getPergerakanStok
  };
};

module.exports = InventoryService;
