const validationService = () => {
  const validateInstance = async (sequelizeModel, dataParams) => {
    try {
      const model = await sequelizeModel.build(dataParams);
      await model.validate();
      return false;
    } catch (e) {
      return e;
    }
  };

  return {
    validateInstance
  };
};

module.exports = validationService;
