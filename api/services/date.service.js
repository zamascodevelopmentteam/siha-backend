const moment = require("moment");

const generalConfig = require("../../config/general");

const LoggerServive = require("./logger.service")();

const service = () => {
  const now = () => {
    return moment()
      .local()
      .format();
  };

  return { now };
};

module.exports = service;
