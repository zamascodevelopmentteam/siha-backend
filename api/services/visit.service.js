const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const _ = require("lodash");
const Op = Sequelize.Op;

const Visit = require("../models/Visit");
const Treatment = require("../models/Treatment");
const Couple = require("../models/Couple");
const User = require("../models/User");
const Upk = require("../models/Upk");
const Patient = require("../models/Patient");
const Medicine = require("../models/Medicine");
const Prescription = require("../models/Prescription");
const Regiment = require("../models/Regiment");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");
const TestHiv = require("../models/TestHiv");
const TestIms = require("../models/TestIms");
const TestVl = require("../models/TestVl");
const PatientLog = require("../models/PatientLog");

const ENUM = require("../../config/enum");
const PatientService = require("./patient.service")();
const VariableService = require("./variable.service")();

const VisitService = () => {
  const getVisitDetailAssociations = () => {
    return [
      {
        model: Treatment,
        required: false,
        include: [
          {
            model: Medicine,
            required: false,
            as: "obatArv1Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "obatArv2Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "obatArv3Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Prescription,
            required: false,
            include: [
              {
                model: PrescriptionMedicine,
                include: [
                  {
                    model: Medicine,
                    required: true,
                    attributes: ["name", "codeName"]
                  }
                ]
              }
            ]
          },
          {
            model: Couple
          }
        ]
      },
      {
        model: TestHiv,
        required: false,
        include: [
          {
            model: Medicine,
            required: false,
            as: "namaReagenR1Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenR2Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenR3Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenNatData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenDnaData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenRnaData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenElisaData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenWbData",
            attributes: ["name", "codeName"]
          }
        ]
      },
      {
        model: TestIms,
        required: false,
        include: [
          {
            model: Medicine,
            required: false,
            as: "namaReagenData",
            attributes: ["name", "codeName"]
          }
        ]
      },
      {
        model: TestVl,
        required: false,
        include: [
          {
            model: Medicine,
            required: false,
            as: "namaReagenData",
            attributes: ["name", "codeName"]
          }
        ]
      },
      {
        model: Upk,
        required: false,
        attributes: ["name"]
      },
      {
        model: Patient,
        required: false,
        attributes: ["id", "name", "nik", "statusPatient"]
      },
      {
        model: User,
        required: false,
        as: "petugasRR",
        attributes: ["name", "nik"]
      },
      {
        model: Prescription,
        required: false,
        include: [
          {
            model: PrescriptionMedicine,
            include: [
              {
                model: Medicine,
                required: true,
                attributes: ["name", "codeName"]
              }
            ]
          }
        ]
      }
    ];
  };

  const getVisitDetail = async whereParam => {
    const visit = await Visit.findOne({
      where: whereParam,
      include: getVisitDetailAssociations(),
      order: [["ordinal", "DESC"]]
    });

    return visit;
  };

  const getDetailTreatmentPrevVisit = async visitId => {
    const visit = await Visit.findOne({
      where: {
        id: visitId
      },
      include: [
        {
          model: Treatment,
          required: true
        }
      ]
    });

    if (visit) {
      var prevTreatmentVisit = await Visit.findOne({
        where: {
          patientId: visit.patientId,
          ordinal: {
            [Op.lt]: visit.ordinal
          }
        },
        include: [
          {
            model: Treatment,
            required: true,
            include: [
              {
                model: Medicine,
                required: false,
                as: "obatArv1Data",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "obatArv2Data",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "obatArv3Data",
                attributes: ["name", "codeName"]
              }
            ]
          }
        ],
        order: [["ordinal", "DESC"]]
      });
      if (prevTreatmentVisit && prevTreatmentVisit.treatment) {
        return prevTreatmentVisit.treatment;
      } else {
        return null;
      }
    } else {
      return null;
    }
  };

  const getNextOrdinalByVisit = async patientId => {
    let query =
      "SELECT MAX(ordinal) as latest_ordinal from visits where patient_id = :patientId group by patient_id ";

    const nextOrdinal = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          patientId: patientId
        }
      })
      .then(results => {
        if (results && results.length > 0) {
          let latestOrdinal = parseInt(results[0].latest_ordinal);
          return isNaN(latestOrdinal) ? 1 : latestOrdinal + 1;
        } else {
          return 1;
        }
      });

    return nextOrdinal;
  };

  const getNextOrdinalByVisitActivity = async (
    visitActivityType,
    patientId,
    visitId
  ) => {
    let baseTable;
    switch (visitActivityType) {
      case ENUM.VISIT_ACT_TYPE.HIV:
        baseTable = "test_hiv";
        break;
      case ENUM.VISIT_ACT_TYPE.IMS:
        baseTable = "test_ims";
        break;
      case ENUM.VISIT_ACT_TYPE.VL_CD4:
        baseTable = "test_vl_cd4";
        break;
      case ENUM.VISIT_ACT_TYPE.TREATMENT:
        baseTable = "treatments";
        break;
      default:
        baseTable = "test_hiv";
    }

    let subQueryVisit =
      " SELECT patient_id FROM visits WHERE visits.id = :visitId ";

    let query =
      "SELECT MAX(ordinal) as latest_ordinal " +
      " FROM (select visits.id, patient_id FROM visits where patient_id IN (" +
      (patientId ? " :patientId " : subQueryVisit) +
      ") ) as visits " +
      " JOIN " +
      baseTable +
      " on visits.id = " +
      baseTable +
      ".visit_id " +
      " GROUP BY patient_id ";

    const nextOrdinal = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          patientId: parseInt(patientId),
          visitId: parseInt(visitId)
        }
      })
      .then(results => {
        if (results && results.length > 0) {
          let latestOrdinal = parseInt(results[0].latest_ordinal);
          return isNaN(latestOrdinal) ? 1 : latestOrdinal + 1;
        } else {
          return 1;
        }
      });

    return nextOrdinal;
  };

  const doTreatmentVisit = async (patient, visit, staff, treatmentParams) => {
    if (patient.statusPatient !== ENUM.STATUS_PATIENT.ODHA && patient.statusPatient !== ENUM.STATUS_PATIENT.ODHA_LAMA) {
      throw {
        message: "patient_is_not_odha"
      };
    }
    const newTreatment = await Treatment.create(
      Object.assign(treatmentParams || {}, {
        ordinal: await getNextOrdinalByVisitActivity(
          ENUM.VISIT_ACT_TYPE.TREATMENT,
          null,
          visit.id
        ),
        visitId: visit.id,
        isOnTransit: patient.upkId === staff.upkId ? false : true,
        createdBy: staff.id
      })
    );
    let metaObject = {
      lastTreatmentDate: new Date()
    };
    if (newTreatment && newTreatment.ordinal == 1) {
      metaObject.firstTreatmentDate = new Date();
    }
    PatientService.updateMetaPatient(patient.id, metaObject);

    const previousTreatmentVisit = await getPreviousTreatmentVisit(visit);
    if (previousTreatmentVisit) {
      if (previousTreatmentVisit.treatment) {
        if (
          previousTreatmentVisit.treatment.akhirFollowUp ===
            ENUM.AKHIR_FOLLOW_UP.RUJUK_KELUAR &&
          previousTreatmentVisit.treatment.tglRujukKeluar
        ) {
          newTreatment.tglRujukMasuk = new Date();
          newTreatment.isOnTransit = false;
          await PatientLog.update(
            {
              endDate: new Date(new Date().setDate(new Date().getDate() - 1)),
              visitId: visit.id,
              tglRujukMasuk: newTreatment.tglRujukMasuk,
              tglRujukKeluar: previousTreatmentVisit.treatment.tglRujukKeluar,
              updatedBy: staff.id
            },
            {
              where: {
                patientId: patient.id,
                upkId: patient.upkId,
                endDate: {
                  [Op.gt]: new Date()
                }
              }
            }
          );
          await PatientLog.create({
            patientId: patient.id,
            upkId: staff.upkId,
            startDate: new Date(),
            createdBy: staff.id
          });
          patient.upkId = staff.upkId;
          await patient.save();
        }
        newTreatment.lsmPenjangkau = previousTreatmentVisit.treatment.lsmPenjangkau
      }
      _.assign(
        newTreatment,
        _.pick(previousTreatmentVisit.treatment, [
          "treatmentStartDate",
          "artStartDate",
          "noRegNas",
          "lsmPenjangkau",
          "tglKonfirmasiHivPos",
          "kelompokPopulasi",
          "statusTb",
          "tglPengobatanTb",
          "statusFungsional",
          "stadiumKlinis",
          "ppk",
          "tglPemberianPpk",
          "statusTbTpt",
          "tglPemberianTbTpt",
          "paduanObatArv",
          "statusPaduan",
          "tglPemberianObat",
          "tglPemberianObatSelesai",
          "notifikasiPasangan",
          "obatArv1",
          "obatArv2",
          "obatArv3",
          "paduanObatArv",
          "statusPaduan",
          "weight",
          "height",
          "tglMeninggal",
          "tglBerhentiArv"
          
        ])
      );
      newTreatment.upkSebelumnya = previousTreatmentVisit.upkId;
      if (previousTreatmentVisit.treatment.couples) {
        await Couple.bulkCreate(
          previousTreatmentVisit.treatment.couples.map(c => ({
            name: c.name,
            age: c.age,
            gender: c.gender,
            relationship: c.relationship,
            treatmentId: newTreatment.id
          }))
        );
      }
      await newTreatment.save();

      // console.log("newTreatment: ", newTreatment.toJSON());
      // console.log(
      //   "copying prev treatment: ",
      //   previousTreatmentVisit.treatment.toJSON()
      // );
    }
    return newTreatment;
  };

  const initiatePrescription = async (visit, staff, isDraft) => {
    const lastPrescription = await getLastGivenPrescription(visit.patientId);

    let prescription;

    if (lastPrescription) {
      prescription = await Prescription.create({
        patientId: visit.patientId,
        visitId: visit.id,
        regimentId: lastPrescription.regimentId,
        prescriptionNumber: lastPrescription.prescriptionNumber,
        paduanObatIds: lastPrescription.paduanObatIds,
        paduanObatStr: lastPrescription.paduanObatStr,
        notes: lastPrescription.notes,
        isDraft: isDraft ? isDraft : false,
        createdBy: staff.id
      });
      //creating prescription copy
      for (
        let ii = 0;
        ii < lastPrescription.prescriptionMedicines.length;
        ii++
      ) {
        const presMed = {
          prescriptionId: prescription.id,
          medicineId: lastPrescription.prescriptionMedicines[ii].medicineId,
          amount: 0,
          jumlahHari: 0,
          notes: lastPrescription.prescriptionMedicines[ii].notes,
          createdBy: staff.id
        };
        await PrescriptionMedicine.create(presMed);
      }
    } else {
      prescription = await Prescription.create({
        isDraft: true,
        createdBy: staff.id,
        patientId: visit.patientId,
        visitId: visit.id
      });
      prescription.prescriptionNumber = visit.upkId
        .toString()
        .concat("-", visit.id, ".", visit.ordinal, "#", prescription.id);
      await prescription.save();

      const tleMedicine = await Medicine.findByPk(
        VariableService.SETTINGS.MAIN.INTRA.TLEMedicineId
      );
      if (tleMedicine) {
        prescription.paduanObatIds = tleMedicine.id;
        prescription.paduanObatStr = tleMedicine.codeName;
        const presMed = {
          prescriptionId: prescription.id,
          medicineId: tleMedicine.id,
          amount: 0,
          jumlahHari: 0,
          notes: "",
          createdBy: staff.id
        };
        await PrescriptionMedicine.create(presMed);
        await prescription.save();
      }
    }
    return prescription;
  };

  const getLastGivenPrescription = async patientId => {
    const prescription = await Prescription.findOne({
      where: {
        patientId: patientId,
        isDraft: false
      },
      include: [
        {
          model: PrescriptionMedicine,
          required: true,
          include: [{ model: Medicine, required: false }]
        }
      ],
      order: [["id", "DESC"]]
    });
    return prescription;
  };

  const getPreviousTreatmentVisit = async currentVisit => {
    const previousTreatmentVisit = await Visit.findOne({
      where: {
        patientId: currentVisit.patientId,
        ordinal: {
          [Op.lt]: currentVisit.ordinal
        },
        visitType: ENUM.VISIT_TYPE.TREATMENT
      },
      include: [
        {
          model: Treatment,
          required: true,
          include: [
            {
              model: Couple,
              required: false
            }
          ]
        }
      ],
      order: [["ordinal", "DESC"]]
    });
    return previousTreatmentVisit;
  };

  const getStatusPaduanObat = async currentTreatmentVisit => {
    let statusPaduan;
    const prevTreatmentVisit = await getPreviousTreatmentVisit(
      currentTreatmentVisit
    );
    if (
      currentTreatmentVisit &&
      currentTreatmentVisit.treatment.prescriptionId &&
      prevTreatmentVisit &&
      prevTreatmentVisit.treatment.prescriptionId
    ) {
      const currentPrescRegiment = await Prescription.findOne({
        where: {
          id: currentTreatmentVisit.treatment.prescriptionId
        },
        include: [
          {
            model: Regiment,
            required: true
          }
        ]
      });
      const prevPrescRegiment = await Prescription.findOne({
        where: {
          id: prevTreatmentVisit.treatment.prescriptionId
        },
        include: [
          {
            model: Regiment,
            required: true
          }
        ]
      });
      if (currentPrescRegiment && prevPrescRegiment) {
        const currentRegiment = currentPrescRegiment.regiment;
        const prevRegiment = prevPrescRegiment.regiment;

        if (
          currentRegiment.combinationIds !== prevRegiment.combinationIds &&
          currentRegiment.name === prevRegiment.name
        ) {
          statusPaduan = ENUM.STATUS_PADUAN.SUBSTITUSI;
        } else if (
          currentRegiment.name !== prevRegiment.name &&
          _.includes(currentRegiment.name, ENUM.REGIMENT.LINI2)
        ) {
          statusPaduan = ENUM.STATUS_PADUAN.SWITCH;
        } else {
          statusPaduan = ENUM.STATUS_PADUAN.ORISINAL;
        }
      } else {
        statusPaduan = ENUM.STATUS_PADUAN.ORISINAL;
      }
    } else {
      statusPaduan = ENUM.STATUS_PADUAN.ORISINAL;
    }
    return statusPaduan;
  };

  const checkVisitDataCompleteness = visitObject => {
    let completenessObject = {
      Treatment: true,
      TestHiv: true,
      TestIms: true,
      TestVl: true,
      Prescription: true
    };
    if (visitObject.treatment) {
      const treatment = visitObject.treatment;
      if (
        !treatment.prescriptionId ||
        !treatment.treatmentStartDate ||
        !treatment.noRegNas ||
        !treatment.tglKonfirmasiHivPos ||
        !treatment.tglKunjungan ||
        !treatment.kelompokPopulasi ||
        !treatment.statusTb ||
        !treatment.tglPengobatanTb ||
        !treatment.statusFungsional ||
        !treatment.stadiumKlinis ||
        _.isNull(treatment.ppk) ||
        !treatment.tglPemberianPpk ||
        !treatment.statusTbTpt ||
        !treatment.tglPemberianTbTpt ||
        !treatment.tglRencanaKunjungan ||
        !treatment.weight
      ) {
        completenessObject.Treatment = false;
      }
    }
    if (visitObject.testHiv) {
      const testHiv = visitObject.testHiv;
      if (
        !testHiv.kelompokPopulasi ||
        !testHiv.alasanTest ||
        !testHiv.tanggalTest ||
        !testHiv.jenisTest ||
        !testHiv.kesimpulanHiv
      ) {
        completenessObject.TestHiv = false;
      }
      if (testHiv.jenisTest) {
        switch (testHiv.jenisTest) {
          case ENUM.HIV_TEST_TYPE.RDT:
            if (!testHiv.namaReagenR1 || !testHiv.hasilTestR1) {
              completenessObject.TestHiv = false;
            }
            if (testHiv.hasilTestR1 === ENUM.TEST_RESULT.REAKTIF) {
              if (!testHiv.namaReagenR2 || !testHiv.hasilTestR2) {
                completenessObject.TestHiv = false;
              }
            }
            if (testHiv.hasilTestR2 === ENUM.TEST_RESULT.REAKTIF) {
              if (!testHiv.namaReagenR3 || !testHiv.hasilTestR3) {
                completenessObject.TestHiv = false;
              }
            }
            break;
          case ENUM.HIV_TEST_TYPE.PCR_DNA:
            if (!testHiv.namaReagenDna || !testHiv.hasilTestDna) {
              completenessObject.TestHiv = false;
            }
            break;
          case ENUM.HIV_TEST_TYPE.PCR_RNA:
            if (!testHiv.namaReagenRna || !testHiv.hasilTestRna) {
              completenessObject.TestHiv = false;
            }
            break;
          case ENUM.HIV_TEST_TYPE.NAT:
            if (!testHiv.namaReagenNat || !testHiv.hasilTestNat) {
              completenessObject.TestHiv = false;
            }
            break;
          default:
            break;
        }
      }
    }
    if (visitObject.testIms) {
      if (
        visitObject.testIms.ditestSifilis == true &&
        !visitObject.testIms.namaReagen
      ) {
        completenessObject.TestIms = false;
      } else if (
        visitObject.testIms.ditestIms == true &&
        _.isNull(visitObject.testIms.hasilTestIms)
      ) {
        completenessObject.TestIms = false;
      } else if (_.isNull(visitObject.testIms.ditestIms)) {
        completenessObject.TestIms = false;
      }
    }
    if (visitObject.testVL) {
      if (
        _.isNull(visitObject.testVL.testVlCd4Type) ||
        _.isNull(visitObject.testVL.namaReagen) ||
        _.isNull(visitObject.testVL.hasilTestVlCd4)
      ) {
        completenessObject.TestVl = false;
      }
    }

    if (
      (visitObject.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA || visitObject.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA) &&
      !visitObject.treatment &&
      !visitObject.testVL
    ) {
      completenessObject.Treatment = false;
      completenessObject.TestVl = false;
    } else if (
      (visitObject.patient.statusPatient !== ENUM.STATUS_PATIENT.ODHA && visitObject.patient.statusPatient !== ENUM.STATUS_PATIENT.ODHA_LAMA) &&
      !visitObject.testHiv &&
      !visitObject.testIms
    ) {
      completenessObject.TestHiv = false;
      completenessObject.TestIms = false;
    }
    return completenessObject;
  };

  const queryBuilder_patientNotification = dayDiff => {
    dayDiff = dayDiff || 7;

    const str = `SELECT
	users.id,
	treatment_visits.patient_id,
	users."name",
	users.fcm_token,
	treatment_visits.tgl_rencana_kunjungan::date as tgl_rencana_kunjungan
FROM (
	SELECT
		users.id,
		users.name,
		users.fcm_token
	FROM
		users
  WHERE fcm_token IS NOT NULL
) users
	JOIN patients ON users.id = patients.user_id
	JOIN (
		SELECT
			patient_id,max(treatments.tgl_rencana_kunjungan) tgl_rencana_kunjungan
		FROM
			visits
			JOIN treatments ON treatments.visit_id = visits.id
			WHERE tgl_rencana_kunjungan IS NOT NULL AND tgl_rencana_kunjungan > now() AND DATE_PART('day', tgl_rencana_kunjungan - now()) = ${dayDiff}
			GROUP BY patient_id
		) treatment_visits ON patients.id = treatment_visits.patient_id
WHERE
	tgl_rencana_kunjungan IS NOT NULL
ORDER BY tgl_rencana_kunjungan ASC, patient_id ASC`;
    return str;
  };

  return {
    getNextOrdinalByVisit,
    getNextOrdinalByVisitActivity,
    getDetailTreatmentPrevVisit,
    getVisitDetail,
    initiatePrescription,
    doTreatmentVisit,
    getLastGivenPrescription,
    getPreviousTreatmentVisit,
    getStatusPaduanObat,
    checkVisitDataCompleteness,
    queryBuilder_patientNotification
  };
};

module.exports = VisitService;
