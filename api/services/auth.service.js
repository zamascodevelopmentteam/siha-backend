const jwt = require("jsonwebtoken");
const User = require("../models/User");

const secret =
  process.env.NODE_ENV === "production"
    ? process.env.JWT_SECRET || "iusd9898dsaDAjd8KK"
    : "secret";
const uuidv4 = require("uuid/v4");

const authService = () => {
  const issue = payload => {
    return jwt.sign(payload, secret, { expiresIn: 10800 * 240 });
  };
  const verify = (token, cb) => jwt.verify(token, secret, {}, cb);
  const issueRefresh = async userId => {
    const refreshToken = uuidv4();
    const user = await User.findByPk(userId);
    user.refreshToken = refreshToken;
    await user.save();
    return refreshToken;
  };

  return {
    issue,
    verify,
    issueRefresh
  };
};

module.exports = authService;
