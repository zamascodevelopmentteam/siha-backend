const User = require("../../models/User");

module.exports = function(agenda) {
  agenda.define("testAgendaJob", async job => {
    const user = await User.findOne({});
    console.log("user", user ? user.toJSON() : "none");
  });

  agenda.define("reset password", async job => {
    // Etc
  });

  // More email related jobs
};
