const Sentry = require("@sentry/node");
const _ = require("lodash");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");

const sequelize = require("../../../config/database");

const ENUM = require("../../../config/enum");

const NotificationService = require("../notification.service")();
const VisitService = require("../visit.service")();

const User = require("../../models/User");
const Upk = require("../../models/Upk");
const Visit = require("../../models/Visit");
const Treatment = require("../../models/Treatment");

module.exports = function(agenda) {
  agenda.define("notifyPatientsAWeekBefore", async (job, done) => {
    try {
      console.log("Notifying patients 7 days before... ");
      let listOfTargetUsers = await sequelize.query(
        VisitService.queryBuilder_patientNotification(7),
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: {}
        }
      );

      if (listOfTargetUsers) {
        const notificationToSend = ENUM.NOTIFICATION.R_1WEEK;

        listOfTargetUsers.forEach(user => {
          console.log(
            `Sending reminder to: ${user.name}, with patient ID ${user.patient_id} and visit date: ${user.tgl_rencana_kunjungan}`
          );
          if (user && user.fcm_token) {
            NotificationService.sendNotification({
              recipient: { userId: user.id, fcm: user.fcm_token },
              title: notificationToSend.title,
              message: notificationToSend.message,
              notificationType: notificationToSend.type,
              meta: {}
            });
          }
        });
      }
    } catch (err) {
      console.log("Error: ", err);
      Sentry.captureException(err);
    } finally {
      done();
    }
  });
  agenda.define("notifyPatientsAtTheDay", async (job, done) => {
    try {
      console.log("Notifying patients a day before... ");
      let listOfTargetUsers = await sequelize.query(
        VisitService.queryBuilder_patientNotification(1),
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: {}
        }
      );

      if (listOfTargetUsers) {
        const notificationToSend = ENUM.NOTIFICATION.R_1DAY;

        listOfTargetUsers.forEach(user => {
          if (user && user.fcm_token) {
            NotificationService.sendNotification({
              recipient: { userId: user.id, fcm: user.fcm_token },
              title: notificationToSend.title,
              message: notificationToSend.message,
              notificationType: notificationToSend.type,
              meta: {}
            });
          }
        });
      }
    } catch (err) {
      console.log("Error: ", err);
      Sentry.captureException(err);
    } finally {
      done();
    }
  });
  agenda.define("notifyPatientsAtTheDay", async (job, done) => {
    try {
      console.log("Notifying patients at the day... ");
      let listOfTargetUsers = await sequelize.query(
        VisitService.queryBuilder_patientNotification(0),
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: {}
        }
      );

      if (listOfTargetUsers) {
        const notificationToSend = ENUM.NOTIFICATION.R_DDAY;

        listOfTargetUsers.forEach(user => {
          if (user && user.fcm_token) {
            NotificationService.sendNotification({
              recipient: { userId: user.id, fcm: user.fcm_token },
              title: notificationToSend.title,
              message: notificationToSend.message,
              notificationType: notificationToSend.type,
              meta: {}
            });
          }
        });
      }
    } catch (err) {
      console.log("Error: ", err);
      Sentry.captureException(err);
    } finally {
      done();
    }
  });
};
