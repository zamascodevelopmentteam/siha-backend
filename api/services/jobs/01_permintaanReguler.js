const Sentry = require("@sentry/node");
const _ = require("lodash");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");

const sequelize = require("../../../config/database");

const Lbpha2Service = require("../report/lbpha2/lbpha2.service")();

const User = require("../../models/User");
const Order = require("../../models/Order");
const Medicine = require("../../models/Medicine");
const OrderItem = require("../../models/OrderItem");
const ENUM = require("../../../config/enum");
const OrderService = require("../Order.service")();
const LoggerServive = require("../logger.service")();

module.exports = function(agenda) {
  agenda.define("generatePermintaanRegulerUPK", async (job, done) => {
    console.info(
      "Running jobs: Buat Permintaan Reguler UPK, at ",
      moment()
        .local()
        .format()
    );
    try {
      let listOfUPK = await sequelize.query(
        Lbpha2Service.generatePermintaanRegulerUPKQueryBuilder(),
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: {}
        }
      );
      listOfUPK = _.groupBy(listOfUPK, "upk_id");
      _.each(listOfUPK, ordersPerUPK => {
        let ordersItems = [];
        let upkId = null;
        let sudinKabKotaId = null;
        let provinceId = null;
        for (i in ordersPerUPK) {
          upkId = ordersPerUPK[i].upk_id;
          sudinKabKotaId = ordersPerUPK[i].sudin_kab_kota_id;
          provinceId = ordersPerUPK[i].province_id;
          const stockRequest =
            ordersPerUPK[i].stok_keluar_reguler *
              ordersPerUPK[i].order_multiplier -
            ordersPerUPK[i].stock_on_hand;
          ordersItems.push({
            medicineId: ordersPerUPK[i].medicine_id,
            stockQty: stockRequest,
            expiredDate: new Date()
          });
        }
        const paramRequest = {
          logisticrole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId,
          ordersItems: ordersItems
        };
        console.log("UPK Name: ", ordersPerUPK[i].upk_name);
        // console.log("paramRequest: ", paramRequest);
        autoGenerateCreate(paramRequest);
      });
    } catch (err) {
      console.log("Error: ", err);
      Sentry.captureException(err);
    } finally {
      done();
    }
  });
  agenda.define("generatePermintaanRegulerSudin", async (job, done) => {
    try {
      console.info(
        "Running jobs: Buat Permintaan Reguler SUDIN, at ",
        moment()
          .local()
          .format()
      );
      let listOfSudin = await sequelize.query(
        Lbpha2Service.generatePermintaanRegulerSudinQueryBuilder(),
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: {}
        }
      );
      listOfSudin = _.groupBy(listOfSudin, "sudin_kab_kota_id");
      _.each(listOfSudin, ordersPerSudin => {
        let ordersItems = [];
        let upkId = null;
        let sudinKabKotaId = null;
        let provinceId = null;
        for (i in ordersPerSudin) {
          sudinKabKotaId = ordersPerSudin[i].sudin_kab_kota_id;
          provinceId = ordersPerSudin[i].province_id;
          const stockRequest =
            ordersPerSudin[i].stok_keluar_reguler *
              ordersPerSudin[i].order_multiplier -
            ordersPerSudin[i].stock_on_hand;
          ordersItems.push({
            medicineId: ordersPerSudin[i].medicine_id,
            stockQty: stockRequest,
            expiredDate: new Date()
          });
        }
        const paramRequest = {
          logisticrole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId,
          ordersItems: ordersItems
        };
        console.log("Sudin Name: ", ordersPerSudin[i].sudin_name);
        // console.log("paramRequest: ", paramRequest);
        autoGenerateCreate(paramRequest);
        // creating orders here
      });
    } catch (err) {
      console.log("Error: ", err);
      Sentry.captureException(err);
    } finally {
      done();
    }
  });
  agenda.define("generatePermintaanRegulerProvince", async (job, done) => {
    try {
      let listOfProvince = await sequelize.query(
        Lbpha2Service.generatePermintaanRegulerProvinceQueryBuilder(),
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: {}
        }
      );
      listOfProvince = _.groupBy(listOfProvince, "province_id");
      _.each(listOfProvince, ordersPerProvince => {
        let ordersItems = [];
        let upkId = null;
        let sudinKabKotaId = null;
        let provinceId = null;
        for (i in ordersPerProvince) {
          provinceId = ordersPerProvince[i].province_id;
          const stockRequest =
            ordersPerProvince[i].stok_keluar_reguler *
              ordersPerProvince[i].order_multiplier -
            ordersPerProvince[i].stock_on_hand;
          ordersItems.push({
            medicineId: ordersPerProvince[i].medicine_id,
            stockQty: stockRequest,
            expiredDate: new Date()
          });
        }
        const paramRequest = {
          logisticrole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId,
          ordersItems: ordersItems
        };

        console.log("Province Name: ", ordersPerProvince[i].province_name);
        // console.log("paramRequest: ", paramRequest);
        autoGenerateCreate(paramRequest);
        // creating orders here
      });
    } catch (err) {
      console.log("Error: ", err);
      Sentry.captureException(err);
    } finally {
      done();
    }
  });

  const generateInvoiceNumber = lastOrder => {
    let lastNum = 0;
    if (lastOrder) {
      lastNum = Number(lastOrder.orderCode.split("/")[2]);
    }
    lastNum++;
    return "INV-REG/" + moment().format("YYYYMMDD") + "/" + String(lastNum);
  };
  const autoGenerateCreate = async param => {
    const {
      logisticrole,
      upkId,
      sudinKabKotaId,
      provinceId,
      ordersItems
    } = param;
    try {
      //for get idFor
      var orderList = [];
      if (logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY) {
        orderList = await OrderService.getAvailableUpk(upkId);
      } else if (logisticrole === ENUM.LOGISTIC_ROLE.SUDIN_ENTITY) {
        orderList = await OrderService.getAvailableSudin(sudinKabKotaId);
      } else if (logisticrole === ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY) {
        orderList = await OrderService.getAvailableProvinces(provinceId);
      }

      const cekOrder = await Order.findOne({
        where: {
          logisticRole: logisticrole,
          upkIdOwner: upkId,
          isRegular: true,
          createdAt: {
            [Op.between]: [
              moment()
                .startOf("day")
                .valueOf(),
              moment()
                .endOf("day")
                .valueOf()
            ]
          }
        },
        order: [["updated_at", "DESC"]]
      });
      if (cekOrder) {
        LoggerServive.error(`Order dari UPK ID: ${upkId} Sudah terbuat!`);
        return "Sudah terbuat";
      }
      const lastOrder = await Order.findOne({
        order: [["updated_at", "DESC"]]
      });
      const order = await Order.create({
        orderCode: generateInvoiceNumber(lastOrder),
        lastOrderStatus: ENUM.LAST_ORDER_STATUS.DRAFT,
        lastApprovalStatus: ENUM.LAST_APPROVAL_STATUS.DRAFT,
        orderNumber: lastOrder
          ? Number(lastOrder.orderCode.split("/")[2]) + 1
          : 1,
        isRegular: true,
        logisticRole: logisticrole,

        upkIdOwner: upkId,
        sudinKabKotaIdOwner: sudinKabKotaId,
        provinceIdOwner: provinceId,

        upkIdFor: orderList && orderList[0] ? orderList[0].upkid : null,
        sudinKabKotaIdFor:
          orderList && orderList[0] ? orderList[0].sudinkotakabid : null,
        provinceIdFor:
          orderList && orderList[0] ? orderList[0].provinceid : null
      }).then(async order => {
        for (i in ordersItems) {
          const medicine = await Medicine.findByPk(ordersItems[i].medicineId);
          await OrderItem.create({
            medicineId: ordersItems[i].medicineId,
            orderId: order.id,
            packageUnitType: medicine.packageUnitType,
            packageQuantity: Math.floor(
              ordersItems[i].stockQty / medicine.packageMultiplier
            ),
            stockUnit: medicine.stockUnitType,
            stockQty: ordersItems[i].stockQty,
            expiredDate: ordersItems[i].expiredDate
          });
        }
        return order;
      });
      console.log("created success for order:", order);
    } catch (err) {
      console.log("Error create order: ", err);
    } finally {
    }
  };
  // More email related jobs
};
