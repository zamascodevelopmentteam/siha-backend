const _ = require("lodash");
const moment = require("moment");
const fs = require("fs");
const fsPromises = require("fs").promises;
const path = require("path");

const Setting = require("../models/Setting");

let settings = {};
const varService = () => {
  const transformEnum = (ENUM, arrValueIncuded) => {
    return arrValueIncuded
      ? Object.values(ENUM).concat(arrValueIncuded)
      : Object.values(ENUM);
  };

  const loadSetting = async () => {
    const settingsFromDB = await Setting.findAll({
      where: {
        isActive: true
      }
    });
    if (settingsFromDB && settingsFromDB.length > 0) {
      settingsFromDB.forEach((value, idx) => {
        settings[value.name] = value.meta;
      });

      if (!settings.MAIN) {
        throw {
          message: "No config found!"
        };
      }
    } else {
      throw {
        message: "No config found!"
      };
    }
    await initiateSetting();
  };

  const initiateSetting = async () => {
    // set momentJS to locale ID
    moment.locale("id");
    // init required directories
    let arrDirs = [
      path.join(
        settings.SITE_SETTING.DOWNLOAD_DIR,
        settings.SITE_SETTING.TMP_DIR
      ),
      path.join(settings.SITE_SETTING.UPLOAD_DIR, settings.SITE_SETTING.TMP_DIR)
    ];
    arrDirs.forEach(item => {
      if (!fs.existsSync(item)) {
        fsPromises.mkdir(item, { recursive: true });
      }
    });
  };

  const getMainSetting = () => {
    return settings;
  };

  const getTmpDownloadPath = () => {
    return path.join(
      settings.SITE_SETTING.DOWNLOAD_DIR,
      settings.SITE_SETTING.TMP_DIR
    );
  };

  return {
    transformEnum,
    loadSetting,
    SETTINGS: settings,
    getMainSetting,
    getTmpDownloadPath
  };
};

module.exports = varService;
