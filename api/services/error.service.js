const _ = require("lodash");

const errorService = () => {
  const errorHandler = (err, req, res, next) => {
    console.log("Error: ", err);
    if (err && err.name === "SequelizeUniqueConstraintError") {
      res.status(err.status || 500).json({
        message: err.message || "something_broke",
        data: _.map(err.errors, item =>
          _.pick(item, ["message", "path", "type"])
        )
      });
    } else if (err && err.name === "SequelizeValidationError") {
      res.status(err.status || 500).json({
        message: err.message || "something_broke",
        data: err
      });
    } else if (err && err.status) {
      res.status(err.status).json(
        Object.assign(
          {
            message: err.message,
            data: err.data
          },
          err.meta
        )
      );
    } else {
      res.status(500).json({
        message: err.message || "something_broke",
        data: err
      });
    }
  };

  const throwFormEntity = (message, data) => {
    return {
      status: 422,
      message,
      name: data.name,
      data
    };
  };

  const throwRecordNotFound = message => {
    return {
      status: 400,
      message: message ? message : "record_not_found",
      data: {}
    };
  };

  return {
    errorHandler,
    throwFormEntity,
    throwRecordNotFound
  };
};

module.exports = errorService;
