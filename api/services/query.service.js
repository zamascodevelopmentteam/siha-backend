const sequelize = require("../../config/database");

const moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const User = require("../models/User");
const Upk = require("../models/Upk");
const SudinKabKota = require("../models/SudinKabKota");
const Province = require("../models/Province");
const CentralMinistry = require("../models/CentralMinistry");

const LoggerServive = require("./logger.service")();
const ENUM = require("../../config/enum");

const queryService = () => {
  const filterPatientsParamsBuilder = q => {
    const keywordStr = q.toLowerCase();
    let queryParams = {
      [Op.or]: [
        {
          nik: {
            [Op.startsWith]: keywordStr
          }
        },
        {
          name: {
            [Op.iLike]: "%" + keywordStr + "%"
          }
        }
      ]
    };
    return queryParams;
  };

  const filterVisitsParamsBuilder = q => {
    const keywordStr = q.toLowerCase();
    let queryParams = {
      [Op.or]: [
        {
          nik: {
            [Op.startsWith]: keywordStr
          }
        },
        {
          name: {
            [Op.iLike]: "%" + keywordStr + "%"
          }
        }
      ]
    };
    return queryParams;
  };

  // monthYear (YYYY-MM)
  const filterOneMonth = (field, monthYear) => {
    const startDay = 1;
    const endDay = 31;

    const currentMonth = new Date(monthYear).getMonth();
    const currentYear = new Date(monthYear).getFullYear();

    const startDate = moment()
      .date(startDay)
      .month(currentMonth)
      .subtract(1, "months")
      .format("YYYY-MM-DD");
    const endDate = moment()
      .date(endDay)
      .month(currentYear)
      .format("YYYY-MM-DD");

    let queryParams = {
      [field]: { [Op.between]: [startDate, endDate] }
    };
    return queryParams;
  };

  // startMonthYear && endMonthYear (YYYY-MM)
  const queryPeriodeByMonth = (field, startMonthYear, endMonthYear) => {
    const startDay = 1;
    const endDay = 31;

    const startMonth = new Date(startMonthYear).getMonth();
    const startYear = new Date(startMonthYear).getFullYear();

    const endMonth = new Date(endMonthYear).getMonth();
    const endYear = new Date(endMonthYear).getFullYear();

    const startDate = moment()
      .date(startDay)
      .month(startMonth)
      .year(startYear)
      .format("YYYY-MM-DD");
    const endDate = moment()
      .date(endDay)
      .month(endMonth)
      .year(endYear)
      .format("YYYY-MM-DD");

    let queryParams =
      field + " BETWEEN '" + startDate + "' AND '" + endDate + "' ";
    return queryParams;
  };

  const includeCreated = () => {
    let queryParams = [
      {
        model: User,
        required: false,
        as: "createdByData",
        attributes: [
          ["name", "createdName"],
          "upkId",
          "sudinKabKotaId",
          "provinceId"
        ]
      },
      {
        model: User,
        required: false,
        as: "updatedByData",
        attributes: [
          ["name", "updatedName"],
          "upkId",
          "sudinKabKotaId",
          "provinceId"
        ]
      }
    ];
    return queryParams;
  };

  const generateOrderCode = async params => {
    const {
      isReguler,
      upkId,
      sudinKabKotaId,
      provinceId,
      logisticRole
    } = params;
    let currentEntitas = [];

    switch (logisticRole) {
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        currentEntitas = await Upk.findByPk(upkId);
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        currentEntitas = await SudinKabKota.findByPk(sudinKabKotaId);
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        currentEntitas = await Province.findByPk(provinceId);
        break;
    }
    if (!currentEntitas) {
      return;
    }

    let orderCode = "";
    let orderOrd = currentEntitas.orderOrd;
    let orderOrdReg = currentEntitas.orderOrdReg;
    if (isReguler) {
      let typeOrder = "SMO";
      let codeEntitas = currentEntitas.codeId;
      orderOrdReg = String(
        currentEntitas.orderOrdReg ? currentEntitas.orderOrdReg + 1 : 1
      );
      orderCode =
        typeOrder +
        "/" +
        codeEntitas +
        "/" +
        moment().format("MM") +
        "/" +
        moment().format("YYYY") +
        "/" +
        String(orderOrdReg.padStart(3, "0"));
    } else {
      let typeOrder = "PK";
      let codeEntitas = currentEntitas.codeId;
      orderOrd = String(
        currentEntitas.orderOrd ? currentEntitas.orderOrd + 1 : 1
      );
      orderCode =
        typeOrder +
        "/" +
        codeEntitas +
        "/" +
        moment().format("MM") +
        "/" +
        moment().format("YYYY") +
        "/" +
        String(orderOrd.padStart(3, "0"));
    }
    switch (logisticRole) {
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        await Upk.update(
          {
            orderOrd: orderOrd,
            orderOrdReg: orderOrdReg
          },
          {
            where: {
              id: upkId
            }
          }
        );
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        await SudinKabKota.update(
          {
            orderOrd: orderOrd,
            orderOrdReg: orderOrdReg
          },
          {
            where: {
              id: sudinKabKotaId
            }
          }
        );
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        await Province.update(
          {
            orderOrd: orderOrd,
            orderOrdReg: orderOrdReg
          },
          {
            where: {
              id: provinceId
            }
          }
        );
        break;
    }

    return orderCode;
  };

  const generateDroppingNumber = async params => {
    const {
      isReguler,
      isDistKhusus,
      upkId,
      sudinKabKotaId,
      provinceId,
      logisticRole
    } = params;
    let currentEntitas = {};
    const centralMinistryId = 1;

    switch (logisticRole) {
      case ENUM.LOGISTIC_ROLE.LAB_ENTITY:
        currentEntitas = await Upk.findByPk(upkId);
        break;
      case ENUM.LOGISTIC_ROLE.PHARMA_ENTITY:
        currentEntitas = await Upk.findByPk(upkId);
        break;
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        currentEntitas = await Upk.findByPk(upkId);
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        currentEntitas = await SudinKabKota.findByPk(sudinKabKotaId);
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        currentEntitas = await Province.findByPk(provinceId);
        break;
      case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
        currentEntitas = await CentralMinistry.findByPk(centralMinistryId);
        break;
    }
    if (!currentEntitas) {
      return;
    }

    let droppingNumber = "";
    let distOrd = currentEntitas.distOrd;
    let distOrdReg = currentEntitas.distOrdReg;
    let distOrdRegKhusus = currentEntitas.distOrdRegKhusus;
    if (isDistKhusus) {
      let typeOrder = "DO-PDO";
      let codeEntitas = currentEntitas.codeId;
      distOrdRegKhusus = String(
        currentEntitas.distOrdRegKhusus
          ? currentEntitas.distOrdRegKhusus + 1
          : 1
      );
      droppingNumber =
        typeOrder +
        "/" +
        codeEntitas +
        "/" +
        moment().format("MM") +
        "/" +
        moment().format("YYYY") +
        "/" +
        String(distOrdRegKhusus.padStart(3, "0"));
    } else if (isReguler) {
      let typeOrder = "DO-SMO";
      let codeEntitas = currentEntitas.codeId;
      distOrdReg = String(
        currentEntitas.distOrdReg ? currentEntitas.distOrdReg + 1 : 1
      );
      droppingNumber =
        typeOrder +
        "/" +
        codeEntitas +
        "/" +
        moment().format("MM") +
        "/" +
        moment().format("YYYY") +
        "/" +
        String(distOrdReg.padStart(3, "0"));
    } else {
      let typeOrder = "DO-PK";
      let codeEntitas = currentEntitas.codeId;
      distOrd = String(currentEntitas.distOrd ? currentEntitas.distOrd + 1 : 1);
      droppingNumber =
        typeOrder +
        "/" +
        codeEntitas +
        "/" +
        moment().format("MM") +
        "/" +
        moment().format("YYYY") +
        "/" +
        String(distOrd.padStart(3, "0"));
    }
    switch (logisticRole) {
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        Upk.update(
          {
            distOrd: distOrd,
            distOrdReg: distOrdReg,
            distOrdRegKhusus: distOrdRegKhusus
          },
          {
            where: {
              id: upkId
            }
          }
        );
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        SudinKabKota.update(
          {
            distOrd: distOrd,
            distOrdReg: distOrdReg,
            distOrdRegKhusus: distOrdRegKhusus
          },
          {
            where: {
              id: sudinKabKotaId
            }
          }
        );
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        Province.update(
          {
            distOrd: distOrd,
            distOrdReg: distOrdReg,
            distOrdRegKhusus: distOrdRegKhusus
          },
          {
            where: {
              id: provinceId
            }
          }
        );
        break;
      case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
        CentralMinistry.update(
          {
            distOrd: distOrd,
            distOrdReg: distOrdReg,
            distOrdRegKhusus: distOrdRegKhusus
          },
          {
            where: {
              id: centralMinistryId
            }
          }
        );
        break;
    }

    return droppingNumber;
  };

  const createTransaction = async () => {
    const t = await sequelize.transaction();

    return t;
  };

  return {
    filterPatientsParamsBuilder,
    filterVisitsParamsBuilder,
    filterOneMonth,
    includeCreated,
    generateOrderCode,
    generateDroppingNumber,
    queryPeriodeByMonth,
    createTransaction
  };
};

module.exports = queryService;
