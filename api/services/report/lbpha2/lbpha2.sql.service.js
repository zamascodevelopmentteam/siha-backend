const moment = require("moment");
const _ = require("lodash");

const ENUM = require("../../../../config/enum");
const VarServive = require("../../variable.service")();
const LoggerServive = require("../../logger.service")();

const lbpha2Service = () => {
  const generateLbpha2SQLInventoriesARV = (month, year, entityParam) => {
    const monthFilter = month ? month : new Date().getMonth();
    const yearFilter = year ? year : new Date().getFullYear();

    const startOfMonthDate = moment()
      .year(yearFilter)
      .month(monthFilter)
      .startOf("month")
      .format();

    const endOfMonthDate = moment()
      .year(yearFilter)
      .month(monthFilter)
      .endOf("month")
      .format();

    let isMinistryStaff =
      entityParam.logisticRole === ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY &&
      !entityParam.columnEntityName;

    LoggerServive.info(
      `Querying LBPHA 2 as ${entityParam.staffRole} with filter Logistic Role: ${entityParam.logisticRole} `
    );

    let stokKeluarEntity = `
  		SELECT
  			prescriptions.medicine_id AS id,
  			sum(prescriptions.amount) AS stock_qty_col_c,
  			patient_visits.${entityParam.columnEntityName}
  		FROM (((
  					SELECT
  						visits.id AS visit_id,
  						visits.patient_id,
  						visits.${entityParam.columnEntityName}
  					FROM (visits
  						JOIN (
  							SELECT
  								patients_1.id
  							FROM
  								patients patients_1) patients ON ((patients.id = visits.patient_id)))) patient_visits
  					JOIN (
  						SELECT
  							treatments_1.id,
  							treatments_1.visit_id,
  							treatments_1.prescription_id,
  							treatments_1.is_on_transit
  						FROM
  							treatments treatments_1
  						WHERE ((treatments_1.prescription_id IS NOT NULL)
  							AND(date_part('month'::text, treatments_1.tgl_pemberian_obat) = '${monthFilter +
                  1}')
  							AND(date_part('year'::text, treatments_1.tgl_pemberian_obat) = '${yearFilter}'))) treatments ON ((patient_visits.visit_id = treatments.visit_id)))
  				JOIN (
  					SELECT
  						prescriptions_1.id, prescription_medicines.amount, prescription_medicines.medicine_id
  					FROM (prescriptions prescriptions_1
  						JOIN prescription_medicines ON ((prescriptions_1.id = prescription_medicines.prescription_id)))
  				WHERE (prescriptions_1.is_draft IS NOT TRUE)) prescriptions ON ((prescriptions.id = treatments.prescription_id)))
  	GROUP BY
  		prescriptions.medicine_id,
  		patient_visits.${entityParam.columnEntityName}`;

    if (
      entityParam &&
      [
        ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY,
        ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
        ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
        ENUM.LOGISTIC_ROLE.UPK_ENTITY
      ].includes(entityParam.logisticRole)
    ) {
      stokKeluarEntity =
        `SELECT
  			distribution_plans_items.medicine_id AS id,
  			sum(distribution_plans_items.package_quantity * brands.package_multiplier) AS stock_qty_col_c,
  			sum(distribution_plans_items.package_quantity) AS package_qty_col_c
        ` +
        (!isMinistryStaff
          ? `,distribution_plans.${entityParam.columnEntityName}_sender AS ${entityParam.columnEntityName}`
          : ``) +
        `
  		FROM
  			distribution_plans
  			JOIN distribution_plans_items ON distribution_plans.id = distribution_plans_items.distribution_plan_id
  			JOIN brands ON brands.id = distribution_plans_items.brand_id
  		WHERE (distribution_plans.logistic_role_sender = '${entityParam.logisticRole}')
  		AND(distribution_plans.approval_status = '${ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL}')
  		AND(distribution_plans_items.brand_id IS NOT NULL)
      ` +
        (!isMinistryStaff
          ? `AND(distribution_plans.${entityParam.columnEntityName}_sender IS NOT NULL)`
          : ``) +
        `
  		AND(distribution_plans.created_at >= '${startOfMonthDate}')
  		AND(distribution_plans.created_at <= '${endOfMonthDate}')
  	GROUP BY
  		distribution_plans_items.medicine_id
      ` +
        (!isMinistryStaff
          ? `	,distribution_plans.${entityParam.columnEntityName}_sender`
          : ``);
    }

    let str =
      `SELECT
	` +
      (!isMinistryStaff
        ? `lbpha2_regular_usage.${entityParam.columnEntityName},`
        : ``) +
      `lbpha2_regular_usage.medicine_id AS medicine_id,
	lbpha2_regular_usage.code_name,
	lbpha2_regular_usage.name,
	(COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer AS stok_awal,
	(COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer AS stok_diterima,
	(COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer AS stok_keluar,
	(COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer AS stok_kadaluarsa,
	((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * lbpha2_regular_usage.package_multiplier) AS stok_selisih_fisik,
	((((COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer + (COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer) - ((COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer + (COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer)) + ((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * lbpha2_regular_usage.package_multiplier)) AS stok_akhir,
	(COALESCE(table_column_g.package_qty_col_g, (0)::bigint))::integer AS stok_akhir_botol,
	(COALESCE(table_inv_on_hand.stock_qty_on_hand, (0)::bigint))::integer AS stock_on_hand,
	(COALESCE(table_inv_on_hand.package_qty_on_hand, (0)::bigint))::integer AS package_on_hand,
	(COALESCE(lbpha2_regular_usage.stok_keluar_reguler, (0)::bigint))::integer AS stok_keluar_reguler,
	(COALESCE(lbpha2_regular_usage.paket_keluar_reguler, (0)::bigint))::integer AS paket_keluar_reguler,
	lbpha2_regular_usage.package_multiplier,
	lbpha2_regular_usage.package_unit_type,
	lbpha2_regular_usage.stock_unit_type,
	table_inv_on_hand.inv_string_ed_on_hand
FROM
(SELECT
  ` +
      (!isMinistryStaff ? `${entityParam.columnEntityName},` : ``) +
      `
  medicine_id,
  code_name,
  lbpha2_regular_usage.name,
  lbpha2_regular_usage.package_multiplier,
  lbpha2_regular_usage.package_unit_type,
  lbpha2_regular_usage.stock_unit_type,
  sum(stok_keluar_reguler) stok_keluar_reguler,
  sum(paket_keluar_reguler) paket_keluar_reguler
FROM
  lbpha2_regular_usage
GROUP BY
  medicine_id,
    ` +
      (!isMinistryStaff ? `${entityParam.columnEntityName},` : ``) +
      `
  code_name,
  lbpha2_regular_usage.name,
  lbpha2_regular_usage.package_multiplier,
  lbpha2_regular_usage.package_unit_type,
  lbpha2_regular_usage.stock_unit_type) AS lbpha2_regular_usage
	LEFT JOIN (
		SELECT
			inventory_logs.medicine_id AS id,
			sum(inventory_logs.after_stock_qty) AS stock_qty_col_a,
			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_a

        ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
		FROM
			inventory_logs
		WHERE ((inventory_logs.medicine_id IS NOT NULL)
    ` +
      (!isMinistryStaff
        ? `AND(inventory_logs.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `AND(inventory_logs.logistic_role = '${entityParam.logisticRole}')
			AND(inventory_logs.id IN(
					SELECT
						max(inventory_logs_1.id) AS max FROM inventory_logs inventory_logs_1
					WHERE (inventory_logs_1.activity_date <= '${startOfMonthDate}')
		GROUP BY
			inventory_logs_1.inventory_id)))
	GROUP BY
		inventory_logs.medicine_id
    ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
  ) table_column_a ON (((lbpha2_regular_usage.medicine_id = table_column_a.id)
          ` +
      (!isMinistryStaff
        ? `AND(table_column_a.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `
        ))
	LEFT JOIN (
		SELECT
			inventory_logs.medicine_id AS id,
			sum(inventory_logs.after_stock_qty) AS stock_qty_col_b,
			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_b

      ` +
      (!isMinistryStaff
        ? `	,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
		FROM
			inventory_logs
		WHERE (((inventory_logs.activity)::text = 'INSERT'::text)
			AND(inventory_logs.medicine_id IS NOT NULL)


      ` +
      (!isMinistryStaff
        ? `AND(inventory_logs.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `

			AND(inventory_logs.activity_date >= '${startOfMonthDate}')
			AND(inventory_logs.activity_date <= '${endOfMonthDate}')
      AND(inventory_logs.logistic_role = '${entityParam.logisticRole}'))
	GROUP BY
		inventory_logs.medicine_id
    ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
		) table_column_b ON (((lbpha2_regular_usage.medicine_id = table_column_b.id)
    ` +
      (!isMinistryStaff
        ? `AND(table_column_b.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `
			))
	LEFT JOIN (${stokKeluarEntity}) table_column_c ON (((lbpha2_regular_usage.medicine_id = table_column_c.id)
  ` +
      (!isMinistryStaff
        ? `AND(table_column_c.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `
			))
	LEFT JOIN (
		SELECT
			inventories.medicine_id AS id,
			sum(inventories.stock_qty) AS stock_qty_col_d,
			sum(inventories.package_quantity) AS package_qty_col_d

      ` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `
		FROM
			inventories
		WHERE ((date_part('month'::text, inventories.expired_date) = '${monthFilter +
      1}')
			AND(date_part('year'::text, inventories.expired_date) = '${yearFilter}')

      ` +
      (!isMinistryStaff
        ? `AND(inventories.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `
      AND(inventories.logistic_role = '${entityParam.logisticRole}'))
	GROUP BY
		inventories.medicine_id
    ` +
      (!isMinistryStaff ? `	,inventories.${entityParam.columnEntityName}` : ``) +
      `
	) table_column_d ON (((lbpha2_regular_usage.medicine_id = table_column_d.id)
  ` +
      (!isMinistryStaff
        ? `AND(table_column_d.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `
			))
	LEFT JOIN (
		SELECT
			inventories.medicine_id AS id,
			sum(inventory_adjustment.package_quantity) AS package_qty_col_e
			` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `
		FROM (inventories
			JOIN inventory_adjustment ON ((inventory_adjustment.inventory_id = inventories.id)))
	WHERE ((date_part('month'::text, inventory_adjustment.created_at) = '${monthFilter +
    1}')
		AND(date_part('year'::text, inventory_adjustment.created_at) ='${yearFilter}')
    ` +
      (!isMinistryStaff
        ? `AND(inventories.${entityParam.columnEntityName} IS NOT NULL) `
        : ``) +
      `
          AND( inventories.logistic_role = '${entityParam.logisticRole}' )
  )
GROUP BY
	inventories.medicine_id
  ` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `

) table_column_e ON (((lbpha2_regular_usage.medicine_id = table_column_e.id)
` +
      (!isMinistryStaff
        ? `AND(table_column_e.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `
    ))
	LEFT JOIN (
		SELECT
			inventory_logs.medicine_id AS id,
			sum(inventory_logs.after_stock_qty) AS stock_qty_col_g,
			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_g
      ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
		FROM
			inventory_logs
		WHERE ((inventory_logs.medicine_id IS NOT NULL)

      ` +
      (!isMinistryStaff
        ? `AND(inventory_logs.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `
			AND(inventory_logs.id IN(
					SELECT
						max(inventory_logs.id) AS max FROM inventory_logs
					GROUP BY
						inventory_logs.inventory_id))
			AND(inventory_logs.activity_date <= '${endOfMonthDate}')
			AND(inventory_logs.prev_expired_date > '${endOfMonthDate}')
      AND( logistic_role = '${entityParam.logisticRole}' )
    )
	GROUP BY
		inventory_logs.medicine_id
    ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
  ) table_column_g ON (((lbpha2_regular_usage.medicine_id = table_column_g.id)
  ` +
      (!isMinistryStaff
        ? `AND(table_column_g.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `

    ))
	LEFT JOIN (
		SELECT
			inventories.medicine_id AS id,
			sum(inventories.stock_qty) AS stock_qty_on_hand,
			sum(inventories.package_quantity) AS package_qty_on_hand,
      ` +
      (!isMinistryStaff ? `inventories.${entityParam.columnEntityName},` : ``) +
      `
			string_agg(to_char(inventories.expired_date, 'DD/MM/YYYY'::text), ','::text) AS inv_string_ed_on_hand
		FROM
			inventories
		WHERE ( logistic_role = '${entityParam.logisticRole}' ) AND (expired_date > '${endOfMonthDate}')
	GROUP BY
		inventories.medicine_id
    ` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `
  ) table_inv_on_hand ON (((lbpha2_regular_usage.medicine_id = table_inv_on_hand.id)
  ` +
      (!isMinistryStaff
        ? `AND(table_inv_on_hand.${entityParam.columnEntityName} = lbpha2_regular_usage.${entityParam.columnEntityName})`
        : ``) +
      `

    ))
ORDER BY
	lbpha2_regular_usage.medicine_id
  ` +
      (!isMinistryStaff
        ? `,lbpha2_regular_usage.${entityParam.columnEntityName}`
        : ``);
    // console.log(str);
    return str;
  };
  const generateLbpha2SQLInventoriesNonARV = (month, year, entityParam) => {
    const monthFilter = month ? month : new Date().getMonth();
    const yearFilter = year ? year : new Date().getFullYear();

    const startOfMonthDate = moment()
      .year(yearFilter)
      .month(monthFilter)
      .startOf("month")
      .format();

    const endOfMonthDate = moment()
      .year(yearFilter)
      .month(monthFilter)
      .endOf("month")
      .format();

    let isMinistryStaff =
      entityParam.logisticRole === ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY &&
      !entityParam.columnEntityName;

    LoggerServive.info(
      `Querying LBPHA 2 brands as ${entityParam.staffRole} with filter Logistic Role: ${entityParam.logisticRole} `
    );

    let stokKeluarEntity =
      `SELECT
        distribution_plans_items.brand_id AS id,
        sum(distribution_plans_items.package_quantity * brands.package_multiplier) AS stock_qty_col_c,
        sum(distribution_plans_items.package_quantity) AS package_qty_col_c
        ` +
      (!isMinistryStaff
        ? `,distribution_plans.${entityParam.columnEntityName}_sender AS ${entityParam.columnEntityName}`
        : ``) +
      `
      FROM
        distribution_plans
        JOIN distribution_plans_items ON distribution_plans.id = distribution_plans_items.distribution_plan_id
        JOIN brands ON brands.id = distribution_plans_items.brand_id
      WHERE (distribution_plans.logistic_role_sender = '${entityParam.logisticRole}')
      AND(distribution_plans.approval_status = '${ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL}')
      AND(distribution_plans_items.brand_id IS NOT NULL)
      ` +
      (!isMinistryStaff
        ? `AND(distribution_plans.${entityParam.columnEntityName}_sender IS NOT NULL)`
        : ``) +
      `
      AND(distribution_plans.created_at >= '${startOfMonthDate}')
      AND(distribution_plans.created_at <= '${endOfMonthDate}')
    GROUP BY
      distribution_plans_items.brand_id
      ` +
      (!isMinistryStaff
        ? `	,distribution_plans.${entityParam.columnEntityName}_sender`
        : ``);

    if (
      entityParam &&
      [
        ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY,
        ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
        ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
        ENUM.LOGISTIC_ROLE.UPK_ENTITY
      ].includes(entityParam.logisticRole)
    ) {
      stokKeluarEntity =
        `SELECT
    			distribution_plans_items.brand_id AS id,
    			sum(distribution_plans_items.package_quantity * brands.package_multiplier) AS stock_qty_col_c,
    			sum(distribution_plans_items.package_quantity) AS package_qty_col_c
          ` +
        (!isMinistryStaff
          ? `,distribution_plans.${entityParam.columnEntityName}_sender AS ${entityParam.columnEntityName}`
          : ``) +
        `
    		FROM
    			distribution_plans
    			JOIN distribution_plans_items ON distribution_plans.id = distribution_plans_items.distribution_plan_id
    			JOIN brands ON brands.id = distribution_plans_items.brand_id
    		WHERE (distribution_plans.logistic_role_sender = '${entityParam.logisticRole}')
    		AND(distribution_plans.approval_status = '${ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL}')
    		AND(distribution_plans_items.brand_id IS NOT NULL)
        ` +
        (!isMinistryStaff
          ? `AND(distribution_plans.${entityParam.columnEntityName}_sender IS NOT NULL)`
          : ``) +
        `
    		AND(distribution_plans.created_at >= '${startOfMonthDate}')
    		AND(distribution_plans.created_at <= '${endOfMonthDate}')
    	GROUP BY
    		distribution_plans_items.brand_id
        ` +
        (!isMinistryStaff
          ? `	,distribution_plans.${entityParam.columnEntityName}_sender`
          : ``);
    }

    let str =
      `SELECT
  	` +
      (!isMinistryStaff ? `upk_brands.${entityParam.columnEntityName},` : ``) +
      `upk_brands.brand_id AS brand_id,
  	upk_brands.code_name,
  	upk_brands.name,
  	(COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer AS stok_awal,
  	(COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer AS stok_diterima,
  	(COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer AS stok_keluar,
  	(COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer AS stok_kadaluarsa,
  	((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * upk_brands.package_multiplier) AS stok_selisih_fisik,
  	((((COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer + (COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer) - ((COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer + (COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer)) + ((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * upk_brands.package_multiplier)) AS stok_akhir,
  	(COALESCE(table_column_g.package_qty_col_g, (0)::bigint))::integer AS stok_akhir_botol,
  	(COALESCE(table_inv_on_hand.stock_qty_on_hand, (0)::bigint))::integer AS stock_on_hand,
  	(COALESCE(table_inv_on_hand.package_qty_on_hand, (0)::bigint))::integer AS package_on_hand,
  	upk_brands.package_multiplier,
  	upk_brands.package_unit_type,
  	upk_brands.stock_unit_type,
  	table_inv_on_hand.inv_string_ed_on_hand
  FROM
      (
      SELECT
      ` +
      (!isMinistryStaff ? `${entityParam.columnEntityName},` : ``) +
      `
        brands.id AS brand_id,
        brands.medicine_id,
        code_name,
        brands.name,
        brands.package_multiplier,
        brands.package_unit_type,
        brands.stock_unit_type
      FROM (
        SELECT
          brands.*,
          medicines.code_name,
          medicines.stock_unit_type
        FROM
          brands
          JOIN medicines ON brands.medicine_id = medicines.id) brands,
        (SELECT upk.id AS upk_id, sudin_kab_kota_id, province_id FROM upk) AS upk
      GROUP BY
        brand_id,
        medicine_id,
        ` +
      (!isMinistryStaff ? `${entityParam.columnEntityName},` : ``) +
      `
        code_name,
        brands.name,
        brands.package_multiplier,
        brands.package_unit_type,
        brands.stock_unit_type) AS upk_brands
  	LEFT JOIN (
  		SELECT
  			inventory_logs.brand_id AS id,
  			sum(inventory_logs.after_stock_qty) AS stock_qty_col_a,
  			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_a

          ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
  		FROM
  			inventory_logs
  		WHERE ((inventory_logs.brand_id IS NOT NULL)
      ` +
      (!isMinistryStaff
        ? `AND(inventory_logs.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `AND(inventory_logs.logistic_role = '${entityParam.logisticRole}')
  			AND(inventory_logs.id IN(
  					SELECT
  						max(inventory_logs_1.id) AS max FROM inventory_logs inventory_logs_1
  					WHERE (inventory_logs_1.activity_date <= '${startOfMonthDate}')
  		GROUP BY
  			inventory_logs_1.inventory_id)))
  	GROUP BY
  		inventory_logs.brand_id
      ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
    ) table_column_a ON (((upk_brands.brand_id = table_column_a.id)
            ` +
      (!isMinistryStaff
        ? `AND(table_column_a.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `
          ))
  	LEFT JOIN (
  		SELECT
  			inventory_logs.brand_id AS id,
  			sum(inventory_logs.after_stock_qty) AS stock_qty_col_b,
  			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_b

        ` +
      (!isMinistryStaff
        ? `	,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
  		FROM
  			inventory_logs
  		WHERE (((inventory_logs.activity)::text = 'INSERT'::text)
  			AND(inventory_logs.brand_id IS NOT NULL)


        ` +
      (!isMinistryStaff
        ? `AND(inventory_logs.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `

  			AND(inventory_logs.activity_date >= '${startOfMonthDate}')
  			AND(inventory_logs.activity_date <= '${endOfMonthDate}')
        AND(inventory_logs.logistic_role = '${entityParam.logisticRole}'))
  	GROUP BY
  		inventory_logs.brand_id
      ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
  		) table_column_b ON (((upk_brands.brand_id = table_column_b.id)
      ` +
      (!isMinistryStaff
        ? `AND(table_column_b.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `
  			))
  	LEFT JOIN (${stokKeluarEntity}) table_column_c ON (((upk_brands.brand_id = table_column_c.id)
    ` +
      (!isMinistryStaff
        ? `AND(table_column_c.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `
  			))
  	LEFT JOIN (
  		SELECT
  			inventories.brand_id AS id,
  			sum(inventories.stock_qty) AS stock_qty_col_d,
  			sum(inventories.package_quantity) AS package_qty_col_d

        ` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `
  		FROM
  			inventories
  		WHERE ((date_part('month'::text, inventories.expired_date) = '${monthFilter +
        1}')
  			AND(date_part('year'::text, inventories.expired_date) = '${yearFilter}')

        ` +
      (!isMinistryStaff
        ? `AND(inventories.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `
        AND(inventories.logistic_role = '${entityParam.logisticRole}'))
  	GROUP BY
  		inventories.brand_id
      ` +
      (!isMinistryStaff ? `	,inventories.${entityParam.columnEntityName}` : ``) +
      `
  	) table_column_d ON (((upk_brands.brand_id = table_column_d.id)
    ` +
      (!isMinistryStaff
        ? `AND(table_column_d.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `
  			))
  	LEFT JOIN (
  		SELECT
  			inventories.brand_id AS id,
  			sum(inventory_adjustment.package_quantity) AS package_qty_col_e
  			` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `
  		FROM (inventories
  			JOIN inventory_adjustment ON ((inventory_adjustment.inventory_id = inventories.id)))
  	WHERE ((date_part('month'::text, inventory_adjustment.created_at) = '${monthFilter +
      1}')
  		AND(date_part('year'::text, inventory_adjustment.created_at) ='${yearFilter}')
      ` +
      (!isMinistryStaff
        ? `AND(inventories.${entityParam.columnEntityName} IS NOT NULL) `
        : ``) +
      `
            AND( inventories.logistic_role = '${entityParam.logisticRole}' )
    )
  GROUP BY
  	inventories.brand_id
    ` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `

  ) table_column_e ON (((upk_brands.brand_id = table_column_e.id)
  ` +
      (!isMinistryStaff
        ? `AND(table_column_e.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `
      ))
  	LEFT JOIN (
  		SELECT
  			inventory_logs.brand_id AS id,
  			sum(inventory_logs.after_stock_qty) AS stock_qty_col_g,
  			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_g
        ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
  		FROM
  			inventory_logs
  		WHERE ((inventory_logs.brand_id IS NOT NULL)

        ` +
      (!isMinistryStaff
        ? `AND(inventory_logs.${entityParam.columnEntityName} IS NOT NULL)`
        : ``) +
      `
  			AND(inventory_logs.id IN(
  					SELECT
  						max(inventory_logs.id) AS max FROM inventory_logs
  					GROUP BY
  						inventory_logs.inventory_id))
  			AND(inventory_logs.activity_date <= '${endOfMonthDate}')
  			AND(inventory_logs.prev_expired_date > '${endOfMonthDate}')
        AND( logistic_role = '${entityParam.logisticRole}' )
      )
  	GROUP BY
  		inventory_logs.brand_id
      ` +
      (!isMinistryStaff
        ? `,inventory_logs.${entityParam.columnEntityName}`
        : ``) +
      `
    ) table_column_g ON (((upk_brands.brand_id = table_column_g.id)
    ` +
      (!isMinistryStaff
        ? `AND(table_column_g.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `

      ))
  	LEFT JOIN (
  		SELECT
  			inventories.brand_id AS id,
  			sum(inventories.stock_qty) AS stock_qty_on_hand,
  			sum(inventories.package_quantity) AS package_qty_on_hand,
        ` +
      (!isMinistryStaff ? `inventories.${entityParam.columnEntityName},` : ``) +
      `
  			string_agg(to_char(inventories.expired_date, 'DD/MM/YYYY'::text), ','::text) AS inv_string_ed_on_hand
  		FROM
  			inventories
  		WHERE ( logistic_role = '${entityParam.logisticRole}' ) AND (expired_date > '${endOfMonthDate}')
  	GROUP BY
  		inventories.brand_id
      ` +
      (!isMinistryStaff ? `,inventories.${entityParam.columnEntityName}` : ``) +
      `
    ) table_inv_on_hand ON (((upk_brands.brand_id = table_inv_on_hand.id)
    ` +
      (!isMinistryStaff
        ? `AND(table_inv_on_hand.${entityParam.columnEntityName} = upk_brands.${entityParam.columnEntityName})`
        : ``) +
      `

      ))
  ORDER BY
  	upk_brands.brand_id
    ` +
      (!isMinistryStaff ? `,upk_brands.${entityParam.columnEntityName}` : ``);

    return str;
  };

  return {
    generateLbpha2SQLInventoriesARV,
    generateLbpha2SQLInventoriesNonARV
  };
};

module.exports = lbpha2Service;
