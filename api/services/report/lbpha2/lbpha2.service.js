const sequelize = require("../../../../config/database");
const moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const ENUM = require("../../../../config/enum");
const VarServive = require("../../variable.service")();
const LoggerServive = require("../../logger.service")();
const Lbpha2SQLService = require("./lbpha2.sql.service")();

const lbpha2Service = () => {
  const regimentQueryBuilder = (
    visitPatientWhereParam,
    treatmentWhereParam
  ) => {
    visitPatientWhereParam = visitPatientWhereParam
      ? "WHERE " + visitPatientWhereParam
      : "";
    treatmentWhereParam = treatmentWhereParam
      ? "AND " + treatmentWhereParam
      : "";
    let str =
      `
          SELECT regiments.id, regiments.name, regiments.combination_str_info, total FROM regiments
            JOIN (
          			SELECT count(patient_visits.id)::int total, prescriptions.regiment_id as regiment_id
                FROM (
          					SELECT visits.id, visits.patient_id as patient_id FROM
                      (select visits.*,upk.sudin_kab_kota_id as sudin_kab_kota_id,upk.province_id as province_id from visits join upk ON upk.id = visits.upk_id ) visits
          							JOIN
          							(
          								SELECT  id, (DATE_PART('year', NOW()) - DATE_PART('year', date_birth)) AS age, date_birth, weight, height
          								FROM patients
          								WHERE date_birth IS NOT NULL
          							)
          							AS patients ON patients.id = visits.patient_id
          							JOIN sudin_kab_kota ON sudin_kab_kota.id = visits.sudin_kab_kota_id
          							JOIN provinces ON provinces.id = sudin_kab_kota.province_id ` +
      visitPatientWhereParam +
      `
          				 )
          				AS patient_visits
          				JOIN (SELECT id, visit_id, prescription_id, is_on_transit, jml_hr_obat_arv_1 FROM treatments where prescription_id IS NOT NULL
          ` +
      treatmentWhereParam +
      `
          					) AS treatments ON patient_visits.id = treatments.visit_id
          				JOIN (SELECT id, regiment_id FROM prescriptions where regiment_id IS NOT NULL AND prescriptions.is_draft IS NOT TRUE)  AS prescriptions ON prescriptions.id = treatments.prescription_id
          				GROUP BY regiment_id
          		) as visits ON visits.regiment_id = regiments.id`;

    return str;
  };

  const FDCQueryBuilder = (
    FDCMedicineId,
    visitPatientWhereParam,
    visitTreatmentWhereString
  ) => {
    visitPatientWhereParam = visitPatientWhereParam
      ? "WHERE " + visitPatientWhereParam
      : "";
    visitTreatmentWhereString = visitTreatmentWhereString
      ? "AND " + visitTreatmentWhereString
      : "";

    let str =
      `     SELECT count(patient_visits.id)::int total, weight
                  FROM (
            					SELECT visits.id, visits.patient_id as patient_id, weight FROM (select visits.*,upk.sudin_kab_kota_id as sudin_kab_kota_id,upk.province_id as province_id from visits join upk ON upk.id = visits.upk_id ) visits
            							JOIN
            							(
            								SELECT  id, weight
            								FROM patients
            								WHERE weight IS NOT NULL
            							)
            							AS patients ON patients.id = visits.patient_id
            							JOIN upk ON upk.id = visits.upk_id
            							JOIN sudin_kab_kota ON sudin_kab_kota.id = upk.sudin_kab_kota_id
            							JOIN provinces ON provinces.id = sudin_kab_kota.province_id ` +
      visitPatientWhereParam +
      `
            				 )
            				AS patient_visits
            				JOIN (SELECT id, visit_id, prescription_id, is_on_transit FROM treatments where prescription_id IS NOT NULL
            ` +
      visitTreatmentWhereString +
      `
            					) AS treatments ON patient_visits.id = treatments.visit_id
            				JOIN (SELECT prescriptions.id FROM prescriptions JOIN prescription_medicines ON prescriptions.id = prescription_medicines.prescription_id where prescriptions.is_draft IS NOT TRUE AND prescription_medicines.medicine_id = ` +
      FDCMedicineId +
      `)  AS prescriptions ON prescriptions.id = treatments.prescription_id
            				GROUP BY weight`;

    return str;
  };

  const inventoryQueryBuilderARV = (
    month,
    year,
    staff,
    filterParams,
    inventoryWhereString
  ) => {
    inventoryWhereString = inventoryWhereString
      ? "WHERE " + inventoryWhereString
      : "";

    let logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
    let columnEntityName = filterParams.columnEntityName;

    switch (staff.logisticrole) {
      case ENUM.LOGISTIC_ROLE.LAB_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.PHARMA_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        }
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else if (filterParams && filterParams.sudinKabKotaId) {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        }
        break;
      case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else if (filterParams && filterParams.sudinKabKotaId) {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        } else if (filterParams && filterParams.provinceId) {
          logisticRole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY;
        }
        break;
    }
    let lbpha2_table = Lbpha2SQLService.generateLbpha2SQLInventoriesARV(
      month,
      year,
      { logisticRole: logisticRole, columnEntityName, staffRole: staff.role }
    );

    let str =
      `
      SELECT * FROM (${lbpha2_table}) AS lbpha2_table
      ` +
      inventoryWhereString +
      `
      ORDER BY
      	lbpha2_table.medicine_id
        `;
    LoggerServive.info(
      `inventoryWhereString: ${inventoryWhereString} - ${filterParams.columnEntityValue}  `
    );

    return str;
  };

  const inventoryQueryBuilderNonARV = (
    month,
    year,
    staff,
    filterParams,
    inventoryWhereString
  ) => {
    inventoryWhereString = inventoryWhereString
      ? "WHERE " + inventoryWhereString
      : "";

    let logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
    let columnEntityName = filterParams.columnEntityName;

    switch (staff.logisticrole) {
      case ENUM.LOGISTIC_ROLE.LAB_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.PHARMA_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        }
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else if (filterParams && filterParams.sudinKabKotaId) {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        }
        break;
      case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else if (filterParams && filterParams.sudinKabKotaId) {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        } else if (filterParams && filterParams.provinceId) {
          logisticRole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY;
        }
        break;
    }
    let lbpha2_table = Lbpha2SQLService.generateLbpha2SQLInventoriesNonARV(
      month,
      year,
      { logisticRole: logisticRole, columnEntityName, staffRole: staff.role }
    );

    let str =
      `
      SELECT * FROM (${lbpha2_table}) AS lbpha2_table
      ` +
      inventoryWhereString +
      `
      ORDER BY
      	lbpha2_table.brand_id
        `;
    LoggerServive.info(
      `inventoryWhereString: ${inventoryWhereString} - ${filterParams.columnEntityValue}  `
    );

    return str;
  };

  const generatePermintaanRegulerUPKQueryBuilder = (upkId = "") => {
    let lbpha2_table = Lbpha2SQLService.generateLbpha2SQLInventoriesARV(
      moment()
        .local()
        .month(),
      moment()
        .local()
        .year(),
      {
        logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
        columnEntityName: "upk_id",
        staffRole: "CRONJOB"
      }
    );

    const whereEntity = upkId ? ` AND upk.id = ${upkId} ` : ``;

    let str = `
      SELECT lbpha2_table.*,
        upk.sudin_kab_kota_id,
        upk.province_id,
        FLOOR(stock_on_hand / GREATEST(lbpha2_table.stok_keluar_reguler, 1)) AS total_ketersediaan_bulan,
        upk.order_multiplier AS order_multiplier,
        upk.name AS upk_name
      FROM (${lbpha2_table}) AS lbpha2_table
        JOIN upk ON upk.id = lbpha2_table.upk_id
      WHERE
        lbpha2_table.stok_keluar > 0
        AND lbpha2_table.stock_on_hand < upk.order_multiplier * GREATEST(lbpha2_table.stok_keluar_reguler, 0) ${whereEntity}
      ORDER BY
      	lbpha2_table.medicine_id
        `;
    // console.log("str: ", str);
    return str;
  };

  const generatePermintaanRegulerSudinQueryBuilder = (sudinKabKotaId = "") => {
    let lbpha2_table = Lbpha2SQLService.generateLbpha2SQLInventoriesARV(
      moment()
        .local()
        .month(),
      moment()
        .local()
        .year(),
      {
        logisticRole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
        columnEntityName: "sudin_kab_kota_id",
        staffRole: "CRONJOB"
      }
    );

    const whereEntity = sudinKabKotaId
      ? ` AND sudin_kab_kota.id = ${sudinKabKotaId} `
      : ``;

    let str = `
      SELECT lbpha2_table.*,
        sudin_kab_kota.id AS sudin_kab_kota_id,
        sudin_kab_kota.province_id,
        FLOOR(stock_on_hand / GREATEST(lbpha2_table.stok_keluar_reguler, 1)) AS total_ketersediaan_bulan,
        sudin_kab_kota.order_multiplier AS order_multiplier,
        sudin_kab_kota.name AS sudin_name
      FROM (${lbpha2_table}) AS lbpha2_table
        JOIN sudin_kab_kota ON sudin_kab_kota.id = lbpha2_table.sudin_kab_kota_id
      WHERE
        lbpha2_table.stock_on_hand < sudin_kab_kota.order_multiplier * GREATEST(lbpha2_table.stok_keluar_reguler, 0) ${whereEntity}
      ORDER BY
      	lbpha2_table.medicine_id
        `;
    return str;
  };

  const generatePermintaanRegulerProvinceQueryBuilder = (provinceId = "") => {
    let lbpha2_table = Lbpha2SQLService.generateLbpha2SQLInventoriesARV(
      moment()
        .local()
        .month(),
      moment()
        .local()
        .year(),
      {
        logisticRole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
        columnEntityName: "province_id",
        staffRole: "CRONJOB"
      }
    );

    const whereEntity = provinceId ? ` AND provinces.id = ${provinceId} ` : ``;

    let str = `
      SELECT lbpha2_table.*,
        provinces.id AS province_id,
        FLOOR(stock_on_hand / GREATEST(lbpha2_table.stok_keluar_reguler, 1)) AS total_ketersediaan_bulan,
        provinces.order_multiplier AS order_multiplier,
        provinces.name AS province_name
      FROM (${lbpha2_table}) AS lbpha2_table
        JOIN provinces ON provinces.id = lbpha2_table.province_id
      WHERE
        lbpha2_table.stock_on_hand < provinces.order_multiplier * GREATEST(lbpha2_table.stok_keluar_reguler, 0) ${whereEntity}
      ORDER BY
      	lbpha2_table.medicine_id
        `;
    return str;
  };

  return {
    regimentQueryBuilder,
    FDCQueryBuilder,
    inventoryQueryBuilderARV,
    inventoryQueryBuilderNonARV,
    generatePermintaanRegulerUPKQueryBuilder,
    generatePermintaanRegulerSudinQueryBuilder,
    generatePermintaanRegulerProvinceQueryBuilder
  };
};

module.exports = lbpha2Service;
