const _ = require("lodash");
const moment = require("moment");

const sequelize = require("../../../../config/database");
const ENUM = require("../../../../config/enum");

const Regiment = require("../../../models/Regiment");
const Medicine = require("../../../models/Medicine");
const Brand = require("../../../models/Brand");
const Inventory = require("../../../models/Inventory");
const RegimentMedicine = require("../../../models/RegimentMedicine");
const Lbpha2 = require("../../../models/Lbpha2");
const User = require("../../../models/User");
const Province = require("../../../models/Province");
const SudinKabKota = require("../../../models/SudinKabKota");
const Upk = require("../../../models/Upk");

const VariableService = require("../../variable.service")();
const Lbpha2Service = require("./lbpha2.service")();

const ReportClass = require("../report.class");

let topHeader = {
  pasienReguler: {
    dewasa: 0,
    anak: 0
  },
  pasienTransit: {
    dewasa: 0,
    anak: 0
  },
  pasienMultiMonth: {
    dewasa: 0,
    anak: 0
  }
};

let topHeaderFDC = {
  pasienReguler: {
    anak: 0,
    tablet_per_hari: 0
  },
  pasienTransit: {
    anak: 0,
    tablet_per_hari: 0
  },
  pasienMultiMonth: {
    anak: 0,
    tablet_per_hari: 0
  }
};

let topHeaderInventory = {
  column_A: 0,
  column_B: 0,
  column_C: 0,
  column_D: 0,
  column_E: 0,
  column_F: 0,
  column_G: 0,
  column_H: "",
  column_I: 0,
  paket_keluar_reguler: 0,
  stok_keluar_reguler: 0,
  package_on_hand: 0,
  stock_on_hand: 0
};

module.exports = class Lbpha2Class extends ReportClass {
  constructor(staff, req, month, year) {
    super(staff, req, month, year);
    super.setReportTitle("Laporan LBPHA 2");
    this.lbpha2 = super.getDefaultReport();

    this.rejimenStandarTable = {
      content: {},
      footer: {}
    };
    this.rejimenLainTable = {
      content: {},
      footer: {}
    };
    this.FDCTable = {
      content: {},
      footer: {}
    };
    // new
    this.FDCABCTable = {
      content: {},
      footer: {}
    };
    this.FDCLPVTable = {
      content: {},
      footer: {}
    };
    // end new
    this.inventoryTable = {
      content: {},
      footer: {}
    };

    this.filter = super.getDefaultFilter();

    this.initiateVariable();
  }

  initiateVariable() {
    this.constructRejimenStandarTable();
    this.constructRejimenLainTable();
    this.constructFDCTable();
    this.constructInventoryTable();
  }

  setFilter(monthIndex, yearIndex) {
    this.filter.month = monthIndex;
    this.filter.year = yearIndex;
  }

  generateEntityRole(tableName, preCondition, postCondition) {
    let str =
      (tableName ? tableName + "." : "") +
      (this.entityBaseId ? this.entityBaseId : "");
    if (!this.entityBaseId) {
      str = "";
    } else {
      str =
        (preCondition || "") +
        " " +
        str +
        " = :entityBaseValue " +
        (postCondition || "");
    }
    return str;
  }

  async buildAllReport() {
    await this.buildReportRejimen();
    await this.buildReportFDCUsage();
    await this.buildReportInventoryUsage();
  }

  //construct object
  constructRejimenStandarTable() {
    this.rejimenStandarTable.content = {
      lini1: {
        title: ENUM.LBPHA2.LINI1,
        regimentList: []
      },
      lini1Pediatrik: {
        title: ENUM.LBPHA2.LINI1_PEDIATRIK,
        regimentList: []
      },
      lini2: {
        title: ENUM.LBPHA2.LINI2,
        regimentList: []
      },
      lini2Pediatrik: {
        title: ENUM.LBPHA2.LINI2_PEDIATRIK,
        regimentList: []
      },
      profilaksis: {
        title: ENUM.LBPHA2.PROFILAKSIS,
        regimentList: []
      }
    };
    this.rejimenStandarTable.footer = {
      totalPasienRejimenStandar: _.cloneDeep(topHeader),
      totalPasien: _.cloneDeep(topHeader)
    };
  }
  constructRejimenLainTable() {
    this.rejimenLainTable.content = {
      title: "Rejimen Lain",
      regimentList: []
    };
    this.rejimenLainTable.footer = {
      totalPasienRejimenLain: _.cloneDeep(topHeader)
    };
  }
  constructFDCTable() {
    this.FDCTable.content = [
      {
        title: "3 - 5.9",
        weightMin: 3,
        weightMax: 6,
        dayMultiplier: 2,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "6 - 9.9",
        weightMin: 6,
        weightMax: 10,
        dayMultiplier: 3,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "10 - 13.9",
        weightMin: 10,
        weightMax: 14,
        dayMultiplier: 4,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "14 - 19.9",
        weightMin: 14,
        weightMax: 20,
        dayMultiplier: 5,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "20 - 24.9",
        weightMin: 20,
        weightMax: 25,
        dayMultiplier: 6,
        data: _.cloneDeep(topHeaderFDC)
      }
    ];
    for (let i = 0; i < this.FDCTable.content.length; i++) {
      this.FDCTable.content[
        i
      ].data.pasienReguler.tablet_per_hari = this.FDCTable.content[
        i
      ].dayMultiplier;
      this.FDCTable.content[
        i
      ].data.pasienTransit.tablet_per_hari = this.FDCTable.content[
        i
      ].dayMultiplier;
      this.FDCTable.content[
        i
      ].data.pasienMultiMonth.tablet_per_hari = this.FDCTable.content[
        i
      ].dayMultiplier;
    }
    this.FDCTable.footer = {
      jumlahTabDiperlukan: {
        pasienReguler: {
          anak: 0,
          jumlahTab: 0
        },
        pasienTransit: {
          anak: 0,
          jumlahTab: 0
        },
        pasienMultiMonth: {
          anak: 0,
          jumlahTab: 0
        }
      },
      jumlahTabDiperlukanBuffer: {
        pasienReguler: {
          anak: 0,
          jumlahTab: 0
        },
        pasienTransit: {
          anak: 0,
          jumlahTab: 0
        },
        pasienMultiMonth: {
          anak: 0,
          jumlahTab: 0
        }
      }
    };

    // FDCABC
    this.FDCABCTable.content = [
      {
        title: "3 - 5.9",
        weightMin: 3,
        weightMax: 6,
        dayMultiplier: 1,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "6 - 9.9",
        weightMin: 6,
        weightMax: 10,
        dayMultiplier: 1.5,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "10 - 13.9",
        weightMin: 10,
        weightMax: 14,
        dayMultiplier: 2,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "14 - 19.9",
        weightMin: 14,
        weightMax: 20,
        dayMultiplier: 2.5,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "20 - 24.9",
        weightMin: 20,
        weightMax: 25,
        dayMultiplier: 3,
        data: _.cloneDeep(topHeaderFDC)
      }
    ];
    for (let i = 0; i < this.FDCABCTable.content.length; i++) {
      this.FDCABCTable.content[
        i
      ].data.pasienReguler.tablet_per_hari = this.FDCABCTable.content[
        i
      ].dayMultiplier;
      this.FDCABCTable.content[
        i
      ].data.pasienTransit.tablet_per_hari = this.FDCABCTable.content[
        i
      ].dayMultiplier;
      this.FDCABCTable.content[
        i
      ].data.pasienMultiMonth.tablet_per_hari = this.FDCABCTable.content[
        i
      ].dayMultiplier;
    }
    this.FDCABCTable.footer = {
      jumlahTabDiperlukan: {
        pasienReguler: {
          anak: 0,
          jumlahTab: 0
        },
        pasienTransit: {
          anak: 0,
          jumlahTab: 0
        },
        pasienMultiMonth: {
          anak: 0,
          jumlahTab: 0
        }
      },
      jumlahTabDiperlukanBuffer: {
        pasienReguler: {
          anak: 0,
          jumlahTab: 0
        },
        pasienTransit: {
          anak: 0,
          jumlahTab: 0
        },
        pasienMultiMonth: {
          anak: 0,
          jumlahTab: 0
        }
      }
    };

    // FDCLPV
    this.FDCLPVTable.content = [
      {
        title: "3 - 5.9",
        weightMin: 3,
        weightMax: 6,
        dayMultiplier: 4,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "6 - 9.9",
        weightMin: 6,
        weightMax: 10,
        dayMultiplier: 6,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "10 - 13.9",
        weightMin: 10,
        weightMax: 14,
        dayMultiplier: 8,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "14 - 19.9",
        weightMin: 14,
        weightMax: 20,
        dayMultiplier: 10,
        data: _.cloneDeep(topHeaderFDC)
      },
      {
        title: "20 - 24.9",
        weightMin: 20,
        weightMax: 25,
        dayMultiplier: 12,
        data: _.cloneDeep(topHeaderFDC)
      }
    ];
    for (let i = 0; i < this.FDCLPVTable.content.length; i++) {
      this.FDCLPVTable.content[
        i
      ].data.pasienReguler.tablet_per_hari = this.FDCLPVTable.content[
        i
      ].dayMultiplier;
      this.FDCLPVTable.content[
        i
      ].data.pasienTransit.tablet_per_hari = this.FDCLPVTable.content[
        i
      ].dayMultiplier;
      this.FDCLPVTable.content[
        i
      ].data.pasienMultiMonth.tablet_per_hari = this.FDCLPVTable.content[
        i
      ].dayMultiplier;
    }
    this.FDCLPVTable.footer = {
      jumlahTabDiperlukan: {
        pasienReguler: {
          anak: 0,
          jumlahTab: 0
        },
        pasienTransit: {
          anak: 0,
          jumlahTab: 0
        },
        pasienMultiMonth: {
          anak: 0,
          jumlahTab: 0
        }
      },
      jumlahTabDiperlukanBuffer: {
        pasienReguler: {
          anak: 0,
          jumlahTab: 0
        },
        pasienTransit: {
          anak: 0,
          jumlahTab: 0
        },
        pasienMultiMonth: {
          anak: 0,
          jumlahTab: 0
        }
      }
    };
  }
  constructInventoryTable() {
    this.inventoryTable.content = {};
    this.inventoryTable.footer = {};
  }
  //end of construct

  getFilterDewasaAnak() {
    return {
      strFilterDewasa:
        " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
        this.generateEntityRole("visits", "AND") +
        " AND (patients.age > 14 OR (patients.age <=14 AND patients.weight > 25) ) ",
      strFilterAnak:
        " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
        this.generateEntityRole("visits", "AND") +
        " AND patients.age <= 14 AND  patients.weight <= 25 "
    };
  }
  getFilterInventoryColumn() {
    return {
      column_A:
        " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
        this.generateEntityRole("visits", "AND") +
        " AND (patients.age > 14 OR (patients.age <=14 AND patients.weight > 25) ) ",
      strFilterAnak:
        " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
        this.generateEntityRole("visits", "AND") +
        " AND patients.age <= 14 AND  patients.weight <= 25 "
    };
  }

  getQueryOptionFDC_Regiment() {
    return {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        month: this.filter.month + 1,
        year: this.filter.year,
        entityBaseValue: this.entityBaseValue
      }
      // logging: console.log
    };
  }

  getQueryRuleFDC_Regiment() {
    return {
      reguler: " is_on_transit IS NOT TRUE ",
      inTransit: " is_on_transit = TRUE ",
      multiMonth: " jml_hr_obat_arv_1 > 30 AND is_on_transit IS NOT TRUE "
    };
  }

  async saveAllReportToDB(date, upkId, sudinKabKotaId, provinceId) {
    const now = moment(date || new Date());

    let lbpha2 = await Lbpha2.findOne({
      where: {
        tanggalLaporan: now.format()
      }
    });
    let roleParam = {};
    switch (this.staff.role) {
      case ENUM.USER_ROLE.RR_STAFF:
        roleParam.upkId = upkId || this.staff.upkId;
      case ENUM.USER_ROLE.PHARMA_STAFF:
        roleParam.upkId = upkId || this.staff.upkId;
        break;
      case ENUM.USER_ROLE.LAB_STAFF:
        roleParam.upkId = upkId || this.staff.upkId;
        break;
      case ENUM.USER_ROLE.SUDIN_STAFF:
        roleParam.sudinKabKotaId = sudinKabKotaId || this.staff.sudinKabKotaId;
        break;
      case ENUM.USER_ROLE.PROVINCE_STAFF:
        roleParam.provinceId = provinceId || this.staff.provinceId;
        break;
      case ENUM.USER_ROLE.MINISTRY_STAFF:
        break;
      default:
        break;
    }

    let tableParam = {
      tabelRejimenStandar: this.lbpha2.rejimenStandarTable,
      tabelRejimenLain: this.lbpha2.rejimenLainTable,
      tabelFdcJunior: this.lbpha2.FDCTable,
      tabelStok: this.lbpha2.inventoryTable
    };

    if (lbpha2) {
      Object.assign(lbpha2, roleParam);
      Object.assign(lbpha2, tableParam);
      lbpha2.updatedBy = this.staff.id;
      await lbpha2.save();
    } else {
      Object.assign(tableParam, roleParam);
      tableParam.tanggalLaporan = now.format();
      tableParam.createdBy = this.staff.id;
      lbpha2 = await Lbpha2.create(tableParam);
    }
    console.log("Lbpha2 saved!");
  }

  async generateSheetJSFile() {
    const sheetJS = super.createSheetJSInstance();
    super.assignMetaReportSheetJS();
    // add sheet cell value here
    // sheetJS.setCellValue("A1", "Laporan Bulanan");
    // end of adding cell value
    super.generateSheetJSFile();
    console.log("Sheet File Generated!");
  }

  //Regiment Ops
  async buildReportRejimen() {
    await super.initiateMetaReport();
    await this.intiateRejimenList();
    await this.loadRejimenReportData();
    await this.constructRejimenList();

    this.lbpha2.rejimenStandarTable = this.rejimenStandarTable;
    this.lbpha2.rejimenLainTable = this.rejimenLainTable;
    this.lbpha2.profilaksisTable = this.profilaksisTable;
  }

  async intiateRejimenList() {
    const regimentList = await Regiment.findAll({
      where: {},
      order: [["name", "DESC"], ["id", "ASC"]],
      attributes: ["id", "name", "combinationIds", "combinationStrInfo", "createdBy", "createdAt"],
      include: [
        {
          model: User,
          as: "createdByData",
          include: [
            {
              model: Province,
              as: 'province'
            },
            {
              model: SudinKabKota,
              as: 'sudinKabKota'
            },
            {
              model: Upk,
              as: 'upk'
            },
          ],
        }
      ],
    });

    this.regimentReportList = [];
    if (regimentList && regimentList.length > 0) {
      for (let i = 0; i < regimentList.length; i++) {
        const regiment = regimentList[i].toJSON();
        let regimentReport = {
          regiment: regiment,
          data: _.cloneDeep(topHeader)
        };

        this.regimentReportList.push(regimentReport);
      }
    }
  }

  incrementRejimenFooter(targetObj, sourceObj) {
    targetObj.pasienReguler.dewasa += sourceObj.pasienReguler.dewasa;
    targetObj.pasienReguler.anak += sourceObj.pasienReguler.anak;
    targetObj.pasienTransit.dewasa += sourceObj.pasienTransit.dewasa;
    targetObj.pasienTransit.anak += sourceObj.pasienTransit.anak;
    targetObj.pasienMultiMonth.dewasa += sourceObj.pasienMultiMonth.dewasa;
    targetObj.pasienMultiMonth.anak += sourceObj.pasienMultiMonth.anak;
  }

  async constructRejimenList() {
    if (this.regimentReportList && this.regimentReportList.length > 0) {
      for (let i = 0; i < this.regimentReportList.length; i++) {
        const dataItem = this.regimentReportList[i];
        const regiment = this.regimentReportList[i].regiment;
        const data = this.regimentReportList[i].data;
        if (regiment.name === ENUM.LBPHA2.LINI1) {
          this.rejimenStandarTable.content.lini1.regimentList.push(dataItem);
          await this.incrementRejimenFooter(
            this.rejimenStandarTable.footer.totalPasienRejimenStandar,
            data
          );
        } else if (regiment.name === ENUM.LBPHA2.LINI1_PEDIATRIK) {
          this.rejimenStandarTable.content.lini1Pediatrik.regimentList.push(
            dataItem
          );
          this.incrementRejimenFooter(
            this.rejimenStandarTable.footer.totalPasienRejimenStandar,
            data
          );
        } else if (regiment.name === ENUM.LBPHA2.LINI2) {
          this.rejimenStandarTable.content.lini2.regimentList.push(dataItem);
          this.incrementRejimenFooter(
            this.rejimenStandarTable.footer.totalPasienRejimenStandar,
            data
          );
        } else if (regiment.name === ENUM.LBPHA2.LINI2_PEDIATRIK) {
          this.rejimenStandarTable.content.lini2Pediatrik.regimentList.push(
            dataItem
          );
          this.incrementRejimenFooter(
            this.rejimenStandarTable.footer.totalPasienRejimenStandar,
            data
          );
        } else if (regiment.name === ENUM.LBPHA2.PROFILAKSIS) {
          this.rejimenStandarTable.content.profilaksis.regimentList.push(
            dataItem
          );
          this.incrementRejimenFooter(
            this.rejimenStandarTable.footer.totalPasienRejimenStandar,
            data
          );
        } else {
          this.rejimenLainTable.content.regimentList.push(dataItem);
          this.incrementRejimenFooter(
            this.rejimenLainTable.footer.totalPasienRejimenLain,
            data
          );
        }
        this.incrementRejimenFooter(
          this.rejimenStandarTable.footer.totalPasien,
          data
        );
      }
    }
  }

  assignResultRejimenToContent(result, table, patientType, fieldName) {
    if (result.length > 0) {
      _.forEach(table, item => {
        const matched = _.find(result, {
          id: item.regiment.id
        });
        if (matched) {
          item.data[patientType][fieldName] = matched.total;
        }
      });
    }
  }

  async loadRejimenReportData() {
    let strFilterDewasa = this.getFilterDewasaAnak().strFilterDewasa;
    let strFilterAnak = this.getFilterDewasaAnak().strFilterAnak;
    let queryOpt = this.getQueryOptionFDC_Regiment();

    let pasienRegulerResultDewasa = await sequelize.query(
      Lbpha2Service.regimentQueryBuilder(
        strFilterDewasa,
        this.getQueryRuleFDC_Regiment().reguler
      ),
      queryOpt
    );
    this.assignResultRejimenToContent(
      pasienRegulerResultDewasa,
      this.regimentReportList,
      "pasienReguler",
      "dewasa"
    );

    const pasienRegulerResultAnak = await sequelize.query(
      Lbpha2Service.regimentQueryBuilder(
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().reguler
      ),
      queryOpt
    );
    this.assignResultRejimenToContent(
      pasienRegulerResultAnak,
      this.regimentReportList,
      "pasienReguler",
      "anak"
    );

    const pasienTransitResultDewasa = await sequelize.query(
      Lbpha2Service.regimentQueryBuilder(
        strFilterDewasa,
        this.getQueryRuleFDC_Regiment().inTransit
      ),
      queryOpt
    );

    this.assignResultRejimenToContent(
      pasienTransitResultDewasa,
      this.regimentReportList,
      "pasienTransit",
      "dewasa"
    );

    const pasienTransitResultAnak = await sequelize.query(
      Lbpha2Service.regimentQueryBuilder(
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().inTransit
      ),
      queryOpt
    );

    this.assignResultRejimenToContent(
      pasienTransitResultAnak,
      this.regimentReportList,
      "pasienTransit",
      "anak"
    );

    const pasienMultiMonthResultDewasa = await sequelize.query(
      Lbpha2Service.regimentQueryBuilder(
        strFilterDewasa,
        this.getQueryRuleFDC_Regiment().multiMonth
      ),
      queryOpt
    );
    this.assignResultRejimenToContent(
      pasienMultiMonthResultDewasa,
      this.regimentReportList,
      "pasienMultiMonth",
      "dewasa"
    );

    const pasienMultiMonthResultAnak = await sequelize.query(
      Lbpha2Service.regimentQueryBuilder(
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().multiMonth
      ),
      queryOpt
    );
    this.assignResultRejimenToContent(
      pasienMultiMonthResultAnak,
      this.regimentReportList,
      "pasienMultiMonth",
      "anak"
    );
  }

  async getRegimentList() {
    return this.regimentList;
  }
  //end of regiment ops

  //FDC Ops
  async buildReportFDCUsage() {
    await super.initiateMetaReport();
    await this.loadFDCReportData();

    this.lbpha2.FDCTable = this.FDCTable;
    this.lbpha2.FDCABCTable = this.FDCABCTable;
    this.lbpha2.FDCLPVTable = this.FDCLPVTable;
  }

  assignResultFDCToContent(result, table, patientType, fieldName) {
    // console.log(result, '..............');
    if (result.length > 0) {
      _.forEach(table.content, rowItem => {
        const matched = _.remove(result, item => {
          return (
            rowItem.weightMin <= item.weight && item.weight < rowItem.weightMax
          );
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            matchedItem.total = matchedItem.total;
            rowItem.data[patientType][fieldName] += matchedItem.total;
            table.footer.jumlahTabDiperlukan[patientType][fieldName] +=
              matchedItem.total;
            table.footer.jumlahTabDiperlukan[patientType].jumlahTab +=
              matchedItem.total * rowItem.dayMultiplier;
            table.footer.jumlahTabDiperlukanBuffer[patientType][fieldName] +=
              matchedItem.total;
            table.footer.jumlahTabDiperlukanBuffer[patientType].jumlahTab +=
              matchedItem.total * rowItem.dayMultiplier * 3;
          });
        }
      });
    }
  }

  async loadFDCReportData() {
    let FDCMedicineId = VariableService.SETTINGS.MAIN.LBPHA2.FDCMedicineId; // 14
    // let FDCMedicineId = 2;
    let strFilterAnak =
      " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
      this.generateEntityRole("visits", "AND") +
      " AND patients.weight < 25 ";

    let queryOpt = this.getQueryOptionFDC_Regiment();

    let pasienRegulerResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().reguler
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienRegulerResultAnak,
      this.FDCTable,
      "pasienReguler",
      "anak"
    );

    let pasienTransitResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().inTransit
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienTransitResultAnak,
      this.FDCTable,
      "pasienTransit",
      "anak"
    );

    let pasienMultiMonthResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().multiMonth
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienMultiMonthResultAnak,
      this.FDCTable,
      "pasienMultiMonth",
      "anak"
    );

    // FCDABC ---------------------------------------------------
    FDCMedicineId = 30249;
    strFilterAnak =
      " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
      this.generateEntityRole("visits", "AND") +
      " AND patients.weight < 25 ";

    queryOpt = this.getQueryOptionFDC_Regiment();

    pasienRegulerResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().reguler
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienRegulerResultAnak,
      this.FDCABCTable,
      "pasienReguler",
      "anak"
    );

    pasienTransitResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().inTransit
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienTransitResultAnak,
      this.FDCABCTable,
      "pasienTransit",
      "anak"
    );

    pasienMultiMonthResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().multiMonth
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienMultiMonthResultAnak,
      this.FDCABCTable,
      "pasienMultiMonth",
      "anak"
    );
    // end FCDABC

    // FCDLPV ---------------------------------------------------
    FDCMedicineId = 30248;
    strFilterAnak =
      " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
      this.generateEntityRole("visits", "AND") +
      " AND patients.weight < 25 ";

    queryOpt = this.getQueryOptionFDC_Regiment();

    pasienRegulerResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().reguler
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienRegulerResultAnak,
      this.FDCLPVTable,
      "pasienReguler",
      "anak"
    );

    pasienTransitResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().inTransit
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienTransitResultAnak,
      this.FDCLPVTable,
      "pasienTransit",
      "anak"
    );

    pasienMultiMonthResultAnak = await sequelize.query(
      Lbpha2Service.FDCQueryBuilder(
        FDCMedicineId,
        strFilterAnak,
        this.getQueryRuleFDC_Regiment().multiMonth
      ),
      queryOpt
    );
    this.assignResultFDCToContent(
      pasienMultiMonthResultAnak,
      this.FDCLPVTable,
      "pasienMultiMonth",
      "anak"
    );
    // end FCDLPV
  }
  //End of FDC

  // stok Ops
  async buildReportInventoryUsage() {
    await super.initiateMetaReport();
    await this.intiateBrandList();
    await this.loadBrandReportData();

    this.lbpha2.inventoryTable = this.inventoryTable;
  }

  async intiateBrandList() {
    const medicineList = await Medicine.findAll({
      where: {
        medicineType: "ARV"
      },
      attributes: [
        "id",
        "name",
        "codeName",
        "medicineType",
        "stockUnitType",
        "packageMultiplier"
      ],
      order: [["id", "ASC"]]
    });

    let medReportList = [];
    if (medicineList && medicineList.length > 0) {
      for (let i = 0; i < medicineList.length; i++) {
        const medicine = medicineList[i].toJSON();
        let itemReport = {
          medicine: medicine,
          data: _.cloneDeep(topHeaderInventory)
        };

        medReportList.push(itemReport);
      }
    }
    this.inventoryTable.content = medReportList;
  }

  assignResultStockToContent(result, table) {
    if (result.length > 0) {
      _.forEach(table.content, rowItem => {
        const matched = _.remove(result, item => {
          return rowItem.medicine.id == item.medicine_id;
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            rowItem.data.column_A += matchedItem.stok_awal;
            rowItem.data.column_B += matchedItem.stok_diterima;
            rowItem.data.column_C += matchedItem.stok_keluar;
            rowItem.data.column_D += matchedItem.stok_kadaluarsa;
            rowItem.data.column_E += matchedItem.stok_selisih_fisik;
            rowItem.data.column_F += matchedItem.stok_akhir;
            rowItem.data.column_G += matchedItem.stok_akhir_botol;
            rowItem.data.column_H += matchedItem.inv_string_ed_on_hand;
            rowItem.data.paket_keluar_reguler += matchedItem.paket_keluar_reguler;
            rowItem.data.stok_keluar_reguler += matchedItem.stok_keluar_reguler;
            rowItem.data.package_on_hand += matchedItem.package_on_hand;
            rowItem.data.stock_on_hand += matchedItem.stock_on_hand;
          });
        }
      });
    }
  }
  constructFinalDataInventory() {
    let orderMultiplier = 3;
    switch (this.staff.role) {
      case ENUM.USER_ROLE.RR_STAFF:
        orderMultiplier = this.report.meta.upk.orderMultiplier;
        break;
      case ENUM.USER_ROLE.PHARMA_STAFF:
        orderMultiplier = this.report.meta.upk.orderMultiplier;
        break;
      case ENUM.USER_ROLE.LAB_STAFF:
        orderMultiplier = this.report.meta.upk.orderMultiplier;
        break;
      case ENUM.USER_ROLE.SUDIN_STAFF:
        if (this.filter && this.filter.upkId) {
          orderMultiplier = this.report.meta.upk.orderMultiplier;
        } else {
          orderMultiplier = this.report.meta.sudinKabKota.orderMultiplier;
        }
        break;
      case ENUM.USER_ROLE.PROVINCE_STAFF:
        if (this.filter && this.filter.upkId) {
          orderMultiplier = this.report.meta.upk.orderMultiplier;
        } else if (this.filter && this.filter.sudinKabKotaId) {
          orderMultiplier = this.report.meta.sudinKabKota.orderMultiplier;
        } else {
          orderMultiplier = this.report.meta.province.orderMultiplier;
        }
        break;
      case ENUM.USER_ROLE.MINISTRY_STAFF:
        if (this.filter && this.filter.upkId) {
          orderMultiplier = this.report.meta.upk.orderMultiplier;
        } else if (this.filter && this.filter.sudinKabKotaId) {
          orderMultiplier = this.report.meta.sudinKabKota.orderMultiplier;
        } else if (this.filter && this.filter.provinceId) {
          orderMultiplier = this.report.meta.province.orderMultiplier;
        } else {
          orderMultiplier = 12;
        }
        break;
      default:
        orderMultiplier = 3;
    }
    _.forEach(this.inventoryTable.content, medicineItem => {
      const requiredStockPackage =
        medicineItem.data.paket_keluar_reguler * orderMultiplier -
        medicineItem.data.package_on_hand;

      // console.log(
      //   "orderMultiplier: ",
      //   requiredStockPackage,
      //   medicineItem.data.paket_keluar_reguler,
      //   orderMultiplier,
      //   medicineItem.data.package_on_hand,
      //   medicineItem.data.column_G
      // );
      if (requiredStockPackage > 0) {
        medicineItem.data.column_I = requiredStockPackage;
      }
      if (
        !medicineItem.data.column_H ||
        medicineItem.data.column_H === "null"
      ) {
        medicineItem.data.column_H = "-";
      }
      // delete medicineItem.data.paket_keluar_reguler;
      delete medicineItem.data.stok_keluar_reguler;
      delete medicineItem.data.package_on_hand;
      delete medicineItem.data.stock_on_hand;
    });
  }
  async loadBrandReportData() {
    const startOfMonthDate = moment(new Date())
      .year(this.filter.year)
      .month(this.filter.month)
      .startOf("month");
    const endOfMonthDate = moment(new Date())
      .year(this.filter.year)
      .month(this.filter.month)
      .endOf("month");

    // COLUMN ALL
    let queryOpt_column_A = this.getQueryOptionFDC_Regiment();
    // queryOpt_column_A.logging = console.log;
    let result = await sequelize.query(
      Lbpha2Service.inventoryQueryBuilderARV(
        this.filter.month,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        this.generateEntityRole()
      ),
      queryOpt_column_A
    );
    this.assignResultStockToContent(result, this.inventoryTable);
    this.constructFinalDataInventory();
  }
  // end of stok ops

  getReport() {
    delete this.lbpha2.content;
    delete this.lbpha2.footer;
    return this.lbpha2;
  }
};
