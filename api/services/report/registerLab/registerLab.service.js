const sequelize = require("../../../../config/database");
const moment = require("moment");
const Sequelize = require("sequelize");
const _ = require("lodash");
const Op = Sequelize.Op;

const ENUM = require("../../../../config/enum");
const VarServive = require("../../variable.service")();
const LoggerServive = require("../../logger.service")();
const Lbpha2SQLService = require("../lbpha2/lbpha2.sql.service")();

const service = () => {
  const lbadbDetailTesLabQueryBuilder = (
    testType,
    hasilTesColumn,
    useGroupByHasilTesColumn,
    month,
    year,
    staff,
    filterParams,
    queryWhereString
  ) => {
    let entityWhereQuery = filterParams.columnEntityName
      ? ` AND ${filterParams.columnEntityName} = ${filterParams.columnEntityValue} `
      : "";
    queryWhereString = queryWhereString ? ` AND ` + queryWhereString : "";

    let groupByArr = [];
    if (filterParams && filterParams.columnEntityName) {
      groupByArr.push(filterParams.columnEntityName);
    }
    if (useGroupByHasilTesColumn) {
      groupByArr.push(hasilTesColumn);
    }
    const groupByString =
      groupByArr.length > 0 ? " GROUP BY " + groupByArr.join(",") : "";

    let str = `SELECT
count(${testType}.id) AS jumlah
${useGroupByHasilTesColumn ? "," + hasilTesColumn + " as kesimpulan " : ""}
FROM
(
  SELECT
    visits.id AS visit_id,
    upk.id AS upk_id,
    sudin_kab_kota_id,
    province_id
  FROM
    upk
  JOIN visits ON upk.id = visits.upk_id
  WHERE DATE_PART('month', visits.visit_date) = ${month} AND DATE_PART('YEAR', visits.visit_date) = ${year}
  ) AS upk_visits
JOIN ${testType} ON upk_visits.visit_id = ${testType}.visit_id
WHERE
${hasilTesColumn} IS NOT NULL
${entityWhereQuery}
${queryWhereString}
${groupByString}
        `;

    return str;
  };

  const komoditasLabQueryBuilder = (
    month,
    year,
    staff,
    filterParams,
    queryWhereString
  ) => {
    let entityWhereQuery = filterParams.columnEntityName
      ? ` AND ${filterParams.columnEntityName} = ${filterParams.columnEntityValue} `
      : "";
    queryWhereString = queryWhereString ? ` AND ` + queryWhereString : "";

    let groupByArr = [];
    if (filterParams && filterParams.columnEntityName) {
      groupByArr.push(filterParams.columnEntityName);
    }
    groupByArr.push("brands.medicine_id");
    const groupByString =
      groupByArr.length > 0 ? " GROUP BY " + groupByArr.join(",") : "";

    let str = `
      SELECT
        brands.medicine_id,
        SUM ( inventory_usage.used_stock_unit ) :: INT jumlah,
        SUM (
        CASE
            
            WHEN test_hiv.hasil_test_r1 = 'INVALID' THEN
            test_hiv.qty_reagen_r1 ELSE 0 
          END +
        CASE
            
            WHEN test_hiv.hasil_test_r2 = 'INVALID' THEN
            test_hiv.qty_reagen_r2 ELSE 0 
            END +
        CASE
            
            WHEN test_hiv.hasil_test_r3 = 'INVALID' THEN
            test_hiv.qty_reagen_r3 ELSE 0 
          END 
            ) :: INT jumlah_invalid
        FROM
          inventory_usage
          JOIN brands ON brands.ID = inventory_usage.brand_id
          JOIN medicines ON medicines.ID = brands.medicine_id
          JOIN non_arv_medicines ON medicines.ID = non_arv_medicines.medicine_id
          JOIN ( SELECT ID, sudin_kab_kota_id, province_id FROM upk ) upk ON upk.ID = inventory_usage.upk_id
          LEFT JOIN test_hiv ON test_hiv.ID = inventory_usage.test_hiv_id 
        WHERE
          (
            non_arv_medicines.is_r1 = TRUE 
            OR non_arv_medicines.is_r2 = TRUE 
            OR non_arv_medicines.is_r3 = TRUE 
            OR non_arv_medicines.is_io_ims = TRUE 
            OR non_arv_medicines.is_vl = TRUE 
            OR non_arv_medicines.is_cd4 = TRUE 
            OR non_arv_medicines.is_machine = TRUE 
            OR non_arv_medicines.is_eid = TRUE 
          )
      ${entityWhereQuery}
      ${queryWhereString}
      ${groupByString}
    `;

    return str;
  };

  const hasilTesLabQueryBuilder = (
    testType,
    hasilTesColumn,
    useGroupByHasilTesColumn,
    month,
    year,
    staff,
    filterParams,
    queryWhereString,
    req
  ) => {
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2)
        month = '0' + month;
      if (day.length < 2)
        day = '0' + day;

      return [year, month, day].join('-');
    }

    let entityWhereQuery = filterParams.columnEntityName
      ? ` AND ${filterParams.columnEntityName} = ${filterParams.columnEntityValue} `
      : "";
    queryWhereString = queryWhereString ? ` AND ` + queryWhereString : "";

    let groupByArr = [];
    if (filterParams && filterParams.columnEntityName) {
      groupByArr.push(filterParams.columnEntityName);
    }
    if (useGroupByHasilTesColumn) {
      groupByArr.push(hasilTesColumn);
    }
    const groupByString =
      groupByArr.length > 0 ? " GROUP BY " + groupByArr.join(",") : "";

    let dateFilter = `DATE_PART('month', visits.visit_date) = ${month} AND DATE_PART('YEAR', visits.visit_date) = ${year}`;

    if (req) {
      const { isSixMonth } = req.query;
      if (isSixMonth) {
        let startDate = new Date(`${year}-${month + 1}-01`);
        var d = startDate.getDate();
        startDate.setMonth(startDate.getMonth() + +-5);
        if (startDate.getDate() != d) {
          startDate.setDate(0);
        }
        startDate = formatDate(startDate);
        let endDate = formatDate(`${year}-${month + 1}-31`);

        dateFilter = `visits.visit_date >= '${startDate}' AND visits.visit_date <= '${endDate}'`;
      }
    }

    let str = `SELECT
	count(${testType}.id) AS jumlah
	${useGroupByHasilTesColumn ? "," + hasilTesColumn + " as kesimpulan " : ""}
FROM
 (
		SELECT
			visits.id AS visit_id,
			upk.id AS upk_id,
			sudin_kab_kota_id,
			province_id
		FROM
			upk
    JOIN visits ON upk.id = visits.upk_id
    WHERE ${dateFilter} 
    ) AS upk_visits
	JOIN ${testType} ON upk_visits.visit_id = ${testType}.visit_id
WHERE
	${hasilTesColumn} IS NOT NULL
  ${entityWhereQuery}
  ${queryWhereString}
${groupByString}
          `;

    return str;
  };

  const inventoryQueryBuilderNonARV = (
    month,
    year,
    staff,
    filterParams,
    inventoryWhereString
  ) => {
    inventoryWhereString = inventoryWhereString
      ? "WHERE " + inventoryWhereString
      : "";

    let logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
    let columnEntityName = filterParams.columnEntityName;

    switch (staff.logisticrole) {
      case ENUM.LOGISTIC_ROLE.LAB_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.PHARMA_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
        break;
      case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        }
        break;
      case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else if (filterParams && filterParams.sudinKabKotaId) {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        }
        break;
      case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
        if (filterParams && filterParams.upkId) {
          logisticRole = ENUM.LOGISTIC_ROLE.UPK_ENTITY;
        } else if (filterParams && filterParams.sudinKabKotaId) {
          logisticRole = ENUM.LOGISTIC_ROLE.SUDIN_ENTITY;
        } else if (filterParams && filterParams.provinceId) {
          logisticRole = ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY;
        } else {
          logisticRole = ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY;
        }
        break;
    }
    let lbpha2_non_arv_table = Lbpha2SQLService.generateLbpha2SQLInventoriesNonARV(
      month,
      year,
      { logisticRole: logisticRole, columnEntityName, staffRole: staff.role }
    );

    let str =
      `
      SELECT * FROM (${lbpha2_non_arv_table}) AS lbpha2_non_arv_table
      ` +
      inventoryWhereString +
      `
      ORDER BY
      	lbpha2_non_arv_table.brand_id
        `;
    LoggerServive.info(
      `inventoryWhereString: ${inventoryWhereString} - ${filterParams.columnEntityValue}  `
    );

    return str;
  };

  return {
    lbadbDetailTesLabQueryBuilder,
    komoditasLabQueryBuilder,
    hasilTesLabQueryBuilder,
    inventoryQueryBuilderNonARV
  };
};

module.exports = service;
