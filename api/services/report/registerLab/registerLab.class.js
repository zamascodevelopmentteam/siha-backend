const _ = require("lodash");
const moment = require("moment");
const { Op, Sequelize } = require("sequelize");

const sequelize = require("../../../../config/database");
const ENUM = require("../../../../config/enum");

const Visit = require("../../../models/Visit");
const Upk = require("../../../models/Upk");
const TestHiv = require("../../../models/TestHiv");
const TestIms = require("../../../models/TestIms");
const TestVl = require("../../../models/TestVl");
const Medicine = require("../../../models/Medicine");
const NonArvMedicine = require("../../../models/NonArvMedicine");
const Brand = require("../../../models/Brand");
const Inventory = require("../../../models/Inventory");

const VariableService = require("../../variable.service")();
const Lbpha2Service = require("../lbpha2/lbpha2.service")();
const RegisterLabService = require("./registerLab.service")();
const InventoryService = require("../../inventory.service")();

const ReportClass = require("../report.class");
const Patient = require("../../../models/Patient");
const SudinKabKota = require("../../../models/SudinKabKota");
const Province = require("../../../models/Province");
const VisitImsLab = require("../../../models/VisitImsLab");
const UsageVlCd4 = require("../../../models/UsageVlCd4");
const UsageVlCd4Medicine = require("../../../models/UsageVlCd4Medicine");

const topHeaderInventory = {
  jenis_komoditas: "",
  tahun_pengadaan: "",
  merk_dagang: "",
  satuan: "",
  tgl_kadaluarsa: "",
  column_A: 0,
  column_B: 0,
  column_C: 0,
  column_D: 0,
  column_E: 0,
  column_F: 0,
  column_G: 0,
  column_H: 0,
  sumber_dana: "",
  package_on_hand: 0,
  stock_on_hand: 0
};

module.exports = class RegisterLabClass extends ReportClass {
  constructor(staff, req, month, year) {
    super(staff, req, month, year);
    this.req = req;

    super.setReportTitle("LAPORAN BULANAN ALAT DAN BAHAN (LBADB)");
    this.registerLab = super.getDefaultReport();

    this.filter = super.getDefaultFilter();

    this.initiateVariable();
  }

  initiateVariable() {
    this.constructLbadbDetail();
    this.constructPenggunaanKomoditasLab();
    this.constructHasilTesLab();
    this.constructInventoryTable();
  }

  generateEntityRole(tableName, preCondition, postCondition) {
    let str =
      (tableName ? tableName + "." : "") +
      (this.entityBaseId ? this.entityBaseId : "");
    if (!this.entityBaseId) {
      str = "";
    } else {
      str =
        (preCondition || "") +
        " " +
        str +
        " = :entityBaseValue " +
        (postCondition || "");
    }
    return str;
  }

  async buildAllReport() {
    await this.buildReportLbadbDetail();
    await this.buildReportPenggunaanKomoditasLab();
    await this.buildReportHasilTesLab();
    await this.buildReportInventoryUsage();
  }

  //construct object
  constructLbadbDetail() {
    this.lbadbDetail = {
      RDT: [],
      EID: [],
      IMS: [],
      VL: [],
      CD4: []
    };
  }
  constructPenggunaanKomoditasLab() {
    this.penggunaanKomoditasLab = {
      content: []
    };
  }
  constructHasilTesLab() {
    this.hasilTesLab = {
      content: [
        {
          title: "Pemeriksaan RDT HIV",
          data: {
            jumlah_pemeriksaan: 0,
            [ENUM.TEST_RESULT.REAKTIF]: 0,
            [ENUM.TEST_RESULT.NON_REAKTIF]: 0,
            [ENUM.TEST_RESULT.INKONKLUSIF]: 0,
            [ENUM.TEST_RESULT.INVALID]: 0
          }
        },
        {
          title: "Pemeriksaan EID",
          data: {
            jumlah_pemeriksaan: 0,
            [ENUM.TEST_RESULT.DETECTED]: 0,
            [ENUM.TEST_RESULT.NOT_DETECTED]: 0,
            [ENUM.TEST_RESULT.INVALID]: 0,
            [ENUM.TEST_RESULT.ERROR]: 0,
            [ENUM.TEST_RESULT.NO_RESULT]: 0
          }
        },
        {
          title: "Pemeriksaan Sifilis",
          data: {
            jumlah_pemeriksaan: 0,
            [ENUM.TEST_RESULT.REAKTIF]: 0,
            [ENUM.TEST_RESULT.NON_REAKTIF]: 0,
            [ENUM.TEST_RESULT.INVALID]: 0
          }
        },
        {
          title: "Pemeriksaan CD4",
          data: {
            jumlah_pemeriksaan: 0,
            [ENUM.TEST_RESULT.LT_200]: 0,
            [ENUM.TEST_RESULT.BTW_200_350]: 0,
            [ENUM.TEST_RESULT.GT_350]: 0,
            [ENUM.TEST_RESULT.INVALID]: 0,
            [ENUM.TEST_RESULT.ERROR]: 0,
            [ENUM.TEST_RESULT.NO_RESULT]: 0
          }
        },
        {
          title: "Pemeriksaan Viral Load",
          data: {
            jumlah_pemeriksaan: 0,
            [ENUM.TEST_RESULT.NOT_DETECTED]: 0,
            [ENUM.TEST_RESULT.LTE_1000]: 0,
            [ENUM.TEST_RESULT.GT_1000]: 0,
            [ENUM.TEST_RESULT.INVALID]: 0,
            [ENUM.TEST_RESULT.ERROR]: 0,
            [ENUM.TEST_RESULT.NO_RESULT]: 0
          }
        }
      ]
    };
  }
  constructInventoryTable() {
    this.inventoryTable = {};
    this.inventoryTable.content = {};
    this.inventoryTable.footer = {};
  }
  //end of construct

  //LBADB detail
  async buildReportLbadbDetail() {
    await super.initiateMetaReport();
    await this.initiateReportLbadb();
    // await this.loadLbadbDetailReportData();

    this.registerLab.lbadbDetail = this.lbadbDetail;
  }
  async initiateReportLbadb() {
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2)
        month = '0' + month;
      if (day.length < 2)
        day = '0' + day;

      return [year, month, day].join('-');
    }

    const { provinceId, sudinKabKotaId, upkId, isSixMonth } = this.req.query;
    let startDate = new Date(`${this.filter.year}-${this.filter.month + 1}-01`);
    let endDate = new Date(`${this.filter.year}-${this.filter.month + 1}-31`);

    if (isSixMonth) {
      // let startDate = new Date(`${this.filter.year}-${this.filter.month + 1}-01`);
      var d = startDate.getDate();
      startDate.setMonth(startDate.getMonth() + +-5);
      if (startDate.getDate() != d) {
        startDate.setDate(0);
      }
      startDate = formatDate(startDate);
      endDate = formatDate(`${this.filter.year}-${this.filter.month + 1}-31`);

      // dateFilter = `visits.visit_date >= '${startDate}' AND visits.visit_date <= '${endDate}'`;
    }

    console.log('Date ..............................');
    console.log(startDate);
    console.log(endDate);
    console.log('End Date ..............................');

    //load medicine R1, R2 and R2
    let r1r2r3meds = [];
    const r1meds = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: Brand,
          required: false
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR1Data",
          where: !isSixMonth ? {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR1Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR1Data.tanggal_test')), this.filter.month + 1)
            ]
          } : {
            tanggal_test: {
              [Op.gte]: startDate,
              [Op.lte]: endDate
            }
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    const r2meds = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: Brand,
          required: false
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR2Data",
          where: !isSixMonth ? {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR2Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR2Data.tanggal_test')), this.filter.month + 1)
            ]
          } : {
            tanggal_test: {
              [Op.gte]: startDate,
              [Op.lte]: endDate
            }
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    const r3meds = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: Brand,
          required: false
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR3Data",
          where: !isSixMonth ? {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR3Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR3Data.tanggal_test')), this.filter.month + 1)
            ]
          } : {
            tanggal_test: {
              [Op.gte]: startDate,
              [Op.lte]: endDate
            }
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    const r12meds = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: Brand,
          required: false
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR12Data",
          where: !isSixMonth ? {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR12Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR12Data.tanggal_test')), this.filter.month + 1)
            ]
          } : {
            tanggal_test: {
              [Op.gte]: startDate,
              [Op.lte]: endDate
            }
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    const r22meds = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: Brand,
          required: false
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR22Data",
          where: !isSixMonth ? {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR22Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR22Data.tanggal_test')), this.filter.month + 1)
            ]
          } : {
            tanggal_test: {
              [Op.gte]: startDate,
              [Op.lte]: endDate
            }
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });

    r1r2r3meds = [...r1meds, ...r2meds, ...r3meds, ...r12meds, ...r22meds];
    let itemsToSend = [];
    if (r1r2r3meds) {
      r1r2r3meds.forEach((item, i) => {
        // usage
        let data = [];

        for (let i = 1; i <= 5; i++) {
          let iNumber = i;

          if (i == 4) {
            i = 12;
          } else if (i == 5) {
            i = 22;
          }

          if (!data[i]) {
            data[i] = {
              REAKTIF: 0,
              NON_REAKTIF: 0,
              INVALID: 0,
              stokDipakai: 0
            };
          }

          if (item['tesReagenR' + i + 'Data']) {
            item['tesReagenR' + i + 'Data'].map(v => {
              if (v['hasilTestR' + i]) {
                if (i == 12 || i == 22) {
                  if (v['typeReagenR' + i] == "R1") {
                    data[1][v['hasilTestR' + i]] += 1;
                    data[1].stokDipakai += v['qtyReagenR' + i];
                  } else if (v['typeReagenR' + i] == "R2") {
                    data[2][v['hasilTestR' + i]] += 1;
                    data[2].stokDipakai += v['qtyReagenR' + i];
                  } else if (v['typeReagenR' + i] == "R3") {
                    data[3][v['hasilTestR' + i]] += 1;
                    data[3].stokDipakai += v['qtyReagenR' + i];
                  }
                } else {
                  data[i][v['hasilTestR' + i]] += 1;
                  data[i].stokDipakai += v['qtyReagenR' + i];
                }
              }
            });
          }

          i = iNumber
        }

        itemsToSend[item.id] = {
          jenis_pemeriksaan: item.nonArvMedicine.testType,
          merk: item.brands[0] ? item.brands[0].name : item.name,
          brand: item.brands[0] ? item.brands[0] : item,
          medicine: item,
          data: {
            r1: {
              title: "Reagen 1",
              stok_dipakai: (itemsToSend[item.id] ? itemsToSend[item.id].data.r1.stok_dipakai : 0) + data[1].stokDipakai,
              [ENUM.TEST_RESULT.REAKTIF]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r1[ENUM.TEST_RESULT.REAKTIF] : 0) + data[1].REAKTIF,
              [ENUM.TEST_RESULT.NON_REAKTIF]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r1[ENUM.TEST_RESULT.NON_REAKTIF] : 0) + data[1].NON_REAKTIF,
              [ENUM.TEST_RESULT.INVALID]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r1[ENUM.TEST_RESULT.INVALID] : 0) + data[1].INVALID
            },
            r2: {
              title: "Reagen 2",
              stok_dipakai: (itemsToSend[item.id] ? itemsToSend[item.id].data.r2.stok_dipakai : 0) + data[2].stokDipakai,
              [ENUM.TEST_RESULT.REAKTIF]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r2[ENUM.TEST_RESULT.REAKTIF] : 0) + data[2].REAKTIF,
              [ENUM.TEST_RESULT.NON_REAKTIF]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r2[ENUM.TEST_RESULT.NON_REAKTIF] : 0) + data[2].NON_REAKTIF,
              [ENUM.TEST_RESULT.INVALID]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r2[ENUM.TEST_RESULT.INVALID] : 0) + data[2].INVALID
            },
            r3: {
              title: "Reagen 3",
              stok_dipakai: (itemsToSend[item.id] ? itemsToSend[item.id].data.r3.stok_dipakai : 0) + data[3].stokDipakai,
              [ENUM.TEST_RESULT.REAKTIF]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r3[ENUM.TEST_RESULT.REAKTIF] : 0) + data[3].REAKTIF,
              [ENUM.TEST_RESULT.NON_REAKTIF]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r3[ENUM.TEST_RESULT.NON_REAKTIF] : 0) + data[3].NON_REAKTIF,
              [ENUM.TEST_RESULT.INVALID]: (itemsToSend[item.id] ? itemsToSend[item.id].data.r3[ENUM.TEST_RESULT.INVALID] : 0) + data[3].INVALID
            }
          }
        };

        //   this.lbadbDetail.RDT.push({
        //     jenis_pemeriksaan: item.nonArvMedicine.testType,
        //     merk: item.brands[0] ? item.brands[0].name : item.name,
        //     brand: item.brands[0] ? item.brands[0] : item,
        //     data: {
        //       r1: {
        //         title: "Reagen 1",
        //         stok_dipakai: data[1].stokDipakai,
        //         [ENUM.TEST_RESULT.REAKTIF]: data[1].REAKTIF,
        //         [ENUM.TEST_RESULT.NON_REAKTIF]: data[1].NON_REAKTIF,
        //         [ENUM.TEST_RESULT.INVALID]: data[1].INVALID
        //       },
        //       r2: {
        //         title: "Reagen 2",
        //         stok_dipakai: data[2].stokDipakai,
        //         [ENUM.TEST_RESULT.REAKTIF]: data[2].REAKTIF,
        //         [ENUM.TEST_RESULT.NON_REAKTIF]: data[2].NON_REAKTIF,
        //         [ENUM.TEST_RESULT.INVALID]: data[2].INVALID
        //       },
        //       r3: {
        //         title: "Reagen 3",
        //         stok_dipakai: data[3].stokDipakai,
        //         [ENUM.TEST_RESULT.REAKTIF]: data[3].REAKTIF,
        //         [ENUM.TEST_RESULT.NON_REAKTIF]: data[3].NON_REAKTIF,
        //         [ENUM.TEST_RESULT.INVALID]: data[3].INVALID
        //       }
        //     }
        //   });
      });

      this.lbadbDetail.RDT = itemsToSend.filter(function (el) {
        return el != null;
      });
    }

    // ---------------------------------------------------------

    //load medicine EID
    const eidMedBrands = await Brand.findAll({
      include: [
        {
          model: Medicine,
          required: true,
          include: [
            {
              model: NonArvMedicine,
              required: true,
              where: {
                isEid: true
              }
            },
            {
              model: TestHiv,
              required: false,
              as: 'tesReagenDnaData',
              where: {
                tanggal_test: {
                  [Op.gte]: startDate,
                  [Op.lte]: endDate
                }
              },
              include: [
                {
                  model: Visit,
                  required: true,
                  include: [
                    {
                      model: Patient,
                      required: true,
                      include: [
                        {
                          model: Upk,
                          required: true,
                          where: (upkId ? { id: upkId } : false),
                          include: [
                            {
                              as: 'sudinKabKota',
                              model: SudinKabKota,
                              required: true,
                              where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                              include: [
                                {
                                  model: Province,
                                  required: true,
                                  where: (provinceId ? { id: provinceId } : false)
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    if (eidMedBrands) {
      eidMedBrands.forEach((item, i) => {
        // usage
        let data = {
          stok_dipakai: 0,
          [ENUM.TEST_RESULT.DETECTED]: 0,
          [ENUM.TEST_RESULT.NOT_DETECTED]: 0,
          [ENUM.TEST_RESULT.INVALID]: 0,
          [ENUM.TEST_RESULT.ERROR]: 0,
          [ENUM.TEST_RESULT.NO_RESULT]: 0,
          [ENUM.TEST_RESULT.KALIBRASI]: 0,
          [ENUM.TEST_RESULT.TERBUANG]: 0,
          [ENUM.TEST_RESULT.GANGGUAN_TEKNIS]: 0,
          [ENUM.TEST_RESULT.SEBAB_LAIN]: 0,
          [ENUM.TEST_RESULT.KONTROL]: 0
        };

        if (item.medicine.tesReagenDnaData) {
          item.medicine.tesReagenDnaData.map(r => {
            data.stok_dipakai += r.qtyReagenDna;
            data[r.hasilTestDna] += 1;
          });
        }

        this.lbadbDetail.EID.push({
          jenis_pemeriksaan: item.medicine.nonArvMedicine.testType,
          merk: item.name,
          brand: _.omit(item.toJSON(), ["medicine"]),
          data: data
        });
      });
    }
    //load medicine IMS
    const imsMedBrands = await Brand.findAll({
      include: [
        {
          model: Medicine,
          required: true,
          include: [
            {
              model: NonArvMedicine,
              required: true,
              where: {
                isMachine: true,
                isIoIms: true
              }
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    // belom ada 
    if (imsMedBrands) {
      imsMedBrands.forEach((item, i) => {
        this.lbadbDetail.IMS.push({
          jenis_pemeriksaan: item.medicine.nonArvMedicine.testType,
          merk: item.name,
          brand: _.omit(item.toJSON(), ["medicine"]),
          data: {
            stok_dipakai: 0,
            [ENUM.TEST_RESULT.REAKTIF]: 0,
            [ENUM.TEST_RESULT.NON_REAKTIF]: 0,
            [ENUM.TEST_RESULT.INVALID]: 0
          }
        });
      });
    }
    //load medicine CD4
    const cd4MedBrands = await Brand.findAll({
      include: [
        {
          model: Medicine,
          required: true,
          include: [
            {
              model: NonArvMedicine,
              required: true,
              where: {
                isCd4: true
              }
            },
            {
              model: TestVl,
              required: false,
              as: 'tesReagenData',
              where: {
                createdAt: {
                  [Op.gte]: startDate,
                  [Op.lte]: endDate
                },
                testVlCd4Type: 'CD4'
              },
              include: [
                {
                  model: Visit,
                  required: true,
                  include: [
                    {
                      model: Patient,
                      required: true,
                      include: [
                        {
                          model: Upk,
                          required: true,
                          where: (upkId ? { id: upkId } : false),
                          include: [
                            {
                              as: 'sudinKabKota',
                              model: SudinKabKota,
                              required: true,
                              where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                              include: [
                                {
                                  model: Province,
                                  required: true,
                                  where: (provinceId ? { id: provinceId } : false)
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });
    if (cd4MedBrands) {
      cd4MedBrands.forEach((item, i) => {
        let data = {
          stok_dipakai: 0,
          [ENUM.TEST_RESULT.LT_200]: 0,
          [ENUM.TEST_RESULT.BTW_200_350]: 0,
          [ENUM.TEST_RESULT.GT_350]: 0,
          [ENUM.TEST_RESULT.INVALID]: 0,
          [ENUM.TEST_RESULT.ERROR]: 0,
          [ENUM.TEST_RESULT.NO_RESULT]: 0,
          [ENUM.TEST_RESULT.KALIBRASI]: 0,
          [ENUM.TEST_RESULT.TERBUANG]: 0,
          [ENUM.TEST_RESULT.GANGGUAN_TEKNIS]: 0,
          [ENUM.TEST_RESULT.SEBAB_LAIN]: 0,
          [ENUM.TEST_RESULT.KONTROL]: 0,
          [ENUM.TEST_RESULT.DETECTED]: 0,
          [ENUM.TEST_RESULT.NOT_DETECTED]: 0,
          [ENUM.TEST_RESULT['20']]: 0,
          [ENUM.TEST_RESULT['34']]: 0,
          [ENUM.TEST_RESULT['40']]: 0,
          [ENUM.TEST_RESULT['50']]: 0,
          'INPUT ANGKA': 0
        };

        if (item.medicine.tesReagenData) {
          item.medicine.tesReagenData.map(r => {
            data.stok_dipakai += r.qtyReagen;
            if (['< 20', '< 34', '< 40', '< 50', '<= 1000', '> 1000', '< 200', '200 - 350', '> 350'].includes(r.hasilTestVlCd4)) {
              data.DETECTED += 1;
            }
            if (r.hasilTestVlCd4 == "INVALID / ERROR") {
              data.INVALID += 1;
              data.ERROR += 1;
            } else if (r.hasilTestInNumber != 'NaN') {
              data['INPUT ANGKA'] += 1;
            }

            data[r.hasilTestVlCd4] += 1;
          });
        }

        this.lbadbDetail.CD4.push({
          jenis_pemeriksaan: item.medicine.nonArvMedicine.testType,
          merk: item.name,
          brand: _.omit(item.toJSON(), ["medicine"]),
          data: data,
          mesin: item.medicine.nonArvMedicine.vlcd4Category
        });
      });
    }
    //load medicine VL
    const vlMedBrands = await Brand.findAll({
      include: [
        {
          model: Medicine,
          required: true,
          include: [
            {
              model: NonArvMedicine,
              required: true,
              where: {
                isVl: true
              }
            },
            {
              model: TestVl,
              required: false,
              as: 'tesReagenData',
              where: {
                date: {
                  [Op.and]: [
                    {
                      [Op.not]: null
                    },
                    {
                      [Op.gte]: startDate,
                      [Op.lte]: endDate
                    }
                  ]
                },
                testVlCd4Type: 'VL'
              },
              include: [
                {
                  model: Visit,
                  required: true,
                  include: [
                    {
                      model: Patient,
                      required: true,
                      include: [
                        {
                          model: Upk,
                          required: true,
                          where: (upkId ? { id: upkId } : false),
                          include: [
                            {
                              as: 'sudinKabKota',
                              model: SudinKabKota,
                              required: true,
                              where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                              include: [
                                {
                                  model: Province,
                                  required: true,
                                  where: (provinceId ? { id: provinceId } : false)
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      order: [["name", "ASC"]]
    });

    let vlMedUsage = {};
    const vlMedUsageGet = await UsageVlCd4.findAll({
      include: [
        {
          model: UsageVlCd4Medicine
        }
      ],
      where: {
        testType: "VL",
        date: {
          [Op.gte]: startDate,
          [Op.lte]: endDate
        }
      }
    });
    vlMedUsageGet.map(r => {
      r.usageVlCd4Medicines.map(rr => {
        // console.log(rr.dataValues);
        if (!vlMedUsage[rr.medicineId]) {
          vlMedUsage[rr.medicineId] = 0;
        }
        vlMedUsage[rr.medicineId] += rr.amount;
      })
    })
    // console.log(vlMedUsage);

    if (vlMedBrands) {
      vlMedBrands.forEach((item, i) => {
        let data = {
          stok_dipakai: 0,
          [ENUM.TEST_RESULT.DETECTED]: 0,
          [ENUM.TEST_RESULT.NOT_DETECTED]: 0,
          [ENUM.TEST_RESULT.LTE_1000]: 0,
          [ENUM.TEST_RESULT.GT_1000]: 0,
          [ENUM.TEST_RESULT.INVALID]: 0,
          [ENUM.TEST_RESULT.ERROR]: 0,
          [ENUM.TEST_RESULT.NO_RESULT]: 0,
          [ENUM.TEST_RESULT.KALIBRASI]: 0,
          [ENUM.TEST_RESULT.TERBUANG]: 0,
          [ENUM.TEST_RESULT.GANGGUAN_TEKNIS]: 0,
          [ENUM.TEST_RESULT.SEBAB_LAIN]: 0,
          [ENUM.TEST_RESULT.KONTROL]: 0,
          REAGENT_USED: vlMedUsage[item.medicine.id] ? vlMedUsage[item.medicine.id] : 0,
          [ENUM.TEST_RESULT['20']]: 0,
          [ENUM.TEST_RESULT['34']]: 0,
          [ENUM.TEST_RESULT['40']]: 0,
          [ENUM.TEST_RESULT['50']]: 0,
          'INPUT ANGKA': 0
        }

        if (item.medicine.tesReagenData) {
          item.medicine.tesReagenData.map(r => {
            if (['< 20', '< 34', '< 40', '< 50', '<= 1000', '> 1000'].includes(r.hasilTestVlCd4)) {
              data.DETECTED += 1;
            }
            if (r.hasilTestVlCd4 == "INVALID / ERROR") {
              data.INVALID += 1;
              data.ERROR += 1;
            } else if (r.hasilTestInNumber != 'NaN') {
              data['INPUT ANGKA'] += 1;
            } else {
              // data.stok_dipakai += r.qtyReagen;
              
              // data.stok_dipakai += 1;
            }
            data[r.hasilTestVlCd4] += 1;
            // data[ENUM.TEST_RESULT.TERBUANG] = data.REAGENT_USED - data.stok_dipakai;
          });
        }

        this.lbadbDetail.VL.push({
          jenis_pemeriksaan: item.medicine.nonArvMedicine.testType,
          merk: item.name,
          brand: _.omit(item.toJSON(), ["medicine"]),
          data: data,
          mesin: item.medicine.nonArvMedicine.vlcd4Category
        });
      });
    }
  }
  async loadLbadbDetailReportData() {
    //Pemeriksaan RDT HIV
    let resultHiv = await sequelize.query(
      RegisterLabService.hasilTesLabQueryBuilder(
        "test_hiv",
        "kesimpulan_hiv",
        true,
        this.filter.month + 1,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        ` jenis_test = '${ENUM.HIV_TEST_TYPE.RDT}' `
      ),
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToContent(this.hasilTesLab.content[0], resultHiv);
    this.countResultToContent(this.hasilTesLab.content[0], resultHiv);
    //End of Pemeriksaan RDT HIV
  }
  //end of LBADB detail

  //Penggunaan komoditas Ops
  async buildReportPenggunaanKomoditasLab() {
    await super.initiateMetaReport();
    await this.initiateReportPenggunaanKomoditasLab();
    await this.loadReportPenggunaanKomoditasLab();

    this.registerLab.komoditasLab = this.penggunaanKomoditasLab;
  }
  async initiateReportPenggunaanKomoditasLab() {
    const { provinceId, sudinKabKotaId, upkId } = this.req.query;

    //load medicine R1, R2 and R2 "Jumlah pemeriksaan test HIV-RDT"
    console.log('load medicine R1, R2 and R2 "Jumlah pemeriksaan test HIV-RDT"');

    let condts = {
      logisticRole: this.staff.logisticrole,
      [Op.and]: [
        sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('inventories.created_at')), this.filter.year),
        // sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('inventories.created_at')), this.filter.month + 1)
      ]
    };
    if (upkId) {
      condts = {
        ...condts,
        upkId: upkId
      }
    }
    if (sudinKabKotaId) {
      condts = {
        ...condts,
        sudinKabKotaId: sudinKabKotaId
      }
    }
    if (provinceId) {
      condts = {
        ...condts,
        provinceId: provinceId
      }
    }

    let r1r2r3meds = [];
    const r1meds = await Medicine.findAll({
      // logging: console.log,
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR1Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR1Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR1Data.tanggal_test')), this.filter.month + 1)
            ]
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      order: [["name", "ASC"]]
    });
    const r2meds = await Medicine.findAll({
      // logging: console.log,
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR2Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR2Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR2Data.tanggal_test')), this.filter.month + 1)
            ]
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      order: [["name", "ASC"]]
    });
    const r3meds = await Medicine.findAll({
      // logging: console.log,
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR3Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR3Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR3Data.tanggal_test')), this.filter.month + 1)
            ]
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      order: [["name", "ASC"]]
    });
    const r12meds = await Medicine.findAll({
      // logging: console.log,
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR12Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR12Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR12Data.tanggal_test')), this.filter.month + 1)
            ]
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      order: [["name", "ASC"]]
    });
    const r22meds = await Medicine.findAll({
      // logging: console.log,
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            [Op.or]: [
              {
                isR1: true
              },
              {
                isR2: true
              },
              {
                isR3: true
              }
            ]
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR22Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR22Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR22Data.tanggal_test')), this.filter.month + 1)
            ]
          },
          include: [
            {
              model: Visit,
              required: true,
              include: [
                {
                  model: Patient,
                  required: true,
                  include: [
                    {
                      model: Upk,
                      required: true,
                      where: (upkId ? { id: upkId } : false),
                      include: [
                        {
                          as: 'sudinKabKota',
                          model: SudinKabKota,
                          required: true,
                          where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                          include: [
                            {
                              model: Province,
                              required: true,
                              where: (provinceId ? { id: provinceId } : false)
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      order: [["name", "ASC"]]
    });

    r1r2r3meds = [...r1meds, ...r2meds, ...r3meds, ...r12meds, ...r22meds];
    let itemsToSend = [];

    if (r1r2r3meds) {
      r1r2r3meds.forEach((item) => {
        // usage
        let data = [];

        for (let i = 1; i <= 5; i++) {
          let iNumber = i;

          if (i == 4) {
            i = 12;
          } else if (i == 5) {
            i = 22;
          }

          if (!data[i]) {
            data[i] = {
              stokDipakai: 0,
              invalid: 0
            };
          }

          if (item['tesReagenR' + i + 'Data']) {
            item['tesReagenR' + i + 'Data'].map(v => {
              if (v['hasilTestR' + i]) {
                if (v['hasilTestR' + i] == 'INVALID') {
                  data[i].invalid += v['qtyReagenR' + i];
                } else {
                  data[i].stokDipakai += v['qtyReagenR' + i];
                }
              }
            });
          }

          i = iNumber;
        }

        let sumberDana = "";
        if (item.inventories) {
          let sd = [];
          item.inventories.map(r => {
            if (!sd.includes(r.fundSource)) {
              sd.push(r.fundSource);
            }
          })

          sumberDana = sd.join(', ');
        }

        itemsToSend[item.id] = {
          title: `Jumlah pemeriksaan test HIV-RDT`,
          komoditas: item.name,
          satuan: item.stockUnitType,
          jumlah_stok_digunakan: (itemsToSend[item.id] ? itemsToSend[item.id].jumlah_stok_digunakan : 0) + (data[1].stokDipakai + data[2].stokDipakai + data[3].stokDipakai + data[12].stokDipakai + data[22].stokDipakai),
          jumlah_stok_terbuang: (itemsToSend[item.id] ? itemsToSend[item.id].jumlah_stok_terbuang : 0) + (data[1].invalid + data[2].invalid + data[3].invalid + data[12].invalid + data[22].invalid),
          medicine: item,
          sumber_dana: itemsToSend[item.id] ? itemsToSend[item.id].sumber_dana : sumberDana,
          stok_akhir: 0,
          kalibrasi: 0
        }

        // this.penggunaanKomoditasLab.content.push({
        //   title: `Jumlah pemeriksaan test HIV-RDT`,
        //   komoditas: item.name,
        //   satuan: item.stockUnitType,
        //   jumlah_stok_digunakan: data[1].stokDipakai + data[2].stokDipakai + data[3].stokDipakai,
        //   jumlah_stok_terbuang: 0,
        //   medicine: item.toJSON(),
        //   sumber_dana: item.fundSource,
        //   stok_akhir: 0,
        //   kalibrasi: 0
        // });
      });

      this.penggunaanKomoditasLab.content = itemsToSend.filter(function (el) {
        return el != null;
      });
    }

    // belom tau tesnya dimana || udah tau
    //load medicine Jumlah pemeriksaan test Sifilis-RDT
    console.log('load medicine Jumlah pemeriksaan test Sifilis-RDT');
    let itemsToSend2 = [];

    const sifilisRDTMed = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            isSifilis: true
          }
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      // where: {
      //   id: {
      //     [Op.in]: [35, 30231, 30246, 30241, 30242]
      //   }
      // },
      order: [["name", "ASC"]]
    });

    if (sifilisRDTMed) {
      sifilisRDTMed.forEach((item, i) => {
        let sumberDana = "";
        if (item.inventories) {
          let sd = [];
          item.inventories.map(r => {
            if (!sd.includes(r.fundSource)) {
              sd.push(r.fundSource);
            }
          })

          sumberDana = sd.join(', ');
        }

        itemsToSend2[item.id] = {
          title: `Jumlah pemeriksaan test Sifilis-RDT`,
          komoditas: item.name,
          satuan: item.stockUnitType,
          jumlah_stok_digunakan: 0,
          jumlah_stok_terbuang: 0,
          medicine: item,
          sumber_dana: sumberDana,
          stok_akhir: 0,
          kalibrasi: 0
        }
      })
    }

    const query = `
      SELECT
        "visitImsLab".rpr_vdrl ->> 'result' AS "result",
        "visitImsLab".rpr_vdrl ->> 'reagent' AS "reagent",
        "visitImsLab".rpr_vdrl ->> 'totalReagentUsed' AS "total",
        medicines.* 
      FROM
        "ims_visit_labs" AS "visitImsLab"
        JOIN "visits" AS "visit" ON "visitImsLab"."visit_id" = "visit"."id" 
        AND ( "visit"."deleted_at" IS NULL AND ( date_part( 'year', "visit_date" ) = 2021 AND date_part( 'month', "visit_date" ) = 4 ) )
        JOIN medicines ON medicines.ID = ( "visitImsLab".rpr_vdrl :: json ->> 'reagent' ) :: INTEGER 
      WHERE
        ( "visitImsLab"."deleted_at" IS NULL ) 
        AND "visitImsLab".rpr_vdrl ->> 'reagent' != ''
    `;
    const sifilisRDT = await sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT
    });

    if (sifilisRDT) {
      sifilisRDT.forEach((item, i) => {
        // usage
        itemsToSend2[item.id] = {
          title: `Jumlah pemeriksaan test Sifilis-RDT`,
          komoditas: item.name,
          satuan: item.stock_unit_type,
          jumlah_stok_digunakan: (itemsToSend2[item.id] ? itemsToSend2[item.id].jumlah_stok_digunakan : 0) + parseInt(item.total),
          jumlah_stok_terbuang: 0,
          medicine: item,
          sumber_dana: itemsToSend2[item.id] ? itemsToSend2[item.id].sumber_dana : null,
          stok_akhir: 0,
          kalibrasi: 0
        }
      });
    }
    let obat = itemsToSend2.filter(function (el) {
      return el != null;
    });
    // console.log(obat);
    obat.map(v => {
      this.penggunaanKomoditasLab.content.push(v);
    })

    // belom tau tesnya dimana || udah tau
    //load medicine Jumlah pemeriksaan test Sifilis-RPR
    console.log('load medicine Jumlah pemeriksaan test Sifilis-RPR');
    const sifilisRPR = await Medicine.findAll({
      include: [
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR1Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR1Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR1Data.tanggal_test')), this.filter.month + 1)
            ]
            // tanggal_test: {
            //   [Op.gte]: startDate,
            //   [Op.lte]: endDate
            // }
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR2Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR2Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR2Data.tanggal_test')), this.filter.month + 1)
            ]
            // tanggal_test: {
            //   [Op.gte]: startDate,
            //   [Op.lte]: endDate
            // }
          }
        },
        {
          model: TestHiv,
          required: false,
          as: "tesReagenR3Data",
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenR3Data.tanggal_test')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenR3Data.tanggal_test')), this.filter.month + 1)
            ]
            // tanggal_test: {
            //   [Op.gte]: startDate,
            //   [Op.lte]: endDate
            // }
          }
        },
        {
          model: Inventory,
          required: false,
          where: condts
        }
      ],
      where: {
        id: {
          [Op.in]: [30232]
        },
        // deletedAt: {
        //   $eq: null
        // }
      },
      order: [["name", "ASC"]]
    });
    if (sifilisRPR) {
      sifilisRPR.forEach((item, i) => {
        // usage
        let data = [];

        for (let i = 1; i <= 3; i++) {
          if (!data[i]) {
            data[i] = {
              stokDipakai: 0
            };
          }

          item['tesReagenR' + i + 'Data'].map(v => {
            if (v['hasilTestR' + i]) {
              data[i].stokDipakai += v['qtyReagenR' + i];
            }
          });
        }

        let sumberDana = "";
        if (item.inventories) {
          let sd = [];
          item.inventories.map(r => {
            if (!sd.includes(r.fundSource)) {
              sd.push(r.fundSource);
            }
          })

          sumberDana = sd.join(', ');
        }

        this.penggunaanKomoditasLab.content.push({
          title: `Jumlah pemeriksaan test Sifilis-RPR`,
          komoditas: item.name,
          satuan: item.stockUnitType,
          jumlah_stok_digunakan: data[1].stokDipakai + data[2].stokDipakai + data[3].stokDipakai,
          jumlah_stok_terbuang: 0,
          medicine: item.toJSON(),
          sumber_dana: sumberDana,
          stok_akhir: 0,
          kalibrasi: 0
        });
      });
    }

    //load medicine Jumlah pemeriksaan PCR-HIV
    console.log('load medicine Jumlah pemeriksaan PCR-HIV');
    const pcrHIV = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            isVl: true
          }
        },
        // {
        //   model: TestVl,
        //   required: false,
        //   as: 'tesReagenData',
        //   where: {
        //     [Op.and]: [
        //       sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenData.created_at')), this.filter.year),
        //       sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenData.created_at')), this.filter.month + 1)
        //     ],
        //     testVlCd4Type: 'VL'
        //   },
        //   include: [
        //     {
        //       model: Visit,
        //       required: true,
        //       include: [
        //         {
        //           model: Patient,
        //           required: true,
        //           include: [
        //             {
        //               model: Upk,
        //               required: true,
        //               where: (upkId ? { id: upkId } : false),
        //               include: [
        //                 {
        //                   as: 'sudinKabKota',
        //                   model: SudinKabKota,
        //                   required: true,
        //                   where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
        //                   include: [
        //                     {
        //                       model: Province,
        //                       required: true,
        //                       where: (provinceId ? { id: provinceId } : false)
        //                     }
        //                   ]
        //                 }
        //               ]
        //             }
        //           ]
        //         }
        //       ]
        //     }
        //   ]
        // },
        {
          model: Inventory,
          required: false,
          where: condts
        },
        {
          model: UsageVlCd4Medicine,
          required: false,
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('usageVlCd4Medicines.created_at')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('usageVlCd4Medicines.created_at')), this.filter.month + 1)
            ],
          },
          include: [
            {
              model: UsageVlCd4,
              required: true,
              where: {
                testType: "VL"
              },
              include: [
                {
                  model: Upk,
                  required: true,
                  where: (upkId ? { id: upkId } : false),
                  include: [
                    {
                      as: 'sudinKabKota',
                      model: SudinKabKota,
                      required: true,
                      where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                      include: [
                        {
                          model: Province,
                          required: true,
                          where: (provinceId ? { id: provinceId } : false)
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
      ],
      // where: {
      //   id: {
      //     [Op.in]: [30105, 30106, 30104]
      //   },
      //   // deletedAt: {
      //   //   $eq: null
      //   // }
      // },
      order: [["name", "ASC"]]
    });
    if (pcrHIV) {
      pcrHIV.forEach((item, i) => {
        // usage
        let data = {
          stok_dipakai: 0,
          INVALID: 0
        };

        // old
        // if (item.tesReagenData) {
        //   item.tesReagenData.map(r => {
        //     if (r.hasilTestVlCd4 == "INVALID / ERROR") {
        //       data.INVALID += 1;
        //     } else {
        //       data.stok_dipakai += r.qtyReagen;
        //     }
        //   });
        // }

        // new
        item.usageVlCd4Medicines.map(r => {
          data.stok_dipakai += r.amount;
          data.INVALID += r.amount - r.usageVlCd4.examination;
        })

        let sumberDana = "";
        if (item.inventories) {
          let sd = [];
          item.inventories.map(r => {
            if (!sd.includes(r.fundSource)) {
              sd.push(r.fundSource);
            }
          })

          sumberDana = sd.join(', ');
        }

        this.penggunaanKomoditasLab.content.push({
          title: `Jumlah pemeriksaan PCR-HIV`,
          komoditas: item.name,
          satuan: item.stockUnitType,
          jumlah_stok_digunakan: data.stok_dipakai,
          jumlah_stok_terbuang: data.INVALID,
          medicine: item.toJSON(),
          sumber_dana: sumberDana,
          stok_akhir: 0,
          kalibrasi: 0
        });
      });
    }

    //load medicine Jumlah pemeriksaan test CD4 
    console.log('load medicine Jumlah pemeriksaan test CD4 ');
    const tesCD4 = await Medicine.findAll({
      include: [
        {
          model: NonArvMedicine,
          required: true,
          where: {
            isCd4: true
          }
        },
        // {
        //   model: TestVl,
        //   required: false,
        //   as: 'tesReagenData',
        //   where: {
        //     [Op.and]: [
        //       sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('tesReagenData.created_at')), this.filter.year),
        //       sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('tesReagenData.created_at')), this.filter.month + 1)
        //     ],
        //     testVlCd4Type: 'CD4'
        //   },
        //   include: [
        //     {
        //       model: Visit,
        //       required: true,
        //       include: [
        //         {
        //           model: Patient,
        //           required: true,
        //           include: [
        //             {
        //               model: Upk,
        //               required: true,
        //               where: (upkId ? { id: upkId } : false),
        //               include: [
        //                 {
        //                   as: 'sudinKabKota',
        //                   model: SudinKabKota,
        //                   required: true,
        //                   where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
        //                   include: [
        //                     {
        //                       model: Province,
        //                       required: true,
        //                       where: (provinceId ? { id: provinceId } : false)
        //                     }
        //                   ]
        //                 }
        //               ]
        //             }
        //           ]
        //         }
        //       ]
        //     }
        //   ]
        // },
        {
          model: Inventory,
          required: false,
          where: condts
        },
        {
          model: UsageVlCd4Medicine,
          required: false,
          where: {
            [Op.and]: [
              sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('usageVlCd4Medicines.created_at')), this.filter.year),
              sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('usageVlCd4Medicines.created_at')), this.filter.month + 1)
            ],
          },
          include: [
            {
              model: UsageVlCd4,
              required: true,
              where: {
                testType: "CD4"
              },
              include: [
                {
                  model: Upk,
                  required: true,
                  where: (upkId ? { id: upkId } : false),
                  include: [
                    {
                      as: 'sudinKabKota',
                      model: SudinKabKota,
                      required: true,
                      where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                      include: [
                        {
                          model: Province,
                          required: true,
                          where: (provinceId ? { id: provinceId } : false)
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
      ],
      // where: {
      //   id: {
      //     [Op.in]: [25]
      //   },
      //   // deletedAt: {
      //   //   $eq: null
      //   // }
      // },
      order: [["name", "ASC"]]
    });
    if (tesCD4) {
      tesCD4.forEach((item, i) => {
        /// usage
        let data = {
          stok_dipakai: 0,
          INVALID: 0
        };

        // old
        // if (item.tesReagenData) {
        //   item.tesReagenData.map(r => {
        //     if (r.hasilTestVlCd4 == "INVALID / ERROR") {
        //       data.INVALID += 1;
        //     } else {
        //       data.stok_dipakai += r.qtyReagen;
        //     }
        //   });
        // }

        // new
        item.usageVlCd4Medicines.map(r => {
          data.stok_dipakai += r.amount;
          data.INVALID += r.amount - r.usageVlCd4.examination;
        })

        let sumberDana = "";
        if (item.inventories) {
          let sd = [];
          item.inventories.map(r => {
            if (!sd.includes(r.fundSource)) {
              sd.push(r.fundSource);
            }
          })

          sumberDana = sd.join(', ');
        }

        this.penggunaanKomoditasLab.content.push({
          title: `Jumlah pemeriksaan test CD4 `,
          komoditas: item.name,
          satuan: item.stockUnitType,
          jumlah_stok_digunakan: data.stok_dipakai,
          jumlah_stok_terbuang: data.INVALID,
          medicine: item.toJSON(),
          sumber_dana: sumberDana,
          stok_akhir: 0,
          kalibrasi: 0
        });
      });
    }
  }
  assignResultToStokDigunakan(arrayOfUsage, type = null) {
    if (arrayOfUsage && arrayOfUsage.length > 0) {
      if (type == 'STOK_AKHIR') {
        this.penggunaanKomoditasLab.content.forEach(komoditasItem => {
          const medicine = arrayOfUsage.filter(v => v.id == komoditasItem.medicine.id);
          if (medicine[0]) {
            // komoditasItem.stok_akhir += medicine[0].package_multiplier * medicine[0].package_quantity;
            komoditasItem.stok_akhir += medicine[0].stock_qty;
          }
        });
      } else if (type == 'STOK_AKHIR_LAYANAN') {
        this.penggunaanKomoditasLab.content.forEach(komoditasItem => {
          const medicine = arrayOfUsage.filter(v => v.id == komoditasItem.medicine.id);
          if (medicine[0]) {
            if (!komoditasItem.stok_akhir_layanan) {
              komoditasItem.stok_akhir_layanan = 0;
            }
            komoditasItem.stok_akhir_layanan += medicine[0].stock_qty;
          }
        });
      } else if (type == 'STOK_AKHIR_KABKOTA') {
        this.penggunaanKomoditasLab.content.forEach(komoditasItem => {
          const medicine = arrayOfUsage.filter(v => v.id == komoditasItem.medicine.id);
          if (medicine[0]) {
            if (!komoditasItem.stok_akhir_kabkota) {
              komoditasItem.stok_akhir_kabkota = 0;
            }
            komoditasItem.stok_akhir_kabkota += medicine[0].stock_qty;
          }
        });
      } else if (type == 'STOK_AKHIR_PROVINSI') {
        this.penggunaanKomoditasLab.content.forEach(komoditasItem => {
          const medicine = arrayOfUsage.filter(v => v.id == komoditasItem.medicine.id);
          if (medicine[0]) {
            if (!komoditasItem.stok_akhir_provinsi) {
              komoditasItem.stok_akhir_provinsi = 0;
            }
            komoditasItem.stok_akhir_provinsi += medicine[0].stock_qty;
          }
        });
      } else {
        arrayOfUsage.forEach(usageItem => {
          this.penggunaanKomoditasLab.content.forEach(komoditasItem => {
            if (komoditasItem.medicine.id === usageItem.medicine_id) {
              if (type == null) {
                komoditasItem.jumlah_stok_digunakan += usageItem.jumlah;
                komoditasItem.jumlah_stok_terbuang += usageItem.jumlah_invalid;
              } else if (type == 'KALIBRASI') {
                komoditasItem.kalibrasi += parseInt(usageItem.jumlah);
              }
            }
          });
        });
      }
    }
  }
  async loadReportPenggunaanKomoditasLab() {
    // let resultKomoditasDigunakan = await sequelize.query(
    //   RegisterLabService.komoditasLabQueryBuilder(
    //     this.filter.month + 1,
    //     this.filter.year,
    //     this.staff,
    //     Object.assign(
    //       {
    //         columnEntityName: this.entityBaseId,
    //         columnEntityValue: this.entityBaseValue
    //       },
    //       this.filter
    //     ),
    //     `date_part('year', inventory_usage.created_at) = ${this.filter.year} AND date_part('month', inventory_usage.created_at) = ${this.filter.month + 1}`
    //   ),
    //   {
    //     type: sequelize.QueryTypes.SELECT
    //   }
    // );
    // this.assignResultToStokDigunakan(resultKomoditasDigunakan);

    let resultKalibrations = await sequelize.query(
      `
      SELECT
        inventories.upk_id,
        inventories.medicine_id,
        SUM ( inventory_kalibrations.stock_qty ) as jumlah
      FROM
        inventory_kalibrations
        JOIN inventories ON inventories.ID = inventory_kalibrations.inventory_id 
      WHERE
        ` + (this.entityBaseId && this.entityBaseValue ? `${this.entityBaseId} = ${this.entityBaseValue} AND ` : ``) + `
        date_part( 'year', inventory_kalibrations.created_at ) = ${this.filter.year} 
        AND date_part( 'month', inventory_kalibrations.created_at ) = ${this.filter.month + 1} 
      GROUP BY
        upk_id,
        inventories.medicine_id
      `,
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToStokDigunakan(resultKalibrations, 'KALIBRASI');

    const { provinceId, sudinKabKotaId, upkId } = this.req.query;

    const medicineType = 'NON_ARV';
    const logisticRole = this.req.user.logisticrole;
    const role = this.req.user.role;
    // const upkId = this.filter.upkId;
    // const sudinKabKotaId = this.filter.sudinKabKotaId;
    // const provinceId = this.filter.provinceId;

    let medicineStock = await InventoryService.getMedicineStock(
      this.req.user,
      medicineType,
      null,
      this.req.query
    );

    // const paramsQuery = {
    //   logisticrole: logisticRole,
    //   role: role,
    //   upkId: upkId,
    //   sudinKabKotaId: sudinKabKotaId,
    //   provinceId: provinceId
    // };

    // let medicineStock = await InventoryService.getMedicineStock(
    //   // logisticRole ? paramsQuery : req.user,
    //   paramsQuery,
    //   medicineType,
    //   null,
    //   this.req.query
    // )

    this.assignResultToStokDigunakan(medicineStock, 'STOK_AKHIR');


    // lab
    if (['UPK_ENTITY'].includes(logisticRole)) {
      let medicineStock2 = await InventoryService.getMedicineStock(
        // logisticRole ? paramsQuery : req.user,
        {
          logisticrole: 'LAB_ENTITY',
          role: 'LAB_STAFF',
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId
        },
        medicineType,
        null,
        this.req.query
      );

      this.assignResultToStokDigunakan(medicineStock2, 'STOK_AKHIR');
    }

    // layanan
    if (['SUDIN_ENTITY', 'PROVINCE_ENTITY', 'MINISTRY_ENTITY'].includes(logisticRole)) {
      let where = ``;
      if (logisticRole == 'SUDIN_ENTITY') {
        where = `WHERE sudin_kab_kota_id = ${sudinKabKotaId} AND`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
      } else if (logisticRole == 'PROVINCE_ENTITY') {
        where = `WHERE province_id = ${provinceId} AND`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
        if (sudinKabKotaId) {
          where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
        }
      } else if (logisticRole == 'MINISTRY_ENTITY') {
        where = `WHERE`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
        if (sudinKabKotaId) {
          where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
        }
        if (provinceId) {
          where += ` province_id = ${provinceId} AND `;
        }
      }

      // layanan
      let medicineStock3 = await InventoryService.getMedicineStock(
        {
          logisticrole: 'UPK_ENTITY',
          role: 'PHARMA_STAFF',
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId
        },
        medicineType,
        null,
        this.req.query,
        ` ${where} logistic_role = 'UPK_ENTITY' `
      );

      this.assignResultToStokDigunakan(medicineStock3, 'STOK_AKHIR_LAYANAN');

      // lab
      let medicineStock31 = await InventoryService.getMedicineStock(
        {
          logisticrole: 'LAB_ENTITY',
          role: 'LAB_STAFF',
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId
        },
        medicineType,
        null,
        this.req.query,
        ` ${where} logistic_role = 'LAB_ENTITY' `
      );

      this.assignResultToStokDigunakan(medicineStock31, 'STOK_AKHIR_LAYANAN');
    }

    // kabKota
    if (['PROVINCE_ENTITY', 'MINISTRY_ENTITY'].includes(logisticRole)) {
      let where = ``;
      if (logisticRole == 'PROVINCE_ENTITY') {
        where = `WHERE province_id = ${provinceId} AND`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
        if (sudinKabKotaId) {
          where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
        }
      } else if (logisticRole == 'MINISTRY_ENTITY') {
        where = `WHERE`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
        if (sudinKabKotaId) {
          where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
        }
        if (provinceId) {
          where += ` province_id = ${provinceId} AND `;
        }
      }

      let medicineStock4 = await InventoryService.getMedicineStock(
        {
          logisticrole: 'SUDIN_ENTITY',
          role: 'SUDIN_STAFF',
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId
        },
        medicineType,
        null,
        this.req.query,
        ` ${where} logistic_role = 'SUDIN_ENTITY' `
      );

      this.assignResultToStokDigunakan(medicineStock4, 'STOK_AKHIR_KABKOTA');
    }

    // provinsi
    if (['MINISTRY_ENTITY'].includes(logisticRole)) {
      let where = `WHERE`;
      if (upkId) {
        where += ` upk_id = ${upkId} AND `;
      }
      if (sudinKabKotaId) {
        where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
      }
      if (provinceId) {
        where += ` province_id = ${provinceId} AND `;
      }

      let medicineStock5 = await InventoryService.getMedicineStock(
        {
          logisticrole: 'PROVINCE_ENTITY',
          role: 'PROVINCE_STAFF',
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId
        },
        medicineType,
        null,
        this.req.query,
        ` ${where} logistic_role = 'PROVINCE_ENTITY' `
      );

      this.assignResultToStokDigunakan(medicineStock5, 'STOK_AKHIR_PROVINSI');
    }
  }
  //end of Penggunaan komoditas Ops

  //Hasil Tes Lab Ops
  async buildReportHasilTesLab() {
    await super.initiateMetaReport();
    await this.loadHasilTesLabReportData();

    this.registerLab.hasilTesLab = this.hasilTesLab;
  }
  assignResultToContent(testLabRow, arrayOfCount) {
    arrayOfCount.forEach((item, i) => {
      if (
        item &&
        item.kesimpulan &&
        _.isNumber(testLabRow.data[item.kesimpulan])
      ) {
        testLabRow.data[item.kesimpulan] += parseInt(item.jumlah);
      }
    });
  }
  countResultToContent(testLabRow, arrayOfCount) {
    let count = 0;
    arrayOfCount.forEach((item, i) => {
      count += parseInt(item.jumlah);
    });
    testLabRow.data.jumlah_pemeriksaan += count;
  }
  async loadHasilTesLabReportData() {
    //Pemeriksaan RDT HIV
    let resultHiv = await sequelize.query(
      RegisterLabService.hasilTesLabQueryBuilder(
        "test_hiv",
        "kesimpulan_hiv",
        true,
        this.filter.month + 1,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        ` jenis_test = '${ENUM.HIV_TEST_TYPE.RDT}' `,
        this.req
      ),
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToContent(this.hasilTesLab.content[0], resultHiv);
    this.countResultToContent(this.hasilTesLab.content[0], resultHiv);
    //End of Pemeriksaan RDT HIV

    //Pemeriksaan PCR-DNA
    let resultEid = await sequelize.query(
      RegisterLabService.hasilTesLabQueryBuilder(
        "test_hiv",
        "kesimpulan_hiv",
        true,
        this.filter.month + 1,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        ` jenis_test = '${ENUM.HIV_TEST_TYPE.PCR_DNA}' `
      ),
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToContent(this.hasilTesLab.content[1], resultEid);
    this.countResultToContent(this.hasilTesLab.content[1], resultEid);
    //Pemeriksaan PCR_DNA

    //Pemeriksaan Sifilis
    let resultSifilis = await sequelize.query(
      RegisterLabService.hasilTesLabQueryBuilder(
        "test_ims",
        "hasil_test_sifilis",
        true,
        this.filter.month + 1,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        )
      ),
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToContent(this.hasilTesLab.content[2], resultSifilis);
    this.countResultToContent(this.hasilTesLab.content[2], resultSifilis);
    //End of Pemeriksaan Sifilis

    //Pemeriksaan CD4
    let resultCD4 = await sequelize.query(
      RegisterLabService.hasilTesLabQueryBuilder(
        "test_vl_cd4",
        "hasil_test_vl_cd4",
        true,
        this.filter.month + 1,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        ` test_vl_cd4_type = '${ENUM.VL_TEST_TYPE.CD4
        }' AND hasil_test_vl_cd4 IN (${_.values(ENUM.TEST_RESULT)
          .map(item => "'" + item + "'")
          .join(",")})  `
      ),
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToContent(this.hasilTesLab.content[3], resultCD4);
    this.countResultToContent(this.hasilTesLab.content[3], resultCD4);
    //End of Pemeriksaan CD4

    //Pemeriksaan VL
    let resultVL = await sequelize.query(
      RegisterLabService.hasilTesLabQueryBuilder(
        "test_vl_cd4",
        "hasil_test_vl_cd4",
        true,
        this.filter.month + 1,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        ` test_vl_cd4_type = '${ENUM.VL_TEST_TYPE.VL
        }' AND hasil_test_vl_cd4 IN (${_.values(ENUM.TEST_RESULT)
          .map(item => "'" + item + "'")
          .join(",")})  `
      ),
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    this.assignResultToContent(this.hasilTesLab.content[4], resultVL);
    this.countResultToContent(this.hasilTesLab.content[4], resultVL);
    //End of Pemeriksaan VL
  }
  //end of Tes Lab Ops

  // stok Ops
  async buildReportInventoryUsage() {
    await super.initiateMetaReport();
    await this.intiateBrandList();
    await this.loadBrandReportData();
    // await this.constructBrandList();

    this.registerLab.inventoryTable = this.inventoryTable;
  }
  async intiateBrandList() {
    console.log('.....................Laporan stok logistik NON-ARV...................');
    const ids = [];
    const nonIms = await NonArvMedicine.findAll({
      attributes: ['medicineId'],
      where: {
        isIms: false
      }
    });
    _.forEach(nonIms, function (val) {
      ids.push(val.medicineId);
    });
    const brandList = await Brand.findAll({
      include: [
        {
          model: Medicine,
          required: true,
          where: {
            medicineType: "NON_ARV",
            // id: {
            //   [Op.in]: ids
            // }
          }
        }
      ],
      order: [["id", "ASC"]]
    });

    let brandReportList = [];
    if (brandList && brandList.length > 0) {
      for (let i = 0; i < brandList.length; i++) {
        const brand = brandList[i].toJSON();
        let itemReport = {
          brand: brand,
          medicine: brand.medicine,
          data: _.cloneDeep(topHeaderInventory)
        };
        itemReport.data.jenis_komoditas = brand.medicine.name;
        itemReport.data.merk_dagang = brand.name;
        itemReport.data.satuan = brand.medicine.stockUnitType;
        itemReport.data.sumber_dana =
          brand.fundSource || brand.medicine.fundSource;

        brandReportList.push(itemReport);
      }
    }
    this.inventoryTable.content = brandReportList;
  }
  assignResultStockToContent(result, table) {
    if (result.length > 0) {
      _.forEach(table.content, rowItem => {
        const matched = _.remove(result, item => {
          return rowItem.brand.id == item.brand_id;
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            rowItem.data.column_A += matchedItem.stok_awal;
            rowItem.data.column_B += matchedItem.stok_diterima;
            rowItem.data.column_C += matchedItem.stok_keluar;
            // rowItem.data.column_D += matchedItem.stok_kadaluarsa;
            rowItem.data.column_E += matchedItem.stok_kadaluarsa;
            rowItem.data.column_F += matchedItem.stok_selisih_fisik;
            rowItem.data.column_G += matchedItem.stock_on_hand;
            rowItem.data.tgl_kadaluarsa += matchedItem.inv_string_ed_on_hand;
            // rowItem.data.column_G += matchedItem.stok_akhir;
            // rowItem.data.column_G += matchedItem.stok_akhir_botol;
            // rowItem.data.paket_keluar_reguler +=
            //   matchedItem.paket_keluar_reguler;
            // rowItem.data.stok_keluar_reguler += matchedItem.stok_keluar_reguler;
            rowItem.data.package_on_hand += matchedItem.package_on_hand;
            rowItem.data.stock_on_hand += matchedItem.stock_on_hand;
          });
        }
      });
    }
  }
  constructFinalDataInventory() {
    let orderMultiplier = 3;
    switch (this.staff.role) {
      case ENUM.USER_ROLE.RR_STAFF:
        orderMultiplier = this.report.meta.upk.orderMultiplier;
        break;
      case ENUM.USER_ROLE.PHARMA_STAFF:
        orderMultiplier = this.report.meta.upk.orderMultiplier;
        break;
      case ENUM.USER_ROLE.LAB_STAFF:
        orderMultiplier = this.report.meta.upk.orderMultiplier;
        break;
      case ENUM.USER_ROLE.SUDIN_STAFF:
        if (this.filter && this.filter.upkId) {
          orderMultiplier = this.report.meta.upk.orderMultiplier;
        } else {
          orderMultiplier = this.report.meta.sudinKabKota.orderMultiplier;
        }
        break;
      case ENUM.USER_ROLE.PROVINCE_STAFF:
        if (this.filter && this.filter.upkId) {
          orderMultiplier = this.report.meta.upk.orderMultiplier;
        } else if (this.filter && this.filter.sudinKabKotaId) {
          orderMultiplier = this.report.meta.sudinKabKota.orderMultiplier;
        } else {
          orderMultiplier = this.report.meta.province.orderMultiplier;
        }
        break;
      case ENUM.USER_ROLE.MINISTRY_STAFF:
        if (this.filter && this.filter.upkId) {
          orderMultiplier = this.report.meta.upk.orderMultiplier;
        } else if (this.filter && this.filter.sudinKabKotaId) {
          orderMultiplier = this.report.meta.sudinKabKota.orderMultiplier;
        } else if (this.filter && this.filter.provinceId) {
          orderMultiplier = this.report.meta.province.orderMultiplier;
        } else {
          orderMultiplier = 12;
        }
        break;
      default:
        orderMultiplier = 3;
    }
    _.forEach(this.inventoryTable.content, brandItem => {
      const requiredStockPackage =
        brandItem.data.column_C * orderMultiplier -
        brandItem.data.stock_on_hand;

      if (requiredStockPackage > 0) {
        brandItem.data.column_H = requiredStockPackage;
      }
      if (
        !brandItem.data.tgl_kadaluarsa ||
        brandItem.data.tgl_kadaluarsa == "null"
      ) {
        brandItem.data.tgl_kadaluarsa = "-";
      }
      // delete brandItem.data.paket_keluar_reguler;
      // delete brandItem.data.stok_keluar_reguler;
      // delete brandItem.data.package_on_hand;
      // delete brandItem.data.stock_on_hand;
    });
  }
  async loadBrandReportData() {
    let result = await sequelize.query(
      Lbpha2Service.inventoryQueryBuilderNonARV(
        this.filter.month,
        this.filter.year,
        this.staff,
        Object.assign(
          {
            columnEntityName: this.entityBaseId,
            columnEntityValue: this.entityBaseValue
          },
          this.filter
        ),
        this.generateEntityRole()
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          month: this.filter.month + 1,
          year: this.filter.year,
          entityBaseValue: this.entityBaseValue
        }
      }
    );
    // console.log("result: ", result);
    this.assignResultStockToContent(result, this.inventoryTable);
    this.constructFinalDataInventory();
  }
  // end of stok ops

  getReport() {
    delete this.registerLab.content;
    delete this.registerLab.footer;
    return this.registerLab;
  }
};
