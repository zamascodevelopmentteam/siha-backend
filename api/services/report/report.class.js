const _ = require("lodash");
const moment = require("moment");
const path = require("path");
const XLSX = require("xlsx");

const sequelize = require("../../../config/database");
const ENUM = require("../../../config/enum");

const Upk = require("../../models/Upk");
const SudinKabKota = require("../../models/SudinKabKota");
const Province = require("../../models/Province");
const CentralMinistry = require("../../models/CentralMinistry");

const VariableService = require("../variable.service")();

const SheetJS = require("../../libraries/sheetJS.class");

module.exports = class ReportClass {
  constructor(staff, req, month, year) {
    this.staff = staff;
    this.filter = {
      month: new Date().getMonth(),
      year: new Date().getFullYear(),
      upkId: staff.upkId
    };
    if (month && year) {
      this.filter.month = parseInt(month) - 1;
      this.filter.year = parseInt(year);
    }
    this.filter = Object.assign(
      this.filter,
      req.query
        ? _.pick(req.query, ["upkId", "sudinKabKotaId", "provinceId"])
        : {}
    );

    this.setEntityRole(this.staff.role);
    this.logging = false;
    this.report = {
      meta: {
        title: "",
        upk: {},
        province: {},
        sudinKabKota: {},
        tanggal: moment()
          .local()
          .format("DD-MM-YYYY HH:mm"),
        bulan: moment().format("MMMM"),
        tahun: moment().format("YYYY"),
        staff: staff
      },
      content: {},
      footer: {}
    };
  }

  getDefaultFilter() {
    return this.filter;
  }

  setReportTitle(title) {
    this.report.meta.title = title;
  }

  setFilter(monthIndex, yearIndex) {
    this.filter.month = monthIndex;
    this.filter.year = yearIndex;
  }

  setLogging(logging) {
    if (logging !== false) this.logging = console.log;
  }

  setEntityRole(userRole) {
    this.entityBaseId = "upk_id";
    this.entityBaseValue = 0;

    switch (userRole) {
      case ENUM.USER_ROLE.RR_STAFF:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
        break;
      case ENUM.USER_ROLE.PHARMA_STAFF:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
        break;
      case ENUM.USER_ROLE.LAB_STAFF:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
        break;
      case ENUM.USER_ROLE.SUDIN_STAFF:
        if (this.filter && this.filter.upkId) {
          this.entityBaseId = "upk_id";
          this.entityBaseValue = this.filter.upkId;
        } else {
          this.entityBaseId = "sudin_kab_kota_id";
          this.entityBaseValue = this.staff.sudinKabKotaId;
        }

        break;
      case ENUM.USER_ROLE.PROVINCE_STAFF:
        if (this.filter && this.filter.upkId) {
          this.entityBaseId = "upk_id";
          this.entityBaseValue = this.filter.upkId;
        } else if (this.filter && this.filter.sudinKabKotaId) {
          this.entityBaseId = "sudin_kab_kota_id";
          this.entityBaseValue = this.filter.sudinKabKotaId;
        } else {
          this.entityBaseId = "province_id";
          this.entityBaseValue = this.staff.provinceId;
        }
        break;
      case ENUM.USER_ROLE.MINISTRY_STAFF:
        if (this.filter && this.filter.upkId) {
          this.entityBaseId = "upk_id";
          this.entityBaseValue = this.filter.upkId;
        } else if (this.filter && this.filter.sudinKabKotaId) {
          this.entityBaseId = "sudin_kab_kota_id";
          this.entityBaseValue = this.filter.sudinKabKotaId;
        } else if (this.filter && this.filter.provinceId) {
          this.entityBaseId = "province_id";
          this.entityBaseValue = this.filter.provinceId;
        } else {
          this.entityBaseId = "";
          this.entityBaseValue = "";
        }
        break;
      default:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
    }
  }

  generateEntityRole(tableName, preCondition, postCondition) {
    let str =
      (tableName ? tableName + "." : "") +
      (this.entityBaseId ? this.entityBaseId : "");
    if (!this.entityBaseId) {
      str = "";
    } else {
      str =
        (preCondition || "") +
        " " +
        str +
        " = :entityBaseValue " +
        (postCondition || "");
    }
    return str;
  }

  async initiateMetaReport() {
    let upkId;
    let sudinKabKotaId;
    let provinceId;

    if (this.report.meta.province.id) {
      return;
    }
    console.log("calling initiateMetaReport...");

    if (this.staff.role === ENUM.USER_ROLE.MINISTRY_STAFF) {
      if (this.filter && this.filter.upkId) {
        upkId = this.filter.upkId;
      } else if (this.filter && this.filter.sudinKabKotaId) {
        sudinKabKotaId = this.filter.sudinKabKotaId;
      } else if (this.filter && this.filter.provinceId) {
        provinceId = this.filter.provinceId;
      }
    }

    if (this.staff.role === ENUM.USER_ROLE.PROVINCE_STAFF) {
      if (this.filter && this.filter.upkId) {
        upkId = this.filter.upkId;
      } else if (this.filter && this.filter.sudinKabKotaId) {
        sudinKabKotaId = this.filter.sudinKabKotaId;
      } else {
        provinceId = this.staff.provinceId;
      }
    }

    if (this.staff.role === ENUM.USER_ROLE.SUDIN_STAFF) {
      if (this.filter && this.filter.upkId) {
        upkId = this.filter.upkId;
      } else {
        sudinKabKotaId = this.staff.sudinKabKotaId;
      }
    }

    if (
      [
        ENUM.USER_ROLE.LAB_STAFF,
        ENUM.USER_ROLE.RR_STAFF,
        ENUM.USER_ROLE.PHARMA_STAFF
      ].includes(this.staff.role)
    ) {
      upkId = this.staff.upkId;
    }

    if (upkId) {
      const upk = await Upk.findByPk(upkId, {
        include: [
          {
            model: SudinKabKota,
            as: "sudinKabKota",
            required: true,
            include: [{ model: Province, required: true }]
          }
        ]
      });
      if (upk) {
        this.report.meta.upk = _.omit(upk.toJSON(), ["sudinKabKota"]);
        this.report.meta.sudinKabKota = _.omit(upk.sudinKabKota.toJSON(), [
          "province"
        ]);
        this.report.meta.province = upk.sudinKabKota.province;
      }
    } else if (sudinKabKotaId) {
      const sudin = await SudinKabKota.findByPk(sudinKabKotaId, {
        include: [{ model: Province, required: true }]
      });
      if (sudin) {
        this.report.meta.sudinKabKota = _.omit(sudin.toJSON(), ["province"]);
        this.report.meta.province = sudin.province;
      }
    } else if (provinceId) {
      const province = await Province.findByPk(provinceId);
      if (province) {
        this.report.meta.province = province;
      }
    }
  }

  getDefaultReport() {
    return this.report;
  }

  getDefaultQueryOption() {
    return Object.assign(
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          month: this.filter.month + 1,
          year: this.filter.year,
          entityBaseValue: this.entityBaseValue
        }
        // logging: console.log
      },
      { logging: this.logging ? this.logging : undefined }
    );
  }

  getDefaultReportMeta() {
    return this.report.meta;
  }

  createSheetJSInstance(ws) {
    const fileName = `${this.report.meta.title}_${moment()
      .local()
      .format("DD-MM-YYYY HH_mm")}.xlsx`;
    const filePath = path.join(VariableService.getTmpDownloadPath(), fileName);

    this.sheetJS = new SheetJS({
      title: this.report.meta.title,
      filePath: filePath
    });
    if (ws) {
      this.sheetJS.addWorksheetData(ws);
    }
    return this.sheetJS;
  }

  assignMetaReportSheetJS() {
    const metaReport = this.report.meta;
    // please read at https://github.com/SheetJS/sheetjs for more
    this.sheetJS.setCellValue("A1", "Laporan Bulanan");
    this.sheetJS.setCellValue("A2", metaReport.title);
    this.sheetJS.setCellValue("A4", "Nama UPK");
    this.sheetJS.setCellValue("B4", metaReport.upk ? metaReport.upk.name : "-");
    this.sheetJS.setCellValue("A5", "Nama Kab/Kota");
    this.sheetJS.setCellValue(
      "B5",
      metaReport.sudinKabKota ? metaReport.sudinKabKota.name : "-"
    );
    this.sheetJS.setCellValue("A6", "Nama Provinsi");
    this.sheetJS.setCellValue(
      "B6",
      metaReport.province ? metaReport.province.name : "-"
    );

    this.sheetJS.setCellValue("D4", "Bulan");
    this.sheetJS.setCellValue("E4", moment().format("MMMM"));
    this.sheetJS.setCellValue("D5", "Tahun");
    this.sheetJS.setCellValue("E5", moment().format("YYYY"));
  }

  generateSheetJSFile(ws) {
    return this.sheetJS.generate();
  }
};
