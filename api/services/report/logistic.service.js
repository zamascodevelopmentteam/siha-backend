const sequelize = require("../../../config/database");
const Sequelize = require("sequelize");
const _ = require("lodash");
const Op = Sequelize.Op;

const logisticSevice = () => {
  const getJumlahIms = async params => {
    let {
      page,
      limit,
      keyword,
      monthPeriode,
      sudinKabKotaId,
      provinceId
    } = params;
    const offset = limit * page;
    const monthParam = new Date(monthPeriode).getMonth() + 1;
    const yearParam = new Date(monthPeriode).getFullYear();
    const periodeParam = monthPeriode
      ? " WHERE date_part('month', visits.visit_date) = " +
        monthParam +
        " AND date_part('year', visits.visit_date) = " +
        yearParam
      : "";
    var whereEntity = "";
    if (sudinKabKotaId) {
      whereEntity = ' WHERE "sudinKabKota".id = ' + sudinKabKotaId;
    } else if (provinceId) {
      whereEntity = ' WHERE "provinces".id = ' + provinceId;
    }
    let query =
      `SELECT
      count(ims_visits.id) total,
      upk.id AS "upkId",
      upk.name AS "upkName"
  FROM
      upk AS "upk"
      LEFT JOIN patients ON upk.id = patients.upk_id
      LEFT JOIN (
          SELECT
              visits.id,
              patient_id
          FROM
              visits
              JOIN test_ims ON test_ims.visit_id = visits.id
          ` +
      periodeParam +
      `) AS ims_visits ON ims_visits.patient_id = patients.id
      LEFT JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = "upk".sudin_kab_kota_id
      LEFT JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
      ` +
      whereEntity +
      `
  GROUP BY
      "upkId",
      "upkName"
  ORDER BY
      total DESC,
      "upkId"`;
    //   LIMIT ` +
    //   limit +
    //   ` OFFSET ` +
    //   offset;

    const ImsList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return ImsList;
  };

  const getKetersediaanStokProv = async provinceId => {
    let query =
      `SELECT
    lbph2.*,
    COALESCE(inTransit. "totalIntransit", 0) AS "totalIntransit",
    CASE WHEN (lbph2. "stokKeluarReguler") > 0 THEN
      FLOOR(lbph2. "stockOnHand" / lbph2. "stokKeluarReguler")
    WHEN (lbph2. "stokKeluarReguler") = 0 THEN
      lbph2. "orderMultiplier" + 1
    END AS "monthAvailability"
  FROM (
    SELECT
      "provinces"."name" AS "entitasName",
      "provinces".id AS "provinceId",
      "provinces".order_multiplier AS "orderMultiplier",
      lbpha2_medicine_usage.code_name AS "codeName",
      lbpha2_medicine_usage.name AS "medicineName",
      SUM(lbpha2_medicine_usage.stock_on_hand) AS "stockOnHand",
      SUM(lbpha2_medicine_usage.stok_keluar_reguler) AS "stokKeluarReguler",
      lbpha2_medicine_usage.package_unit_type AS "packageUnitType",
      lbpha2_medicine_usage.stock_unit_type AS "stockUnitType",
      lbpha2_medicine_usage.package_multiplier AS "packageMultiplier"
    FROM
      lbpha2_medicine_usage
      JOIN upk ON upk.id = lbpha2_medicine_usage.upk_id
      JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = upk.sudin_kab_kota_id
      JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
    GROUP BY
      "entitasName",
      "provinceId",
      "orderMultiplier",
      "codeName",
      "medicineName",
      "packageUnitType",
      "stockUnitType",
      "packageMultiplier"
    ORDER BY
      "entitasName",
      "codeName") AS lbph2
    left JOIN (
      SELECT
        sum(distribution_plans_items.package_quantity) AS "totalIntransit",
        distribution_plans.sudin_kab_kota_id AS "sudinKabKotaId",
        distribution_plans.province_id AS "provinceId"
      FROM
        distribution_plans_items
        JOIN distribution_plans ON distribution_plans_items.distribution_plan_id = distribution_plans.id
      WHERE
        distribution_plans.plan_status = 'PROCESSED'
      GROUP BY
        distribution_plans.sudin_kab_kota_id,
        distribution_plans.province_id) AS inTransit ON inTransit. "provinceId" = lbph2. "provinceId"
  WHERE
    lbph2. "provinceId" = ` + provinceId;
    const ketersediaanList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return ketersediaanList;
  };

  const getKetersediaanStokSudin = async sudinKabKotaId => {
    let query =
      `SELECT
    lbph2.*,
    COALESCE(inTransit. "totalIntransit", 0) AS "totalIntransit",
    CASE WHEN (lbph2. "stokKeluarReguler") > 0 THEN
      FLOOR(lbph2. "stockOnHand" / lbph2. "stokKeluarReguler")
    WHEN (lbph2. "stokKeluarReguler") = 0 THEN
      lbph2. "orderMultiplier" + 1
    END AS "monthAvailability"
  FROM (
    SELECT
      "provinces"."name" AS "provinceName",
      "provinces".id AS "provinceId",
      "sudinKabKota"."name" AS "entitasName",
      "sudinKabKota".id AS "sudinKabKotaId",
      "sudinKabKota".order_multiplier AS "orderMultiplier",
      lbpha2_medicine_usage.code_name AS "codeName",
      lbpha2_medicine_usage.name AS "medicineName",
      SUM(lbpha2_medicine_usage.stock_on_hand) AS "stockOnHand",
      SUM(lbpha2_medicine_usage.stok_keluar_reguler) AS "stokKeluarReguler",
      lbpha2_medicine_usage.package_unit_type AS "packageUnitType",
      lbpha2_medicine_usage.stock_unit_type AS "stockUnitType",
      lbpha2_medicine_usage.package_multiplier AS "packageMultiplier"
    FROM
      lbpha2_medicine_usage
      JOIN upk ON upk.id = lbpha2_medicine_usage.upk_id
      JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = upk.sudin_kab_kota_id
      JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
    GROUP BY
      "provinceName",
      "provinceId",
      "entitasName",
      "sudinKabKotaId",
      "orderMultiplier",
      "codeName",
      "medicineName",
      "packageUnitType",
      "stockUnitType",
      "packageMultiplier"
    ORDER BY
      "provinceName",
      "entitasName",
      "codeName") AS lbph2
    left JOIN (
      SELECT
        sum(distribution_plans_items.package_quantity) AS "totalIntransit",
        distribution_plans.sudin_kab_kota_id AS "sudinKabKotaId",
        distribution_plans.province_id AS "provinceId"
      FROM
        distribution_plans_items
        JOIN distribution_plans ON distribution_plans_items.distribution_plan_id = distribution_plans.id
      WHERE
        distribution_plans.plan_status = 'PROCESSED'
      GROUP BY
        distribution_plans.sudin_kab_kota_id,
        distribution_plans.province_id) AS inTransit ON inTransit. "sudinKabKotaId" = lbph2. "sudinKabKotaId"
  WHERE
    lbph2. "sudinKabKotaId" = ` + sudinKabKotaId;
    const ketersediaanList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return ketersediaanList;
  };

  const getKetersediaanStokUpk = async upkId => {
    let query =
      `SELECT
    lbph2.*,
    COALESCE(inTransit. "totalIntransit", 0) AS "totalIntransit",
    CASE WHEN (lbph2. "stokKeluarReguler") > 0 THEN
      FLOOR(lbph2. "stockOnHand" / lbph2. "stokKeluarReguler")
    WHEN (lbph2. "stokKeluarReguler") = 0 THEN
      lbph2. "orderMultiplier" + 1
    END AS "monthAvailability"
  FROM (
    SELECT
      upk. "name" AS "entitasName",
      upk. id AS "upkId",
      lbpha2_medicine_usage.code_name AS "codeName",
      lbpha2_medicine_usage.name AS "medicineName",
      upk.order_multiplier AS "orderMultiplier",
      lbpha2_medicine_usage.stock_on_hand AS "stockOnHand",
      lbpha2_medicine_usage.stok_keluar_reguler AS "stokKeluarReguler",
      lbpha2_medicine_usage.package_unit_type AS "packageUnitType",
      lbpha2_medicine_usage.stock_unit_type AS "stockUnitType",
      lbpha2_medicine_usage.package_multiplier AS "packageMultiplier"
    FROM
      lbpha2_medicine_usage
      JOIN upk ON upk.id = lbpha2_medicine_usage.upk_id
      JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = upk.sudin_kab_kota_id
      JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id) AS lbph2
    LEFT JOIN (
      SELECT
        sum(distribution_plans_items.package_quantity) AS "totalIntransit",
        distribution_plans.upk_id AS "upkId",
        distribution_plans.sudin_kab_kota_id AS "sudinKabKotaId",
        distribution_plans.province_id AS "provinceId"
      FROM
        distribution_plans_items
        JOIN distribution_plans ON distribution_plans_items.distribution_plan_id = distribution_plans.id
      WHERE
        distribution_plans.plan_status = 'PROCESSED'
      GROUP BY
        distribution_plans.upk_id,
        distribution_plans.sudin_kab_kota_id,
        distribution_plans.province_id) AS inTransit ON inTransit. "upkId" = lbph2. "upkId"
  WHERE
    lbph2. "upkId" = ` +
      upkId +
      `
  ORDER BY
    "entitasName",
    "codeName"`;

    const ketersediaanList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return ketersediaanList;
  };

  return {
    getJumlahIms,
    getKetersediaanStokProv,
    getKetersediaanStokSudin,
    getKetersediaanStokUpk
  };
};

module.exports = logisticSevice;
