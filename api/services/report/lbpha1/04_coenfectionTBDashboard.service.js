const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const CoenfectionTBDasboardService = () => {
  // LBPHA Lembar 1 NO 4.1
  const getPatientsTB = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_tb IS NOT NULL AND ( treatments.status_tb = :statusTb_1 OR treatments.status_tb = :statusTb_2 OR treatments.status_tb = :statusTb_3 ) " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTb_1: ENUM.STATUS_TB.NEGATIF_TB,
          statusTb_2: ENUM.STATUS_TB.TIDAK_TAU_STATUS,
          statusTb_3: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsTBKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_tb IS NOT NULL AND ( treatments.status_tb = :statusTb_1 OR treatments.status_tb = :statusTb_2 OR treatments.status_tb = :statusTb_3 ) " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTb_1: ENUM.STATUS_TB.NEGATIF_TB,
          statusTb_2: ENUM.STATUS_TB.TIDAK_TAU_STATUS,
          statusTb_3: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 4.2
  const getPatientsTBHiv = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_tb IS NOT NULL AND DATE_PART('month', treatments.tgl_pengobatan_tb) = :thisMonthIndex AND DATE_PART('year', treatments.tgl_pengobatan_tb) = :thisYearIndex " +
          " ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsTBHivKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_tb IS NOT NULL AND DATE_PART('month', treatments.tgl_pengobatan_tb) = :thisMonthIndex AND DATE_PART('year', treatments.tgl_pengobatan_tb) = :thisYearIndex " +
          " "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 4.3
  const getPatientsTbHivArv = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_tb IS NOT NULL AND DATE_PART('month', treatments.tgl_pengobatan_tb) = :thisMonthIndex AND DATE_PART('year', treatments.tgl_pengobatan_tb) = :thisYearIndex " +
          " AND treatments.obat_arv_1 IS NOT NULL ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsTbHivArvKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_tb IS NOT NULL AND DATE_PART('month', treatments.tgl_pengobatan_tb) = :thisMonthIndex AND DATE_PART('year', treatments.tgl_pengobatan_tb) = :thisYearIndex " +
          " AND treatments.obat_arv_1 IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getPatientsTB,
    getPatientsTBKelPopulasi,
    getPatientsTBHiv,
    getPatientsTBHivKelPopulasi,
    getPatientsTbHivArv,
    getPatientsTbHivArvKelPopulasi
  };
};

module.exports = CoenfectionTBDasboardService;
