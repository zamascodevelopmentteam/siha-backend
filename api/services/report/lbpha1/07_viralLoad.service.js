const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const ViralLoadDasboardService = () => {
  // LBPHA Lembar 1 NO 7.1
  const getPatientsVL = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardLabTestQueryBuilder(
        ENUM.VISIT_ACT_TYPE.VL_CD4,
        entityBaseId,
        entityBaseValue,
        " test_vl_cd4.test_vl_cd4_type IS NOT NULL " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsVLKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiTestLabDashboardQueryBuilder(
        ENUM.VISIT_ACT_TYPE.VL_CD4,
        entityBaseId,
        entityBaseValue,
        // " test_vl_cd4.test_vl_cd4_type IS NOT NULL " +
        " test_vl_cd4.test_vl_cd4_type = :testType " +
          " AND DATE_PART('month', test_vl_cd4.date) = :thisMonthIndex AND DATE_PART('YEAR', test_vl_cd4.date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          testType: ENUM.VL_TEST_TYPE.VL
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 7.2
  const getPatientsOverVirus = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardLabTestQueryBuilder(
        ENUM.VISIT_ACT_TYPE.VL_CD4,
        entityBaseId,
        entityBaseValue,
        " test_vl_cd4.test_vl_cd4_type = :testLab AND test_vl_cd4.hasil_test_vl_cd4 <> 'INVALID / ERROR' AND test_vl_cd4.hasil_lab_satuan = 'KOPI/ml'  " +
          " AND DATE_PART('month', test_vl_cd4.date) = :thisMonthIndex AND DATE_PART('YEAR', test_vl_cd4.date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          testLab: ENUM.VL_TEST_TYPE.VL,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsOverVirusKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiTestLabDashboardQueryBuilder(
        ENUM.VISIT_ACT_TYPE.VL_CD4,
        entityBaseId,
        entityBaseValue,
        " test_vl_cd4.test_vl_cd4_type = :testLab AND test_vl_cd4.hasil_test_vl_cd4 <= '< 1000' AND test_vl_cd4.hasil_lab_satuan = 'KOPI/ml' " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          testLab: ENUM.VL_TEST_TYPE.VL,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getPatientsVL,
    getPatientsVLKelPopulasi,
    getPatientsOverVirus,
    getPatientsOverVirusKelPopulasi
  };
};

module.exports = ViralLoadDasboardService;
