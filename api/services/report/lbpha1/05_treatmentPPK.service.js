const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const TreatmentPPKService = () => {
  // LBPHA Lembar 1 NO 5.1
  const getNewPatientsPPK = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.ppk IS TRUE AND DATE_PART('month', treatments.tgl_pemberian_ppk) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex " +
          "  ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getNewPatientsPPKKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.ppk IS TRUE AND DATE_PART('month', treatments.tgl_pemberian_ppk) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex " +
          "  "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 5.2
  const getPatientsPPK = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.ppk IS TRUE " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsPPKKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.ppk IS TRUE " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getNewPatientsPPK,
    getNewPatientsPPKKelPopulasi,
    getPatientsPPK,
    getPatientsPPKKelPopulasi
  };
};

module.exports = TreatmentPPKService;
