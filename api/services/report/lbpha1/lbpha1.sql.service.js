const moment = require("moment");
const _ = require("lodash");

const ENUM = require("../../../../config/enum");
const VarServive = require("../../variable.service")();
const LoggerServive = require("../../logger.service")();

const lbpha1Service = () => {
  const visitDashboardQueryBuilder = (
    entityBaseId,
    entityBaseValue,
    visitWhereParam,
    patientWhereParam,
    visitPatientWhereParam,
    extraVar
  ) => {
    let visitWhereString = visitWhereParam ? " WHERE " + visitWhereParam : "";
    let patientWhereString = patientWhereParam
      ? " AND " + patientWhereParam
      : "";
    let visitPatientWhereString = visitPatientWhereParam
      ? " WHERE " + visitPatientWhereParam
      : " WHERE treatment_id IS NOT NULL ";

    let entityFilter = entityBaseId
      ? ` ${entityBaseId} =  ${entityBaseValue} AND `
      : ``;

    let str =
      `SELECT age, COUNT(age) FROM(SELECT ` +
      ` patients.id, age ` +
      ` FROM ` +
      ` (SELECT id, (DATE_PART('year', NOW()) - DATE_PART('year', date_birth)) AS age, date_birth, gender, meta FROM
           (SELECT patients.meta, patients.id, status_patient, date_birth, gender, upk.id AS upk_id, upk.sudin_kab_kota_id, upk.province_id
          FROM patients
          JOIN upk ON upk.id = patients.upk_id ) patients WHERE ${entityFilter} patients.status_patient != '${ENUM.STATUS_PATIENT.HIV_NEGATIF}' AND date_birth IS NOT NULL ` +
      patientWhereString +
      ` ) as patients` +
      ` LEFT JOIN (` +
      ` SELECT visits.id, visits.patient_id, treatments.id as treatment_id, profilaksis.id as profilaksis_id ` +
      ` FROM visits ` +
      ` LEFT JOIN treatments ON visits.id = treatments.visit_id` +
      ` LEFT JOIN profilaksis ON visits.id = profilaksis.visit_id` +
      (extraVar && extraVar.join ? extraVar.join : ``) +
      visitWhereString +
      (extraVar && extraVar.where ? extraVar.where : ``) +
      `
        GROUP BY visits.id, visits.patient_id, treatment_id, profilaksis_id
      ) AS visits on visits.patient_id = patients.id ` +
      visitPatientWhereString +
      ` GROUP BY patients.id, age ` +
      ` ) as treatment_visits ` +
      ` GROUP by age ` +
      ` ORDER by age `;

    return str;
  };

  const kelPopulasiDashboardQueryBuilder = (
    entityBaseId,
    entityBaseValue,
    visitWhereParam,
    patientWhereParam,
    visitPatientWhereParam
  ) => {
    let visitWhereString = visitWhereParam ? " WHERE " + visitWhereParam : "";
    let patientWhereString = patientWhereParam
      ? " AND " + patientWhereParam
      : "";
    let visitPatientWhereString = visitPatientWhereParam
      ? " AND " + visitPatientWhereParam
      : " AND treatment_id IS NOT NULL ";

    let entityFilter = entityBaseId
      ? ` ${entityBaseId} =  ${entityBaseValue} AND `
      : ``;

    let str =
      `SELECT COUNT(id), kelompok_populasi FROM(SELECT ` +
      ` patients.id, kelompok_populasi ` +
      ` FROM ` +
      ` (SELECT id, gender, upk_id, sudin_kab_kota_id, province_id, meta, kelompok_populasi  FROM (
          SELECT patients.id, status_patient, date_birth, gender, upk.id AS upk_id, upk.sudin_kab_kota_id, upk.province_id, patients.meta, patients.kelompok_populasi AS kelompok_populasi
     FROM patients
     JOIN upk ON upk.id = patients.upk_id ) AS patients WHERE ${entityFilter} patients.status_patient != '${ENUM.STATUS_PATIENT.HIV_NEGATIF}' ` +
      patientWhereString +
      ` ) as patients` +
      ` LEFT JOIN (` +
      ` SELECT visits.id, patient_id, treatments.id as treatment_id, profilaksis.id as profilaksis_id ` +
      ` FROM visits ` +
      ` LEFT JOIN treatments ON visits.id = treatments.visit_id` +
      ` LEFT JOIN profilaksis ON visits.id = profilaksis.visit_id` +
      visitWhereString +
      `
	       GROUP BY visits.id, visits.patient_id, treatment_id, profilaksis_id
      ) AS visits on visits.patient_id = patients.id ` +
      ` WHERE kelompok_populasi IS NOT NULL ` +
      visitPatientWhereString +
      ` GROUP BY patients.id, kelompok_populasi ` +
      ` ) as treatment_visits ` +
      ` GROUP by kelompok_populasi `;

    return str;
  };

  const visitDashboardLabTestQueryBuilder = (
    testType,
    entityBaseId,
    entityBaseValue,
    visitWhereParam,
    patientWhereParam,
    visitPatientWhereParam
  ) => {
    let baseTable;
    let visitWhereString = visitWhereParam ? " WHERE " + visitWhereParam : "";
    let patientWhereString = patientWhereParam
      ? " AND " + patientWhereParam
      : "";
    let visitPatientWhereString = visitPatientWhereParam
      ? " WHERE " + visitPatientWhereParam
      : " WHERE test_id IS NOT NULL ";

    let entityFilter = entityBaseId
      ? ` ${entityBaseId} =  ${entityBaseValue} AND `
      : ``;

    switch (testType) {
      case ENUM.VISIT_ACT_TYPE.IMS:
        baseTable = "ims_visit_labs";
        break;
      case ENUM.VISIT_ACT_TYPE.VL_CD4:
        baseTable = "test_vl_cd4";
        break;
      default:
        baseTable = "ims_visit_labs";
    }

    let str =
      `SELECT age, COUNT(age) FROM(SELECT ` +
      ` patients.id, age ` +
      ` FROM ` +
      ` (SELECT id, (DATE_PART('year', NOW()) - DATE_PART('year', date_birth)) AS age, date_birth, gender, upk_id, sudin_kab_kota_id, province_id FROM (SELECT patients.id, status_patient, date_birth, gender, upk.id AS upk_id, upk.sudin_kab_kota_id, upk.province_id
     FROM patients
     JOIN upk ON upk.id = patients.upk_id ) AS patients WHERE ${entityFilter} patients.status_patient = '` +
      ENUM.STATUS_PATIENT.ODHA +
      `' AND date_birth IS NOT NULL ` +
      patientWhereString +
      ` ) as patients` +
      ` LEFT JOIN (` +
      ` SELECT visits.id, patient_id, ` +
      baseTable +
      `.id as test_id ` +
      ` FROM visits ` +
      ` LEFT JOIN ` +
      baseTable +
      ` ON visits.id = ` +
      baseTable +
      `.visit_id` +
      visitWhereString +
      `
        GROUP BY visits.id, visits.patient_id, test_id
      ) AS visits on visits.patient_id = patients.id ` +
      visitPatientWhereString +
      ` GROUP BY patients.id, age ` +
      ` ) as treatment_visits ` +
      ` GROUP by age ` +
      ` ORDER by age `;

    return str;
  };

  const kelPopulasiTestLabDashboardQueryBuilder = (
    testType,
    entityBaseId,
    entityBaseValue,
    visitWhereParam,
    patientWhereParam,
    visitPatientWhereParam
  ) => {
    let baseTable;
    let visitWhereString = visitWhereParam ? " WHERE " + visitWhereParam : "";
    let patientWhereString = patientWhereParam
      ? " AND " + patientWhereParam
      : "";
    let visitPatientWhereString = visitPatientWhereParam
      ? " AND " + visitPatientWhereParam
      : " AND test_id IS NOT NULL ";

    let entityFilter = entityBaseId
      ? ` ${entityBaseId} =  ${entityBaseValue} AND `
      : ``;

    switch (testType) {
      case ENUM.VISIT_ACT_TYPE.IMS:
        baseTable = "ims_visit_labs";
        break;
      case ENUM.VISIT_ACT_TYPE.VL_CD4:
        baseTable = "test_vl_cd4";
        break;
      default:
        baseTable = "ims_visit_labs";
    }

    let str =
      `SELECT COUNT(id), kelompok_populasi FROM(SELECT ` +
      ` patients.id, kelompok_populasi ` +
      ` FROM ` +
      ` (SELECT id, gender, upk_id, sudin_kab_kota_id, province_id, kelompok_populasi  FROM (SELECT patients.id, status_patient, date_birth, gender, upk.id AS upk_id, upk.sudin_kab_kota_id, upk.province_id, patients.kelompok_populasi AS kelompok_populasi
     FROM patients
     JOIN upk ON upk.id = patients.upk_id ) AS patients WHERE ${entityFilter} patients.status_patient = '${ENUM.STATUS_PATIENT.ODHA}' ` +
      patientWhereString +
      ` ) as patients` +
      ` LEFT JOIN (` +
      ` SELECT visits.id, patient_id, ` +
      baseTable +
      `.id as test_id ` +
      ` FROM visits JOIN treatments ON visits.id = treatments.visit_id ` +
      ` LEFT JOIN ` +
      baseTable +
      ` ON visits.id = ` +
      baseTable +
      `.visit_id` +
      visitWhereString +
      `
        GROUP BY visits.id, visits.patient_id, test_id
        ) AS visits on visits.patient_id = patients.id ` +
      ` WHERE kelompok_populasi IS NOT NULL ` +
      visitPatientWhereString +
      ` GROUP BY patients.id, kelompok_populasi ` +
      ` ) as treatment_visits ` +
      ` GROUP by kelompok_populasi `;

    return str;
  };

  return {
    visitDashboardQueryBuilder,
    kelPopulasiDashboardQueryBuilder,
    visitDashboardLabTestQueryBuilder,
    kelPopulasiTestLabDashboardQueryBuilder
  };
};

module.exports = lbpha1Service;
