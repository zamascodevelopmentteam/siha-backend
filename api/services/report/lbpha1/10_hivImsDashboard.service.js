const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const HivImsDashboardService = () => {
  // LBPHA Lembar 1 NO 10.1
  const getOdhaIms = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardLabTestQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        " ims_visit_labs.is_tested IS TRUE " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaImsKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiTestLabDashboardQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        " ims_visit_labs.is_tested IS TRUE " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 10.2
  const getOdhaImsPositive = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardLabTestQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        " ims_visit_labs.is_tested IS TRUE  " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaImsPositiveKelPopulasi = async (
    entityBaseId, entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiTestLabDashboardQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        " ims_visit_labs.is_tested IS TRUE " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 10.3
  const getOdhaSifilis = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardLabTestQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        " ims_visit_labs.rpr_vdrl_titer IS NOT NULL " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaSifilisKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiTestLabDashboardQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        " ims_visit_labs.rpr_vdrl_titer IS NOT NULL  " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 10.4
  const getOdhaSifilisPositive = async (
    entityBaseId, entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardLabTestQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        // " ims_visit_labs.rpr_vdrl_titer IN ('1/8', '1/4', '1/2') " +
        " ims_visit_labs.rpr_vdrl->>'result' = 'REAKTIF' " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaSifilisPositiveKelPopulasi = async (
    entityBaseId, entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiTestLabDashboardQueryBuilder(
        ENUM.VISIT_ACT_TYPE.IMS,
        entityBaseId, entityBaseValue,
        // " ims_visit_labs.rpr_vdrl_titer IN ('1/8', '1/4', '1/2') " +
        " ims_visit_labs.rpr_vdrl->>'result' = 'REAKTIF' " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getOdhaIms,
    getOdhaImsKelPopulasi,
    getOdhaImsPositive,
    getOdhaImsPositiveKelPopulasi,
    getOdhaSifilis,
    getOdhaSifilisKelPopulasi,
    getOdhaSifilisPositive,
    getOdhaSifilisPositiveKelPopulasi
  };
};

module.exports = HivImsDashboardService;
