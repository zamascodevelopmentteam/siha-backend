const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const NotificationCoupleDasboardService = () => {
  // LBPHA Lembar 1 NO 9.1
  const getPatientsOfferCouple = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.notifikasi_pasangan IS NOT NULL AND treatments.notifikasi_pasangan <> :notifCouple " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          notifCouple: ENUM.NOTIF_COUPLE.TDK_MEMENUHI,
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsOfferCoupleKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.notifikasi_pasangan IS NOT NULL AND treatments.notifikasi_pasangan <> :notifCouple " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          notifCouple: ENUM.NOTIF_COUPLE.TDK_MEMENUHI,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 9.2
  const getPatientsAcceptOfferCouple = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.notifikasi_pasangan = :notifCouple " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          notifCouple: ENUM.NOTIF_COUPLE.MENERIMA,
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsAcceptOfferCoupleKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.notifikasi_pasangan = :notifCouple " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          notifCouple: ENUM.NOTIF_COUPLE.MENERIMA,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 9.3
  const getPatientsCouple = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    let entityFilter = entityBaseId
      ? ` AND ${entityBaseId} =  ${entityBaseValue}  `
      : ``;
    let str =
      `SELECT age, COUNT(age) FROM(SELECT ` +
      ` treatment_id,age ` +
      ` FROM ` +
      ` (SELECT id FROM (SELECT patients.id, status_patient, date_birth, gender, upk.id AS upk_id, upk.sudin_kab_kota_id, upk.province_id, patients.meta
 FROM patients
 JOIN upk ON upk.id = patients.upk_id) AS patients WHERE patients.status_patient = '` +
      ENUM.STATUS_PATIENT.ODHA +
      `' ${entityFilter} ` +
      ` ) as patients` +
      ` JOIN (` +
      ` SELECT visits.id, patient_id, visit_date, check_out_date, treatments.id AS treatment_id, couples.age AS age, name ` +
      ` FROM visits ` +
      ` JOIN treatments ON visits.id = treatments.visit_id` +
      ` JOIN couples ON treatments.id = couples.treatment_id` +
      ` WHERE couples.gender = :gender AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ` +
      ` ) AS visits on visits.patient_id = patients.id ` +
      ` GROUP BY treatment_id, age ` +
      ` ) as treatment_visits ` +
      ` GROUP by age ` +
      ` ORDER by age `;

    const ageList = await sequelize.query(str, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        // notifCouple: ENUM.NOTIF_COUPLE.MENERIMA,
        gender: gender,
        thisMonthIndex: monthIndex || new Date().getMonth() + 1,
        thisYearIndex: yearIndex || new Date().getFullYear()
      }
    });

    return ageList;
  };
  const getPatientsCoupleKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    let entityFilter = entityBaseId
      ? ` AND ${entityBaseId} =  ${entityBaseValue}  `
      : ``;
    let str =
      `SELECT kelompok_populasi, COUNT(id) FROM(SELECT ` +
      ` treatment_id as id,kelompok_populasi ` +
      ` FROM ` +
      ` (SELECT id, kelompok_populasi FROM (SELECT patients.id, status_patient, date_birth, gender, upk.id AS upk_id, upk.sudin_kab_kota_id, upk.province_id, patients.meta, patients.kelompok_populasi AS kelompok_populasi
 FROM patients
 JOIN upk ON upk.id = patients.upk_id) AS patients WHERE patients.status_patient = '` +
      ENUM.STATUS_PATIENT.ODHA +
      `' ${entityFilter} ` +
      ` ) as patients` +
      ` JOIN (` +
      ` SELECT visits.id, patient_id, visit_date, check_out_date, treatments.id AS treatment_id, couples.age AS age, name ` +
      ` FROM visits ` +
      ` JOIN treatments ON visits.id = treatments.visit_id` +
      ` JOIN couples ON treatments.id = couples.treatment_id` +
      ` WHERE DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ` +
      ` ) AS visits on visits.patient_id = patients.id ` +
      ` GROUP BY treatment_id, kelompok_populasi ` +
      ` ) as treatment_visits ` +
      ` GROUP by kelompok_populasi `;

    const kelPopulasiList = await sequelize.query(str, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        // notifCouple: ENUM.NOTIF_COUPLE.MENERIMA,
        thisMonthIndex: monthIndex || new Date().getMonth() + 1,
        thisYearIndex: yearIndex || new Date().getFullYear()
      }
    });

    return kelPopulasiList;
  };

  return {
    getPatientsOfferCouple,
    getPatientsOfferCoupleKelPopulasi,
    getPatientsAcceptOfferCouple,
    getPatientsAcceptOfferCoupleKelPopulasi,
    getPatientsCouple,
    getPatientsCoupleKelPopulasi
  };
};

module.exports = NotificationCoupleDasboardService;
