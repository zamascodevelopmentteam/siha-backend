const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const TherapyTPTService = () => {
  // LBPHA Lembar 1 NO 6.1
  const getPatientsPPINH = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.status_tb_tpt <> :statusTbTpt AND DATE_PART('month', treatments.tgl_pemberian_tb_tpt) = :thisMonthIndex " +
          "  ",
        " patients.gender = :gender AND DATE_PART('month',(patients.meta->>'convertToODHADate')::DATE) = :thisMonthIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTbTpt: ENUM.STATUS_TB_TPT.TDK_DIBERIKAN,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1, thisYearIndex: yearIndex || new Date().getFullYear(),
        }
      }
    );

    return ageList;
  };
  const getPatientsPPINHKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.status_tb_tpt <> :statusTbTpt AND DATE_PART('month', treatments.tgl_pemberian_tb_tpt) = :thisMonthIndex " +
          "  ",
        " DATE_PART('month',(patients.meta->>'convertToODHADate')::DATE) = :thisMonthIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTbTpt: ENUM.STATUS_TB_TPT.TDK_DIBERIKAN,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1, thisYearIndex: yearIndex || new Date().getFullYear(),
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 6.2
  const getOldPatientsPPINH = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.status_tb_tpt <> :statusTbTpt AND DATE_PART('month', treatments.tgl_pemberian_tb_tpt) = :thisMonthIndex " +
          "  ",
        " patients.gender = :gender AND DATE_PART('month',(patients.meta->>'convertToODHADate')::DATE) < :thisMonthIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTbTpt: ENUM.STATUS_TB_TPT.TDK_DIBERIKAN,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1, thisYearIndex: yearIndex || new Date().getFullYear(),
        }
      }
    );

    return ageList;
  };
  const getOldPatientsPPINHKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.status_tb_tpt <> :statusTbTpt AND DATE_PART('month', treatments.tgl_pemberian_tb_tpt) = :thisMonthIndex " +
          "  ",
        " DATE_PART('month',(patients.meta->>'convertToODHADate')::DATE) < :thisMonthIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTbTpt: ENUM.STATUS_TB_TPT.TDK_DIBERIKAN,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1, thisYearIndex: yearIndex || new Date().getFullYear(),
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 6.3
  const getPatientsRifampentine = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.status_tb_tpt = :statusTbTpt AND DATE_PART('month', treatments.tgl_pemberian_tb_tpt) = :thisMonthIndex " +
          "  ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTbTpt: ENUM.STATUS_TB_TPT.RIFAMPETINE,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1, thisYearIndex: yearIndex || new Date().getFullYear(),
        }
      }
    );

    return ageList;
  };
  const getPatientsRifampentineKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.status_tb_tpt = :statusTbTpt AND DATE_PART('month', treatments.tgl_pemberian_tb_tpt) = :thisMonthIndex " +
          "  "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTbTpt: ENUM.STATUS_TB_TPT.RIFAMPETINE,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1, thisYearIndex: yearIndex || new Date().getFullYear(),
        }
      }
    );

    return kelPopulasiList;
  };
  return {
    getPatientsPPINH,
    getPatientsPPINHKelPopulasi,
    getOldPatientsPPINH,
    getOldPatientsPPINHKelPopulasi,
    getPatientsRifampentine,
    getPatientsRifampentineKelPopulasi
  };
};

module.exports = TherapyTPTService;
