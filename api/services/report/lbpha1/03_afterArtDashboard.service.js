const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const AfterArtDashboardService = () => {
  // LBPHA Lembar 1 NO 3.1
  const getOdhaDeaths = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    // const ageList = await sequelize.query(
    //   Lbpha1SQLService.visitDashboardQueryBuilder(
    //     entityBaseId,
    //     entityBaseValue,
    //     " treatments.akhir_follow_up = :akhirFollowUp " +
    //       " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
    //     " patients.gender = :gender "
    //   ),
    //   {
    //     type: sequelize.QueryTypes.SELECT,
    //     replacements: {
    //       gender: gender,
    //       akhirFollowUp: ENUM.AKHIR_FOLLOW_UP.MENINGGAL,
    //       thisMonthIndex: monthIndex || new Date().getMonth() + 1,
    //       thisYearIndex: yearIndex || new Date().getFullYear()
    //     }
    //   }
    // );
    
    // format 
    // [{age: 99, count: 99}, {age: 100, count: 100}]

    const year = yearIndex || new Date().getFullYear();
    const month = monthIndex || new Date().getMonth() + 1;
    let where =  ``;

    if (entityBaseId && entityBaseValue) {
      if (entityBaseId != 'province_id') {
        where = ` AND ${entityBaseId} = ${entityBaseValue} `;
      } else {
        where = ` AND sudin_kab_kota.${entityBaseId} = ${entityBaseValue} `;
      }
    }

    const query = `
      SELECT
        ( DATE_PART( 'year', NOW( ) ) - DATE_PART( 'year', date_birth ) ) AS age,
        COUNT ( * ) AS COUNT 
      FROM
        patients
        JOIN upk ON upk.ID = patients.upk_id
        JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
        JOIN provinces ON provinces.ID = sudin_kab_kota.province_id 
      WHERE
        status_patient = 'ODHA' 
        AND gender = '${gender}' 
        AND DATE_PART( 'year', tgl_meninggal ) = ${year} 
        AND DATE_PART( 'month', tgl_meninggal ) = ${month}
        ${where}
      GROUP BY
        ( DATE_PART( 'year', NOW( ) ) - DATE_PART( 'year', date_birth ) )
    `;
    const ageList = await sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT
    });

    return ageList;
  };
  const getOdhaDeathsKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    // const kelPopulasiList = await sequelize.query(
    //   Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
    //     entityBaseId,
    //     entityBaseValue,
    //     " treatments.akhir_follow_up = :akhirFollowUp " +
    //       " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
    //   ),
    //   {
    //     type: sequelize.QueryTypes.SELECT,
    //     replacements: {
    //       akhirFollowUp: ENUM.AKHIR_FOLLOW_UP.MENINGGAL,
    //       thisMonthIndex: monthIndex || new Date().getMonth() + 1,
    //       thisYearIndex: yearIndex || new Date().getFullYear()
    //     }
    //   }
    // );

    // format
    // [{ count: '1', kelompok_populasi: [ 'WPS' ] }]

    const year = yearIndex || new Date().getFullYear();
    const month = monthIndex || new Date().getMonth() + 1;
    let where =  ``;

    if (entityBaseId && entityBaseValue) {
      if (entityBaseId != 'province_id') {
        where = ` AND ${entityBaseId} = ${entityBaseValue} `;
      } else {
        where = ` AND sudin_kab_kota.${entityBaseId} = ${entityBaseValue} `;
      }
    }

    const query = `
      SELECT
        kelompok_populasi,
        COUNT ( * ) AS COUNT
      FROM
        patients
        JOIN upk ON upk.ID = patients.upk_id
        JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
        JOIN provinces ON provinces.ID = sudin_kab_kota.province_id 
      WHERE
        status_patient = 'ODHA' 
        AND DATE_PART( 'year', tgl_meninggal ) = ${year} 
        AND DATE_PART( 'month', tgl_meninggal ) = ${month}
        ${where}
      GROUP BY
      kelompok_populasi
    `;
    const kelPopulasiList = await sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT
    });

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.2
  const getOdhasStopArt = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.akhir_follow_up = :akhirFollowUp " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          akhirFollowUp: ENUM.AKHIR_FOLLOW_UP.BERHENTI_ARV,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhasStopArtKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.akhir_follow_up = :akhirFollowUp " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          akhirFollowUp: ENUM.AKHIR_FOLLOW_UP.BERHENTI_ARV,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.3
  const getOdhasNotVisit = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.bulan_periode_laporan = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender AND patients.status_patient = :statusPatient ",
        " visits.treatment_id IS NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPatient: ENUM.STATUS_PATIENT.ODHA
        }
      }
    );

    return ageList;
  };
  const getOdhasNotVisitKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.bulan_periode_laporan = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.status_patient = :statusPatient ",
        " visits.treatment_id IS NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPatient: ENUM.STATUS_PATIENT.ODHA
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.4
  const getFailFollowUp3Month = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    let threeMonthsAgoMoment = moment()
      .endOf("month")
      .subtract(4, "months")
      .format();
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " visits.visit_date > :threeMonthsAgoMoment AND visits.visit_type = :visitType  ",
        " patients.gender = :gender AND patients.status_patient = :statusPatient AND (patients.meta->>'lastTreatmentDate')::DATE <= :threeMonthsAgoMoment ",
        " visits.treatment_id IS NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        //
        replacements: {
          gender: gender,
          threeMonthsAgoMoment: threeMonthsAgoMoment,
          statusPatient: ENUM.STATUS_PATIENT.ODHA,
          visitType: ENUM.VISIT_TYPE.TREATMENT
        }
      }
    );
    return ageList;
  };
  const getFailFollowUp3MonthKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    let threeMonthsAgoMoment = moment()
      .endOf("month")
      .subtract(4, "months")
      .format();
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " visits.visit_date > :threeMonthsAgoMoment AND visits.visit_type = :visitType  ",
        " patients.status_patient = :statusPatient AND (patients.meta->>'lastTreatmentDate')::DATE <= :threeMonthsAgoMoment ",
        " visits.treatment_id IS NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          threeMonthsAgoMoment: threeMonthsAgoMoment,
          statusPatient: ENUM.STATUS_PATIENT.ODHA,
          visitType: ENUM.VISIT_TYPE.TREATMENT
        }
      }
    );
    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.5.1
  const getCumulativeOdhaArt = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender ",
        " visits.treatment_id IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,

        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.ORISINAL
        }
      }
    );

    return ageList;
  };
  const getCumulativeOdhaArtKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        null,
        " visits.treatment_id IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.ORISINAL
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.5.1
  const getOdhaRejimen1Original = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender ",
        " visits.treatment_id IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.ORISINAL
        }
      }
    );

    return ageList;
  };
  const getOdhaRejimen1OriginalKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        null,
        " visits.treatment_id IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.ORISINAL
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.5.2
  const getOdhaRejimen1 = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender ",
        " visits.treatment_id IS NOT NULL ",
        {
          join: ` LEFT JOIN prescriptions ON visits.id = prescriptions.visit_id ` + ` LEFT JOIN regiments ON prescriptions.regiment_id = regiments.id `,
          where: ` AND prescriptions.is_draft = 'false' AND regiments.name = 'LINI I' `
        }
      ),
      {
        type: sequelize.QueryTypes.SELECT,

        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.SUBSTITUSI
        }
      }
    );

    return ageList;
  };
  const getOdhaRejimen1KelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        null,
        " visits.treatment_id IS NOT NULL ",
        {
          join: ` LEFT JOIN prescriptions ON visits.id = prescriptions.visit_id ` + ` LEFT JOIN regiments ON prescriptions.regiment_id = regiments.id `,
          where: ` AND prescriptions.is_draft = 'false' AND regiments.name = 'LINI I' `
        }
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.SUBSTITUSI
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 3.5.3
  const getOdhaRejimen2 = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender ",
        " visits.treatment_id IS NOT NULL ",
        {
          join: ` LEFT JOIN prescriptions ON visits.id = prescriptions.visit_id ` + ` LEFT JOIN regiments ON prescriptions.regiment_id = regiments.id `,
          where: ` AND prescriptions.is_draft = 'false' AND regiments.name = 'LINI II' `
        }
      ),
      {
        type: sequelize.QueryTypes.SELECT,

        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.SWITCH
        }
      }
    );

    return ageList;
  };
  const getOdhaRejimen2KelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.status_paduan = :statusPaduan AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        null,
        " visits.treatment_id IS NOT NULL ",
        {
          join: ` LEFT JOIN prescriptions ON visits.id = prescriptions.visit_id ` + ` LEFT JOIN regiments ON prescriptions.regiment_id = regiments.id `,
          where: ` AND prescriptions.is_draft = 'false' AND regiments.name = 'LINI II' `
        }
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear(),
          statusPaduan: ENUM.STATUS_PADUAN.SWITCH
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getCumulativeOdhaArt,
    getCumulativeOdhaArtKelPopulasi,
    getFailFollowUp3Month,
    getFailFollowUp3MonthKelPopulasi,
    getOdhaDeaths,
    getOdhaDeathsKelPopulasi,
    getOdhasNotVisit,
    getOdhasNotVisitKelPopulasi,
    getOdhasStopArt,
    getOdhasStopArtKelPopulasi,
    getOdhaRejimen1Original,
    getOdhaRejimen1OriginalKelPopulasi,
    getOdhaRejimen1,
    getOdhaRejimen1KelPopulasi,
    getOdhaRejimen2,
    getOdhaRejimen2KelPopulasi
  };
};

module.exports = AfterArtDashboardService;
