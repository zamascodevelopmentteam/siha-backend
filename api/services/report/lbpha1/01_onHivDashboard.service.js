const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");
const sequelize = require("../../../../config/database");
const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();

const OnArtDashboardService = () => {
  // LBPHA Lembar 1 NO 1.1
  const getNewOdhaHIV = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.ordinal = 1 AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getNewOdhaHIVKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.ordinal = 1 AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 1.2
  const getOdhaReferIn = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.tgl_rujuk_masuk IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaReferInKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.tgl_rujuk_masuk IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 1.3
  const getOdhaReferOut = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.tgl_rujuk_keluar IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaReferOutKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.tgl_rujuk_keluar IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 1.4
  const getCumulativeOdhaHIV = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.id IS NOT NULL AND DATE_PART('month', visits.visit_date) <= :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) <= :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getCumulativeOdhaHIVKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.id IS NOT NULL AND DATE_PART('month', visits.visit_date) <= :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) <= :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 1.5
  const getOdhaNotArtNotVisit = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.id IS NULL  ",
        " patients.gender = :gender AND patients.status_patient = :statusPatient "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusPatient: ENUM.STATUS_PATIENT.HIV_POSITIF
        }
      }
    );

    return ageList;
  };
  const getOdhaNotArtNotVisitKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.id IS NULL ",
        " patients.status_patient = :statusPatient "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusPatient: ENUM.STATUS_PATIENT.HIV_POSITIF
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 1.6
  const getCumulativeOdhaNonArtDeath = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.id IS NULL ",
        " patients.gender = :gender AND patients.status_patient = :statusPatient "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusPatient: ENUM.STATUS_PATIENT.HIV_POSITIF
        }
      }
    );

    return ageList;
  };
  const getCumulativeOdhaNonArtDeathKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.id IS NULL ",
        " patients.status_patient = :statusPatient "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusPatient: ENUM.STATUS_PATIENT.HIV_POSITIF
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 1.7
  const getTotalVisitHiv = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.ordinal >= 1 " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getTotalVisitHivKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.ordinal >= 1 " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          statusTb: ENUM.STATUS_TB.POSITIF_TB,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getCumulativeOdhaHIV,
    getCumulativeOdhaHIVKelPopulasi,
    getCumulativeOdhaNonArtDeath,
    getCumulativeOdhaNonArtDeathKelPopulasi,
    getNewOdhaHIV,
    getNewOdhaHIVKelPopulasi,
    getOdhaNotArtNotVisit,
    getOdhaNotArtNotVisitKelPopulasi,
    getOdhaReferIn,
    getOdhaReferInKelPopulasi,
    getOdhaReferOut,
    getOdhaReferOutKelPopulasi,
    getTotalVisitHiv,
    getTotalVisitHivKelPopulasi
  };
};

module.exports = OnArtDashboardService;
