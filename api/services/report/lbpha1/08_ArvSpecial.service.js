const moment = require("moment");
const sequelize = require("../../../../config/database");

const ENUM = require("../../../../config/enum");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const ArvvSpecsialDasboardService = () => {
  // LBPHA Lembar 1 NO 8.1
  const getPatientsOldArv = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.jml_hr_obat_arv_1 > 30 OR treatments.jml_hr_obat_arv_2 > 30 OR treatments.jml_hr_obat_arv_3 > 30 " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsOldArvKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " treatments.jml_hr_obat_arv_1 > 30 OR treatments.jml_hr_obat_arv_2 > 30 OR treatments.jml_hr_obat_arv_3 > 30 " +
          " AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 8.2
  const getPatientsProfilaksis = async (
    entityBaseId,
    entityBaseValue,
    gender,
    monthIndex,
    yearIndex
  ) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " DATE_PART('month', profilaksis.tgl_profilaksis) = :thisMonthIndex AND DATE_PART('YEAR', profilaksis.tgl_profilaksis) = :thisYearIndex " +
          "  ",
        null,
        " profilaksis_id IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getPatientsProfilaksisKelPopulasi = async (
    entityBaseId,
    entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId,
        entityBaseValue,
        " DATE_PART('month', profilaksis.tgl_profilaksis) = :thisMonthIndex AND DATE_PART('YEAR', profilaksis.tgl_profilaksis) = :thisYearIndex " +
          "  ",
        null,
        " profilaksis_id IS NOT NULL "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };
  return {
    getPatientsOldArv,
    getPatientsOldArvKelPopulasi,
    getPatientsProfilaksis,
    getPatientsProfilaksisKelPopulasi
  };
};

module.exports = ArvvSpecsialDasboardService;
