const ENUM = require("../../../../config/enum");
const VarServive = require("../../variable.service")();
const _ = require("lodash");

const ResponseService = () => {
  const dashboard = data => {
    let underOne = 0,
      oneToFourteen = 0;
    let fifteenToNineteen = 0,
      twentyToTwentyFour = 0;
    let twentyFiveToTwentyNine = 0,
      thirtyToThirtyfour = 0;
    let thirtyFiveToThirtyNine = 0,
      fortyToFourtyFour = 0;
    let fortyFiveToFortyNine = 0,
      overFifthy = 0;
    let total = 0;
    for (let x = 0; x < data.length; x++) {
      const age = parseInt(data[x].age);
      const count = parseInt(data[x].count);
      switch (true) {
        case age < 1:
          underOne += count;
          break;
        case age < 15 && age > 0:
          oneToFourteen += count;
          break;
        case age < 20 && age > 14:
          fifteenToNineteen += count;
          break;
        case age < 25 && age > 19:
          twentyToTwentyFour += count;
          break;
        case age < 30 && age > 24:
          twentyFiveToTwentyNine += count;
          break;
        case age < 35 && age > 29:
          thirtyToThirtyfour += count;
          break;
        case age < 40 && age > 34:
          thirtyFiveToThirtyNine += count;
          break;
        case age < 45 && age > 39:
          fortyToFourtyFour += count;
          break;
        case age < 50 && age > 44:
          fortyFiveToFortyNine += count;
          break;
        case age > 50:
          overFifthy += count;
          break;
        default:
          break;
      }

      total += count;
    }

    let result = [
      {
        age: "< 1",
        count: underOne
      },
      {
        age: "1-14",
        count: oneToFourteen
      },
      {
        age: "15-19",
        count: fifteenToNineteen
      },
      {
        age: "20-24",
        count: twentyToTwentyFour
      },
      {
        age: "25-29",
        count: twentyFiveToTwentyNine
      },
      {
        age: "30-34",
        count: thirtyToThirtyfour
      },
      {
        age: "35-39",
        count: thirtyFiveToThirtyNine
      },
      {
        age: "40-44",
        count: fortyToFourtyFour
      },
      {
        age: "45-49",
        count: fortyFiveToFortyNine
      },
      {
        age: "> 50",
        count: overFifthy
      },
      {
        jumlah: total
      }
    ];
    return result;
  };

  const kelompokPopulasi = (listOfData, debug) => {
    const KEL_POPULASI = ENUM.KEL_POPULASI;
    let kelPopulasiArr = _.map(KEL_POPULASI, item => {
      return {
        label: item,
        count: 0
      };
    });
    let countTotal = 0;
    if (debug) {
      console.log(listOfData, 'aaaaaaaaaaa');
    }
    if (listOfData && listOfData.length > 0) {
      for (let i = 0; i < listOfData.length; i++) {
        for (let ix = 0; ix < listOfData[i].kelompok_populasi.length; ix++) {
          const v = listOfData[i].kelompok_populasi[ix];
          if (debug) {
            console.log(v, 'aaa' + ix, listOfData[i].count);
          }
          for (let ii = 0; ii < kelPopulasiArr.length; ii++) {
            if (kelPopulasiArr[ii].label === v) {
              kelPopulasiArr[ii].count += parseInt(listOfData[i].count);
              countTotal += parseInt(listOfData[i].count);
              break;
            }
          }
        }
      }
    }

    kelPopulasiArr.push({
      label: "Total",
      count: countTotal
    });
    if (debug) {
      console.log(kelPopulasiArr, 'aaaaaaaaaaaaaaaaaaa');
    }
    return kelPopulasiArr;
  };

  return {
    dashboard,
    kelompokPopulasi
  };
};

module.exports = ResponseService;
