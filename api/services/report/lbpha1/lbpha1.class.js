const _ = require("lodash");
const moment = require("moment");

const ENUM = require("../../../../config/enum");

const VariableService = require("../../variable.service")();

const ReportClass = require("../report.class");

module.exports = class Lbpha2Class extends ReportClass {
  constructor(staff, req, month, year) {
    super(staff, req, month, year);
    super.setReportTitle("Laporan LBPHA 1");
    this.lbpha1 = super.getDefaultReport();

    this.filter = super.getDefaultFilter();
  }

  async initiateMetaReport() {
    await super.initiateMetaReport();
  }

  getEntitySet() {
    return {
      entityBaseId: this.entityBaseId,
      entityBaseValue: this.entityBaseValue
    };
  }

  getFilter() {
    return this.filter;
  }

  getReport() {
    return this.lbpha1;
  }
};
