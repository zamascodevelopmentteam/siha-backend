const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");
const sequelize = require("../../../../config/database");

const Lbpha1SQLService = require("./lbpha1.sql.service")();
const LoggerService = require("../../logger.service")();

const OnArtDashboardService = () => {
  // LBPHA Lembar 1 NO 2.1
  const getNewOdhaArt = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.ordinal = 1 AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getNewOdhaArtKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.ordinal = 1 AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 2.2
  const getOdhaReferInArt = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.tgl_rujuk_masuk IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );
    return ageList;
  };
  const getOdhaReferInArtKelPopulasi = async (entityBaseId, entityBaseValue, monthIndex, yearIndex) => {
    const thisMonthIndex = monthIndex || new Date().getMonth() + 1;
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.tgl_rujuk_masuk IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );
    return kelPopulasiList;
  };

  //LBPHA Lembar 1 NO 2.3
  const getOdhaReferOutArt = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.tgl_rujuk_keluar IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getOdhaReferOutArtKelPopulasi = async (
    entityBaseId, entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " treatments.tgl_rujuk_keluar IS NOT NULL AND DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  // LBPHA Lembar 1 NO 2.4
  const getCumulativeOdhaArt = async (entityBaseId, entityBaseValue, gender, monthIndex, yearIndex) => {
    const ageList = await sequelize.query(
      Lbpha1SQLService.visitDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex ",
        " patients.gender = :gender "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          gender: gender,
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return ageList;
  };
  const getCumulativeOdhaArtKelPopulasi = async (
    entityBaseId, entityBaseValue,
    monthIndex,
    yearIndex
  ) => {
    const kelPopulasiList = await sequelize.query(
      Lbpha1SQLService.kelPopulasiDashboardQueryBuilder(
        entityBaseId, entityBaseValue,
        " DATE_PART('month', visits.visit_date) = :thisMonthIndex AND DATE_PART('YEAR', visits.visit_date) = :thisYearIndex  "
      ),
      {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          thisMonthIndex: monthIndex || new Date().getMonth() + 1,
          thisYearIndex: yearIndex || new Date().getFullYear()
        }
      }
    );

    return kelPopulasiList;
  };

  return {
    getCumulativeOdhaArt,
    getCumulativeOdhaArtKelPopulasi,
    getNewOdhaArt,
    getNewOdhaArtKelPopulasi,
    getOdhaReferInArt,
    getOdhaReferInArtKelPopulasi,
    getOdhaReferOutArt,
    getOdhaReferOutArtKelPopulasi
  };
};

module.exports = OnArtDashboardService;
