const sequelize = require("../../../config/database");
const Sequelize = require("sequelize");
const _ = require("lodash");
const Op = Sequelize.Op;

const cohortService = () => {
  const getJumlahIms = async params => {
    let {
      page,
      limit,
      keyword,
      monthPeriode,
      sudinKabKotaId,
      provinceId
    } = params;

    let query =
      `SELECT
      count(ims_visits.id) total,
      upk.id AS "upkId",
      upk.name AS "upkName"
  FROM
      upk AS "upk"
      LEFT JOIN patients ON upk.id = patients.upk_id
      LEFT JOIN (
          SELECT
              visits.id,
              patient_id
          FROM
              visits
              JOIN test_ims ON test_ims.visit_id = visits.id
          ` +
      periodeParam +
      `) AS ims_visits ON ims_visits.patient_id = patients.id
      LEFT JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = "upk".sudin_kab_kota_id
      LEFT JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
      ` +
      whereEntity +
      `
  GROUP BY
      "upkId",
      "upkName"
  ORDER BY
      total DESC,
      "upkId"`;

    const ImsList = await sequelize
      .query(query, {
        type: sequelize.QueryTypes.SELECT
      })
      .then(results => {
        return results;
      });

    return ImsList;
  };

  return {
    getJumlahIms
  };
};

module.exports = cohortService;
