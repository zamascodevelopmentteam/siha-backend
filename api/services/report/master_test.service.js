const sequelize = require("../../../config/database");
const Sequelize = require("sequelize");
const _ = require("lodash");
const Op = Sequelize.Op;

const masterTestService = () => {
  const getTestHIVReportQueryBuilder = params => {
    let { visitWhereString, roleWhereString, month, year } = params;

    visitWhereString = visitWhereString ? " AND " + visitWhereString : "";
    roleWhereString = roleWhereString ? " WHERE " + roleWhereString : "";
    roleWhereString = roleWhereString.replace("upk.upk_id", "upk.id");

    let query =
//       `SELECT
//       gender,
//  	age,
//  	kelompok_populasi,
//  	count(test_visits.id)::int total
//   FROM
//   		(select
//   			id,
//   			gender,
//   			(DATE_PART('year', NOW()) - DATE_PART('year', date_birth)) AS age
//   			FROM
//   			patients WHERE date_birth IS NOT NULL
//   		) as patients
//   		JOIN (
//           SELECT
//               visits.id,
//               patient_id,
//               visits.upk_id,
//               patients.kelompok_populasi
//           FROM
//               (
//                 SELECT *
//                 FROM visits
//                 WHERE DATE_PART('month', visits.visit_date) = ${month} AND DATE_PART('year', visits.visit_date) = ${year}

//               ) visits
//               JOIN test_hiv ON test_hiv.visit_id = visits.id
//               JOIN patients ON patients.id = visits.patient_id
//           WHERE patients.kelompok_populasi IS NOT NULL
//           ` +
//     visitWhereString +
//     `
//        ) AS test_visits ON test_visits.patient_id = patients.id
//       JOIN upk ON upk.id = test_visits.upk_id
//       JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = upk.sudin_kab_kota_id
//       JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
//        ` +
//     roleWhereString +
//     `
//  GROUP BY
//  	gender,
//  	age,
//  	kelompok_populasi`;
`SELECT
patients.id,
gender,
age,
kelompok_populasi,
fullname,
count(test_visits.id)::int total
FROM
(select
  id,
  gender,
  (DATE_PART('year', NOW()) - DATE_PART('year', date_birth)) AS age
  FROM
  patients WHERE date_birth IS NOT NULL
) as patients
JOIN (
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',1) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',2) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',3) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',4) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',5) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',6) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',7) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',8) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',9) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',9) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',10) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',11) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`
    UNION
    SELECT
        visits.id,
        patient_id,
        visits.upk_id,
        split_part(array_to_string(patients.kelompok_populasi,','),',',12) as kelompok_populasi,
        patients.fullname
    FROM
        (
          SELECT *
          FROM test_hiv
          WHERE DATE_PART('month', test_hiv.tanggal_test) = ${month} AND DATE_PART('year', test_hiv.tanggal_test) = ${year}

        ) test_hiv
        JOIN visits ON test_hiv.visit_id = visits.id
        JOIN patients ON patients.id = visits.patient_id
    WHERE patients.kelompok_populasi IS NOT NULL` + visitWhereString +`  
 ) AS test_visits ON test_visits.patient_id = patients.id
JOIN upk ON upk.id = test_visits.upk_id
JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = upk.sudin_kab_kota_id
JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
 ` +
  roleWhereString +
  `
  AND kelompok_populasi !=''
GROUP BY
gender,
age,
kelompok_populasi,
fullname,
patients."id"`;
    return query;
  };

  const getTestIMSReportQueryBuilder = params => {
    let { visitWhereString, roleWhereString } = params;

    visitWhereString = visitWhereString ? " AND " + visitWhereString : "";
    roleWhereString = roleWhereString ? " WHERE " + roleWhereString : "";
    roleWhereString = roleWhereString.replace("upk.upk_id", "upk.id");

    let query =
      `SELECT
      gender,
 	age,
 	kelompok_populasi,
 	count(test_visits.id)::int total
  FROM
  		(select
  			id,
  			gender,
  			(DATE_PART('year', NOW()) - DATE_PART('year', date_birth)) AS age
  			FROM
  			patients WHERE date_birth IS NOT NULL
  		) as patients
  		JOIN (
          SELECT
              visits.id,
              patient_id,
              visits.upk_id,
              test_hiv.kelompok_populasi
          FROM
              visits
              JOIN test_hiv ON test_hiv.visit_id = visits.id
              JOIN test_ims ON test_ims.visit_id = visits.id
          WHERE test_hiv.kelompok_populasi IS NOT NULL
          ` +
      visitWhereString +
      `
       ) AS test_visits ON test_visits.patient_id = patients.id
      JOIN upk ON upk.id = test_visits.upk_id
      JOIN sudin_kab_kota AS "sudinKabKota" ON "sudinKabKota".id = upk.sudin_kab_kota_id
      JOIN provinces AS "provinces" ON "provinces".id = "sudinKabKota".province_id
      ` +
      roleWhereString +
      `
 GROUP BY
 	gender,
 	age,
 	kelompok_populasi
 `;

    return query;
  };

  return {
    getTestHIVReportQueryBuilder,
    getTestIMSReportQueryBuilder
  };
};

module.exports = masterTestService;
