const _ = require("lodash");
const moment = require("moment");

const sequelize = require("../../../config/database");
const ENUM = require("../../../config/enum");

const Regiment = require("../../models/Regiment");
const Medicine = require("../../models/Medicine");
const Brand = require("../../models/Brand");
const Inventory = require("../../models/Inventory");
const RegimentMedicine = require("../../models/RegimentMedicine");
const Lbpha2 = require("../../models/Lbpha2");

const VariableService = require("../variable.service")();

let topHeader = {
  pasienReguler: {
    dewasa: 0,
    anak: 0
  },
  pasienTransit: {
    dewasa: 0,
    anak: 0
  },
  pasienMultiMonth: {
    dewasa: 0,
    anak: 0
  }
};

let topHeaderInventory = {
  column_A: 0,
  column_B: 0,
  column_C: 0,
  column_D: 0,
  column_E: 0,
  column_F: 0,
  column_G: 0,
  column_H: null,
  column_I: 0,
  column_J: 0,
  column_K: null
};

module.exports = class CohortClass {
  constructor(staff) {
    this.staff = staff;
    this.setEntityRole(this.staff.role);
    this.report = {};
    this.filter = {
      month: new Date().getMonth(),
      year: new Date().getFullYear(),
      upkId: staff.upkId
    };

    this.report.table = {
      content: {},
      footer: {}
    };

    this.initiateVariable();
  }

  initiateVariable() {
    this.constructTable();
  }

  setFilter(monthIndex, yearIndex) {
    this.filter.month = monthIndex;
    this.filter.year = yearIndex;
  }

  setEntityRole(userRole) {
    this.entityBaseId = "upk_id";
    this.entityBaseValue = 0;

    switch (userRole) {
      case ENUM.USER_ROLE.RR_STAFF:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
        break;
      case ENUM.USER_ROLE.PHARMA_STAFF:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
        break;
      case ENUM.USER_ROLE.LAB_STAFF:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
        break;
      case ENUM.USER_ROLE.SUDIN_STAFF:
        this.entityBaseId = "sudin_kab_kota_id";
        this.entityBaseValue = this.staff.sudinKabKotaId;
        break;
      case ENUM.USER_ROLE.PROVINCE_STAFF:
        this.entityBaseId = "province_id";
        this.entityBaseValue = this.staff.provinceId;
        break;
      case ENUM.USER_ROLE.MINISTRY_STAFF:
        this.entityBaseId = "";
        this.entityBaseValue = "";
        break;
      default:
        this.entityBaseId = "upk_id";
        this.entityBaseValue = this.staff.upkId;
    }
  }

  generateEntityRole(tableName, preCondition, postCondition) {
    let str =
      (tableName ? tableName + "." : "") +
      (this.entityBaseId ? this.entityBaseId : "");
    if (!this.entityBaseId) {
      str = "";
    } else {
      str =
        (preCondition || "") +
        " " +
        str +
        " = :entityBaseValue " +
        (postCondition || "");
    }
    return str;
  }

  async buildReport() {
    await this.loadReportData();
  }

  //construct object
  constructTable() {
    this.report = {
      content: {
        jmlOrangMulaiART: {
          title: "Jumlah Orang Memulai ART di klinik ini - kohort orisinal",
          data: []
        },
        jmlOrangRujukMasuk: {
          title: "Jumlah Rujuk Masuk",
          data: []
        },
        jmlOrangRujukKeluar: {
          title: "Jumlah Rujuk Keluar",
          data: []
        },
        jmlKohortSekarang: {
          title: "Jumlah kohort sekarang	",
          data: []
        }
      }
    };
  }
  //end of construct

  getFilterInventoryColumn() {
    return {
      column_A:
        " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
        this.generateEntityRole("visits", "AND") +
        " AND (patients.age > 14 OR (patients.age <=14 AND patients.weight > 25) ) ",
      strFilterAnak:
        " DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year " +
        this.generateEntityRole("visits", "AND") +
        " AND patients.age <= 14 AND  patients.weight <= 25 "
    };
  }

  getQueryOptionFDC_Regiment() {
    return {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        month: this.filter.month + 1,
        year: this.filter.year,
        entityBaseValue: this.entityBaseValue
      }
      // logging: console.log
    };
  }

  assignResultToContent(result, table) {
    if (result.length > 0) {
      _.forEach(table.content, rowItem => {
        const matched = _.remove(result, item => {
          return rowItem.medicine.id == item.medicine_id;
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            rowItem.data.column_A += matchedItem.stok_awal;
            rowItem.data.column_B += matchedItem.stok_diterima;
            rowItem.data.column_C += matchedItem.stok_keluar;
            rowItem.data.column_D += matchedItem.stok_kadaluarsa;
            rowItem.data.column_E += matchedItem.stok_selisih_fisik;
            rowItem.data.column_F += matchedItem.stok_akhir;
            rowItem.data.column_G += matchedItem.stok_akhir_botol;
          });
        }
      });
    }
  }
  async loadReportData() {}

  getReport() {
    return this.report;
  }
};
