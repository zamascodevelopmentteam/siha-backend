const _ = require("lodash");
const moment = require("moment");

const sequelize = require("../../../config/database");
const ENUM = require("../../../config/enum");

const VariableService = require("../variable.service")();
const MasterTestService = require("./master_test.service")();

const ReportClass = require("./report.class");

module.exports = class MasterHivClass extends ReportClass {
  constructor(staff, req, month, year) {
    super(staff, req, month, year);
    super.setReportTitle("Laporan Master Test HIV");
    this.report = super.getDefaultReport();

    this.filter = super.getDefaultFilter();

    this.initiateVariable();
  }

  initiateVariable() {
    this.constructTable();
  }

  async buildReport() {
    await super.initiateMetaReport();
    await this.loadReportData();
  }

  async buildReportById() {
    await super.initiateMetaReport();
    await this.loadReportDataById();
  }

  //construct object
  constructTable() {
    this.ageList = [
      {
        title: "<1",
        ageMin: 0,
        ageMax: 1
      },
      {
        title: "1 - 14",
        ageMin: 1,
        ageMax: 14
      },
      {
        title: "15 - 19",
        ageMin: 15,
        ageMax: 20
      },
      {
        title: "20 - 24",
        ageMin: 20,
        ageMax: 25
      },
      {
        title: "25 - 29",
        ageMin: 25,
        ageMax: 30
      },
      {
        title: "30 - 34",
        ageMin: 30,
        ageMax: 35
      },
      {
        title: "35 - 39",
        ageMin: 35,
        ageMax: 40
      },
      {
        title: "40 - 44",
        ageMin: 40,
        ageMax: 45
      },
      {
        title: "45 - 49",
        ageMin: 45,
        ageMax: 50
      },
      {
        title: ">=50",
        ageMin: 50,
        ageMax: 150
      }
    ];
    const KEL_POPULASI = _.values(ENUM.KEL_POPULASI);
    let kelPopulasiArr = _.map(KEL_POPULASI, (item, index) => {
      return {
        label: index + 1 + ". " + item,
        value: item,
        data: Array.from(Array(this.ageList.length * 2), () => 0)
      };
    });

    Object.assign(this.report, {
      meta: super.getDefaultReportMeta(),
      content: {
        diTestHivList: {
          title: "Jumlah orang yang dites HIV",
          data: _.cloneDeep(kelPopulasiArr)
        },
        testHivPositifList: {
          title: "Jumlah orang yang HIV positif",
          data: _.cloneDeep(kelPopulasiArr)
        }
      }
    });
    this.report.header = {};
    this.report.header[ENUM.GENDER.LAKI_LAKI] = _.cloneDeep(this.ageList);
    this.report.header[ENUM.GENDER.PEREMPUAN] = _.cloneDeep(this.ageList);
  }
  //end of construct

  getQueryOption() {
    return super.getDefaultQueryOption();
  }

  assignResultToContent(result, table) {
    if (result.length > 0) {
      const ageListGender1StartIndex = 0;
      const ageListGender2StartIndex = this.ageList.length;

      _.forEach(table.data, kelPopulasiItem => {
        const matched = _.remove(result, item => {
          return kelPopulasiItem.value == item.kelompok_populasi;
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            const matchedIndex = _.findIndex(this.ageList, ageListItem => {
              return (
                ageListItem.ageMin <= matchedItem.age &&
                matchedItem.age < ageListItem.ageMax
              );
            });

            if (
              matchedIndex >= 0 &&
              matchedItem.gender === ENUM.GENDER.LAKI_LAKI
            ) {
              kelPopulasiItem.data[matchedIndex] += matchedItem.total;
            } else if (
              matchedIndex >= 0 &&
              matchedItem.gender === ENUM.GENDER.PEREMPUAN
            ) {
              kelPopulasiItem.data[matchedIndex + this.ageList.length] +=
                matchedItem.total;
            }
          });
        }
      });
    }
  }
  // new
  assignResultToContentById(result, table) {
    let patienId = [];
    if (result.length > 0) {
      const ageListGender1StartIndex = 0;
      const ageListGender2StartIndex = this.ageList.length;

      _.forEach(table.data, kelPopulasiItem => {
        const matched = _.remove(result, item => {
          return kelPopulasiItem.value == item.kelompok_populasi;
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            const matchedIndex = _.findIndex(this.ageList, ageListItem => {
              return (
                ageListItem.ageMin <= matchedItem.age &&
                matchedItem.age < ageListItem.ageMax
              );
            });
            if (matchedIndex >= 0 && !patienId.includes(matchedItem.id)) {
              patienId.push(matchedItem.id);
              if (
                matchedIndex >= 0 &&
                matchedItem.gender === ENUM.GENDER.LAKI_LAKI
              ) {
                kelPopulasiItem.data[matchedIndex] += matchedItem.total;
              } else if (
                matchedIndex >= 0 &&
                matchedItem.gender === ENUM.GENDER.PEREMPUAN
              ) {
                kelPopulasiItem.data[matchedIndex + this.ageList.length] +=
                  matchedItem.total;
              }
            }
          });
        }
      });
    }
  }
  // end new
  async loadReportData() {
    let queryOpt = this.getDefaultQueryOption();
    let queryParams = {
      visitWhereString:
        " DATE_PART('month', test_hiv.tanggal_test) = :month AND DATE_PART('year', test_hiv.tanggal_test) = :year ",
      roleWhereString: super.generateEntityRole("upk"),
      month: this.filter.month + 1,
      year: this.filter.year
    };
    const result = await sequelize.query(
      MasterTestService.getTestHIVReportQueryBuilder(queryParams),
      queryOpt
    );
    this.assignResultToContent(result, this.report.content.diTestHivList);

    let queryOptHivPositif = this.getDefaultQueryOption();
    queryOptHivPositif.replacements.kesimpulan_hiv = ENUM.TEST_RESULT.REAKTIF;
    queryOptHivPositif.logging = console.log;
    let queryParamsHivPositif = {
      visitWhereString:
        " test_hiv.kesimpulan_hiv = 'REAKTIF' AND DATE_PART('month', test_hiv.tanggal_test) = :month AND DATE_PART('year', test_hiv.tanggal_test) = :year ",
      roleWhereString: super.generateEntityRole("upk"),
      month: this.filter.month + 1,
      year: this.filter.year
    };
    const resultHivPositif = await sequelize.query(
      MasterTestService.getTestHIVReportQueryBuilder(queryParamsHivPositif),
      queryOptHivPositif
    );
    this.assignResultToContent(
      resultHivPositif,
      this.report.content.testHivPositifList
    );
  }
  // new
  async loadReportDataById() {
    let queryOpt = this.getDefaultQueryOption();
    let queryParams = {
      visitWhereString:
        " DATE_PART('month', test_hiv.tanggal_test) = :month AND DATE_PART('year', test_hiv.tanggal_test) = :year ",
      roleWhereString: super.generateEntityRole("upk"),
      month: this.filter.month + 1,
      year: this.filter.year
    };
    const result = await sequelize.query(
      MasterTestService.getTestHIVReportQueryBuilder(queryParams),
      queryOpt
    );
    this.assignResultToContentById(result, this.report.content.diTestHivList);

    let queryOptHivPositif = this.getDefaultQueryOption();
    queryOptHivPositif.replacements.kesimpulan_hiv = ENUM.TEST_RESULT.REAKTIF;
    // queryOptHivPositif.logging = console.log;
    let queryParamsHivPositif = {
      visitWhereString:
        " kesimpulan_hiv = :kesimpulan_hiv AND DATE_PART('month', test_hiv.tanggal_test) = :month AND DATE_PART('year', test_hiv.tanggal_test) = :year ",
      roleWhereString: super.generateEntityRole("upk"),
      month: this.filter.month + 1,
      year: this.filter.year
    };
    const resultHivPositif = await sequelize.query(
      MasterTestService.getTestHIVReportQueryBuilder(queryParamsHivPositif),
      queryOptHivPositif
    );
    this.assignResultToContentById(
      resultHivPositif,
      this.report.content.testHivPositifList
    );
  }
  // end new

  getReport() {
    return this.report;
  }
};
