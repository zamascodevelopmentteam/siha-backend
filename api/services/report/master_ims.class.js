const _ = require("lodash");
const moment = require("moment");

const sequelize = require("../../../config/database");
const ENUM = require("../../../config/enum");

const VariableService = require("../variable.service")();
const MasterTestService = require("./master_test.service")();

const ReportClass = require("./report.class");

module.exports = class MasterImsClass extends ReportClass {
  constructor(staff, req) {
    super(staff, req);
    super.setReportTitle("Laporan Master Test IMS");
    this.report = super.getDefaultReport();

    this.initiateVariable();
  }

  initiateVariable() {
    this.constructTable();
  }

  async buildReport() {
    await super.initiateMetaReport();
    await this.loadReportData();
  }

  //construct object
  constructTable() {
    this.ageList = [
      {
        title: "<1",
        ageMin: 0,
        ageMax: 1
      },
      {
        title: "1 - 14",
        ageMin: 1,
        ageMax: 15
      },
      {
        title: "15 - 19",
        ageMin: 15,
        ageMax: 20
      },
      {
        title: "20 - 24",
        ageMin: 20,
        ageMax: 25
      },
      {
        title: "25 - 29",
        ageMin: 25,
        ageMax: 30
      },
      {
        title: "30 - 34",
        ageMin: 30,
        ageMax: 35
      },
      {
        title: "35 - 39",
        ageMin: 35,
        ageMax: 40
      },
      {
        title: "40 - 44",
        ageMin: 40,
        ageMax: 45
      },
      {
        title: "45 - 49",
        ageMin: 45,
        ageMax: 50
      },
      {
        title: ">=50",
        ageMin: 50,
        ageMax: 150
      }
    ];
    const KEL_POPULASI = _.values(ENUM.KEL_POPULASI);
    let kelPopulasiArr = _.map(KEL_POPULASI, (item, index) => {
      return {
        label: index + 1 + ". " + item,
        value: item,
        data: Array.from(Array(this.ageList.length * 2), () => 0)
      };
    });

    Object.assign(this.report, {
      meta: super.getDefaultReportMeta(),
      content: {
        diTestImsList: {
          title: "Jumlah orang yang dites IMS",
          data: _.cloneDeep(kelPopulasiArr)
        },
        testImsPositifList: {
          title: "Jumlah orang yang Positif IMS",
          data: _.cloneDeep(kelPopulasiArr)
        },
        diTestSifilisList: {
          title: "Jumlah orang yang dites Sifilis",
          data: _.cloneDeep(kelPopulasiArr)
        },
        testSifilisPositifList: {
          title: "Jumlah orang yang Positif Sifilis",
          data: _.cloneDeep(kelPopulasiArr)
        }
      }
    });
    this.report.header = {};
    this.report.header[ENUM.GENDER.LAKI_LAKI] = _.cloneDeep(this.ageList);
    this.report.header[ENUM.GENDER.PEREMPUAN] = _.cloneDeep(this.ageList);
  }
  //end of construct

  getQueryOption() {
    return super.getDefaultQueryOption();
  }

  assignResultToContent(result, table) {
    if (result.length > 0) {
      const ageListGender1StartIndex = 0;
      const ageListGender2StartIndex = this.ageList.length;

      _.forEach(table.data, kelPopulasiItem => {
        const matched = _.remove(result, item => {
          return kelPopulasiItem.value == item.kelompok_populasi;
        });
        if (matched && matched.length > 0) {
          _.forEach(matched, matchedItem => {
            const matchedIndex = _.findIndex(this.ageList, ageListItem => {
              return (
                ageListItem.ageMin <= matchedItem.age &&
                matchedItem.age < ageListItem.ageMax
              );
            });

            if (
              matchedIndex >= 0 &&
              matchedItem.gender === ENUM.GENDER.LAKI_LAKI
            ) {
              kelPopulasiItem.data[matchedIndex] += matchedItem.total;
            } else if (
              matchedIndex >= 0 &&
              matchedItem.gender === ENUM.GENDER.PEREMPUAN
            ) {
              kelPopulasiItem.data[matchedIndex + this.ageList.length] +=
                matchedItem.total;
            }
          });
        }
      });
    }
  }
  async loadReportData() {
    let queryOpt = this.getDefaultQueryOption();
    let queryParams = {
      visitWhereString:
        " ditest_ims IS TRUE AND DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year ",
      roleWhereString: super.generateEntityRole("upk")
    };
    let result = await sequelize.query(
      MasterTestService.getTestIMSReportQueryBuilder(queryParams),
      queryOpt
    );
    this.assignResultToContent(result, this.report.content.diTestImsList);

    queryOpt = this.getDefaultQueryOption();
    queryParams = {
      visitWhereString:
        " ditest_ims IS TRUE AND hasil_test_ims IS TRUE AND DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year ",
      roleWhereString: super.generateEntityRole("upk")
    };
    result = await sequelize.query(
      MasterTestService.getTestIMSReportQueryBuilder(queryParams),
      queryOpt
    );
    this.assignResultToContent(result, this.report.content.testImsPositifList);

    queryOpt = this.getDefaultQueryOption();
    queryParams = {
      visitWhereString:
        " ditest_sifilis IS TRUE AND DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year ",
      roleWhereString: super.generateEntityRole("upk")
    };
    result = await sequelize.query(
      MasterTestService.getTestIMSReportQueryBuilder(queryParams),
      queryOpt
    );
    this.assignResultToContent(result, this.report.content.diTestSifilisList);

    queryOpt = this.getDefaultQueryOption();
    queryParams = {
      visitWhereString:
        " ditest_sifilis IS TRUE AND hasil_test_ims IS TRUE AND DATE_PART('month', visits.visit_date) = :month AND DATE_PART('year', visits.visit_date) = :year ",
      roleWhereString: super.generateEntityRole("upk")
    };
    result = await sequelize.query(
      MasterTestService.getTestIMSReportQueryBuilder(queryParams),
      queryOpt
    );
    this.assignResultToContent(
      result,
      this.report.content.testSifilisPositifList
    );
  }

  getReport() {
    return this.report;
  }
};
