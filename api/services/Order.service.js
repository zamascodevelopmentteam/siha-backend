const sequelize = require("../../config/database");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const Order = require("../models/Order");
const OrderItem = require("../models/OrderItem");
const Upk = require("../models/Upk");
const SudinKabKota = require("../models/SudinKabKota");
const Province = require("../models/Province");
const CentralMinistry = require("../models/CentralMinistry");
const Medicine = require("../models/Medicine");
const ENUM = require("../../config/enum");
const moment = require("moment");

const OrderService = () => {
  const getAvailableUpk = async upkid => {
    const sudinList = await sequelize.query(
      "SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry, is_active " +
        "from sudin_kab_kota " +
        "WHERE id = (SELECT sudin_kab_kota_id from upk WHERE id = " +
        upkid +
        ")  ",
      { type: sequelize.QueryTypes.SELECT }
    );

    const upkList = await sequelize.query(
      "SELECT name, id as upkId, null as sudinKotaKabId, null as provinceId, null as centralMinistry " +
        "from upk " +
        "WHERE sudin_kab_kota_id = (SELECT sudin_kab_kota_id from upk WHERE id = " +
        upkid +
        ") AND is_active = TRUE AND id != " +
        upkid,
      { type: sequelize.QueryTypes.SELECT }
    );

    if (sudinList.length > 0 && !sudinList[0].is_active) {
      const provincesList = await sequelize.query(
        "SELECT name, null as upkId, null as sudinKotaKabId, id as provinceId, null as centralMinistry " +
          "from provinces " +
          "WHERE is_active = TRUE AND id = (SELECT province_id from sudin_kab_kota WHERE id = " +
          sudinList[0].sudinkotakabid +
          ")  ",
        { type: sequelize.QueryTypes.SELECT }
      );
      return provincesList.concat(upkList);
    }
    if (sudinList.length > 0) {
      delete sudinList[0].is_active;
    }

    return sudinList.concat(upkList);
  };

  const getAvailableUpkDown = async upkid => {
    const upkListDown = [
      {
        name: ENUM.LAB_PHARMA.LAB_ENTITY,
        upkid: upkid,
        sudinkotakabid: null,
        provinceid: null,
        centralministry: null,
        logisticRole: ENUM.LOGISTIC_ROLE.LAB_ENTITY
      },
      {
        name: ENUM.LAB_PHARMA.PHARMA_ENTITY,
        upkid: upkid,
        sudinkotakabid: null,
        provinceid: null,
        centralministry: null,
        logisticRole: ENUM.LOGISTIC_ROLE.PHARMA_ENTITY
      }
    ];

    const upkList = await sequelize.query(
      "SELECT name, id as upkId, null as sudinKotaKabId, null as provinceId, null as centralMinistry " +
        "from upk " +
        "WHERE sudin_kab_kota_id = (SELECT sudin_kab_kota_id from upk WHERE id = " +
        upkid +
        ") AND is_active = TRUE AND id != " +
        upkid,
      { type: sequelize.QueryTypes.SELECT }
    );

    return upkListDown.concat(upkList);
  };

  const getAvailableSudin = async sudinKabKotaId => {
    const provincesList = await sequelize.query(
      "SELECT name, null as upkId, null as sudinKotaKabId, id as provinceId, null as centralMinistry " +
        "from provinces " +
        "WHERE is_active = TRUE AND id = (SELECT province_id from sudin_kab_kota WHERE id = " +
        sudinKabKotaId +
        ")  ",
      { type: sequelize.QueryTypes.SELECT }
    );

    const sudinList = await sequelize.query(
      "SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry " +
        "from sudin_kab_kota " +
        "WHERE is_active = TRUE AND province_id = (SELECT province_id from sudin_kab_kota WHERE id = " +
        sudinKabKotaId +
        ") AND id != " +
        sudinKabKotaId,
      { type: sequelize.QueryTypes.SELECT }
    );

    return provincesList.concat(sudinList);
  };

  const getAvailableSudinDown = async sudinKabKotaId => {
    const upkList = await sequelize.query(
      "SELECT name, id as upkId, null as sudinKotaKabId, null as provinceId, null as centralMinistry " +
        "from upk " +
        "WHERE is_active = TRUE AND sudin_kab_kota_id = " +
        sudinKabKotaId,
      { type: sequelize.QueryTypes.SELECT }
    );

    const sudinList = await sequelize.query(
      "SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry " +
        "from sudin_kab_kota " +
        "WHERE is_active = TRUE AND province_id = (SELECT province_id from sudin_kab_kota WHERE id = " +
        sudinKabKotaId +
        ") AND id != " +
        sudinKabKotaId,
      { type: sequelize.QueryTypes.SELECT }
    );

    return upkList.concat(sudinList);
  };

  const getAvailableProvinces = async provinceId => {
    const centralList = await sequelize.query(
      "SELECT name, null as upkId, null as sudinKotaKabId, null as provinceId, id as centralMinistry " +
        "from central_ministry " +
        "WHERE id = (SELECT central_ministry_id from provinces WHERE id = " +
        provinceId +
        ")  ",
      { type: sequelize.QueryTypes.SELECT }
    );

    const provincesList = await sequelize.query(
      "SELECT name, null as upkId, null as sudinKotaKabId, id as provinceId, null as centralMinistry " +
        "from provinces " +
        "WHERE is_active = TRUE AND central_ministry_id = (SELECT central_ministry_id from provinces WHERE id = " +
        provinceId +
        ") AND id != " +
        provinceId,
      { type: sequelize.QueryTypes.SELECT }
    );

    return centralList.concat(provincesList);
  };

  const getAvailableProvincesDown = async provinceId => {
    const sudinList = await sequelize.query(
      "SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry " +
        "from sudin_kab_kota " +
        "WHERE is_active = TRUE AND province_id = " +
        provinceId,
      { type: sequelize.QueryTypes.SELECT }
    );

    const provincesList = await sequelize.query(
      "SELECT name, null as upkId, null as sudinKotaKabId, id as provinceId, null as centralMinistry " +
        "from provinces " +
        "WHERE is_active = TRUE AND central_ministry_id = (SELECT central_ministry_id from provinces WHERE id = " +
        provinceId +
        ") AND id != " +
        provinceId,
      { type: sequelize.QueryTypes.SELECT }
    );

    return sudinList.concat(provincesList);
  };

  const getAvailableMinistry = async () => {
    const provincesList = await sequelize.query(
      "SELECT name, null as upkId, null as sudinKotaKabId, id as provinceId, null as centralMinistry " +
        "from provinces " +
        "WHERE is_active = TRUE ",
      { type: sequelize.QueryTypes.SELECT }
    );

    return provincesList;
  };

  const getOrderList = async (whereObj, params, isMinistry = false) => {
    let { page, limit, keyword } = params;
    var currentEntitas = [];
    if (isMinistry) {
      const centralMinistryId = 1;

      currentEntitas = await CentralMinistry.findByPk(centralMinistryId);
    }
    const orderList = await Order.findAndCountAll({
      // logging: console.log,
      where: whereObj,
      include: [
        {
          model: Upk,
          required: false,
          as: "upkOwnerData",
          attributes: ["name"]
        },
        {
          model: SudinKabKota,
          required: false,
          as: "sudinKabKotaOwnerData",
          attributes: ["name"]
        },
        {
          model: Province,
          required: false,
          as: "provinceOwnerData",
          attributes: ["name"]
        },
        {
          model: Upk,
          required: false,
          as: "upkForData",
          attributes: ["name"]
        },
        {
          model: SudinKabKota,
          required: false,
          as: "sudinKabKotaForData",
          attributes: ["name"]
        },
        {
          model: Province,
          required: false,
          as: "provinceForData",
          attributes: ["name"]
        }
      ],
      limit,
      offset: limit * page,
      order: [["updatedAt", "DESC"]]
    });
    var orderData = [];
    // console.log(orderList, 'aaaaaaaa');
    if (orderList) {
      orderData = orderList.rows.map(resultItem => {
        var dataDetail = resultItem.toJSON();
        dataDetail.senderName =
          (dataDetail.upkForData && dataDetail.upkForData.name) ||
          (dataDetail.sudinKabKotaForData && dataDetail.sudinKabKotaForData.name) ||
          (dataDetail.provinceForData && dataDetail.provinceForData.name);
        dataDetail.recieverName =
          (dataDetail.upkOwnerData && dataDetail.upkOwnerData.name) ||
          (dataDetail.sudinKabKotaOwnerData && dataDetail.sudinKabKotaOwnerData.name) ||
          (dataDetail.provinceOwnerData && dataDetail.provinceOwnerData.name);
        delete dataDetail.upkForData;
        delete dataDetail.sudinKabKotaForData;
        delete dataDetail.provinceForData;
        delete dataDetail.upkOwnerData;
        delete dataDetail.sudinKabKotaOwnerData;
        delete dataDetail.provinceOwnerData;

        if (isMinistry) {
          dataDetail.senderName = currentEntitas.name;
        }
        return dataDetail;
      });
    }

    return { count: orderList.count, rows: orderData };
  };

  const generateInvoiceNumber = lastOrder => {
    let lastNum = 0;
    if (lastOrder) {
      lastNum = Number(lastOrder.orderCode.split("/")[2]);
    }
    lastNum++;
    return "INV-REG/" + moment().format("YYYYMMDD") + "/" + String(lastNum);
  };

  const createOrderReguler = async param => {
    const {
      logisticrole,
      upkId,
      sudinKabKotaId,
      provinceId,
      ordersItems
    } = param;
    try {
      var orderList = [];
      var whereObj = {
        isRegular: true,
        createdAt: {
          [Op.between]: [
            moment()
              .startOf("day")
              .valueOf(),
            moment()
              .endOf("day")
              .valueOf()
          ]
        }
      };
      if (logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY) {
        orderList = await getAvailableUpk(upkId);
        Object.assign(whereObj, {
          logisticRole: logisticrole,
          upkIdOwner: upkId
        });
      } else if (logisticrole === ENUM.LOGISTIC_ROLE.SUDIN_ENTITY) {
        orderList = await getAvailableSudin(sudinKabKotaId);
        Object.assign(whereObj, {
          logisticRole: logisticrole,
          sudinKabKotaIdOwner: sudinKabKotaId
        });
      } else if (logisticrole === ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY) {
        orderList = await getAvailableProvinces(provinceId);
        Object.assign(whereObj, {
          logisticRole: logisticrole,
          provinceIdOwner: provinceId
        });
      } else {
        throw "Data Entitas tidak terdaftar";
      }

      const cekOrder = await Order.findOne({
        where: whereObj,
        order: [["updated_at", "DESC"]]
      });
      if (cekOrder) {
        throw "Permintaan Reguler Bulan ini sudah terbuat!";
      }
      const lastOrder = await Order.findOne({
        order: [["updated_at", "DESC"]]
      });
      const order = await Order.create({
        orderCode: generateInvoiceNumber(lastOrder),
        lastOrderStatus: ENUM.LAST_ORDER_STATUS.DRAFT,
        lastApprovalStatus: ENUM.LAST_APPROVAL_STATUS.DRAFT,
        orderNumber: lastOrder
          ? Number(lastOrder.orderCode.split("/")[2]) + 1
          : 1,
        isRegular: true,
        logisticRole: logisticrole,

        upkIdOwner: upkId,
        sudinKabKotaIdOwner: sudinKabKotaId,
        provinceIdOwner: provinceId,

        upkIdFor: orderList && orderList[0] ? orderList[0].upkid : null,
        sudinKabKotaIdFor:
          orderList && orderList[0] ? orderList[0].sudinkotakabid : null,
        provinceIdFor:
          orderList && orderList[0] ? orderList[0].provinceid : null
      }).then(async order => {
        for (i in ordersItems) {
          const medicine = await Medicine.findByPk(ordersItems[i].medicineId);
          await OrderItem.create({
            medicineId: ordersItems[i].medicineId,
            orderId: order.id,
            packageUnitType: medicine.packageUnitType,
            packageQuantity: Math.floor(
              ordersItems[i].stockQty / medicine.packageMultiplier
            ),
            stockUnit: medicine.stockUnitType,
            stockQty: ordersItems[i].stockQty,
            expiredDate: ordersItems[i].expiredDate
          });
        }
      });
      return { status: 200, message: "Permintaan reguler berhasil dibuat" };
    } catch (err) {
      return {
        status: 500,
        message: `Permintaan reguler gagal dibuat: ${err}`
      };
    }
  };

  const getAvailableSudinOnlyOne = async upkId => {
    const data = await sequelize.query(`
      SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry, is_active
      from sudin_kab_kota WHERE id = (SELECT sudin_kab_kota_id from upk WHERE id = ${upkId}) LIMIT 1
      `, { type: sequelize.QueryTypes.SELECT }
    );
    return data.length > 0 ? data[0] : {};
  };

  const getAvailableProvinceOnlyOne = async provinceId => {
    const data = await sequelize.query(`
      SELECT name, null as upkId, null as sudinKotaKabId, id as provinceId, null as centralMinistry
      from provinces WHERE is_active = TRUE AND central_ministry_id =
      (SELECT central_ministry_id from provinces WHERE id = ${provinceId}) AND id != ${provinceId} LIMIT 1
      `, { type: sequelize.QueryTypes.SELECT }
    );
    return data.length > 0 ? data[0] : {};
  };

  const getPusat = async provinceId => {
    const data = await sequelize.query(`
      SELECT name, null as upkId, null as sudinKotaKabId, null as provinceId, id as centralMinistry
      from central_ministry WHERE id = (SELECT central_ministry_id from provinces WHERE id = ${provinceId}) LIMIT 1
      `, { type: sequelize.QueryTypes.SELECT }
    );
    return data.length > 0 ? data[0] : {};
  };

  // new
  const getAvailableUpkUp = async upkid => {
    const sudinList = await sequelize.query(
      "SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry, is_active " +
        "from sudin_kab_kota " +
        "WHERE id = (SELECT sudin_kab_kota_id from upk WHERE id = " +
        upkid +
        ")  ",
      { type: sequelize.QueryTypes.SELECT }
    );
    return sudinList;
  };

  const getAvailableSudinById = async id => {
    const data = await sequelize.query(`
      SELECT name, null as upkId, id as sudinKotaKabId, null as provinceId, null as centralMinistry, is_active
      from sudin_kab_kota WHERE id = ${id} LIMIT 1
      `, { type: sequelize.QueryTypes.SELECT }
    );
    return data.length > 0 ? data[0] : {};
  };
  // end new

  return {
    createOrderReguler,
    getAvailableUpk,
    getAvailableUpkDown,
    getAvailableSudin,
    getAvailableSudinDown,
    getAvailableSudinOnlyOne,
    getAvailableProvinces,
    getAvailableProvincesDown,
    getAvailableProvinceOnlyOne,
    getAvailableMinistry,
    getPusat,
    getOrderList,
    getAvailableUpkUp,
    getAvailableSudinById
  };
};

module.exports = OrderService;
