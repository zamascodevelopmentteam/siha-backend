const sequelize = require("../../config/database");
const moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const NotificationService = require("../services/notification.service")();
const LoggerServive = require("../services/logger.service")();

const User = require("../models/User");
const Patient = require("../models/Patient");
const Notification = require("../models/Notification");

const ENUM = require("../../config/enum");

const PatientService = () => {
  const updateMetaPatient = async (patientId, metaObject) => {
    let query =
      " UPDATE patients SET meta = (meta::jsonb || '" +
      JSON.stringify(metaObject) +
      "'::jsonb) WHERE id = " +
      patientId;

    const patientUpdated = await sequelize.query(query, {
      type: sequelize.QueryTypes.UPDATE
    });

    return patientUpdated;
  };

  const sendSmsOTP = async (
    staffObject,
    userId,
    patientId,
    phone,
    otpCode,
    sendOnlyOneADay
  ) => {
    const prevNotification = await Notification.findOne({
      where: {
        [Op.and]: [
          sequelize.where(
            sequelize.fn("DATE", sequelize.col("sent_at")),
            moment()
              .local()
              .format("YYYY-MM-DD")
          ),
          {
            userId: userId
          },
          {
            sms: phone
          },
          {
            notificationType: ENUM.NOTIFICATION.SMS_OTP.type
          }
        ]
      },
      order: [["sentAt", "DESC"]]
    });
    if (sendOnlyOneADay) {
      if (prevNotification) {
        return LoggerServive.error(
          `OTP already being sent to this user: ${userId} today!`
        );
      }
    }

    if (
      prevNotification &&
      moment().diff(moment(prevNotification.sentAt), "minutes") < 5
    ) {
      return LoggerServive.error(
        `OTP already being sent to this user: ${userId} < 5 minutes ago, pls wait 5 minutes to get the new OTP!`
      );
    }

    NotificationService.sendNotification({
      recipient: {
        userId: userId,
        sms: phone
      },
      notificationType: ENUM.NOTIFICATION.SMS_OTP.type,
      message: `${ENUM.NOTIFICATION.SMS_OTP.message} ${otpCode}`,
      title: ENUM.NOTIFICATION.SMS_OTP.title,
      createdBy: staffObject.id,
      meta: {
        patientId: patientId
      }
    });
  };

  return {
    updateMetaPatient,
    sendSmsOTP
  };
};

module.exports = PatientService;
