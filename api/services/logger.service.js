const chalk = require("chalk");
const util = require("util");

const loggerService = () => {
  const error = (...args) => {
    console.error(chalk.bold.red(...args));
  };

  const success = (...args) => {
    console.info(chalk.bold.green(...args));
  };

  const info = (...args) => {
    console.info(chalk.bold.blue(...args));
  };

  const utilLogObject = objectToInspect => {
    console.log(
      util.inspect(objectToInspect, { showHidden: false, depth: null })
    );
  };

  return {
    error,
    success,
    info,
    utilLogObject
  };
};

module.exports = loggerService;
