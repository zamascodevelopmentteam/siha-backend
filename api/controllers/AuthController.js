const User = require("../models/User");
const authService = require("../services/auth.service");
const bcryptService = require("../services/bcrypt.service");
const Patient = require("../models/Patient");
const ROLES = require("../../config/roles");

const Province = require("../models/Province");
const SudinKabKota = require("../models/SudinKabKota");
const Upk = require("../models/Upk");

const AuthController = () => {
  const register = async (req, res) => {
    const { body } = req;
    try {
      const user = await User.create({
        nik: body.nik,
        password: body.password,
        name: body.name,
        role: body.role,
        logisticrole: body.logisticrole,
        avatar: body.avatar,
        email: body.email,
        phone: body.phone,
        upkId: body.upkId,
        provinceId: body.provinceId,
        sudinKabKotaId: body.sudinKabKotaId
      });

      user.created_by = user.id;
      user.updated_by = user.id;
      await user.save();

      const token = authService().issue({ id: user.id });
      const refreshToken = await authService().issueRefresh(user.id);
      return res.status(200).json({
        status: 200,
        message: "Success",
        data: {
          token,
          user,
          refreshToken
        }
      });
    } catch (err) {
      console.log(err);
      return res
        .status(500)
        .json({ status: 500, message: err.message, data: null });
    }
  };

  const login = async (req, res, next) => {
    const { nik, password, fcmToken } = req.body;

    if (nik && password) {
      try {
        var userObj = await User.findOne({
          where: {
            nik
          },
          include: [
            {
              model: Province,
              as: 'province'
            },
            {
              model: SudinKabKota,
              as: 'sudinKabKota'
            },
            {
              model: Upk,
              required: false,
              // as: 'upk'
            }
          ],
          attributes: { include: ["password", "refreshToken"] }
        });

        if (!userObj || (userObj && !userObj.isActive)) {
          return res.status(400).json({
            status: 400,
            message: "Bad Request: User not found",
            data: null
          });
        }

        if (bcryptService().comparePassword(password, userObj.password)) {
          const token = authService().issue({ id: userObj.id });
          const refreshToken = await authService().issueRefresh(userObj.id);

          if (fcmToken) {
            userObj.fcmToken = fcmToken;
            await userObj.save();
          }

          var user = {};
          if (userObj.role === ROLES.PATIENT) {
            const patient = await Patient.findOne({
              where: {
                userId: userObj.id
              }
            });
            if (!patient) {
              return res.status(403).json({
                status: 403,
                message: "patient_not_found",
                data: null
              });
            }
            user = userObj.toJSON();
            user.patient = patient.toJSON();
          } else {
            user = userObj.toJSON();
          }
          return res.status(200).json({
            status: 200,
            message: "Success",
            data: {
              token,
              refreshToken,
              user
            }
          });
        }

        return res.status(403).json({
          status: 403,
          message: "username or password don't match",
          data: null
        });
      } catch (err) {
        return next(err);
      }
    }

    return res.status(400).json({
      status: 400,
      message: "Bad Request: Email or password is wrong",
      data: null
    });
  };

  const refresh = async (req, res) => {
    try {
      const { refreshToken, userId } = req.body;
      const user = await User.findByPk(userId);
      if (user.refreshToken === refreshToken) {
        const newRefreshToken = await authService().issueRefresh(user.id);
        user.refreshToken = newRefreshToken;
        await user.save();
        const token = authService().issue({ id: user.id });
        res.send({
          status: 200,
          message: "Success",
          data: {
            token,
            refreshToken: newRefreshToken
          }
        });
      } else {
        res.status(403).json({ status: 403, message: "Forbidden", data: null });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ status: 500, message: error.message, data: null });
    }
  };

  const logout = async (req, res) => {
    try {
      const { user } = req;
      user.refreshToken = null;
      if (req.query.fcmToken) {
        user.fcmToken = null;
      }
      user.refreshToken = null;
      await user.save();
      res.send({
        status: 200,
        message: "Success",
        data: null
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ status: 500, message: error.message, data: null });
    }
  };

  // const changePassword = async (req, res) => {
  //   try {
  //     const { oldPassword, newPassword } = req.body;
  //     if (bcryptService().comparePassword(oldPassword, req.user.password)) {
  //       req.user.password = bcryptService().password(newPassword);
  //       await req.user.save();
  //       res.status(200).json({
  //         status: 200,
  //         message: 'Success',
  //         data: null,
  //       });
  //     } else {
  //       res.status(403).json({
  //         status: 403,
  //         message: 'Forbidden',
  //         data: null,
  //       });
  //     }
  //   } catch (error) {
  //     console.log(error);
  //     res.status(500).json({ status: 500, message: error.message, data: null });
  //   }
  // };

  // const reset = async (req, res) => {
  //   const { nik } = req.body;
  //   try {
  //     const user = await User.findOne({
  //       where: { nik }
  //     });
  //     const newPassword = Math.random().toString(36).substr(2);
  //     user.password = bcryptService().password(newPassword);
  //     await user.save();
  //     otpService().send(user.email, 'New password : ' + newPassword, '[M-SIHA] New password');
  //     res.send({
  //       status: 200,
  //       message: 'Success',
  //       data: null,
  //     });
  //   } catch (error) {
  //     console.log(error);
  //     res.status(500).json({ status: 500, message: error.message, data: null });
  //   }
  // };

  const forgotPassword = async (req, res, next) => {
    const body = req.body;
    try {
      let record;
      if (body.name) {
        record = await User.findOne({
          where: {
            nik: body.name
          },
          // attributes: ['id']
        });
      } else {
        throw {
          status: 400,
          message: "name is null"
        };
      }
      
      if (record) {
        let dataParams = {};
        dataParams.password = bcryptService().password(body.password);
        Object.assign(record, dataParams);
        await record.save();
      } else {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    register,
    login,
    refresh,
    logout,
    // changePassword,
    // reset
    forgotPassword
  };
};

module.exports = AuthController;
