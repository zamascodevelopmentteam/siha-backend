const moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const DataTable = require("../libraries/dataTable.class");

const VisitService = require("../services/visit.service")();
const PatientService = require("../services/patient.service")();
const QueryService = require("../services/query.service")();
const ENUM = require("../../config/enum");

const Visit = require("../models/Visit");
const Treatment = require("../models/Treatment");
const Couple = require("../models/Couple");
const User = require("../models/User");
const Upk = require("../models/Upk");
const Patient = require("../models/Patient");
const Medicine = require("../models/Medicine");
const Prescription = require("../models/Prescription");
const TestHiv = require("../models/TestHiv");
const TestIms = require("../models/TestIms");
const TestVl = require("../models/TestVl");
const PatientLog = require("../models/PatientLog");

const Controller = () => {
  const create = async (req, res, next) => {

    const { visitId } = req.params;
    const { body } = req;

    try {
      const patient = await Patient.findOne({
        where: {
          id: req.visit.patientId
        }
      });

      const treatment = await VisitService.doTreatmentVisit(
        patient,
        req.visit,
        req.user
      );

      const prescription = await VisitService.initiatePrescription(
        req.visit,
        req.user,
        true
      );

      if (treatment && prescription) {
        treatment.prescriptionId = prescription.id;
        treatment.paduanObatArv = prescription.paduanObatStr;
        treatment.tglKunjungan = req.visit.visitDate;

        treatment.statusPaduan = await VisitService.getStatusPaduanObat({
          id: req.visit.id,
          ordinal: req.visit.ordinal,
          patientId: req.visit.patientId,
          treatment
        });
        await treatment.save();
      }

      res.send({
        status: 200,
        data: treatment
      });
    } catch (err) {
      return res.send({
        status: 200,
        error: err
      });
      next(err);
    }
  };

  const getTreatment = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const treatment = await Treatment.findOne({
        where: {
          visitId: visitId
        },
        include: [Couple, Prescription]
      });

      res.send({
        status: 200,
        message: "Success",
        data: treatment
      });
    } catch (err) {
      next(err);
    }
  };

  const updateTreatment = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;

    try {
      let treatmentParams = {
        isDiberiOAT: body.isDiberiOAT,
        periksaTB: body.periksaTB,
        treatmentStartDate: body.treatmentStartDate
          ? body.treatmentStartDate
          : undefined,
        artStartDate: body.artStartDate
          ? body.artStartDate
          : undefined,
        noRegNas: body.noRegNas,
        tglKonfirmasiHivPos: body.tglKonfirmasiHivPos
          ? body.tglKonfirmasiHivPos
          : undefined,
        tglKunjungan: body.tglKunjungan ? body.tglKunjungan : undefined,
        tglRujukMasuk: body.tglRujukMasuk ? body.tglRujukMasuk : undefined,
        tglRujukKeluar: body.tglRujukKeluar ? body.tglRujukKeluar : undefined,
        kelompokPopulasi: body.kelompokPopulasi,
        statusTb: body.statusTb,
        tglPengobatanTb: body.tglPengobatanTb
          ? body.tglPengobatanTb
          : undefined,
        statusFungsional: body.statusFungsional,
        stadiumKlinis: body.stadiumKlinis,
        ppk: body.ppk,
        tglPemberianPpk: body.tglPemberianPpk
          ? body.tglPemberianPpk
          : undefined,
        statusTbTpt: body.statusTbTpt,
        tglPemberianTbTpt: body.tglPemberianTbTpt
          ? body.tglPemberianTbTpt
          : undefined,
        tglPemberianObat: body.tglPemberianObat || undefined,
        lsmPenjangkau: body.lsmPenjangkau,
        notifikasiPasangan: body.notifikasiPasangan,
        weight: body.weight,
        height: body.height,
        tglPemberianObatSelesai: body.tglPemberianObatSelesai || undefined,
        bulanPemeriksaan: body.bulanPemeriksaan || udefined,
        skoring: body.skoring || undefined,
        didampingi: body.didampingi ? body.didampingi.toString() : "false",
        upkId: body.upkId || undefined,
      };

      if (body.akhirFollowUp) {
        treatmentParams = Object.assign(
          {
            akhirFollowUp: body.akhirFollowUp,
            tglRujukKeluar: body.tglRujukKeluar || null,
            tglMeninggal: body.tglMeninggal || null,
            tglBerhentiArv: body.tglBerhentiArv || null
          },
          treatmentParams
        );
      }

      let treatment = await Treatment.findOne({
        include: [
          Visit
        ],
        where: {
          visitId: visitId
        }
      });

      if (treatment) {
        // update tglKunjungan
          await Visit.update({
            visitDate: treatmentParams.tglKunjungan
          }, {
            where: {
              id: visitId
            }
          })
          console.log('update...........');
        // end update tglKunjungan
        treatment = Object.assign(treatment, treatmentParams);
        treatment.updatedBy = req.user.id;
        treatment.save();

        if (treatmentParams.upkId) {
          let patient = await Patient.findOne({
            where: {
              id: treatment.visit.patientId
            }
          });

          patient.upkId = treatmentParams.upkId
          if (patient.upkId != treatmentParams.upkId) {
            patient.save();

            Visit.update({
              upkId: treatmentParams.upkId
            }, {
              where: {
                patientId: patient.id
              }
            })
          }
        }
      } else {
        treatmentParams.visitId = visitId;
        treatmentParams.createdBy = req.user.id;
        treatment = await Treatment.create(treatmentParams);
      }

      if (treatmentParams.notifikasiPasangan) {
        await Couple.destroy({
          where: {
            treatmentId: treatment.id
          },
          force: true
        });
      }

      let coupleInstance = [];
      const couplesBody = body.couples || body.couple;
      if (
        treatmentParams.notifikasiPasangan == ENUM.NOTIF_COUPLE.MENERIMA &&
        couplesBody &&
        couplesBody.length > 0
      ) {
        const couples = couplesBody.map(value => {
          return {
            name: value.name,
            age: value.age,
            gender: value.gender,
            relationship: value.relationship,
            treatmentId: treatment.id
          };
        });
        coupleInstance = await Couple.bulkCreate(couples);
      }

      await Patient.update(
        {
          weight: treatmentParams.weight || undefined,
          height: treatmentParams.height || undefined,
          artStartDate: treatmentParams.artStartDate || undefined,
          tglMeninggal:
            treatmentParams.akhirFollowUp &&
              treatmentParams.akhirFollowUp === ENUM.AKHIR_FOLLOW_UP.MENINGGAL &&
              treatmentParams.tglMeninggal
              ? treatmentParams.tglMeninggal
              : undefined
        },
        {
          where: {
            id: req.visit.patientId
          }
        }
      );

      res.send({
        status: 200,
        data: Object.assign(treatment.toJSON(), { couples: coupleInstance })
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    create,
    getTreatment,
    updateTreatment
  };
};

module.exports = Controller;
