const { PATIENT } = require("../../config/roles");
const User = require("../models/User");

const UserController = () => {
  const getDetails = async (req, res) => {
    const { userId } = req.params;
    try {
      if (req.user.role === PATIENT && Number(userId) !== req.user.id) {
        res.status(403).json({
          status: 403,
          message: "Forbidden",
          data: null
        });
      } else {
        const user = await User.findByPk(userId);
        res.send({
          status: 200,
          message: "Success",
          data: user
        });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: 500,
        message: error.message,
        data: null
      });
    }
  };

  //   const updateProfile = async (req, res) => {
  //     const { userId } = req.params;
  //     const { body } = req;
  //     try {
  //       if (Number(userId) !== req.user.id) {
  //         res.status(403).json({
  //           status: 403,
  //           message: "Forbidden",
  //           data: null
  //         });
  //       } else {
  //         const user = await User.findByPk(userId);
  //         const result = await user
  //           .update(body)
  //           .then(() => {
  //             return UserDetail.findOne({
  //               where: {
  //                 userId: userId
  //               }
  //             });
  //           })
  //           .then(userDetail => {
  //             return userDetail.update(body);
  //           })
  //           .then(() => {
  //             return User.findByPk(userId, {
  //               include: UserDetail
  //             });
  //           });

  //         res.send({
  //           status: 200,
  //           message: "Success",
  //           data: result
  //         });
  //       }
  //     } catch (error) {
  //       console.log(error);
  //       res.status(500).json({
  //         status: 500,
  //         message: error.message,
  //         data: null
  //       });
  //     }
  //   };

  return {
    getDetails
    //     updateProfile
  };
};

module.exports = UserController;
