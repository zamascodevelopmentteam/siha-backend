const differenceBy = require("lodash/differenceBy");
const intersectionBy = require("lodash/intersectionBy");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const sequelize = require("../../config/database");

const Order = require("../models/Order");
const OrderLog = require("../models/OrderLog");
const OrderItem = require("../models/OrderItem");
const Medicine = require("../models/Medicine");
const DistributionPlan = require("../models/DistributionPlan");
const Upk = require("../models/Upk");
const Province = require("../models/Province");
const SudinKabKota = require("../models/SudinKabKota");
const User = require("../models/User");
const OrderService = require("../services/Order.service")();
const QueryService = require("../services/query.service")();
const moment = require("moment");
const ENUM = require("../../config/enum");
const CONSTANT = require("../../config/constant");
const Lbpha2Service = require("../services/report/lbpha2/lbpha2.service")();
const ImsPrescription = require("../models/ImsPrescription");
const ImsPrescriptionMedicine = require("../models/ImsPrescriptionMedicine");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");
const Prescription = require("../models/Prescription");
const Visit = require("../models/Visit");
const eror = new Error();
const formatD = 'YYYY-MM-DD';
const formatDT = 'YYYY-MM-DD HH:mm:ss';

const OrderController = () => {
  const generateInvoiceNumber = lastOrder => {
    let lastNum = 0;
    if (lastOrder) {
      lastNum = Number(lastOrder.orderCode.split("/")[2]);
    }
    lastNum++;
    return "INV/" + moment().format("YYYYMMDD") + "/" + String(lastNum);
  };

  const getOrderDetail = async (req, res, next) => {
    const { orderId } = req.params;
    const { user } = req;
    try {
      let order = await Order.findByPk(orderId, {
        include: [
          {
            model: OrderItem,
            include: [
              {
                model: Medicine
              }
            ]
          },
          {
            model: DistributionPlan
          },
          {
            model: Upk,
            required: false,
            as: "upkOwnerData",
            attributes: ["name"]
          },
          {
            model: SudinKabKota,
            required: false,
            as: "sudinKabKotaOwnerData",
            attributes: ["name"]
          },
          {
            model: Province,
            required: false,
            as: "provinceOwnerData",
            attributes: ["name"]
          },
          {
            model: Upk,
            required: false,
            as: "upkForData",
            attributes: ["name"]
          },
          {
            model: SudinKabKota,
            required: false,
            as: "sudinKabKotaForData",
            attributes: ["name"]
          },
          {
            model: Province,
            required: false,
            as: "provinceForData",
            attributes: ["name"]
          },
          {
            model: OrderLog,
            required: false,
            include: [
              {
                model: User,
                required: false,
                as: "actorData",
                attributes: [["name", "actorName"]]
              }
            ]
          }
        ].concat(QueryService.includeCreated()),
        order: [
          [OrderItem, 'type', 'ASC'],
          [OrderItem, Medicine, 'name', 'ASC']
        ]
      });
      if (order) {
      }
      order = order.toJSON();
      order.senderName =
        (order.upkForData && order.upkForData.name) ||
        (order.sudinKabKotaForData && order.sudinKabKotaForData.name) ||
        (order.provinceForData && order.provinceForData.name);
      order.recieverName =
        (order.upkOwnerData && order.upkOwnerData.name) ||
        (order.sudinKabKotaOwnerData && order.sudinKabKotaOwnerData.name) ||
        (order.provinceOwnerData && order.provinceOwnerData.name);
      order.senderType = order.upkForData
        ? ENUM.LOGISTIC_ROLE.UPK_ENTITY
        : order.sudinKabKotaForData
          ? ENUM.LOGISTIC_ROLE.SUDIN_ENTITY
          : order.provinceForData
            ? ENUM.LOGISTIC_ROLE.provinceIdFor
            : ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY;
      order.senderId = order.upkForData
        ? order.upkIdFor
        : order.sudinKabKotaForData
          ? order.sudinKabKotaIdFor
          : order.provinceForData
            ? order.provinceIdFor
            : null;
      order.isAllowedToConfirm = false;
      if (
        order.lastApprovalStatus === ENUM.LAST_APPROVAL_STATUS.DRAFT ||
        order.lastApprovalStatus === ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION
      ) {
        order.isAllowedToConfirm =
          order.createdBy === user.id
            ? false
            : user.upkId === order.upkIdOwner
              ? true
              : user.sudinkabkotaid === order.sudinKabKotaIdOwner
                ? true
                : user.provinceid === order.provinceIdOwner
                  ? true
                  : true;
      } else if (
        order.lastApprovalStatus === ENUM.LAST_APPROVAL_STATUS.APPROVED_INTERNAL
      ) {
        order.isAllowedToConfirm =
          order.createdBy === user.id
            ? false
            : user.upkId === order.upkIdFor
              ? true
              : user.sudinkabkotaid === order.sudinKabKotaIdFor
                ? true
                : user.provinceid === order.provinceIdFor
                  ? true
                  : false;
        if (
          order.logisticRole === ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY &&
          user.logisticrole === ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY
        ) {
          order.isAllowedToConfirm = true;
        }
      }
      delete order.upkForData;
      delete order.sudinKabKotaForData;
      delete order.provinceForData;
      delete order.upkOwnerData;
      delete order.sudinKabKotaOwnerData;
      delete order.provinceOwnerData;

      res.send({
        status: 200,
        message: "Success",
        data: order
      });
    } catch (err) {
      next(err);
    }
  };

  const confirmOrder = async (req, res, next) => {
    const { orderId } = req.params;
    const { status } = req.query;
    const { note } = req.body;
    const { user } = req;
    try {
      const lastOrder = await Order.findByPk(orderId);
      if (user.id === lastOrder.createdBy) {
        throw { message: "Validation Error, You have no authority" };
      }
      if (status && lastOrder) {
        if (
          lastOrder.lastApprovalStatus === ENUM.LAST_APPROVAL_STATUS.DRAFT &&
          status !== ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION
        ) {
          throw { message: "Validation Error" };
        }
        if (
          lastOrder.lastApprovalStatus ===
          ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION &&
          status !== ENUM.LAST_APPROVAL_STATUS.APPROVED_INTERNAL &&
          status !== ENUM.LAST_APPROVAL_STATUS.REJECTED_INTERNAL
        ) {
          throw { message: "Validation Error" };
        }
        if (
          lastOrder.lastApprovalStatus ===
          ENUM.LAST_APPROVAL_STATUS.APPROVED_INTERNAL &&
          status !== ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL &&
          status !== ENUM.LAST_APPROVAL_STATUS.REJECTED_UPPER
        ) {
          throw { message: "Validation Error" };
        }
        if (
          lastOrder.lastApprovalStatus ===
          ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL ||
          lastOrder.lastApprovalStatus ===
          ENUM.LAST_APPROVAL_STATUS.REJECTED_UPPER ||
          lastOrder.lastApprovalStatus ===
          ENUM.LAST_APPROVAL_STATUS.REJECTED_INTERNAL
        ) {
          throw { message: "Validation Error, data has been processed" };
        }
        var lastOrderStatus = ENUM.ORDER_STATUS_STATE[status];
        var lastApprovalStatus = status;
        var approvalNotes = lastOrder.approvalNotes
          ? lastOrder.approvalNotes + "\n"
          : "";
        approvalNotes += user.name + ": " + (note ? note : "-");
        Order.update(
          {
            lastOrderStatus: lastOrderStatus,
            lastApprovalStatus: lastApprovalStatus,
            approvalNotes: approvalNotes,
            updatedBy: user.id
          },
          {
            where: {
              id: orderId
            }
          }
        );
      }
      res.send({
        status: 200,
        message: "Success",
        data: null
      });
    } catch (err) {
      next(err);
    }
  };

  const createOrdersLog = async (req, res, next) => {
    const {
      upkId,
      sudinKabKotaId,
      provinceId,
      upkid,
      sudinkabkotaid,
      provinceid,
      orderNotes,
      ordersItems
    } = req.body;
    const { user } = req;
    try {
      const lastOrder = await Order.findOne({
        order: [["updated_at", "DESC"]]
      });
      let orderCode = await QueryService.generateOrderCode({
        isRegular: false,
        logisticRole: user.logisticrole,
        upkId: user.upkId,
        sudinKabKotaId: user.sudinKabKotaId,
        provinceId: user.provinceId
      });
      const order = await Order.create({
        orderCode: orderCode,
        lastOrderStatus: ENUM.LAST_ORDER_STATUS.DRAFT,
        lastApprovalStatus: ENUM.LAST_APPROVAL_STATUS.DRAFT,
        orderNotes: orderNotes,
        orderNumber: lastOrder
          ? Number(lastOrder.orderCode.split("/")[2]) + 1
          : 1,
        isRegular: false,
        picId: user.id,
        logisticRole: user.logisticrole,

        upkIdOwner: user.upkId,
        sudinKabKotaIdOwner: user.sudinKabKotaId,
        provinceIdOwner: user.provinceId,

        upkIdFor: upkid || upkId,
        sudinKabKotaIdFor: sudinkabkotaid || sudinKabKotaId,
        provinceIdFor: provinceid || provinceId,

        createdBy: user.id
      }).then(async order => {
        for (i in ordersItems) {
          const medicine = await Medicine.findByPk(ordersItems[i].medicineId);
          await OrderItem.create({
            medicineId: ordersItems[i].medicineId,
            orderId: order.id,
            packageUnitType: medicine.packageUnitType,
            packageQuantity: ordersItems[i].packageQuantity,
            expiredDate: ordersItems[i].expiredDate,
            notes: ordersItems[i].notes,
            createdBy: user.id
          });
        }
        return order;
      });
      res.send({
        status: 200,
        message: "Success",
        data: order
      });
    } catch (err) {
      next(err);
    }
  };

  const updateOrder = async (req, res, next) => {
    const { user } = req;
    const { orderId } = req.params;
    const { orderNotes, ordersItems } = req.body;

    try {
      var order = await Order.findByPk(orderId, {
        include: {
          model: OrderItem
        }
      });

      if (order.lastOrderStatus !== ENUM.LAST_ORDER_STATUS.DRAFT) {
        throw { message: "Can't be updated, data is being processed" };
      }

      const dataInsert = differenceBy(ordersItems, order.orderItems, "id");
      const dataUpdate = intersectionBy(ordersItems, order.orderItems, "id");
      const dataDelete = differenceBy(order.orderItems, ordersItems, "id");

      for (i in dataInsert) {
        const medicine = await Medicine.findByPk(dataInsert[i].medicineId);
        await OrderItem.create({
          medicineId: dataInsert[i].medicineId,
          orderId: orderId,
          packageUnitType: medicine.packageUnitType,
          packageQuantity: dataInsert[i].packageQuantity,
          expiredDate: dataInsert[i].expiredDate,
          notes: dataInsert[i].notes,
          createdBy: user.id
        });
      }

      for (i in dataUpdate) {
        const medicine = await Medicine.findByPk(dataInsert[i].medicineId);
        await OrderItem.update(
          {
            medicineId: dataUpdate[i].medicineId,
            packageUnitType: medicine.packageUnitType,
            packageQuantity: dataUpdate[i].packageQuantity,
            expiredDate: dataUpdate[i].expiredDate,
            notes: dataUpdate[i].notes,
            updatedBy: user.id
          },
          {
            where: {
              id: dataUpdate[i].id
            }
          }
        );
      }

      for (i in dataDelete) {
        await OrderItem.update(
          {
            deletedAt: new Date()
          },
          {
            where: {
              id: dataDelete[i].id
            }
          }
        );
      }

      res.status(200).json({
        status: 200,
        data: req.body,
        message: "success"
      });
    } catch (err) {
      next(err);
    }
  };

  const getOrdersLog = async (req, res, next) => {
    let { page, limit, keyword, status, isRegular } = req.query;
    const { user } = req;
    const params = {
      keyword: keyword,
      page: page ? page : CONSTANT.DEFAULT_PAGE,
      limit: limit ? limit : CONSTANT.DEFAULT_LIMIT
    };
    try {
      var whereObj = {};
      var isMinistry = false;
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            upkIdOwner: user.upkId
          };
          break;
        case "SUDIN_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
            sudinKabKotaIdOwner: user.sudinKabKotaId
          };
          break;
        case "PROVINCE_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
            provinceIdOwner: user.provinceId
          };
          break;
        case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
          isMinistry = true;
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY
          };
          break;
      }
      if (status && status !== "ALL") {
        Object.assign(whereObj, {
          lastApprovalStatus: status
        });
      }
      if (isRegular && isRegular !== "ALL") {
        Object.assign(whereObj, {
          isRegular: isRegular
        });
      }
      const orderList = await OrderService.getOrderList(
        whereObj,
        params,
        isMinistry
      );
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList.rows,
        paging: {
          page: Number(page),
          size: orderList.rows.length,
          total: orderList.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getOrdersLogIn = async (req, res, next) => {
    let { page, limit, keyword, status, isRegular } = req.query;
    const { user } = req;
    const params = {
      keyword: keyword,
      page: page ? page : CONSTANT.DEFAULT_PAGE,
      limit: limit ? limit : CONSTANT.DEFAULT_LIMIT
    };
    try {
      var whereObj = {};
      var isMinistry = false;
      switch (user.logisticrole) {
        case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
          whereObj = {
            upkIdFor: user.upkId
          };
          break;
        case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
          whereObj = {
            sudinKabKotaIdFor: user.sudinKabKotaId
          };
          break;
        case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
          whereObj = {
            provinceIdFor: user.provinceId
          };
          break;
        case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
          isMinistry = true;
          whereObj = {
            upkIdFor: null,
            sudinKabKotaIdFor: null,
            provinceIdFor: null
          };
          break;
      }
      if (
        status &&
        status !== ENUM.LAST_APPROVAL_STATUS.DRAFT &&
        status !== ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION &&
        status !== ENUM.LAST_APPROVAL_STATUS.REJECTED_INTERNAL &&
        status !== ENUM.LAST_APPROVAL_STATUS.REJECTED &&
        status !== "ALL"
      ) {
        Object.assign(whereObj, {
          lastApprovalStatus: status
        });
      } else {
        Object.assign(whereObj, {
          lastApprovalStatus: {
            [Op.notIn]: [
              ENUM.LAST_APPROVAL_STATUS.DRAFT,
              ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION,
              ENUM.LAST_APPROVAL_STATUS.REJECTED_INTERNAL
            ]
          }
        });
      }
      if (isRegular && isRegular !== "ALL") {
        Object.assign(whereObj, {
          isRegular: isRegular
        });
      }
      // console.log(whereObj);
      const orderList = await OrderService.getOrderList(
        whereObj,
        params,
        isMinistry
      );
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList.rows,
        paging: {
          page: Number(page),
          size: orderList.rows.length,
          total: orderList.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getOrdersLogAvailable = async (req, res, next) => {
    const { user } = req;
    try {
      var orderList = [];
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          orderList = await OrderService.getAvailableUpk(user.upkId);
          break;
        case "SUDIN_ENTITY":
          orderList = await OrderService.getAvailableSudin(user.sudinKabKotaId);
          break;
        case "PROVINCE_ENTITY":
          orderList = await OrderService.getAvailableProvinces(user.provinceId);
          break;
        case "MINISTRY_ENTITY":
          orderList = await OrderService.getAvailableMinistry();
          break;
      }
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList
      });
    } catch (err) {
      next(err);
    }
  };

  const getOrdersLogAvailableUp = async (req, res, next) => {
    const { user } = req;
    try {
      var orderList = [];
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          // yang lain belum ada, baru yang ini doang
          orderList = await OrderService.getAvailableUpkUp(user.upkId);
          break;
        case "SUDIN_ENTITY":
          orderList = await OrderService.getAvailableSudin(user.sudinKabKotaId);
          break;
        case "PROVINCE_ENTITY":
          orderList = await OrderService.getAvailableProvinces(user.provinceId);
          break;
        case "MINISTRY_ENTITY":
          orderList = await OrderService.getAvailableMinistry();
          break;
      }
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList
      });
    } catch (err) {
      next(err);
    }
  };

  const getOrdersLogAvailableDown = async (req, res, next) => {
    const { user } = req;
    try {
      var orderList = [];
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          orderList = await OrderService.getAvailableUpkDown(user.upkId);
          break;
        case "SUDIN_ENTITY":
          orderList = await OrderService.getAvailableSudinDown(
            user.sudinKabKotaId
          );
          break;
        case "PROVINCE_ENTITY":
          orderList = await OrderService.getAvailableProvincesDown(
            user.provinceId
          );
          break;
        case "MINISTRY_ENTITY":
          orderList = await OrderService.getAvailableMinistry();
          break;
      }
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList
      });
    } catch (err) {
      next(err);
    }
  };

  const createOrderReguler = async (req, res, next) => {
    const { user } = req;
    try {
      var entitasListItem = [];
      let ordersItems = [];
      switch (user.logisticrole) {
        case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
          entitasListItem = await sequelize.query(
            Lbpha2Service.generatePermintaanRegulerUPKQueryBuilder(user.upkId),
            {
              type: sequelize.QueryTypes.SELECT,
              replacements: {}
            }
          );
          break;
        case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
          entitasListItem = await sequelize.query(
            Lbpha2Service.generatePermintaanRegulerSudinQueryBuilder(
              user.sudinKabKotaId
            ),
            {
              type: sequelize.QueryTypes.SELECT,
              replacements: {}
            }
          );
          break;
        case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
          entitasListItem = await sequelize.query(
            Lbpha2Service.generatePermintaanRegulerProvinceQueryBuilder(
              user.provinceId
            ),
            {
              type: sequelize.QueryTypes.SELECT,
              replacements: {}
            }
          );
          break;
      }
      if (entitasListItem.length === 0) {
        throw {
          message:
            "create permintaan reguler gagal: Tidak ada barang yang dibutuhkan"
        };
      }

      for (i in entitasListItem) {
        upkId = entitasListItem[i].upk_id;
        sudinKabKotaId = entitasListItem[i].sudin_kab_kota_id;
        provinceId = entitasListItem[i].province_id;
        const stockRequest =
          entitasListItem[i].stok_keluar_reguler *
          entitasListItem[i].order_multiplier -
          entitasListItem[i].stock_on_hand;
        ordersItems.push({
          medicineId: entitasListItem[i].medicine_id,
          stockQty: stockRequest,
          expiredDate: new Date()
        });
      }

      const paramRequest = {
        logisticrole: user.logisticrole,
        upkId: upkId,
        sudinKabKotaId: sudinKabKotaId,
        provinceId: provinceId,
        ordersItems: ordersItems
      };

      const orderReg = await OrderService.createOrderReguler(paramRequest);
      if (orderReg.status === 500) {
        throw { message: orderReg.message };
      }

      res.send({
        status: 200,
        message: orderReg.message,
        data: entitasListItem,
        paramRequest: paramRequest
      });
    } catch (err) {
      next(err);
    }
  };

  const createReguler = async (req, res, next) => {
    const { logisticrole, upkId, sudinKabKotaId, provinceId, id } = req.user;
    const now = moment().format(formatDT);
    const startOfTheMonth = moment().startOf('month').format(formatDT);
    const month = moment().month();
    let startDateAllowReq = moment().date(26);
    let startSearchDate = startDateAllowReq;
    let endDateAllowReq = moment().month(month + 1).date(5).format(formatD);
    const nowD = moment().format(formatD);
    const { create } = req.query;

    try {
      let medicinesFromPatient = [], atasan, lastOrderUser;
      switch (logisticrole) {
        case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
          lastOrderUser = await getLastOrder({
            upkId,
            sudinKabKotaId,
            provinceId,
            nowD,
            startSearchDate
          });

          medicinesFromPatient = await getMedicineFromLastMonthByUpk({
            upkId,
            now,
            lastOrderUser,
            startOfTheMonth
          });

          atasan = await OrderService.getAvailableSudinOnlyOne(upkId);
          break;
        case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
          lastOrderUser = await getLastOrder({
            sudinKabKotaId,
            provinceId,
            startSearchDate,
            nowD
          });

          // medicinesFromPatient = await getMedicineForSudinAndProvince({
          //   sudinKabKotaId,
          //   provinceId,
          //   now,
          //   lastOrderUser,
          //   startOfTheMonth
          // });

          // atasan = await OrderService.getAvailableProvinceOnlyOne(sudinKabKotaId);
          atasan = await OrderService.getAvailableSudin(sudinKabKotaId);
          atasan = atasan.length > 0 ? atasan[0] : {};
          break;
        case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
          lastOrderUser = await getLastOrder({
            provinceId,
            startSearchDate,
            nowD
          });

          // medicinesFromPatient = await getMedicineForSudinAndProvince({
          //   provinceId,
          //   now,
          //   lastOrderUser,
          //   startOfTheMonth
          // });

          atasan = await OrderService.getPusat(provinceId);
          break;
        default:
      }

      if (medicinesFromPatient.hasOwnProperty('stack') && medicinesFromPatient.hasOwnProperty('message')) {
        throw medicinesFromPatient;
      }


      const { medicines, totalPatient, ids } = medicinesFromPatient;

      // if(medicines.length == 0) {
      //   eror.message = "Request gagal! Tidak ada permintaan";
      //   throw eror;
      // }

      if (create == 't') {
        // if(lastOrderUser) {
        //   eror.message = "Data permintaan untuk periode Anda sudah ada!";
        //   throw eror;
        // }
        if (!moment().isBetween(startDateAllowReq.format(formatD), endDateAllowReq, 'days', [])) {
          eror.message = "Invalid time to submit";
          throw eror;
        }

        const lastOrder = await Order.findOne({
          order: [["updated_at", "DESC"]]
        });

        const orderCode = await QueryService.generateOrderCode({
          isRegular: true,
          logisticRole: logisticrole,
          upkId: upkId,
          sudinKabKotaId: sudinKabKotaId,
          provinceId: provinceId
        });

        const orderObj = {
          orderCode,
          lastOrderStatus: ENUM.LAST_ORDER_STATUS.DRAFT,
          lastApprovalStatus: ENUM.LAST_APPROVAL_STATUS.DRAFT,
          orderNumber: lastOrder
            ? Number(lastOrder.orderCode.split("/")[2]) + 1
            : 1,
          isRegular: true,
          picId: id,
          logisticRole: logisticrole,
          provinceIdOwner: provinceId,
          provinceIdFor: atasan.provinceid,
          createdBy: id
        };

        switch (logisticrole) {
          case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
            Object.assign(orderObj, {
              totalPatient,
              upkIdOwner: upkId,
              sudinKabKotaIdOwner: sudinKabKotaId,
              upkIdFor: atasan.upkid,
              sudinKabKotaIdFor: atasan.sudinkotakabid,
            });
            break;
          case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
            Object.assign(orderObj, {
              sudinKabKotaIdOwner: sudinKabKotaId,
              sudinKabKotaIdFor: atasan.sudinkabkotaid,
            });
            break;
          // case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
          //   break;
          default:
        }

        const destination = req.body.destination;
        Object.assign(orderObj, {
          upkIdFor: destination.upkId || null,
          sudinKabKotaIdFor: destination.sudinKabKotaId || null,
          provinceIdFor: destination.provinceId || null
        });

        const order = await Order.create(orderObj);

        const orderId = order.id;
        // for(const val of medicines) {
        //   const medicine = await Medicine.findByPk(val.medicineid);
        //   await OrderItem.create({
        //     medicineId: val.medicineid,
        //     orderId,
        //     packageUnitType: medicine.packageUnitType,
        //     packageQuantity: val.qty,
        //     expiredDate: nowD,
        //     createdBy: id
        //   });
        // }

        // console.log(req.body.medichines,'++++++++++++');
        req.body.medichines.map(async val => {
          if (val.orderQty > 0 && (val.type ? !['r1', 'r2', 'r3'].includes(val.type) : true)) {
            const medicine = await Medicine.findByPk(val.medicine_id);
            await OrderItem.create({
              type: val.type,
              medicineId: val.medicine_id,
              orderId,
              packageUnitType: medicine.packageUnitType,
              packageQuantity: val.orderQty,
              expiredDate: nowD,
              createdBy: id,
              totalPatient: val.totalPatient,
              reason: val.reason
            });
          } else {
            if (val.rdtAllIds && parseInt(val.orderQty) > 0) {
              val.rdtAllIds.map(async id => {
                const medicine = await Medicine.findByPk(id);
                await OrderItem.create({
                  type: val.type,
                  medicineId: medicine.id,
                  orderId,
                  packageUnitType: medicine.packageUnitType,
                  packageQuantity: 0,
                  expiredDate: nowD,
                  createdBy: id,
                  totalPatient: 0,
                  reason: val.reason,
                  rAmount: val.orderQty
                });
              });
            }
          }
        })

        // const resultUpdate = await updatePrescription({
        //   upkId,
        //   sudinKabKotaId,
        //   provinceId,
        //   lastOrderUser,
        //   now,
        //   startOfTheMonth,
        //   logisticrole,
        //   orderId,
        //   ids
        // });
        // if (typeof resultUpdate != 'boolean') {
        //   throw resultUpdate;
        // }
        return res.send({
          status: 200,
          data: order
        });
      }
      return res.send({
        status: 200,
        data: {
          medicines,
          totalPatient,
          to: atasan
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const updatePrescription = async obj => {
    try {
      const {
        upkId,
        sudinKabKotaId,
        provinceId,
        lastOrderUser,
        now,
        startOfTheMonth,
        logisticrole,
        orderId,
        ids
      } = obj;

      lastMonth = lastOrderUser ? lastOrderUser.createdAt : startOfTheMonth;
      let queryIms, query;

      switch (logisticrole) {
        case ENUM.LOGISTIC_ROLE.UPK_ENTITY:
          queryIms = `
            update ims_prescriptions set order_id = '${orderId}'
            join visits on visits.id = ims_prescriptions.visit_id
            where visits.visit_date >= '${lastMonth}'
            and visits.visit_date <= '${now}'
            and visits.upk_id = ${upkId};
          `;
          await sequelize.query(queryIms, {
            type: sequelize.QueryTypes.UPDATE
          });

          query = `
            update prescriptions set order_id = '${orderId}'
            join visits on visits.id = prescriptions.visit_id
            where visits.visit_date >= '${lastMonth}'
            and visits.visit_date <= '${now}'
            and visits.upk_id = ${upkId};
          `;
          await sequelize.query(query, {
            type: sequelize.QueryTypes.UPDATE
          });
          break;
        case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
        case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
          query = `update from orders set general_order_id = ${orderId}
          where id in (${ids.join(",")});
          `;
          await sequelize.query(query, {
            type: sequelize.QueryTypes.UPDATE
          });
          break;
        default:
      }
      return true;
    } catch (error) {
      return error;
    }
  };

  const getMedicineFromLastMonthByUpk = async obj => {
    try {
      const { upkId, now, lastOrderUser, startOfTheMonth } = obj;
      let lastMonth;
      if (lastOrderUser.length > 0) {
        lastMonth = moment(lastOrderUser[0].created_at).format(formatDT);
      } else {
        lastMonth = startOfTheMonth;
      }

      const queryIms = `SELECT medicine_id as medicineid, medicines.name as medicinename, SUM(ims_prescription_medicines.total_qty) as qty
        from ims_prescription_medicines
        join ims_prescriptions on ims_prescriptions.id = ims_prescription_medicines.prescription_id
        join visits on visits.id = ims_prescriptions.visit_id
        join medicines on medicines.id = ims_prescription_medicines.medicine_id
        where visits.visit_date >= '${lastMonth}' AND visits.visit_date <= '${now}' and
        visits.upk_id = ${upkId}
        group by medicineid, medicinename;
      `;
      const findPrescriptionIms = await sequelize.query(queryIms, {
        type: sequelize.QueryTypes.SELECT
      });

      const queryPatientIms = `
        SELECT COUNT(*) as totalpatient from ims_prescriptions
        join visits on visits.id = ims_prescriptions.visit_id
        where visits.visit_date >= '${lastMonth}' AND visits.visit_date <= '${now}' and
        visits.upk_id = ${upkId}
      `;
      const ttlPatientIms = await sequelize.query(queryPatientIms, {
        type: sequelize.QueryTypes.SELECT
      });

      const query = `SELECT medicine_id as medicineid, medicines.name as medicinename, SUM(prescription_medicines.amount) as qty
        from prescription_medicines
        join prescriptions on prescriptions.id = prescription_medicines.prescription_id
        join visits on visits.id = prescriptions.visit_id
        join medicines on medicines.id = prescription_medicines.medicine_id
        where visits.visit_date >= '${lastMonth}' AND visits.visit_date <= '${now}' and
        visits.upk_id = ${upkId}
        group by medicineid, medicinename;
      `;
      const findPrescription = await sequelize.query(query, {
        type: sequelize.QueryTypes.SELECT
      });

      const queryPatient = `
        SELECT COUNT(*) as totalpatient from prescriptions
        join visits on visits.id = prescriptions.visit_id
        where visits.visit_date >= '${lastMonth}' AND visits.visit_date <= '${now}' and
        visits.upk_id = ${upkId}
      `;
      const ttlPatient = await sequelize.query(queryPatient, {
        type: sequelize.QueryTypes.SELECT
      });

      let medicines = findPrescriptionIms.concat(findPrescription).reduce(function (accumulator, cur) {
        const { medicineid } = cur.medicineid, found = accumulator.find(function (elem) {
          return elem.medicineid == medicineid
        });
        if (found) found.qty += cur.qty;
        else accumulator.push(cur);
        return accumulator;
      }, []);

      return {
        medicines,
        totalPatient: Number(ttlPatient[0].totalpatient) + Number(ttlPatientIms[0].totalpatient)
      }
    } catch (err) {
      return err;
    }
  };

  const getMedicineForSudinAndProvince = async obj => {
    try {
      let lastMonth, queryIds, totalPatient = 0;
      const { sudinKabKotaId, provinceId, now, lastOrderUser, startOfTheMonth, logisticrole } = obj;
      lastMonth = lastOrderUser ? lastOrderUser.createdAt : startOfTheMonth;
      const select = `select id from orders where`;
      const where = `
        is_regular = true and
        created_at >= ${lastMonth} and
        created_at <= ${now} and
        general_order_id is null;
      `;
      switch (logisticrole) {
        case ENUM.LOGISTIC_ROLE.SUDIN_ENTITY:
          queryIds = `
            sudin_kab_kota_id_owner = ${sudinKabKotaId} and
            province_id_owner = ${provinceId} and
          `;
          break;
        case ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY:
          queryIds = `
            province_id_owner = ${provinceId} and
          `;
          break;
        default:
      }
      let ids = await sequelize.query(`${select} ${queryIds} ${where}`, {
        type: sequelize.QueryTypes.SELECT
      });

      const query = `
        select medicine_id as medicineid,
        medicines.name as medicinename,
        SUM(order_items.package_quantity) as qty,
        SUM(orders.total_patient) as totalpatient
        from order_items where order_id in (${queryIds})
        join medicines on medicines.id = order_items.medicine_id
        join orders on orders.id = order_items.order_id
        group by medicineid, medicinename;
      `;
      const medicines = await sequelize.query(query, {
        type: sequelize.QueryTypes.SELECT
      });
      _.forEach(find, function (val) {
        totalPatient += Number(val.totalpatient);
      });
      ids = _.map(ids, function (val) {
        return Number(val.id);
      });
      return {
        medicines,
        totalPatient,
        ids
      }
    } catch (err) {
      return err;
    }
  };

  const getLastOrder = async obj => {
    let { upkId, sudinKabKotaId, provinceId, startSearchDate, nowD } = obj;
    let where, select, role;
    select = `select * from orders where`;

    if (upkId) {
      startSearchDate = startSearchDate.subtract(1, 'months').format(formatD);
      role = `upk_id_owner = ${upkId} and
        sudin_kab_kota_id_owner = ${sudinKabKotaId} and
        province_id_owner = ${provinceId} and`;
    } else if (sudinKabKotaId) {
      startSearchDate = startSearchDate.subtract(3, 'months').format(formatD);
      role = `upk_id_owner is null and
        sudin_kab_kota_id_owner = ${sudinKabKotaId} and
        province_id_owner = ${provinceId} and`;
    } else if (provinceId) {
      startSearchDate = startSearchDate.subtract(6, 'months').format(formatD);
      role = `upk_id_owner is null and
        sudin_kab_kota_id_owner is null and
        province_id_owner = ${provinceId} and`;
    }

    where = `
      created_at >= '${startSearchDate}' and
      created_at <= '${nowD}' and
      is_regular = true
      order by id DESC limit 1;
    `;
    return await sequelize.query(`${select} ${role} ${where};`, {
      type: sequelize.QueryTypes.SELECT
    });
  }

  const reportRelokasi = async (req, res, next) => {
    let { page, limit, keyword, status, isRegular, month } = req.query;
    const { user } = req;
    const params = {
      keyword: keyword,
      page: page ? page : CONSTANT.DEFAULT_PAGE,
      limit: limit ? limit : CONSTANT.DEFAULT_LIMIT
    };
    try {
      var whereObj = {};
      var isMinistry = false;
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            upkIdOwner: user.upkId
          };
          break;
        case "SUDIN_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
            sudinKabKotaIdOwner: user.sudinKabKotaId
          };
          break;
        case "PROVINCE_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
            provinceIdOwner: user.provinceId
          };
          break;
        case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
          isMinistry = true;
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY
          };
          break;
      }
      if (status && status !== "ALL") {
        Object.assign(whereObj, {
          lastApprovalStatus: status
        });
      }
      if (isRegular && isRegular !== "ALL") {
        Object.assign(whereObj, {
          isRegular: isRegular
        });
      }
      // const orderList = await OrderItem.findAndCountAll(
      //   whereObj,
      //   params,
      //   isMinistry
      // );
      const orderList = await OrderItem.findAndCountAll({
        include: [
          {
            model: Medicine,
            required: true
          },
          {
            model: Order,
            required: true,
            where: whereObj,
            include: [
              {
                model: Upk,
                required: false,
                as: "upkOwnerData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaOwnerData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceOwnerData",
                attributes: ["name"]
              },
              {
                model: Upk,
                required: false,
                as: "upkForData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaForData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceForData",
                attributes: ["name"]
              }
            ],
          },
        ],
        order: [
          ['order_id', 'DESC']
        ]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList.rows,
        // paging: {
        //   page: Number(page),
        //   size: orderList.rows.length,
        //   total: orderList.count
        // }
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    createOrdersLog,
    updateOrder,
    confirmOrder,
    getOrderDetail,
    getOrdersLog,
    getOrdersLogIn,
    getOrdersLogAvailable,
    getOrdersLogAvailableDown,
    createOrderReguler,
    createReguler,
    getOrdersLogAvailableUp,
    reportRelokasi
  };
};

module.exports = OrderController;
