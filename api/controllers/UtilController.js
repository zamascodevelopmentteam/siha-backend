const moment = require("moment");
const path = require("path");

const SheetJS = require("../libraries/sheetJS.class");

const ResponseService = require("../services/response.service")();
const VariableService = require("../services/variable.service")();
const ENUM = require("../../config/enum");

const UtilController = () => {
  const getEnum = (req, res, next) => {
    res.status(200).json({
      status: 200,
      data: ENUM
    });
  };

  const downloadSheetJS = async (req, res, next) => {
    try {
      const fileName = `Coba file excel_${moment().format()}.xlsx`;
      const filePath = path.join(
        VariableService.getTmpDownloadPath(), // ditaruh di folder download/tmp
        fileName // ini nama file excel-nya
      );

      const sheetJS = new SheetJS({
        filePath: filePath // set lokasi file excel yg akan disimpan
      });

      sheetJS.setCellValue("A1", "ini contoh isian di A1");
      sheetJS.setCellValue("B1", "ini contoh isian di B1");
      sheetJS.setCellValue("B2", "ini contoh isian di B2");

      const result = sheetJS.generate(); // jgn lupa panggil ini untuk save file excel

      await ResponseService.download(res, result.filePath, fileName, true); // ini untuk download file, kemudian hapus kalo user beres download
    } catch (error) {
      next(error);
    }
  };

  return {
    getEnum,
    downloadSheetJS
  };
};

module.exports = UtilController;
