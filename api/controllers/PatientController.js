const moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const DataTable = require("../libraries/dataTable.class");

const NotificationService = require("../services/notification.service")();
const authService = require("../services/auth.service");

const Patient = require("../models/Patient");
const User = require("../models/User");
const Visit = require("../models/Visit");
const Treatment = require("../models/Treatment");
const Medicine = require("../models/Medicine");
const Upk = require("../models/Upk");
const SudinKabKota = require("../models/SudinKabKota");
const Province = require("../models/Province");
const PatientLog = require("../models/PatientLog");
const TestHiv = require("../models/TestHiv");
const TestIms = require("../models/TestIms");
const TestVl = require("../models/TestVl");
const Prescription = require("../models/Prescription");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");

const bcryptService = require("../services/bcrypt.service");
const VisitService = require("../services/visit.service")();
const PatientService = require("../services/patient.service")();
const QueryService = require("../services/query.service")();
const ENUM = require("../../config/enum");
const ROLE = "PATIENT";

const PatientController = () => {
  const create = async (req, res, next) => {
    const {
      nik,
      name,
      email,
      phone,
      lsmPenjangkau,
      kelompokPopulasi,
      addressKTP,
      addressDomicile,
      dateBirth,
      gender,
      statusPatient,
      weight,
      height,
      namaPmo,
      hubunganPmo,
      noHpPmo,
      motherNik
    } = req.body;
    const {
      id,
      upkId,
      provinceId,
      sudinKabKotaId
    } = req.user;
    try {
      let userParams = {
        nik,
        name,
        email,
        phone,
        upkId,
        provinceId,
        sudinKabKotaId,
        isActive: false,
        createdBy: id,
        role: ROLE,
      };

      let patientParams = {
        nik,
        name,
        addressKTP,
        addressDomicile,
        dateBirth,
        gender,
        email,
        lsmPenjangkau,
        kelompokPopulasi,
        statusPatient:
          statusPatient === ENUM.STATUS_PATIENT.ODHA
            ? ENUM.STATUS_PATIENT.ODHA
            : (statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA ? ENUM.STATUS_PATIENT.ODHA : ENUM.STATUS_PATIENT.BLM_TAHU),
        phone,
        weight,
        height,
        namaPmo,
        hubunganPmo,
        noHpPmo,
        createdBy: id,
        upkId,
        motherNik,
        odhaLama: statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA ? true : false
      };

      if (!patientParams.nik) {
        const upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        const countNoNIK = await Patient.count({
          where: {
            nik: {
              [Op.startsWith]: "99"
            }
          }
        });

        const orderNumberNoNik =
          isNaN(countNoNIK) || !countNoNIK ? 1 : countNoNIK + 1;
        let nik;
        if (upk) {
          nik =
            "99." +
            upk.id.toString() +
            "-" +
            upk.sudinKabKota.id.toString() +
            "-" +
            upk.sudinKabKota.provinceId.toString() +
            "." +
            orderNumberNoNik.toString();
        } else {
          nik = "99." + new Date().getTime().toString();
        }
        userParams.nik = nik;
        patientParams.nik = nik;
      } else {
        var userCheck = await User.findOne({
          where: {
            nik: patientParams.nik
          }
        });
        if (userCheck) {
          throw {
            message: "Duplikasi, NIK sudah terdaftar"
          };
        }
      }

      if (statusPatient == ENUM.STATUS_PATIENT.HIV_NEGATIF) {
        delete userParams["email"];
        delete userParams["namaPmo"];
        delete userParams["hubunganPmo"];
        delete userParams["noHpPmo"];
      }

      const userPatient = await User.create(userParams).then(async value => {
        patientParams.userId = value.id;
        const newPatient = await Patient.create(patientParams);
        if (
          newPatient &&
          newPatient.id & (statusPatient == ENUM.STATUS_PATIENT.ODHA || statusPatient == ENUM.STATUS_PATIENT.ODHA_LAMA)
        ) {
          PatientService.updateMetaPatient(newPatient.id, {
            convertToODHADate: moment()
              .local()
              .format()
          });
        }
        return newPatient;
      });

      res.status(200).json({
        status: 200,
        message: "Success",
        data: userPatient
      });
    } catch (error) {
      next(error);
    }
  };

  const updatePatient = async (req, res, next) => {
    const { id } = req.user;
    const {
      nik,
      name,
      addressKTP,
      addressDomicile,
      dateBirth,
      tglMeninggal,
      gender,
      statusPatient,
      phone,
      email,
      weight,
      height,
      namaPmo,
      hubunganPmo,
      lsmPenjangkau,
      kelompokPopulasi,
      noHpPmo,
      motherNik
    } = req.body;
    try {
      let params = {
        nik,
        name,
        addressKTP,
        addressDomicile,
        dateBirth,
        gender,
        phone,
        email,
        weight,
        height,
        namaPmo,
        hubunganPmo,
        noHpPmo,
        lsmPenjangkau,
        kelompokPopulasi,
        statusPatient: statusPatient ? statusPatient : undefined,
        tglMeninggal: tglMeninggal || null,
        updatedBy: id,
        motherNik
      };

      const patient = await Patient.update(params, {
        where: {
          id: req.patient.id
        },
        returning: true,
        plain: true
      });

      const user = await User.update(
        {
          nik,
          name,
          email,
          phone,
          updatedBy: id
        },
        {
          where: {
            id: req.patient.userId
          },
          returning: true,
          plain: true
        }
      );

      res.status(200).json({
        status: 200,
        message: "Success",
        data: patient
      });
    } catch (error) {
      next(error);
    }
  };

  const convertODHAWithAccount = async (req, res, next) => {
    const { patientId } = req.params;
    const { body } = req;
    try {
      const patient = await Patient.findByPk(patientId);
      const user = await User.findByPk(patient.userId, {
        attributes: { include: ["password", "refreshToken"] }
      });

      if (!user || (user && user.isActive)) {
        throw {
          status: 400,
          message: "user_is_not_found_or_already_activated"
        };
      }

      if (patient.statusPatient == ENUM.STATUS_PATIENT.HIV_NEGATIF) {
        throw {
          status: 400,
          message: "patient_is_hiv_negative"
        };
      } else if (patient.statusPatient == ENUM.STATUS_PATIENT.ODHA || patient.statusPatient == ENUM.STATUS_PATIENT.ODHA_LAMA) {
        PatientService.updateMetaPatient(patientId, {
          convertToODHADate: moment()
            .local()
            .format()
        });
      }

      const otpCode = Date.now()
        .toString()
        .substring(9);
      user.password = bcryptService().password(otpCode);
      user.phone = body.phone;
      // user.isActive = true;
      user.updatedBy = req.user.id;

      patient.statusPatient = ENUM.STATUS_PATIENT.ODHA;
      patient.phone = body.phone;
      patient.namaPmo = body.namaPmo;
      patient.hubunganPmo = body.hubunganPmo;
      patient.noHpPmo = body.noHpPmo;
      patient.upkId = req.user.upkId;

      PatientService.sendSmsOTP(
        req.user,
        user.id,
        patient.id,
        user.phone,
        otpCode,
        true
      );

      await authService().issueRefresh(patient.userId);
      await user.save();
      await patient.save();

      res.status(200).json({
        status: 200,
        message: "Success",
        data: patient
      });
    } catch (error) {
      next(error);
    }
  };

  const convertODHAWithoutAccount = async (req, res, next) => {
    const { patientId } = req.params;
    const { body } = req;
    try {
      const patient = await Patient.findByPk(patientId);
      const user = await User.findByPk(patient.userId);

      const otpCode = Date.now()
        .toString()
        .substring(9);

      if (!user) {
        throw {
          status: 400,
          message: "user_is_not_found"
        };
      }

      if (patient.statusPatient == ENUM.STATUS_PATIENT.HIV_NEGATIF) {
        throw {
          status: 400,
          message: "patient_is_hiv_negative"
        };
      } else if (patient.statusPatient == ENUM.STATUS_PATIENT.ODHA || patient.statusPatient == ENUM.STATUS_PATIENT.ODHA_LAMA) {
        PatientService.updateMetaPatient(patientId, {
          convertToODHADate: moment()
            .local()
            .format()
        });
      }

      user.password = bcryptService().password(otpCode);
      user.phone = body.phone;
      user.isActive = false;
      user.updatedBy = req.user.id;

      patient.statusPatient = ENUM.STATUS_PATIENT.ODHA;
      patient.phone = body.phone;
      patient.namaPmo = body.namaPmo;
      patient.hubunganPmo = body.hubunganPmo;
      patient.noHpPmo = body.noHpPmo;
      patient.upkId = req.user.upkId;

      await user.save();
      await patient.save();

      res.status(200).json({
        status: 200,
        message: "Success",
        data: patient
      });
    } catch (error) {
      next(error);
    }
  };

  const getOtp = async (req, res, next) => {
    const { patientId } = req.params;
    try {
      const patient = await Patient.findByPk(patientId, {
        include: [
          {
            model: User,
            required: true,
            as: "user"
          }
        ]
      });
      const user = patient.user;

      if (!user || (user && user.isActive)) {
        throw {
          status: 400,
          message: "user_is_not_found_or_already_activated"
        };
      }

      if (
        patient &&
        (patient.statusPatient == ENUM.STATUS_PATIENT.HIV_NEGATIF ||
          patient.statusPatient == ENUM.STATUS_PATIENT.BLM_TAHU)
      ) {
        throw {
          status: 400,
          message: "patient_is_hiv_negative"
        };
      }

      const otpCode = Date.now()
        .toString()
        .substring(9);

      user.password = bcryptService().password(otpCode);
      await user.save();

      PatientService.sendSmsOTP(
        req.user,
        user.id,
        patient.id,
        user.phone,
        otpCode
      );
      await authService().issueRefresh(user.id);

      res.status(200).json({
        status: 200,
        message: "Success",
        data: user
      });
    } catch (error) {
      next(error);
    }
  };

  const submitOtp = async (req, res, next) => {
    const { patientId } = req.params;
    const { otpCode } = req.body;
    try {
      const patient = await Patient.findByPk(patientId, {
        include: [
          {
            model: User,
            required: true,
            // attributes: ["id", "password"],
            attributes: { include: ["password", "refreshToken"] },
            as: "user"
          }
        ]
      });
      const user = patient.user;

      if (!user || (user && user.isActive)) {
        throw {
          status: 400,
          message: "user_is_not_found_or_already_activated"
        };
      }

      if (bcryptService().comparePassword(otpCode || "xxxxxx", user.password)) {
        user.isActive = true;
        user.updatedBy = req.user.id;
        await user.save();

        res.send({
          message: "Success",
          user
        });
      } else {
        throw {
          status: 400,
          message: "otp_is_invalid"
        };
      }
    } catch (error) {
      next(error);
    }
  };

  const getPatients = async (req, res, next) => {
    const { keyword, startDate, endDate } = req.query;
    try {
      // const startDate = moment()
      //   .date(startDay)
      //   .month(currentMonth)
      //   .subtract(1, "months")
      //   .format("YYYY-MM-DD");
      // const endDate = moment()
      //   .date(endDay)
      //   .month(currentYear)
      //   .format("YYYY-MM-DD");

      let whereVisitDate = {};

      if (startDate && endDate) {
        whereVisitDate = {
          visitDate: { [Op.between]: [startDate, endDate] }
        };
      }

      let queryParams = {};
      if (keyword) {
        queryParams = QueryService.filterPatientsParamsBuilder(keyword);
      }

      const dataTable = new DataTable(Patient, req);
      dataTable.setIncludeParams([
        {
          model: User,
          required: false,
          as: "user",
          attributes: ["id", "nik", "name", "phone", "upkId"]
        },
        {
          model: User,
          required: false,
          as: "createdByUser",
          attributes: ["id", "nik", "name", "phone", "upkId"]
        },
        { model: Upk, required: true, attributes: ["id", "name"] },
        {
          model: Visit,
          required: (startDate && endDate) ? true : false,
          attributes: ["visitDate"],
          where: (startDate && endDate) ? whereVisitDate : {}
        },
      ]);
      dataTable.addWhereParams(
        Object.assign(
          {
            upkId: req.user.upkId
          },
          queryParams
        )
      );
      await dataTable.buildResult();
      res.json(dataTable.getResult());
    } catch (error) {
      next(error);
    }
  };

  const getDetail = async (req, res, next) => {
    const { patientId } = req.params;
    try {
      const user = await Patient.findByPk(patientId, {
        include: [
          {
            model: Upk,
            attributes: ["name", "address"],
            include: [
              {
                model: SudinKabKota,
                as: "sudinKabKota",
                attributes: ["name"],
                include: [{ model: Province, attributes: ["name"] }]
              }
            ]
          },
          {
            model: User,
            as: "user"
          }
        ]
      });

      res.send({
        status: 200,
        message: "Success",
        data: user
      });
    } catch (error) {
      next(error);
    }
  };

  const getPatientByNik = async (req, res, next) => {
    const { nik } = req.params;
    try {
      const user = await Patient.findOne({
        where: {
          nik: nik
        },
        include: [
          {
            model: User,
            required: false,
            as: "user",
            attributes: ["id", "nik", "name", "phone", "upkId", "isActive"]
          },
          {
            model: User,
            required: false,
            as: "createdByUser",
            attributes: ["id", "nik", "name", "phone", "upkId"]
          },
          { model: Upk, required: false, attributes: ["id", "name"] }
        ]
      });

      res.send({
        status: 200,
        message: "Success",
        data: user
      });
    } catch (error) {
      next(error);
    }
  };

  const getVisit = async (req, res, next) => {
    const { type } = req.params;
    const { page, limit } = req.query;
    try {
      const patient = await Patient.findOne({
        where: {
          userId: req.user.id
        }
      });

      if (!patient) {
        throw new Error("Patient record is not found!");
      }

      const dataTable = new DataTable(Visit, req);
      dataTable.setIncludeParams([
        {
          model: Patient,
          required: true,
          attributes: ["id", "nik", "name", "statusPatient", "upkId"]
        },
        {
          model: Prescription,
          required: false,
          include: [
            {
              model: PrescriptionMedicine,
              include: [
                {
                  model: Medicine,
                  required: true,
                  attributes: ["name", "codeName"]
                }
              ]
            }
          ]
        },
        {
          model: Treatment,
          required: false,
          include: [
            { model: Medicine, required: false, as: "obatArv1Data" },
            { model: Medicine, required: false, as: "obatArv2Data" },
            { model: Medicine, required: false, as: "obatArv3Data" }
          ]
        },
        {
          model: TestHiv,
          required: false
        },
        {
          model: TestIms,
          required: false
        },
        {
          model: TestVl,
          required: false
        },
        {
          model: Upk,
          required: false,
          attributes: ["name"]
        },
        {
          model: User,
          required: false,
          as: "petugasRR",
          attributes: ["name", "nik"]
        }
      ]);
      dataTable.addWhereParams({
        patientId: patient.id,
        visitType: type
      });
      await dataTable.buildResult();
      res.json(dataTable.getResult());
    } catch (err) {
      next(err);
    }
  };

  const getTodayVisit = async (req, res, next) => {
    const { patientId } = req.params;
    let patient = req.patient;

    try {
      const visit = await VisitService.getVisitDetail({
        patientId: patient.id,
        visitDate: {
          [Op.between]: [
            moment()
              .startOf("day")
              .valueOf(),
            moment()
              .endOf("day")
              .valueOf()
          ]
        }
      });

      res.send({
        status: 200,
        message: "Success",
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  const updateSisa = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const patient = await Patient.findOne({
        where: {
          userId: req.user.id
        }
      });

      if (!patient) {
        throw new Error("patient_not_found");
      }

      const prevTreatment = await VisitService.getDetailTreatmentPrevVisit(
        visitId
      );

      if (prevTreatment) {
        prevTreatment.sisaObatArv1 = req.body.sisaObatArv1;
        prevTreatment.sisaObatArv2 = req.body.sisaObatArv2;
        prevTreatment.sisaObatArv3 = req.body.sisaObatArv3;
        await prevTreatment.save();
      } else {
        throw {
          status: 400,
          message: "no_previous_treatment_found"
        };
      }

      res.send({
        status: 200,
        message: "success",
        data: prevTreatment
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    create,
    updatePatient,
    convertODHAWithAccount,
    convertODHAWithoutAccount,
    getOtp,
    submitOtp,
    getDetail,
    getPatients,
    getPatientByNik,
    getVisit,
    getTodayVisit,
    updateSisa
  };
};

module.exports = PatientController;
