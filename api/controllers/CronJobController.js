const moment = require("moment");
const path = require("path");

const SheetJS = require("../libraries/sheetJS.class");

const ResponseService = require("../services/response.service")();
const VariableService = require("../services/variable.service")();
const AgendaService = require("../services/agenda.service")();
const ENUM = require("../../config/enum");

const Controller = () => {
  const generatePermintaanRegulerARV_upk = (req, res, next) => {
    try {
      AgendaService.getAgendaInstance().now("generatePermintaanRegulerUPK");
      res.status(200).json({
        status: 200
      });
    } catch (error) {
      next(error);
    }
  };
  const generatePermintaanRegulerARV_sudin = (req, res, next) => {
    try {
      AgendaService.getAgendaInstance().now("generatePermintaanRegulerSudin");
      res.status(200).json({
        status: 200
      });
    } catch (error) {
      next(error);
    }
  };
  const generatePermintaanRegulerARV_province = (req, res, next) => {
    try {
      AgendaService.getAgendaInstance().now(
        "generatePermintaanRegulerProvince"
      );
      res.status(200).json({
        status: 200
      });
    } catch (error) {
      next(error);
    }
  };

  const notifyPatientsH_7 = async (req, res, next) => {
    try {
      AgendaService.getAgendaInstance().now("notifyPatientsAWeekBefore");
      res.status(200).json({
        status: 200
      });
    } catch (error) {
      next(error);
    }
  };
  const notifyPatientsH_1 = async (req, res, next) => {
    try {
      AgendaService.getAgendaInstance().now("notifyPatientsADayBefore");
      res.status(200).json({
        status: 200
      });
    } catch (error) {
      next(error);
    }
  };
  const notifyPatientsH_0 = async (req, res, next) => {
    try {
      AgendaService.getAgendaInstance().now("notifyPatientsAtTheDay");
      res.status(200).json({
        status: 200
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    generatePermintaanRegulerARV_upk,
    generatePermintaanRegulerARV_sudin,
    generatePermintaanRegulerARV_province,
    notifyPatientsH_7,
    notifyPatientsH_1,
    notifyPatientsH_0
  };
};

module.exports = Controller;
