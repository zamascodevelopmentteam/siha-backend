const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const ENUM = require("../../config/enum");

const CohortClass = require("../services/report/cohort.class");
const MasterHivClass = require("../services/report/master_hiv.class");
const MasterImsClass = require("../services/report/master_ims.class");
// const VariableService = require("../services/variable.service");

const Upk = require("../models/Upk");
const SudinKabKota = require("../models/SudinKabKota");
const Province = require("../models/Province");

const XLSX = require("xlsx");

const sequelize = require("../../config/database");
const Medicine = require("../models/Medicine");
const NonArvMedicine = require("../models/NonArvMedicine");
const TestVl = require("../models/TestVl");
const Visit = require("../models/Visit");
const Patient = require("../models/Patient");

const ReportIntraController = () => {
  const getCohortReport = async (req, res, next) => {
    try {
      const cohortClass = new CohortClass(req.user, req);
      await cohortClass.buildReport();
      res.json(cohortClass.getReport());
    } catch (error) {
      next(error);
    }
  };

  const getMasterHivReport = async (req, res, next, returnItem = false) => {
    try {
      let month2 = null;
      let year2 = null;
      const { month } = req.query;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
      }
      const masterHivClass = new MasterHivClass(req.user, req, month2, year2);
      await masterHivClass.buildReport();
      const data = masterHivClass.getReport();
      if (returnItem) {
        return data;
      }
      res.json(data);
    } catch (error) {
      next(error);
    }
  };

  const getMasterHivReport2 = async (req, res, next, returnItem = false) => {
    try {
      let upk, sudinKabKota, province;
      const { month, provinceId, sudinKabKotaId, upkId } = req.query;
      const roleId = req.user.role;
      // meta
      if (upkId) {
        upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (upk) {
          sudinKabKota = upk.sudinKabKota;
          province = upk.sudinKabKota.province;
        }
      } else if (sudinKabKotaId) {
        sudinKabKota = await SudinKabKota.findByPk(sudinKabKotaId, {
          include: [{ model: Province, required: true }]
        });
        if (sudinKabKota) {
          province = sudinKabKota.province;
        }
      } else if (provinceId) {
        province = await Province.findByPk(provinceId);
      }
      // end meta

      let tgl;
      let month2 = null;
      let year2 = null;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
        tgl = year2 + "-" + month2;
      }

      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( tanggal_test, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (provinceId) {
        where.push(` provinces.ID = ${provinceId} `);
      }
      if (sudinKabKotaId) {
        where.push(` sudin_kab_kota.ID = ${sudinKabKotaId} `);
      }
      if (upkId) {
        where.push(` upk.ID = ${upkId} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          patients.fullname,
          EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
          patients.gender,
          patients.kelompok_populasi,
          th.kesimpulan_hiv
        FROM
          test_hiv th
          JOIN visits
          JOIN patients
          JOIN upk
          JOIN sudin_kab_kota ON sudin_kab_kota."id" = upk.sudin_kab_kota_id ON upk."id" = patients.upk_id ON patients."id" = visits.patient_id ON visits."id" = th.visit_id
          JOIN provinces ON provinces.ID = sudin_kab_kota.province_id
        ${newWhere}
      `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      const kesi = ["WPS", "Waria", "Penasun", "LSL", "Bumil", "Pasien TB", "Pasien IMS", "Pasien Hepatitis", "Pasangan ODHA", "Anak ODHA", "WBP", "Populasi Umum"];

      let result1 = [];
      let result2 = [];
      let total1 = [];
      let total2 = [];

      kesi.map((r, i) => {
        result1[i] = {
          label: (i + 1) + ". " + r,
          value: r,
          data: []
        };

        result2[i] = {
          label: (i + 1) + ". " + r,
          value: r,
          data: []
        };

        for (let ii = 0; ii <= 19; ii++) {
          result1[i].data[ii] = 0;
          result2[i].data[ii] = 0;
          total1[ii] = 0;
          total2[ii] = 0;
        }
      });

      const _insertData = (result, total, r, l, p, positif = false) => {
        if (!positif) {
          if (r.gender == ENUM.GENDER.LAKI_LAKI) {
            total[l] += 1;
            result.map((rr, i) => {
              if (r.kelompok_populasi.includes(rr.value)) {
                result[i].data[l] += 1;
              }
            });
          } else {
            total[p] += 1;
            result.map((rr, i) => {
              if (r.kelompok_populasi.includes(rr.value)) {
                result[i].data[p] += 1;
              }
            });
          }
        } else {
          if (r.kesimpulan_hiv == "REAKTIF") {
            if (r.gender == ENUM.GENDER.LAKI_LAKI) {
              total[l] += 1;
              result.map((rr, i) => {
                if (r.kelompok_populasi.includes(rr.value)) {
                  result[i].data[l] += 1;
                }
              });
            } else {
              total[p] += 1;
              result.map((rr, i) => {
                if (r.kelompok_populasi.includes(rr.value)) {
                  result[i].data[p] += 1;
                }
              });
            }
          }
        }

      }

      data.map(r => {
        if (r.age < 1) {
          _insertData(result1, total1, r, 0, 10);
          _insertData(result2, total2, r, 0, 10, true);
        } else if (r.age >= 1 && r.age <= 14) {
          _insertData(result1, total1, r, 1, 11);
          _insertData(result2, total2, r, 1, 11, true);
        } else if (r.age >= 15 && r.age <= 19) {
          _insertData(result1, total1, r, 2, 12);
          _insertData(result2, total2, r, 2, 12, true);
        } else if (r.age >= 20 && r.age <= 24) {
          _insertData(result1, total1, r, 3, 13);
          _insertData(result2, total2, r, 3, 13, true);
        } else if (r.age >= 25 && r.age <= 29) {
          _insertData(result1, total1, r, 4, 14);
          _insertData(result2, total2, r, 4, 14, true);
        } else if (r.age >= 30 && r.age <= 34) {
          _insertData(result1, total1, r, 5, 15);
          _insertData(result2, total2, r, 5, 15, true);
        } else if (r.age >= 35 && r.age <= 39) {
          _insertData(result1, total1, r, 6, 16);
          _insertData(result2, total2, r, 6, 16, true);
        } else if (r.age >= 40 && r.age <= 44) {
          _insertData(result1, total1, r, 7, 17);
          _insertData(result2, total2, r, 7, 17, true);
        } else if (r.age >= 45 && r.age <= 49) {
          _insertData(result1, total1, r, 8, 18);
          _insertData(result2, total2, r, 8, 18, true);
        } else if (r.age >= 50) {
          _insertData(result1, total1, r, 9, 19);
          _insertData(result2, total2, r, 9, 19, true);
        }
      });

      const rest = {
        content: {
          diTestHivList: {
            data: result1,
            title: "Jumlah orang yang dites HIV",
            total: total1
          },
          testHivPositifList: {
            data: result2,
            title: "Jumlah orang yang HIV positif",
            total: total2
          }
        },
        meta: {
          upk,
          sudinKabKota,
          province
        }
      };

      if (returnItem) {
        return rest;
      }

      res.json(rest);
    } catch (error) {
      next(error);
    }
  };

  const getMasterHivReportById = async (req, res, next, returnItem = false) => {
    try {
      let month2 = null;
      let year2 = null;
      const { month } = req.query;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
      }
      const masterHivClass = new MasterHivClass(req.user, req, month2, year2);
      await masterHivClass.buildReportById();
      const data = masterHivClass.getReport();
      if (returnItem) {
        return data;
      }
      res.json(data);
    } catch (error) {
      next(error);
    }
  };

  const getMasterImsReport = async (req, res, next) => {
    try {
      const masterImsClass = new MasterImsClass(req.user, req);
      await masterImsClass.buildReport();
      res.json(masterImsClass.getReport());
    } catch (error) {
      next(error);
    }
  };

  const getMasterHivReportExcel = async (req, res, next) => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    let dateObj = new Date();
    let monthh = dateObj.getMonth();
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let date = day + '-' + monthh + '-' + year;
    let dateMoth = day + ' ' + monthNames[monthh - 1] + ' ' + year;

    let month2 = null;
    let year2 = null;
    let { month, meta } = req.query;
    // console.log(meta, 'aaaaaaaaaaaaa');
    // meta = JSON.parse(meta)
    if (month) {
      month2 = parseInt(month.split('-')[1]) - 1;
      year2 = month.split('-')[0];
    }

    // const dataById = await getMasterHivReportById(req, res, next, true);
    const data = await getMasterHivReport2(req, res, next, true);
    meta = data.meta;
    let total = new Array();
    const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X"];
    const sheetObj = {
      "Laporan HIV": {
        "!ref": "A1:X40",
        // ---------------------------
        A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
        A2: { t: 's', v: meta && meta.upk && meta.upk.name ? meta.upk.name : "-" },
        A3: { t: 's', v: "Nama Kab/Kota" },
        A4: { t: 's', v: meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
        A5: { t: 's', v: "Nama Provinsi" },
        A6: { t: 's', v: meta && meta.province && meta.province.name ? meta.province.name : "-" },
        // ---------------------------
        C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
        C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
        C3: { t: 's', v: "Tahun" },
        C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
        C5: { t: 's', v: "Tanggal Akses" },
        C6: { t: 's', v: dateMoth },

        A7: { t: 's', v: "Indikator" },
        B7: { t: 's', v: "Laki-laki" },
        M7: { t: 's', v: "Perempuan" },
        X7: { t: 's', v: "Total" },
        // ---------------------------
        B8: { t: 's', v: "< 1" },
        C8: { t: 's', v: "1-14" },
        D8: { t: 's', v: "15-19" },
        E8: { t: 's', v: "20-24" },
        F8: { t: 's', v: "25-29" },
        G8: { t: 's', v: "30-34" },
        H8: { t: 's', v: "35-39" },
        I8: { t: 's', v: "40-44" },
        J8: { t: 's', v: "45-49" },
        K8: { t: 's', v: "> 50" },
        L8: { t: 's', v: "Jumlah" },
        M8: { t: 's', v: "< 1" },
        N8: { t: 's', v: "1-14" },
        O8: { t: 's', v: "15-19" },
        P8: { t: 's', v: "20-24" },
        Q8: { t: 's', v: "25-29" },
        R8: { t: 's', v: "30-34" },
        S8: { t: 's', v: "35-39" },
        T8: { t: 's', v: "40-44" },
        U8: { t: 's', v: "45-49" },
        V8: { t: 's', v: "> 50" },
        W8: { t: 's', v: "Jumlah" },
        "!cols": [
          { wch: 30 }
        ],
        "!merges": [
          { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
          { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
          { s: { r: 6, c: 12 }, e: { r: 6, c: 22 } },
          { s: { r: 6, c: 23 }, e: { r: 7, c: 23 } },
          // ----------------------------------------------
          // { s: { r: 15, c: 1 }, e: { r: 15, c: 10 } },
          // { s: { r: 15, c: 12 }, e: { r: 15, c: 21 } },
          // { s: { r: 29, c: 1 }, e: { r: 29, c: 10 } },
          // { s: { r: 29, c: 12 }, e: { r: 29, c: 21 } },
        ]
      }
    };
    // ---------------------------------------------------
    // {
    //   dataById && Object.keys(dataById.content).map((key, idx) => {
    //     if (total[idx] == undefined) {
    //       total[idx] = new Array();
    //     }
    //     dataById.content[key].data.map((value) => {
    //       value.data.slice(0, 10).map((valueCount, idxCount) => {
    //         if (total[idx][0] == undefined) {
    //           total[idx][0] = new Array();
    //         }
    //         if (total[idx][0][idxCount] === undefined) {
    //           total[idx][0][idxCount] = 0;
    //         }
    //         total[idx][0][idxCount] += valueCount;
    //       });
    //       value.data.slice(10, 20).map((valueCount, idxCount) => {
    //         if (total[idx][1] == undefined) {
    //           total[idx][1] = new Array();
    //         }
    //         if (total[idx][1][idxCount] === undefined) {
    //           total[idx][1][idxCount] = 0;
    //         }
    //         total[idx][1][idxCount] += valueCount;
    //       })
    //     })
    //   })
    // }
    // ---------------------------------------------------
    let row = 9;
    {
      Object.keys(data.content).map((key, idx) => {
        let granTotalMale = 0;
        let granTotalFemale = 0;
        let sumMaleContent = 0;
        let sumFemaleContent = 0;

        sheetObj["Laporan HIV"]["A" + row] = { t: 's', v: data.content[key].title };
        {
          data.content[key] && data.content[key].total.slice(0, 10).map((v, i) => {
            granTotalMale += v;
            sheetObj["Laporan HIV"][indexCols[i + 1] + row] = { t: 'n', v: v };
          })
        }
        sheetObj["Laporan HIV"]["L" + row] = { t: 'n', v: granTotalMale };
        {
          data.content[key] && data.content[key].total.slice(10, 20).map((v, i) => {
            granTotalFemale += v;
            sheetObj["Laporan HIV"][indexCols[i + 12] + row] = { t: 'n', v: v };
          })
        }
        sheetObj["Laporan HIV"]["W" + row] = { t: 'n', v: granTotalFemale };
        sheetObj["Laporan HIV"]["X" + row] = { t: 'n', v: granTotalMale + granTotalFemale };

        row++;

        {
          data.content[key].data.map((value, idxData) => {
            let sumMale = 0;
            let sumFemale = 0;
            sheetObj["Laporan HIV"]["A" + row] = { t: 's', v: value.label };
            {
              value.data
                .slice(0, 10)
                .map((valueCount, i) => {
                  sumMale = sumMale + valueCount;
                  sumMaleContent = sumMaleContent + valueCount;
                  sheetObj["Laporan HIV"][indexCols[i + 1] + row] = { t: 'n', v: !["WPS", "Bumil"].includes(value.value) ? valueCount : '' };
                })
            }
            sheetObj["Laporan HIV"]["L" + row] = { t: 'n', v: !["WPS", "Bumil"].includes(value.value) ? sumMale : '' };
            {
              value.data
                .slice(10, 20)
                .map((valueCount, i) => {
                  sumFemale = sumFemale + valueCount;
                  sumFemaleContent = sumFemaleContent + valueCount;
                  sheetObj["Laporan HIV"][indexCols[i + 12] + row] = { t: 'n', v: !["Waria", "LSL"].includes(value.value) ? valueCount : '' };
                })
            }
            sheetObj["Laporan HIV"]["W" + row] = { t: 'n', v: !["Waria", "LSL"].includes(value.value) ? sumFemale : '' };
            sheetObj["Laporan HIV"]["X" + row] = { t: 'n', v: sumMale + sumFemale };

            row++;
          })
        }
        // sheetObj["Laporan HIV"]["!merges"].push({ s: { r: row, c: 1 }, e: { r: row, c: 10 } });
        sheetObj["Laporan HIV"]["B" + row] = { t: 's', v: "Jumlah Kasus Laki-laki Berdasarkan Kelompok Populasi" };
        sheetObj["Laporan HIV"]["L" + row] = { t: 'n', v: sumMaleContent };

        // sheetObj["Laporan HIV"]["!merges"].push({ s: { r: row, c: 12 }, e: { r: row, c: 21 } });
        sheetObj["Laporan HIV"]["M" + row] = { t: 's', v: "Jumlah Kasus Perempuan Berdasarkan Kelompok Populasi" };
        sheetObj["Laporan HIV"]["W" + row] = { t: 'n', v: sumFemaleContent };
        sheetObj["Laporan HIV"]["X" + row] = { t: 'n', v: sumMaleContent + sumFemaleContent };

        row++;
      })
    }
    // console.log(sheetObj);
    console.log("excel");

    XLSX.writeFile({
      SheetNames: ["Laporan HIV"],
      Sheets: sheetObj
    }, './api/excel/LaporanHIV-' + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
    return res.send({
      status: 200,
      message: "Success",
      data: "/api/excel/LaporanHIV-" + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
    });
  }

  const getTestHivReport = async (req, res, next) => {
    try {
      let upk, sudinKabKota, province;
      const { month, provinceId, sudinKabKotaId, upkId, excel } = req.query;
      const roleId = req.user.role;
      // meta
      if (upkId) {
        upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (upk) {
          sudinKabKota = upk.sudinKabKota;
          province = upk.sudinKabKota.province;
        }
      } else if (sudinKabKotaId) {
        sudinKabKota = await SudinKabKota.findByPk(sudinKabKotaId, {
          include: [{ model: Province, required: true }]
        });
        if (sudinKabKota) {
          province = sudinKabKota.province;
        }
      } else if (provinceId) {
        province = await Province.findByPk(provinceId);
      }
      // end meta

      let tgl;
      let month2 = null;
      let year2 = null;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
        tgl = year2 + "-" + month2;
      }

      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( tanggal_test, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (provinceId) {
        where.push(` provinces.ID = ${provinceId} `);
      }
      if (sudinKabKotaId) {
        where.push(` sudin_kab_kota.ID = ${sudinKabKotaId} `);
      }
      if (upkId) {
        where.push(` upk.ID = ${upkId} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          to_char( th.tanggal_test, 'YYYY' ) AS tahun,
          to_char( th.tanggal_test, 'MM' ) AS bulan,
          provinces.NAME AS provinsi,
          sudin_kab_kota.NAME AS kabkota,
          upk.NAME AS upk,
          upk.id AS upk_id,
          patients.fullname,
          EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
          patients.gender,
          patients.kelompok_populasi,
          th.kesimpulan_hiv,
          th.tanggal_test 
        FROM
          test_hiv th
          JOIN visits
          JOIN patients
          JOIN upk
          JOIN sudin_kab_kota ON sudin_kab_kota."id" = upk.sudin_kab_kota_id ON upk."id" = patients.upk_id ON patients."id" = visits.patient_id ON visits."id" = th.visit_id
          JOIN provinces ON provinces.ID = sudin_kab_kota.province_id 
        ${newWhere}
        ORDER BY
          provinces.NAME ASC,
          sudin_kab_kota.NAME ASC,
          upk.NAME ASC,
          th.tanggal_test ASC
      `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      const kesi = ["WPS", "Waria", "Penasun", "LSL", "Bumil", "Pasien TB", "Pasien IMS", "Pasien Hepatitis", "Pasangan ODHA", "Anak ODHA", "WBP", "Populasi Umum"];

      let result1 = [];
      let result2 = [];
      let total1 = [];
      let total2 = [];
      let lastUpk = "";
      let rest = {
        content: {},
        meta: {
          upk,
          sudinKabKota,
          province
        }
      };

      const _insertData = (result, total, r, l, p, positif = false) => {
        if (!positif) {
          if (r.gender == ENUM.GENDER.LAKI_LAKI) {
            total[l] += 1;
            result.map((rr, i) => {
              if (r.kelompok_populasi.includes(rr.value)) {
                result[i].data[l] += 1;
              }
            });
          } else {
            total[p] += 1;
            result.map((rr, i) => {
              if (r.kelompok_populasi.includes(rr.value)) {
                result[i].data[p] += 1;
              }
            });
          }
        } else {
          if (r.kesimpulan_hiv == "REAKTIF") {
            if (r.gender == ENUM.GENDER.LAKI_LAKI) {
              total[l] += 1;
              result.map((rr, i) => {
                if (r.kelompok_populasi.includes(rr.value)) {
                  result[i].data[l] += 1;
                }
              });
            } else {
              total[p] += 1;
              result.map((rr, i) => {
                if (r.kelompok_populasi.includes(rr.value)) {
                  result[i].data[p] += 1;
                }
              });
            }
          }
        }

      }

      data.map(r => {
        if (lastUpk != r.upk_id) {
          lastUpk = r.upk_id;

          result1 = [];
          result2 = [];
          total1 = [];
          total2 = [];

          kesi.map((r, i) => {
            result1[i] = {
              label: "Jumlah orang yang dites HIV -" + r,
              value: r,
              data: []
            };

            result2[i] = {
              label: "Jumlah orang yang HIV -" + r,
              value: r,
              data: []
            };

            for (let ii = 0; ii <= 19; ii++) {
              result1[i].data[ii] = 0;
              result2[i].data[ii] = 0;
              total1[ii] = 0;
              total2[ii] = 0;
            }
          });
        }

        if (r.age < 1) {
          _insertData(result1, total1, r, 0, 10);
          _insertData(result2, total2, r, 0, 10, true);
        } else if (r.age >= 1 && r.age <= 14) {
          _insertData(result1, total1, r, 1, 11);
          _insertData(result2, total2, r, 1, 11, true);
        } else if (r.age >= 15 && r.age <= 19) {
          _insertData(result1, total1, r, 2, 12);
          _insertData(result2, total2, r, 2, 12, true);
        } else if (r.age >= 20 && r.age <= 24) {
          _insertData(result1, total1, r, 3, 13);
          _insertData(result2, total2, r, 3, 13, true);
        } else if (r.age >= 25 && r.age <= 29) {
          _insertData(result1, total1, r, 4, 14);
          _insertData(result2, total2, r, 4, 14, true);
        } else if (r.age >= 30 && r.age <= 34) {
          _insertData(result1, total1, r, 5, 15);
          _insertData(result2, total2, r, 5, 15, true);
        } else if (r.age >= 35 && r.age <= 39) {
          _insertData(result1, total1, r, 6, 16);
          _insertData(result2, total2, r, 6, 16, true);
        } else if (r.age >= 40 && r.age <= 44) {
          _insertData(result1, total1, r, 7, 17);
          _insertData(result2, total2, r, 7, 17, true);
        } else if (r.age >= 45 && r.age <= 49) {
          _insertData(result1, total1, r, 8, 18);
          _insertData(result2, total2, r, 8, 18, true);
        } else if (r.age >= 50) {
          _insertData(result1, total1, r, 9, 19);
          _insertData(result2, total2, r, 9, 19, true);
        }

        if (rest.content[r.upk_id] == undefined) {
          rest.content[r.upk_id] = {};
        }
        rest.content[r.upk_id] = {
          diTestHivList: {
            tahun: r.tahun,
            bulan: r.bulan,
            provinsi: r.provinsi,
            kabkota: r.kabkota,
            upk: r.upk,
            data: result1,
            title: "Jumlah orang yang dites HIV",
            total: total1
          },
          testHivPositifList: {
            tahun: r.tahun,
            bulan: r.bulan,
            provinsi: r.provinsi,
            kabkota: r.kabkota,
            upk: r.upk,
            data: result2,
            title: "Jumlah orang yang HIV positif",
            total: total2
          }
        };
      });

      if (excel == "true") {
        return getTestHivReportExcel(req, res, next, rest);
      }

      res.json(rest);
    } catch (error) {
      next(error);
    }
  };

  const getTestHivReportExcel = async (req, res, next, rest) => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    let dateObj = new Date();
    let monthh = dateObj.getMonth();
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let date = day + '-' + monthh + '-' + year;

    let month2 = null;
    let year2 = null;
    let { month, meta } = req.query;
    if (month) {
      month2 = parseInt(month.split('-')[1]) - 1;
      year2 = month.split('-')[0];
    }

    let dateMoth = day + ' ' + monthNames[month2] + ' ' + year;

    const data = rest;
    meta = data.meta;
    // let total = new Array();
    const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC"];
    const sheetObj = {};
    const sheetList = [];
    let row = 9;

    Object.keys(data.content).map(kkey => [
      Object.keys(data.content[kkey]).map((key, idx) => {
        if (!sheetList.includes(data.content[kkey][key].kabkota)) {
          sheetList.push(data.content[kkey][key].kabkota);
        }

        if (sheetObj[data.content[kkey][key].kabkota] == undefined) {
          row = 9
          sheetObj[data.content[kkey][key].kabkota] = {
            "!ref": "A1:AC10000",
            // ---------------------------
            A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
            A2: { t: 's', v: meta && meta.upk && meta.upk.name ? meta.upk.name : "-" },
            A3: { t: 's', v: "Nama Kab/Kota" },
            A4: { t: 's', v: meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
            A5: { t: 's', v: "Nama Provinsi" },
            A6: { t: 's', v: meta && meta.province && meta.province.name ? meta.province.name : "-" },
            // ---------------------------
            C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
            C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
            C3: { t: 's', v: "Tahun" },
            C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
            C5: { t: 's', v: "Tanggal Akses" },
            C6: { t: 's', v: dateMoth },
            // ---------------------------
            A7: { t: 's', v: "Tahun" },
            B7: { t: 's', v: "Bulan" },
            C7: { t: 's', v: "Provinsi" },
            D7: { t: 's', v: "KabKota" },
            E7: { t: 's', v: "Faskes" },
            F7: { t: 's', v: "Variabel" },
            G7: { t: 's', v: "Laki-laki" },
            R7: { t: 's', v: "Perempuan" },
            AC7: { t: 's', v: "Total" },
            // ---------------------------
            G8: { t: 's', v: "< 1" },
            H8: { t: 's', v: "1-14" },
            I8: { t: 's', v: "15-19" },
            J8: { t: 's', v: "20-24" },
            K8: { t: 's', v: "25-29" },
            L8: { t: 's', v: "30-34" },
            M8: { t: 's', v: "35-39" },
            N8: { t: 's', v: "40-44" },
            O8: { t: 's', v: "45-49" },
            P8: { t: 's', v: "> 50" },
            Q8: { t: 's', v: "Jumlah" },
            R8: { t: 's', v: "< 1" },
            S8: { t: 's', v: "1-14" },
            T8: { t: 's', v: "15-19" },
            U8: { t: 's', v: "20-24" },
            V8: { t: 's', v: "25-29" },
            W8: { t: 's', v: "30-34" },
            X8: { t: 's', v: "35-39" },
            Y8: { t: 's', v: "40-44" },
            Z8: { t: 's', v: "45-49" },
            AA8: { t: 's', v: "> 50" },
            AB8: { t: 's', v: "Jumlah" },
            // "!cols": [
            //   { wch: 30 }
            // ],
            // "!merges": [
            //   { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
            //   { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
            //   { s: { r: 6, c: 12 }, e: { r: 6, c: 22 } },
            //   { s: { r: 6, c: 23 }, e: { r: 7, c: 23 } }
            // ]
          }
        }

        let granTotalMale = 0;
        let granTotalFemale = 0;
        sheetObj[data.content[kkey][key].kabkota]["A" + row] = { t: 's', v: idx === 0 ? data.content[kkey][key].tahun : '' };
        sheetObj[data.content[kkey][key].kabkota]["B" + row] = { t: 's', v: idx === 0 ? data.content[kkey][key].bulan : '' };
        sheetObj[data.content[kkey][key].kabkota]["C" + row] = { t: 's', v: idx === 0 ? data.content[kkey][key].provinsi : '' };
        sheetObj[data.content[kkey][key].kabkota]["D" + row] = { t: 's', v: idx === 0 ? data.content[kkey][key].kabkota : '' };
        sheetObj[data.content[kkey][key].kabkota]["E" + row] = { t: 's', v: idx === 0 ? data.content[kkey][key].upk : '' };
        sheetObj[data.content[kkey][key].kabkota]["F" + row] = { t: 's', v: data.content[kkey][key].title };
        {
          data.content[kkey][key] && data.content[kkey][key].total.slice(0, 10).map((v, i) => {
            granTotalMale += v;
            sheetObj[data.content[kkey][key].kabkota][indexCols[i + 6] + row] = { t: 'n', v: v };
          })
        }
        sheetObj[data.content[kkey][key].kabkota]["Q" + row] = { t: 'n', v: granTotalMale };
        {
          data.content[kkey][key] && data.content[kkey][key].total.slice(10, 20).map((v, i) => {
            granTotalFemale += v;
            sheetObj[data.content[kkey][key].kabkota][indexCols[i + 17] + row] = { t: 'n', v: v };
          })
        }
        sheetObj[data.content[kkey][key].kabkota]["AB" + row] = { t: 'n', v: granTotalFemale };
        sheetObj[data.content[kkey][key].kabkota]["AC" + row] = { t: 'n', v: granTotalMale + granTotalFemale };

        row++;
      }),
      Object.keys(data.content[kkey]).map((key, idx) => {
        // let sumMaleContent = 0;
        // let sumFemaleContent = 0;
        {
          data.content[kkey][key].data.map((value, idxData) => {
            let sumMale = 0;
            let sumFemale = 0;
            sheetObj[data.content[kkey][key].kabkota]["F" + row] = { t: 's', v: value.label };
            {
              value.data
                .slice(0, 10)
                .map((valueCount, i) => {
                  sumMale = sumMale + valueCount;
                  // sumMaleContent = sumMaleContent + valueCount;
                  sheetObj[data.content[kkey][key].kabkota][indexCols[i + 6] + row] = { t: 'n', v: !["WPS", "Bumil"].includes(value.value) ? valueCount : '' };
                })
            }
            sheetObj[data.content[kkey][key].kabkota]["Q" + row] = { t: 'n', v: !["WPS", "Bumil"].includes(value.value) ? sumMale : '' };
            {
              value.data
                .slice(10, 20)
                .map((valueCount, i) => {
                  sumFemale = sumFemale + valueCount;
                  // sumFemaleContent = sumFemaleContent + valueCount;
                  sheetObj[data.content[kkey][key].kabkota][indexCols[i + 17] + row] = { t: 'n', v: !["Waria", "LSL"].includes(value.value) ? valueCount : '' };
                })
            }
            sheetObj[data.content[kkey][key].kabkota]["AB" + row] = { t: 'n', v: !["Waria", "LSL"].includes(value.value) ? sumFemale : '' };
            sheetObj[data.content[kkey][key].kabkota]["AC" + row] = { t: 'n', v: sumMale + sumFemale };

            row++;
          })
        }
      })
    ])

    XLSX.writeFile({
      SheetNames: sheetList,
      Sheets: sheetObj
    }, './api/excel/LaporanHIV_Rekap-' + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
    return res.send({
      status: 200,
      message: "Success",
      data: "/api/excel/LaporanHIV_Rekap-" + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
    });
  }

  const getTestHivReportIndividual = async (req, res, next) => {
    try {
      let upk, sudinKabKota, province;
      const { month, provinceId, sudinKabKotaId, upkId, excel } = req.query;
      const roleId = req.user.role;
      // meta
      if (upkId) {
        upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (upk) {
          sudinKabKota = upk.sudinKabKota;
          province = upk.sudinKabKota.province;
        }
      } else if (sudinKabKotaId) {
        sudinKabKota = await SudinKabKota.findByPk(sudinKabKotaId, {
          include: [{ model: Province, required: true }]
        });
        if (sudinKabKota) {
          province = sudinKabKota.province;
        }
      } else if (provinceId) {
        province = await Province.findByPk(provinceId);
      }
      // end meta

      let tgl;
      let month2 = null;
      let year2 = null;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
        tgl = year2 + "-" + month2;
      }

      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( tanggal_test, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (provinceId) {
        where.push(` provinces.ID = ${provinceId} `);
      }
      if (sudinKabKotaId) {
        where.push(` sudin_kab_kota.ID = ${sudinKabKotaId} `);
      }
      if (upkId) {
        where.push(` upk.ID = ${upkId} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          to_char( th.tanggal_test, 'YYYY' ) AS tahun,
          to_char( th.tanggal_test, 'MM' ) AS bulan,
          provinces.NAME AS provinsi,
          sudin_kab_kota.NAME AS kabkota,
          upk.NAME AS upk,
          upk.ID AS upk_id,
          patients.nik,
          patients.fullname,
          EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
          patients.gender,
          patients.kelompok_populasi,
          th.kesimpulan_hiv,
          th.tanggal_test,
          treatments.art_start_date
        FROM
          test_hiv th
          JOIN visits
          JOIN patients
          JOIN upk
          JOIN sudin_kab_kota ON sudin_kab_kota."id" = upk.sudin_kab_kota_id ON upk."id" = patients.upk_id ON patients."id" = visits.patient_id ON visits."id" = th.visit_id
          JOIN provinces ON provinces.ID = sudin_kab_kota.province_id
          LEFT JOIN treatments ON treatments.visit_id = th.visit_id 
        ${newWhere}
        GROUP BY
          th.tanggal_test,
          patients.nik,
          provinces.NAME,
          sudin_kab_kota.NAME,
          upk.NAME,
          upk.ID,
          patients.fullname,
          patients.date_birth,
          patients.gender,
          patients.kelompok_populasi,
          th.kesimpulan_hiv,
          treatments.art_start_date
        ORDER BY
          provinces.NAME ASC,
          sudin_kab_kota.NAME ASC,
          upk.NAME ASC,
          th.tanggal_test ASC,
          patients.fullname ASC
      `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      let rest = {
        content: data,
        meta: {
          upk,
          sudinKabKota,
          province
        }
      };

      if (excel == "true") {
        return getTestHivReportIndividualExcel(req, res, next, rest);
      }

      res.json(rest);
    } catch (error) {
      next(error);
    }
  };

  const getTestHivReportIndividualExcel = async (req, res, next, rest) => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    let dateObj = new Date();
    let monthh = dateObj.getMonth();
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let date = day + '-' + monthh + '-' + year;

    let month2 = null;
    let year2 = null;
    let { month, meta } = req.query;
    if (month) {
      month2 = parseInt(month.split('-')[1]) - 1;
      year2 = month.split('-')[0];
    }

    let dateMoth = day + ' ' + monthNames[month2] + ' ' + year;

    const data = rest;
    meta = data.meta;
    // let total = new Array();
    const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC"];
    const sheetObj = {};
    const sheetList = [];
    let row = 8;
    let lastUpk;

    {
      data.content.map(r => {
        let showUpk = false;
        if (lastUpk !== r.upk_id) {
          lastUpk = r.upk_id;
          showUpk = true;
        }

        if (!sheetList.includes(r.kabkota)) {
          sheetList.push(r.kabkota);
        }

        if (sheetObj[r.kabkota] == undefined) {
          row = 8;
          sheetObj[r.kabkota] = {
            "!ref": "A1:AC1000",
            // ---------------------------
            A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
            A2: { t: 's', v: meta && meta.upk && meta.upk.name ? meta.upk.name : "-" },
            A3: { t: 's', v: "Nama Kab/Kota" },
            A4: { t: 's', v: meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
            A5: { t: 's', v: "Nama Provinsi" },
            A6: { t: 's', v: meta && meta.province && meta.province.name ? meta.province.name : "-" },
            // ---------------------------
            C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
            C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
            C3: { t: 's', v: "Tahun" },
            C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
            C5: { t: 's', v: "Tanggal Akses" },
            C6: { t: 's', v: dateMoth },
            // ---------------------------
            A7: { t: 's', v: "Tahun" },
            B7: { t: 's', v: "Bulan" },
            C7: { t: 's', v: "Provinsi" },
            D7: { t: 's', v: "KabKota" },
            E7: { t: 's', v: "Faskes" },
            F7: { t: 's', v: "NIK" },
            G7: { t: 's', v: "Nama" },
            H7: { t: 's', v: "Tanggal Tes HIV" },
            I7: { t: 's', v: "Hasil Tes HIV" },
            J7: { t: 's', v: "Tanggal Mulai ART" }
            // "!cols": [
            //   { wch: 30 }
            // ],
            // "!merges": [
            //   { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
            //   { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
            //   { s: { r: 6, c: 12 }, e: { r: 6, c: 22 } },
            //   { s: { r: 6, c: 23 }, e: { r: 7, c: 23 } }
            // ]
          }
        }

        let dateObj = new Date(r.tanggal_test);
        let monthh = dateObj.getMonth();
        let day = String(dateObj.getDate()).padStart(2, '0');
        let year = dateObj.getFullYear();
        let tanggal_test = day + ' ' + monthNames[monthh] + ' ' + year;

        let art_start_date = '';
        if (r.art_start_date) {
          dateObj = new Date(r.art_start_date);
          monthh = dateObj.getMonth();
          day = String(dateObj.getDate()).padStart(2, '0');
          year = dateObj.getFullYear();
          art_start_date = day + ' ' + monthNames[monthh] + ' ' + year;
        }

        sheetObj[r.kabkota]["A" + row] = { t: 's', v: showUpk ? r.tahun : '' };
        sheetObj[r.kabkota]["B" + row] = { t: 's', v: showUpk ? r.bulan : '' };
        sheetObj[r.kabkota]["C" + row] = { t: 's', v: showUpk ? r.provinsi : '' };
        sheetObj[r.kabkota]["D" + row] = { t: 's', v: showUpk ? r.kabkota : '' };
        sheetObj[r.kabkota]["E" + row] = { t: 's', v: showUpk ? r.upk : '' };
        sheetObj[r.kabkota]["F" + row] = { t: 's', v: r.nik };
        sheetObj[r.kabkota]["G" + row] = { t: 's', v: r.fullname };
        sheetObj[r.kabkota]["H" + row] = { t: 's', v: tanggal_test };
        sheetObj[r.kabkota]["I" + row] = { t: 's', v: r.kesimpulan_hiv };
        sheetObj[r.kabkota]["J" + row] = { t: 's', v: art_start_date };

        row++;
      })
    }

    XLSX.writeFile({
      SheetNames: sheetList,
      Sheets: sheetObj
    }, './api/excel/LaporanHIV_Rekap_Individu-' + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
    return res.send({
      status: 200,
      message: "Success",
      data: "/api/excel/LaporanHIV_Rekap_Individu-" + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
    });
  }

  const getRekapRencanaKunjunganOdha = async (req, res, next) => {
    try {
      let upk, sudinKabKota, province;
      const { month, provinceId, sudinKabKotaId, upkId, excel } = req.query;
      const roleId = req.user.role;
      // meta
      if (upkId) {
        upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (upk) {
          sudinKabKota = upk.sudinKabKota;
          province = upk.sudinKabKota.province;
        }
      } else if (sudinKabKotaId) {
        sudinKabKota = await SudinKabKota.findByPk(sudinKabKotaId, {
          include: [{ model: Province, required: true }]
        });
        if (sudinKabKota) {
          province = sudinKabKota.province;
        }
      } else if (provinceId) {
        province = await Province.findByPk(provinceId);
      }
      // end meta

      let tgl;
      let month2 = null;
      let year2 = null;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
        tgl = year2 + "-" + month2;
      }

      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        // newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        newTgl = tgl;
        where.push(` to_char( treatments.tgl_kunjungan, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (provinceId) {
        where.push(` id_provinsi = ${provinceId} `);
      }
      if (sudinKabKotaId) {
        where.push(` id_kabkota = ${sudinKabKotaId} `);
      }
      if (upkId) {
        where.push(` id_upk = ${upkId} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` id_upk = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          rk.*,
          treatments.tgl_kunjungan AS kunjungan_sebelumnya 
        FROM
          rekap_kunjungan rk
          LEFT JOIN treatments ON treatments.visit_id = rk.prev_visit_id
        ${newWhere}
      `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      let rest = {
        content: data,
        meta: {
          upk,
          sudinKabKota,
          province
        }
      };

      if (excel == "true") {
        return getRekapRencanaKunjunganOdhaExcel(req, res, next, rest);
      }

      res.json(rest);
    } catch (error) {
      next(error);
    }
  };

  const getRekapRencanaKunjunganOdhaExcel = async (req, res, next, rest) => {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    let dateObj = new Date();
    let monthh = dateObj.getMonth();
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let date = day + '-' + monthh + '-' + year;

    let month2 = null;
    let year2 = null;
    let { month, meta } = req.query;
    if (month) {
      month2 = parseInt(month.split('-')[1]) - 1;
      year2 = month.split('-')[0];
    }

    let dateMoth = day + ' ' + monthNames[month2] + ' ' + year;

    const data = rest;
    meta = data.meta;
    // let total = new Array();
    const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC"];
    const sheetObj = {};
    const sheetList = [];
    let row = 8;
    let lastUpk;

    {
      data.content.map(r => {
        let showUpk = false;
        if (lastUpk !== r.id_upk) {
          lastUpk = r.id_upk;
          showUpk = true;
        }

        if (!sheetList.includes(r.kabkota)) {
          sheetList.push(r.kabkota);
        }

        if (sheetObj[r.kabkota] == undefined) {
          row = 8;
          sheetObj[r.kabkota] = {
            "!ref": "A1:AC1000",
            // ---------------------------
            A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
            A2: { t: 's', v: meta && meta.upk && meta.upk.name ? meta.upk.name : "-" },
            A3: { t: 's', v: "Nama Kab/Kota" },
            A4: { t: 's', v: meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
            A5: { t: 's', v: "Nama Provinsi" },
            A6: { t: 's', v: meta && meta.province && meta.province.name ? meta.province.name : "-" },
            // ---------------------------
            C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
            C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
            C3: { t: 's', v: "Tahun" },
            C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
            C5: { t: 's', v: "Tanggal Akses" },
            C6: { t: 's', v: dateMoth },
            // ---------------------------
            A7: { t: 's', v: "Provinsi" },
            B7: { t: 's', v: "KabKota" },
            C7: { t: 's', v: "Faskes" },
            D7: { t: 's', v: "Kunjungan Bulan Sebelumnya" },
            E7: { t: 's', v: "Rencana Berkunjung" },
            F7: { t: 's', v: "Kunjungan Bulan ini" },
            G7: { t: 's', v: "No Register Nasional" },
            H7: { t: 's', v: "NIK" },
            I7: { t: 's', v: "Nama" },
            J7: { t: 's', v: "Jenis Kelamin" },
            K7: { t: 's', v: "Tanggal Lahir" },
            L7: { t: 's', v: "Tanggal Mulai ART" },
            M7: { t: 's', v: "No HP" },
            N7: { t: 's', v: "Nama PMO" },
            O7: { t: 's', v: "No HP PMO" },
            P7: { t: 's', v: "Keterangan" }
            // "!cols": [
            //   { wch: 30 }
            // ],
            // "!merges": [
            //   { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
            //   { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
            //   { s: { r: 6, c: 12 }, e: { r: 6, c: 22 } },
            //   { s: { r: 6, c: 23 }, e: { r: 7, c: 23 } }
            // ]
          }
        }

        let dateObj;
        let monthh;
        let day;
        let year;

        let kunjungan_sebelumnya = '';
        if (r.kunjungan_sebelumnya) {
          dateObj = new Date(r.kunjungan_sebelumnya);
          monthh = dateObj.getMonth();
          day = String(dateObj.getDate()).padStart(2, '0');
          year = dateObj.getFullYear();

          kunjungan_sebelumnya = day + ' ' + monthNames[monthh] + ' ' + year;
        }

        let rencana_kunjungan = '';
        if (r.rencana_kunjungan) {
          dateObj = new Date(r.rencana_kunjungan);
          monthh = dateObj.getMonth();
          day = String(dateObj.getDate()).padStart(2, '0');
          year = dateObj.getFullYear();

          rencana_kunjungan = day + ' ' + monthNames[monthh] + ' ' + year;
        }

        let kunjungan_bulan_ini = '';
        if (r.kunjungan_bulan_ini) {
          dateObj = new Date(r.kunjungan_bulan_ini);
          monthh = dateObj.getMonth();
          day = String(dateObj.getDate()).padStart(2, '0');
          year = dateObj.getFullYear();

          kunjungan_bulan_ini = day + ' ' + monthNames[monthh] + ' ' + year;
        }

        let tanggal_mulai_art = '';
        if (r.tanggal_mulai_art) {
          dateObj = new Date(r.tanggal_mulai_art);
          monthh = dateObj.getMonth();
          day = String(dateObj.getDate()).padStart(2, '0');
          year = dateObj.getFullYear();

          tanggal_mulai_art = day + ' ' + monthNames[monthh] + ' ' + year;
        }

        sheetObj[r.kabkota]["A" + row] = { t: 's', v: showUpk ? r.provinsi : '' };
        sheetObj[r.kabkota]["B" + row] = { t: 's', v: showUpk ? r.kabkota : '' };
        sheetObj[r.kabkota]["C" + row] = { t: 's', v: showUpk ? r.upk : '' };
        sheetObj[r.kabkota]["D" + row] = { t: 's', v: r.kunjungan_sebelumnya ? kunjungan_sebelumnya : '' };
        sheetObj[r.kabkota]["E" + row] = { t: 's', v: r.rencana_kunjungan ? rencana_kunjungan : '' };
        sheetObj[r.kabkota]["F" + row] = { t: 's', v: r.kunjungan_bulan_ini ? kunjungan_bulan_ini : '' };
        sheetObj[r.kabkota]["G" + row] = { t: 's', v: r.no_register_nasional };
        sheetObj[r.kabkota]["H" + row] = { t: 's', v: r.nik };
        sheetObj[r.kabkota]["I" + row] = { t: 's', v: r.nama };
        sheetObj[r.kabkota]["J" + row] = { t: 's', v: r.jenis_kelamin };

        sheetObj[r.kabkota]["K" + row] = { t: 's', v: r.tgl_lahir };
        sheetObj[r.kabkota]["L" + row] = { t: 's', v: r.tanggal_mulai_art ? tanggal_mulai_art : '' };
        sheetObj[r.kabkota]["M" + row] = { t: 's', v: r.no_hp };
        sheetObj[r.kabkota]["N" + row] = { t: 's', v: r.nama_pmo };
        sheetObj[r.kabkota]["O" + row] = { t: 's', v: r.no_hp_pmo };
        sheetObj[r.kabkota]["P" + row] = { t: 's', v: '' };

        row++;
      })
    }

    XLSX.writeFile({
      SheetNames: sheetList,
      Sheets: sheetObj
    }, './api/excel/LaporanHIV_Rekap_Rencana_Kunjungan_odha-' + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
    return res.send({
      status: 200,
      message: "Success",
      data: "/api/excel/LaporanHIV_Rekap_Rencana_Kunjungan_odha-" + (meta && meta.upk && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
    });
  }

  const getTestVl = async (req, res, next) => {
    try {
      let upk, sudinKabKota, province;
      const { month, provinceId, sudinKabKotaId, upkId, excel } = req.query;
      const roleId = req.user.role;
      // meta
      if (upkId) {
        upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (upk) {
          sudinKabKota = upk.sudinKabKota;
          province = upk.sudinKabKota.province;
        }
      } else if (sudinKabKotaId) {
        sudinKabKota = await SudinKabKota.findByPk(sudinKabKotaId, {
          include: [{ model: Province, required: true }]
        });
        if (sudinKabKota) {
          province = sudinKabKota.province;
        }
      } else if (provinceId) {
        province = await Province.findByPk(provinceId);
      }
      // end meta

      let tgl;
      let month2 = null;
      let year2 = null;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
        tgl = year2 + "-" + month2;
      }

      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        // newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        newTgl = tgl;
        where.push(` to_char( tc.created_at, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (provinceId) {
        where.push(` provinces.ID = ${provinceId} `);
      }
      if (sudinKabKotaId) {
        where.push(` sudin_kab_kota.ID = ${sudinKabKotaId} `);
      }
      if (upkId) {
        where.push(` upk.ID = ${upkId} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          to_char( tc.created_at, 'YYYY' ) AS tahun,
          to_char( tc.created_at, 'MM' ) AS bulan,
          provinces.NAME AS provinsi,
          sudin_kab_kota.NAME AS kabkota,
          upk.NAME AS upk,
          upk.ID AS upk_id,
          treatments.no_reg_nas,
          patients.nik,
          patients.fullname,
          patients.gender AS jenis_kelamin,
          patients.date_birth,
          treatments.art_start_date,
          tc.created_at AS vl_date,
          NULL AS kesimpulan_tes_vl,
          tc.hasil_test_vl_cd4 AS hasil_tes_vl,
          NULL AS tes_bulan_ke,
          NULL AS periode_tes,
          NULL AS tahun_tes_vl,
          NULL AS bulan_tes_vl,
          NULL AS last_tes,
          NULL AS status_akhir_folow_up,
          NULL AS tangga_akhir_folow_up
        FROM
          test_vl_cd4 tc
          JOIN visits
          JOIN patients
          JOIN upk
          JOIN sudin_kab_kota ON sudin_kab_kota."id" = upk.sudin_kab_kota_id ON upk."id" = patients.upk_id ON patients."id" = visits.patient_id ON visits."id" = tc.visit_id
          JOIN provinces ON provinces.ID = sudin_kab_kota.province_id
          LEFT JOIN treatments ON treatments.visit_id = tc.visit_id 
        ${newWhere}
        ORDER BY
          provinces.NAME ASC,
          sudin_kab_kota.NAME ASC,
          upk.NAME ASC,
          tc.created_at ASC,
          patients.fullname ASC
      `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      let rest = {
        content: data,
        meta: {
          upk,
          sudinKabKota,
          province
        }
      };

      // if (excel == "true") {
      //   return getTestHivReportIndividualExcel(req, res, next, rest);
      // }

      res.json(rest);
    } catch (error) {
      next(error);
    }
  };

  const getVlcd4Report = async (req, res, next) => {
    try {
      let upk, sudinKabKota, province;
      const { month, provinceId, sudinKabKotaId, upkId } = req.query;

      // meta
      if (upkId) {
        upk = await Upk.findByPk(upkId, {
          include: [
            {
              model: SudinKabKota,
              as: "sudinKabKota",
              required: true,
              include: [{ model: Province, required: true }]
            }
          ]
        });
        if (upk) {
          sudinKabKota = upk.sudinKabKota;
          province = upk.sudinKabKota.province;
        }
      } else if (sudinKabKotaId) {
        sudinKabKota = await SudinKabKota.findByPk(sudinKabKotaId, {
          include: [{ model: Province, required: true }]
        });
        if (sudinKabKota) {
          province = sudinKabKota.province;
        }
      } else if (provinceId) {
        province = await Province.findByPk(provinceId);
      }
      // end meta

      let month2 = null;
      let year2 = null;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
      }

      const data = await Medicine.findAll({
        // logging: console.log,
        include: [
          {
            model: NonArvMedicine,
            required: true,
            where: {
              [Op.or]: {
                isVl: true,
                isCd4: true
              }
            }
          },
          {
            model: TestVl,
            required: false,
            as: "tesReagenData",
            where: {
              [Op.and]: [
                sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('date')), year2),
                sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('date')), month2)
              ]
            },
            include: [
              {
                model: Visit,
                required: true,
                include: [
                  {
                    model: Patient,
                    required: true,
                    include: [
                      {
                        model: Upk,
                        required: true,
                        where: (upkId ? { id: upkId } : false),
                        include: [
                          {
                            as: 'sudinKabKota',
                            model: SudinKabKota,
                            required: true,
                            where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                            include: [
                              {
                                model: Province,
                                required: true,
                                where: (provinceId ? { id: provinceId } : false)
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        order: [
          [NonArvMedicine, "vlcd4Category", "ASC"],
          ["name", "ASC"]
        ]
      });

      let resdata = {};
      data.map(r => {
        const category = r.nonArvMedicine.isVl ? 'VL' : 'CD4';

        let usedReagent = 0;
        r.tesReagenData.map(rr => {
          if (rr.hasiTtestVlCd4 != ENUM.TEST_RESULT.INVALIDERROR) {
            usedReagent += 1;
          }
        });
        
        if (!resdata[category]) {
          resdata[category] = {};
        }
        if (!resdata[category][r.nonArvMedicine.vlcd4Category]) {
          resdata[category][r.nonArvMedicine.vlcd4Category] = [];
        }

        const pm = r.packageMultiplier;
        let pmToSend = r.packageMultiplier;
        for (let i = 0; pmToSend - usedReagent <= 0; i++) {
          pmToSend += pm;
        }

        resdata[category][r.nonArvMedicine.vlcd4Category].push({
          name: r.name,
          packageMultiplier: pmToSend,
          stockUnitType: r.stockUnitType,
          usedReagent: usedReagent,
          terbuang: pmToSend - usedReagent,
        });
      });

      let rest = {
        content: resdata,
        meta: {
          upk,
          sudinKabKota,
          province
        }
      };

      res.json(rest);
    } catch (error) {
      next(error);
    }
  }

  return { getCohortReport, getMasterHivReport, getMasterHivReportById, getMasterImsReport, getMasterHivReportExcel, getTestHivReport, getMasterHivReport2, getTestHivReportIndividual, getRekapRencanaKunjunganOdha, getTestVl, getVlcd4Report };
};

module.exports = ReportIntraController;
