const moment = require("moment");
const _ = require("lodash");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const sequelize = require("../../config/database");
const Joi = require("@hapi/joi");

const DataTable = require("../libraries/dataTable.class");

const VisitService = require("../services/visit.service")();
const PatientService = require("../services/patient.service")();
const QueryService = require("../services/query.service")();
const DateService = require("../services/date.service")();
const InventoryService = require("../services/inventory.service")();
const ENUM = require("../../config/enum");

const Visit = require("../models/Visit");
const Treatment = require("../models/Treatment");
const Couple = require("../models/Couple");
const User = require("../models/User");
const Upk = require("../models/Upk");
const Patient = require("../models/Patient");
const Medicine = require("../models/Medicine");
const Prescription = require("../models/Prescription");
const Profilaksis = require("../models/Profilaksis");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");
const TestHiv = require("../models/TestHiv");
const TestIms = require("../models/TestIms");
const TestVl = require("../models/TestVl");
const PatientLog = require("../models/PatientLog");
const VisitIms = require("../models/VisitIms");
const VisitImsLab = require("../models/VisitImsLab");
const ImsPrescription = require("../models/ImsPrescription");
const ImsPrescriptionMedicine = require("../models/ImsPrescriptionMedicine");
const NonArvMedicine = require("../models/NonArvMedicine");
const ArvMedicine = require("../models/ArvMedicine");
const AlkesMedicine = require("../models/AlkesMedicine");
const ReportLogs = require("../models/ReportLogs");
const Regiment = require("../models/Regiment");

const XLSX = require("xlsx");
const eror = new Error();

const VisitController = () => {
  const create = async (req, res, next) => {
    const { visitDate } = req.body;
    const patientId = req.body.patientId || req.params.patientId;
    try {
      const patient = req.patient;
      if (!patient || (patient && patient.tglMeninggal)) {
        throw { status: 400, message: "patient_not_found" };
      }

      const visit = await Visit.create({
        ordinal: await VisitService.getNextOrdinalByVisit(patientId),
        isGenerated: false,
        visitType:
          patient.statusPatient === ENUM.STATUS_PATIENT.ODHA || patient.statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA
            ? ENUM.VISIT_TYPE.TREATMENT
            : ENUM.VISIT_TYPE.TEST,
        patientId: patientId,
        userRrId: req.user.id,
        upkId: req.user.upkId,
        createdBy: req.user.id,
        visitDate: DateService.now()
      }).then(async visitCreated => {
        return await Visit.findOne({
          where: {
            id: visitCreated.id
          },
          include: [
            {
              model: Patient,
              required: true,
              attributes: ["id", "nik", "name", "statusPatient", "upkId"]
            },
            { model: Upk, required: true, attributes: ["name", "id", "provinceId", "sudinKabKotaId"] },
            {
              model: User,
              required: false,
              as: "petugasRR",
              attributes: ["id", "nik", "name", "phone", "upkId"]
            }
          ]
        });
      });

      res.send({
        status: 200,
        message: "Success",
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  // new
  const deleteVisit = async (req, res, next) => {
    const id = req.params.id;
    try {
      const cekVisit = await Visit.findByPk(id);
      if (!cekVisit) {
        return res.send({
          status: 400,
          message: "Data Not Found"
        });
      }

      const visit = await Visit.destroy({
        where: {
          id: id
        }
      });

      if (visit) {
        return res.send({
          status: 200,
          message: "Success"
        });
      }

      return res.send({
        status: 400,
        message: "Error"
      });
    } catch (err) {
      next(err);
    }
  };
  // end new

  const getVisitByPatient = async (req, res, next) => {
    const { patientId } = req.params;
    let { page, limit, keyword } = req.query;
    try {
      req.query.order = req.query.order
        ? req.query.order
        : [
          ["ordinal", "DESC"],
          ["visitDate", "DESC"]
        ];
      const dataTable = new DataTable(Visit, req);
      dataTable.setIncludeParams([
        {
          model: Treatment,
          required: false,
          attributes: ["id", "ordinal", "isOnTransit"],
          include: [
            {
              model: Upk,
              required: false,
              as: "upkSebelumnyaData",
              attributes: ["name", "id", "provinceId", "sudinKabKotaId"]
            }
          ]
        },
        {
          model: Prescription,
          required: false,
          include: [
            {
              model: PrescriptionMedicine,
              include: [
                {
                  model: Medicine,
                  required: true,
                  attributes: ["name", "codeName"]
                }
              ]
            },
            {
              model: Regiment,
              required: false
            }
          ]
        },
        {
          model: Profilaksis,
          required: false,
          include: [
            {
              model: Medicine,
              required: false,
              attributes: ["name", "codeName"]
            }
          ]
        },
        {
          model: TestHiv,
          required: false,
          // attributes: ["id", "ordinal"],
          // attributes: {exclude: ['namaReagenR12', 'hasilTestR12', 'qtyReagenR12', 'typeReagenR12', 'hasilTestR22', 'namaReagenR22', 'qtyReagenR22', 'typeReagenR22']},
          include: [
            {
              model: Medicine,
              required: false,
              as: "namaReagenR1Data",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenR2Data",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenR3Data",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenNatData",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenDnaData",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenRnaData",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenElisaData",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenWbData",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenR12Data",
              attributes: ["name", "codeName"]
            },
            {
              model: Medicine,
              required: false,
              as: "namaReagenR22Data",
              attributes: ["name", "codeName"]
            }
          ]
        },
        // {
        //   model: TestIms,
        //   required: false,
        //   attributes: ["id", "ordinal"]
        // },
        {
          model: VisitIms,
          as: "testIms",
          required: false,
          attributes: ["reasonVisit", "complaint", "pregnancyStage", "physicalExamination", "upkId", "isIvaTested", "ivaTest", "tesLab"],
          include: [
            {
              model: Upk,
              attributes: ["name", "id", "provinceId", "sudinKabKotaId"]
            }
          ]
        },
        {
          model: VisitImsLab,
          as: 'visitImsLab',
          required: false,
          // attributes: [
          //   "pmnUretraServiks",
          //   "diplokokusIntraselUretraServiks",
          //   "pmnAnus",
          //   "diplokokusIntraselAnus",
          //   "tVaginalis",
          //   "kandida",
          //   "ph",
          //   "sniffTest",
          //   "clueCells",
          //   "rprVdrl",
          //   "tphaTppa",
          //   "rprVdrlTiter",
          //   "etc",
          //   "diagnosisSyndrome",
          //   "diagnosisKlinis",
          //   "diagnosisLab",
          //   'isTested'
          // ]
        },
        {
          model: ImsPrescription,
          as: 'visitPrescription',
          required: false,
          attributes: ["note", "dateGivenMedicine", "condom", "lubricant", "rencanaKunjungan", "isDraft"],
          include: [
            {
              model: ImsPrescriptionMedicine,
              as: 'medicines',
              required: false,
              attributes: ["medicineId", "medicineName", "totalQty", "totalDays", "note"]
            },
          ]
        },
        {
          model: TestVl,
          required: false,
          attributes: ["id", "ordinal"]
        },
        {
          model: Upk,
          required: false,
          attributes: ["name", "id", "provinceId", "sudinKabKotaId"]
        },
        {
          model: User,
          required: false,
          as: "petugasRR",
          attributes: ["name", "nik"]
        },
        {
          model: Patient,
          required: true,
          attributes: ["id", "nik", "name", "statusPatient", "upkId"]
        }
      ]);
      dataTable.addWhereParams({
        patientId: patientId,
        // [Op.or]: [
        //   { upkId: req.user.upkId },
        //   { "$patient.upk_id$": req.user.upkId }
        // ]
      });
      await dataTable.buildResult();
      let result = dataTable.getResult();
      let prevTestHIV = null;

      // for (let x = 0; x < result.data; x++) {
      //   result.data[x] = result.data[x].toJSON();
      //   result.data[x].prevTestHiv = prevTestHIV;
      //   prevTestHIV = result.data[x].testHiv;
      //   // result.data[x] = visitItemJSON;
      // }
      result.data = result.data.reverse().map(visitItem => {
        var visitItemJSON = visitItem.toJSON();
        visitItemJSON.prevTestHiv = prevTestHIV;
        prevTestHIV = visitItemJSON.testHiv;

        return visitItemJSON;
      });
      result.data = result.data.reverse();
      res.json(result);
    } catch (err) {
      next(err);
    }
  };

  const getVisitDetail = async (req, res, next) => {
    const { visitId } = req.params;

    try {
      const visit = await VisitService.getVisitDetail({
        id: visitId
      });

      res.send({
        status: 200,
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  const getPreviousTreatmentVisitDetail = async (req, res, next) => {
    const { visitId } = req.params;

    if (req.visit.ordinal <= 1) {
      return res.send({
        status: 200,
        message: "Success",
        data: null
      });
    }

    try {
      const visit = await VisitService.getVisitDetail({
        patientId: req.visit.patientId,
        ordinal: {
          [Op.lt]: req.visit.ordinal
        },
        visitType: ENUM.VISIT_TYPE.TREATMENT
      });

      if (!visit || (visit && !visit.treatment)) {
        return res.send({
          status: 200,
          message: "Success",
          data: null
        });
      }

      res.send({
        status: 200,
        message: "Success",
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  const ikhtisar = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;
    try {
      await Treatment.update(
        {
          noRegNas: body.noRegNas,
          tglKunjungan: body.tglKunjungan
        },
        {
          where: {
            visitId: visitId
          }
        }
      );

      const treatment = await Treatment.findOne({
        where: {
          visitId: visitId
        },
        attributes: ["no_reg_nas", "tglKunjungan"]
      });

      res.send({
        status: 200,
        message: "Success",
        data: treatment
      });
    } catch (err) {
      next(err);
    }
  };

  const getIkhtisar = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const treatment = await Treatment.findOne({
        where: {
          visitId: visitId
        },
        attributes: ["no_reg_nas", "tglKunjungan"]
      });

      res.send({
        status: 200,
        message: "Success",
        data: treatment
      });
    } catch (err) {
      next(err);
    }
  };

  const getSisaObat = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const prevTreatment = await VisitService.getDetailTreatmentPrevVisit(
        visitId
      );

      res.send({
        status: 200,
        message: "Success",
        data: prevTreatment
      });
    } catch (err) {
      next(err);
    }
  };

  const sisaObat = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;
    try {
      const prevTreatment = await VisitService.getDetailTreatmentPrevVisit(
        visitId
      );

      if (prevTreatment) {
        prevTreatment.sisaObatArv1 = body.sisaObatArv1;
        prevTreatment.sisaObatArv2 = body.sisaObatArv2;
        prevTreatment.sisaObatArv3 = body.sisaObatArv3;
        await prevTreatment.save();
      } else {
        throw {
          status: 400,
          message: "no_previous_treatment_found"
        };
      }

      res.send({
        status: 200,
        message: "Success",
        data: prevTreatment
      });
    } catch (err) {
      next(err);
    }
  };

  const updatePatientVisitDateRequest = async (req, res, next) => {
    const { body } = req;
    try {
      const visit = await Visit.findByPk(req.visit.id, {
        include: [
          {
            model: Treatment,
            required: true
          }
        ]
      });

      if (visit) {
        const tglRencanaKunjunganMoment = moment(visit.tglRencanaKunjungan);
        if (body.tglPermintaanKunjungan) {
          const tglPermintaanKunjunganMoment = moment(
            body.tglPermintaanKunjungan
          );
          if (
            tglPermintaanKunjunganMoment.isBefore(tglRencanaKunjunganMoment)
          ) {
            visit.treatment.tglRencanaKunjungan = visit.treatment.tglPermintaanKunjunganMoment.format();
            await visit.treatment.save();
          }
        } else {
          visit.treatment.tglPermintaanKunjungan = null;
          await visit.treatment.save();
        }
      }

      res.send({
        status: 200,
        message: "Success",
        data: visit.treatment
      });
    } catch (err) {
      next(err);
    }
  };

  const getVisitListByUpk = async (req, res, next) => {
    let { page, limit, keyword, listType, whereParam, orderDate } = req.query;
    try {
      let queryPatientParams = {};
      let queryVisitDateParams = {};
      let queryWhereParams = {};
      if (keyword) {
        queryPatientParams = QueryService.filterPatientsParamsBuilder(keyword);
      }
      if (whereParam) {
        whereParam =
          typeof whereParam === "string" ? JSON.parse(whereParam) : whereParam;
        if (whereParam.ischeckOut === true) {
          Object.assign(queryWhereParams, {
            checkOutDate: { [Op.ne]: null }
          });
        }
        if (whereParam.ischeckOut === false) {
          Object.assign(queryWhereParams, {
            checkOutDate: { [Op.eq]: null }
          });
        }

        if (whereParam.isOnTransit && whereParam.isOnTransit === true) {
          Object.assign(queryWhereParams, {
            "$treatment.is_on_transit$": true
          });
        } else if (whereParam.isOnTransit === false) {
          Object.assign(queryWhereParams, {
            "$treatment.is_on_transit$": false
          });
        }

        if (whereParam.visitDate && whereParam.visitDate[0]) {
          Object.assign(queryWhereParams, {
            [Op.and]: [
              {
                visitDate: {
                  [Op.gte]: moment(whereParam.visitDate[0]).valueOf()
                }
              },
              {
                visitDate: {
                  [Op.lte]: moment(whereParam.visitDate[1])
                    .subtract(-1, "day")
                    .valueOf()
                }
              }
            ]
          });
        }
      }

      switch (listType) {
        case "ALL":
          queryVisitDateParams = {};
          break;
        case "TODAY":
          queryVisitDateParams.visitDate = {
            [Op.between]: [
              moment()
                .startOf("day")
                .valueOf(),
              moment()
                .endOf("day")
                .valueOf()
            ]
          };
          break;
        case "HISTORY":
          queryVisitDateParams.visitDate = {
            [Op.lt]: moment()
              .startOf("day")
              .valueOf()
          };
          break;
        case "RENCANA_KUNJUNGAN":
          queryVisitDateParams["$treatment.tgl_rencana_kunjungan$"] = {
            [Op.between]: [
              moment()
                .startOf("day")
                .format(),
              moment()
                .add(2, "weeks")
                .endOf("day")
                .format()
            ]
          };
          break;
        default:
          break;
      }

      req.query.order = req.query.order
        ? req.query.order
        : [
          ["ordinal", "DESC"],
          ["visitDate", "DESC"]
        ];
      if (orderDate) {
        req.query.order = [["visitDate", orderDate]];
      }
      const dataTable = new DataTable(Visit, req);
      dataTable.setIncludeParams([
        {
          model: Patient,
          where: queryPatientParams,
          required: true,
          include: [
            {
              model: User,
              required: false,
              as: "user",
              attributes: ["id", "nik", "name", "isActive"]
            }
          ],
          attributes: ["id", "nik", "name", "statusPatient", "upkId"]
        },
        {
          model: Treatment,
          required: false,
          include: [
            {
              model: Upk,
              required: false,
              as: "upkSebelumnyaData",
              attributes: ["name", "id", "provinceId", "sudinKabKotaId"]
            }
          ]
        },
        {
          model: TestHiv,
          required: false
        },
        {
          model: VisitIms,
          as: 'testIms',
          required: false
        },
        {
          model: TestVl,
          required: false
        },
        { model: Upk, required: true, attributes: ["name", "id", "provinceId", "sudinKabKotaId"] },
        {
          model: User,
          required: false,
          as: "petugasRR",
          attributes: ["id", "nik", "name", "phone", "upkId"]
        }
      ]);
      dataTable.addWhereParams(
        Object.assign(
          {
            upkId: req.user.upkId
          },
          queryVisitDateParams,
          queryWhereParams
        )
      );
      switch (req.user.role) {
        case ENUM.USER_ROLE.LAB_STAFF:
          dataTable.addWhereParams({
            [Op.or]: [
              {
                "$testHiv.id$": {
                  [Op.not]: null
                }
              },
              {
                "$testIms.id$": {
                  [Op.not]: null
                }
              },
              {
                "$testVL.id$": {
                  [Op.not]: null
                }
              }
            ]
          });
          break;
        case ENUM.USER_ROLE.PHARMA_STAFF:
          dataTable.addWhereParams({
            [Op.and]: [
              {
                "$treatment.id$": {
                  [Op.not]: null
                }
              }
            ]
          });
          break;
        default:
          break;
      }
      await dataTable.buildResult();
      let result = dataTable.getResult();
      result.data = result.data.map(visitItem => {
        var visitItemJSON = visitItem.toJSON();
        visitItemJSON.completeness = VisitService.checkVisitDataCompleteness(
          visitItemJSON
        );
        visitItemJSON.isComplete =
          visitItemJSON.completeness.Treatment &&
            visitItemJSON.completeness.TestHiv &&
            visitItemJSON.completeness.TestIms &&
            visitItemJSON.completeness.TestVl &&
            visitItemJSON.completeness.Prescription
            ? true
            : false;

        if (listType === "RENCANA_KUNJUNGAN") {
          visitItemJSON.isComplete = true;
          visitItemJSON.needConfirmation = false;
          if (visitItemJSON.treatment) {
            if (
              visitItemJSON.treatment.tglPermintaanKunjungan &&
              visitItemJSON.treatment.tglRencanaKunjungan !==
              visitItemJSON.treatment.tglPermintaanKunjungan
            ) {
              visitItemJSON.needConfirmation = true;
            }
            visitItemJSON.ordinal++;
            visitItemJSON.visitDate =
              visitItemJSON.treatment.tglRencanaKunjungan;
          }
        }

        return visitItemJSON;
      });
      res.json(result);
    } catch (err) {
      next(err);
    }
  };

  const endVisit = async (req, res, next, returnItem = false) => {
    const { visitId } = req.params;

    try {
      if (req.visit.checkOutDate) {
        throw {
          status: 400,
          message: "visit_has_ended"
        };
      }

      const visit = await Visit.findByPk(req.visit.id, {
        include: [
          {
            model: Patient,
            required: true
          },
          {
            model: Treatment,
            required: false
          },
          {
            model: Prescription,
            required: false
          },
          {
            model: ImsPrescription,
            required: false,
            as: "visitPrescription"
          },
          {
            model: TestHiv,
            required: false
          },
          {
            model: VisitIms,
            as: 'testIms',
            required: false
          },
          {
            model: VisitImsLab,
            required: false,
            as: "visitImsLab"
          },
          {
            model: TestVl,
            required: false
          }
        ]
      });

      if (visit.treatment && !visit.treatment.prescriptionId) {
        throw {
          status: 400,
          message: "prescription_not_found"
        };
      }

      // Based on task https://www.meistertask.com/app/task/m8AZUkoD/
      // the prescription is not required when finishing the visit
      // if (visit.prescription && visit.prescription.isDraft) {
      //   throw {
      //     status: 400,
      //     message: "prescription_has_not_been_given_yet"
      //   };
      // }

      if (
        (visit.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA || visit.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA) &&
        !visit.prescription &&
        !visit.treatment
      ) {
        throw {
          status: 400,
          message: "treatment_or_prescription_not_found"
        };
      }

      if (
        (visit.patient.statusPatient !== ENUM.STATUS_PATIENT.ODHA && visit.patient.statusPatient !== ENUM.STATUS_PATIENT.ODHA_LAMA) &&
        !visit.prescription &&
        !visit.testHiv &&
        !visit.testIms &&
        !visit.testVl
      ) {
        throw {
          status: 400,
          message: "no_lab_test_choosen_or_presc_not_found"
        };
      }
      
      if (visit.visitPrescription) {
        if (!visit.visitPrescription.isDraft) {
          visit.checkOutDate = moment()
          .local()
          .format();
        }
      } else if (visit.testIms) {
        if (visit.visitImsLab) {
          if (!visit.visitImsLab.isTested) {
            visit.checkOutDate = moment()
            .local()
            .format();
          }
        }
      } else {
        visit.checkOutDate = moment()
          .local()
          .format();
      }
      visit.updatedBy = req.user.id;
      if (
        (visit.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA || visit.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA) &&
        visit.prescription
      ) {
        visit.visitType = ENUM.VISIT_TYPE.TREATMENT;
      }
      await visit.save();

      if (returnItem) {
        return {
          status: 200,
          message: "Success",
          data: visit
        };
      }

      res.send({
        status: 200,
        message: "Success",
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  const endVisitIms = async (req, res, next, returnItem = false) => {
    const { visitId } = req.params;

    try {
      if (req.visit.checkOutDate) {
        throw {
          status: 400,
          message: "visit_has_ended"
        };
      }

      const visit = await Visit.findByPk(req.visit.id, {
        include: [
          {
            model: Patient,
            required: true
          },
          {
            model: Prescription,
            required: false
          },
          {
            model: TestHiv,
            required: false
          },
          // {
          //   model: ImsPrescription,
          //   as: 'imsPrescription',
          //   required: false
          // },
          {
            model: VisitIms,
            as: 'testIms',
            required: false
          }
        ]
      });

      // if (!visit.imsPrescription) {
      //   throw {
      //     status: 400,
      //     message: "prescription_not_found"
      //   };
      // }

      if (visit.prescription ) {
        if (!visit.prescription.isDraft) {
          visit.checkOutDate = moment()
          .local()
          .format();
        }
      } else if (visit.testHiv) {
        if (visit.testHiv.kesimpulanHiv != null) {
          visit.checkOutDate = moment()
          .local()
          .format();
        }
      } else {
        visit.checkOutDate = moment()
          .local()
          .format();
      }

      visit.updatedBy = req.user.id;
      if (
        (visit.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA || visit.patient.statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA) &&
        visit.prescription
      ) {
        visit.visitType = ENUM.VISIT_TYPE.TREATMENT;
      }
      await visit.save();

      if (returnItem) {
        return {
          status: 200,
          message: "Success",
          data: visit
        };
      }

      res.send({
        status: 200,
        message: "Success",
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  const createIms = async (req, res, next) => {
    const { reasonVisit } = req.body;
    const visitId = req.params.visitId;
    const findOldVisitIms = await VisitIms.findOne({
      where: {
        visitId
      }
    });
    let error = false;
    const errorObj = {
      status: 409,
      message: "Failed",
      error: {}
    };
    if (findOldVisitIms) {
      return res.send({
        status: 409,
        message: "Failed",
        error: "Data Exist"
      });
    }
    if (error) {
      return res.send(errorObj);
    }
    let createObj = {
      visitId,
      createdBy: req.user.id,
    };
    if (reasonVisit && (ENUM.REASON_VISIT_IMS.indexOf(reasonVisit) >= 0)) {
      createObj = Object.assign(createObj, { reasonVisit });
    }
    try {
      const { id } = await VisitIms.create(createObj);

      const data = await findVisitIms(visitId);

      return res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const updateIms = async (req, res, next) => {
    const visitId = req.params.visitId;
    let error = false;
    const updateObj = {};
    const errorObj = {
      status: 409,
      message: "Failed",
      error: {}
    };
    if (req.user.role == 'DOCTOR') {
      const { complaint } = req.body;
      if (ENUM.COMPLAINT_IMS.indexOf(complaint) < 0) {
        errorObj.error.complaint = [
          "Invalid input"
        ];
        error = true;
      } else {
        updateObj.complaint = complaint
      }
    } else { //check for RR_STAFF
      const { reasonVisit } = req.body;
      updateObj.reasonVisit = ENUM.REASON_VISIT_IMS.indexOf(reasonVisit) >= 0 ? reasonVisit : null;
    }
    if (error) {
      return res.send(errorObj);
    }
    try {
      await VisitIms.update(
        updateObj,
        {
          where: {
            visitId
          }
        }
      );
      const data = await findVisitIms(visitId);

      res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const createExaminationIms = async (req, res, next) => {
    try {
      const visitId = req.params.visitId;
      let { isPregnant, pregnancyStage, upkId, physicalExamination, reasonReferral, isIvaTested, ivaTest, tesLab } = req.body;
      let error = false, ivaQuery = ``;
      const errorObj = {
        status: 409,
        message: "Failed",
        error: {}
      };
      // const findUpkId = await Upk.findOne({
      //   where: {
      //     id: upkId
      //   }
      // });
      const patient = await Patient.findOne({
        where: {
          id: req.visit.patientId
        },
      });
      if (patient.gender == 'PEREMPUAN') {
        isPregnant = isPregnant ? true : false;
        pregnancyStage = !isPregnant ? null : pregnancyStage;
        isIvaTested = isIvaTested ? true : false;
        ivaTest = isIvaTested ? ivaTest : false;
      } else {
        isPregnant = false;
        isIvaTested = false;
        pregnancyStage = null;
      }
      ivaQuery = `, iva_test = ${ivaTest}`;
      let query = `UPDATE ims_visits SET is_iva_tested = ${isIvaTested} ${ivaQuery}, is_pregnant = ${isPregnant}, pregnancy_stage = ${pregnancyStage ? "'" + pregnancyStage + "'" : pregnancyStage}, upk_id = ${upkId}, physical_examination = '${JSON.stringify(physicalExamination)}', reason_referral = '${reasonReferral}', tes_lab = '${tesLab}'  WHERE visit_id = ${visitId}`;
      // console.log(query);
      if (isPregnant) {
        if (ENUM.PREGNANCY_STAGE.indexOf(pregnancyStage) < 0) {
          errorObj.error.pregnancyStage = [
            "Required"
          ];
          error = true;
        }
      }
      if (Object.keys(physicalExamination).length < ENUM.PHYSICAL_EXAMINATION.length) {
        errorObj.error.physicalExamination = [
          "Total Input Less Than Required"
        ];
        error = true;
      }
      // if (!upkId) {
      //   errorObj.error.upkId = [
      //     "Required"
      //   ];
      //   error = true;
      // }
      if (error) {
        return res.send(errorObj);
      }

      physicalExamination = JSON.stringify(physicalExamination);
      await sequelize.query(query, {
        type: sequelize.QueryTypes.UPDATE
      });

      if (!tesLab) {
        const lab = await createLabIms(req, res, next, true);
      }

      res.send({
        status: 200,
        message: "Success",
      });
    } catch (err) {
      next(err);
    }
  };

  const getIms = async (req, res, next) => {
    const visitId = req.params.visitId;
    const role = req.user.role;
    const attributes = [
      'visitId',
      'reasonVisit'
    ];
    if (role == 'DOCTOR') {
      attributes.push('complaint');
    }
    try {
      const data = await VisitIms.findOne({
        where: {
          visitId
        },
        attributes
      });
      return res.send(
        {
          status: 200,
          message: "Success",
          data
        }
      );
    } catch (err) {
      next(err);
    }
  };

  const getExaminationIms = async (req, res, next) => {
    const visitId = req.params.visitId;
    try {
      const data = await VisitIms.findOne({
        where: {
          visitId
        },
        attributes: [
          'isPregnant',
          'pregnancyStage',
          'upkId',
          'physicalExamination',
          'isIvaTested',
          'ivaTest'
        ],
        include: [
          {
            model: Upk,
            attributes: ["name", "id", "provinceId", "sudinKabKotaId"]
          }
        ]
      });
      return res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const createLabIms = async (req, res, next, returnItem = false) => {
    let query;
    const visitId = req.params.visitId;
    const user = req.user;
    let {
      pmnUretraServiks,
      diplokokusIntraselUretraServiks,
      pmnAnus,
      diplokokusIntraselAnus,
      tVaginalis,
      kandida,
      ph,
      sniffTest,
      clueCells,
      rprVdrl,
      realRprVdrl,
      tphaTppa,
      rprVdrlTiter,
      etc,
      is_tested,
      pmnVagina,
      pmnUretra,
      pmnMata,
      trichomonasVag,
      diplokokusIntraselVagina,
      diplokokusIntraselUretra,
      diplokokusIntraselMata,
      pseudohifaBlastospora,
      pemeriksaan
    } = req.body;
    if (!is_tested || returnItem) {
      pmnUretraServiks = null;
      diplokokusIntraselUretraServiks = null;
      pmnAnus = null;
      diplokokusIntraselAnus = null;
      tVaginalis = null;
      kandida = null;
      ph = null;
      sniffTest = null;
      clueCells = null;
      rprVdrl = `'{}'`;
      realRprVdrl = `'{}'`;
      tphaTppa = `'{}'`;
      rprVdrlTiter = `'EMPTY'`;
      etc = null;
      pmnVagina = null;
      pmnUretra = null;
      pmnMata = null;
      trichomonasVag = null;
      diplokokusIntraselVagina = null;
      diplokokusIntraselUretra = null;
      diplokokusIntraselMata = null;
      pseudohifaBlastospora = null;
      pemeriksaan = null;
    } else {
      rprVdrl = `'${JSON.stringify(rprVdrl)}'`;
      realRprVdrl = `'${JSON.stringify(realRprVdrl)}'`;
      tphaTppa = `'${JSON.stringify(tphaTppa)}'`;
      rprVdrlTiter = `'${rprVdrlTiter}'`;
      etc = !etc ? null : `'${etc}'`;
      //
      pmnUretraServiks = pmnUretraServiks != undefined ? pmnUretraServiks : null;
      diplokokusIntraselUretraServiks = diplokokusIntraselUretraServiks != undefined ? diplokokusIntraselUretraServiks : null;
      pmnAnus = pmnAnus != undefined ? pmnAnus : null;
      diplokokusIntraselAnus = diplokokusIntraselAnus != undefined ? diplokokusIntraselAnus : null;
      //
      pmnVagina = pmnVagina != undefined ? pmnVagina : null;
      pmnUretra = pmnUretra != undefined ? pmnUretra : null;
      pmnMata = pmnMata != undefined ? pmnMata : null;
      trichomonasVag = trichomonasVag != undefined ? trichomonasVag : null;
      diplokokusIntraselVagina = diplokokusIntraselVagina != undefined ? diplokokusIntraselVagina : null;
      diplokokusIntraselUretra = diplokokusIntraselUretra != undefined ? diplokokusIntraselUretra : null;
      diplokokusIntraselMata = diplokokusIntraselMata != undefined ? diplokokusIntraselMata : null;
      pseudohifaBlastospora = pseudohifaBlastospora != undefined ? pseudohifaBlastospora : null;
      clueCells = clueCells != undefined ? clueCells : null;
      //
      pemeriksaan = pemeriksaan != undefined ? pemeriksaan : null;
    }

    try {
      const findLab = await VisitImsLab.findOne({
        where: {
          visitId,
        }
      });
      if (findLab) { //update
        query = `UPDATE ims_visit_labs SET
          pmn_uretra_serviks = ${pmnUretraServiks},
          diplokokus_intrasel_uretra_serviks = ${diplokokusIntraselUretraServiks},
          pmn_anus = ${pmnAnus},
          diplokokus_intrasel_anus = ${diplokokusIntraselAnus},
          t_vaginalis = ${tVaginalis},
          kandida = ${kandida},
          ph = ${ph},
          sniff_test = ${sniffTest},
          clue_cells = ${clueCells},
          rpr_vdrl = ${rprVdrl},
          real_rpr_vdrl = ${realRprVdrl},
          tpha_tppa = ${tphaTppa},
          rpr_vdrl_titer = ${rprVdrlTiter},
          etc = ${etc},
          is_tested = ${is_tested},
          pmn_vagina = ${pmnVagina},
          pmn_uretra = ${pmnUretra},
          pmn_mata = ${pmnMata},
          trichomonas_vag = ${trichomonasVag},
          diplokokus_intrasel_vagina = ${diplokokusIntraselVagina},
          diplokokus_intrasel_uretra = ${diplokokusIntraselUretra},
          diplokokus_intrasel_mata = ${diplokokusIntraselMata},
          pseudohifa_blastospora = ${pseudohifaBlastospora},
          pemeriksaan = '${pemeriksaan}'
          WHERE visit_id = ${visitId}
        `;
      } else { //create
        if (returnItem) {
          is_tested = false;
        }
        query = `INSERT INTO ims_visit_labs (
            visit_id,
            pmn_uretra_serviks,
            diplokokus_intrasel_uretra_serviks,
            pmn_anus,
            diplokokus_intrasel_anus,
            t_vaginalis,
            kandida,
            ph,
            sniff_test,
            clue_cells,
            rpr_vdrl,
            real_rpr_vdrl,
            tpha_tppa,
            rpr_vdrl_titer,
            etc,
            is_tested,
            pmn_vagina,
            pmn_uretra,
            pmn_mata,
            trichomonas_vag,
            diplokokus_intrasel_vagina,
            diplokokus_intrasel_uretra,
            diplokokus_intrasel_mata,
            pseudohifa_blastospora,
            pemeriksaan
          ) VALUES (
            ${visitId},
            ${pmnUretraServiks},
            ${diplokokusIntraselUretraServiks},
            ${pmnAnus},
            ${diplokokusIntraselAnus},
            ${tVaginalis},
            ${kandida},
            ${ph},
            ${sniffTest},
            ${clueCells},
            ${rprVdrl},
            ${realRprVdrl},
            ${tphaTppa},
            ${rprVdrlTiter},
            ${etc},
            ${is_tested},
            ${pmnVagina},
            ${pmnUretra},
            ${pmnMata},
            ${trichomonasVag},
            ${diplokokusIntraselVagina},
            ${diplokokusIntraselUretra},
            ${diplokokusIntraselMata},
            ${pseudohifaBlastospora},
            '${pemeriksaan}'
          )`;
      }
      await sequelize.query(query, {
        type: findLab ? sequelize.QueryTypes.UPDATE : sequelize.QueryTypes.INSERT
      });

      //update stok inventory
      const visitIms = await VisitIms.findOne({
        where: {
          visitId
        }
      });
      const visitObject = {
        patientId: req.visit.patientId,
        visitId,
        visitActivityType: ENUM.VISIT_ACT_TYPE.IMS,
        visitActivityId: visitIms.id,
      };
      rprVdrl = req.body.rprVdrl;
      tphaTppa = req.body.tphaTppa;
      if (findLab) {
        const oldRprVdrl = findLab.rprVdrl;
        const oldTphaTppa = findLab.tphaTppa;
        if (rprVdrl.hasOwnProperty('totalReagentUsed') && rprVdrl.totalReagentUsed > 0) {
          let oldQty = Number(oldRprVdrl.totalReagentUsed);
          let newQty = Number(rprVdrl.totalReagentUsed);
          if (oldRprVdrl.reagent == rprVdrl.reagent) {
            let selisih = oldQty > newQty ? (oldQty - newQty) : (newQty - oldQty);
            await InventoryService.updateInventoryUPKByUsage(
              rprVdrl.reagent,
              selisih,
              user,
              visitObject,
              oldQty > newQty ? true : false
            );
          } else {
            //tambah stok obat lama
            await InventoryService.updateInventoryUPKByUsage(
              oldRprVdrl.reagent,
              oldRprVdrl.totalReagentUsed,
              user,
              visitObject,
              true
            );
            //kurangi stok obat pengganti
            await InventoryService.updateInventoryUPKByUsage(
              rprVdrl.reagent,
              rprVdrl.totalReagentUsed,
              user,
              visitObject
            );
          }
        }
        if (tphaTppa.hasOwnProperty('totalReagentUsed') && tphaTppa.totalReagentUsed > 0) {
          let oldQty = Number(oldTphaTppa.totalReagentUsed);
          let newQty = Number(tphaTppa.totalReagentUsed);
          if (oldTphaTppa.reagent == tphaTppa.reagent) {
            let selisih = oldQty > newQty ? (oldQty - newQty) : (newQty - oldQty);
            await InventoryService.updateInventoryUPKByUsage(
              tphaTppa.reagent,
              selisih,
              user,
              visitObject,
              oldQty > newQty ? true : false
            );
          } else {
            //tambah stok obat lama
            await InventoryService.updateInventoryUPKByUsage(
              oldTphaTppa.reagent,
              oldTphaTppa.totalReagentUsed,
              user,
              visitObject,
              true
            );
            //kurangi stok obat pengganti
            await InventoryService.updateInventoryUPKByUsage(
              tphaTppa.reagent,
              tphaTppa.totalReagentUsed,
              user,
              visitObject
            );
            //-------------------------------------------------
          }
        }
      } else {
        //update rprVdrl
        // console.log(rprVdrl)
        await InventoryService.updateInventoryUPKByUsage(
          rprVdrl.reagent,
          Number(rprVdrl.totalReagentUsed),
          user,
          visitObject
        );
        //update tphaTppa
        await InventoryService.updateInventoryUPKByUsage(
          tphaTppa.reagent,
          Number(tphaTppa.totalReagentUsed),
          user,
          visitObject
        );
      }

      if (returnItem) {
        return {
          status: 200,
          message: "Success",
        };
      }

      return res.send({
        status: 200,
        message: "Success",
      });
    } catch (err) {
      if (returnItem) {
        return {
          status: 200,
          message: "Successs",
          data: err
        };
      }

      return res.send({
        status: 200,
        message: "Success",
        data: err
      });
      next(err);
    }
  };

  const findVisitIms = async id => {
    return await Visit.findOne({
      where: {
        id
      },
      include: [
        {
          model: Patient,
          required: true,
          attributes: ["id", "nik", "name", "statusPatient", "upkId"]
        },
        { model: Upk, required: true, attributes: ["id", "name", "provinceId", "sudinKabKotaId"] },
        {
          model: User,
          required: false,
          as: "petugasRR",
          attributes: ["id", "nik", "name", "phone", "upkId"]
        },
        {
          model: VisitIms,
          as: "testIms",
          attributes: [
            "id",
            "reasonVisit",
            "complaint"
          ]
        }
      ]
    });
  };

  const prescriptionIms = async (req, res, next) => {
    const _now = moment().format('YYYY-MM-DD HH:mm:ss');
    let { note, dateGivenMedicine, condom, lubricant, medicines, counseling, isDraft, rencanaKunjungan } = req.body;
    let { user: { id, role }, params: { visitId }, visit: { patientId }, body } = req;
    try {
      const medicines = body.medicines;
      let medicinesToStore = medicines.map(r => {
        return {
          medicineId: r.medicineId,
          totalQty: r.totalQty,
          totalDays: r.totalDays,
          note: r.note,
        }
      });

      body = {
        ...body,
        note: body.note ? body.note : "-",
        medicines: medicinesToStore
      };

      let prescriptionCreated, tempObj = {}, stockLeftObj = {}, stockLeft;
      const schema = Joi.object({
        note: Joi.string().optional(),
        counseling: Joi.string().optional(),
        // dateGivenMedicine: Joi.date().max("now").required(),
        condom: Joi.number().integer().min(0).required(),
        lubricant: Joi.number().integer().min(0).required(),
        isDraft: Joi.boolean(),
        // rencanaKunjungan: Joi.date().required(),
        medicines: Joi.array().items(
          Joi.object({
            medicineId: Joi.number().integer().required(),
            totalQty: Joi.number().integer().min(1).required(),
            totalDays: Joi.number().integer().min(1).required(),
            note: Joi.string().optional()
          })
        )
      }).unknown();
      const { error } = await schema.validate(body);
      if (error) {
        return res.send({
          status: 409,
          message: "Failed",
          data: error
        });
      }

      const patient = await Patient.findByPk(patientId);
      const presc = await ImsPrescription.findOne({
        where: {
          visitId
        }
      });
      const visitIms = await VisitIms.findOne({
        attributes: ['id'],
        where: {
          visitId
        }
      });
      if (!patient) {
        eror.message = "Patient not found";
        throw eror;
      }
      
      if (presc && !presc.isDraft) {
        // eror.message = "Data tidak bisa diubah!";
        eror.message = "prescription_has_already_been_given";
        throw eror;
      }
      for (const val of medicines) {
        const stock = await InventoryService.getMedicineStock(
          req.user,
          null,
          val.medicineId
        );
        if (stock.stock_qty == 0) {
          stockLeftObj[val.medicineId] = stock.stock_qty;
        }
      }
      // stockLeft = Object.keys(stockLeftObj);
      // if (stockLeft.length > 0) {
      //   throw {
      //     message: "lack of medicine stock for ids",
      //     data: stockLeft
      //   };
      // }

      dateGivenMedicine = moment(dateGivenMedicine).format("YYYY-MM-DD");
      const prescriptionObj = {
        patientId,
        visitId,
        dateGivenMedicine,
        condom,
        lubricant,
        isDraft,
        rencanaKunjungan
      };
      if (note) {
        prescriptionObj.note = note;
      }
      if (role == 'DOCTOR' && counseling) {
        prescriptionObj.counseling = counseling;
      }
      if (presc) {
        await ImsPrescription.update(prescriptionObj, {
          where: {
            visitId
          }
        });
      } else {
        prescriptionObj.createdBy = id;
        prescriptionCreated = await ImsPrescription.create(prescriptionObj);
        prescriptionCreated = prescriptionCreated.id;
      }
      for (const val of medicines) {
        //asumsi user masukin medicineId valid
        let obj = {
          medicineId: val.medicineId,
          prescription_id: prescriptionCreated || presc.id
        };
        const visitObject = {
          patientId,
          visitId,
          visitActivityType: ENUM.VISIT_ACT_TYPE.IMS,
          visitActivityId: visitIms.id
        };
        const obat = await Medicine.findByPk(val.medicineId);
        const imsObat = await ImsPrescriptionMedicine.findOne({
          where: obj
        });

        if (Number.isInteger(tempObj[val.medicineId])) {
          tempObj[val.medicineId] += val.totalQty;
        } else {
          tempObj[val.medicineId] = val.totalQty;
        }

        if (imsObat) {
          Object.assign(imsObat, {
            totalQty: tempObj[val.medicineId],
            totalDays: val.totalDays,
            note: val.note || "",
            updatedBy: id
          });
          await imsObat.save();
        } else {
          Object.assign(obj, {
            prescriptionId: prescriptionCreated || presc.id,
            medicineName: obat.name,
            totalQty: val.totalQty,
            totalDays: val.totalDays,
            note: val.note,
            createdBy: id
          });
          await ImsPrescriptionMedicine.create(obj);
          await InventoryService.updateInventoryUPKByUsage(
            val.medicineId,
            Number(val.totalQty),
            req.user,
            visitObject
          );
        }
      }

      return res.send({
        status: 200,
        message: "Success"
      });
    } catch (err) {
      next(err);
    }
  };

  const fetchPrescriptionIms = async (req, res, next) => {
    const visitId = req.params.visitId;
    try {
      const data = await ImsPrescription.findOne({
        attributes: ImsPrescription.showSpecAttr,
        where: {
          visitId
        },
        include: [
          {
            model: ImsPrescriptionMedicine,
            as: "medicines",
            attributes: [
              "medicineId",
              "medicineName",
              "totalQty",
              "totalDays",
              "note"
            ]
          }
        ]
      });
      return res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const updateDiagnosis = async (req, res, next) => {
    try {
      const visitId = req.params.visitId;
      const { syndrome, klinis, lab } = req.body;
      let query = `UPDATE ims_visit_labs SET diagnosis_syndrome = '${JSON.stringify(syndrome)}', diagnosis_klinis = '${JSON.stringify(klinis)}', diagnosis_lab = '${JSON.stringify(lab)}' WHERE visit_id = ${visitId}`;
      await sequelize.query(query, {
        type: sequelize.QueryTypes.UPDATE
      });
      return res.send({
        status: 200,
        message: "Success"
      });
    } catch (err) {
      next(err);
    }
  };

  const fetchImsLab = async (req, res, next) => {
    try {
      const visitId = req.params.visitId;
      const attributes = [
        'id',
        'pmnUretraServiks',
        'diplokokusIntraselUretraServiks',
        'pmnAnus',
        'diplokokusIntraselAnus',
        'tVaginalis',
        'kandida',
        'ph',
        'sniffTest',
        'clueCells',
        'rprVdrl',
        'tphaTppa',
        'rprVdrlTiter',
        'etc',
      ];
      if (req.user.role == 'DOCTOR') {
        attributes.push('diagnosisSyndrome', 'diagnosisKlinis', 'diagnosisLab');
      }
      const data = await VisitImsLab.findOne({
        where: {
          visitId
        },
        attributes
      });
      return res.send({
        status: 200,
        message: 'Success',
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const makeFirstCharUppercase = function (word) {
    return `${word.charAt(0).toUpperCase()}${word.slice(1)}`;
  }

  const reportByAge = async (req, res, next) => {
    const { tgl, province, sudin, upk, format } = req.query;
    try {
      let result = [], saring = [], temp, tempQuery, mana;
      const female = makeFirstCharUppercase(ENUM.GENDER.PEREMPUAN);
      const male = makeFirstCharUppercase(ENUM.GENDER.LAKI_LAKI);
      const ttlRowsPerQuery = 13;
      const titlesChild = [
        "<1",
        "1-4",
        "5-14",
        "15-19",
        "20-24",
        "25-29",
        "30-34",
        "35-39",
        "40-44",
        "45-49",
        "50-59",
        ">=60",
        "Jumlah",
      ]
      const selisihTahun = `EXTRACT( YEAR FROM now()::date ) - EXTRACT( YEAR FROM patients.date_birth )`;
      const awal = `SELECT
      SUM( CASE WHEN (${selisihTahun}) = 0 THEN 1 ELSE 0 END) AS "0",
      SUM( CASE WHEN (${selisihTahun}) >= 1 AND (${selisihTahun}) <= 4 THEN 1 ELSE 0 END) AS "1",
      SUM( CASE WHEN (${selisihTahun}) >= 5 AND (${selisihTahun}) <= 14 THEN 1 ELSE 0 END) AS "2",
      SUM( CASE WHEN (${selisihTahun}) >= 15 AND (${selisihTahun}) <= 19 THEN 1 ELSE 0 END) AS "3",
      SUM( CASE WHEN (${selisihTahun}) >= 20 AND (${selisihTahun}) <= 24 THEN 1 ELSE 0 END) AS "4",
      SUM( CASE WHEN (${selisihTahun}) >= 25 AND (${selisihTahun}) <= 29 THEN 1 ELSE 0 END) AS "5",
      SUM( CASE WHEN (${selisihTahun}) >= 30 AND (${selisihTahun}) <= 34 THEN 1 ELSE 0 END) AS "6",
      SUM( CASE WHEN (${selisihTahun}) >= 35 AND (${selisihTahun}) <= 39 THEN 1 ELSE 0 END) AS "7",
      SUM( CASE WHEN (${selisihTahun}) >= 40 AND (${selisihTahun}) <= 44 THEN 1 ELSE 0 END) AS "8",
      SUM( CASE WHEN (${selisihTahun}) >= 45 AND (${selisihTahun}) <= 49 THEN 1 ELSE 0 END) AS "9",
      SUM( CASE WHEN (${selisihTahun}) >= 50 AND (${selisihTahun}) <= 59 THEN 1 ELSE 0 END) AS "10",
      SUM( CASE WHEN (${selisihTahun}) >= 60 THEN 1 ELSE 0 END) AS "11",
      COUNT(*) AS "12"`;
      const roleId = req.user.role;

      if (tgl) {
        saring.push(`visits.visit_date::date BETWEEN '${moment(tgl).startOf('month').format('YYYY-MM-DD')}' AND '${moment(tgl).endOf('month').format('YYYY-MM-DD')}'`);
      }
      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        saring.push(`upk.id = ${req.user.upkId}`);
      } else {
        if (upk && ['MINISTRY_STAFF', 'PROVINCE_STAFF', 'SUDIN_STAFF'].indexOf(roleId) >= 0) {
          saring.push(`upk.id = ${upk}`);
        }
      }
      if (roleId == 'SUDIN_STAFF') {
        saring.push(`upk.sudin_kab_kota_id = ${sudin}`);
      } else {
        if (sudin && ['MINISTRY_STAFF', 'PROVINCE_STAFF'].indexOf(roleId) >= 0) {
          saring.push(`upk.sudin_kab_kota_id = ${req.user.sudinKabKotaId}`);
        }
      }
      if (province && roleId == 'MINISTRY_STAFF') {
        saring.push(`upk.province_id = ${province}`);
      }
      mana = saring.length > 0 ? saring.join(" AND ") : "";
      tempQuery = `${awal}, gender FROM visits
        JOIN patients ON visits.patient_id = patients.id
        JOIN upk ON visits.upk_id = upk.id
        JOIN ims_visits ON visits.id = ims_visits.visit_id
        ${mana ? 'WHERE ' + mana : mana} GROUP BY gender, patients.id;`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Jumlah yang berkunjung", true, true, ttlRowsPerQuery));

      let queryTable = `, gender FROM ims_visit_labs JOIN visits ON ims_visit_labs.visit_id = visits.id JOIN patients ON visits.patient_id = patients.id JOIN upk ON visits.upk_id = upk.id`;
      const grup = `GROUP BY gender`;
      //------------------------------------------------------------------------------------------------------------------------

      //------------------------------------------------------------------------------------------------------------------------
      result.push(["Sindromik"]);
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"dtv": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Duh Tubuh Vagina", true, false, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"dtu": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Duh Tubuh Uretra", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"dta": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Duh Tubuh Anus", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"dtm": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Duh Tubuh Mata", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"prp": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Penyakit Radang Panggul", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"ug": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Ulkus genital", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"pembengkakanSkrotum": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Pembengkakan Skortum", false, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_syndrome @> '{"bi": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Bubo inguinal", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------

      //------------------------------------------------------------------------------------------------------------------------
      result.push(["Klinis"]);
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"herpes": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Herpes genitalis", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"sifilis": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Sifilis primer", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"sifilisSekunder": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Sifilis sekunder", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"chancroid": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Chancroid", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"kutilAnogenital": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Kutil anogenital", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"uretritis": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Uretritis gonore", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"moluskum": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Moluskum", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"moluskum": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Moluskum", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"kontagiosum": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Kontagiosum", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------

      //------------------------------------------------------------------------------------------------------------------------
      result.push(["Laboratorium"]);
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"servisitisGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Servisitis gonore", true, false, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"ServisitisNonGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Servisitis non gonore", true, false, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"VaginitisGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Vaginitis gonore", true, false, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"VaginitisNonGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Vaginitis non gonore", true, false, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"uritritisGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Uritritis gonore", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"uritritisNonGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Uritritis non gonore", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"proktitisGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Proktitis gonore", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"proktitisNonGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Proktitis non gonore", true, true, ttlRowsPerQuery));
      // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"konjungtivitisGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Konjungtivitis gonore", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"konjungtivitisNonGonore": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Konjungtivitis non gonore", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"sifilisDini": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Sifilis dini", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"sifilisLaten": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Sifilis laten", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"sifilisKongenital": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Sifilis kongenital", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"trikomoniasis": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Trikomoniasis", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"vaginosisBakterial": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Vaginosis bakterial", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"kandidiasis": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Kandidiasis", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${queryTable} WHERE diagnosis_lab @> '{"limfogranulomaVenereum": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(checkArrayReport(temp, "Limfogranuloma venereum", true, true, ttlRowsPerQuery));
      //------------------------------------------------------------------------------------------------------------------------

      // //------------------------------------------------------------------------------------------------------------------------
      // result.push(["Sifilis"]);
      // //------------------------------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${queryTable} WHERE rpr_vdrl_titer <> 'EMPTY' ${mana ? 'AND ' + mana : mana} ${grup};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(checkArrayReport(temp, "Jumlah yang diperiksa", true, true, ttlRowsPerQuery));
      // //------------------------------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${queryTable} WHERE diagnosis_klinis @> '{"sifilis": true}' AND diagnosis_lab @> '{"sifilis": true}' ${mana ? 'AND ' + mana : mana} ${grup};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(checkArrayReport(temp, "Jumlah yang positif Sifilis", true, true, ttlRowsPerQuery));
      // //------------------------------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${queryTable} WHERE rpr_vdrl_titer IN ('1/8', '1/4', '1/2') ${mana ? 'AND ' + mana : mana} ${grup};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(checkArrayReport(temp, "Jumlah Sifilis Aktif (titer >= 1:8 atau naik 4 kali lipat dari pemeriksaan sebelumnya)", true, true, ttlRowsPerQuery));
      // //------------------------------------------------------------------------------------------------------------------------
      // queryTable = `, gender FROM ims_prescriptions JOIN visits ON ims_prescriptions.visit_id = visits.id JOIN patients ON visits.patient_id = patients.id JOIN upk ON visits.upk_id = upk.id`;
      // tempQuery = `${awal} ${queryTable} ${mana ? 'AND ' + mana : mana} ${grup};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(checkArrayReport(temp, "Jumlah yang diobati", true, true, ttlRowsPerQuery));
      // //------------------------------------------------------------------------------------------------------------------------
      if (format == 'excel') {
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA"];
        const sheetObj = {
          "Laporan 1": {
            "!ref": "A1:AA35",
            A1: { t: 's', v: "Indikator" }, // t => type s => string, n => number, b => boolean
            B1: { t: 's', v: "Perempuan" },
            O1: { t: 's', v: "Laki-laki" },
            "!cols": [
              { wch: 30 }
            ],
            "!merges": [
              { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
              { s: { r: 0, c: 1 }, e: { r: 0, c: 13 } }, /* r => row ke berapa, c => column ke berapa */
              { s: { r: 0, c: 14 }, e: { r: 0, c: 26 } },
            ]
          }
        };
        for (let i = 0; i < 2; i++) {
          _.forEach(indexCols, function (item, i) {
            if (i > 0) {
              sheetObj["Laporan 1"][`${item}2`] = {
                t: 's',
                v: titlesChild[(i - 1) % titlesChild.length]
              }
            }
          });
        }
        _.forEach(result, function (val, index) {
          _.forEach(val, function (item, i) {
            sheetObj["Laporan 1"][`${indexCols[i]}${index + 3}`] = {
              t: 's',
              v: item ? item : ''
            }
          });
        });
        XLSX.writeFile({
          SheetNames: ["Laporan 1"],
          Sheets: sheetObj
        }, './api/excel/Laporan1.xlsx');
        // return res.download('./api/excel/Laporan1.xlsx')
        return res.send({
          status: 200,
          message: "Success",
          data: "/api/excel/Laporan1.xlsx"
        });
      } else if (format == 'pdf') {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('https://facebook.com/', {
          waitUntil: 'load',
        });
        await page.pdf({
          path: './api/pdf/Laporan1.pdf',
          format: 'letter',
          scale: 0.7,
          printBackground: true,
          landscape: true,
          // width: 1024,
          // height: 768
        });
        await browser.close();
        return res.send({
          status: 200,
          message: "Success",
        });
      }
      return res.send({
        status: 200,
        message: "Success",
        data: {
          rowsTitle: [
            {
              title: "Indikator",
              child: [],
            },
            {
              title: female,
              children: titlesChild,
            },
            {
              title: male,
              children: titlesChild,
            }
          ],
          data: result
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const reportByAge2 = async (req, res, next) => {
    try {
      const { tgl, province, sudin, upk, format } = req.query;
      const roleId = req.user.role;
      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( visits.visit_date, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (province) {
        where.push(` provinces.ID = ${province} `);
      }
      if (sudin) {
        where.push(` sudin_kab_kota.ID = ${sudin} `);
      }
      if (upk) {
        where.push(` upk.ID = ${upk} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
          SELECT
            upk.ID AS upk,
            sudin_kab_kota.ID AS sudin_kab_kota,
            provinces.ID AS province,
            patients.nik,
            patients.fullname,
            patients.gender,
            -- 'PEREMPUAN' AS gender,
            EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
            patients.kelompok_populasi,
            ims_visit_labs.diagnosis_syndrome,
            ims_visit_labs.diagnosis_klinis,
            ims_visit_labs.diagnosis_lab,
            visits.visit_date 
          FROM
            ims_visit_labs
            JOIN visits ON visits.ID = ims_visit_labs.visit_id
            JOIN patients ON patients.ID = visits.patient_id
            JOIN upk ON upk.ID = patients.upk_id
            JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
            JOIN provinces ON provinces.ID = sudin_kab_kota.province_id
          ${newWhere}
        `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      const DIAGNOSIS_SYNDROME = {
        "dtv": "Duh Tubuh Vagina",
        "dtu": "Duh Tubuh Uretra",
        "dta": "Duh Tubuh Anus",
        "dtm": "Duh Tubuh Mata",
        "prp": "Penyakit Radang Panggul",
        "ug": "Ulkus genital",
        "pembengkakanSkrotum": "Pembengkakan Skortum",
        "bi": "Bubo inguinal"
      };
      const DIAGNOSIS_KLINIS = {
        "herpes": "Herpes genitalis",
        "sifilis": "Sifilis primer",
        "sifilisSekunder": "Sifilis sekunder",
        "chancroid": "Chancroid",
        "kutilAnogenital": "Kutil anogenital",
        "uretritis": "Uretritis gonore",
        "moluskum": "Moluskum",
        "kontagiosum": "Kontagiosum"
      };
      const DIAGNOSIS_LAB = {
        "servisitisGonore": "Servisitis gonore",
        "ServisitisNonGonore": "Servisitis non gonore",
        "VaginitisGonore": "Vaginitis gonore",
        "VaginitisNonGonore": "Vaginitis non gonore",
        // -----------
        "uritritisGonore": "Uritritis gonore",
        "uritritisNonGonore": "Uritritis non gonore",
        "proktitisGonore": "Proktitis gonore",
        "proktitisNonGonore": "Proktitis non gonore",
        "konjungtivitisGonore": "Konjungtivitis gonore",
        "konjungtivitisNonGonore": "Konjungtivitis non gonore",
        "sifilisDini": "Sifilis dini",
        "sifilisLaten ": "Sifilis laten",
        "sifilisKongenital": "Sifilis kongenital",
        "trikomoniasis": "Trikomoniasis",
        "vaginosisBakterial": "Vaginosis bakterial",
        "kandidiasis": "Kandidiasis",
        "limfogranulomaVenereum": "Limfogranuloma venereum",
      };

      let listData = {
        "Jumlah yang berkunjung": [],
        // SYNDROME -------------------------
        "dtv": [],
        "dtu": [],
        "dta": [],
        "dtm": [],
        "prp": [],
        "ug": [],
        "pembengkakanSkrotum": [],
        "bi": [],
        // KLINIS -------------------------
        "herpes": [],
        "sifilis": [],
        "sifilisSekunder": [],
        "chancroid": [],
        "kutilAnogenital": [],
        "uretritis": [],
        "moluskum": [],
        "kontagiosum": [],
        // LAB ---------------------------
        "servisitisGonore": [],
        "ServisitisNonGonore": [],
        "VaginitisGonore": [],
        "VaginitisNonGonore": [],
        // ---------
        "uritritisGonore": [],
        "uritritisNonGonore": [],
        "proktitisGonore": [],
        "proktitisNonGonore": [],
        "konjungtivitisGonore": [],
        "konjungtivitisNonGonore": [],
        "sifilisDini": [],
        "sifilisLaten ": [],
        "sifilisKongenital": [],
        "trikomoniasis": [],
        "vaginosisBakterial": [],
        "kandidiasis": [],
        "limfogranulomaVenereum": []
      };

      const _insertData = (listData, r, p, l) => {
        if (r.gender == ENUM.GENDER.PEREMPUAN) {
          listData['Jumlah yang berkunjung'][p] += 1;
          Object.keys(DIAGNOSIS_SYNDROME).map(key => {
            if (r.diagnosis_syndrome[key] == true) {
              listData[key][p] += 1;
              listData[key][12] += 1;
            }
          });
          Object.keys(DIAGNOSIS_KLINIS).map(key => {
            if (r.diagnosis_klinis[key] == true) {
              listData[key][p] += 1;
              listData[key][12] += 1;
            }
          });
          Object.keys(DIAGNOSIS_LAB).map(key => {
            if (r.diagnosis_lab[key] == true) {
              listData[key][p] += 1;
              listData[key][12] += 1;
            }
          });
        } else {
          listData['Jumlah yang berkunjung'][l] += 1;
          Object.keys(DIAGNOSIS_SYNDROME).map(key => {
            if (r.diagnosis_syndrome[key] == true) {
              listData[key][l] += 1;
              listData[key][25] += 1;
            }
          });
          Object.keys(DIAGNOSIS_KLINIS).map(key => {
            if (r.diagnosis_klinis[key] == true) {
              listData[key][l] += 1;
              listData[key][25] += 1;
            }
          });
          Object.keys(DIAGNOSIS_LAB).map(key => {
            if (r.diagnosis_lab[key] == true) {
              listData[key][l] += 1;
              listData[key][25] += 1;
            }
          });
        }
      }

      for (let i = 0; i < 26; i++) {
        if (listData["Jumlah yang berkunjung"][i] == undefined) {
          listData["Jumlah yang berkunjung"][i] = 0;
          Object.keys(DIAGNOSIS_SYNDROME).map(key => {
            listData[key][i] = 0;
          });
          Object.keys(DIAGNOSIS_KLINIS).map(key => {
            listData[key][i] = 0;
          })
          Object.keys(DIAGNOSIS_LAB).map(key => {
            listData[key][i] = 0;
          });
        }
      }

      data.map(r => {
        if (r.age < 1) {
          _insertData(listData, r, 0, 13);
        } else if (r.age >= 1 && r.age <= 4) {
          _insertData(listData, r, 1, 14);
        } else if (r.age >= 5 && r.age <= 14) {
          _insertData(listData, r, 2, 15);
        } else if (r.age >= 15 && r.age <= 19) {
          _insertData(listData, r, 3, 16);
        } else if (r.age >= 20 && r.age <= 24) {
          _insertData(listData, r, 4, 17);
        } else if (r.age >= 25 && r.age <= 29) {
          _insertData(listData, r, 5, 18);
        } else if (r.age >= 30 && r.age <= 34) {
          _insertData(listData, r, 6, 19);
        } else if (r.age >= 35 && r.age <= 39) {
          _insertData(listData, r, 7, 20);
        } else if (r.age >= 40 && r.age <= 44) {
          _insertData(listData, r, 8, 21);
        } else if (r.age >= 45 && r.age <= 49) {
          _insertData(listData, r, 9, 22);
        } else if (r.age >= 50 && r.age <= 59) {
          _insertData(listData, r, 10, 23);
        } else if (r.age >= 60) {
          _insertData(listData, r, 11, 24);
        }

        if (r.gender == ENUM.GENDER.PEREMPUAN) {
          listData['Jumlah yang berkunjung'][12] += 1;
        } else {
          listData['Jumlah yang berkunjung'][25] += 1;
        }
      });

      let result = [];
      result.push(["Jumlah yang berkunjung", ...listData["Jumlah yang berkunjung"]]);
      result.push(["Sindromik"]);
      Object.keys(DIAGNOSIS_SYNDROME).map(key => {
        result.push([DIAGNOSIS_SYNDROME[key], ...listData[key]]);
      });
      result.push(["Klinis"]);
      Object.keys(DIAGNOSIS_KLINIS).map(key => {
        result.push([DIAGNOSIS_KLINIS[key], ...listData[key]]);
      })
      result.push(["Laboratorium"]);
      Object.keys(DIAGNOSIS_LAB).map(key => {
        result.push([DIAGNOSIS_LAB[key], ...listData[key]]);
      })

      if (format == 'excel') {
        const titlesChild = [
          "<1",
          "1-4",
          "5-14",
          "15-19",
          "20-24",
          "25-29",
          "30-34",
          "35-39",
          "40-44",
          "45-49",
          "50-59",
          ">=60",
          "Jumlah",
        ]
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA"];
        const sheetObj = {
          "Laporan 1": {
            "!ref": "A1:AA35",
            A1: { t: 's', v: "Indikator" }, // t => type s => string, n => number, b => boolean
            B1: { t: 's', v: "Perempuan" },
            O1: { t: 's', v: "Laki-laki" },
            "!cols": [
              { wch: 30 }
            ],
            "!merges": [
              { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
              { s: { r: 0, c: 1 }, e: { r: 0, c: 13 } }, /* r => row ke berapa, c => column ke berapa */
              { s: { r: 0, c: 14 }, e: { r: 0, c: 26 } },
            ]
          }
        };
        for (let i = 0; i < 2; i++) {
          _.forEach(indexCols, function (item, i) {
            if (i > 0) {
              sheetObj["Laporan 1"][`${item}2`] = {
                t: 's',
                v: titlesChild[(i - 1) % titlesChild.length]
              }
            }
          });
        }
        _.forEach(result, function (val, index) {
          let hideLaki = false;
          let hidePerempuan = false;
          _.forEach(val, function (item, i) {
            if (["Duh Tubuh Vagina", "Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore"].includes(item)) {
              hideLaki = true;
            }
            if (["Pembengkakan Skortum"].includes(item)) {
              hidePerempuan = true;
            }
            sheetObj["Laporan 1"][`${indexCols[i]}${index + 3}`] = {
              t: 's',
              v: item ? item : (hideLaki ? (i >= 14 && i <= 26 ? '-' : item) : (hidePerempuan ? (i >= 1 && i <= 13 ? '-' : item) : '0'))
            }
          });
        });
        XLSX.writeFile({
          SheetNames: ["Laporan 1"],
          Sheets: sheetObj
        }, './api/excel/Laporan1.xlsx');
        // return res.download('./api/excel/Laporan1.xlsx')
        return res.send({
          status: 200,
          message: "Success",
          data: "/api/excel/Laporan1.xlsx"
        });
      } else if (format == 'pdf') {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('https://facebook.com/', {
          waitUntil: 'load',
        });
        await page.pdf({
          path: './api/pdf/Laporan1.pdf',
          format: 'letter',
          scale: 0.7,
          printBackground: true,
          landscape: true,
          // width: 1024,
          // height: 768
        });
        await browser.close();
        return res.send({
          status: 200,
          message: "Success",
        });
      }

      return res.send({
        status: 200,
        message: "Success",
        data: {
          data: result
        }
      });
    } catch (err) {
      next(err);
    }
  }

  const reportByAll = async (req, res, next) => {
    try {
      const { tgl, province, sudin, upk, format } = req.query;
      const roleId = req.user.role;
      let
        where = [
          `ims_visits.tes_lab = TRUE`
        ],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( visits.visit_date, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (province) {
        where.push(` provinces.ID = ${province} `);
      }
      if (sudin) {
        where.push(` sudin_kab_kota.ID = ${sudin} `);
      }
      if (upk) {
        where.push(` upk.ID = ${upk} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          upk.ID AS upk,
          sudin_kab_kota.ID AS sudin_kab_kota,
          provinces.ID AS province,
          patients.nik,
          patients.fullname,
          patients.gender,
          EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
          patients.kelompok_populasi,
          patients.status_patient,
          patients.lsm_penjangkau,
          ims_visit_labs.diagnosis_syndrome,
          ims_visit_labs.diagnosis_klinis,
          ims_visit_labs.diagnosis_lab,
          ims_visit_labs.pemeriksaan,
          ims_visit_labs.tpha_tppa,
	        ims_visit_labs.rpr_vdrl,
          visits.visit_date,
          ims_prescriptions.date_given_medicine,
          ims_prescriptions.condom,
          test_hiv.kesimpulan_hiv,
          ims_visits.tes_lab,
          ims_visits.is_iva_tested
        FROM
          ims_visit_labs
          JOIN visits ON visits.ID = ims_visit_labs.visit_id
          LEFT JOIN ims_visits ON ims_visits.visit_id = ims_visit_labs.visit_id
          LEFT JOIN ims_prescriptions ON ims_prescriptions.visit_id = ims_visit_labs.visit_id
          LEFT JOIN test_hiv ON test_hiv.visit_id = ims_visit_labs.visit_id
          JOIN patients ON patients.ID = visits.patient_id
          JOIN upk ON upk.ID = patients.upk_id
          JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
          JOIN provinces ON provinces.ID = sudin_kab_kota.province_id
        ${newWhere}
        `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      const DIAGNOSIS_SYNDROME = {
        "dtv": "Duh Tubuh Vagina",
        "dtu": "Duh Tubuh Uretra",
        "dta": "Duh Tubuh Anus",
        "dtm": "Duh Tubuh Mata",
        "prp": "Penyakit Radang Panggul",
        "ug": "Ulkus genital",
        "pembengkakanSkrotum": "Pembengkakan Skortum",
        "bi": "Bubo inguinal"
      };
      const DIAGNOSIS_KLINIS = {
        "herpes": "Herpes genitalis",
        "sifilis": "Sifilis primer",
        "sifilisSekunder": "Sifilis sekunder",
        "chancroid": "Chancroid",
        "kutilAnogenital": "Kutil anogenital",
        "uretritis": "Uretritis gonore",
        "moluskum": "Moluskum",
        "kontagiosum": "Kontagiosum"
      };
      const DIAGNOSIS_LAB = {
        "servisitisGonore": "Servisitis gonore",
        "VaginitisGonore": "Vaginitis gonore",
        "uritritisGonore": "Uritritis gonore",
        "proktitisGonore": "Proktitis gonore",
        "konjungtivitisGonore": "Konjungtivitis gonore",
        // ---------------------------------------
        "ServisitisNonGonore": "Servisitis non gonore",
        "VaginitisNonGonore": "Vaginitis non gonore",
        "uritritisNonGonore": "Uritritis non gonore",
        "proktitisNonGonore": "Proktitis non gonore",
        "konjungtivitisNonGonore": "Konjungtivitis non gonore",
        "sifilisDini": "Sifilis dini",
        "sifilisKongenital": "Sifilis kongenital",
        // "sifilisLaten ": "Sifilis laten",
        "sifilisLaten ": "Sifilis lanjut",
        "trikomoniasis": "Trikomoniasis",
        "vaginosisBakterial": "Vaginosis bakterial",
        "kandidiasis": "Kandidiasis",
        "limfogranulomaVenereum": "Limfogranuloma venereum",
      };
      const KEL_POPULASI = {
        "WPS": 27,
        "LSL": 28,
        "Waria": 29,
        "Penasun": 30,
        "Bumil": 31,
        "Pasien Hepatitis": 33,
        "Pasangan ODHA": 34,
        "Anak ODHA": 35,
        "WBP": 36,
        "Populasi Umum": 37
      };

      let listData = {
        "Jumlah yang berkunjung": [],
        "Jumlah pasien IMS yang ditemukan": [],
        "Jumlah kondom yang diberikan": [],
        "Jumlah pasien IMS yang tes HIV": [],
        "Jumlah pasien yang berkunjung yang dirujuk oleh LSM": [],
        // SYNDROME -----------------------------------------------
        "dtv": [],
        "dtu": [],
        "dta": [],
        "dtm": [],
        "prp": [],
        "ug": [],
        "pembengkakanSkrotum": [],
        "bi": [],
        // KLINIS -------------------------------------------------
        "herpes": [],
        "sifilis": [],
        "sifilisSekunder": [],
        "chancroid": [],
        "kutilAnogenital": [],
        "uretritis": [],
        "moluskum": [],
        "kontagiosum": [],
        // LAB -----------------------------------------------------
        "servisitisGonore": [],
        "ServisitisNonGonore": [],
        "VaginitisGonore": [],
        "VaginitisNonGonore": [],
        // ---------
        "uritritisGonore": [],
        "uritritisNonGonore": [],
        "proktitisGonore": [],
        "proktitisNonGonore": [],
        "konjungtivitisGonore": [],
        "konjungtivitisNonGonore": [],
        "sifilisDini": [],
        "sifilisLaten ": [],
        "sifilisKongenital": [],
        "trikomoniasis": [],
        "vaginosisBakterial": [],
        "kandidiasis": [],
        "limfogranulomaVenereum": [],
        // SIFILIS -------------------------------------------------------------
        "Jumlah pasien yang dites Sifilis": [],
        "Jumlah pasien yang positif Sifilis": [],
        "Jumlah pasien sifilis yang diobati": [],
        "Jumlah pasien Sifilis yang diperiksa IVA Test": [],
        "Jumlah pasien yang positif Gonore": [],
        "Jumlah pasien gonore yang diobati": []
      };

      const _insertImsFoundData = (listData, r, i, totalIndex, item, itemName, imsIsFound) => {
        item.map(v => {
          if (!imsIsFound) {
            if (r[itemName][v] == true) {
              listData['Jumlah pasien IMS yang ditemukan'][26] += 1;
              listData['Jumlah pasien IMS yang ditemukan'][i] += 1;
              listData['Jumlah pasien IMS yang ditemukan'][totalIndex] += 1;
              imsIsFound = true;

              // by kelompok populasi ------------------------
              r.kelompok_populasi.map(v => {
                if (KEL_POPULASI[v]) {
                  listData['Jumlah pasien IMS yang ditemukan'][KEL_POPULASI[v]] += 1;
                }
              });

              if (r.status_patient == "ODHA") {
                listData['Jumlah pasien IMS yang ditemukan'][32] += 1;
              }
              // end by kelompok populasi ------------------------
            }
          }
        });

        return imsIsFound;
      }

      const _insertDiagnosisData = (listData, r, i, totalIndex, item, itemName) => {
        Object.keys(item).map(key => {
          if (r[itemName][key] == true) {
            // by umur dan jenis kelamin ------------------------
            listData[key][i] += 1;
            listData[key][totalIndex] += 1;
            listData[key][26] += 1;

            // Jumlah pasien yang positif Gonore ----------------------------------------------------------------------
            if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key)) {
              listData['Jumlah pasien yang positif Gonore'][i] += 1;
              listData['Jumlah pasien yang positif Gonore'][totalIndex] += 1;
              listData['Jumlah pasien yang positif Gonore'][26] += 1;
            }
            // end Jumlah pasien yang positif Gonore ----------------------------------------------------------------------

            // Jumlah pasien gonore yang diobati ----------------------------------------------------------------------
            if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key) && r.date_given_medicine != null) {
              listData['Jumlah pasien gonore yang diobati'][i] += 1;
              listData['Jumlah pasien gonore yang diobati'][totalIndex] += 1;
              listData['Jumlah pasien gonore yang diobati'][26] += 1;
            }
            // end Jumlah pasien gonore yang diobati ----------------------------------------------------------------------
            // end by umur dan jenis kelamin ------------------------

            // by kelompok populasi ------------------------
            r.kelompok_populasi.map(v => {
              if (KEL_POPULASI[v]) {
                listData[key][KEL_POPULASI[v]] += 1;
                if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key)) {
                  listData['Jumlah pasien yang positif Gonore'][KEL_POPULASI[v]] += 1;
                }
                if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key) && r.date_given_medicine != null) {
                  listData['Jumlah pasien gonore yang diobati'][KEL_POPULASI[v]] += 1;
                }
              }
            });

            if (r.status_patient == "ODHA") {
              listData[key][32] += 1;
              if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key)) {
                listData['Jumlah pasien yang positif Gonore'][32] += 1;
              }
              if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key) && r.date_given_medicine != null) {
                listData['Jumlah pasien gonore yang diobati'][32] += 1;
              }
            }
            // end by kelompok populasi ------------------------
          }
        });
      }

      // laki 0-12 | perempuan 13-25 | total 26 | kelompok populasi 27-36
      const _insertData = (listData, r, l, p) => {
        let i = null;
        let totalIndex = null;
        if (r.gender == ENUM.GENDER.PEREMPUAN) {
          i = p;
          totalIndex = 25;
        } else {
          i = l;
          totalIndex = 12;
        }

        // by umur dan jenis kelamin ------------------------
        // Jumlah yang berkunjung ----------------------------------------------------------------------
        listData['Jumlah yang berkunjung'][i] += 1;
        listData['Jumlah yang berkunjung'][totalIndex] += 1;
        listData['Jumlah yang berkunjung'][26] += 1;
        // end Jumlah yang berkunjung ----------------------------------------------------------------------

        // Jumlah kondom yang diberikan -------------------------------------------------------------------
        listData['Jumlah kondom yang diberikan'][i] += r.condom;
        listData['Jumlah kondom yang diberikan'][totalIndex] += r.condom;
        listData['Jumlah kondom yang diberikan'][26] += r.condom;
        // end Jumlah kondom yang diberikan -------------------------------------------------------------------

        // Jumlah pasien IMS yang tes HIV -------------------------------------------------------------------
        if (r.kesimpulan_hiv == "REAKTIF") {
          listData['Jumlah pasien IMS yang tes HIV'][i] += 1;
          listData['Jumlah pasien IMS yang tes HIV'][totalIndex] += 1;
          listData['Jumlah pasien IMS yang tes HIV'][26] += 1;
        }
        // end Jumlah pasien IMS yang tes HIV -------------------------------------------------------------------

        // Jumlah pasien yang berkunjung yang dirujuk oleh LSM -------------------------------------------------------------------
        if (["SPIRITIA", "IAC", "UNFPA", "IPPI", "YPI", "PKVHI", "PKBI"].includes(r.lsm_penjangkau)) {
          listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][i] += 1;
          listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][totalIndex] += 1;
          listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][26] += 1;
        }
        // end Jumlah pasien yang berkunjung yang dirujuk oleh LSM -------------------------------------------------------------------

        // Jumlah pasien yang dites Sifilis -------------------------------------------------------------------
        if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan)) {
          listData['Jumlah pasien yang dites Sifilis'][i] += 1;
          listData['Jumlah pasien yang dites Sifilis'][totalIndex] += 1;
          listData['Jumlah pasien yang dites Sifilis'][26] += 1;
        }
        // end Jumlah pasien yang dites Sifilis -------------------------------------------------------------------

        // Jumlah pasien yang positif Sifilis -------------------------------------------------------------------
        if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF")) {
          listData['Jumlah pasien yang positif Sifilis'][i] += 1;
          listData['Jumlah pasien yang positif Sifilis'][totalIndex] += 1;
          listData['Jumlah pasien yang positif Sifilis'][26] += 1;
        }
        // end Jumlah pasien yang positif Sifilis -------------------------------------------------------------------

        // Jumlah pasien sifilis yang diobati -------------------------------------------------------------------
        if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.date_given_medicine != null) {
          listData['Jumlah pasien sifilis yang diobati'][i] += 1;
          listData['Jumlah pasien sifilis yang diobati'][totalIndex] += 1;
          listData['Jumlah pasien sifilis yang diobati'][26] += 1;
        }
        // end Jumlah pasien sifilis yang diobati -------------------------------------------------------------------

        // Jumlah pasien Sifilis yang diperiksa IVA Test -------------------------------------------------------------------
        if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.is_iva_tested) {
          listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][i] += 1;
          listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][totalIndex] += 1;
          listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][26] += 1;
        }
        // end Jumlah pasien Sifilis yang diperiksa IVA Test -------------------------------------------------------------------

        // Jumlah pasien IMS yang ditemukan -------------------------------------------------------------
        let imsIsFound = false;
        const sindromik = ["dtu", "prp", "ug", "pembengkakanSkrotum", "bi"];
        imsIsFound = _insertImsFoundData(listData, r, i, totalIndex, sindromik, 'diagnosis_syndrome', imsIsFound);
        if (!imsIsFound) {
          const klinis = ["herpes", "sifilis", "sifilisSekunder", "chancroid", "kutilAnogenital", "uretritis"];
          imsIsFound = _insertImsFoundData(listData, r, i, totalIndex, klinis, 'diagnosis_klinis', imsIsFound);
        }
        if (!imsIsFound) {
          const laboratorium = ["servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore", "sifilisDini", "sifilisLaten ", "sifilisKongenital", "trikomoniasis", "limfogranulomaVenereum"];
          imsIsFound = _insertImsFoundData(listData, r, i, totalIndex, laboratorium, 'diagnosis_lab', imsIsFound);
        }
        // end Jumlah pasien IMS yang ditemukan -------------------------------------------------------------
        // end by umur dan jenis kelamin ------------------------

        // DIAGNOSIS -----------------------------------------------------------------------------
        _insertDiagnosisData(listData, r, i, totalIndex, DIAGNOSIS_SYNDROME, 'diagnosis_syndrome');
        _insertDiagnosisData(listData, r, i, totalIndex, DIAGNOSIS_KLINIS, 'diagnosis_klinis');
        _insertDiagnosisData(listData, r, i, totalIndex, DIAGNOSIS_LAB, 'diagnosis_lab');
        // end DIAGNOSIS -----------------------------------------------------------------------------

        // by kelompok populasi ------------------------
        r.kelompok_populasi.map(v => {
          if (KEL_POPULASI[v]) {
            listData['Jumlah yang berkunjung'][KEL_POPULASI[v]] += 1;
            listData['Jumlah kondom yang diberikan'][KEL_POPULASI[v]] += r.condom;
            if (r.kesimpulan_hiv == "REAKTIF") {
              listData['Jumlah pasien IMS yang tes HIV'][KEL_POPULASI[v]] += 1;
            }
            if (["SPIRITIA", "IAC", "UNFPA", "IPPI", "YPI", "PKVHI", "PKBI"].includes(r.lsm_penjangkau)) {
              listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][KEL_POPULASI[v]] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan)) {
              listData['Jumlah pasien yang dites Sifilis'][KEL_POPULASI[v]] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF")) {
              listData['Jumlah pasien yang positif Sifilis'][KEL_POPULASI[v]] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.date_given_medicine != null) {
              listData['Jumlah pasien sifilis yang diobati'][KEL_POPULASI[v]] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.is_iva_tested) {
              listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][KEL_POPULASI[v]] += 1;
            }
          }
        });

        if (r.status_patient == "ODHA") {
          listData['Jumlah yang berkunjung'][32] += 1;
          listData['Jumlah kondom yang diberikan'][32] += r.condom;
          if (r.kesimpulan_hiv == "REAKTIF") {
            listData['Jumlah pasien IMS yang tes HIV'][32] += 1;
          }
          if (["SPIRITIA", "IAC", "UNFPA", "IPPI", "YPI", "PKVHI", "PKBI"].includes(r.lsm_penjangkau)) {
            listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][32] += 1;
          }
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan)) {
            listData['Jumlah pasien yang dites Sifilis'][32] += 1;
          }
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF")) {
            listData['Jumlah pasien yang positif Sifilis'][32] += 1;
          }
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.date_given_medicine != null) {
            listData['Jumlah pasien sifilis yang diobati'][32] += 1;
          }
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.is_iva_tested) {
            listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][32] += 1;
          }
        }
        // end by kelompok populasi ------------------------
      }

      for (let i = 0; i <= 37; i++) {
        if (listData["Jumlah yang berkunjung"][i] == undefined) {
          listData["Jumlah yang berkunjung"][i] = 0;
          listData['Jumlah pasien IMS yang ditemukan'][i] = 0;
          listData['Jumlah kondom yang diberikan'][i] = 0;
          listData['Jumlah pasien IMS yang tes HIV'][i] = 0;
          listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][i] = 0;
          listData['Jumlah pasien yang dites Sifilis'][i] = 0;
          listData['Jumlah pasien yang positif Sifilis'][i] = 0;
          listData['Jumlah pasien sifilis yang diobati'][i] = 0;
          listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][i] = 0;
          listData['Jumlah pasien yang positif Gonore'][i] = 0;
          listData['Jumlah pasien gonore yang diobati'][i] = 0;
          Object.keys(DIAGNOSIS_SYNDROME).map(key => {
            listData[key][i] = 0;
          });
          Object.keys(DIAGNOSIS_KLINIS).map(key => {
            listData[key][i] = 0;
          })
          Object.keys(DIAGNOSIS_LAB).map(key => {
            listData[key][i] = 0;
          });
        }
      }

      data.map(r => {
        if (r.age < 1) {
          _insertData(listData, r, 0, 13);
        } else if (r.age >= 1 && r.age <= 4) {
          _insertData(listData, r, 1, 14);
        } else if (r.age >= 5 && r.age <= 14) {
          _insertData(listData, r, 2, 15);
        } else if (r.age >= 15 && r.age <= 19) {
          _insertData(listData, r, 3, 16);
        } else if (r.age >= 20 && r.age <= 24) {
          _insertData(listData, r, 4, 17);
        } else if (r.age >= 25 && r.age <= 29) {
          _insertData(listData, r, 5, 18);
        } else if (r.age >= 30 && r.age <= 34) {
          _insertData(listData, r, 6, 19);
        } else if (r.age >= 35 && r.age <= 39) {
          _insertData(listData, r, 7, 20);
        } else if (r.age >= 40 && r.age <= 44) {
          _insertData(listData, r, 8, 21);
        } else if (r.age >= 45 && r.age <= 49) {
          _insertData(listData, r, 9, 22);
        } else if (r.age >= 50 && r.age <= 59) {
          _insertData(listData, r, 10, 23);
        } else if (r.age >= 60) {
          _insertData(listData, r, 11, 24);
        }
      });

      let result = [];
      result.push(["Jumlah yang berkunjung", ...listData["Jumlah yang berkunjung"]]);
      result.push(["Jumlah pasien IMS yang ditemukan", ...listData["Jumlah pasien IMS yang ditemukan"]]);
      result.push(["Jumlah kondom yang diberikan", ...listData["Jumlah kondom yang diberikan"]]);
      result.push(["Jumlah pasien IMS yang tes HIV", ...listData["Jumlah pasien IMS yang tes HIV"]]);
      result.push(["Jumlah pasien yang berkunjung yang dirujuk oleh LSM", ...listData["Jumlah pasien yang berkunjung yang dirujuk oleh LSM"]]);
      result.push(["Sindromik"]);
      Object.keys(DIAGNOSIS_SYNDROME).map(key => {
        result.push([DIAGNOSIS_SYNDROME[key], ...listData[key]]);
      });
      result.push(["Klinis"]);
      Object.keys(DIAGNOSIS_KLINIS).map(key => {
        result.push([DIAGNOSIS_KLINIS[key], ...listData[key]]);
      })
      result.push(["Laboratorium"]);
      result.push(["Gonore"]);
      Object.keys(DIAGNOSIS_LAB).map(key => {
        result.push([DIAGNOSIS_LAB[key], ...listData[key]]);
        if (DIAGNOSIS_LAB[key] == "Konjungtivitis gonore") {
          result.push(["Non Gonore"]);
        }
      })
      result.push(["Sifilis"]);
      result.push(["Jumlah pasien yang dites Sifilis", ...listData["Jumlah pasien yang dites Sifilis"]]);
      result.push(["Jumlah pasien yang positif Sifilis", ...listData["Jumlah pasien yang positif Sifilis"]]);
      result.push(["Jumlah pasien sifilis yang diobati", ...listData["Jumlah pasien sifilis yang diobati"]]);
      result.push(["Jumlah pasien Sifilis yang diperiksa IVA Test", ...listData["Jumlah pasien Sifilis yang diperiksa IVA Test"]]);
      result.push(["Gonore"]);
      result.push(["Jumlah pasien yang positif Gonore", ...listData["Jumlah pasien yang positif Gonore"]]);
      result.push(["Jumlah pasien gonore yang diobati", ...listData["Jumlah pasien gonore yang diobati"]]);

      if (format == 'excel') {
        const titlesChild = [
          "<1",
          "1-4",
          "5-14",
          "15-19",
          "20-24",
          "25-29",
          "30-34",
          "35-39",
          "40-44",
          "45-49",
          "50-59",
          ">=60",
          "Jumlah",
        ]
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA"];
        const sheetObj = {
          "Laporan 1": {
            "!ref": "A1:AA35",
            A1: { t: 's', v: "Indikator" }, // t => type s => string, n => number, b => boolean
            B1: { t: 's', v: "Perempuan" },
            O1: { t: 's', v: "Laki-laki" },
            "!cols": [
              { wch: 30 }
            ],
            "!merges": [
              { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
              { s: { r: 0, c: 1 }, e: { r: 0, c: 13 } }, /* r => row ke berapa, c => column ke berapa */
              { s: { r: 0, c: 14 }, e: { r: 0, c: 26 } },
            ]
          }
        };
        for (let i = 0; i < 2; i++) {
          _.forEach(indexCols, function (item, i) {
            if (i > 0) {
              sheetObj["Laporan 1"][`${item}2`] = {
                t: 's',
                v: titlesChild[(i - 1) % titlesChild.length]
              }
            }
          });
        }
        _.forEach(result, function (val, index) {
          let hideLaki = false;
          let hidePerempuan = false;
          _.forEach(val, function (item, i) {
            if (["Duh Tubuh Vagina", "Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore"].includes(item)) {
              hideLaki = true;
            }
            if (["Pembengkakan Skortum"].includes(item)) {
              hidePerempuan = true;
            }
            sheetObj["Laporan 1"][`${indexCols[i]}${index + 3}`] = {
              t: 's',
              v: item ? item : (hideLaki ? (i >= 14 && i <= 26 ? '-' : item) : (hidePerempuan ? (i >= 1 && i <= 13 ? '-' : item) : '0'))
            }
          });
        });
        XLSX.writeFile({
          SheetNames: ["Laporan 1"],
          Sheets: sheetObj
        }, './api/excel/Laporan1.xlsx');
        // return res.download('./api/excel/Laporan1.xlsx')
        return res.send({
          status: 200,
          message: "Success",
          data: "/api/excel/Laporan1.xlsx"
        });
      } else if (format == 'pdf') {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('https://facebook.com/', {
          waitUntil: 'load',
        });
        await page.pdf({
          path: './api/pdf/Laporan1.pdf',
          format: 'letter',
          scale: 0.7,
          printBackground: true,
          landscape: true,
          // width: 1024,
          // height: 768
        });
        await browser.close();
        return res.send({
          status: 200,
          message: "Success",
        });
      }

      return res.send({
        status: 200,
        message: "Success",
        data: {
          data: result
        }
      });
    } catch (err) {
      next(err);
    }
  }

  const reportByAllNew = async (req, res, next) => {
    try {
      const { tgl, province, sudin, upk, format } = req.query;
      const roleId = req.user.role;
      const userId = req.user.id;
      let
        where = [
          `ims_visits.tes_lab = TRUE`
        ],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( visits.visit_date, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (province) {
        where.push(` provinces.ID = ${province} `);
      }
      if (sudin) {
        where.push(` sudin_kab_kota.ID = ${sudin} `);
      }
      if (upk) {
        where.push(` upk.ID = ${upk} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          ims_visit_labs.*,
          pv.diagnosis_syndrome AS prev_diagnosis_syndrome,
          pv.diagnosis_klinis AS prev_diagnosis_klinis,
          pv.diagnosis_lab AS prev_diagnosis_lab 
        FROM
          (
          SELECT
            visits.ID AS visit_id,
            LAG ( visits.ID ) OVER ( PARTITION BY visits.patient_id ORDER BY visits.ID ) AS prev_visit_id,
            upk.ID AS upk,
            sudin_kab_kota.ID AS sudin_kab_kota,
            provinces.ID AS province,
            patients.nik,
            patients.fullname,
            patients.gender,
            EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
            patients.kelompok_populasi,
            patients.status_patient,
            patients.lsm_penjangkau,
            ims_visit_labs.diagnosis_syndrome,
            ims_visit_labs.diagnosis_klinis,
            ims_visit_labs.diagnosis_lab,
            ims_visit_labs.pemeriksaan,
            ims_visit_labs.tpha_tppa,
            ims_visit_labs.rpr_vdrl,
            visits.visit_date,
            ims_prescriptions.date_given_medicine,
            ims_prescriptions.condom,
            test_hiv.kesimpulan_hiv,
            ims_visits.tes_lab,
            ims_visits.is_iva_tested,
            ims_visits.reason_visit 
          FROM
            ims_visit_labs
            JOIN visits ON visits.ID = ims_visit_labs.visit_id
            JOIN ims_visits ON ims_visits.visit_id = ims_visit_labs.visit_id
            LEFT JOIN ims_prescriptions ON ims_prescriptions.visit_id = ims_visit_labs.visit_id
            LEFT JOIN test_hiv ON test_hiv.visit_id = ims_visit_labs.visit_id
            JOIN patients ON patients.ID = visits.patient_id
            JOIN upk ON upk.ID = patients.upk_id
            JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
            JOIN provinces ON provinces.ID = sudin_kab_kota.province_id 
          ${newWhere}
          ) ims_visit_labs
          LEFT JOIN ims_visit_labs pv ON pv.visit_id = ims_visit_labs.prev_visit_id
      `;

      // console.log(sql);
      // SELECT
      //   ims_visit_labs.*,
      //   pv.diagnosis_syndrome AS prev_diagnosis_syndrome,
      //   pv.diagnosis_klinis AS prev_diagnosis_klinis,
      //   pv.diagnosis_lab AS prev_diagnosis_lab 
      // FROM
      //   (
      //   SELECT
      //     visits.ID AS visit_id,
      //     LAG ( visits.ID ) OVER ( PARTITION BY visits.patient_id ORDER BY visits.ID ) AS prev_visit_id,
      //     upk.ID AS upk,
      //     sudin_kab_kota.ID AS sudin_kab_kota,
      //     provinces.ID AS province,
      //     patients.nik,
      //     patients.fullname,
      //     patients.gender,
      //     EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
      //     patients.kelompok_populasi,
      //     patients.status_patient,
      //     patients.lsm_penjangkau,
      //     ims_visit_labs.diagnosis_syndrome,
      //     ims_visit_labs.diagnosis_klinis,
      //     ims_visit_labs.diagnosis_lab,
      //     ims_visit_labs.pemeriksaan,
      //     ims_visit_labs.tpha_tppa,
      //     ims_visit_labs.rpr_vdrl,
      //     visits.visit_date,
      //     ip.date_given_medicine,
      //     ip.condom,
      //     ARRAY (.........
      //     SELECT
      //       concat ( 'medicine : ', mc.NAME) 
      //     FROM
      //       ims_prescription_medicines pm
      //       JOIN medicines mc ON mc.ID = pm.medicine_id 
      //     WHERE
      //       pm.prescription_id = ip.ID 
      //     ) AS medicines,...............
      //     test_hiv.kesimpulan_hiv,
      //     ims_visits.tes_lab,
      //     ims_visits.is_iva_tested,
      //     ims_visits.reason_visit 
      //   FROM
      //     ims_visit_labs
      //     JOIN visits ON visits.ID = ims_visit_labs.visit_id
      //     JOIN ims_visits ON ims_visits.visit_id = ims_visit_labs.visit_id
      //     LEFT JOIN ims_prescriptions ip ON ip.visit_id = ims_visit_labs.visit_id
      //     LEFT JOIN test_hiv ON test_hiv.visit_id = ims_visit_labs.visit_id
      //     JOIN patients ON patients.ID = visits.patient_id
      //     JOIN upk ON upk.ID = patients.upk_id
      //     JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
      //     JOIN provinces ON provinces.ID = sudin_kab_kota.province_id 
      //   WHERE
      //     ims_visits.tes_lab = TRUE 
      //     AND to_char( visits.visit_date, 'YYYY-MM' ) = '2021-05' 
      //   ) ims_visit_labs
      //   LEFT JOIN ims_visit_labs pv ON pv.visit_id = ims_visit_labs.prev_visit_id

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      const DIAGNOSIS_SYNDROME = {
        "dtv": "Duh Tubuh Vagina",
        "dtu": "Duh Tubuh Uretra",
        "dta": "Duh Tubuh Anus",
        "dtm": "Duh Tubuh Mata",
        "prp": "Penyakit Radang Panggul",
        "ug": "Ulkus genital",
        "pembengkakanSkrotum": "Pembengkakan Skortum",
        "bi": "Bubo inguinal"
      };
      const DIAGNOSIS_KLINIS = {
        "herpes": "Herpes genitalis",
        "sifilis": "Sifilis primer",
        "sifilisSekunder": "Sifilis sekunder",
        "chancroid": "Chancroid",
        "kutilAnogenital": "Kutil anogenital",
        "uretritis": "Uretritis gonore",
        "moluskum": "Moluskum",
        "kontagiosum": "Kontagiosum"
      };
      const DIAGNOSIS_LAB = {
        "servisitisGonore": "Servisitis gonore",
        "VaginitisGonore": "Vaginitis gonore",
        "uritritisGonore": "Uritritis gonore",
        "proktitisGonore": "Proktitis gonore",
        "konjungtivitisGonore": "Konjungtivitis gonore",
        // ---------------------------------------
        "ServisitisNonGonore": "Servisitis non gonore",
        "VaginitisNonGonore": "Vaginitis non gonore",
        "uritritisNonGonore": "Uritritis non gonore",
        "proktitisNonGonore": "Proktitis non gonore",
        "konjungtivitisNonGonore": "Konjungtivitis non gonore",
        "sifilisDini": "Sifilis dini",
        "sifilisKongenital": "Sifilis kongenital",
        // "sifilisLaten ": "Sifilis laten",
        "sifilisLaten ": "Sifilis lanjut",
        "trikomoniasis": "Trikomoniasis",
        "vaginosisBakterial": "Vaginosis bakterial",
        "kandidiasis": "Kandidiasis",
        "limfogranulomaVenereum": "Limfogranuloma venereum",
      };
      const KEL_POPULASI = {
        "WPS": 27,
        "LSL": 28,
        "Waria": 29,
        "Penasun": 30,
        "Bumil": 31,
        "Pasien Hepatitis": 33,
        "Pasangan ODHA": 34,
        "Anak ODHA": 35,
        "WBP": 36,
        "Populasi Umum": 37
      };

      let listData = {
        "Jumlah yang berkunjung": [],
        "Jumlah kasus baru IMS yang ditemukan": [],
        "Jumlah pasien IMS yang ditemukan": [],
        "Jumlah kondom yang diberikan": [],
        "Jumlah pasien IMS yang tes HIV": [],
        "Jumlah pasien yang berkunjung yang dirujuk oleh LSM": [],
        // SYNDROME -----------------------------------------------
        "dtv": [],
        "dtu": [],
        "dta": [],
        "dtm": [],
        "prp": [],
        "ug": [],
        "pembengkakanSkrotum": [],
        "bi": [],
        // KLINIS -------------------------------------------------
        "herpes": [],
        "sifilis": [],
        "sifilisSekunder": [],
        "chancroid": [],
        "kutilAnogenital": [],
        "uretritis": [],
        "moluskum": [],
        "kontagiosum": [],
        // LAB -----------------------------------------------------
        "servisitisGonore": [],
        "ServisitisNonGonore": [],
        "VaginitisGonore": [],
        "VaginitisNonGonore": [],
        // ---------
        "uritritisGonore": [],
        "uritritisNonGonore": [],
        "proktitisGonore": [],
        "proktitisNonGonore": [],
        "konjungtivitisGonore": [],
        "konjungtivitisNonGonore": [],
        "sifilisDini": [],
        "sifilisLaten ": [],
        "sifilisKongenital": [],
        "trikomoniasis": [],
        "vaginosisBakterial": [],
        "kandidiasis": [],
        "limfogranulomaVenereum": [],
        // SIFILIS -------------------------------------------------------------
        "Jumlah pasien yang dites Sifilis": [],
        "Jumlah pasien yang positif Sifilis": [],
        "Jumlah pasien sifilis yang diobati": [],
        "Jumlah pasien Sifilis yang diperiksa IVA Test": [],
        "Jumlah pasien yang positif Gonore": [],
        "Jumlah pasien gonore yang diobati": []
      };

      const _insertImsFoundData = (listData, r, i, totalIndex, item, itemName, imsIsFound) => {
        item.map(v => {
          if (!imsIsFound) {
            if (r[itemName][v] == true) {
              listData['Jumlah pasien IMS yang ditemukan'][26] += 1;
              listData['Jumlah pasien IMS yang ditemukan'][i] += 1;
              listData['Jumlah pasien IMS yang ditemukan'][totalIndex] += 1;
              imsIsFound = true;

              // by kelompok populasi ------------------------
              r.kelompok_populasi.map(v => {
                if (KEL_POPULASI[v]) {
                  listData['Jumlah pasien IMS yang ditemukan'][KEL_POPULASI[v]] += 1;
                }
              });

              if (r.status_patient == "ODHA") {
                listData['Jumlah pasien IMS yang ditemukan'][32] += 1;
              }
              // end by kelompok populasi ------------------------
            }
          }
        });

        return imsIsFound;
      }

      const _insertDiagnosisData = (listData, r, i, totalIndex, item, itemName) => {
        Object.keys(item).map(key => {
          if (r[itemName][key] == true) {
            // by umur dan jenis kelamin ------------------------
            listData[key][i] += 1;
            listData[key][totalIndex] += 1;
            listData[key][26] += 1;

            // Jumlah pasien yang positif Gonore ----------------------------------------------------------------------
            if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key)) {
              listData['Jumlah pasien yang positif Gonore'][i] += 1;
              listData['Jumlah pasien yang positif Gonore'][totalIndex] += 1;
              listData['Jumlah pasien yang positif Gonore'][26] += 1;
            }
            // end Jumlah pasien yang positif Gonore ----------------------------------------------------------------------

            // Jumlah pasien gonore yang diobati ----------------------------------------------------------------------
            if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key) && r.date_given_medicine != null) {
              listData['Jumlah pasien gonore yang diobati'][i] += 1;
              listData['Jumlah pasien gonore yang diobati'][totalIndex] += 1;
              listData['Jumlah pasien gonore yang diobati'][26] += 1;
            }
            // end Jumlah pasien gonore yang diobati ----------------------------------------------------------------------
            // end by umur dan jenis kelamin ------------------------

            // by kelompok populasi ------------------------
            r.kelompok_populasi.map(v => {
              if (KEL_POPULASI[v]) {
                listData[key][KEL_POPULASI[v]] += 1;
                if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key)) {
                  listData['Jumlah pasien yang positif Gonore'][KEL_POPULASI[v]] += 1;
                }
                if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key) && r.date_given_medicine != null) {
                  listData['Jumlah pasien gonore yang diobati'][KEL_POPULASI[v]] += 1;
                }
              }
            });

            if (r.status_patient == "ODHA") {
              listData[key][32] += 1;
              if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key)) {
                listData['Jumlah pasien yang positif Gonore'][32] += 1;
              }
              if (["uretritis", "servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore"].includes(key) && r.date_given_medicine != null) {
                listData['Jumlah pasien gonore yang diobati'][32] += 1;
              }
            }
            // end by kelompok populasi ------------------------
          }
        });
      }

      // laki 0-12 | perempuan 13-25 | total 26 | kelompok populasi 27-36
      const _insertData = (listData, r, l, p) => {
        let isCountable = true;

        if (r.reason_visit == 'FOLLOWUP') {
          if (JSON.stringify(r.diagnosis_syndrome) == JSON.stringify(r.prev_diagnosis_syndrome) && JSON.stringify(r.diagnosis_klinis) == JSON.stringify(r.prev_diagnosis_klinis) && JSON.stringify(r.diagnosis_lab) == JSON.stringify(r.prev_diagnosis_lab)) {
            isCountable = false;
          }
        }

        let i = null;
        let totalIndex = null;
        if (r.gender == ENUM.GENDER.PEREMPUAN) {
          i = p;
          totalIndex = 25;
        } else {
          i = l;
          totalIndex = 12;
        }

        // by kelompok populasi Jumlah yang berkunjung ------------------------
        r.kelompok_populasi.map(v => {
          if (KEL_POPULASI[v]) {
            listData['Jumlah yang berkunjung'][KEL_POPULASI[v]] += 1;
          }
        });

        if (r.status_patient == "ODHA") {
          listData['Jumlah yang berkunjung'][32] += 1;
        }
        // end by kelompok populasi Jumlah yang berkunjung ------------------------

        // by umur dan jenis kelamin ------------------------
        // Jumlah yang berkunjung ----------------------------------------------------------------------
        listData['Jumlah yang berkunjung'][i] += 1;
        listData['Jumlah yang berkunjung'][totalIndex] += 1;
        listData['Jumlah yang berkunjung'][26] += 1;
        // end Jumlah yang berkunjung ----------------------------------------------------------------------
        if (isCountable) {
          // Jumlah kasus baru IMS yang ditemukan ----------------------------------------------------------------------
          listData['Jumlah kasus baru IMS yang ditemukan'][i] += 1;
          listData['Jumlah kasus baru IMS yang ditemukan'][totalIndex] += 1;
          listData['Jumlah kasus baru IMS yang ditemukan'][26] += 1;
          // end Jumlah kasus baru IMS yang ditemukan ----------------------------------------------------------------------
          
          // Jumlah kondom yang diberikan -------------------------------------------------------------------
          listData['Jumlah kondom yang diberikan'][i] += r.condom;
          listData['Jumlah kondom yang diberikan'][totalIndex] += r.condom;
          listData['Jumlah kondom yang diberikan'][26] += r.condom;
          // end Jumlah kondom yang diberikan -------------------------------------------------------------------

          // Jumlah pasien IMS yang tes HIV -------------------------------------------------------------------
          if (r.kesimpulan_hiv == "REAKTIF") {
            listData['Jumlah pasien IMS yang tes HIV'][i] += 1;
            listData['Jumlah pasien IMS yang tes HIV'][totalIndex] += 1;
            listData['Jumlah pasien IMS yang tes HIV'][26] += 1;
          }
          // end Jumlah pasien IMS yang tes HIV -------------------------------------------------------------------

          // Jumlah pasien yang berkunjung yang dirujuk oleh LSM -------------------------------------------------------------------
          if (["SPIRITIA", "IAC", "UNFPA", "IPPI", "YPI", "PKVHI", "PKBI"].includes(r.lsm_penjangkau)) {
            listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][i] += 1;
            listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][totalIndex] += 1;
            listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][26] += 1;
          }
          // end Jumlah pasien yang berkunjung yang dirujuk oleh LSM -------------------------------------------------------------------

          // Jumlah pasien yang dites Sifilis -------------------------------------------------------------------
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan)) {
            listData['Jumlah pasien yang dites Sifilis'][i] += 1;
            listData['Jumlah pasien yang dites Sifilis'][totalIndex] += 1;
            listData['Jumlah pasien yang dites Sifilis'][26] += 1;
          }
          // end Jumlah pasien yang dites Sifilis -------------------------------------------------------------------

          // Jumlah pasien yang positif Sifilis -------------------------------------------------------------------
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF")) {
            listData['Jumlah pasien yang positif Sifilis'][i] += 1;
            listData['Jumlah pasien yang positif Sifilis'][totalIndex] += 1;
            listData['Jumlah pasien yang positif Sifilis'][26] += 1;
          }
          // end Jumlah pasien yang positif Sifilis -------------------------------------------------------------------

          // Jumlah pasien sifilis yang diobati -------------------------------------------------------------------
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.date_given_medicine != null) {
            listData['Jumlah pasien sifilis yang diobati'][i] += 1;
            listData['Jumlah pasien sifilis yang diobati'][totalIndex] += 1;
            listData['Jumlah pasien sifilis yang diobati'][26] += 1;
          }
          // end Jumlah pasien sifilis yang diobati -------------------------------------------------------------------

          // Jumlah pasien Sifilis yang diperiksa IVA Test -------------------------------------------------------------------
          if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.is_iva_tested) {
            listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][i] += 1;
            listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][totalIndex] += 1;
            listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][26] += 1;
          }
          // end Jumlah pasien Sifilis yang diperiksa IVA Test -------------------------------------------------------------------

          // Jumlah pasien IMS yang ditemukan -------------------------------------------------------------
          let imsIsFound = false;
          const sindromik = ["dtu", "prp", "ug", "pembengkakanSkrotum", "bi"];
          imsIsFound = _insertImsFoundData(listData, r, i, totalIndex, sindromik, 'diagnosis_syndrome', imsIsFound);
          if (!imsIsFound) {
            const klinis = ["herpes", "sifilis", "sifilisSekunder", "chancroid", "kutilAnogenital", "uretritis"];
            imsIsFound = _insertImsFoundData(listData, r, i, totalIndex, klinis, 'diagnosis_klinis', imsIsFound);
          }
          if (!imsIsFound) {
            const laboratorium = ["servisitisGonore", "VaginitisGonore", "uritritisGonore", "proktitisGonore", "konjungtivitisGonore", "sifilisDini", "sifilisLaten ", "sifilisKongenital", "trikomoniasis", "limfogranulomaVenereum"];
            imsIsFound = _insertImsFoundData(listData, r, i, totalIndex, laboratorium, 'diagnosis_lab', imsIsFound);
          }
          // end Jumlah pasien IMS yang ditemukan -------------------------------------------------------------
          // end by umur dan jenis kelamin ------------------------

          // DIAGNOSIS -----------------------------------------------------------------------------
          _insertDiagnosisData(listData, r, i, totalIndex, DIAGNOSIS_SYNDROME, 'diagnosis_syndrome');
          _insertDiagnosisData(listData, r, i, totalIndex, DIAGNOSIS_KLINIS, 'diagnosis_klinis');
          _insertDiagnosisData(listData, r, i, totalIndex, DIAGNOSIS_LAB, 'diagnosis_lab');
          // end DIAGNOSIS -----------------------------------------------------------------------------

          // by kelompok populasi ------------------------
          r.kelompok_populasi.map(v => {
            if (KEL_POPULASI[v]) {
              listData['Jumlah kasus baru IMS yang ditemukan'][KEL_POPULASI[v]] += 1;
              listData['Jumlah kondom yang diberikan'][KEL_POPULASI[v]] += r.condom;
              if (r.kesimpulan_hiv == "REAKTIF") {
                listData['Jumlah pasien IMS yang tes HIV'][KEL_POPULASI[v]] += 1;
              }
              if (["SPIRITIA", "IAC", "UNFPA", "IPPI", "YPI", "PKVHI", "PKBI"].includes(r.lsm_penjangkau)) {
                listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][KEL_POPULASI[v]] += 1;
              }
              if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan)) {
                listData['Jumlah pasien yang dites Sifilis'][KEL_POPULASI[v]] += 1;
              }
              if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF")) {
                listData['Jumlah pasien yang positif Sifilis'][KEL_POPULASI[v]] += 1;
              }
              if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.date_given_medicine != null) {
                listData['Jumlah pasien sifilis yang diobati'][KEL_POPULASI[v]] += 1;
              }
              if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.is_iva_tested) {
                listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][KEL_POPULASI[v]] += 1;
              }
            }
          });

          if (r.status_patient == "ODHA") {
            listData['Jumlah kasus baru IMS yang ditemukan'][32] += 1;
            listData['Jumlah kondom yang diberikan'][32] += r.condom;
            if (r.kesimpulan_hiv == "REAKTIF") {
              listData['Jumlah pasien IMS yang tes HIV'][32] += 1;
            }
            if (["SPIRITIA", "IAC", "UNFPA", "IPPI", "YPI", "PKVHI", "PKBI"].includes(r.lsm_penjangkau)) {
              listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][32] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan)) {
              listData['Jumlah pasien yang dites Sifilis'][32] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF")) {
              listData['Jumlah pasien yang positif Sifilis'][32] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.date_given_medicine != null) {
              listData['Jumlah pasien sifilis yang diobati'][32] += 1;
            }
            if (["RAPID_SIFILIS", "TPHA"].includes(r.pemeriksaan) && (r.rpr_vdrl.result == "REAKTIF" || r.tpha_tppa.result == "REAKTIF") && r.is_iva_tested) {
              listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][32] += 1;
            }
          }
          // end by kelompok populasi ------------------------
        }
      }

      for (let i = 0; i <= 37; i++) {
        if (listData["Jumlah yang berkunjung"][i] == undefined) {
          listData["Jumlah yang berkunjung"][i] = 0;
          listData["Jumlah kasus baru IMS yang ditemukan"][i] = 0;
          listData['Jumlah pasien IMS yang ditemukan'][i] = 0;
          listData['Jumlah kondom yang diberikan'][i] = 0;
          listData['Jumlah pasien IMS yang tes HIV'][i] = 0;
          listData['Jumlah pasien yang berkunjung yang dirujuk oleh LSM'][i] = 0;
          listData['Jumlah pasien yang dites Sifilis'][i] = 0;
          listData['Jumlah pasien yang positif Sifilis'][i] = 0;
          listData['Jumlah pasien sifilis yang diobati'][i] = 0;
          listData['Jumlah pasien Sifilis yang diperiksa IVA Test'][i] = 0;
          listData['Jumlah pasien yang positif Gonore'][i] = 0;
          listData['Jumlah pasien gonore yang diobati'][i] = 0;
          Object.keys(DIAGNOSIS_SYNDROME).map(key => {
            listData[key][i] = 0;
          });
          Object.keys(DIAGNOSIS_KLINIS).map(key => {
            listData[key][i] = 0;
          })
          Object.keys(DIAGNOSIS_LAB).map(key => {
            listData[key][i] = 0;
          });
        }
      }

      data.map(r => {
        if (r.age < 1) {
          _insertData(listData, r, 0, 13);
        } else if (r.age >= 1 && r.age <= 4) {
          _insertData(listData, r, 1, 14);
        } else if (r.age >= 5 && r.age <= 14) {
          _insertData(listData, r, 2, 15);
        } else if (r.age >= 15 && r.age <= 19) {
          _insertData(listData, r, 3, 16);
        } else if (r.age >= 20 && r.age <= 24) {
          _insertData(listData, r, 4, 17);
        } else if (r.age >= 25 && r.age <= 29) {
          _insertData(listData, r, 5, 18);
        } else if (r.age >= 30 && r.age <= 34) {
          _insertData(listData, r, 6, 19);
        } else if (r.age >= 35 && r.age <= 39) {
          _insertData(listData, r, 7, 20);
        } else if (r.age >= 40 && r.age <= 44) {
          _insertData(listData, r, 8, 21);
        } else if (r.age >= 45 && r.age <= 49) {
          _insertData(listData, r, 9, 22);
        } else if (r.age >= 50 && r.age <= 59) {
          _insertData(listData, r, 10, 23);
        } else if (r.age >= 60) {
          _insertData(listData, r, 11, 24);
        }
      });

      let result = [];
      result.push(["Jumlah yang berkunjung", ...listData["Jumlah yang berkunjung"]]);
      result.push(["Jumlah kasus baru IMS yang ditemukan", ...listData["Jumlah kasus baru IMS yang ditemukan"]]);
      result.push(["Jumlah pasien IMS yang ditemukan", ...listData["Jumlah pasien IMS yang ditemukan"]]);
      result.push(["Jumlah kondom yang diberikan", ...listData["Jumlah kondom yang diberikan"]]);
      result.push(["Jumlah pasien IMS yang tes HIV", ...listData["Jumlah pasien IMS yang tes HIV"]]);
      result.push(["Jumlah pasien yang berkunjung yang dirujuk oleh LSM", ...listData["Jumlah pasien yang berkunjung yang dirujuk oleh LSM"]]);
      result.push(["Sindromik"]);
      Object.keys(DIAGNOSIS_SYNDROME).map(key => {
        result.push([DIAGNOSIS_SYNDROME[key], ...listData[key]]);
      });
      result.push(["Klinis"]);
      Object.keys(DIAGNOSIS_KLINIS).map(key => {
        result.push([DIAGNOSIS_KLINIS[key], ...listData[key]]);
      })
      result.push(["Laboratorium"]);
      result.push(["Gonore"]);
      Object.keys(DIAGNOSIS_LAB).map(key => {
        result.push([DIAGNOSIS_LAB[key], ...listData[key]]);
        if (DIAGNOSIS_LAB[key] == "Konjungtivitis gonore") {
          result.push(["Non Gonore"]);
        }
      })
      result.push(["Sifilis"]);
      result.push(["Jumlah pasien yang dites Sifilis", ...listData["Jumlah pasien yang dites Sifilis"]]);
      result.push(["Jumlah pasien yang positif Sifilis", ...listData["Jumlah pasien yang positif Sifilis"]]);
      result.push(["Jumlah pasien sifilis yang diobati", ...listData["Jumlah pasien sifilis yang diobati"]]);
      result.push(["Jumlah pasien Sifilis yang diperiksa IVA Test", ...listData["Jumlah pasien Sifilis yang diperiksa IVA Test"]]);
      result.push(["Gonore"]);
      result.push(["Jumlah pasien yang positif Gonore", ...listData["Jumlah pasien yang positif Gonore"]]);
      result.push(["Jumlah pasien gonore yang diobati", ...listData["Jumlah pasien gonore yang diobati"]]);

      if (format == 'excel') {
        const titlesChild = [
          "<1",
          "1-4",
          "5-14",
          "15-19",
          "20-24",
          "25-29",
          "30-34",
          "35-39",
          "40-44",
          "45-49",
          "50-59",
          ">=60",
          "Jumlah",
        ]
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA"];
        const sheetObj = {
          "Laporan 1": {
            "!ref": "A1:AA35",
            A1: { t: 's', v: "Indikator" }, // t => type s => string, n => number, b => boolean
            B1: { t: 's', v: "Perempuan" },
            O1: { t: 's', v: "Laki-laki" },
            "!cols": [
              { wch: 30 }
            ],
            "!merges": [
              { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
              { s: { r: 0, c: 1 }, e: { r: 0, c: 13 } }, /* r => row ke berapa, c => column ke berapa */
              { s: { r: 0, c: 14 }, e: { r: 0, c: 26 } },
            ]
          }
        };
        for (let i = 0; i < 2; i++) {
          _.forEach(indexCols, function (item, i) {
            if (i > 0) {
              sheetObj["Laporan 1"][`${item}2`] = {
                t: 's',
                v: titlesChild[(i - 1) % titlesChild.length]
              }
            }
          });
        }
        _.forEach(result, function (val, index) {
          let hideLaki = false;
          let hidePerempuan = false;
          _.forEach(val, function (item, i) {
            if (["Duh Tubuh Vagina", "Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore"].includes(item)) {
              hideLaki = true;
            }
            if (["Pembengkakan Skortum"].includes(item)) {
              hidePerempuan = true;
            }
            sheetObj["Laporan 1"][`${indexCols[i]}${index + 3}`] = {
              t: 's',
              v: item ? item : (hideLaki ? (i >= 14 && i <= 26 ? '-' : item) : (hidePerempuan ? (i >= 1 && i <= 13 ? '-' : item) : '0'))
            }
          });
        });
        XLSX.writeFile({
          SheetNames: ["Laporan 1"],
          Sheets: sheetObj
        }, './api/excel/Laporan1.xlsx');
        // return res.download('./api/excel/Laporan1.xlsx')
        return res.send({
          status: 200,
          message: "Success",
          data: "/api/excel/Laporan1.xlsx"
        });
      } else if (format == 'pdf') {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('https://facebook.com/', {
          waitUntil: 'load',
        });
        await page.pdf({
          path: './api/pdf/Laporan1.pdf',
          format: 'letter',
          scale: 0.7,
          printBackground: true,
          landscape: true,
          // width: 1024,
          // height: 768
        });
        await browser.close();
        return res.send({
          status: 200,
          message: "Success",
        });
      }

      console.log('Report Logs ..........................................................');
      const year = tgl.split('-')[0];
      const month = tgl.split('-')[1];
      let report = await ReportLogs.findOne({
        // logging: console.log,
        where: {
          upkId: upk ? upk : null,
          sudinKabKotaId: sudin ? sudin : null,
          provinceId: province ? province : null,
          report: 'IMS',
          year: year,
          month: month,
          // createdAt: Sequelize.where(Sequelize.literal("to_char(created_at, 'YYYY-MM-DD')"), `${moment().format('YYYY-MM-DD')}`)
        }
      });
      // console.log(report);
      if (report) {
        if (moment(report.createdAt).format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')) {
          report.data = result;
          report.updatedBy = userId;
          await report.save();
        }
      } else {
        report = await ReportLogs.create({
          upkId: upk,
          sudinKabKotaId: sudin,
          provinceId: province,
          report: 'IMS',
          year: year,
          month: month,
          data: result,
          createdBy: userId
        });
      }

      return res.send({
        status: 200,
        message: "Success",
        data: {
          data: result
        },
        lastReport: report.data
      });
    } catch (err) {
      next(err);
    }
  }

  const checkArrayReport = function (arrayFromQuery, text, perempuanData = false, lakiData = false, ttlEmptyData) {
    let co = [], ce = [];
    function pushIntoArray(input, type) {
      if (input.length == 0) {
        for (let i = 0; i < ttlEmptyData; i++) {
          if (type == 'ce') {
            ce.push(perempuanData ? '0' : null);
          } else {
            co.push(lakiData ? '0' : null);
          }
        }
      } else {
        _.forEach(input, function (item, idx) {
          if (type == 'ce') {
            ce[idx] = perempuanData ? item : null;
          } else {
            co[idx] = lakiData ? item : null;
          }
        });
      }
    }
    _.forEach(arrayFromQuery, function (val, index) {
      if (val.gender == 'PEREMPUAN') {
        delete val.gender;
        ce = Object.values(val);
      } else {
        delete val.gender;
        co = Object.values(val);
      }
    });
    pushIntoArray(ce, 'ce');
    pushIntoArray(co, 'co');

    return [text].concat(ce, co);
  };

  const reportByPopulate = async (req, res, next) => {
    try {
      function convertIntoArrayOfValues(array, title, kel = null) {
        const indexCowo = [1, 2, 4, 7], indexCewe = [0, 3, 5, 6];
        const temp = [title];
        _.forEach(array, function (val) {
          _.forEach(val, function (value, idx) {
            if (value) {
              if (kel == 'ce') {
                temp.push(indexCewe.indexOf(Number(idx)) >= 0 ? value : null);
              } else if (kel == 'co') {
                temp.push(indexCowo.indexOf(Number(idx)) >= 0 ? value : null);
              } else {
                temp.push(value);
              }
            } else {
              if (kel == 'ce') {
                temp.push(indexCewe.indexOf(Number(idx)) >= 0 ? "0" : null);
              } else if (kel == 'co') {
                temp.push(indexCowo.indexOf(Number(idx)) >= 0 ? "0" : null);
              } else {
                temp.push("0");
              }
            }
          });
        });
        return temp;
      }
      const { tgl, province, sudin, upk, format } = req.query;
      let tempQuery, temp;
      const saring = [], result = [],
        title = [
          'Indikator',
          'WPS',
          'LSL',
          'Waria',
          'Penasun Perempuan',
          'Penasun Laki-Laki',
          'Ibu Hamil',
          'ODHA Perempuan',
          'ODHA Laki-Laki',
        ];
      const awal = `SELECT
        SUM( CASE WHEN patients.kelompok_populasi @> '{WPS}' THEN 1 ELSE 0 END ) AS "0",
        SUM( CASE WHEN patients.kelompok_populasi @> '{LSL}' THEN 1 ELSE 0 END ) AS "1",
        SUM( CASE WHEN patients.kelompok_populasi @> '{Waria}' THEN 1 ELSE 0 END ) AS "2",
        SUM( CASE WHEN (patients.kelompok_populasi @> '{Penasun}' AND patients.gender = 'PEREMPUAN') THEN 1 ELSE 0 END ) AS "3",
        SUM( CASE WHEN (patients.kelompok_populasi @> '{Penasun}' AND patients.gender = 'LAKI_LAKI') THEN 1 ELSE 0 END ) AS "4",
        SUM( CASE WHEN patients.kelompok_populasi @> '{Bumil}' THEN 1 ELSE 0 END ) AS "5",
        SUM( CASE WHEN (patients.kelompok_populasi @> '{"Pasangan ODHA"}' AND patients.gender = 'PEREMPUAN') THEN 1 ELSE 0 END ) AS "6",
        SUM( CASE WHEN (patients.kelompok_populasi @> '{"Pasangan ODHA"}' AND patients.gender = 'LAKI_LAKI') THEN 1 ELSE 0 END ) AS "7"
      `;
      const joinPatientUpk = `JOIN patients ON visits.patient_id = patients.id
      JOIN upk ON visits.upk_id = upk.id`;
      const tableVisit = `FROM visits ${joinPatientUpk}`;
      const tableLab = `FROM ims_visit_labs
      JOIN visits ON ims_visit_labs.visit_id = visits.id ${joinPatientUpk}`;
      const tableObat = `FROM ims_prescriptions
      JOIN visits ON ims_prescriptions.visit_id = visits.id ${joinPatientUpk}`;
      const tableVisitIms = `FROM ims_visits
      JOIN visits ON ims_visits.visit_id = visits.id ${joinPatientUpk}`;

      // new
      const joinImsVisit = `JOIN ims_visits ON visits.id = ims_visits.visit_id`;
      const tableVisit2 = `FROM visits ${joinPatientUpk} ${joinImsVisit}`;
      // new

      const roleId = req.user.role;
      if (tgl) {
        saring.push(`visits.visit_date::date BETWEEN '${moment(tgl).startOf('month').format('YYYY-MM-DD')}' AND '${moment(tgl).endOf('month').format('YYYY-MM-DD')}'`);
      }
      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        saring.push(`upk.id = ${req.user.upkId}`);
      } else {
        if (upk && ['MINISTRY_STAFF', 'PROVINCE_STAFF', 'SUDIN_STAFF'].indexOf(roleId) >= 0) {
          saring.push(`upk.id = ${upk}`);
        }
      }
      if (roleId == 'SUDIN_STAFF') {
        saring.push(`upk.sudin_kab_kota_id = ${sudin}`);
      } else {
        if (sudin && ['MINISTRY_STAFF', 'PROVINCE_STAFF'].indexOf(roleId) >= 0) {
          saring.push(`upk.sudin_kab_kota_id = ${req.user.sudinKabKotaId}`);
        }
      }
      if (province && roleId == 'MINISTRY_STAFF') {
        saring.push(`upk.province_id = ${province}`);
      }

      tempQuery = `${awal} ${tableVisit2} ${saring.length > 0 ? 'WHERE ' + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Jumlah yang berkunjung"));
      // --------------------------------------------------------------------------------------------------
      // result.push(["Gonore Positif"]);

      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"servisitisGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Servisitis Gonore", "ce"));
      // ||||||||||||
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"ServisitisNonGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Servisitis non gonore", "ce"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"VaginitisGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Vaginitis gonore", "ce"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"VaginitisNonGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Vaginitis non gonore", "ce"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"uritritisGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Uritritis gonore"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"uritritisNonGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Uritritis non gonore"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"proktitisGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Proktitis gonore"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"proktitisNonGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Proktitis non gonore"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"konjungtivitisGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Konjungtivitis gonore"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"konjungtivitisNonGonore": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Konjungtivitis non gonore"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"sifilisDini": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Sifilis dini"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"sifilisLaten": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Sifilis laten"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"sifilisKongenital": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Sifilis kongenital"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"trikomoniasis": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Trikomoniasis"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"vaginosisBakterial": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Vaginosis bakterial"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"kandidiasis": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Kandidiasis"));
      //------------------------------------------------------------------------------------------------------------------------
      tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"limfogranulomaVenereum": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      temp = await sequelize.query(tempQuery, {
        type: sequelize.QueryTypes.SELECT
      });
      result.push(convertIntoArrayOfValues(temp, "Limfogranuloma venereum"));

      // // --------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_klinis @> '{"servisitis": true}' AND diagnosis_lab @> '{"gonorea": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Servisitis", "ce"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_klinis @> '{"uretritis": true}' AND diagnosis_lab @> '{"gonorea": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Urethritis", "co"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_klinis @> '{"proktitis": true}' AND diagnosis_lab @> '{"gonorea": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Proctitis", "co"));
      // //--------------------------------------------------------------------------------------------------
      // result.push(["Non Gonore"]);
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_klinis @> '{"servisitis": true}' AND diagnosis_lab @> '{"nonGonorea": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Servisitis", "ce"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_klinis @> '{"uretritis": true}' AND diagnosis_lab @> '{"nonGonorea": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Urethritis", "co"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_klinis @> '{"proktitis": true}' AND diagnosis_lab @> '{"nonGonorea": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Proctitis", "co"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"vaginosisBakteri": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Vaginosis Bakterial", "ce"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"trikomoniasis": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Trikomoniasis", "ce"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"kandidiasis": true}' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Kandidiasis", "ce"));
      // //--------------------------------------------------------------------------------------------------
      // result.push(["Sifilis"]);
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE rpr_vdrl_titer <> 'EMPTY' ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Jumlah yang diperiksa"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE diagnosis_lab @> '{"sifilis": true}' AND diagnosis_klinis @> '{"sifilis": true}'  ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Jumlah yang positif Sifilis"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableLab} WHERE rpr_vdrl_titer IN ('1/8', '1/4', '1/2') ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Jumlah Sifilis Aktif (titer >= 1:8 atau naik 4 kali lipat)"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableObat} ${saring.length > 0 ? "WHERE " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Jumlah yang diobati"));
      // //--------------------------------------------------------------------------------------------------
      // tempQuery = `${awal} ${tableVisitIms} WHERE is_iva_tested = true ${saring.length > 0 ? "AND " + saring.join(" AND ") : ''};`;
      // temp = await sequelize.query(tempQuery, {
      //   type: sequelize.QueryTypes.SELECT
      // });
      // result.push(convertIntoArrayOfValues(temp, "Yang diperiksa IVA Test", "ce"));
      // //--------------------------------------------------------------------------------------------------
      if (format == 'excel') {
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
        const sheetObj = {
          "Laporan 2": {
            "!ref": "A1:I20",
            A1: { t: 's', v: "Indikator" }, // t => type s => string, n => number, b => boolean
            B1: { t: 's', v: "WPS" },
            C1: { t: 's', v: "LSL" },
            D1: { t: 's', v: "Waria" },
            E1: { t: 's', v: "Penasun Perempuan" },
            F1: { t: 's', v: "Penasun Laki-Laki" },
            G1: { t: 's', v: "Ibu Hamil" },
            H1: { t: 's', v: "ODHA Perempuan" },
            I1: { t: 's', v: "ODHA Laki-Laki" },
            "!cols": [
              { wch: 30 }
            ]
          }
        };
        _.forEach(result, function (val, index) {
          _.forEach(val, function (item, i) {
            sheetObj["Laporan 2"][`${indexCols[i]}${index + 2}`] = {
              t: 's',
              v: item ? item : ''
            }
          });
        });
        XLSX.writeFile({
          SheetNames: ["Laporan 2"],
          Sheets: sheetObj
        }, './api/excel/Laporan2.xlsx');
        // return res.download('./api/excel/Laporan2.xlsx');
        return res.send({
          status: 200,
          message: "Success",
          data: "/api/excel/Laporan2.xlsx"
        });
      }
      return res.send({
        status: 200,
        message: "Success",
        data: {
          rowsTitle: title,
          data: result
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const reportByPopulate2 = async (req, res, next) => {
    try {
      const { tgl, province, sudin, upk, format } = req.query;
      const roleId = req.user.role;
      let
        where = [],
        newWhere = ``,
        newTgl = "";

      if (tgl) {
        newTgl = tgl.split('-')[0] + "-" + tgl.split('-')[1];
        where.push(` to_char( visits.visit_date, 'YYYY-MM' ) = '${newTgl}' `);
      }
      if (province) {
        where.push(` provinces.ID = ${province} `);
      }
      if (sudin) {
        where.push(` sudin_kab_kota.ID = ${sudin} `);
      }
      if (upk) {
        where.push(` upk.ID = ${upk} `);
      }

      if (['RR_STAFF', 'PHARMA_STAFF', 'DOCTOR'].indexOf(roleId) >= 0) {
        where.push(` upk.ID = ${req.user.upkId} `);
      }

      where.map(w => {
        if (newWhere === ``) {
          newWhere += ` WHERE ${w} `;
        } else {
          newWhere += ` AND ${w} `;
        }
      });

      let sql = `
        SELECT
          upk.ID AS upk,
          sudin_kab_kota.ID AS sudin_kab_kota,
          provinces.ID AS province,
          patients.nik,
          patients.fullname,
          patients.gender,
          EXTRACT ( YEAR FROM age( patients.date_birth ) ) AS age,
          patients.kelompok_populasi,
          ims_visit_labs.diagnosis_syndrome,
          ims_visit_labs.diagnosis_klinis,
          ims_visit_labs.diagnosis_lab,
          visits.visit_date 
        FROM
          ims_visit_labs
          JOIN visits ON visits.ID = ims_visit_labs.visit_id
          JOIN patients ON patients.ID = visits.patient_id
          JOIN upk ON upk.ID = patients.upk_id
          JOIN sudin_kab_kota ON sudin_kab_kota.ID = upk.sudin_kab_kota_id
          JOIN provinces ON provinces.ID = sudin_kab_kota.province_id
        ${newWhere}
      `;

      let data = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });

      const DIAGNOSIS_LAB = {
        "servisitisGonore": "Servisitis gonore",
        "ServisitisNonGonore": "Servisitis non gonore",
        "VaginitisGonore": "Vaginitis gonore",
        "VaginitisNonGonore": "Vaginitis non gonore",
        // -----------
        "uritritisGonore": "Uritritis gonore",
        "uritritisNonGonore": "Uritritis non gonore",
        "proktitisGonore": "Proktitis gonore",
        "proktitisNonGonore": "Proktitis non gonore",
        "konjungtivitisGonore": "Konjungtivitis gonore",
        "konjungtivitisNonGonore": "Konjungtivitis non gonore",
        "sifilisDini": "Sifilis dini",
        "sifilisLaten ": "Sifilis laten",
        "sifilisKongenital": "Sifilis kongenital",
        "trikomoniasis": "Trikomoniasis",
        "vaginosisBakterial": "Vaginosis bakterial",
        "kandidiasis": "Kandidiasis",
        "limfogranulomaVenereum": "Limfogranuloma venereum",
      };

      let listData = {
        "Jumlah yang berkunjung": [],
        "servisitisGonore": [],
        "ServisitisNonGonore": [],
        "VaginitisGonore": [],
        "VaginitisNonGonore": [],
        // ---------
        "uritritisGonore": [],
        "uritritisNonGonore": [],
        "proktitisGonore": [],
        "proktitisNonGonore": [],
        "konjungtivitisGonore": [],
        "konjungtivitisNonGonore": [],
        "sifilisDini": [],
        "sifilisLaten ": [],
        "sifilisKongenital": [],
        "trikomoniasis": [],
        "vaginosisBakterial": [],
        "kandidiasis": [],
        "limfogranulomaVenereum": []
      };

      const kesi = ["WPS", "LSL", "Waria", "Penasun", "Penasun L", "Bumil", "Pasangan ODHA", "Pasangan ODHA L"];

      const _insertData = (listData, i, r) => {
        listData["Jumlah yang berkunjung"][i] += 1;
        Object.keys(listData).map((key, idx) => {
          if (idx > 0) {
            if (r.diagnosis_lab[key] == true) {
              listData[key][i] += 1;
            }
          }
        });
      }

      data.map(r => {
        for (let i = 0; i <= 7; i++) {
          if (listData["Jumlah yang berkunjung"][i] == undefined) {
            Object.keys(listData).map(key => {
              listData[key][i] = 0;
            });
          }

          if (!["Penasun", "Penasun L", "Pasangan ODHA", "Pasangan ODHA L"].includes(kesi[i])) {
            if (r.kelompok_populasi.includes(kesi[i])) {
              _insertData(listData, i, r);
            }
          } else {
            let kelomsi = "Penasun";
            if (["Pasangan ODHA", "Pasangan ODHA L"].includes(kesi[i])) {
              kelomsi = "Pasangan ODHA";
            }
            if (["Penasun", "Pasangan ODHA"].includes(kesi[i])) {
              if (r.kelompok_populasi.includes(kelomsi) && r.gender == ENUM.GENDER.PEREMPUAN) {
                _insertData(listData, i, r);
              }
            } else {
              if (r.kelompok_populasi.includes(kelomsi) && r.gender == ENUM.GENDER.LAKI_LAKI) {
                _insertData(listData, i, r);
              }
            }
          }
        }
      });

      let result = [];
      result.push(["Jumlah yang berkunjung", ...listData["Jumlah yang berkunjung"]]);
      Object.keys(listData).map((key, idx) => {
        if (idx > 0) {
          result.push([DIAGNOSIS_LAB[key], ...listData[key]]);
        }
      })

      const title = [
        'Indikator',
        'WPS',
        'LSL',
        'Waria',
        'Penasun Perempuan',
        'Penasun Laki-Laki',
        'Ibu Hamil',
        'ODHA Perempuan',
        'ODHA Laki-Laki',
      ];

      if (format == 'excel') {
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
        const sheetObj = {
          "Laporan 2": {
            "!ref": "A1:I20",
            A1: { t: 's', v: "Indikator" }, // t => type s => string, n => number, b => boolean
            B1: { t: 's', v: "WPS" },
            C1: { t: 's', v: "LSL" },
            D1: { t: 's', v: "Waria" },
            E1: { t: 's', v: "Penasun Perempuan" },
            F1: { t: 's', v: "Penasun Laki-Laki" },
            G1: { t: 's', v: "Ibu Hamil" },
            H1: { t: 's', v: "ODHA Perempuan" },
            I1: { t: 's', v: "ODHA Laki-Laki" },
            "!cols": [
              { wch: 30 }
            ]
          }
        };
        _.forEach(result, function (val, index) {
          let hideLaki = false;
          _.forEach(val, function (item, i) {
            if (["Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore"].includes(item)) {
              hideLaki = true;
            }
            sheetObj["Laporan 2"][`${indexCols[i]}${index + 2}`] = {
              t: 's',
              v: item ? item : (hideLaki ? ([2, 3, 5, 8].includes(i) ? '-' : item) : '0')
            }
          });
        });
        XLSX.writeFile({
          SheetNames: ["Laporan 2"],
          Sheets: sheetObj
        }, './api/excel/Laporan2.xlsx');
        // return res.download('./api/excel/Laporan2.xlsx');
        return res.send({
          status: 200,
          message: "Success",
          data: "/api/excel/Laporan2.xlsx"
        });
      }

      return res.send({
        status: 200,
        message: "Success",
        data: {
          rowsTitle: title,
          data: result
        }
      });
    } catch (err) {
      next(err);
    }
  }

  const fetchReagenIms = async (req, res, next) => {
    try {
      const data = await NonArvMedicine.findAll({
        // where: {
        //   isIoIms: true
        // },
        attributes: ["id"],
        include: [
          {
            model: Medicine,
            attributes: ['name', 'stockUnitType', 'category']
          }
        ]
      });
      return res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const fetchReagenImsReal = async (req, res, next) => {
    try {
      const data = await Medicine.findAll({
        where: {
          category: "Reagen IMS"
        },
      });
      return res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  const fetchMedicineIms = async (req, res, next) => {
    try {
      const data = await Medicine.findAll({
        attributes: ['id', 'name', 'medicineType'],
        where: {
          [Op.or]: [
            {
              medicineType: ['ALKES', 'ARV']
            },
            {
              medicineType: 'NON_ARV',
              category: 'IO_IMS'
            }
          ]
        }
      });
      res.send({
        status: 200,
        message: "Success",
        data
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    create,
    deleteVisit,
    getVisitByPatient,
    getVisitDetail,
    getPreviousTreatmentVisitDetail,
    endVisit,
    ikhtisar,
    getIkhtisar,
    sisaObat,
    getSisaObat,
    getVisitListByUpk,
    createIms,
    updateIms,
    getIms,
    fetchReagenIms,
    fetchReagenImsReal,
    createExaminationIms,
    getExaminationIms,
    createLabIms,
    prescriptionIms,
    fetchPrescriptionIms,
    updateDiagnosis,
    fetchImsLab,
    reportByAge,
    reportByPopulate,
    fetchMedicineIms,
    reportByPopulate2,
    reportByAge2,
    reportByAll,
    reportByAllNew,
    endVisitIms
  };
};

module.exports = VisitController;
