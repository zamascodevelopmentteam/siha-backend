const moment = require("moment");
const _ = require("lodash");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const TestHiv = require("../models/TestHiv");
const TestIms = require("../models/TestIms");
const TestVl = require("../models/TestVl");
const Visit = require("../models/Visit");
const Medicine = require("../models/Medicine");
const Patient = require("../models/Patient");
const Upk = require("../models/Upk");
const Profilaksis = require("../models/Profilaksis");
const LabUsageNonpatient = require("../models/LabUsageNonpatient");

const InventoryService = require("../services/inventory.service")();
const VisitService = require("../services/visit.service")();
const PatientService = require("../services/patient.service")();
const ENUM = require("../../config/enum");

const visitController = require("./VisitController")();

const ExaminationController = () => {
  const getHiv = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const visitHiv = await TestHiv.findOne({
        where: {
          visitId: visitId
        },
        include: [
          {
            model: Medicine,
            required: false,
            as: "namaReagenR1Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenR2Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenR3Data",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenNatData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenDnaData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenRnaData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenElisaData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "namaReagenWbData",
            attributes: ["name", "codeName"]
          }
        ]
      });

      res.status(200).json({
        status: 200,
        message: "success",
        data: visitHiv
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getIms = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const visitIms = await TestIms.findOne({
        where: {
          visitId: visitId
        },
        include: [
          {
            model: Medicine,
            required: false,
            as: "namaReagenData",
            attributes: ["name", "codeName"]
          }
        ]
      });

      res.status(200).json({
        status: 200,
        message: "Success",
        data: visitIms
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getVl = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const visitVl = await TestVl.findOne({
        where: {
          visitId: visitId
        },
        include: [
          {
            model: Medicine,
            required: false,
            as: "namaReagenData",
            attributes: ["name", "codeName"]
          }
        ]
      });

      res.status(200).json({
        status: 200,
        message: "Success",
        data: visitVl
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getProfilaksis = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const visitProfilaksis = await Profilaksis.findOne({
        where: {
          visitId: visitId
        },
        include: [
          {
            model: Medicine,
            required: false,
            attributes: ["name", "codeName"]
          }
        ]
      });

      res.status(200).json({
        status: 200,
        message: "Success",
        data: visitProfilaksis
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getExamByVisit = async (req, res, next) => {
    const { visitId } = req.params;
    try {
      const visit = await Visit.findByPk(visitId, {
        include: [
          {
            model: TestHiv
          },
          {
            model: TestIms
          },
          {
            model: TestVl
          }
        ]
      });

      res.status(200).json({
        status: 200,
        message: "Success",
        data: visit
      });
    } catch (err) {
      next(err);
    }
  };

  const getHivHistoryByPatient = async (req, res, next) => {
    const { patientId } = req.params;
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      const results = await Visit.findAndCountAll({
        limit,
        offset: limit * page,
        where: {
          patientId: patientId
        },
        include: [
          {
            model: TestHiv,
            required: true,
            include: [
              {
                model: Medicine,
                required: false,
                as: "namaReagenR1Data",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenR2Data",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenR3Data",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenNatData",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenDnaData",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenRnaData",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenElisaData",
                attributes: ["name", "codeName"]
              },
              {
                model: Medicine,
                required: false,
                as: "namaReagenWbData",
                attributes: ["name", "codeName"]
              }
            ]
          },
          {
            model: Upk,
            required: false,
            attributes: ["name"]
          }
        ],
        order: [
          ["visitDate", "ASC"],
          ["ordinal", "ASC"]
        ]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getImsHistoryByPatient = async (req, res, next) => {
    const { patientId } = req.params;
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      const results = await Visit.findAndCountAll({
        limit,
        offset: limit * page,
        where: {
          patientId: patientId
        },
        include: [
          {
            model: TestIms,
            required: true,
            include: [
              {
                model: Medicine,
                required: false,
                as: "namaReagenData",
                attributes: ["name", "codeName"]
              }
            ]
          },
          {
            model: Upk,
            required: false,
            attributes: ["name"]
          }
        ],
        order: [
          ["visitDate", "ASC"],
          ["ordinal", "ASC"]
        ]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getLabUsageList = async (req, res, next) => {
    let { page, limit, keyword, typeUsage } = req.query;
    const { user } = req;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    let whereObj = {
      upkId: user.upkId
    };

    if (typeUsage && typeUsage !== "ALL") {
      Object.assign(whereObj, {
        testType: typeUsage
      });
    }
    try {
      const results = await LabUsageNonpatient.findAndCountAll({
        limit,
        offset: limit * page,
        where: whereObj,
        include: [
          {
            model: Medicine,
            required: false,
            as: "medicineData",
            attributes: ["name", "codeName"]
          },
          {
            model: Medicine,
            required: false,
            as: "toolData",
            attributes: ["name", "codeName"]
          }
        ],
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getVlHistoryByPatient = async (req, res, next) => {
    const { patientId } = req.params;
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      const results = await Visit.findAndCountAll({
        limit,
        offset: limit * page,
        where: {
          patientId: patientId
        },
        include: [
          {
            model: TestVl,
            required: true,
            include: [
              {
                model: Medicine,
                required: false,
                as: "namaReagenData",
                attributes: ["name", "codeName"]
              }
            ]
          },
          {
            model: Upk,
            required: false,
            attributes: ["name"]
          }
        ],
        order: [
          ["visitDate", "ASC"],
          ["ordinal", "ASC"]
        ]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const getProfilakasisHistoryByPatient = async (req, res, next) => {
    const { patientId } = req.params;
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      const results = await Visit.findAndCountAll({
        limit,
        offset: limit * page,
        where: {
          patientId: patientId
        },
        include: [
          {
            model: Profilaksis,
            required: true,
            include: [
              {
                model: Medicine,
                required: false,
                attributes: ["name", "codeName"]
              }
            ]
          },
          {
            model: Upk,
            required: false,
            attributes: ["name"]
          }
        ],
        order: [
          ["visitDate", "ASC"],
          ["ordinal", "ASC"]
        ]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (err) {
      err.status = 500;
      next(err);
    }
  };

  const updateHiv = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;
    let testHivParams = {
      visitId: visitId,
      lsmPenjangkau: body.lsmPenjangkau,
      kelompokPopulasi: body.kelompokPopulasi,
      alasanTest: body.alasanTest,
      tanggalTest: body.tanggalTest ? body.tanggalTest : undefined,
      jenisTest: body.jenisTest,
      namaReagenR1: body.namaReagenR1 || body.namaReagen1 || null,
      hasilTestR1: body.hasilTestR1 || body.hasilTest1,
      namaReagenR2: body.namaReagenR2 || body.namaReagen2 || null,
      hasilTestR2: body.hasilTestR2 || body.hasilTest2,
      namaReagenR3: body.namaReagenR3 || body.namaReagen3 || null,
      hasilTestR3: body.hasilTestR3 || body.hasilTest3,
      namaReagenNat: body.namaReagenNat || null,
      hasilTestNat: body.hasilTestNat,
      namaReagenDna: body.namaReagenDna || null,
      hasilTestDna: body.hasilTestDna,
      namaReagenRna: body.namaReagenRna || null,
      hasilTestRna: body.hasilTestRna,
      namaReagenElisa: body.namaReagenElisa || null,
      hasilTestElisa: body.hasilTestElisa,
      rujukanLabLanjut: body.rujukanLabLanjut,
      namaReagenWb: body.namaReagenWb || null,
      hasilTestWb: body.hasilTestWb,
      kesimpulanHiv: body.kesimpulanHiv,
      namaReagenR12: body.namaReagenR12 || body.namaReagen12 || null,
      hasilTestR12: body.hasilTestR12 || body.hasilTest12,
      namaReagenR22: body.namaReagenR22 || body.namaReagen22 || null,
      hasilTestR22: body.hasilTestR22 || body.hasilTest22,
      typeReagenR12: body.typeReagenR12 || body.typeReagenR12,
      typeReagenR22: body.typeReagenR22 || body.typeReagenR22,
    };

    if (
      testHivParams.kesimpulanHiv === "" ||
      testHivParams.kesimpulanHiv === "-"
    ) {
      testHivParams.kesimpulanHiv = null;
    }

    if (testHivParams.namaReagenR1) {
      testHivParams.qtyReagenR1 = body.qtyReagenR1 || 1;
    }

    if (testHivParams.namaReagenR2) {
      testHivParams.qtyReagenR2 = body.qtyReagenR2 || 1;
    }

    if (testHivParams.namaReagenR3) {
      testHivParams.qtyReagenR3 = body.qtyReagenR3 || 1;
    }

    if (testHivParams.namaReagenR12) {
      testHivParams.qtyReagenR12 = body.qtyReagenR12 || 1;
    }

    if (testHivParams.namaReagenR22) {
      testHivParams.qtyReagenR22 = body.qtyReagenR22 || 1;
    }

    if (testHivParams.namaReagenNat) {
      testHivParams.qtyReagenNat = body.qtyReagenNat || 1;
    }

    if (testHivParams.namaReagenDna) {
      testHivParams.qtyReagenDna = body.qtyReagenDna || 1;
    }

    if (testHivParams.namaReagenRna) {
      testHivParams.qtyReagenRna = body.qtyReagenRna || 1;
    }

    if (testHivParams.namaReagenElisa) {
      testHivParams.qtyReagenElisa = body.qtyReagenElisa || 1;
    }

    if (testHivParams.namaReagenWb) {
      testHivParams.qtyReagenWb = body.qtyReagenWb || 1;
    }

    try {
      const testHivBefore = await TestHiv.findOne({
        where: {
          visitId: visitId
        }
      });

      let testHivAfter;
      // console.log("testHivParams: ", testHivParams);
      if (testHivBefore) {
        testHivAfter = await TestHiv.update(
          Object.assign(testHivParams, {
            updatedBy: req.user.id
          }),
          {
            where: {
              visitId: visitId
            }
          }
        ).then(() => {
          return TestHiv.findOne({
            where: {
              visitId: visitId
            }
          });
        });
      } else {
        testHivAfter = await TestHiv.create(
          Object.assign(testHivParams, {
            createdBy: req.user.id
          })
        );
        testHivAfter.ordinal = await VisitService.getNextOrdinalByVisitActivity(
          ENUM.VISIT_ACT_TYPE.HIV,
          null,
          visitId
        );
        testHivAfter.save();
      }

      // ----------------------------------------------------------------------------
      // if (testHivParams.hasilTestR1 == "NON_REAKTIF" || (testHivParams.hasilTestR1 == "REAKTIF" && testHivParams.hasilTestR2 == "REAKTIF" && testHivParams.hasilTestR3 == "REAKTIF" && testHivParams.kesimpulanHiv == "REAKTIF") || (testHivParams.hasilTestR12 == "REAKTIF" && testHivParams.hasilTestR22 == "REAKTIF" && testHivParams.hasilTestR3 == "REAKTIF" && testHivParams.kesimpulanHiv == "REAKTIF")) {
      //   const endVisit = await visitController.endVisit(req, res, next, true);
      // }
      if (testHivParams.hasilTestR1 == "NON_REAKTIF") {
        const endVisit = await visitController.endVisit(req, res, next, true);
      }
      // ----------------------------------------------------------------------------

      if (
        (!testHivBefore ||
          testHivBefore.kesimpulanHiv !== ENUM.TEST_RESULT.REAKTIF) &&
        testHivAfter.kesimpulanHiv == ENUM.TEST_RESULT.REAKTIF
      ) {
        const updatedPatient = await Patient.update(
          {
            statusPatient: ENUM.STATUS_PATIENT.HIV_POSITIF
          },
          {
            where: {
              id: req.visit.patientId,
              statusPatient: { [Op.ne]: ENUM.STATUS_PATIENT.ODHA }
            }
          }
        );
        if (updatedPatient) {
          PatientService.updateMetaPatient(req.visit.patientId, {
            tglKonfirmasiHivPos:
              testHivAfter.tanggalTest ||
              moment()
                .local()
                .format()
          });
        }
      } else {
        let statusPatient;
        switch (testHivParams.kesimpulanHiv) {
          case ENUM.TEST_RESULT.REAKTIF:
            statusPatient = ENUM.STATUS_PATIENT.HIV_POSITIF;
            break;
          case ENUM.TEST_RESULT.NON_REAKTIF:
            statusPatient = ENUM.STATUS_PATIENT.HIV_NEGATIF;
            break;
          // case ENUM.TEST_RESULT.INKONKLUSIF:
          //   statusPatient = ENUM.STATUS_PATIENT.HIV_NEGATIF;
          //   break;
          // case ENUM.TEST_RESULT.INVALID:
          //   statusPatient = ENUM.STATUS_PATIENT.BLM_TAHU;
          //   break;
        }
        await Patient.update(
          {
            statusPatient: statusPatient
          },
          {
            where: {
              id: req.visit.patientId,
              statusPatient: { [Op.ne]: ENUM.STATUS_PATIENT.ODHA }
            }
          }
        );
      }

      // affect the overall inventory
      let visitObject = {
        patientId: req.visit.patientId,
        visitId: req.visit.id,
        visitActivityType: ENUM.VISIT_ACT_TYPE.HIV,
        visitActivityId: testHivAfter.id
      };
      // console.log(testHivAfter, '--------------------', testHivBefore);
      if (
        testHivAfter &&
        testHivAfter.namaReagenR1 &&
        testHivAfter.qtyReagenR1 > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenR1))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenR1,
          testHivAfter.qtyReagenR1,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenR2 &&
        testHivAfter.qtyReagenR2 > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenR2))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenR2,
          testHivAfter.qtyReagenR2,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenR3 &&
        testHivAfter.qtyReagenR3 > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenR3))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenR3,
          testHivAfter.qtyReagenR3,
          req.user,
          visitObject
        );
      }
      // new
      if (
        testHivAfter &&
        testHivAfter.namaReagenR12 &&
        testHivAfter.qtyReagenR12 > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenR12))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenR12,
          testHivAfter.qtyReagenR12,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenR22 &&
        testHivAfter.qtyReagenR22 > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenR22))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenR22,
          testHivAfter.qtyReagenR22,
          req.user,
          visitObject
        );
      }
      // end new
      if (
        testHivAfter &&
        testHivAfter.namaReagenNat &&
        testHivAfter.qtyReagenNat > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenNat))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenNat,
          testHivAfter.qtyReagenNat,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenDna &&
        testHivAfter.qtyReagenDna > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenDna))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenDna,
          testHivAfter.qtyReagenDna,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenRna &&
        testHivAfter.qtyReagenRna > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenRna))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenRna,
          testHivAfter.qtyReagenRna,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenElisa &&
        testHivAfter.qtyReagenElisa > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenElisa))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenElisa,
          testHivAfter.qtyReagenElisa,
          req.user,
          visitObject
        );
      }
      if (
        testHivAfter &&
        testHivAfter.namaReagenWb &&
        testHivAfter.qtyReagenWb > 0 &&
        (!testHivBefore || (testHivBefore && !testHivBefore.namaReagenWb))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testHivAfter.namaReagenWb,
          testHivAfter.qtyReagenWb,
          req.user,
          visitObject
        );
      }

      // update visit type tes => treatment (agar bisa lasung perawatan)
      if ((testHivParams.hasilTestR1 == "REAKTIF" && testHivParams.hasilTestR2 == "REAKTIF" && testHivParams.hasilTestR3 == "REAKTIF" && testHivParams.kesimpulanHiv == "REAKTIF") || (testHivParams.hasilTestR12 == "REAKTIF" && testHivParams.hasilTestR22 == "REAKTIF" && testHivParams.hasilTestR3 == "REAKTIF" && testHivParams.kesimpulanHiv == "REAKTIF")) {
        const visit = await Visit.update({
          visitType: ENUM.VISIT_TYPE.TREATMENT
        }, {
          where: {
            id: req.visit.id
          }
        })
      }

      res.status(200).json({
        status: 200,
        message: "Success",
        data: testHivAfter
      });
    } catch (err) {
      next(err);
    }
  };

  const updateIms = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;
    let testParams = {
      visitId: visitId,
      ditestIms: body.ditestIms,
      namaReagen: body.namaReagen,
      hasilTestIms: body.hasilTestIms,
      ditestSifilis: body.ditestSifilis,
      hasilTestSifilis: body.hasilTestSifilis
    };
    try {
      if (!_.values(ENUM.TEST_RESULT).includes(testParams.hasilTestSifilis)) {
        if (
          testParams.hasilTestSifilis === "TRUE" ||
          testParams.hasilTestSifilis === "true"
        ) {
          testParams.hasilTestSifilis = ENUM.TEST_RESULT.REAKTIF;
        } else if (
          testParams.hasilTestSifilis === "FALSE" ||
          testParams.hasilTestSifilis === "false"
        ) {
          testParams.hasilTestSifilis = ENUM.TEST_RESULT.NON_REAKTIF;
        }
      }

      const testImsBefore = await TestIms.findOne({
        where: {
          visitId: visitId
        }
      });

      if (testParams.namaReagen) {
        testParams.qtyReagen = body.qtyReagen || 1;
      }

      let testImsAfter;
      if (testImsBefore) {
        testImsAfter = await TestIms.update(
          Object.assign(testParams, {
            updatedBy: req.user.id
          }),
          {
            where: {
              visitId: visitId
            }
          }
        ).then(() => {
          return TestIms.findOne({
            where: {
              visitId: visitId
            }
          });
        });
      } else {
        testImsAfter = await TestIms.create(
          Object.assign(testParams, {
            createdBy: req.user.id
          })
        );
        testImsAfter.ordinal = await VisitService.getNextOrdinalByVisitActivity(
          ENUM.VISIT_ACT_TYPE.IMS,
          null,
          visitId
        );
        testImsAfter.save();
      }

      // affect the overall inventory
      let visitObject = {
        patientId: req.visit.patientId,
        visitId: req.visit.id,
        visitActivityType: ENUM.VISIT_ACT_TYPE.IMS,
        visitActivityId: testImsAfter.id
      };
      if (
        testImsAfter &&
        testImsAfter.namaReagen &&
        testImsAfter.qtyReagen > 0 &&
        (!testImsBefore || (testImsBefore && !testImsBefore.namaReagen))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testImsAfter.namaReagen,
          testImsAfter.qtyReagen,
          req.user,
          visitObject
        );
      }

      res.status(200).json({
        status: 200,
        message: "Success",
        data: testImsAfter
      });
    } catch (err) {
      next(err);
    }
  };

  const updateVl = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;
    let testParams = {
      visitId: visitId,
      namaReagen: body.namaReagen,
      testVlCd4Type: body.testVlCd4Type,
      hasilTestVlCd4: body.hasilTestVlCd4,
      reason: body.reason || null,
      date: body.date || null,
      reagentType: body.reagentType || null,
    };

    try {
      if (testParams.namaReagen) {
        testParams.qtyReagen = body.qtyReagen || 1;
      }

      if (testParams.hasilTestVlCd4) {
        const hasilTestInNumber = parseInt(testParams.hasilTestVlCd4);
        if (
          isNaN(hasilTestInNumber) &&
          !_.values(ENUM.TEST_RESULT).includes(testParams.hasilTestVlCd4)
        ) {
          throw {
            status: 402,
            message: "invalid_value_for_vlcd4_result"
          };
        }

        if (_.isNumber(hasilTestInNumber)) {
          testParams.hasilTestInNumber = hasilTestInNumber;
        }
      }

      switch (testParams.testVlCd4Type) {
        case ENUM.VL_TEST_TYPE.VL:
          testParams.hasilLabSatuan = ENUM.HASIL_LAB_SATUAN.KOPI;
          if (testParams.hasilTestInNumber) {
            if (testParams.hasilTestInNumber <= 1000) {
              testParams.hasilTestVlCd4 = ENUM.TEST_RESULT.LTE_1000;
            } else if (testParams.hasilTestInNumber > 1000) {
              testParams.hasilTestVlCd4 = ENUM.TEST_RESULT.GT_1000;
            }
          }
          break;
        case ENUM.VL_TEST_TYPE.CD4:
          testParams.hasilLabSatuan = ENUM.HASIL_LAB_SATUAN.SEL;
          if (testParams.hasilTestInNumber) {
            if (testParams.hasilTestInNumber < 200) {
              testParams.hasilTestVlCd4 = ENUM.TEST_RESULT.LT_200;
            } else if (
              testParams.hasilTestInNumber >= 200 &&
              testParams.hasilTestInNumber <= 350
            ) {
              testParams.hasilTestVlCd4 = ENUM.TEST_RESULT.BTW_200_350;
            } else if (testParams.hasilTestInNumber > 350) {
              testParams.hasilTestVlCd4 = ENUM.TEST_RESULT.GT_350;
            }
          }
          break;
        default:
          break;
      }

      const testVlBefore = await TestVl.findOne({
        where: {
          visitId: visitId
        }
      });

      let testVlAfter;
      if (testVlBefore) {
        testVlAfter = await TestVl.update(
          Object.assign(testParams, {
            updatedBy: req.user.id
          }),
          {
            where: {
              visitId: visitId
            }
          }
        ).then(() => {
          return TestVl.findOne({
            where: {
              visitId: visitId
            }
          });
        });
      } else {
        testVlAfter = await TestVl.create(
          Object.assign(testParams, {
            createdBy: req.user.id
          })
        );
        testVlAfter.ordinal = await VisitService.getNextOrdinalByVisitActivity(
          ENUM.VISIT_ACT_TYPE.VL_CD4,
          null,
          visitId
        );
        testVlAfter.save();
      }

      // affect the overall inventory
      // let visitObject = {
      //   patientId: req.visit.patientId,
      //   visitId: req.visit.id,
      //   visitActivityType: ENUM.VISIT_ACT_TYPE.VL_CD4,
      //   visitActivityId: testVlAfter.id
      // };
      // if (
      //   testVlAfter &&
      //   testVlAfter.namaReagen &&
      //   testVlAfter.qtyReagen > 0 &&
      //   (!testVlBefore || (testVlBefore && !testVlBefore.namaReagen))
      // ) {
      //   await InventoryService.updateInventoryUPKByUsage(
      //     testVlAfter.namaReagen,
      //     testVlAfter.qtyReagen,
      //     req.user,
      //     visitObject
      //   );
      // }

      res.status(200).json({
        status: 200,
        message: "Success",
        data: testVlAfter
      });
    } catch (err) {
      next(err);
    }
  };

  const updateProfilaksis = async (req, res, next) => {
    const { visitId } = req.params;
    const { body } = req;
    let testParams = {
      visitId: visitId,
      tglProfilaksis: body.tglProfilaksis,
      statusProfilaksis: body.statusProfilaksis,
      obatDiberikan: body.obatDiberikan,
      qtyObat: body.qtyObat
    };
    try {
      const testProfilaksisBefore = await Profilaksis.findOne({
        where: {
          visitId: visitId
        }
      });

      let testProfilaksisAfter;
      if (testProfilaksisBefore) {
        testProfilaksisAfter = await Profilaksis.update(
          Object.assign(testParams, {
            updatedBy: req.user.id
          }),
          {
            where: {
              visitId: visitId
            }
          }
        ).then(() => {
          return Profilaksis.findOne({
            where: {
              visitId: visitId
            }
          });
        });
      } else {
        testProfilaksisAfter = await Profilaksis.create(
          Object.assign(testParams, {
            createdBy: req.user.id
          })
        );
      }

      let visitObject = {
        patientId: req.visit.patientId,
        visitId: visitId,
        visitActivityType: ENUM.VISIT_ACT_TYPE.PROFILAKSIS,
        visitActivityId: testProfilaksisAfter.id
      };
      if (
        testProfilaksisAfter &&
        testProfilaksisAfter.qtyObat > 0 &&
        (!testProfilaksisBefore ||
          (testProfilaksisBefore && !testProfilaksisBefore.obatDiberikan))
      ) {
        await InventoryService.updateInventoryUPKByUsage(
          testProfilaksisAfter.obatDiberikan,
          testProfilaksisAfter.qtyObat,
          req.user,
          visitObject
        );
      }

      res.status(200).json({
        status: 200,
        message: "Success",
        data: testProfilaksisAfter
      });
    } catch (err) {
      next(err);
    }
  };

  const createLabUsage = async (req, res, next) => {
    const { body, user } = req;
    let labParams = {
      upkId: user.upkId,
      medicineId: body.medicineId,
      toolId: body.toolId,
      isDbs: body.isDbs,
      testType: body.testType,
      dateUsage: body.dateUsage,
      usageStock: body.usageStock,
      usageControl: body.usageControl,
      usageError: body.usageError,
      usageOthers: body.usageOthers,
      usageWasted: body.usageWasted
    };
    try {
      const labUsageNonpatient = await LabUsageNonpatient.create(
        Object.assign(labParams, {
          createdBy: req.user.id
        })
      );

      let visitObject = {
        patientId: null,
        visitId: null,
        visitActivityType: ENUM.VISIT_ACT_TYPE.LAB_USAGE_NONPATIENT,
        visitActivityId: labUsageNonpatient.id
      };
      if (labUsageNonpatient && labUsageNonpatient.usageStock > 0) {
        await InventoryService.updateInventoryUPKByUsage(
          labUsageNonpatient.medicineId,
          labUsageNonpatient.usageStock,
          req.user,
          visitObject
        );
      }

      res.status(200).json({
        status: 200,
        message: "Success",
        data: labUsageNonpatient
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    getHiv,
    getIms,
    getProfilaksis,
    getVl,
    getExamByVisit,
    getHivHistoryByPatient,
    getImsHistoryByPatient,
    getProfilakasisHistoryByPatient,
    getVlHistoryByPatient,
    updateHiv,
    updateIms,
    updateProfilaksis,
    updateVl,
    createLabUsage,
    getLabUsageList
  };
};

module.exports = ExaminationController;
