const Brand = require("../models/Brand");
const Inventory = require("../models/Inventory");
const InventoryLog = require("../models/InventoryLog");
const InventoryAdjustment = require("../models/InventoryAdjustment");
const Medicine = require("../models/Medicine");
const Upk = require("../models/Upk");
const Province = require("../models/Province");
const Receipt = require("../models/Receipt");
const ReceiptItem = require("../models/ReceiptItem");
const Sequelize = require("sequelize");
const SudinKabKota = require("../models/SudinKabKota");
const Op = Sequelize.Op;
const sequelize = require("../../config/database");

const QueryService = require("../services/query.service")();
const OrderService = require("../services/Order.service")();
const LogisticService = require("../services/report/logistic.service")();
const InventoryService = require("../services/inventory.service")();
const ENUM = require("../../config/enum");
const CONSTANT = require("../../config/constant");
const DistributionPlan = require("../models/DistributionPlan");
const InventoryUsage = require("../models/InventoryUsage");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");
const Visit = require("../models/Visit");
const Prescription = require("../models/Prescription");

const ReportLogisticController = () => {
  const getKetersediaanObat = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    const { upkId, sudinKabKotaId, provinceId, month } = req.query;
    const logisticRole = req.user.logisticrole;
    const medicineType = "ARV";
    let filterMonth = month + '-31 23:59:59';
    const now = new Date().getFullYear() + '-' + (new Date().getMonth() + 1 < 10 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1));
    if (month == now) {
      filterMonth = now + '-' + (new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate()) + ' 23:59:59';
    }
    let year2 = month.split('-')[0];
    let month2 = month.split('-')[1];
    try {
      // let ketersediaanList = [];
      // if (!upkId && !sudinKabKotaId && !provinceId) {
      //   throw {
      //     message: "parameter upkId or sudinKabKotaId or provinceId is required"
      //   };
      // }
      // if (upkId) {
      //   ketersediaanList = await LogisticService.getKetersediaanStokUpk(upkId);
      // } else if (sudinKabKotaId) {
      //   ketersediaanList = await LogisticService.getKetersediaanStokSudin(sudinKabKotaId);
      // } else if (provinceId) {
      //   ketersediaanList = await LogisticService.getKetersediaanStokProv(provinceId);
      // }

      // var dataList = [];
      // if (ketersediaanList) {
      //   dataList = ketersediaanList.map(resultItem => {
      //     var dataDetail = resultItem;
      //     if (dataDetail.monthAvailability <= 0) {
      //       dataDetail.statusAvailability = ENUM.STATUS_KETERSEDIAAN.STOCKOUT;
      //     } else if (dataDetail.monthAvailability < dataDetail.orderMultiplier) {
      //       dataDetail.statusAvailability = ENUM.STATUS_KETERSEDIAAN.SHORTAGE;
      //     } else if (dataDetail.monthAvailability === dataDetail.orderMultiplier) {
      //       dataDetail.statusAvailability = ENUM.STATUS_KETERSEDIAAN.NORMAL;
      //     } else {
      //       dataDetail.statusAvailability = ENUM.STATUS_KETERSEDIAAN.OVERSTOCK;
      //     }
      //     return dataDetail;
      //   });
      // }

      let dataList = [];
      // let medicines = await InventoryService.getMedicineStock(
      //   req.user,
      //   medicineType,
      //   null,
      //   req.query,
      //   null,
      //   " medicines.name ASC "
      // );

      let medicines = await Medicine.findAll({
        where: {
          medicineType: medicineType
        },
        include: [
          {
            model: PrescriptionMedicine,
            attributes: ['id'],
            required: false,
            where: {
              [Op.and]: [
                sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('prescriptionMedicines.created_at')), year2),
                sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('prescriptionMedicines.created_at')), month2)
              ],
            },
            include: [
              {
                model: Prescription,
                required: true,
                include: [
                  {
                    model: Visit,
                    required: true,
                    include: [
                      {
                        model: Upk,
                        required: true,
                        where: (upkId ? { id: upkId } : false),
                        include: [
                          {
                            as: 'sudinKabKota',
                            model: SudinKabKota,
                            required: true,
                            where: (sudinKabKotaId ? { id: sudinKabKotaId } : false),
                            include: [
                              {
                                model: Province,
                                required: true,
                                where: (provinceId ? { id: provinceId } : false)
                              }
                            ]
                          }
                        ]
    
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        order: [["name", "ASC"]]
      });

      dataList = medicines.map(r => {
        let rr = { ...r.dataValues };
        rr.stok_layanan = 0;
        rr.stok_kabkota = 0;
        rr.stok_provinsi = 0;
        rr.stok_pusat = 0;
        return rr;
      });

      if (['UPK_ENTITY', 'SUDIN_ENTITY', 'PROVINCE_ENTITY', 'MINISTRY_ENTITY'].includes(logisticRole)) {
        let where = ``;
        if (logisticRole == 'UPK_ENTITY') {
          where = ` WHERE upk_id = ${upkId} AND `;
        } else if (logisticRole == 'SUDIN_ENTITY') {
          where = ` WHERE sudin_kab_kota_id = ${sudinKabKotaId} AND `;
          if (upkId) {
            where += ` upk_id = ${upkId} AND `;
          }
        } else if (logisticRole == 'PROVINCE_ENTITY') {
          where = ` WHERE province_id = ${provinceId} AND `;
          if (upkId) {
            where += ` upk_id = ${upkId} AND `;
          }
          if (sudinKabKotaId) {
            where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
          }
        } else if (logisticRole == 'MINISTRY_ENTITY') {
          where = ` WHERE `;
          if (upkId) {
            where += ` upk_id = ${upkId} AND `;
          }
          if (sudinKabKotaId) {
            where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
          }
          if (provinceId) {
            where += ` province_id = ${provinceId} AND `;
          }
        }

        let stokLayanan = await InventoryService.getMedicineStock(
          {
            logisticrole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            role: ENUM.USER_ROLE.PHARMA_STAFF,
            upkId: upkId,
            sudinKabKotaId: sudinKabKotaId,
            provinceId: provinceId
          },
          medicineType,
          null,
          null,
          ` ${where} logistic_role = 'UPK_ENTITY' AND inventories.created_at < '${filterMonth}' `
        );

        // console.log(stokLayanan);

        stokLayanan.map(r => {
          const index = dataList.findIndex(x => x.id == r.id);
          if (index >= 0) {
            dataList[index].stok_layanan += parseInt(r.stock_qty / r.package_multiplier);
          }
        })
        // -----------------------------------------------------
        let stokFarma = await InventoryService.getMedicineStock(
          {
            logisticrole: ENUM.LOGISTIC_ROLE.PHARMA_ENTITY,
            role: ENUM.USER_ROLE.PHARMA_STAFF,
            upkId: upkId,
            sudinKabKotaId: sudinKabKotaId,
            provinceId: provinceId
          },
          medicineType,
          null,
          null,
          ` ${where} logistic_role = 'PHARMA_ENTITY' AND inventories.created_at < '${filterMonth}' `
        );

        stokFarma.map(r => {
          const index = dataList.findIndex(x => x.id == r.id);
          if (index >= 0) {
            dataList[index].stok_layanan += parseInt(r.stock_qty / r.package_multiplier);
          }
        })
      }

      if (['SUDIN_ENTITY', 'PROVINCE_ENTITY', 'MINISTRY_ENTITY'].includes(logisticRole)) {
        let where = ``;
        if (logisticRole == 'SUDIN_ENTITY') {
          where = `WHERE sudin_kab_kota_id = ${sudinKabKotaId} AND`;
          if (upkId) {
            where += ` upk_id = ${upkId} AND `;
          }
        } else if (logisticRole == 'PROVINCE_ENTITY') {
          where = `WHERE province_id = ${provinceId} AND`;
          if (upkId) {
            where += ` upk_id = ${upkId} AND `;
          }
          if (sudinKabKotaId) {
            where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
          }
        } else if (logisticRole == 'MINISTRY_ENTITY') {
          where = `WHERE`;
          if (upkId) {
            where += ` upk_id = ${upkId} AND `;
          }
          if (sudinKabKotaId) {
            where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
          }
          if (provinceId) {
            where += ` province_id = ${provinceId} AND `;
          }
        }

        let stokKabkota = await InventoryService.getMedicineStock(
          {
            logisticrole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
            role: ENUM.USER_ROLE.SUDIN_STAFF,
            upkId: upkId,
            sudinKabKotaId: sudinKabKotaId,
            provinceId: provinceId
          },
          medicineType,
          null,
          null,
          ` ${where} logistic_role = 'SUDIN_ENTITY' AND inventories.created_at < '${filterMonth}' `
        );

        stokKabkota.map(r => {
          const index = dataList.findIndex(x => x.id == r.id);
          if (index >= 0) {
            dataList[index].stok_kabkota += parseInt(r.stock_qty / r.package_multiplier);
          }
        })
      }

      if (['PROVINCE_ENTITY', 'MINISTRY_ENTITY'].includes(logisticRole)) {
        let where = `WHERE`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
        if (sudinKabKotaId) {
          where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
        }
        if (provinceId) {
          where += ` province_id = ${provinceId} AND `;
        }

        let stokProvinsi = await InventoryService.getMedicineStock(
          {
            logisticrole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
            role: ENUM.USER_ROLE.PROVINCE_STAFF,
            upkId: upkId,
            sudinKabKotaId: sudinKabKotaId,
            provinceId: provinceId
          },
          medicineType,
          null,
          null,
          ` ${where} logistic_role = 'PROVINCE_ENTITY' AND inventories.created_at < '${filterMonth}' `
        );

        stokProvinsi.map(r => {
          const index = dataList.findIndex(x => x.id == r.id);
          if (index >= 0) {
            dataList[index].stok_provinsi += parseInt(r.stock_qty / r.package_multiplier);
          }
        })
      }

      if (['MINISTRY_ENTITY'].includes(logisticRole)) {
        let where = `WHERE`;
        if (upkId) {
          where += ` upk_id = ${upkId} AND `;
        }
        if (sudinKabKotaId) {
          where += ` sudin_kab_kota_id = ${sudinKabKotaId} AND `;
        }
        if (provinceId) {
          where += ` province_id = ${provinceId} AND `;
        }

        let stokPusat = await InventoryService.getMedicineStock(
          req.user,
          medicineType
        );

        stokPusat.map(r => {
          const index = dataList.findIndex(x => x.id == r.id);
          if (index >= 0) {
            dataList[index].stok_pusat += parseInt(r.stock_qty / r.package_multiplier);
          }
        })
      }

      let selectedUpk = {};
      if (upkId) {
        selectedUpk = await Upk.findByPk(upkId);
      }
      

      res.status(200).json({
        status: 200,
        message: "success",
        data: dataList,
        upk: selectedUpk,
        paging: {
          page: Number(page),
          size: dataList.length,
          total: dataList.length
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const getLayananAktif = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    try {
      const entitas = await Upk.findAndCountAll({
        attributes: ["name"],
        where: {
          isActive: true
        },
        include: [
          {
            model: SudinKabKota,
            attributes: ["name"],
            as: "sudinKabKota",
            include: [
              {
                model: Province,
                attributes: ["name"]
              }
            ]
          }
        ],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: entitas.rows,
        paging: {
          page: Number(page),
          size: entitas.rows.length,
          total: entitas.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getLayananDesentralisasi = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    try {
      const entitas = await Upk.findAndCountAll({
        attributes: ["id", ["name", "upkName"]],
        include: [
          {
            model: SudinKabKota,
            attributes: [["name", "sudinKabKotaName"]],
            as: "sudinKabKota",
            where: {
              isActive: false
            },
            include: [
              {
                model: Province,
                attributes: [["name", "provinceName"]]
              }
            ]
          }
        ],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: entitas.rows,
        paging: {
          page: Number(page),
          size: entitas.rows.length,
          total: entitas.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getPenerimaanObat = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    const { month, upkId, sudinKabKotaId, provinceId } = req.query;
    try {
      var queryParams = {};
      var whereReceipt = {};
      if (month) {
        // queryParams = QueryService.filterOneMonth("createdAt", month + '-01');
        queryParams = {
          ...queryParams,
          [Op.and]: [
            sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('receiptItem.created_at')), month.split('-')[0]),
            sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('receiptItem.created_at')), month.split('-')[1])
          ]
        };
      }

      if (upkId) {
        whereReceipt = {
          ...whereReceipt,
          upkId: upkId
        };
      } else if (sudinKabKotaId) {
        whereReceipt = {
          ...whereReceipt,
          sudinKabKotaId: sudinKabKotaId
        };
      } else if (provinceId) {
        whereReceipt = {
          ...whereReceipt,
          provinceId: provinceId
        };
      }

      const receiptItem = await ReceiptItem.findAndCountAll({
        // attributes: [
        //   "receiptItemStatus",
        //   "receivedPkgUnitType",
        //   "receivedPkgQuantity",
        //   "receiptItemStatus",
        //   "batchCode",
        //   "receivedExpiredDate"
        // ],
        where: queryParams,
        include: [
          {
            model: Inventory
          },
          {
            model: Receipt,
            // attributes: ["receiptNumber"],
            where: whereReceipt,
            include: [
              {
                model: DistributionPlan,
              }
            ].concat(QueryService.includeCreated())
          },
          {
            model: Brand,
            attributes: [["name", "brandName"]],
            include: [
              {
                model: Medicine,
                attributes: [
                  ["name", "medicineName"],
                  "codeName",
                  "medicineType"
                ]
              }
            ]
          }
        ],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: receiptItem.rows,
        paging: {
          page: Number(page),
          size: receiptItem.rows.length,
          total: receiptItem.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getPengunaanObat = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    const { month, upkId, sudinKabKotaId, provinceId } = req.query;
    try {
      var queryParams = {};
      var whereReceipt = {};
      if (month) {
        queryParams = {
          ...queryParams,
          [Op.and]: [
            sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('inventoryUsage.created_at')), month.split('-')[0]),
            sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('inventoryUsage.created_at')), month.split('-')[1])
          ]
        };
      }

      if (upkId) {
        queryParams = {
          ...queryParams,
          upkId: upkId
        };
        whereReceipt = {
          ...whereReceipt,
          upkId: upkId
        };
      } else if (sudinKabKotaId) {
        whereReceipt = {
          ...whereReceipt,
          sudinKabKotaId: sudinKabKotaId
        };
      } else if (provinceId) {
        whereReceipt = {
          ...whereReceipt,
          provinceId: provinceId
        };
      }

      const itemUsage = await InventoryUsage.findAndCountAll({
        where: queryParams,
        include: [
          {
            model: Inventory,
            include: [
              {
                model: ReceiptItem,
                include: [
                  {
                    model: Receipt,
                    where: whereReceipt,
                    include: [
                      {
                        model: DistributionPlan,
                      }
                    ].concat(QueryService.includeCreated())
                  },
                  {
                    model: Brand,
                    include: [
                      {
                        model: Medicine
                      }
                    ]
                  },
                ]
              },
            ]
          },
          {
            model: Brand,
            include: [
              {
                model: Medicine
              }
            ]
          }
        ],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.status(200).json({
        status: 200,
        message: "success",
        data: itemUsage.rows,
        paging: {
          page: Number(page),
          size: itemUsage.rows.length,
          total: itemUsage.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getRelokasi = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    const params = {
      keyword: keyword,
      page: page ? page : CONSTANT.DEFAULT_PAGE,
      limit: limit ? limit : CONSTANT.DEFAULT_LIMIT
    };
    try {
      var whereObj = {
        [Op.and]: {
          [Op.or]: [
            {
              [Op.and]: [
                {
                  logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY
                },
                { upkIdFor: { [Op.ne]: null } }
              ]
            },
            {
              [Op.and]: [
                {
                  logisticRole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY
                },
                { sudinKabKotaIdFor: { [Op.ne]: null } }
              ]
            },
            {
              [Op.and]: [
                {
                  logisticRole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY
                },
                { provinceIdFor: { [Op.ne]: null } }
              ]
            },
            {
              [Op.and]: [
                { lastOrderStatus: ENUM.LAST_ORDER_STATUS.PROCESSED },
                { lastOrderStatus: ENUM.LAST_ORDER_STATUS.EXECUTED }
              ]
            }
          ]
        }
      };
      const orderList = await OrderService.getOrderList(whereObj, params);
      res.status(200).json({
        status: 200,
        message: "success",
        data: orderList.rows,
        paging: {
          page: Number(page),
          size: orderList.rows.length,
          total: orderList.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getAdjustment = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    const { monthPeriode, upkId, sudinKabKotaId, provinceId } = req.query;
    try {
      var queryParams = [];
      var whereInventory = [];
      if (monthPeriode) {
        queryParams = QueryService.filterOneMonth("createdAt", monthPeriode);
      }

      if (upkId) {
        whereInventory = {
          upkId: upkId
        };
      } else if (sudinKabKotaId) {
        whereInventory = {
          sudinKabKotaId: sudinKabKotaId
        };
      } else if (provinceId) {
        whereInventory = {
          provinceId: provinceId
        };
      }

      const listAdjustment = await InventoryAdjustment.findAndCountAll({
        where: queryParams,
        include: [
          {
            model: Inventory,
            attributes: ["id"],
            where: whereInventory,
            include: [
              {
                model: Brand,
                attributes: [["name", "brandName"]],
                include: [
                  {
                    model: Medicine,
                    attributes: [["id", "medicineId"], "codeName"]
                  }
                ]
              }
            ]
          }
        ].concat(QueryService.includeCreated()),
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: listAdjustment.rows,
        paging: {
          page: Number(page),
          size: listAdjustment.rows.length,
          total: listAdjustment.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getPergerakanStok = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    const params = req.query;
    try {
      const listInventoryLog = await InventoryService.getPergerakanStok(params);
      res.status(200).json({
        status: 200,
        message: "success",
        data: listInventoryLog,
        paging: {
          page: Number(page),
          size: limit,
          total: listInventoryLog.length
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getJumlahIms = async (req, res, next) => {
    const {
      page,
      limit,
      keyword,
      monthPeriode,
      sudinKabKotaId,
      provinceId
    } = req.query;
    const params = {
      keyword: keyword,
      page: page ? page : CONSTANT.DEFAULT_PAGE,
      limit: limit ? limit : CONSTANT.DEFAULT_LIMIT,
      monthPeriode: monthPeriode,
      sudinKabKotaId: sudinKabKotaId,
      provinceId: provinceId
    };
    try {
      const imsList = await LogisticService.getJumlahIms(params);
      res.status(200).json({
        status: 200,
        message: "success",
        data: imsList,
        paging: {
          page: Number(page),
          size: imsList.length,
          total: imsList.length
        }
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    getKetersediaanObat,
    getLayananAktif,
    getLayananDesentralisasi,
    getPenerimaanObat,
    getPengunaanObat,
    getRelokasi,
    getAdjustment,
    getPergerakanStok,
    getJumlahIms
  };
};

module.exports = ReportLogisticController;
