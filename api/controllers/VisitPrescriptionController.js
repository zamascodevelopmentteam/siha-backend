const moment = require("moment");

const Medicine = require("../models/Medicine");
const Regiment = require("../models/Regiment");
const Prescription = require("../models/Prescription");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");
const RegimentMedicine = require("../models/RegimentMedicine");
const Brand = require("../models/Brand");
const Treatment = require("../models/Treatment");
const Patient = require("../models/Patient");
const Visit = require("../models/Visit");

const ENUM = require("../../config/enum");
const InventoryService = require("../services/inventory.service")();
const PatientService = require("../services/patient.service")();
const VisitService = require("../services/visit.service")();
const ErrorService = require("../services/error.service")();
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const visitController = require("./VisitController")();

const VisitPrescriptionController = () => {
  const getPrescriptions = async (req, res) => {
    const { patientId } = req.params;
    const { page, limit } = req.query;
    try {
      const prescriptions = await Prescription.findAndCountAll({
        where: {
          patient_id: patientId
        },
        include: [PrescriptionMedicine],
        offset: page * limit,
        limit: limit,
        order: [["updated_at", "DESC"]]
      });
      res.send({
        status: 200,
        message: "Success",
        data: prescriptions.rows.map(element => {
          return {
            id: element.id,
            prescriptionNumber: element.prescriptionNumber
          };
        }),
        paging: {
          page: Number(page),
          size: prescriptions.rows.length,
          total: prescriptions.count
        }
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: 500,
        message: error.message,
        data: null
      });
    }
  };

  const getPrescriptionsByVisitId = async (req, res) => {
    const { visitId } = req.params;
    try {
      const prescriptions = await Prescription.findOne({
        where: {
          visitId: visitId
        },
        include: [PrescriptionMedicine, Treatment, Regiment]
      });
      res.send({
        status: 200,
        message: "Success",
        data: prescriptions
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: 500,
        message: error.message,
        data: null
      });
    }
  };

  const getDetailById = async (req, res, next) => {
    const { prescriptionId } = req.params;
    try {
      const prescription = await Prescription.findByPk(prescriptionId, {
        include: [
          {
            model: PrescriptionMedicine,
            include: [Medicine]
          }
        ]
      });
      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  const getLastPrescription = async (req, res, next) => {
    const { patientId } = req.params;
    try {
      const prescription = await Prescription.findOne({
        where: {
          patientId: patientId
        },
        include: [
          {
            model: PrescriptionMedicine,
            include: [
              {
                model: Medicine,
                required: true,
                attributes: ["id", "name", "codeName", "stockUnitType"]
              }
            ]
          }
        ],
        order: [["id", "DESC"]]
      });
      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  const getLastGivenPrescription = async (req, res, next) => {
    const { patientId } = req.params;
    try {
      const prescription = await VisitService.getLastGivenPrescription(
        patientId
      );
      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  const getDetailByVisitId = async (req, res, next) => {
    try {
      const prescription = await Prescription.findOne({
        where: {
          visitId: req.visit.id
        },
        include: [
          {
            model: PrescriptionMedicine,
            include: [Medicine]
          }
        ]
      });
      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  const getStatusPaduanObat = async (req, res, next) => {
    const { visitId } = req.params;
    const visit = req.visit;
    try {
      const treatmentVisit = await Visit.findOne({
        where: {
          id: visitId
        },
        include: [
          {
            model: Treatment,
            required: true
          }
        ]
      });

      const statusPaduan = await VisitService.getStatusPaduanObat(
        treatmentVisit
      );
      res.send({
        status: 200,
        message: "Success",
        data: {
          statusPaduan
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const createPrescription = async (req, res, next) => {
    try {
      let prescription = await Prescription.findOne({
        where: {
          visitId: req.visit.id
        }
      });

      if (prescription) {
        throw {
          status: 422,
          message: "prescription_has_already_been_created"
        };
      }

      prescription = await VisitService.initiatePrescription(
        req.visit,
        req.user,
        true
      );

      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  const updatePrescription = async (req, res, next) => {
    const { body } = req;
    const { visitId } = req.params;
    const medicines = req.medicineList;

    try {
      if (req.visit.checkOutDate) {
        throw {
          status: 400,
          message: "visit_has_ended"
        };
      }

      const treatment = await Treatment.findOne({
        where: {
          visitId: visitId
        }
      });

      let prescription = await Prescription.findOne({
        where: {
          visitId: visitId
        }
      });

      if (prescription && !prescription.isDraft) {
        throw {
          status: 400,
          message: "prescription_has_already_been_given"
        };
      }

      if (!prescription) {
        prescription = await Prescription.create({
          notes: req.body.notes,
          isDraft: true,
          createdBy: req.user.id,
          patientId: req.visit.patientId,
          visitId: req.visit.id
        });
        prescription.prescriptionNumber = req.visit.upkId
          .toString()
          .concat(
            "-",
            req.visit.id,
            ".",
            req.visit.ordinal,
            "#",
            prescription.id
          );
        await prescription.save();
      } else {
        prescription.isDraft = true;
        prescription.updatedBy = req.user.id;
        prescription.notes = req.body.notes;
        await prescription.save();
      }

      //delete all prev presc meds
      await PrescriptionMedicine.destroy({
        where: {
          prescriptionId: prescription.id
        },
        force: true
      });

      //replace with new pres meds
      let prescriptionMedicine = [];
      for (let x = 0; x < medicines.length; x++) {
        const element = medicines[x];

        prescriptionMedicine.push({
          medicineId: element.medicineId,
          amount: element.stockQty,
          notes: element.notes,
          jumlahHari: element.jumlahHari,
          prescriptionId: prescription.id,
          createdBy: req.user.id
        });
      }

      let medicineList = [];
      if (prescriptionMedicine.length > 0) {
        medicineList = await PrescriptionMedicine.bulkCreate(
          prescriptionMedicine,
          {
            returning: true
          }
        );
      }

      let ids = medicines.map(item => item.medicineId);
      let arvIds = [];
      let nonArvMeds = [];

      const meds = await Medicine.findAll({
        where: {
          id: {
            [Op.in]: ids
          }
        }
      });
      meds.map(r => {
        if (r.medicineType == 'ARV') {
          arvIds.push(r.id);
        } else {
          nonArvMeds.push(r);
        }
      });

      let where = [];
      if (arvIds.length > 1) {
        console.log('more than 1 med');
        arvIds.map(r => {
          // where.push(Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `%${r}%`));
          // ( combination_ids LIKE'6+%' OR combination_ids LIKE'%+6+%' OR combination_ids LIKE'%+6' ) 
          where.push({
            [Op.or]: [
              // Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `${r}`),
              Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `${r}+%`),
              Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `%+${r}+%`),
              Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `%+${r}`),
            ]
          });
        });

        where = {
          [Op.and]: where
        };
      } else {
        console.log('1 med');
        // where.push({
        //   combinationIds: `'${arvIds[0]}'`
        // });
        // where = {
        //   combinationIds: `${arvIds[0]}`
        // };
        where = Sequelize.where(
          Sequelize.cast(Sequelize.col('combination_ids'), 'varchar'),
          {[Op.eq]: arvIds[0].toString()}
        ); 
      }

      // console.log(arvIds, 'aaaaaaaaaaaaaaaa');



      const regiment = await Regiment.findOne({
        where: where
      });

      console.log(where, regiment);

      let nonArvStrInfo = '';
      if (nonArvMeds.length) {
        nonArvStrInfo = ' | ';
        nonArvMeds.map(r => {
          nonArvStrInfo += r.codeName;
        });
      }

      if (regiment) {
        prescription.paduanObatIds = regiment.combinationIds;
        prescription.paduanObatStr = regiment.combinationStrInfo + nonArvStrInfo;
      } else {
        let medsToStore = medicines.filter(item => item.medicine.medicineType == 'ARV');
        prescription.paduanObatIds = medsToStore
          .map(item => item.medicineId)
          .join("+");
        prescription.paduanObatStr = medsToStore
          .map(item => item.medicine.codeName)
          .join("+") + nonArvStrInfo;
      }

      // const regiment = await Regiment.findOne({
      //   where: {
      //     combinationIds: prescription.paduanObatIds
      //   }
      // });
      if (regiment) {
        prescription.regimentId = regiment.id;
      } else {
        prescription.regimentId = null;
      }
      await prescription.save();

      if (treatment) {
        treatment.tglPemberianObat =
          req.body.tglPemberianObat ||
          moment()
            .local()
            .format();
        treatment.tglPemberianObatSelesai =
          req.body.tglPemberianObatSelesai ||
          moment()
            .local()
            .format();
        treatment.paduanObatArv = prescription.paduanObatStr;

        treatment.statusPaduan = req.body.statusPaduan || await VisitService.getStatusPaduanObat({
          id: visitId,
          ordinal: req.visit.ordinal,
          patientId: req.visit.patientId,
          treatment
        });

        treatment.prescriptionId = prescription.id;
        await treatment.save();
      }

      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  const givePrescription = async (req, res, next) => {
    const { visitId } = req.params;
    // console.log(req.body)
    try {
      if (req.visit.checkOutDate) {
        throw {
          status: 400,
          message: "visit_has_ended"
        };
      }

      const patient = await Patient.findOne({
        where: {
          id: req.visit.patientId
        }
      });

      const treatment = await Treatment.findOne({
        where: {
          visitId: visitId
        }
      });

      const prescription = await Prescription.findOne({
        where: {
          visitId: visitId
        },
        include: [
          {
            model: PrescriptionMedicine,
            required: true,
            include: [{ model: Medicine, required: false }]
          }
        ]
      });

      if ((patient.statusPatient === ENUM.STATUS_PATIENT.ODHA || patient.statusPatient === ENUM.STATUS_PATIENT.ODHA_LAMA) && !treatment) {
        throw {
          status: 400,
          message: "odha_patient_has_no_treatment_found"
        };
      }

      if (!prescription) {
        throw {
          status: 400,
          message: "prescription_not_found"
        };
      }

      if (prescription) {
        if (!prescription.isDraft) {
          throw {
            status: 400,
            message: "prescription_has_already_been_given"
          };
        } else if (prescription.prescriptionMedicines.length === 0) {
          throw {
            status: 400,
            message: "prescription_does_not_have_any_medicines"
          };
        } else if (prescription.prescriptionMedicines) {
          for (
            let ii = 0;
            ii < prescription.prescriptionMedicines.length;
            ii++
          ) {
            if (
              prescription.prescriptionMedicines[ii].amount < 1 ||
              prescription.prescriptionMedicines[ii].jumlahHari < 1
            ) {
              throw {
                status: 400,
                message: "prescription_med_amount_can_not_be_0"
              };
            }
          }
        }
      }

      if (prescription.paduanObatIds) {
        let ids = prescription.paduanObatIds.split('+');
        let isArv = true;
        const meds = await Medicine.findAll({
          where: {
            id: {
              [Op.in]: ids
            }
          }
        });
        meds.map(r => {
          if (isArv) {
            isArv = r.medicineType == 'ARV'
          }
        });

        let where = [];
        if (ids.length > 1) { 
          ids.map(r => {
            // where.push(Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `%${r}%`));
            where.push({
              [Op.or]: [
                // Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `${r}`),
                Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `${r}+%`),
                Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `%+${r}+%`),
                Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('combination_ids')), 'LIKE', `%+${r}`),
              ]
            });
          });

          where = {
            [Op.and]: where
          };
        } else {
          // where.push({
          //   combinationIds: `'${ids[0]}'`
          // });
          // where = {
          //   combinationIds: `'${ids[0]}'`
          // };
          where = Sequelize.where(
            Sequelize.cast(Sequelize.col('combination_ids'), 'varchar'),
            {[Op.eq]: ids[0].toString()}
          );
        }
          
        const regiment = await Regiment.findOne({
          where: where
        });

        if (regiment) {
          // prescription.regimentId = regiment.id;
        } else {
          if (isArv) {
            let medsToStore = prescription.prescriptionMedicines.filter(item => item.medicine.medicineType == 'ARV');
            const combinationStrInfoRegiment = medsToStore
              .map(item => item.medicine.codeName)
              .join("+");
            let ids = medsToStore
              .map(item => item.medicine.id)
              .join("+");

            const newRegiment = await Regiment.create({
              name: "NONSTANDAR#" + combinationStrInfoRegiment,
              combinationIds: ids,
              combinationStrInfo: combinationStrInfoRegiment,
              createdBy: req.user.id
            });
            for (
              let ii = 0;
              ii < medsToStore.length;
              ii++
            ) {
              await RegimentMedicine.create({
                medicineId: medsToStore[ii].medicine.id,
                regimentId: newRegiment.id,
                createdBy: req.user.id
              });
              // console.log('iteration meds: ',i , ' #', ii, presMed);
            }
            prescription.regimentId = newRegiment.id;
          }
        }
        await prescription.save();
      }

      //update treatment
      if (treatment) {
        treatment.updatedBy = req.user.id;
        treatment.tglKunjungan =
          treatment.tglKunjungan ||
          moment()
            .local()
            .format();

        let theClosestDayCount = Number.MAX_SAFE_INTEGER;
        for (let i = 0; i < prescription.prescriptionMedicines.length; i++) {
          let presMedicine = prescription.prescriptionMedicines[i];
          if (i == 0) {
            treatment.obatArv1 = presMedicine.medicineId;
            treatment.jmlHrObatArv1 = presMedicine.jumlahHari;
          } else if (i == 1) {
            treatment.obatArv2 = presMedicine.medicineId;
            treatment.jmlHrObatArv2 = presMedicine.jumlahHari;
          } else if (i == 2) {
            treatment.obatArv3 = presMedicine.medicineId;
            treatment.jmlHrObatArv3 = presMedicine.jumlahHari;
          }
          // console.log('will affect to : ', presMedicine.medicine.codeName, ' as many: ', presMedicine.amount);

          //affect overall inventories
          let visitObject = {
            patientId: req.visit.patientId,
            visitId: req.visit.id,
            visitActivityType: ENUM.VISIT_ACT_TYPE.TREATMENT,
            visitActivityId: treatment.id
          };
          if (i < 3 && presMedicine.amount > 0) {
            const inventoryChanged = await InventoryService.updateInventoryUPKByUsage(
              presMedicine.medicineId,
              presMedicine.amount,
              req.user,
              visitObject
            );
            // console.log('inventoryChanged : ', i, inventoryChanged);
          }
          theClosestDayCount =
            presMedicine.jumlahHari < theClosestDayCount
              ? presMedicine.jumlahHari
              : theClosestDayCount;
        }

        let tglKunjunganMoment = moment(treatment.tglKunjungan);
        let tglRencanaKunjunganMoment = moment(new Date()).add(1, "months");
        if (theClosestDayCount < Number.MAX_SAFE_INTEGER) {
          tglRencanaKunjunganMoment = moment(treatment.tglKunjungan).add(
            theClosestDayCount,
            "days"
          );
        }
        treatment.tglRencanaKunjungan = tglRencanaKunjunganMoment.format();
        treatment.bulanPeriodeLaporan = tglKunjunganMoment.month() + 1;

        //generate virtual treatment
        let monthDiff =
          tglRencanaKunjunganMoment.month() - tglKunjunganMoment.month();
        for (let i = 0; i < monthDiff - 1; i++) {
          tglKunjunganMoment.add(1, "months");
          const createdVisit = await Visit.create({
            isGenerated: true,
            ordinal: await VisitService.getNextOrdinalByVisit(
              req.visit.patientId
            ),
            visitType: ENUM.VISIT_TYPE.TREATMENT,
            visitDate: tglKunjunganMoment.format(),
            checkOutDate: tglKunjunganMoment.format(),
            patientId: req.visit.patientId,
            upkId: req.user.upkId,
            userRrId: req.user.id,
            createdBy: req.user.id
          });
          const createdPrescription = await Prescription.create({
            patientId: req.visit.patientId,
            visitId: createdVisit.id,
            regimentId: prescription.regimentId,
            prescriptionNumber: prescription.prescriptionNumber,
            paduanObatIds: prescription.paduanObatIds,
            paduanObatStr: prescription.paduanObatStr,
            notes: prescription.notes,
            isDraft: false,
            createdBy: req.user.id
          });
          //creating prescription copy
          for (
            let ii = 0;
            ii < prescription.prescriptionMedicines.length;
            ii++
          ) {
            const presMed = {
              prescriptionId: createdPrescription.id,
              medicineId: prescription.prescriptionMedicines[ii].medicineId,
              amount: 0,
              jumlahHari: 0,
              notes: prescription.prescriptionMedicines[ii].notes,
              createdBy: req.user.id
            };
            await PrescriptionMedicine.create(presMed);
            // console.log('iteration meds: ',i , ' #', ii, presMed);
          }

          let treatmentTemp = Object.assign(treatment.toJSON(), {
            id: undefined,
            visitId: createdVisit.id,
            prescriptionId: createdPrescription.id,
            isGenerated: true,
            tglKunjungan: tglKunjunganMoment.format(),
            tglRencanaKunjungan: treatment.tglRencanaKunjungan,
            ordinal: treatment.ordinal,
            obatArv1: treatment.obatArv1,
            jmlHrObatArv1: 0,
            obatArv2: treatment.obatArv2,
            jmlHrObatArv2: 0,
            obatArv3: treatment.obatArv3,
            jmlHrObatArv3: 0,
            bulanPeriodeLaporan: tglKunjunganMoment.month() + 1,
            createdBy: req.user.id
          });

          const treatmentParams = await Treatment.create(treatmentTemp);

          PatientService.updateMetaPatient(req.visit.patientId, {
            lastTreatmentDate: tglKunjunganMoment.format()
          });
          // console.log('iteration: ',i , visitParams, prescriptionParams, treatmentParams);
        }

        await treatment.save();
      } else {
        for (let i = 0; i < prescription.prescriptionMedicines.length; i++) {
          let presMedicine = prescription.prescriptionMedicines[i];

          //affect overall inventories
          let visitObject = {
            patientId: req.visit.patientId,
            visitId: req.visit.id
          };
          if (i < 3 && presMedicine.amount > 0) {
            const inventoryChanged = await InventoryService.updateInventoryUPKByUsage(
              presMedicine.medicineId,
              presMedicine.amount,
              req.user,
              visitObject
            );
            // console.log('inventoryChanged : ', i, inventoryChanged);
          }
        }
      }

      prescription.isDraft = false;
      await prescription.save();

      const endVisit = await visitController.endVisit(req, res, next, true);

      res.send({
        status: 200,
        message: "Success",
        data: prescription
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    getPrescriptions,
    getPrescriptionsByVisitId,
    getLastPrescription,
    getLastGivenPrescription,
    createPrescription,
    updatePrescription,
    givePrescription,
    getDetailById,
    getDetailByVisitId,
    getStatusPaduanObat
  };
};
module.exports = VisitPrescriptionController;
