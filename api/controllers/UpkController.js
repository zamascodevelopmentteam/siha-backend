const moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const Visit = require("../models/Visit");
const TestHiv = require("../models/TestHiv");
const Treatment = require("../models/Treatment");
const Upk = require("../models/Upk");
const Patient = require("../models/Patient");
const User = require("../models/User");

const UpkController = () => {
  const getStatistic = async (req, res) => {
    try {
      const { hospitalId } = req.params;
      // const upkId = hospitalId;
      const upkId = req.user.upkId;
      const visitEstimation = await Treatment.count({
        where: {
          // upkId: upkId,
          tglRencanaKunjungan: {
            [Op.between]: [
              moment()
                .startOf("month")
                .valueOf(),
              moment()
                .endOf("month")
                .valueOf()
            ]
          }
        },
        include: [
          {
            model: Visit,
            required: true,
            where: {
              upkId: upkId
            }
          }
        ]
      });
      const visitThisMonth = await Visit.count({
        where: {
          upkId: upkId,
          visitDate: {
            [Op.between]: [
              moment()
                .startOf("month")
                .valueOf(),
              moment()
                .endOf("month")
                .valueOf()
            ]
          }
        }
      });
      const visitThisDay = await Visit.count({
        where: {
          upkId: upkId,
          visitDate: {
            [Op.between]: [
              moment()
                .startOf("day")
                .valueOf(),
              moment()
                .endOf("day")
                .valueOf()
            ]
          }
        }
      });
      const examThisDay = await TestHiv.count({
        where: {
          createdAt: {
            [Op.between]: [
              moment()
                .startOf("day")
                .valueOf(),
              moment()
                .endOf("day")
                .valueOf()
            ]
          }
        },
        include: [
          {
            model: Visit,
            required: true,
            where: {
              upkId: upkId
            }
          }
        ]
      });

      res.send({
        status: 200,
        message: "Success",
        data: {
          visitEstimation,
          visitThisDay,
          visitThisMonth,
          examThisDay
        }
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: 500,
        message: error.message,
        data: null
      });
    }
  };

  const getVisitsToday = async (req, res, next) => {
    try {
      const { hospitalId } = req.params;
      // const upkId = hospitalId || req.params.upkId;
      const upkId = req.user.upkId;
      const visitsToday = await Visit.findAll({
        attributes: ["ordinal"],
        where: {
          upkId: upkId,
          visit_date: {
            [Op.between]: [
              moment()
                .startOf("day")
                .valueOf(),
              moment()
                .endOf("day")
                .valueOf()
            ]
          }
        },
        include: [
          {
            model: Patient,
            required: true,
            attributes: ["name"],
            include: [
              {
                model: User,
                required: true,
                as: "user",
                attributes: ["name", "avatar"]
              }
            ]
          }
        ],
        order: [["updated_at", "DESC"]],
        limit: 3
      });
      res.send({
        status: 200,
        message: "Success",
        data: visitsToday.map(data => {
          let dataObj = {
            ordinal: data.ordinal,
            User: data.patient.User
          };
          return dataObj;
        })
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    getStatistic,
    getVisitsToday
  };
};

module.exports = UpkController;
