const ENUM = require("../../config/enum");

const Lbpha2 = require("../services/report/lbpha2/lbpha2.class");
const VariableService = require("../services/variable.service");

const XLSX = require("xlsx");
const RegLab = require("./ReportRegisterLabController");

const ReportLBPHA2Controller = () => {
    const getLbpha2Report = async (req, res, next, returnItem = false) => {
        const upkId = req.user.upkId;
        try {
            let month2 = null;
            let year2 = null;
            const { month, withNonArv, lbpha } = req.query;
            if (month) {
                month2 = month.split('-')[1];
                year2 = month.split('-')[0];
            }

            const lbpha2 = new Lbpha2(req.user, req, month2, year2);
            await lbpha2.buildAllReport();
            // await lbpha2.generateSheetJSFile();
            await lbpha2.saveAllReportToDB();

            const data = lbpha2.getReport();
            if (lbpha) {
                data.inventoryTable.content.unshift({
                    title: "ARV"
                });
            }

            if (withNonArv) {
                const nonArv = await RegLab().getRegisterLabReport(req, res, next, true);
                if (lbpha) {
                    data.inventoryTable.content.push({
                        title: "Non ARV"
                    });
                }
                // perlu di fix
                nonArv.inventoryTable.content.map(value => {
                    const f = (value.data.column_A + value.data.column_B) - (value.data.column_C + value.data.column_E) + value.data.column_F;
                    data.inventoryTable.content.push({
                        data: {
                            column_A: value.data.column_A,
                            column_B: value.data.column_B,
                            column_C: value.data.column_C,
                            column_D: value.data.column_E,
                            column_E: value.data.column_F,
                            column_F: f,
                            column_G: value.data.column_G / value.medicine.packageMultiplier,
                            column_H: value.data.tgl_kadaluarsa,
                            column_I: value.data.column_H,
                            paket_keluar_reguler: value.data.column_C,
                        },
                        medicine: value.medicine
                    });
                });
            }

            if (returnItem) {
                return data;
            }

            res.json(data);
        } catch (error) {
            next(error);
        }
    };

    const getLbpha2ReportExcel = async (req, res, next) => {
        let { meta } = req.query;
        meta = JSON.parse(meta);

        let month2 = null;
        let year2 = null;
        const { month } = req.query;
        if (month) {
            month2 = parseInt(month.split('-')[1]) - 1;
            year2 = month.split('-')[0];
        }

        const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
        let dateObj = new Date();
        let monthh = dateObj.getMonth();
        let day = String(dateObj.getDate()).padStart(2, '0');
        let year = dateObj.getFullYear();
        let date = day + '-' + monthh + '-' + year;
        let dateMoth = day + ' ' + monthNames[monthh - 1] + ' ' + year;

        const data = await getLbpha2Report(req, res, next, true);
        const {
            // meta: meta,
            rejimenStandarTable: rejimenStandar,
            rejimenLainTable: rejimenLain,
            FDCTable: FDC,
            inventoryTable: inventory
        } = data;
        // console.log(rejimenStandar, 'aaaaaaaaaaaaaaaaaaa');
        const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X"];
        const sheetObj = {
            "Laporan LBPHA2": {
                "!ref": "A1:X300",
                // ---------------------------
                A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
                A2: { t: 's', v: meta && meta.upk.name ? meta.upk.name : "-" },
                A3: { t: 's', v: "Nama Kab/Kota" },
                A4: { t: 's', v: meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
                A5: { t: 's', v: "Nama Provinsi" },
                A6: { t: 's', v: meta && meta.province.name ? meta.province.name : "-" },
                // ---------------------------
                C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
                C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
                C3: { t: 's', v: "Tahun" },
                C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
                C5: { t: 's', v: "Tanggal Akses" },
                C6: { t: 's', v: dateMoth },
                // ----------------------------
                A7: { t: 's', v: "Rejimen Standar" },
                A9: { t: 's', v: "No" },
                B9: { t: 's', v: "Rejimen Standar" },
                C8: { t: 's', v: "Pasien Reguler" },
                E8: { t: 's', v: "Pasien Transit" },
                G8: { t: 's', v: "Pasien Reguler yang Mengambil Obat > 1 bulan" },
                C9: { t: 's', v: "Dewasa" },
                D9: { t: 's', v: "Anak" },
                E9: { t: 's', v: "Dewasa" },
                F9: { t: 's', v: "Anak" },
                G9: { t: 's', v: "Dewasa" },
                H9: { t: 's', v: "Anak" },
                "!merges": [
                    // { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
                    // { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
                ]
            }
        };

        let row = 10;
        let idx = 0;
        Object.keys(rejimenStandar.content).map(keyContent => {
            sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: rejimenStandar.content[keyContent].title };

            row++;

            {
                rejimenStandar.content[keyContent].regimentList.map(
                    (value, index) => {
                        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 'n', v: (index + 1) };
                        sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: value.regiment.combinationStrInfo };
                        {
                            Object.keys(value.data).map(key => {
                                Object.keys(value.data[key]).map(keyData => {
                                    if (idx == 6) {
                                        idx = 0;
                                    }

                                    sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 'n', v: value.data[key][keyData] };

                                    idx++;
                                })
                            })
                        }

                        row++;
                    }
                )
            }
        })
        idx = 0;
        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Total Pasien Rejimen Standar" };
        {
            Object.keys(rejimenStandar.footer.totalPasienRejimenStandar).map(key => {
                {
                    Object.keys(rejimenStandar.footer.totalPasienRejimenStandar[key]).map(keyPasien => {
                        if (idx == 6) {
                            idx = 0;
                        }

                        sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 'n', v: rejimenStandar.footer.totalPasienRejimenStandar[key][keyPasien] };

                        idx++;
                    })
                }
            })
        }

        row++;

        idx = 0;
        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Total Pasien" };
        {
            Object.keys(rejimenStandar.footer.totalPasien).map(key => {
                {
                    Object.keys(rejimenStandar.footer.totalPasien[key]).map(keyPasien => {
                        if (idx == 6) {
                            idx = 0;
                        }

                        sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 'n', v: rejimenStandar.footer.totalPasien[key][keyPasien] };

                        idx++;
                    })
                }
            })
        }
        // ----------------------------------------------
        row++;
        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Rejimen Lain" };

        row++;

        sheetObj["Laporan LBPHA2"]["C" + row] = { t: 's', v: "Pasien Reguler" };
        sheetObj["Laporan LBPHA2"]["E" + row] = { t: 's', v: "Pasien Transit" };
        sheetObj["Laporan LBPHA2"]["G" + row] = { t: 's', v: "Pasien Reguler yang Mengambil Obat > 1 bulan" };

        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "No" };
        sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: "Rejimen Lainn" };
        sheetObj["Laporan LBPHA2"]["C" + row] = { t: 's', v: "Dewasa" };
        sheetObj["Laporan LBPHA2"]["D" + row] = { t: 's', v: "Anak" };
        sheetObj["Laporan LBPHA2"]["E" + row] = { t: 's', v: "Dewasa" };
        sheetObj["Laporan LBPHA2"]["F" + row] = { t: 's', v: "Anak" };
        sheetObj["Laporan LBPHA2"]["G" + row] = { t: 's', v: "Dewasa" };
        sheetObj["Laporan LBPHA2"]["H" + row] = { t: 's', v: "Anak" };

        row++;


        rejimenLain.content.regimentList.map((value, index) => {
            sheetObj["Laporan LBPHA2"]["A" + row] = { t: 'n', v: (index + 1) };
            sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: value.regiment.combinationStrInfo };
            {
                Object.keys(value.data).map(key => {
                    Object.keys(value.data[key]).map(keyData => {
                        if (idx == 6) {
                            idx = 0;
                        }

                        sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 'n', v: value.data[key][keyData] };

                        idx++;
                    })
                })
            }

            row++;
        })
        idx = 0;
        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Total Pasien Rejimen Lain" };
        {
            Object.keys(rejimenLain.footer.totalPasienRejimenLain).map(key => {
                {
                    Object.keys(rejimenLain.footer.totalPasienRejimenLain[key]).map(keyPasien => {
                        if (idx == 6) {
                            idx = 0;
                        }

                        sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 'n', v: rejimenLain.footer.totalPasienRejimenLain[key][keyPasien] };

                        idx++;
                    })
                }
            })
        }
        // ------------------------------------------------
        row++;
        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "FDC" };

        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Ketarangan Penggunaan FDC Junior" };
        sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: "ZDV/3TC/NVP Triple FDC Junior (60/30/50)" };

        row++;

        sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: "Pasien Reguler" };
        sheetObj["Laporan LBPHA2"]["D" + row] = { t: 's', v: "Pasien Transit" };
        sheetObj["Laporan LBPHA2"]["F" + row] = { t: 's', v: "Pasien yang Mengambil Obat >1 bulan" };

        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Berat Badan (kg)" };
        sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: "Jumah pasien Anak" };
        sheetObj["Laporan LBPHA2"]["C" + row] = { t: 's', v: "Tablet yang diperlukan/hari" };
        sheetObj["Laporan LBPHA2"]["D" + row] = { t: 's', v: "Jumah pasien Anak" };
        sheetObj["Laporan LBPHA2"]["E" + row] = { t: 's', v: "Tablet yang diperlukan/hari" };
        sheetObj["Laporan LBPHA2"]["F" + row] = { t: 's', v: "Jumah pasien Anak" };
        sheetObj["Laporan LBPHA2"]["G" + row] = { t: 's', v: "Tablet yang diperlukan/hari" };

        row++;

        idx = 0;
        FDC.content.map((content) => {
            sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: content.title };
            {
                Object.keys(content.data).map(keyData => {
                    Object.keys(content.data[keyData]).map(keyDetail => {
                        if (idx == 6) {
                            idx = 0;
                        }

                        sheetObj["Laporan LBPHA2"][indexCols[idx + 1] + row] = { t: 'n', v: content.data[keyData][keyDetail] };

                        idx++;
                    })
                })
            }

            row++;
        })

        idx = 0;
        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Jumlah tab yang diperlukan" };
        {
            Object.keys(FDC.footer.jumlahTabDiperlukan).map(key =>
                Object.keys(FDC.footer.jumlahTabDiperlukan[key]).map(keyDetail => {
                    if (idx == 6) {
                        idx = 0;
                    }

                    sheetObj["Laporan LBPHA2"][indexCols[idx + 1] + row] = { t: 'n', v: FDC.footer.jumlahTabDiperlukan[key][keyDetail] };

                    idx++;
                })
            )
        }

        idx = 0;
        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Jumlah tab yang diperlukan + buffer 2 bulan" };
        {
            Object.keys(FDC.footer.jumlahTabDiperlukanBuffer).map(key =>
                Object.keys(FDC.footer.jumlahTabDiperlukanBuffer[key]).map(keyDetail => {
                    if (idx == 6) {
                        idx = 0;
                    }

                    sheetObj["Laporan LBPHA2"][indexCols[idx + 1] + row] = { t: 'n', v: FDC.footer.jumlahTabDiperlukanBuffer[key][keyDetail] };

                    idx++;
                })
            )
        }
        // -------------------------------
        row++;
        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "Stock Obat" };

        row++;

        sheetObj["Laporan LBPHA2"]["C" + row] = { t: 's', v: "Stok obat awal bulan (tablet)" };
        sheetObj["Laporan LBPHA2"]["D" + row] = { t: 's', v: "Stok obat yang diterima bulan ini (tablet)" };
        sheetObj["Laporan LBPHA2"]["E" + row] = { t: 's', v: "Stok obat yang dikeluarkan bulan ini (tablet)" };
        sheetObj["Laporan LBPHA2"]["F" + row] = { t: 's', v: "Stok obat yang kadaluarsa bulan ini (tablet)" };
        sheetObj["Laporan LBPHA2"]["G" + row] = { t: 's', v: "Selisih fisik stok obat dengan pencatatan bulan ini (tablet)" };
        sheetObj["Laporan LBPHA2"]["H" + row] = { t: 's', v: "Stok obat pada akhir bulan ini (tablet)" };
        sheetObj["Laporan LBPHA2"]["I" + row] = { t: 's', v: "Stok obat pada akhir bulan ini (botol)" };
        sheetObj["Laporan LBPHA2"]["J" + row] = { t: 's', v: "Tanggal kadaluarsa (botol)" };
        sheetObj["Laporan LBPHA2"]["K" + row] = { t: 's', v: "Perkiraan Jumlah obat yang diperlukan (botol)" };
        sheetObj["Laporan LBPHA2"]["L" + row] = { t: 's', v: "Jumlah Pasien reguler bulan ini" };

        row++;

        sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: "No" };
        sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: "Nama Obat" };
        sheetObj["Laporan LBPHA2"]["C" + row] = { t: 's', v: "(A)" };
        sheetObj["Laporan LBPHA2"]["D" + row] = { t: 's', v: "(B)" };
        sheetObj["Laporan LBPHA2"]["E" + row] = { t: 's', v: "(C)" };
        sheetObj["Laporan LBPHA2"]["F" + row] = { t: 's', v: "(D)" };
        sheetObj["Laporan LBPHA2"]["G" + row] = { t: 's', v: "(E)" };
        sheetObj["Laporan LBPHA2"]["H" + row] = { t: 's', v: "(F) = (A+b)-(C+D)+E" };
        sheetObj["Laporan LBPHA2"]["I" + row] = { t: 's', v: "(G)" };
        sheetObj["Laporan LBPHA2"]["J" + row] = { t: 's', v: "(H)" };
        sheetObj["Laporan LBPHA2"]["K" + row] = { t: 's', v: "(I)" };
        sheetObj["Laporan LBPHA2"]["L" + row] = { t: 's', v: "(-)" };

        row++;

        idx = 0;
        inventory.content.map((value, index) => {
            if (!value.title) {
                sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: (index + 1) };
                sheetObj["Laporan LBPHA2"]["B" + row] = { t: 's', v: value.medicine.name };
                {
                    Object.keys(value.data).map((key) => {
                        if (idx == 10) {
                            idx = 0;
                        }

                        if (typeof value.data[key] === "number") {
                            sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 'n', v: formatNumber(value.data[key]) };
                        } else {
                            sheetObj["Laporan LBPHA2"][indexCols[idx + 2] + row] = { t: 's', v: value.data[key] };
                        }

                        idx++;
                    })
                }
            } else {
                sheetObj["Laporan LBPHA2"]["A" + row] = { t: 's', v: value.title };
            }

            row++;
        })

        XLSX.writeFile({
            SheetNames: ["Laporan LBPHA2"],
            Sheets: sheetObj
        }, './api/excel/LaporanLBPHA2-' + (meta && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
        return res.send({
            status: 200,
            message: "Success",
            data: "/api/excel/LaporanLBPHA2-" + (meta && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
        });
    }

    const getArvNonArvExcel = async (req, res, next) => {
        let { meta } = req.query;
        if (meta) {
            meta = JSON.parse(meta);
        }

        let month2 = null;
        let year2 = null;
        const { month, type } = req.query;
        if (month) {
            month2 = parseInt(month.split('-')[1]) - 1;
            year2 = month.split('-')[0];
        }

        const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
        let dateObj = new Date();
        let monthh = dateObj.getMonth();
        let day = String(dateObj.getDate()).padStart(2, '0');
        let year = dateObj.getFullYear();
        let date = day + '-' + monthh + '-' + year;
        let dateMoth = day + ' ' + monthNames[monthh - 1] + ' ' + year;

        const data = await getLbpha2Report(req, res, next, true);
        // const nonArv = await RegLab().getRegisterLabReport(req, res, next, true);
        // console.log(nonArv.inventoryTable.content);

        let {
            inventoryTable: inventory
        } = data;

        // const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X"];
        const sheetObj = {
            "Penggunaan ARV & non-ARV": {
                "!ref": "A1:X300",
                // ---------------------------
                // A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
                // A2: { t: 's', v: meta && meta.upk.name ? meta.upk.name : "-" },
                // A3: { t: 's', v: "Nama Kab/Kota" },
                // A4: { t: 's', v: meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
                // A5: { t: 's', v: "Nama Provinsi" },
                // A6: { t: 's', v: meta && meta.province.name ? meta.province.name : "-" },
                // // ---------------------------
                // C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
                // C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
                // C3: { t: 's', v: "Tahun" },
                // C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
                // C5: { t: 's', v: "Tanggal Akses" },
                // C6: { t: 's', v: dateMoth },
                // ----------------------------
                A1: { t: 's', v: "Laporan Penggunaan ARV & non-ARV" },
                A2: { t: 's', v: "Nama Barang" },
                B2: { t: 's', v: "Tipe" },
                C2: { t: 's', v: "Stok Awal" },
                D2: { t: 's', v: "Stok Diterima" },
                E2: { t: 's', v: "Stok Digunakan" },
                F2: { t: 's', v: "Stok Dikirim" },
                G2: { t: 's', v: "Stok Selisih" },
                H2: { t: 's', v: "Stok Akhir" },
                I2: { t: 's', v: "Expired Date" },
                "!merges": [
                    // { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
                    // { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
                ]
            }
        };

        let filteredData2, datax = inventory.content;

        if (type.length) {
            filteredData2 = datax.filter(item => {
                let startsWithCondition =
                    item.medicine.medicineType.toLowerCase() == type.toLowerCase()
                let includesCondition =
                    item.medicine.medicineType.toLowerCase() == type.toLowerCase()
                if (startsWithCondition) {
                    return startsWithCondition
                } else if (!startsWithCondition && includesCondition) {
                    return includesCondition
                } else return null
            })
            datax = filteredData2;
        }

        let row = 3;
        datax.map((value) => {
            sheetObj["Penggunaan ARV & non-ARV"]["A" + row] = { t: 's', v: value.medicine.name + ' ' + value.medicine.codeName };
            sheetObj["Penggunaan ARV & non-ARV"]["B" + row] = { t: 's', v: value.medicine.medicineType };
            sheetObj["Penggunaan ARV & non-ARV"]["C" + row] = { t: 's', v: value.data.column_A };
            sheetObj["Penggunaan ARV & non-ARV"]["D" + row] = { t: 's', v: value.data.column_B };
            sheetObj["Penggunaan ARV & non-ARV"]["E" + row] = { t: 's', v: value.data.column_C };
            sheetObj["Penggunaan ARV & non-ARV"]["F" + row] = { t: 's', v: value.data.paket_keluar_reguler };
            sheetObj["Penggunaan ARV & non-ARV"]["G" + row] = { t: 's', v: value.data.column_E };
            sheetObj["Penggunaan ARV & non-ARV"]["H" + row] = { t: 's', v: value.data.column_G };
            sheetObj["Penggunaan ARV & non-ARV"]["I" + row] = { t: 's', v: value.data.column_H };

            row++;
        })

        // nonArv.inventoryTable.content.map(value => {
        //   sheetObj["Penggunaan ARV & non-ARV"]["A" + row] = { t: 's', v: value.medicine.name + ' ' + value.medicine.codeName };
        //   sheetObj["Penggunaan ARV & non-ARV"]["B" + row] = { t: 's', v: value.medicine.medicineType };
        //   sheetObj["Penggunaan ARV & non-ARV"]["C" + row] = { t: 's', v: value.data.column_A };
        //   sheetObj["Penggunaan ARV & non-ARV"]["D" + row] = { t: 's', v: value.data.column_B };
        //   sheetObj["Penggunaan ARV & non-ARV"]["E" + row] = { t: 's', v: value.data.column_C };
        //   // ---
        //   // sheetObj["Penggunaan ARV & non-ARV"]["F" + row] = { t: 's', v: value.data.paket_keluar_reguler };
        //   sheetObj["Penggunaan ARV & non-ARV"]["F" + row] = { t: 's', v: value.data.column_C };
        //   // ---
        //   sheetObj["Penggunaan ARV & non-ARV"]["G" + row] = { t: 's', v: value.data.column_F };
        //   sheetObj["Penggunaan ARV & non-ARV"]["H" + row] = { t: 's', v: value.data.column_G };

        //   row++;
        // });

        XLSX.writeFile({
            SheetNames: ["Penggunaan ARV & non-ARV"],
            Sheets: sheetObj
        }, './api/excel/LaporanPenggunaanARV--non-ARV-' + (meta && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
        return res.send({
            status: 200,
            message: "Success",
            data: "/api/excel/LaporanPenggunaanARV--non-ARV-" + (meta && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
        });
    }

    const getRegimenExcel = async (req, res, next) => {
        // let { meta } = req.query;
        // if (meta) {
        //   meta = JSON.parse(meta);
        // }

        let month2 = null;
        let year2 = null;
        const { month } = req.query;
        if (month) {
            month2 = parseInt(month.split('-')[1]) - 1;
            year2 = month.split('-')[0];
        }

        const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
        let dateObj = new Date();
        let monthh = dateObj.getMonth();
        let day = String(dateObj.getDate()).padStart(2, '0');
        let year = dateObj.getFullYear();
        let date = day + '-' + monthh + '-' + year;
        let dateMoth = day + ' ' + monthNames[monthh - 1] + ' ' + year;

        const data = await getLbpha2Report(req, res, next, true);

        const {
            meta: meta,
            rejimenStandarTable: rejimenStandar,
            rejimenLainTable: rejimenLain,
            // FDCTable: FDC,
            // inventoryTable: inventory
        } = data;

        // const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X"];
        const sheetObj = {
            "Laporan Rejimen": {
                "!ref": "A1:X300",
                // ---------------------------
                A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
                A2: { t: 's', v: meta && meta.upk.name ? meta.upk.name : "-" },
                A3: { t: 's', v: "Nama Kab/Kota" },
                A4: { t: 's', v: meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
                A5: { t: 's', v: "Nama Provinsi" },
                A6: { t: 's', v: meta && meta.province.name ? meta.province.name : "-" },
                // ---------------------------
                C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
                C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
                C3: { t: 's', v: "Tahun" },
                C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
                C5: { t: 's', v: "Tanggal Akses" },
                C6: { t: 's', v: dateMoth },
                // ----------------------------
                A7: { t: 's', v: "Tahun" },
                B7: { t: 's', v: "Bulan" },
                C7: { t: 's', v: "Provinsi" },
                D7: { t: 's', v: "Kabupaten" },
                E7: { t: 's', v: "UPK" },
                F7: { t: 's', v: "Kode UPK" },
                G7: { t: 's', v: "Dewasa" },
                H7: { t: 's', v: "Anak" },
                I7: { t: 's', v: "Keterangan" },
                J7: { t: 's', v: "Lini" },
                K7: { t: 's', v: "Tipe Rejimen" },
                "!merges": [
                    // { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
                    // { s: { r: 6, c: 1 }, e: { r: 6, c: 11 } },
                ]
            }
        };

        let row = 8;
        Object.keys(rejimenStandar.content).map((key) => {
            rejimenStandar.content[key].regimentList.map(v => {
                let province = "-";
                let kabupaten = "-";
                let upk = "-";
                let upkCode = "-";
                let dewasa = 0;
                let anak = 0;
                let dateObj = new Date(v.regiment.createdAt);
                let monthh = dateObj.getMonth();
                let day = String(dateObj.getDate()).padStart(2, '0');
                let year = dateObj.getFullYear();

                if (v.regiment.createdByData) {
                    if (v.regiment.createdByData.province) {
                        province = v.regiment.createdByData.province.name;
                    }
                    if (v.regiment.createdByData.sudinKabKota) {
                        kabupaten = v.regiment.createdByData.sudinKabKota.name;
                    }
                    if (v.regiment.createdByData.upk) {
                        upk = v.regiment.createdByData.upk.name;
                        upkCode = v.regiment.createdByData.upk.codeId;
                    }
                }

                dewasa += v.data.pasienMultiMonth.dewasa;
                dewasa += v.data.pasienReguler.dewasa;
                dewasa += v.data.pasienTransit.dewasa;

                anak += v.data.pasienMultiMonth.anak;
                anak += v.data.pasienReguler.anak;
                anak += v.data.pasienTransit.anak;

                sheetObj["Laporan Rejimen"]["A" + row] = { t: 's', v: year };
                sheetObj["Laporan Rejimen"]["B" + row] = { t: 's', v: monthNames[monthh] };
                sheetObj["Laporan Rejimen"]["C" + row] = { t: 's', v: province };
                sheetObj["Laporan Rejimen"]["D" + row] = { t: 's', v: kabupaten };
                sheetObj["Laporan Rejimen"]["E" + row] = { t: 's', v: upk };
                sheetObj["Laporan Rejimen"]["F" + row] = { t: 's', v: upkCode };
                sheetObj["Laporan Rejimen"]["G" + row] = { t: 's', v: dewasa };
                sheetObj["Laporan Rejimen"]["H" + row] = { t: 's', v: anak };
                sheetObj["Laporan Rejimen"]["I" + row] = { t: 's', v: v.regiment.combinationStrInfo };
                sheetObj["Laporan Rejimen"]["J" + row] = { t: 's', v: rejimenStandar.content[key].title };
                sheetObj["Laporan Rejimen"]["K" + row] = { t: 's', v: "Rejimen Standar" };

                row++;
            });
        })

        rejimenLain.content.regimentList.map(v => {
            let province = "-";
            let kabupaten = "-";
            let upk = "-";
            let upkCode = "-";
            let dewasa = 0;
            let anak = 0;
            let dateObj = new Date(v.regiment.createdAt);
            let monthh = dateObj.getMonth();
            let day = String(dateObj.getDate()).padStart(2, '0');
            let year = dateObj.getFullYear();

            if (v.regiment.createdByData) {
                if (v.regiment.createdByData.province) {
                    province = v.regiment.createdByData.province.name;
                }
                if (v.regiment.createdByData.sudinKabKota) {
                    kabupaten = v.regiment.createdByData.sudinKabKota.name;
                }
                if (v.regiment.createdByData.upk) {
                    upk = v.regiment.createdByData.upk.name;
                    upkCode = v.regiment.createdByData.upk.codeId;
                }
            }

            dewasa += v.data.pasienMultiMonth.dewasa;
            dewasa += v.data.pasienReguler.dewasa;
            dewasa += v.data.pasienTransit.dewasa;

            anak += v.data.pasienMultiMonth.anak;
            anak += v.data.pasienReguler.anak;
            anak += v.data.pasienTransit.anak;

            sheetObj["Laporan Rejimen"]["A" + row] = { t: 's', v: year };
            sheetObj["Laporan Rejimen"]["B" + row] = { t: 's', v: monthNames[monthh] };
            sheetObj["Laporan Rejimen"]["C" + row] = { t: 's', v: province };
            sheetObj["Laporan Rejimen"]["D" + row] = { t: 's', v: kabupaten };
            sheetObj["Laporan Rejimen"]["E" + row] = { t: 's', v: upk };
            sheetObj["Laporan Rejimen"]["F" + row] = { t: 's', v: upkCode };
            sheetObj["Laporan Rejimen"]["G" + row] = { t: 's', v: dewasa };
            sheetObj["Laporan Rejimen"]["H" + row] = { t: 's', v: anak };
            sheetObj["Laporan Rejimen"]["I" + row] = { t: 's', v: v.regiment.combinationStrInfo };
            sheetObj["Laporan Rejimen"]["J" + row] = { t: 's', v: "-" };
            sheetObj["Laporan Rejimen"]["K" + row] = { t: 's', v: "Rejimen Lain" };

            row++;
        })

        XLSX.writeFile({
            SheetNames: ["Laporan Rejimen"],
            Sheets: sheetObj
        }, './api/excel/LaporanPenggunaanARV--non-ARV-' + (meta && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + '-' + date + '.xlsx');
        return res.send({
            status: 200,
            message: "Success",
            data: "/api/excel/LaporanPenggunaanARV--non-ARV-" + (meta && meta.upk.name ? meta.upk.name.replace(/ /g, "-") : "-") + "-" + date + ".xlsx"
        });
    }

    const formatNumber = num => {
        if (!num) {
            return 0;
        }
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    };

    return { getLbpha2Report, getLbpha2ReportExcel, getArvNonArvExcel, getRegimenExcel };
};

module.exports = ReportLBPHA2Controller;
