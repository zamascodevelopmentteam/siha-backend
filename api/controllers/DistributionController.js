const differenceBy = require("lodash/differenceBy");
const intersectionBy = require("lodash/intersectionBy");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const DistPlan = require("../models/DistributionPlan");
const DistPlanItem = require("../models/DistributionPlanItem");
const Inventory = require("../models/Inventory");
const Brand = require("../models/Brand");
const Receipt = require("../models/Receipt");
const ReceiptItem = require("../models/ReceiptItem");
const Order = require("../models/Order");
const Upk = require("../models/Upk");
const SudinKabKota = require("../models/SudinKabKota");
const Province = require("../models/Province");
const Medicine = require("../models/Medicine");
const QueryService = require("../services/query.service")();
const CONSTANT = require("../../config/constant");
const ENUM = require("../../config/enum");
const OrderItem = require("../models/OrderItem");

const DistributionController = () => {
  const getPlans = async (req, res, next) => {
    let { page, limit, keyword, status } = req.query;
    const { user } = req;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    try {
      var whereObj = {};
      switch (user.logisticrole) {
        case ENUM.LOGISTIC_ROLE.LAB_ENTITY:
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.LAB_ENTITY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
            upkId: user.upkId
          };
          break;
        case ENUM.LOGISTIC_ROLE.PHARMA_ENTITY:
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.PHARMA_ENTITY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
            upkId: user.upkId
          };
          break;
        case "UPK_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
            upkId: user.upkId
          };
          break;
        case "SUDIN_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
            sudinKabKotaId: user.sudinKabKotaId
          };
          break;
        case "PROVINCE_ENTITY":
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
            provinceId: user.provinceId
          };
          break;
        case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
          whereObj = {
            logisticRole: ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL
          };
          break;
      }
      if (status && status !== "ALL") {
        whereObj.planStatus = status;
      }

      const planList = await DistPlan.findAndCountAll({
        where: whereObj,
        include: [
          {
            model: Order,
            attributes: ["id", "lastApprovalStatus"],
            include: [
              {
                model: Upk,
                required: false,
                as: "upkOwnerData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaOwnerData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceOwnerData",
                attributes: ["name"]
              },
              {
                model: Upk,
                required: false,
                as: "upkForData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaForData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceForData",
                attributes: ["name"]
              }
            ]
          },
          {
            model: Upk,
            required: false,
            attributes: ["name"]
          },
          {
            model: SudinKabKota,
            required: false,
            attributes: ["name"]
          },
          {
            model: Province,
            required: false,
            attributes: ["name"]
          },
          {
            model: Upk,
            required: false,
            as: "upkSenderData",
            attributes: ["name"]
          },
          {
            model: SudinKabKota,
            required: false,
            as: "sudinKabKotaSenderData",
            attributes: ["name"]
          },
          {
            model: Province,
            required: false,
            as: "provinceSenderData",
            attributes: ["name"]
          }
        ].concat(QueryService.includeCreated()),
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      var listDelivery = [];
      if (planList) {
        listDelivery = planList.rows.map(resultItem => {
          var dataDetail = resultItem.toJSON();
          if (dataDetail.distType) {
            dataDetail.senderName = dataDetail.entityName;
            dataDetail.recieverName =
              (dataDetail.upk && dataDetail.upk.name) ||
              (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
              (dataDetail.province && dataDetail.province.name);
          } else if (dataDetail.order) {
            dataDetail.senderName =
              (dataDetail.order.upkForData &&
                dataDetail.order.upkForData.name) ||
              (dataDetail.order.sudinKabKotaForData &&
                dataDetail.order.sudinKabKotaForData.name) ||
              (dataDetail.order.provinceForData &&
                dataDetail.order.provinceForData.name);
            delete dataDetail.order;
          } else if (!dataDetail.order) {
            dataDetail.senderName =
              (dataDetail.upkSenderData && dataDetail.upkSenderData.name) ||
              (dataDetail.sudinKabKotaSenderData &&
                dataDetail.sudinKabKotaSenderData.name) ||
              (dataDetail.provinceSenderData &&
                dataDetail.provinceSenderData.name);
          } else {
            dataDetail.senderName = "NO NAME";
            dataDetail.recieverName = "NO NAME";
          }
          dataDetail.recieverName =
            (dataDetail.upk && dataDetail.upk.name) ||
            (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
            (dataDetail.province && dataDetail.province.name);
          dataDetail.isRelocation =
            dataDetail.logisticRole === dataDetail.logisticRoleSender;

          return dataDetail;
        });
      }
      res.status(200).json({
        status: 200,
        message: "success",
        data: listDelivery,
        paging: {
          page: Number(page),
          size: listDelivery.length,
          total: planList.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getDelivery = async (req, res, next) => {
    let {
      page,
      limit,
      keyword,
      status,
      distributionType,
      relokasi
    } = req.query;
    const { user } = req;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    try {
      var whereObj = {};
      var whereDis = {};
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          Object.assign(whereDis, {
            logisticRoleSender: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            upkIdSender: user.upkId
          });
          break;
        case "SUDIN_ENTITY":
          Object.assign(whereDis, {
            logisticRoleSender: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
            sudinKabKotaIdSender: user.sudinKabKotaId
          });
          break;
        case "PROVINCE_ENTITY":
          Object.assign(whereDis, {
            logisticRoleSender: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
            provinceIdSender: user.provinceId
          });
          break;
        case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
          Object.assign(whereDis, {
            logisticRoleSender: ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY
          });
          break;
      }
      if (status && status !== "ALL") {
        Object.assign(whereDis, {
          planStatus: status
        });
      }

      if (distributionType && distributionType === "DISTRIBUSI_KHUSUS") {
        Object.assign(whereDis, {
          $order$: { [Op.is]: null }
        });
      } else if (
        distributionType &&
        distributionType === "PENGIRIMAN_REGULER"
      ) {
        Object.assign(whereDis, {
          "$order.is_regular$": true
        });
      } else if (distributionType && distributionType === "PENGIRIMAN_KHUSUS") {
        Object.assign(whereDis, {
          "$order.is_regular$": false
        });
      }

      if (relokasi && relokasi === "YA") {
        Object.assign(whereDis, {
          logisticRole: { [Op.eq]: { [Op.col]: "logistic_role_sender" } }
        });
      } else if (relokasi && relokasi === "TIDAK") {
        Object.assign(whereDis, {
          logisticRole: { [Op.ne]: { [Op.col]: "logistic_role_sender" } }
        });
      }

      const planList = await DistPlan.findAndCountAll({
        where: whereDis,
        include: [
          {
            model: Order,
            attributes: [
              "id",
              "lastApprovalStatus",
              "upkIdFor",
              "sudinKabKotaIdFor",
              "provinceIdFor"
            ],
            required: false,
            // where: whereObj,
            include: [
              {
                model: Upk,
                required: false,
                as: "upkOwnerData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaOwnerData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceOwnerData",
                attributes: ["name"]
              },
              {
                model: Upk,
                required: false,
                as: "upkForData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaForData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceForData",
                attributes: ["name"]
              }
            ]
          },
          {
            model: Upk,
            attributes: ["name"]
          },
          {
            model: SudinKabKota,
            attributes: ["name"]
          },
          {
            model: Province,
            attributes: ["name"]
          }
        ].concat(QueryService.includeCreated()),
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      var listDelivery = [];
      if (planList) {
        listDelivery = planList.rows.map(resultItem => {
          var dataDetail = resultItem.toJSON();
          if (dataDetail.distType) {
            dataDetail.senderName = dataDetail.entityName;
            dataDetail.recieverName =
              (dataDetail.upk && dataDetail.upk.name) ||
              (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
              (dataDetail.province && dataDetail.province.name);
          } else if (dataDetail.order) {
            dataDetail.senderName =
              (dataDetail.order.upkForData &&
                dataDetail.order.upkForData.name) ||
              (dataDetail.order.sudinKabKotaForData &&
                dataDetail.order.sudinKabKotaForData.name) ||
              (dataDetail.order.provinceForData &&
                dataDetail.order.provinceForData.name);
          }
          dataDetail.recieverName =
            (dataDetail.upk && dataDetail.upk.name) ||
            (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
            (dataDetail.province && dataDetail.province.name);
          delete dataDetail.order;
          return dataDetail;
        });
      }
      res.status(200).json({
        status: 200,
        message: "success",
        data: listDelivery,
        paging: {
          page: Number(page),
          size: listDelivery.length,
          total: planList.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const getPlanDetails = async (req, res, next) => {
    const { user } = req;
    const { id } = req.params;
    try {
      var planDetails = await DistPlan.findByPk(id, {
        include: [
          {
            model: DistPlanItem,
            attributes: [
              ["id", "distPlanItemId"],
              "orderItemId",
              "packageUnitType",
              "unitPrice",
              ["total_price", "totalPriceItem"],
              "packageQuantity",
              "expiredDate",
              "medicineId",
              "notes"
            ],
            include: [
              {
                model: Medicine,
                attributes: ["id", "name", "codeName"]
              },
              {
                model: Inventory,
                include: [
                  {
                    model: Brand,
                    attributes: ["name"]
                  },
                  {
                    model: Medicine,
                    attributes: ["name", "codeName"]
                  }
                ]
              },
              {
                model: Inventory,
                required: false,
                as: "inventoryForData",
                include: [
                  {
                    model: Brand,
                    attributes: ["name"]
                  },
                  {
                    model: Medicine,
                    attributes: ["name", "codeName"]
                  }
                ]
              },
              {
                model: OrderItem
              }
            ]
          },
          {
            model: Receipt,
            required: false,
            attributes: [
              ["id", "receiptId"],
              "receiptNumber",
              "receiptStatus",
              "notes"
            ],
            include: [
              {
                model: ReceiptItem,
                required: false,
                attributes: [
                  ["id", "receiptItemId"],
                  "receiptItemStatus",
                  "batchCode",
                  "receivedExpiredDate",
                  "receivedPkgUnitType",
                  "receivedPkgQuantity"
                ],
                include: [
                  {
                    model: Inventory,
                    include: [
                      {
                        model: Brand,
                        attributes: ["name"],
                        include: [
                          {
                            model: Medicine,
                            attributes: ["id", "name", "codeName"]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            model: Order,
            required: false,
            attributes: [
              "id",
              "lastOrderStatus",
              "lastApprovalStatus",
              "logisticRole",
              "upkIdOwner",
              "sudinKabKotaIdOwner",
              "provinceIdOwner",
              "upkIdFor",
              "sudinKabKotaIdFor",
              "provinceIdFor"
            ],
            include: [
              {
                model: Upk,
                required: false,
                as: "upkOwnerData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaOwnerData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceOwnerData",
                attributes: ["name"]
              },
              {
                model: Upk,
                required: false,
                as: "upkForData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaForData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceForData",
                attributes: ["name"]
              }
            ]
          }
        ].concat(QueryService.includeCreated())
      });
      var dataDetail = [];
      if (planDetails) {
        dataDetail = planDetails.toJSON();
        if (dataDetail.distType) {
          dataDetail.senderName = dataDetail.entityName;
          dataDetail.recieverName =
            (dataDetail.upk && dataDetail.upk.name) ||
            (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
            (dataDetail.province && dataDetail.province.name);
        } else if (dataDetail.order) {
          dataDetail.senderName =
            (dataDetail.order.upkForData && dataDetail.order.upkForData.name) ||
            (dataDetail.order.sudinKabKotaForData &&
              dataDetail.order.sudinKabKotaForData.name) ||
            (dataDetail.order.provinceForData &&
              dataDetail.order.provinceForData.name);
        }
        dataDetail.recieverName =
          (dataDetail.upk && dataDetail.upk.name) ||
          (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
          (dataDetail.province && dataDetail.province.name);
        dataDetail.isAllowedToConfirm = false;
        if (
          dataDetail.approvalStatus === ENUM.LAST_APPROVAL_STATUS.DRAFT ||
          (dataDetail.approvalStatus ===
            ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION &&
            dataDetail.order)
        ) {
          dataDetail.isAllowedToConfirm =
            dataDetail.createdBy === user.id
              ? false
              : dataDetail.order.upkIdFor &&
                dataDetail.order.upkIdFor === user.upkId
              ? true
              : dataDetail.order.sudinKabKotaIdFor &&
                dataDetail.order.sudinKabKotaIdFor === user.sudinKabKotaId
              ? true
              : dataDetail.order.provinceIdFor &&
                dataDetail.order.provinceIdFor === user.provinceId
              ? true
              : false;

          if (
            !dataDetail.order.upkIdFor &&
            !dataDetail.order.sudinKabKotaIdFor &&
            !dataDetail.order.provinceIdFor &&
            user.logisticrole === ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY
          ) {
            dataDetail.isAllowedToConfirm = true;
          }
        } else if (
          dataDetail.approvalStatus ===
            ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL &&
          dataDetail.planStatus === ENUM.PLAN_STATUS.PROCESSED
        ) {
          dataDetail.isAllowedToConfirm =
            dataDetail.upkId &&
            dataDetail.upkId === user.upkId &&
            dataDetail.logisticRole === user.logisticrole
              ? true
              : dataDetail.sudinKabKotaId &&
                dataDetail.sudinKabKotaId === user.sudinKabKotaId &&
                dataDetail.logisticRole === user.logisticrole
              ? true
              : dataDetail.provinceId &&
                dataDetail.provinceId === user.provinceId &&
                dataDetail.logisticRole === user.logisticrole
              ? true
              : false;
        }
        delete dataDetail.order;
      }

      res.status(200).json({
        status: 200,
        message: "success",
        data: dataDetail
      });
    } catch (err) {
      next(err);
    }
  };

  const getPlanDetailsByCode = async (req, res, next) => {
    const { user } = req;
    const { droppingNumber } = req.body;
    try {
      if (!droppingNumber) {
        throw { message: "droppingNumber is undefined" };
      }
      var planDetails = await DistPlan.findOne({
        where: { droppingNumber: droppingNumber },
        include: [
          {
            model: DistPlanItem,
            attributes: [
              ["id", "distPlanItemId"],
              "orderItemId",
              "packageUnitType",
              "packageQuantity",
              "expiredDate",
              "notes"
            ],
            include: [
              {
                model: Medicine,
                attributes: ["name", "codeName"]
              },
              {
                model: Inventory,
                attributes: ["batchCode"]
              }
            ]
          },
          {
            model: Order,
            attributes: ["id", "lastOrderStatus", "lastApprovalStatus"],
            include: [
              {
                model: Upk,
                required: false,
                as: "upkOwnerData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaOwnerData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceOwnerData",
                attributes: ["name"]
              },
              {
                model: Upk,
                required: false,
                as: "upkForData",
                attributes: ["name"]
              },
              {
                model: SudinKabKota,
                required: false,
                as: "sudinKabKotaForData",
                attributes: ["name"]
              },
              {
                model: Province,
                required: false,
                as: "provinceForData",
                attributes: ["name"]
              }
            ]
          }
        ].concat(QueryService.includeCreated())
      });
      var dataDetail = [];
      if (planDetails) {
        dataDetail = planDetails.toJSON();
        if (dataDetail.distType) {
          dataDetail.senderName = dataDetail.entityName;
          dataDetail.recieverName =
            (dataDetail.upk && dataDetail.upk.name) ||
            (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
            (dataDetail.province && dataDetail.province.name);
        } else if (dataDetail.order) {
          dataDetail.senderName =
            (dataDetail.order.upkForData && dataDetail.order.upkForData.name) ||
            (dataDetail.order.sudinKabKotaForData &&
              dataDetail.order.sudinKabKotaForData.name) ||
            (dataDetail.order.provinceForData &&
              dataDetail.order.provinceForData.name);
        }
        dataDetail.recieverName =
          (dataDetail.upk && dataDetail.upk.name) ||
          (dataDetail.sudinKabKotum && dataDetail.sudinKabKotum.name) ||
          (dataDetail.province && dataDetail.province.name);
        delete dataDetail.order;
      } else {
        throw { message: "Data " + droppingNumber + " not found", status: 404 };
      }

      res.status(200).json({
        status: 200,
        message: "success",
        data: dataDetail
      });
    } catch (err) {
      next(err);
    }
  };

  const createPlanForAllUpk = async (req, res, next) => {
    let { page, limit } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    const { distributionPlans, distributionPlansItems } = req.body;
    try {
      const listUpk = await Upk.findAndCountAll({
        limit,
        offset: limit * page,
        order: [["id", "ASC"]]
      });

      if (listUpk) {
        listUpk.rows.map(async resultItem => {
          var upkDetail = resultItem.toJSON();
          var totalPriceAll = 0;
          for (i in distributionPlansItems) {
            const currentBrand = await Brand.findByPk(
              distributionPlansItems[i].brandId,
              {
                attributes: ["packageMultiplier"],
                include: {
                  model: Medicine,
                  attributes: ["stockUnitType", "packageUnitType", "unitPrice"]
                }
              }
            );
            const totalPriceItem =
              parseFloat(distributionPlansItems[i].packageQuantity) *
              (parseFloat(currentBrand.medicine.unitPrice) || parseFloat(0));
            totalPriceAll += parseFloat(totalPriceItem);
          }
          var createdPlan = await DistPlan.create({
            distType: distributionPlans.distType,
            entityName: distributionPlans.entityName,
            droppingNumber: distributionPlans.droppingNumber,
            fundSource: ENUM.fUND_SOURCE.APBN,
            planStatus: ENUM.PLAN_STATUS.ACCEPTED_FULLY,
            approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
            totalPrice: parseFloat(totalPriceAll),
            logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            provinceId: upkDetail.provinceId,
            sudinKabKotaId: upkDetail.sudinKabKotaId,
            upkId: upkDetail.id
          });
          const receiptNumber = "REC" + distributionPlans.droppingNumber + "/1";
          const createdReceipt = await Receipt.create({
            distributionPlanId: createdPlan.id,
            receiptNumber: receiptNumber,
            logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
            receiptStatus: ENUM.RECEIPT_STATUS.ACCEPTED_FULLY,
            provinceId: upkDetail.provinceId,
            sudinKabKotaId: upkDetail.sudinKabKotaId,
            upkId: upkDetail.id,
            receivedAt: new Date().getDate()
          });

          for (i in distributionPlansItems) {
            const currentBrand = await Brand.findByPk(
              distributionPlansItems[i].brandId,
              {
                attributes: ["packageMultiplier", "medicineId"],
                include: {
                  model: Medicine,
                  attributes: [
                    ["id", "medicineId"],
                    "stockUnitType",
                    "packageUnitType",
                    "unitPrice",
                    "fundSource"
                  ]
                }
              }
            );
            const totalPriceItem =
              parseFloat(distributionPlansItems[i].packageQuantity) *
              (parseFloat(currentBrand.medicine.unitPrice) || 0);

            distributionPlansItems[i].distributionPlanId = createdPlan.id;
            distributionPlansItems[i].inventoryIdFor =
              distributionPlansItems[i].inventoryId;
            distributionPlansItems[i].packageUnitType =
              currentBrand.medicine.packageUnitType;
            distributionPlansItems[i].unitPrice =
              parseFloat(currentBrand.medicine.unitPrice) || parseFloat(0);
            distributionPlansItems[i].totalPrice = parseFloat(totalPriceItem);
            const newDistPlanItem = await DistPlanItem.create(
              distributionPlansItems[i]
            );

            const newReceiptItem = await ReceiptItem.create({
              batchCode: distributionPlansItems[i].batchCode,
              receiptItemStatus: ENUM.RECEIPT_ITEM_STATUS.NOT_CHANGED,
              receivedExpiredDate: distributionPlansItems[i].expiredDate,
              receivedPkgUnitType: currentBrand.medicine.packageUnitType,
              receivedPkgQuantity: distributionPlansItems[i].packageQuantity,
              logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
              receiptId: createdReceipt.id,
              brandId: distributionPlansItems[i].brandId
            });

            const newInventory = await Inventory.create({
              receiptItemId: newReceiptItem.id,
              batchCode: distributionPlansItems[i].batchCode,
              expiredDate: distributionPlansItems[i].expiredDate,
              packageUnitType: currentBrand.medicine.packageUnitType,
              packageQuantity: distributionPlansItems[i].packageQuantity,
              stockUnit: currentBrand.medicine.stockUnitType,
              logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY,
              medicineId: currentBrand.dataValues.medicineId,
              fundSource: distributionPlansItems[i].fundSource
                ? distributionPlansItems[i].fundSource
                : null,
              packagePrice: distributionPlansItems[i].packagePrice
                ? distributionPlansItems[i].packagePrice
                : null,
              manufacture: distributionPlansItems[i].manufacture
                ? distributionPlansItems[i].manufacture
                : null,
              stockQty:
                distributionPlansItems[i].packageQuantity *
                currentBrand.packageMultiplier,
              provinceId: upkDetail.provinceId,
              sudinKabKotaId: upkDetail.sudinKabKotaId,
              upkId: upkDetail.id,
              receiptId: createdReceipt.id,
              brandId: distributionPlansItems[i].brandId
            });
            await DistPlanItem.update(
              {
                inventoryIdFor: newInventory.id
              },
              {
                where: {
                  id: newDistPlanItem.id
                }
              }
            );
          }
        });
      }
      res.status(201).json({
        status: 201,
        message: "created",
        data: listUpk
      });
    } catch (err) {
      next(err);
    }
  };

  const createPlan = async (req, res, next) => {
    const { user } = req;
    const { orders } = req.query;
    const { distributionPlans, distributionPlansItems } = req.body;
    if (orders && orders === "FALSE") {
      try {
        var totalPriceAll = 0;
        for (i in distributionPlansItems) {
          const currentBrand = await Brand.findByPk(
            distributionPlansItems[i].brandId,
            {
              attributes: ["packageMultiplier"],
              include: {
                model: Medicine,
                attributes: ["stockUnitType", "packageUnitType", "unitPrice"]
              }
            }
          );
          const totalPriceItem =
            parseFloat(distributionPlansItems[i].packageQuantity) *
            (parseFloat(currentBrand.medicine.unitPrice) || parseFloat(0));
          totalPriceAll += parseFloat(totalPriceItem);
        }
        var createdPlan = await DistPlan.create({
          distType: distributionPlans.distType,
          entityName: distributionPlans.entityName,
          droppingNumber: distributionPlans.droppingNumber,
          fundSource: ENUM.fUND_SOURCE.APBN,
          planStatus: ENUM.PLAN_STATUS.ACCEPTED_FULLY,
          approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
          totalPrice: parseFloat(totalPriceAll),
          logisticRole: user.logisticrole,
          provinceId: user.provinceId,
          sudinKabKotaId: user.sudinKabKotaId,
          upkId: user.upkId,
          createdBy: user.id,
          updatedBy: user.id
        });
        const receiptNumber = "REC" + distributionPlans.droppingNumber + "/1";
        const createdReceipt = await Receipt.create({
          distributionPlanId: createdPlan.id,
          receiptNumber: receiptNumber,
          logisticRole: user.logisticrole,
          receiptStatus: ENUM.RECEIPT_STATUS.ACCEPTED_FULLY,
          provinceId: user.provinceId,
          sudinKabKotaId: user.sudinKabKotaId,
          upkId: user.upkId,
          receivedAt: new Date().getDate(),
          receivedBy: user.id,
          createdBy: user.id,
          updatedBy: user.id
        });

        for (i in distributionPlansItems) {
          const currentBrand = await Brand.findByPk(
            distributionPlansItems[i].brandId,
            {
              attributes: ["packageMultiplier", "medicineId"],
              include: {
                model: Medicine,
                attributes: [
                  ["id", "medicineId"],
                  "stockUnitType",
                  "packageUnitType",
                  "unitPrice",
                  "fundSource"
                ]
              }
            }
          );
          const totalPriceItem =
            parseFloat(distributionPlansItems[i].packageQuantity) *
            (parseFloat(currentBrand.medicine.unitPrice) || 0);

          distributionPlansItems[i].distributionPlanId = createdPlan.id;
          distributionPlansItems[i].createdBy = user.id;
          distributionPlansItems[i].inventoryIdFor =
            distributionPlansItems[i].inventoryId;
          distributionPlansItems[i].packageUnitType =
            currentBrand.medicine.packageUnitType;
          distributionPlansItems[i].unitPrice =
            parseFloat(currentBrand.medicine.unitPrice) || parseFloat(0);
          distributionPlansItems[i].totalPrice = parseFloat(totalPriceItem);
          const newDistPlanItem = await DistPlanItem.create(
            distributionPlansItems[i]
          );

          const newReceiptItem = await ReceiptItem.create({
            batchCode: distributionPlansItems[i].batchCode,
            receiptItemStatus: ENUM.RECEIPT_ITEM_STATUS.NOT_CHANGED,
            receivedExpiredDate: distributionPlansItems[i].expiredDate,
            receivedPkgUnitType: currentBrand.medicine.packageUnitType,
            receivedPkgQuantity: distributionPlansItems[i].packageQuantity,
            createdBy: user.id,
            logisticRole: user.logisticrole,
            receiptId: createdReceipt.id,
            brandId: distributionPlansItems[i].brandId
          });

          const newInventory = await Inventory.create({
            receiptItemId: newReceiptItem.id,
            batchCode: distributionPlansItems[i].batchCode,
            expiredDate: distributionPlansItems[i].expiredDate,
            packageUnitType: currentBrand.medicine.packageUnitType,
            packageQuantity: distributionPlansItems[i].packageQuantity,
            stockUnit: currentBrand.medicine.stockUnitType,
            logisticRole: user.logisticrole,
            medicineId: currentBrand.dataValues.medicineId,
            fundSource: distributionPlansItems[i].fundSource
              ? distributionPlansItems[i].fundSource
              : null,
            packagePrice: distributionPlansItems[i].packagePrice
              ? distributionPlansItems[i].packagePrice
              : null,
            manufacture: distributionPlansItems[i].manufacture
              ? distributionPlansItems[i].manufacture
              : null,
            stockQty:
              distributionPlansItems[i].packageQuantity *
              currentBrand.packageMultiplier,
            provinceId: user.provinceId,
            sudinKabKotaId: user.sudinKabKotaId,
            upkId: user.upkId,
            createdBy: user.id,
            receiptId: createdReceipt.id,
            brandId: distributionPlansItems[i].brandId
          });
          await DistPlanItem.update(
            {
              inventoryIdFor: newInventory.id
            },
            {
              where: {
                id: newDistPlanItem.id
              }
            }
          );
        }

        res.status(201).json({
          status: 201,
          message: "created",
          data: createdPlan
        });
      } catch (err) {
        next(err);
      }
    } else {
      const lastDist = await DistPlan.findAll({
        where: {
          orderId: distributionPlans.orderId
        }
      });
      const orderDetail = await Order.findOne({
        where: {
          id: distributionPlans.orderId
        }
      });
      const droppingNumber = await QueryService.generateDroppingNumber({
        isRegular: orderDetail.isRegular,
        isDistKhusus: false,
        logisticRole: orderDetail.logisticRole,
        upkId: orderDetail.upkIdOwner,
        sudinKabKotaId: orderDetail.sudinKabKotaIdOwner,
        provinceId: orderDetail.provinceIdOwner
      });
      try {
        var totalPriceAll = 0;
        for (i in distributionPlansItems) {
          const currentInventory = await Inventory.findByPk(
            distributionPlansItems[0].inventoryId,
            {
              include: {
                model: Brand,
                include: {
                  model: Medicine,
                  attributes: ["stockUnitType", "packageUnitType", "unitPrice"]
                }
              }
            }
          );
          const totalPriceItem =
            parseFloat(distributionPlansItems[i].packageQuantity) *
            (parseFloat(currentInventory.brand.medicine.unitPrice) ||
              parseFloat(0));
          totalPriceAll += totalPriceItem;
        }
        var createdPlan = await DistPlan.create({
          orderId: distributionPlans.orderId,
          droppingNumber: droppingNumber,
          fundSource: ENUM.fUND_SOURCE.NON_APBN,
          planStatus: ENUM.PLAN_STATUS.DRAFT,
          approvalStatus: ENUM.LAST_APPROVAL_STATUS.DRAFT,
          totalPrice: parseFloat(totalPriceAll),
          logisticRole: orderDetail.logisticRole,
          provinceId: orderDetail.provinceIdOwner,
          sudinKabKotaId: orderDetail.sudinKabKotaIdOwner,
          upkId: orderDetail.upkIdOwner,

          logisticRoleSender: user.logisticrole,
          upkIdSender: user.upkId,
          provinceIdSender: user.provinceId,
          sudinKabKotaIdSender: user.sudinKabKotaId,

          createdBy: user.id,
          updatedBy: user.id
        });
        for (i in distributionPlansItems) {
          const currentInventory = await Inventory.findByPk(
            distributionPlansItems[0].inventoryId,
            {
              include: {
                model: Brand,
                include: {
                  model: Medicine,
                  attributes: ["stockUnitType", "packageUnitType", "unitPrice"]
                }
              }
            }
          );
          const totalPriceItem =
            parseFloat(distributionPlansItems[i].packageQuantity) *
            (parseFloat(currentInventory.brand.medicine.unitPrice) ||
              parseFloat(0));

          distributionPlansItems[i].unitPrice =
            parseFloat(currentInventory.brand.medicine.unitPrice) ||
            parseFloat(0);
          distributionPlansItems[i].totalPrice = parseFloat(totalPriceItem);

          distributionPlansItems[i].distributionPlanId = createdPlan.id;
          distributionPlansItems[i].brandId = currentInventory.brand.id;
          distributionPlansItems[i].medicineId =
            currentInventory.brand.medicineId;
          distributionPlansItems[i].packageUnitType =
            currentInventory.packageUnitType;
          distributionPlansItems[i].expiredDate = currentInventory.expiredDate;
          distributionPlansItems[i].createdBy = user.id;
          await DistPlanItem.create(distributionPlansItems[i]);
        }
        res.status(201).json({
          status: 201,
          message: "created",
          data: createdPlan
        });
      } catch (err) {
        next(err);
      }
    }
  };

  const createDistribution = async (req, res, next) => {
    const { user } = req;
    const { distributionPlans, distributionPlansItems } = req.body;
    try {
      var totalPriceAll = 0;
      for (i in distributionPlansItems) {
        const currentInventory = await Inventory.findOne({
          where: {
            id: distributionPlansItems[i].inventoryId
          },
          include: {
            model: Brand,
            include: {
              model: Medicine,
              attributes: ["stockUnitType", "packageUnitType", "unitPrice"]
            }
          }
        });
        const totalPriceItem =
          parseFloat(distributionPlansItems[i].packageQuantity) *
          (parseFloat(currentInventory.brand.medicine.unitPrice) || 0);
        totalPriceAll += parseFloat(totalPriceItem);
      }
      const logisticRole = distributionPlans.logisticRole
        ? distributionPlans.logisticRole
        : distributionPlans.upkId
        ? ENUM.LOGISTIC_ROLE.UPK_ENTITY
        : distributionPlans.sudinKabKotaId
        ? ENUM.LOGISTIC_ROLE.SUDIN_ENTITY
        : distributionPlans.provinceId
        ? ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY
        : null;
      let droppingNumber = await QueryService.generateDroppingNumber({
        isRegular: false,
        isDistKhusus: true,
        logisticRole: logisticRole,
        upkId: distributionPlans.upkId,
        sudinKabKotaId: distributionPlans.sudinKabKotaId,
        provinceId: distributionPlans.provinceId
      });
      var createdPlan = await DistPlan.create({
        droppingNumber: droppingNumber,
        notes: distributionPlans.notes,
        fundSource: ENUM.fUND_SOURCE.APBN,
        planStatus: ENUM.PLAN_STATUS.PROCESSED,
        approvalStatus: ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL,
        totalPrice: parseFloat(totalPriceAll),
        logisticRole: logisticRole,
        provinceId: distributionPlans.provinceId,
        sudinKabKotaId: distributionPlans.sudinKabKotaId,
        upkId: distributionPlans.upkId,

        logisticRoleSender: user.logisticrole,
        upkIdSender: user.upkId,
        provinceIdSender: user.provinceId,
        sudinKabKotaIdSender: user.sudinKabKotaId,

        createdBy: user.id,
        updatedBy: user.id
      });

      for (i in distributionPlansItems) {
        const currentInventory = await Inventory.findByPk(
          distributionPlansItems[i].inventoryId,
          {
            include: {
              model: Brand,
              include: {
                model: Medicine,
                attributes: ["stockUnitType", "packageUnitType", "unitPrice"]
              }
            }
          }
        );
        const totalPriceItem =
          parseFloat(distributionPlansItems[i].packageQuantity) *
          (parseFloat(currentInventory.brand.medicine.unitPrice) || 0);
        totalPriceAll += parseFloat(totalPriceItem);

        distributionPlansItems[i].distributionPlanId = createdPlan.id;
        distributionPlansItems[i].createdBy = user.id;
        distributionPlansItems[i].expiredDate = currentInventory.expiredDate;
        distributionPlansItems[i].medicineId =
          currentInventory.brand.medicineId;
        distributionPlansItems[i].brandId = currentInventory.brand.id;
        distributionPlansItems[i].unitPrice =
          parseFloat(currentInventory.brand.medicine.unitPrice) ||
          parseFloat(0);
        distributionPlansItems[i].totalPrice = parseFloat(totalPriceItem);
        (distributionPlansItems[i].fundSource =
          currentInventory.brand.medicine.fundSource),
          await DistPlanItem.create(distributionPlansItems[i]);

        var packageQuantity =
          currentInventory.packageQuantity -
          distributionPlansItems[i].packageQuantity;
        if (packageQuantity < 0) {
          throw {
            message:
              "Validation Error, stocks aren't sufficient" +
              currentInventory.brand.name
          };
        }
        var stockQty =
          currentInventory.stockQty -
          distributionPlansItems[i].packageQuantity *
            currentInventory.brand.packageMultiplier;
        await Inventory.update(
          {
            packageQuantity: packageQuantity,
            stockQty: stockQty
          },
          {
            where: {
              id: distributionPlansItems[i].inventoryId
            }
          }
        );
      }
      res.status(201).json({
        status: 201,
        message: "created",
        data: createdPlan
      });
    } catch (err) {
      next(err);
    }
  };

  const updateDistribution = async (req, res, next) => {
    const { user } = req;
    const { distributionPlanId } = req.params;
    const { distributionPlansItems } = req.body;

    try {
      var plan = await DistPlan.findByPk(distributionPlanId, {
        include: {
          model: DistPlanItem
        }
      });

      if (plan.planStatus !== ENUM.PLAN_STATUS.DRAFT) {
        throw { message: "Can't be updated, data is being processed" };
      }

      const dataInsert = differenceBy(
        distributionPlansItems,
        plan.distribution_plans_items,
        "id"
      );
      const dataUpdate = intersectionBy(
        distributionPlansItems,
        plan.distribution_plans_items,
        "id"
      );
      const dataDelete = differenceBy(
        plan.distribution_plans_items,
        distributionPlansItems,
        "id"
      );

      for (i in dataInsert) {
        const inventoryId = dataInsert[i].inventoryId;
        const currentInventory = await Inventory.findByPk(inventoryId, {
          include: {
            model: Brand
          }
        });

        dataInsert[i].distributionPlanId = distributionPlanId;
        dataInsert[i].medicineId = currentInventory.brand.medicineId;
        dataInsert[i].packageUnitType = currentInventory.packageUnitType;
        dataInsert[i].expiredDate = currentInventory.expiredDate;
        dataInsert[i].createdBy = user.id;
        delete dataInsert[i].id;
        await DistPlanItem.create(dataInsert[i]);
      }

      for (i in dataUpdate) {
        const inventoryId = dataInsert[i].inventoryId;
        const currentInventory = await Inventory.findByPk(inventoryId, {
          include: {
            model: Brand
          }
        });

        await DistPlanItem.update(
          {
            packageQuantity: dataUpdate[i].packageQuantity,
            medicineId: currentInventory.brand.medicineId,
            inventoryId: dataUpdate[i].inventoryId,
            notes: dataUpdate[i].notes,
            updatedBy: user.id
          },
          {
            where: {
              id: dataUpdate[i].id
            }
          }
        );
      }

      for (i in dataDelete) {
        await DistPlanItem.update(
          {
            deletedAt: new Date()
          },
          {
            where: {
              id: dataDelete[i].id
            }
          }
        );
      }

      res.status(200).json({
        status: 200,
        data: distributionPlansItems,
        message: "success"
      });
    } catch (err) {
      next(err);
    }
  };

  const confirmDistribution = async (req, res, next) => {
    const { user } = req;
    const { distributionPlanId } = req.params;
    const { status } = req.query;
    const { notes, distributionPlansItems } = req.body;
    try {
      if (
        status !== ENUM.PLAN_STATUS.ACCEPTED_FULLY &&
        status !== ENUM.PLAN_STATUS.LOST &&
        status !== ENUM.PLAN_STATUS.ACCEPTED_DIFFERENT
      ) {
        throw { message: "Validation Error, status invalid" };
      }
      var plan = await DistPlan.findByPk(distributionPlanId, {
        include: {
          model: DistPlanItem
        }
      });
      if (
        plan.planStatus === ENUM.PLAN_STATUS.ACCEPTED_FULLY ||
        plan.planStatus === ENUM.PLAN_STATUS.LOST ||
        plan.planStatus === ENUM.PLAN_STATUS.ACCEPTED_DIFFERENT
      ) {
        throw { message: "Validation Error, data has been processed" };
      }
      await DistPlan.update(
        {
          planStatus: status,
          updatedBy: user.id
        },
        {
          where: {
            id: distributionPlanId
          }
        }
      );
      var receivedAt = new Date();
      var receiptNumber;
      if (plan.droppingNumber !== null) {
        receiptNumber = "REC".concat(plan.droppingNumber, "/", plan.orderId);
      }
      const createdReceipt = await Receipt.create({
        distributionPlanId: distributionPlanId,
        receiptNumber: receiptNumber,
        logisticRole: user.logisticrole,
        receiptStatus: status,
        provinceId: plan.provinceId,
        sudinKabKotaId: plan.sudinKabKotaId,
        upkId: plan.upkId,
        notes: notes ? notes : null,
        receivedAt: receivedAt.getDate(),
        receivedBy: user.id,
        createdBy: user.id,
        updatedBy: user.id
      });
      if (
        status === ENUM.PLAN_STATUS.ACCEPTED_FULLY ||
        status === ENUM.PLAN_STATUS.LOST
      ) {
        var planItems = await DistPlanItem.findAll({
          where: {
            distributionPlanId: distributionPlanId
          }
        });
        for (i in planItems) {
          var currentInventory = await Inventory.findOne({
            where: {
              id: planItems[i].inventoryId
            },
            include: [
              {
                model: Brand,
                include: [
                  {
                    model: Medicine
                  }
                ]
              }
            ]
          });
          const newReceiptItem = await ReceiptItem.create({
            batchCode: currentInventory.batchCode,
            receiptItemStatus:
              status === ENUM.PLAN_STATUS.ACCEPTED_FULLY
                ? ENUM.RECEIPT_ITEM_STATUS.NOT_CHANGED
                : ENUM.RECEIPT_ITEM_STATUS.LESS_THAN_DEMAND,
            receivedExpiredDate: currentInventory.expiredDate,
            receivedPkgUnitType: currentInventory.packageUnitType,
            receivedPkgQuantity:
              status === ENUM.PLAN_STATUS.ACCEPTED_FULLY
                ? planItems[i].packageQuantity
                : 0,
            createdBy: user.id,
            logisticRole: user.logisticrole,
            receiptId: createdReceipt.id,
            brandId: currentInventory.brandId
          });
          if (status === ENUM.PLAN_STATUS.ACCEPTED_FULLY) {
            let packageQuantityInv =
              status === ENUM.PLAN_STATUS.ACCEPTED_FULLY
                ? planItems[i].packageQuantity
                : distributionPlansItems[i].requiredPackageQuantity;
            let stockQty =
              status === ENUM.PLAN_STATUS.ACCEPTED_FULLY
                ? planItems[i].packageQuantity *
                  currentInventory.brand.packageMultiplier
                : distributionPlansItems[i].requiredPackageQuantity *
                  currentInventory.brand.packageMultiplier;
            await Inventory.create({
              receiptItemId: newReceiptItem.id,
              batchCode: currentInventory.batchCode,
              expiredDate: currentInventory.expiredDate,
              packageUnitType: currentInventory.packageUnitType,
              packageQuantity: packageQuantityInv,
              medicineId: currentInventory.brand.medicineId,
              logisticRole: user.logisticrole,
              stockUnit: currentInventory.brand.medicine.stockUnitType,
              stockQty: stockQty,
              fundSource: currentInventory.fundSource,
              packagePrice: currentInventory.packagePrice,
              manufacture: currentInventory.manufacture,
              provinceId: user.provinceId,
              sudinKabKotaId: user.sudinKabKotaId,
              upkId: user.upkId,
              createdBy: user.id,
              receiptId: createdReceipt.id,
              brandId: currentInventory.brandId
            });
          }
          await DistPlanItem.update(
            {
              actualRecievedPackageQuantity: ENUM.PLAN_STATUS.ACCEPTED_FULLY
                ? planItems[i].requiredPackageQuantity
                : 0,
              updatedBy: user.id
            },
            {
              where: {
                id: planItems[i].id
              }
            }
          );
        }
      } else if (status === ENUM.PLAN_STATUS.ACCEPTED_DIFFERENT) {
        for (i in distributionPlansItems) {
          var planItems = await DistPlanItem.findOne({
            where: {
              id: distributionPlansItems[i].id
            }
          });
          var currentInventory = await Inventory.findOne({
            where: {
              id: planItems.inventoryId
            },
            include: [
              {
                model: Brand,
                include: [
                  {
                    model: Medicine
                  }
                ]
              }
            ]
          });
          var receiptItemStatus = ENUM.RECEIPT_ITEM_STATUS.NOT_CHANGED;
          if (
            distributionPlansItems[i].actualRecievedPackageQuantity >
            planItems.packageQuantity
          ) {
            receiptItemStatus = ENUM.RECEIPT_ITEM_STATUS.MORE_THAN_DEMAND;
          } else if (
            distributionPlansItems[i].actualRecievedPackageQuantity <
            planItems.packageQuantity
          ) {
            receiptItemStatus = ENUM.RECEIPT_ITEM_STATUS.LESS_THAN_DEMAND;
          }
          const newReceiptItem = await ReceiptItem.create({
            batchCode: currentInventory.batchCode,
            receiptItemStatus: receiptItemStatus,
            receivedExpiredDate: currentInventory.expiredDate,
            receivedPkgUnitType: currentInventory.packageUnitType,
            receivedPkgQuantity:
              distributionPlansItems[i].actualRecievedPackageQuantity,
            createdBy: user.id,
            logisticRole: user.logisticrole,
            receiptId: createdReceipt.id,
            brandId: currentInventory.brandId
          });
          await Inventory.create({
            receiptItemId: newReceiptItem.id,
            batchCode: currentInventory.batchCode,
            expiredDate: currentInventory.expiredDate,
            packageUnitType: currentInventory.packageUnitType,
            packageQuantity:
              distributionPlansItems[i].actualRecievedPackageQuantity,
            stockUnit: currentInventory.stockUnit,
            stockUnit: currentInventory.brand.medicine.stockUnitType,
            medicineId: currentInventory.brand.medicineId,
            logisticRole: user.logisticrole,
            stockQty:
              distributionPlansItems[i].actualRecievedPackageQuantity *
              currentInventory.brand.packageMultiplier,
            fundSource: currentInventory.fundSource,
            packagePrice: currentInventory.packagePrice,
            manufacture: currentInventory.manufacture,
            provinceId: user.provinceId,
            sudinKabKotaId: user.sudinKabKotaId,
            upkId: user.upkId,
            createdBy: user.id,
            receiptId: createdReceipt.id,
            brandId: currentInventory.brandId
          });
          await DistPlanItem.update(
            {
              actualRecievedPackageQuantity:
                distributionPlansItems[i].actualRecievedPackageQuantity,
              updatedBy: user.id
            },
            {
              where: {
                id: planItems.id
              }
            }
          );
        }
      }

      res.status(201).json({
        status: 201,
        message: "created"
      });
    } catch (err) {
      next(err);
    }
  };

  const approvalDistribution = async (req, res, next) => {
    const { user } = req;
    const { distributionPlanId } = req.params;
    const { status } = req.query;
    const { note } = req.body;
    try {
      const lastDistPlan = await DistPlan.findByPk(distributionPlanId, {
        include: [
          {
            model: Order,
            attributes: [
              ["id", "orderId"],
              "lastOrderStatus",
              "lastApprovalStatus"
            ]
          }
        ]
      });
      if (user.id === lastDistPlan.createdBy) {
        throw { message: "Validation Error, You have no authority" };
      }
      if (status && lastDistPlan) {
        if (
          lastDistPlan.approvalStatus === ENUM.LAST_APPROVAL_STATUS.DRAFT &&
          status !== ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION
        ) {
          throw { message: "Validation Error, can't change to IN_EVALUATION" };
        }
        if (
          lastDistPlan.approvalStatus ===
            ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION &&
          status !== ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL &&
          status !== ENUM.LAST_APPROVAL_STATUS.REJECTED
        ) {
          throw { message: "Validation Error, can't change to " + status };
        }
        if (
          lastDistPlan.approvalStatus ===
            ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL ||
          lastDistPlan.approvalStatus === ENUM.LAST_APPROVAL_STATUS.REJECTED
        ) {
          throw { message: "Validation Error, data has been processed" };
        }
        var planStatus = ENUM.DIST_STATUS_STATE[status];
        var approvalStatus = status;
        var approvalNotes = lastDistPlan.approvalNotes
          ? lastDistPlan.approvalNotes + "\n"
          : "";
        approvalNotes += user.name + ": " + (note ? note : "-");
        if (status === ENUM.LAST_APPROVAL_STATUS.APPROVED_FULL) {
          var planItems = await DistPlanItem.findAll({
            where: {
              distributionPlanId: distributionPlanId
            }
          });
          var inventoryUpdated = [];
          for (i in planItems) {
            var currentInventory = await Inventory.findOne({
              where: {
                id: planItems[i].inventoryId
              },
              include: [
                {
                  model: Brand
                }
              ]
            });
            var packageQuantity =
              currentInventory.packageQuantity - planItems[i].packageQuantity;

            if (packageQuantity < 0) {
              throw {
                message:
                  "Validation Error, stocks aren't sufficient" +
                  currentInventory.brand.name
              };
            }
            var stockQty =
              currentInventory.stockQty -
              planItems[i].packageQuantity *
                currentInventory.brand.packageMultiplier;

            inventoryUpdated.push({
              id: planItems[i].inventoryId,
              packageQuantity: packageQuantity,
              stockQty: stockQty
            });
          }
        }
        for (i in inventoryUpdated) {
          await Inventory.update(
            {
              packageQuantity: inventoryUpdated[i].packageQuantity,
              stockQty: inventoryUpdated[i].stockQty
            },
            {
              where: {
                id: inventoryUpdated[i].id
              }
            }
          );
        }
        await DistPlan.update(
          {
            planStatus: planStatus,
            approvalStatus: approvalStatus,
            approvalNotes: approvalNotes
          },
          {
            where: {
              id: distributionPlanId
            }
          }
        );
      }
      if (
        lastDistPlan.order &&
        lastDistPlan.order.dataValues.lastOrderStatus ===
          ENUM.LAST_ORDER_STATUS.PROCESSED
      ) {
        await Order.update(
          {
            lastOrderStatus: ENUM.LAST_ORDER_STATUS.EXECUTED
          },
          {
            where: {
              id: lastDistPlan.order.dataValues.orderId
            }
          }
        );
      }
      res.send({
        status: 200,
        message: "Success",
        data: null
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    getDelivery,
    getPlans,
    getPlanDetails,
    getPlanDetailsByCode,
    confirmDistribution,
    approvalDistribution,
    createPlanForAllUpk,
    createPlan,
    createDistribution,
    updateDistribution
  };
};

module.exports = DistributionController;
