const Sequelize = require("sequelize");
const sequelize = require("../../config/database");
const Inventory = require("../models/Inventory");
const Medicine = require("../models/Medicine");
const UsageVlCd4 = require("../models/UsageVlCd4");
const UsageVlCd4Medicine = require("../models/UsageVlCd4Medicine");
// const ENUM = require("../../config/enum");

const Op = Sequelize.Op;

const LabController = () => {
    const getData = async (req, res, next) => {
        try {
            const user = req.user;
            const { id } = req.query;

            let where = {
                upkId: user.upkId
            }

            if (id) {
                where = {
                    ...where,
                    id: id
                }
            }

            let data = await UsageVlCd4.findAll({
                include: [
                    {
                        model: UsageVlCd4Medicine,
                        required: false,
                        include: [
                            {
                                model: Medicine,
                                required: false
                            }
                        ]
                    }
                ],
                where: where,
                order: [
                    ["date", "DESC"]
                ]
            });

            if (data.length == 1 && id) {
                data = data[0];
            }

            res.json({
                data: data
            });
        } catch (error) {
            next(error);
        }
    };

    const saveData = async (req, res, next) => {
        try {
            const user = req.user;
            const { id } = req.query;
            const body = req.body;
            let data = {
                testType: body.testType,
                reagentType: body.reagentType,
                date: body.date || null,
                upkId: user.upkId,
                examination: body.examination,
            }
            let record;

            if (!id) {
                data.createdBy = user.id;

                record = await UsageVlCd4.create(data);
            } else {
                await UsageVlCd4Medicine.destroy({
                    where: {
                        usageVlCd4Id: id
                    },
                    force: true
                });

                record = await UsageVlCd4.findByPk(id);

                data.updatedBy = user.id;
                Object.assign(record, data);

                record.save();
            }

            body.medicines.map(async r => {
                if (!id) { // kondisi tambah
                    const med = await Medicine.findOne({
                        include: [
                            {
                                model: Inventory
                            }
                        ],
                        where: {
                            id: r.medicineId
                        },
                        order: [
                            [Inventory, 'expiredDate', 'ASC']
                        ]
                    });
                    if (med) {
                        if (med.inventories) {
                            const inv = med.inventories[0];
                            let stockQty = inv.stockQty - r.amount;
                            let packageQuantity = parseInt(stockQty / med.packageMultiplier);

                            await Inventory.update(
                                {
                                    packageQuantity: packageQuantity,
                                    stockQty: stockQty,
                                },
                                {
                                    where: {
                                        id: inv.id
                                    }
                                }
                            );

                            r.inventoryId = inv.id;
                        }
                    }
                }

                r.usageVlCd4Id = record.id;
                await UsageVlCd4Medicine.create(r);
            })

            res.json({
                data: record
            });
        } catch (error) {
            next(error);
        }
    }

    const deleteData = async (req, res, next) => {
        try {
            const { id } = req.query;
            // await UsageVlCd4Medicine.destroy({
            //     where: {
            //         usageVlCd4Id: id
            //     },
            //     // force: true
            // });

            // let record = await UsageVlCd4.findByPk(id);

            // record.destroy();

            // ---------------------------------
            let data = await UsageVlCd4.findOne({
                include: [
                    {
                        model: UsageVlCd4Medicine,
                        include: [
                            {
                                model: Inventory,
                                include: [
                                    {
                                        model: Medicine
                                    }
                                ]
                            }
                        ]
                    }
                ],
                where: {
                    id: id
                }
            });

            data.usageVlCd4Medicines.map(async r => {
                const inv = r.inventory;
                const med = r.inventory.medicine;
                let stockQty = inv.stockQty + r.amount;
                let packageQuantity = parseInt(stockQty / med.packageMultiplier);

                await Inventory.update(
                    {
                        packageQuantity: packageQuantity,
                        stockQty: stockQty,
                    },
                    {
                        where: {
                            id: inv.id
                        }
                    }
                );
            })

            await UsageVlCd4Medicine.destroy({
                where: {
                    usageVlCd4Id: id
                },
                // force: true
            });

            data.destroy();
            // ---------------------------------

            res.json({
                success: true
            });
        } catch (error) {
            next(error);
        }
    }

    return { getData, saveData, deleteData };
}

module.exports = LabController;