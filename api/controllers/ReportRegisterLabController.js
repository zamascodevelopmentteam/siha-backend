const ENUM = require("../../config/enum");

const RegisterLab = require("../services/report/registerLab/registerLab.class");
const VariableService = require("../services/variable.service");

const controller = () => {
  const getRegisterLabReport = async (req, res, next, returnItem = false) => {
    const upkId = req.user.upkId;
    try {
      let month2 = null;
      let year2 = null;
      const { month } = req.query;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
      }
      const registerLab = new RegisterLab(req.user, req, month2, year2);
      await registerLab.buildReportPenggunaanKomoditasLab();
      await registerLab.buildReportInventoryUsage();

      const data = registerLab.getReport();

      if (returnItem) {
        return data;
      }

      res.json(data);
    } catch (error) {
      next(error);
    }
  };
  const getLbadbDetailReport = async (req, res, next) => {
    const upkId = req.user.upkId;
    try {
      let month2 = null;
      let year2 = null;
      const { month } = req.query;
      if (month) {
        month2 = month.split('-')[1];
        year2 = month.split('-')[0];
      }
      const registerLab = new RegisterLab(req.user, req, month2, year2);
      await registerLab.buildReportLbadbDetail();
      await registerLab.buildReportHasilTesLab();
      res.json(registerLab.getReport());
    } catch (error) {
      next(error);
    }
  };

  return {
    getRegisterLabReport,
    getLbadbDetailReport
  };
};

module.exports = controller;
