const InventoryService = require("../services/inventory.service")();
const Inventory = require("../models/Inventory");
const InventoryAdjustment = require("../models/InventoryAdjustment");
const InventoryKalibrations = require("../models/InventoryKalibrations");
const Brand = require("../models/Brand");
const Medicine = require("../models/Medicine");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const sequelize = require("../../config/database");

const QueryService = require("../services/query.service")();
const LogisticRole = require("../../config/logistic_role");
const CONSTANT = require("../../config/constant");
const ENUM = require("../../config/enum");

const InventoryController = () => {
  const getInventoryListStock = async (req, res, next) => {
    const { keyword, upkId, sudinKabKotaId, provinceId } = req.query;
    try {
      var InventoryList = [];
      if (upkId !== undefined && upkId) {
        val_type = "upkId";
        InventoryList = await InventoryService.getInventoryStock(
          "upk_id",
          upkId,
          keyword
        );
      } else if (sudinKabKotaId !== undefined && sudinKabKotaId) {
        val_type = "sudinKabKotaId";
        InventoryList = await InventoryService.getInventoryStock(
          "sudin_kab_kota_id",
          sudinKabKotaId,
          keyword
        );
      } else if (provinceId !== undefined && provinceId) {
        val_type = "provinceId";
        InventoryList = await InventoryService.getInventoryStock(
          "province_id",
          provinceId,
          keyword
        );
      } else {
        InventoryList = await InventoryService.getInventoryStock(
          null,
          null,
          keyword
        );
      }
      res.status(200).json({
        status: 200,
        message: "success",
        data: InventoryList
      });
    } catch (err) {
      next(err);
    }
  };

  const getInventoryPickingList = async (req, res, next) => {
    const { user } = req;
    const { medicineId, expiredDate } = req.params;
    try {
      var whereObj = [];
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          whereObj = {
            packageQuantity: {
              [Op.gt]: 0
            },
            expiredDate: {
              [Op.gte]: expiredDate
            },
            logisticRole: user.logisticrole,
            upkId: user.upkId
          };
          break;
        case "SUDIN_ENTITY":
          whereObj = {
            packageQuantity: {
              [Op.gt]: 0
            },
            expiredDate: {
              [Op.gte]: expiredDate
            },
            logisticRole: user.logisticrole,
            sudinKabKotaId: user.sudinKabKotaId
          };
          break;
        case "PROVINCE_ENTITY":
          whereObj = {
            packageQuantity: {
              [Op.gt]: 0
            },
            expiredDate: {
              [Op.gte]: expiredDate
            },
            logisticRole: user.logisticrole,
            provinceId: user.provinceId
          };
          break;
        case ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY:
          whereObj = {
            packageQuantity: {
              [Op.gt]: 0
            },
            expiredDate: {
              [Op.gte]: expiredDate
            },
            logisticRole: user.logisticrole
          };
          break;
      }

      const pickingList = await Inventory.findAll({
        where: whereObj,
        attributes: [
          ["id", "inventoryId"],
          "batchCode",
          "brandId",
          "expiredDate",
          "packageUnitType",
          "packageQuantity"
        ],
        include: [
          {
            model: Brand,
            attributes: [["name", "brandName"]],
            where: {
              medicineId: medicineId
            },
            include: [
              {
                model: Medicine,
                attributes: [
                  ["id", "medicineId"],
                  "codeName",
                  "fundSource",
                  "unitPrice"
                ]
              }
            ]
          }
        ],
        order: [
          ["expiredDate", "ASC"],
          ["createdAt", "DESC"]
        ]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: pickingList
      });
    } catch (err) {
      next(err);
    }
  };

  const getAllInventories = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    let medicineId = req.query.medicineId || req.params.medicineId;
    try {
      let whereMedicineParams = {};
      let whereParams = {};
      switch (req.user.logisticrole) {
        case LogisticRole.LAB_ENTITY:
          whereParams = {
            upkId: req.user.upkId,
            logisticRole: LogisticRole.LAB_ENTITY
          };
          break;
        case LogisticRole.PHARMA_ENTITY:
          whereParams = {
            upkId: req.user.upkId,
            logisticRole: LogisticRole.PHARMA_ENTITY
          };
          break;
        case LogisticRole.UPK_ENTITY:
          whereParams = {
            upkId: req.user.upkId,
            logisticRole: LogisticRole.UPK_ENTITY
          };
          break;
        case LogisticRole.SUDIN_ENTITY:
          whereParams = {
            sudinKabKotaId: req.user.sudinKabKotaId,
            logisticRole: LogisticRole.SUDIN_ENTITY
          };
          break;
        case LogisticRole.PROVINCE_ENTITY:
          whereParams = {
            provinceId: req.user.provinceId,
            logisticRole: LogisticRole.PROVINCE_ENTITY
          };
          break;
        case LogisticRole.MINISTRY_ENTITY:
          whereParams = {
            logisticRole: LogisticRole.MINISTRY_ENTITY
          };
          break;
        default:
          whereParams = {
            upkId: req.user.upkId
          };
          break;
      }

      if (keyword) {
        Object.assign(whereParams, {
          [Op.or]: [
            {
              batchCode: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              "$brand.name$": {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      if (medicineId) {
        // whereMedicineParams.medicineId = medicineId;
        Object.assign(whereParams, {
          medicineId: medicineId
        });
      }

      Object.assign(whereParams, {
        stockQty: {
          [Op.gt]: 0
        }
      });

      const inventories = await Inventory.findAndCountAll({
        where: whereParams,
        include: [
          {
            model: Brand,
            required: false,
            attributes: ["id", "name", "packageMultiplier"],
            where: whereMedicineParams,
            include: [
              {
                model: Medicine,
                required: true,
                attributes: ["id", "name", "codeName", "packageMultiplier", "category"]
              }
            ]
          }
        ],
        order: [
          // ['expiredDate', 'ASC'],
          ["updatedAt", "DESC"]
        ],
        limit,
        offset: limit * page
      });

      res.send({
        status: 200,
        message: "Success",
        data: inventories.rows,
        paging: {
          page: Number(page),
          size: inventories.rows.length,
          total: inventories.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const updateAdjustment = async (req, res, next) => {
    const { user } = req;
    let {
      inventoryId,
      // adjustmentType,
      valueAdjustment,
      notes,
      reportCode,
      unitType
    } = req.body;
    unitType = unitType ? unitType : "PAKET";
    let adjustmentType =
      valueAdjustment > 0
        ? ENUM.ADJUSTMENT_TYPE.ADDITION
        : ENUM.ADJUSTMENT_TYPE.DEDUCTION;
    try {
      const currentInventory = await Inventory.findByPk(inventoryId, {
        include: {
          model: Medicine,
          attributes: ["packageMultiplier", "stockUnitType"]
        }
      });
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          if (user.upkId !== currentInventory.upkId) {
            throw { message: "Validation Error, You have no authority" };
          }
          break;
        case "SUDIN_ENTITY":
          if (user.sudinKabKotaId !== currentInventory.sudinKabKotaId) {
            throw { message: "Validation Error, You have no authority" };
          }
          break;
        case "PROVINCE_ENTITY":
          if (user.provinceId !== currentInventory.provinceId) {
            throw { message: "Validation Error, You have no authority" };
          }
          break;
      }
      let valueChange = valueAdjustment;
      const InventoryList = await InventoryAdjustment.create({
        adjustmentType: adjustmentType,
        packageUnitType:
          unitType === "PAKET" ? currentInventory.packageUnitType : null,
        packageQuantity: unitType === "PAKET" ? valueChange : 0,
        stockUnit:
          unitType === "SATUAN"
            ? currentInventory.stockUnit
            : currentInventory.medicine.stockUnitType,
        stockQty:
          unitType === "SATUAN"
            ? valueChange
            : valueChange * currentInventory.medicine.packageMultiplier,
        inventoryId: inventoryId,
        notes: notes,
        reportCode: reportCode,
        createdBy: user.id
      });

      // ------------------------------------------------------------
      if (reportCode == "KALIBRASI") {
        const InventoryList = await InventoryKalibrations.create({
          adjustmentType: adjustmentType,
          packageUnitType:
            unitType === "PAKET" ? currentInventory.packageUnitType : null,
          packageQuantity: unitType === "PAKET" ? valueChange : 0,
          stockUnit:
            unitType === "SATUAN"
              ? currentInventory.stockUnit
              : currentInventory.medicine.stockUnitType,
          stockQty:
            unitType === "SATUAN"
              ? valueChange * -1
              : (valueChange * currentInventory.medicine.packageMultiplier) * -1,
          inventoryId: inventoryId,
          notes: notes,
          reportCode: reportCode,
          createdBy: user.id
        });
      }
      // ------------------------------------------------------------

      let packageQuantitInv =
        unitType === "PAKET"
          ? currentInventory.packageQuantity + valueChange
          : currentInventory.packageQuantity +
          Math.floor(
            valueChange / currentInventory.medicine.packageMultiplier
          );
      let stockQtyInv =
        unitType === "SATUAN"
          ? currentInventory.stockQty + valueChange
          : currentInventory.stockQty +
          valueChange * currentInventory.medicine.packageMultiplier;
      await Inventory.update(
        {
          packageQuantity: packageQuantitInv,
          stockQty: stockQtyInv,
          isAdjustment: true,
          updateBy: user.id
        },
        {
          where: {
            id: inventoryId
          }
        }
      );
      res.status(200).json({
        status: 200,
        message: "success",
        data: inventoryId
      });
    } catch (err) {
      next(err);
    }
  };

  const getStockAdjustment = async (req, res, next) => {
    const { user } = req;
    let { page, limit, keyword, month } = req.query;
    page = page ? page : CONSTANT.DEFAULT_PAGE;
    limit = limit ? limit : CONSTANT.DEFAULT_LIMIT;
    const { medicineId } = req.params;
    try {
      var whereInventory = [];
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          whereInventory = {
            upkId: user.upkId
          };
          break;
        case "SUDIN_ENTITY":
          whereInventory = {
            sudinKabKotaId: user.sudinKabKotaId
          };
          break;
        case "PROVINCE_ENTITY":
          whereInventory = {
            provinceId: user.provinceId
          };
          break;
        case "PHARMA_ENTITY":
          whereInventory = {
            upkId: user.upkId,
            createdBy: user.id
          };
          break;
        case "LAB_ENTITY":
          whereInventory = {
            upkId: user.upkId,
            createdBy: user.id
          };
          break;
      }

      if (month) {
        const
          year2 = month.split("-")[0],
          month2 = month.split("-")[1];
        whereInventory = {
          ...whereInventory,
          [Op.and]: [
            sequelize.where(sequelize.fn("date_part", 'year', sequelize.col('inventoryAdjustment.created_at')), year2),
            sequelize.where(sequelize.fn("date_part", 'month', sequelize.col('inventoryAdjustment.created_at')), month2)
          ]
        };
      }

      const listAdjustment = await InventoryAdjustment.findAndCountAll({
        include: [
          {
            model: Inventory,
            // attributes: ["id"],
            where: whereInventory,
            include: [
              {
                model: Brand,
                where: medicineId != 0 ? { medicineId: medicineId } : {},
                attributes: [["name", "brandName"]],
                include: [
                  {
                    model: Medicine,
                    // attributes: [["id", "medicineId"], "codeName"]
                  }
                ]
              }
            ]
          }
        ].concat(QueryService.includeCreated()),
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });
      res.status(200).json({
        status: 200,
        message: "success",
        data: listAdjustment.rows,
        paging: {
          page: Number(page),
          size: listAdjustment.rows.length,
          total: listAdjustment.count
        }
      });
    } catch (err) {
      next(err);
    }
  };

  const stockKalibration = async (req, res, next) => {
    console.log('stockKalibration');

    const { user } = req;
    let {
      inventoryId,
      // adjustmentType,
      valueAdjustment,
      notes,
      reportCode,
      unitType
    } = req.body;
    unitType = unitType ? unitType : "PAKET";
    let adjustmentType =
      valueAdjustment > 0
        ? ENUM.ADJUSTMENT_TYPE.ADDITION
        : ENUM.ADJUSTMENT_TYPE.DEDUCTION;
    try {
      const currentInventory = await Inventory.findByPk(inventoryId, {
        include: {
          model: Medicine,
          attributes: ["packageMultiplier", "stockUnitType"]
        }
      });
      switch (user.logisticrole) {
        case "UPK_ENTITY":
          if (user.upkId !== currentInventory.upkId) {
            throw { message: "Validation Error, You have no authority" };
          }
          break;
        case "SUDIN_ENTITY":
          if (user.sudinKabKotaId !== currentInventory.sudinKabKotaId) {
            throw { message: "Validation Error, You have no authority" };
          }
          break;
        case "PROVINCE_ENTITY":
          if (user.provinceId !== currentInventory.provinceId) {
            throw { message: "Validation Error, You have no authority" };
          }
          break;
      }
      let valueChange = valueAdjustment;
      const InventoryList = await InventoryKalibrations.create({
        adjustmentType: adjustmentType,
        packageUnitType:
          unitType === "PAKET" ? currentInventory.packageUnitType : null,
        packageQuantity: unitType === "PAKET" ? valueChange : 0,
        stockUnit:
          unitType === "SATUAN"
            ? currentInventory.stockUnit
            : currentInventory.medicine.stockUnitType,
        stockQty:
          unitType === "SATUAN"
            ? valueChange
            : valueChange * currentInventory.medicine.packageMultiplier,
        inventoryId: inventoryId,
        notes: notes,
        reportCode: reportCode,
        createdBy: user.id
      });

      if (InventoryList) {
        return res.status(200).json({
          status: 200,
          message: "success",
          data: inventoryId
        });
      }

      return res.status(50).json({
        status: 50,
        message: "error",
        data: InventoryList
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    getStockAdjustment,
    updateAdjustment,
    getInventoryListStock,
    getInventoryPickingList,
    getAllInventories,
    stockKalibration
  };
};

module.exports = InventoryController;
