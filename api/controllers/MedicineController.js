const Medicine = require("../models/Medicine");
const Brand = require("../models/Brand");
const Regiment = require("../models/Regiment");
const RegimentMedicine = require("../models/RegimentMedicine");
const Inventory = require("../models/Inventory");
const User = require("../models/User");
const Province = require("../models/Province");
const SudinKabKota = require("../models/SudinKabKota");
const Upk = require("../models/Upk");
const sequelize = require("../../config/database");

const LogisticRole = require("../../config/logistic_role");

const InventoryService = require("../services/inventory.service")();

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const MedicineController = () => {
  const getMedicines = async (req, res) => {
    const { page, limit } = req.query;
    try {
      const medicines = await Brand.findAndCountAll({
        limit,
        attributes: [""],
        offset: limit * page,
        order: [["name", "ASC"]],
        include: [
          {
            model: Medicine,
            attributes: [
              "id",
              "name",
              "codeName",
              "medicineType",
              "stockUnitType"
            ]
          }
        ]
      });
      res.send({
        status: 200,
        message: "Success",
        data: medicines.rows,
        paging: {
          page: Number(page),
          size: medicines.rows.length,
          total: medicines.count
        }
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: 500,
        message: error.message,
        data: null
      });
    }
  };

  const getRegiments = async (req, res, next) => {
    try {
      const regiments = await Regiment.findAll({
        where: {},
        include: [
          Medicine,
          {
            model: User,
            as: "createdByData",
            include: [
              {
                model: Province,
                as: 'province'
              },
              {
                model: SudinKabKota,
                as: 'sudinKabKota'
              },
              {
                model: Upk,
                as: 'upk'
              },
            ],
          }
        ],
        order: [["id", "ASC"], [{ model: Medicine }, "id", "ASC"]]
      });
      res.send({
        status: 200,
        message: "Success",
        data: regiments
      });
    } catch (error) {
      next(error);
    }
  };

  const getRegiment = async (req, res, next) => {
    const { regimentId } = req.params;
    try {
      const regiment = await Regiment.findOne({
        where: {
          id: regimentId
        }
      });

      res.send({
        status: 200,
        message: "Success",
        data: regiment
      });
    } catch (error) {
      next(error);
    }
  };

  const getRegimentByCombinationIds = async (req, res, next) => {
    const { ids } = req.query;
    try {
      const regiment = await Regiment.findOne({
        where: {
          combinationIds: ids ? ids.replace(/ /g, "+") : "-"
        },
        include: [Medicine],
        order: [["id", "ASC"], [{ model: Medicine }, "id", "ASC"]]
      });

      res.send({
        status: 200,
        message: "Success",
        data: regiment
      });
    } catch (error) {
      next(error);
    }
  };

  const detailInventory = async (req, res) => {
    const { inventoryId } = req.params;
    try {
      const inventory = await sequelize.query(
        "select inventories.batch_code,inventories.expired_date,inventories.package_quantity,stock_qty,medicines.id,medicines.name,medicines.code_name,medicines.medicine_type," +
          "medicines.stock_unit_type,medicines.package_unit_type from medicines,inventories,brands " +
          "where medicines.id = brands.medicine_id and brands.id = inventories.brand_id and inventories.id = " +
          inventoryId,
        { type: sequelize.QueryTypes.SELECT }
      );

      res.send({
        status: 200,
        message: "Success",
        data: inventory
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: 500,
        message: error.message,
        data: null
      });
    }
  };

  const getMedicineStocks = async (req, res, next) => {
    const { medicineType } = req.params;
    const { logisticRole, role, upkId, sudinKabKotaId, provinceId } = req.query;
    const paramsQuery = {
      logisticrole: logisticRole,
      role: role ? role : req.user.role,
      upkId: upkId,
      sudinKabKotaId: sudinKabKotaId,
      provinceId: provinceId
    };
    try {
      let medicineStock = await InventoryService.getMedicineStock(
        logisticRole ? paramsQuery : req.user,
        medicineType,
        null,
        req.query
      );

      res.send({
        status: 200,
        data: medicineStock
      });
    } catch (err) {
      next(err);
    }
  };

  const getMedicineStockById = async (req, res, next) => {
    const { medicineId } = req.params;
    try {
      let medicineStock = await InventoryService.getMedicineStock(
        req.user,
        null,
        medicineId
      );

      res.send({
        status: 200,
        data: medicineStock
      });
    } catch (err) {
      next(err);
    }
  };

  return {
    getMedicines,
    getRegiments,
    getRegiment,
    getRegimentByCombinationIds,
    detailInventory,
    getMedicineStocks,
    getMedicineStockById
  };
};

module.exports = MedicineController;
