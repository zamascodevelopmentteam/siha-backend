const onHivDashboardService = require("../services/report/lbpha1/01_onHivDashboard.service")();
const onArtDashboardService = require("../services/report/lbpha1/02_onArtDashboard.service")();
const afterArtDashboardService = require("../services/report/lbpha1/03_afterArtDashboard.service")();
const hivImsDashboardService = require("../services/report/lbpha1/10_hivImsDashboard.service")();
const coenfectionTBDasboardService = require("../services/report/lbpha1/04_coenfectionTBDashboard.service")();
const treatmentPPKDashboardService = require("../services/report/lbpha1/05_treatmentPPK.service")();
const therapyTPTDashboardService = require("../services/report/lbpha1/06_therapyTPT.service")();
const viralLoadDashboardService = require("../services/report/lbpha1/07_viralLoad.service")();
const arvSpecialDasboardService = require("../services/report/lbpha1/08_ArvSpecial.service")();
const notificationCoupleDasboardService = require("../services/report/lbpha1/09_notificationCouple.service")();
const responseService = require("../services/report/lbpha1/responseReport.service")();

const Lbpha1 = require("../services/report/lbpha1/lbpha1.class");

const ENUM = require("../../config/enum");
const XLSX = require("xlsx");

const ReportLBPHA1Controller = () => {
  const initiateLbpha1Report = async req => {
    let month2 = null;
    let year2 = null;
    const { month } = req.query;
    if (month) {
      month2 = month.split('-')[1];
      year2 = month.split('-')[0];
    }

    const lbpha1 = new Lbpha1(req.user, req, month2, year2);
    const entitySet = lbpha1.getEntitySet();
    const filter = lbpha1.getFilter();

    const entityBaseId = entitySet.entityBaseId;
    const entityBaseValue = entitySet.entityBaseValue;
    const monthIndex = filter.month + 1;
    const yearIndex = filter.year;
    return {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    };
  };

  const getMetaReport = async (req, res, next, returnItem = false) => {
    try {
      const lbpha1 = new Lbpha1(req.user, req);
      await lbpha1.initiateMetaReport();

      const data = lbpha1.getReport();

      if (returnItem) {
        return data.meta;
      }
      
      res.json(data);
    } catch (error) {
      next(error);
    }
  };

  const getOnHivDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    let data = [
      // 0
      {
        newOdhaHIV: {
          "LAKI-LAKI": responseService.dashboard(
            await onHivDashboardService.getNewOdhaHIV(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onHivDashboardService.getNewOdhaHIV(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onHivDashboardService.getNewOdhaHIVKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 1
      {
        odhaReferIn: {
          "LAKI-LAKI": responseService.dashboard(
            await onHivDashboardService.getOdhaReferIn(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onHivDashboardService.getOdhaReferIn(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onHivDashboardService.getOdhaReferInKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 2
      {
        odhaReferOut: {
          "LAKI-LAKI": responseService.dashboard(
            await onHivDashboardService.getOdhaReferOut(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onHivDashboardService.getOdhaReferOut(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onHivDashboardService.getOdhaReferOutKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 3 = 0 + 1 - 2
      {
        cumulativeOdhaHIV: {
          
        }
      },
      {
        odhaNonArtNotVisit: {
          "LAKI-LAKI": responseService.dashboard(
            await onHivDashboardService.getOdhaNotArtNotVisit(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onHivDashboardService.getOdhaNotArtNotVisit(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onHivDashboardService.getOdhaNotArtNotVisitKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        cumulativeOdhaNonArtDeath: {
          "LAKI-LAKI": responseService.dashboard(
            await onHivDashboardService.getCumulativeOdhaNonArtDeath(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onHivDashboardService.getCumulativeOdhaNonArtDeath(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onHivDashboardService.getCumulativeOdhaNonArtDeathKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        totalVisitHiv: {
          "LAKI-LAKI": responseService.dashboard(
            await onHivDashboardService.getTotalVisitHiv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onHivDashboardService.getTotalVisitHiv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onHivDashboardService.getTotalVisitHivKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    data[3] = {
      cumulativeOdhaHIV: {
        "LAKI-LAKI": [
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][0].count + data[1].odhaReferIn["LAKI-LAKI"][0].count - data[2].odhaReferOut["LAKI-LAKI"][0].count },
          { age: "1-14", count: data[0].newOdhaHIV["LAKI-LAKI"][1].count + data[1].odhaReferIn["LAKI-LAKI"][1].count - data[2].odhaReferOut["LAKI-LAKI"][1].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][2].count + data[1].odhaReferIn["LAKI-LAKI"][2].count - data[2].odhaReferOut["LAKI-LAKI"][2].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][3].count + data[1].odhaReferIn["LAKI-LAKI"][3].count - data[2].odhaReferOut["LAKI-LAKI"][3].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][4].count + data[1].odhaReferIn["LAKI-LAKI"][4].count - data[2].odhaReferOut["LAKI-LAKI"][4].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][5].count + data[1].odhaReferIn["LAKI-LAKI"][5].count - data[2].odhaReferOut["LAKI-LAKI"][5].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][6].count + data[1].odhaReferIn["LAKI-LAKI"][6].count - data[2].odhaReferOut["LAKI-LAKI"][6].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][7].count + data[1].odhaReferIn["LAKI-LAKI"][7].count - data[2].odhaReferOut["LAKI-LAKI"][7].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][8].count + data[1].odhaReferIn["LAKI-LAKI"][8].count - data[2].odhaReferOut["LAKI-LAKI"][8].count },
          { age: "< 1", count: data[0].newOdhaHIV["LAKI-LAKI"][9].count + data[1].odhaReferIn["LAKI-LAKI"][9].count - data[2].odhaReferOut["LAKI-LAKI"][9].count },
          { jumlah: data[0].newOdhaHIV["LAKI-LAKI"][10].jumlah + data[1].odhaReferIn["LAKI-LAKI"][10].jumlah - data[2].odhaReferOut["LAKI-LAKI"][10].jumlah },
        ],
        PEREMPUAN: [
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][0].count + data[1].odhaReferIn["PEREMPUAN"][0].count - data[2].odhaReferOut["PEREMPUAN"][0].count },
          { age: "1-14", count: data[0].newOdhaHIV["PEREMPUAN"][1].count + data[1].odhaReferIn["PEREMPUAN"][1].count - data[2].odhaReferOut["PEREMPUAN"][1].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][2].count + data[1].odhaReferIn["PEREMPUAN"][2].count - data[2].odhaReferOut["PEREMPUAN"][2].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][3].count + data[1].odhaReferIn["PEREMPUAN"][3].count - data[2].odhaReferOut["PEREMPUAN"][3].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][4].count + data[1].odhaReferIn["PEREMPUAN"][4].count - data[2].odhaReferOut["PEREMPUAN"][4].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][5].count + data[1].odhaReferIn["PEREMPUAN"][5].count - data[2].odhaReferOut["PEREMPUAN"][5].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][6].count + data[1].odhaReferIn["PEREMPUAN"][6].count - data[2].odhaReferOut["PEREMPUAN"][6].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][7].count + data[1].odhaReferIn["PEREMPUAN"][7].count - data[2].odhaReferOut["PEREMPUAN"][7].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][8].count + data[1].odhaReferIn["PEREMPUAN"][8].count - data[2].odhaReferOut["PEREMPUAN"][8].count },
          { age: "< 1", count: data[0].newOdhaHIV["PEREMPUAN"][9].count + data[1].odhaReferIn["PEREMPUAN"][9].count - data[2].odhaReferOut["PEREMPUAN"][9].count },
          { jumlah: data[0].newOdhaHIV["PEREMPUAN"][10].jumlah + data[1].odhaReferIn["PEREMPUAN"][10].jumlah - data[2].odhaReferOut["PEREMPUAN"][10].jumlah },
        ],
        KELOMPOK_POPULASI: [
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][0].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][0].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][0].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][1].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][1].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][1].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][2].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][2].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][2].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][3].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][3].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][3].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][4].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][4].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][4].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][5].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][5].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][5].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][6].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][6].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][6].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][7].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][7].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][7].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][8].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][8].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][8].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][9].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][9].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][9].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][10].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][10].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][10].count },
          { label: "WPS", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][11].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][11].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][11].count },
          { label: "Total", count: data[0].newOdhaHIV["KELOMPOK_POPULASI"][12].count + data[1].odhaReferIn["KELOMPOK_POPULASI"][12].count - data[2].odhaReferOut["KELOMPOK_POPULASI"][12].count },
        ]
      }
    };

    if (returnItem) {
      return data;
    }

    try {
      res.send({
        status: 200,
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getOnArtDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await await initiateLbpha1Report(req);

    let data = [
      // 0
      {
        newOdhaArt: {
          "LAKI-LAKI": responseService.dashboard(
            await onArtDashboardService.getNewOdhaArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onArtDashboardService.getNewOdhaArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onArtDashboardService.getNewOdhaArtKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 1
      {
        odhaReferInArt: {
          "LAKI-LAKI": responseService.dashboard(
            await onArtDashboardService.getOdhaReferInArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onArtDashboardService.getOdhaReferInArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onArtDashboardService.getOdhaReferInArtKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 2
      {
        odhaReferOutArt: {
          "LAKI-LAKI": responseService.dashboard(
            await onArtDashboardService.getOdhaReferOutArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await onArtDashboardService.getOdhaReferOutArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await onArtDashboardService.getOdhaReferOutArtKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 3
      {
        cumulativeOdhaArt: {
          
        }
      },
    ]

    let laki = [];
    let perempuan = [];
    let kelompokPopulasi = [];
    for (let i = 0; i < 12; i++) {
      if (i < 10) {
        laki.push({ age: "< 1", count: data[0].newOdhaArt["LAKI-LAKI"][i].count + data[1].odhaReferInArt["LAKI-LAKI"][i].count - data[2].odhaReferOutArt["LAKI-LAKI"][i].count });
        perempuan.push({ age: "< 1", count: data[0].newOdhaArt["PEREMPUAN"][i].count + data[1].odhaReferInArt["PEREMPUAN"][i].count - data[2].odhaReferOutArt["PEREMPUAN"][i].count });
      }
      kelompokPopulasi.push({ label: "WPS", count: data[0].newOdhaArt["KELOMPOK_POPULASI"][i].count + data[1].odhaReferInArt["KELOMPOK_POPULASI"][i].count - data[2].odhaReferOutArt["KELOMPOK_POPULASI"][i].count });
    }
    laki.push({ jumlah: data[0].newOdhaArt["LAKI-LAKI"][10].jumlah + data[1].odhaReferInArt["LAKI-LAKI"][10].jumlah - data[2].odhaReferOutArt["LAKI-LAKI"][10].jumlah });
    perempuan.push({ jumlah: data[0].newOdhaArt["PEREMPUAN"][10].jumlah + data[1].odhaReferInArt["PEREMPUAN"][10].jumlah - data[2].odhaReferOutArt["PEREMPUAN"][10].jumlah });
    kelompokPopulasi.push({ label: "Total", count: data[0].newOdhaArt["KELOMPOK_POPULASI"][12].count + data[1].odhaReferInArt["KELOMPOK_POPULASI"][12].count - data[2].odhaReferOutArt["KELOMPOK_POPULASI"][12].count });

    data[3] = {
      cumulativeOdhaArt: {
        "LAKI-LAKI": laki,
        PEREMPUAN: perempuan,
        KELOMPOK_POPULASI: kelompokPopulasi
      }
    };

    if (returnItem) {
      return data;
    }

    try {
      res.send({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getAfterArtDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    let data = [
      // 0
      {
        odhaDeath: {
          "LAKI-LAKI": responseService.dashboard(
            await afterArtDashboardService.getOdhaDeaths(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await afterArtDashboardService.getOdhaDeaths(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await afterArtDashboardService.getOdhaDeathsKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 1
      {
        odhaStopArt: {
          "LAKI-LAKI": responseService.dashboard(
            await afterArtDashboardService.getOdhasStopArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await afterArtDashboardService.getOdhasStopArt(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await afterArtDashboardService.getOdhasStopArtKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 2
      {
        odhaNotVisit: {
          "LAKI-LAKI": responseService.dashboard(
            await afterArtDashboardService.getOdhasNotVisit(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await afterArtDashboardService.getOdhasNotVisit(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await afterArtDashboardService.getOdhasNotVisitKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 3
      {
        failFollowUp3Month: {
          "LAKI-LAKI": responseService.dashboard(
            await afterArtDashboardService.getFailFollowUp3Month(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await afterArtDashboardService.getFailFollowUp3Month(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await afterArtDashboardService.getFailFollowUp3MonthKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 4
      {
        cumulativeOdhaArt_3: {
          
        }
      },
      // 5
      {
        odhaRejimen1Original: {

        }
      },
      // 6
      {
        odhaRejimen1: {
          "LAKI-LAKI": responseService.dashboard(
            await afterArtDashboardService.getOdhaRejimen1(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await afterArtDashboardService.getOdhaRejimen1(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await afterArtDashboardService.getOdhaRejimen1KelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      // 7
      {
        odhaRejimen2: {
          "LAKI-LAKI": responseService.dashboard(
            await afterArtDashboardService.getOdhaRejimen2(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await afterArtDashboardService.getOdhaRejimen2(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await afterArtDashboardService.getOdhaRejimen2KelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    const onArt = await getOnArtDashboard(req, res, next, true);
    let laki = [];
    let perempuan = [];
    let kelompokPopulasi = [];
    for (let i = 0; i < 12; i++) {
      if (i < 10) {
        laki.push({ age: "< 1", count: onArt[3].cumulativeOdhaArt["LAKI-LAKI"][i].count - data[0].odhaDeath["LAKI-LAKI"][i].count - data[1].odhaStopArt["LAKI-LAKI"][i].count - data[2].odhaNotVisit["LAKI-LAKI"][i].count - data[3].failFollowUp3Month["LAKI-LAKI"][i].count });
        perempuan.push({ age: "< 1", count: onArt[3].cumulativeOdhaArt["PEREMPUAN"][i].count - data[0].odhaDeath["PEREMPUAN"][i].count - data[1].odhaStopArt["PEREMPUAN"][i].count - data[2].odhaNotVisit["PEREMPUAN"][i].count - data[3].failFollowUp3Month["PEREMPUAN"][i].count });
      }
      kelompokPopulasi.push({ label: "WPS", count: onArt[3].cumulativeOdhaArt["KELOMPOK_POPULASI"][i].count - data[0].odhaDeath["KELOMPOK_POPULASI"][i].count - data[1].odhaStopArt["KELOMPOK_POPULASI"][i].count - data[2].odhaNotVisit["KELOMPOK_POPULASI"][i].count - data[3].failFollowUp3Month["KELOMPOK_POPULASI"][i].count });
    }
    laki.push({ jumlah: onArt[3].cumulativeOdhaArt["LAKI-LAKI"][10].jumlah - data[0].odhaDeath["LAKI-LAKI"][10].jumlah - data[1].odhaStopArt["LAKI-LAKI"][10].jumlah - data[2].odhaNotVisit["LAKI-LAKI"][10].jumlah - data[3].failFollowUp3Month["LAKI-LAKI"][10].jumlah });
    perempuan.push({ jumlah: onArt[3].cumulativeOdhaArt["PEREMPUAN"][10].jumlah - data[0].odhaDeath["PEREMPUAN"][10].jumlah - data[1].odhaStopArt["PEREMPUAN"][10].jumlah - data[2].odhaNotVisit["PEREMPUAN"][10].jumlah - data[3].failFollowUp3Month["PEREMPUAN"][10].jumlah });
    kelompokPopulasi.push({ label: "Total", count: onArt[3].cumulativeOdhaArt["KELOMPOK_POPULASI"][12].count - data[0].odhaDeath["KELOMPOK_POPULASI"][12].count - data[1].odhaStopArt["KELOMPOK_POPULASI"][12].count - data[2].odhaNotVisit["KELOMPOK_POPULASI"][12].count - data[3].failFollowUp3Month["KELOMPOK_POPULASI"][12].count });

    data[4] = {
      cumulativeOdhaArt_3: {
        "LAKI-LAKI": laki,
        PEREMPUAN: perempuan,
        KELOMPOK_POPULASI: kelompokPopulasi
      }
    };

    laki = [];
    perempuan = [];
    kelompokPopulasi = [];
    for (let i = 0; i < 12; i++) {
      if (i < 10) {
        laki.push({ age: "< 1", count: data[4].cumulativeOdhaArt_3["LAKI-LAKI"][i].count - data[6].odhaRejimen1["LAKI-LAKI"][i].count - data[7].odhaRejimen2["LAKI-LAKI"][i].count });
        perempuan.push({ age: "< 1", count: data[4].cumulativeOdhaArt_3["PEREMPUAN"][i].count - data[6].odhaRejimen1["PEREMPUAN"][i].count - data[7].odhaRejimen2["PEREMPUAN"][i].count });
      }
      kelompokPopulasi.push({ label: "WPS", count: data[4].cumulativeOdhaArt_3["KELOMPOK_POPULASI"][i].count - data[6].odhaRejimen1["KELOMPOK_POPULASI"][i].count - data[7].odhaRejimen2["KELOMPOK_POPULASI"][i].count });
    }
    laki.push({ jumlah: data[4].cumulativeOdhaArt_3["LAKI-LAKI"][10].jumlah - data[6].odhaRejimen1["LAKI-LAKI"][10].jumlah - data[7].odhaRejimen2["LAKI-LAKI"][10].jumlah });
    perempuan.push({ jumlah: data[4].cumulativeOdhaArt_3["PEREMPUAN"][10].jumlah - data[6].odhaRejimen1["PEREMPUAN"][10].jumlah - data[7].odhaRejimen2["PEREMPUAN"][10].jumlah });
    kelompokPopulasi.push({ label: "Total", count: data[4].cumulativeOdhaArt_3["KELOMPOK_POPULASI"][12].count - data[6].odhaRejimen1["KELOMPOK_POPULASI"][12].count - data[7].odhaRejimen2["KELOMPOK_POPULASI"][12].count });

    data[5] = {
      odhaRejimen1Original: {
        "LAKI-LAKI": laki,
        PEREMPUAN: perempuan,
        KELOMPOK_POPULASI: kelompokPopulasi
      }
    };

    if (returnItem) {
      return data;
    }

    try {
      res.send({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getCoenfectionTBDasboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        patients_tb: {
          "LAKI-LAKI": responseService.dashboard(
            await coenfectionTBDasboardService.getPatientsTB(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await coenfectionTBDasboardService.getPatientsTB(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await coenfectionTBDasboardService.getPatientsTBKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_t_hiv: {
          "LAKI-LAKI": responseService.dashboard(
            await coenfectionTBDasboardService.getPatientsTBHiv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await coenfectionTBDasboardService.getPatientsTBHiv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await coenfectionTBDasboardService.getPatientsTBHivKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_tb_hiv_arv: {
          "LAKI-LAKI": responseService.dashboard(
            await coenfectionTBDasboardService.getPatientsTbHivArv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await coenfectionTBDasboardService.getPatientsTbHivArv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await coenfectionTBDasboardService.getPatientsTbHivArvKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    if (returnItem) {
      return data;
    }

    try {
      res.status(200).json({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getTreatmentPPKDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        new_patients_PPK: {
          "LAKI-LAKI": responseService.dashboard(
            await treatmentPPKDashboardService.getNewPatientsPPK(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await treatmentPPKDashboardService.getNewPatientsPPK(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await treatmentPPKDashboardService.getNewPatientsPPKKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_PPK: {
          "LAKI-LAKI": responseService.dashboard(
            await treatmentPPKDashboardService.getPatientsPPK(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await treatmentPPKDashboardService.getPatientsPPK(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await treatmentPPKDashboardService.getPatientsPPKKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    if (returnItem) {
      return data;
    }

    try {
      res.status(200).json({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getTherapyTPTDasboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        new_patients_PP_INH: {
          "LAKI-LAKI": responseService.dashboard(
            await therapyTPTDashboardService.getPatientsPPINH(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await therapyTPTDashboardService.getPatientsPPINH(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await therapyTPTDashboardService.getPatientsPPINHKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        old_patients_PP_INH: {
          "LAKI-LAKI": responseService.dashboard(
            await therapyTPTDashboardService.getOldPatientsPPINH(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await therapyTPTDashboardService.getOldPatientsPPINH(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await therapyTPTDashboardService.getOldPatientsPPINHKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_rifampentin: {
          "LAKI-LAKI": responseService.dashboard(
            await therapyTPTDashboardService.getPatientsRifampentine(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await therapyTPTDashboardService.getPatientsRifampentine(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await therapyTPTDashboardService.getPatientsRifampentineKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    if (returnItem) {
      return data;
    }

    try {
      res.status(200).json({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getViralLoadDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        patients_VL: {
          "LAKI-LAKI": responseService.dashboard(
            await viralLoadDashboardService.getPatientsVL(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await viralLoadDashboardService.getPatientsVL(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await viralLoadDashboardService.getPatientsVLKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_over_VL: {
          "LAKI-LAKI": responseService.dashboard(
            await viralLoadDashboardService.getPatientsOverVirus(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await viralLoadDashboardService.getPatientsOverVirus(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await viralLoadDashboardService.getPatientsOverVirusKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ]

    if (returnItem) {
      return data;
    }

    try {
      res.status(200).json({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getArvSpecialDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        patients_ARV: {
          "LAKI-LAKI": responseService.dashboard(
            await arvSpecialDasboardService.getPatientsOldArv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await arvSpecialDasboardService.getPatientsOldArv(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await arvSpecialDasboardService.getPatientsOldArvKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_profilaksis: {
          "LAKI-LAKI": responseService.dashboard(
            await arvSpecialDasboardService.getPatientsProfilaksis(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await arvSpecialDasboardService.getPatientsProfilaksis(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await arvSpecialDasboardService.getPatientsProfilaksisKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    if (returnItem) {
      return data;
    }

    try {
      res.status(200).json({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getNotificationCoupleDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        patients_offer_couple_notif: {
          "LAKI-LAKI": responseService.dashboard(
            await notificationCoupleDasboardService.getPatientsOfferCouple(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await notificationCoupleDasboardService.getPatientsOfferCouple(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await notificationCoupleDasboardService.getPatientsOfferCoupleKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        patients_accept_offer_couple_notif: {
          "LAKI-LAKI": responseService.dashboard(
            await notificationCoupleDasboardService.getPatientsAcceptOfferCouple(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await notificationCoupleDasboardService.getPatientsAcceptOfferCouple(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await notificationCoupleDasboardService.getPatientsAcceptOfferCoupleKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        couple: {
          "LAKI-LAKI": responseService.dashboard(
            await notificationCoupleDasboardService.getPatientsCouple(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await notificationCoupleDasboardService.getPatientsCouple(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await notificationCoupleDasboardService.getPatientsCoupleKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ]

    if (returnItem) {
      return data;
    }

    try {
      res.status(200).json({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getHivImsDashboard = async (req, res, next, returnItem = false) => {
    const {
      entityBaseId,
      entityBaseValue,
      monthIndex,
      yearIndex
    } = await initiateLbpha1Report(req);

    const data = [
      {
        odhaIms: {
          "LAKI-LAKI": responseService.dashboard(
            await hivImsDashboardService.getOdhaIms(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await hivImsDashboardService.getOdhaIms(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await hivImsDashboardService.getOdhaImsKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        odhaImsPositive: {
          "LAKI-LAKI": responseService.dashboard(
            await hivImsDashboardService.getOdhaImsPositive(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await hivImsDashboardService.getOdhaImsPositive(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await hivImsDashboardService.getOdhaImsPositiveKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        odhaSifilis: {
          "LAKI-LAKI": responseService.dashboard(
            await hivImsDashboardService.getOdhaSifilis(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await hivImsDashboardService.getOdhaSifilis(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await hivImsDashboardService.getOdhaSifilisKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      },
      {
        odhaSifilisPositive: {
          "LAKI-LAKI": responseService.dashboard(
            await hivImsDashboardService.getOdhaSifilisPositive(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.LAKI_LAKI,
              monthIndex,
              yearIndex
            )
          ),
          PEREMPUAN: responseService.dashboard(
            await hivImsDashboardService.getOdhaSifilisPositive(
              entityBaseId,
              entityBaseValue,
              ENUM.GENDER.PEREMPUAN,
              monthIndex,
              yearIndex
            )
          ),
          KELOMPOK_POPULASI: responseService.kelompokPopulasi(
            await hivImsDashboardService.getOdhaSifilisPositiveKelPopulasi(
              entityBaseId,
              entityBaseValue,
              monthIndex,
              yearIndex
            )
          )
        }
      }
    ];

    if (returnItem) {
      return data;
    }

    try {
      res.send({
        status: 200,
        message: "Success",
        data: data
      });
    } catch (error) {
      next(error);
    }
  };

  const getExcel = async (req, res, next) => {
    let month2 = null;
    let year2 = null;
    const { month } = req.query;
    if (month) {
      month2 = parseInt(month.split('-')[1]) - 1;
      year2 = month.split('-')[0];
    }
    
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    let dateObj = new Date();
    let monthh = dateObj.getMonth();
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let date = day + '-' + monthh + '-' + year;
    let dateMoth = day + ' ' + monthNames[monthh - 1] + ' ' + year;

    const meta = await getMetaReport(req, res, next, true);
    let data = {};
    data.onHiv = await getOnHivDashboard(req, res, next, true);
    data.onArt = await getOnArtDashboard(req, res, next, true);
    data.afterArt = await getAfterArtDashboard(req, res, next, true);
    data.coenfectionTB = await getCoenfectionTBDasboard(req, res, next, true);
    data.treatmentPPK = await getTreatmentPPKDashboard(req, res, next, true);
    data.therapyTPT = await getTherapyTPTDasboard(req, res, next, true);
    data.viralLoad = await getViralLoadDashboard(req, res, next, true);
    data.arvSpecial = await getArvSpecialDashboard(req, res, next, true);
    data.notificationCouple = await getNotificationCoupleDashboard(req, res, next, true);
    data.hivIms = await getHivImsDashboard(req, res, next, true);

    const indexCols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ"];
    const titleList = {
      onHiv: '1. Masuk Dalam Perawatan HIV',
      onArt: '2. Masuk dengan ART',
      afterArt: '3. Dampak ART',
      coenfectionTB: '4. Koinfeksi TB-HIV',
      treatmentPPK: '5. Pengobatan Pencegahan Kotrimosazol (PPK)',
      therapyTPT: '6. Terapi Pencegahan TBC (TPT)',
      viralLoad: '7. Viral Load',
      arvSpecial: '8. Jumlah Penggunaan ARV Khusus',
      notificationCouple: '9. Notifikasi Pasangan',
      hivIms: '10. HIV IMS'
    };
    const age = ["< 1", "1-14", "15-19", "20-24", "25-29", "30-24", "35-39", "40-44", "45-49", "> 50", "Jumlah"];
    const specialOPopulation = ["WPS", "Waria", "Penasun", "LSL", "Bumil", "Pasien TB", "Pasien IMS", "Pasien Hepatitis", "Pasangan ODHA", "Anak ODHA", "WBP", "Lainnya", "Total"];
    const sheetObj = {
      "Laporan LBPHA1": {
        "!ref": "A1:AJ100",
        // ---------------------------
        A1: { t: 's', v: "Nama UPK" }, // t => type s => string, n => number, b => boolean
        A2: { t: 's', v: meta && meta.upk.name ? meta.upk.name : "-" },
        A3: { t: 's', v: "Nama Kab/Kota" },
        A4: { t: 's', v: meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-" },
        A5: { t: 's', v: "Nama Provinsi" },
        A6: { t: 's', v: meta && meta.province.name ? meta.province.name : "-" },
        // ---------------------------
        C1: { t: 's', v: "Bulan" }, // t => type s => string, n => number, b => boolean
        C2: { t: 's', v: month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-") },
        C3: { t: 's', v: "Tahun" },
        C4: { t: 's', v: year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-") },
        C5: { t: 's', v: "Tanggal Akses" },
        C6: { t: 's', v: dateMoth },
        // ----------------------------
        "!cols": [
          { wch: 50 }
        ],
        "!merges": [
          // { s: { r: 6, c: 0 }, e: { r: 7, c: 0 } }, /* r => row ke berapa, c => column ke berapa */
        ]
      }
    };

    let row = 7;

    Object.keys(data).forEach(function (key) {
      sheetObj["Laporan LBPHA1"]["A" + row] = { t: 's', v: titleList[key] };

      row++

      data[key].map((value, idx) => {
        if (idx == 0) {
          sheetObj["Laporan LBPHA1"]["A" + row] = { t: 's', v: 'Variabel' };
          sheetObj["Laporan LBPHA1"]["B" + row] = { t: 's', v: 'Laki-Laki' };
          sheetObj["Laporan LBPHA1"]["M" + row] = { t: 's', v: 'Perempuan' };
          sheetObj["Laporan LBPHA1"]["X" + row] = { t: 's', v: 'Kelompok Populasi Khusus' };

          row++;

          age.map((v, i) => {
            sheetObj["Laporan LBPHA1"][indexCols[i + 1] + row] = { t: 's', v: v };
          })
          age.map((v, i) => {
            sheetObj["Laporan LBPHA1"][indexCols[i + 12] + row] = { t: 's', v: v };
          })
          specialOPopulation.map((v, i) => {
            sheetObj["Laporan LBPHA1"][indexCols[i + 23] + row] = { t: 's', v: v };
          })

          row++;
        }
        let plus = 1;
        const valueKey = Object.keys(value)[0];
        if (key === "afterArt" && idx === 4) {
          // console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
          sheetObj["Laporan LBPHA1"]["A" + row] = { t: 's', v: "3.5 Jumlah kumulatif ODHA dengan ART s/d akhir bulan ini" };

          row++;
        }
        sheetObj["Laporan LBPHA1"]["A" + row] = { t: 's', v: ENUM.VARIABLE_TYPE[valueKey] };
        Object.keys(value[valueKey]).map(key2 => {
          if (key2 === "LAKI-LAKI") {
            plus = 1;
          } else if (key2 === "PEREMPUAN") {
            plus = 12;            
          } else if (key2 === "KELOMPOK_POPULASI") {
            plus = 23;            
          }
          value[valueKey][key2].map((value2, idx3) => {
            const num = value2.count || value2.jumlah;
            sheetObj["Laporan LBPHA1"][indexCols[idx3 + plus] + row] = { t: 'n', v: (num || 0) };
          })
      })

        row++;
      });

      row++;
    });

    XLSX.writeFile({
      SheetNames: ["Laporan LBPHA1"],
      Sheets: sheetObj
    }, './api/excel/LaporanLBPHA1-' + date + '.xlsx');
    return res.send({
      status: 200,
      message: "Success",
      data: "/api/excel/LaporanLBPHA1-" + date + ".xlsx",
      data2: data
    })


    // try {
    //   res.send({
    //     status: 200,
    //     message: "Success",
    //     data: data
    //   });
    // } catch (error) {
    //   next(error);
    // }
  };

  return {
    getMetaReport,
    getOnHivDashboard,
    getOnArtDashboard,
    getAfterArtDashboard,
    getHivImsDashboard,
    getCoenfectionTBDasboard,
    getTreatmentPPKDashboard,
    getTherapyTPTDasboard,
    getViralLoadDashboard,
    getArvSpecialDashboard,
    getNotificationCoupleDashboard,
    getExcel
  };
};

module.exports = ReportLBPHA1Controller;
