const ResponseService = require("../services/response.service")();
const VariableService = require("../services/variable.service")();
const LoggerServive = require("../services/logger.service")();
const ENUM = require("../../config/enum");

const Controller = () => {
  const rajasmsHook = (req, res, next) => {
    LoggerServive.info(`Retrieving Webhook from RajaSMS!`);
    console.log(req.method, req.body, req.query, req.params, req.protocol);
    res.status(200).json({
      data: {
        method: req.method,
        body: req.body,
        query: req.query,
        params: req.params,
        protocol: req.protocol
      }
    });
  };

  return {
    rajasmsHook
  };
};

module.exports = Controller;
