const { Model, Sequelize, Op } = require("sequelize");
const sequelize = require("../../config/database");

const ENUM = require("../../config/enum");
const varService = require("../services/variable.service")();
const QueryService = require("../services/query.service")();
const bcryptService = require("../services/bcrypt.service")();

const Medicine = require("../models/Medicine");
const ArvMedicine = require("../models/ArvMedicine");
const NonArvMedicine = require("../models/NonArvMedicine");
const Brand = require("../models/Brand");
const Regiment = require("../models/Regiment");
const RegimentMedicine = require("../models/RegimentMedicine");
const Prescription = require("../models/Prescription");
const PrescriptionMedicine = require("../models/PrescriptionMedicine");
const Producer = require("../models/Producer");

const User = require("../models/User");
const Province = require("../models/Province");
const SudinKabKota = require("../models/SudinKabKota");
const Upk = require("../models/Upk");
const Patient = require("../models/Patient");

const MasterDataController = () => {
  const addOrUpdateMedicine = async (req, res, next) => {
    const body = req.body;
    const medicineId = req.params.medicineId;
    try {
      let dataParams = {
        name: body.name,
        codeName: body.codeName,
        unitPrice: body.unitPrice,
        fundSource: body.fundSource,
        medicineType: body.medicineType,
        stockUnitType: body.stockUnitType,
        packageUnitType: body.packageUnitType,
        packageMultiplier: body.packageMultiplier,
        ingredients: body.ingredients,
        sediaan: body.sediaan,
        imageUrl: body.imageUrl,
        category: body.category
      };
      let medicine;
      if (medicineId) {
        medicine = await Medicine.findByPk(medicineId);
        if (!medicine) {
          throw {
            status: 400,
            message: "medicine_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(medicine, dataParams);
        await medicine.save();
        await Brand.update(
          {
            name: medicine.name,
            notes: medicine.ingredients,
            packageUnitType: medicine.packageUnitType,
            packageMultiplier: medicine.packageMultiplier,
            updatedBy: req.user.id
          },
          {
            where: {
              medicineId: medicine.id,
              isGeneric: true
            }
          }
        );
        await ArvMedicine.destroy({
          where: {
            medicineId: medicine.id
          },
          force: true
        });
        await NonArvMedicine.destroy({
          where: {
            medicineId: medicine.id
          },
          force: true
        });
      } else {
        dataParams.createdBy = req.user.id;
        medicine = await Medicine.create(dataParams);
        await Brand.create({
          medicineId: medicine.id,
          name: medicine.name,
          isGeneric: true,
          notes: medicine.ingredients,
          packageUnitType: medicine.packageUnitType,
          packageMultiplier: medicine.packageMultiplier,
          createdBy: req.user.id
        });
      }
      if (medicine.medicineType == ENUM.MEDICINE_TYPE.NON_ARV) {
        const medParams = {
          isR1: body.isR1 == true ? true : false,
          isR2: body.isR2 == true ? true : false,
          isR3: body.isR3 == true ? true : false,
          isSifilis: body.isSifilis == true ? true : false,
          isAnak: body.isAnak == true ? true : false,
          isAlkes: body.isAlkes == true ? true : false,
          isIoIms: body.isIoIms == true ? true : false,
          isVl: body.isVl == true ? true : false,
          isCd4: body.isCd4 == true ? true : false,
          isMachine: body.isMachine == true ? true : false,
          isPreventif: body.isPreventif == true ? true : false,
          testType: body.testType,
          vlcd4Category: body.vlcd4Category ? body.vlcd4Category : null
        };
        medParams.createdBy = req.user.id;
        medParams.medicineId = medicine.id;
        await NonArvMedicine.create(medParams);
      } else {
        const medParams = {
          isLini1: body.isLini1 == true ? true : false,
          isLini2: body.isLini2 == true ? true : false
        };
        medParams.createdBy = req.user.id;
        medParams.medicineId = medicine.id;
        await ArvMedicine.create(medParams);
      }
      res.status(200).json({
        data: medicine
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteMedicine = async (req, res, next) => {
    const medicineId = req.params.medicineId;
    try {
      await Promise.all([
        Brand.destroy({
          where: {
            medicineId: medicineId
          }
        }),
        ArvMedicine.destroy({
          where: {
            medicineId: medicineId
          }
        }),
        NonArvMedicine.destroy({
          where: {
            medicineId: medicineId
          }
        })
      ]);
      const result = await Medicine.destroy({
        where: {
          id: medicineId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getMedicine = async (req, res, next) => {
    const medicineId = req.params.medicineId;
    try {
      const medicine = await Medicine.findByPk(medicineId, {
        include: [
          {
            model: Brand
          },
          {
            model: ArvMedicine
          },
          {
            model: NonArvMedicine
          }
        ].concat(QueryService.includeCreated())
      });
      res.status(200).json({
        data: medicine
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllMedicines = async (req, res, next) => {
    let { page, limit, keyword, medicineType } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              codeName: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              ingredients: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      if (medicineType && medicineType !== "ALL") {
        Object.assign(whereParam, {
          medicineType: medicineType
        });
      }
      const medicines = await Medicine.findAndCountAll({
        // logging: console.log,
        where: whereParam,
        include: [
          {
            model: Brand,
            // required: true
            required: false
          },
          {
            model: ArvMedicine,
            required: false
          },
          {
            model: NonArvMedicine,
            required: false,
            // attributes: {exclude: ['vlcd4Category']}
          }
        ].concat(QueryService.includeCreated()),
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: medicines.rows,
        paging: {
          page: Number(page),
          size: medicines.rows.length,
          total: medicines.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateBrand = async (req, res, next) => {
    const body = req.body;
    const medicineId = req.body.medicineId;
    const brandId = req.params.brandId;
    try {
      let dataParams = {
        medicineId: medicineId ? medicineId : undefined,
        name: body.name,
        notes: body.notes,
        packageUnitType: body.packageUnitType,
        packageMultiplier: body.packageMultiplier
      };
      let brand;
      if (brandId) {
        brand = await Brand.findByPk(brandId);
        if (!brand) {
          throw {
            status: 400,
            message: "brand_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(brand, dataParams);
        await brand.save();
      } else {
        if (!dataParams.medicineId) {
          throw {
            status: 400,
            message: "medicine_not_found"
          };
        }
        dataParams.createdBy = req.user.id;
        brand = await Brand.create(dataParams);
      }
      res.status(200).json({
        data: brand
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteBrand = async (req, res, next) => {
    const brandId = req.params.brandId;
    try {
      const result = await Brand.destroy({
        where: {
          id: brandId,
          isGeneric: false
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getBrand = async (req, res, next) => {
    const brandId = req.params.brandId;
    try {
      const brand = await Brand.findByPk(brandId, {
        include: [
          {
            model: Medicine
          }
        ]
      });
      if (!brand) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: brand
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllBrands = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              notes: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const brands = await Brand.findAndCountAll({
        where: whereParam,
        include: [
          {
            model: Medicine,
            required: true
          }
        ].concat(QueryService.includeCreated()),
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: brands.rows,
        paging: {
          page: Number(page),
          size: brands.rows.length,
          total: brands.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllMedicineBrands = async (req, res, next) => {
    const medicineId = req.params.medicineId;
    try {
      let whereParam = {
        medicineId: medicineId
      };
      // if (keyword) {
      //   Object.assign(whereParam, {
      //     [Op.or]: [
      //       {
      //         name: {
      //           [Op.iLike]: "%" + keyword + "%"
      //         }
      //       },
      //       {
      //         notes: {
      //           [Op.iLike]: "%" + keyword + "%"
      //         }
      //       }
      //     ]
      //   });
      // }
      const brands = await Brand.findAll({
        where: whereParam
      });

      res.send({
        status: 200,
        data: brands
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateRegiment = async (req, res, next) => {
    const body = req.body;
    const regimentId = req.params.regimentId;
    try {
      let dataParams = {
        name: body.name,
        combinationIds: req.medicineList.map(item => item.id).join("+"),
        combinationStrInfo: req.medicineList
          .map(item => item.codeName)
          .join("+")
      };
      let regiment;
      if (regimentId) {
        regiment = await Regiment.findByPk(regimentId);
        if (!regiment) {
          throw {
            status: 400,
            message: "regiment_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(regiment, dataParams);
        await regiment.save();
        await RegimentMedicine.destroy({
          where: {
            regimentId: regimentId
          },
          force: true
        });
      } else {
        dataParams.createdBy = req.user.id;
        regiment = await Regiment.create(dataParams);
      }
      await RegimentMedicine.bulkCreate(
        req.medicineList.map(item => ({
          medicineId: item.id,
          regimentId: regiment.id,
          createdBy: req.user.id
        }))
      );

      res.status(200).json({
        data: regiment
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteRegiment = async (req, res, next) => {
    const regimentId = req.params.regimentId;
    try {
      const result = await Regiment.destroy({
        where: {
          id: regimentId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getRegiment = async (req, res, next) => {
    const regimentId = req.params.regimentId;
    try {
      const regiment = await Regiment.findByPk(regimentId, {
        include: [
          {
            model: Medicine
          }
        ]
      });
      if (!regiment) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: regiment
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllRegiments = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              combinationStrInfo: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const regiments = await Regiment.findAndCountAll({
        where: whereParam,
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: regiments.rows,
        paging: {
          page: Number(page),
          size: regiments.rows.length,
          total: regiments.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdatePrescription = async (req, res, next) => {
    const body = req.body;
    const recordId = req.params.prescriptionId;
    try {
      let dataParams = {
        notes: body.notes,
        isDraft: false,
        paduanObatIds: req.medicineList.map(item => item.id).join("+"),
        paduanObatStr: req.medicineList.map(item => item.codeName).join("+"),
        prescriptionNumber:
          "MASTER#" + req.medicineList.map(item => item.codeName).join("+")
      };
      let record;
      if (recordId) {
        record = await Prescription.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(record, dataParams);
        await record.save();
        await PrescriptionMedicine.destroy({
          where: {
            prescriptionId: recordId
          },
          force: true
        });
      } else {
        dataParams.createdBy = req.user.id;
        record = await Prescription.create(dataParams);
      }
      const regiment = await Regiment.findOne({
        where: {
          combinationIds: record.paduanObatIds
        }
      });
      if (regiment) {
        record.regimentId = regiment.id;
      } else {
        record.regimentId = null;
      }
      await record.save();
      await PrescriptionMedicine.bulkCreate(
        req.medicineList.map(item => ({
          medicineId: item.id,
          prescriptionId: record.id,
          createdBy: req.user.id
        }))
      );

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deletePrescription = async (req, res, next) => {
    const recordId = req.params.prescriptionId;
    try {
      const result = await Prescription.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getPrescription = async (req, res, next) => {
    const recordId = req.params.prescriptionId;
    try {
      const record = await Prescription.findByPk(recordId, {
        include: [
          {
            model: PrescriptionMedicine,
            include: [Medicine]
          },
          {
            model: Regiment
          }
        ]
      });
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllPrescriptions = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              prescriptionNumber: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              paduanObatStr: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const results = await Prescription.findAndCountAll({
        where: whereParam,
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateProvince = async (req, res, next) => {
    const body = req.body;
    const recordId = req.params.provinceId;
    try {
      let dataParams = {
        name: body.name,
        isActive: body.isActive,
        orderMultiplier: body.orderMultiplier,
        address: body.address
      };
      let record;
      if (recordId) {
        record = await Province.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(record, dataParams);
        await record.save();
      } else {
        dataParams.createdBy = req.user.id;
        record = await Province.create(dataParams);
      }

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteProvince = async (req, res, next) => {
    const recordId = req.params.provinceId;
    try {
      const result = await Province.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getProvince = async (req, res, next) => {
    const recordId = req.params.provinceId;
    try {
      const record = await Province.findByPk(recordId, {});
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllProvinces = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const results = await Province.findAndCountAll({
        where: whereParam,
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateSudin = async (req, res, next) => {
    const body = req.body;
    const recordId = req.params.sudinId;
    const provinceId = body.provinceId;
    try {
      let dataParams = {
        provinceId: provinceId ? provinceId : undefined,
        name: body.name,
        isActive: body.isActive,
        orderMultiplier: body.orderMultiplier,
        address: body.address
      };
      let record;
      if (recordId) {
        record = await SudinKabKota.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(record, dataParams);
        await record.save();
      } else {
        if (!dataParams.provinceId) {
          throw {
            status: 400,
            message: "province_not_found"
          };
        }
        dataParams.createdBy = req.user.id;
        record = await SudinKabKota.create(dataParams);
      }

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteSudin = async (req, res, next) => {
    const recordId = req.params.sudinId;
    try {
      const result = await SudinKabKota.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getSudin = async (req, res, next) => {
    const recordId = req.params.sudinId;
    try {
      const record = await SudinKabKota.findByPk(recordId, {
        include: [{ model: Province, required: true }]
      });
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllSudin = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;

    const { provinceId } = req.params;
    let whereParam = {};
    try {
      if (provinceId) {
        whereParam.provinceId = provinceId;
      }
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            },
            {
              "$province.name$": {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const results = await SudinKabKota.findAndCountAll({
        where: whereParam,
        include: [{ model: Province, required: true }],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateUpk = async (req, res, next) => {
    const body = req.body;
    const recordId = req.params.upkId;
    const sudinId = body.sudinId;
    try {
      let dataParams = {
        sudinKabKotaId: sudinId ? sudinId : undefined,
        name: body.name,
        isActive: body.isActive,
        orderMultiplier: body.orderMultiplier,
        address: body.address
      };
      let record;
      if (recordId) {
        record = await Upk.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(record, dataParams);
        await record.save();
      } else {
        if (!dataParams.sudinKabKotaId) {
          throw {
            status: 400,
            message: "sudin_not_found"
          };
        }
        dataParams.createdBy = req.user.id;
        record = await Upk.create(dataParams);
      }

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteUpk = async (req, res, next) => {
    const recordId = req.params.upkId;
    try {
      const result = await Upk.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getUpk = async (req, res, next) => {
    const recordId = req.params.upkId;
    try {
      const record = await Upk.findByPk(recordId, {
        include: [{ model: SudinKabKota, as: "sudinKabKota", required: true }]
      });
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllUpk = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;

    const { sudinId } = req.params;
    let whereParam = {};
    try {
      if (sudinId) {
        whereParam.sudinKabKotaId = sudinId;
      }
      if (keyword) {
        keyword = keyword.toLowerCase();
        Object.assign(whereParam, {
          [Op.or]: [
            sequelize.where(
              sequelize.fn("lower", sequelize.col('upk.name')),
              {
                [Op.like]: "%" + keyword + "%"
              }
            ),
            sequelize.where(
              sequelize.fn("lower", sequelize.col('sudinKabKota.name')),
              {
                [Op.like]: "%" + keyword + "%"
              }
            ),
            // {
            //   name: {
            //     [Op.like]: "%" + keyword + "%"
            //   }
            // },
            // {
            //   "$sudinKabKota.name$": {
            //     [Op.like]: "%" + keyword + "%"
            //   }
            // }
          ]
        });
      }
      const results = await Upk.findAndCountAll({
        where: whereParam,
        include: [{ model: SudinKabKota, as: "sudinKabKota", required: true }],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateUser = async (req, res, next) => {
    const body = req.body;
    const recordId = req.params.userId;
    const upkId = body.upkId;
    try {
      let dataParams = {
        name: body.name,
        nik: body.nik,
        isActive: body.isActive,
        password: body.password,
        role: body.role,
        logisticrole: body.logisticrole,
        email: body.email,
        phone: body.phone,
        provinceId: body.provinceId,
        upkId: body.upkId,
        sudinKabKotaId: body.sudinKabKotaId,
        address: body.address
      };
      let record;
      if (recordId) {
        record = await User.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        dataParams.password = bcryptService.password(body.password);
        dataParams.updatedBy = req.user.id;
        Object.assign(record, dataParams);
        await record.save();
      } else {
        dataParams.createdBy = req.user.id;
        record = await User.create(dataParams);
      }

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteUser = async (req, res, next) => {
    const recordId = req.params.userId;
    try {
      const result = await User.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getUser = async (req, res, next) => {
    const recordId = req.params.userId;
    let { name } = req.query;
    try {
      const record = await User.findByPk(recordId, {
        include: [
          { model: Upk },
          { model: SudinKabKota, as: "sudinKabKota" },
          { model: Province }
        ]
      });
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllUsers = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      const results = await User.findAndCountAll({
        where: {
          [Op.or]: [
            {
              nik: {
                [Op.like]: `%${keyword}%`
              }
            },
            {
              name: {
                [Op.like]: `%${keyword}%`
              }
            },
          ]
        },
        // include: [{ model: Upk, required: true }],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllUsersByRole = async (req, res, next) => {
    let { page, limit, keyword, role } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      const results = await User.findAndCountAll({
        where: {
          role: role,
          [Op.or]: [
            {
              nik: {
                [Op.like]: `%${keyword}%`
              }
            },
            {
              name: {
                [Op.like]: `%${keyword}%`
              }
            }
          ]
        },
        // include: [{ model: Upk, required: true }],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdatePatient = async (req, res, next) => {
    const {
      name,
      nik,
      addressKTP,
      addressDomicile,
      dateBirth,
      gender,
      phone,
      statusPatient,
      weight,
      height,
      namaPmo,
      hubunganPmo,
      noHpPmo,
      lsmPenjangkau,
      kelompokPopulasi,
      upkId,
      provinceId,
      sudinKabKotaId
    } = req.body;
    const recordId = req.params.patientId;
    let record;
    try {
      let dataParams = {
        name,
        nik,
        addressKTP,
        addressDomicile,
        dateBirth,
        gender,
        phone,
        statusPatient,
        weight,
        height,
        namaPmo,
        hubunganPmo,
        noHpPmo,
        upkId,
      };
      if (recordId) {
        record = await Patient.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        if (record.userId) {
          await User.update(
            {
              name,
              nik,
              phone,
              provinceId,
              sudinKabKotaId,
              upkId,
              updatedBy: req.user.id
            },
            {
              where: {
                id: record.userId
              }
            }
          );
        }
        const query = `UPDATE patients SET fullname = '${name}', nik = '${nik}', "address_KTP" = '${addressKTP}',
          address_domicile = '${addressDomicile}', date_birth = '${dateBirth}', gender = '${gender}', phone = '${phone}',
          status_patient = '${statusPatient}', weight = ${weight}, height = ${height}, nama_pmo = '${namaPmo}',
          hubungan_pmo = '${hubunganPmo}', no_hp_pmo = '${noHpPmo}', upk_id = ${upkId},
          kelompok_populasi = '${kelompokPopulasi}', lsm_penjangkau = '${lsmPenjangkau}', updated_by = ${req.user.id} WHERE id = ${recordId}
        ;`;
        await sequelize.query(query, {
          type: sequelize.QueryTypes.UPDATE
        });
      } else {
        dataParams.createdBy = req.user.id;
        const user = await User.create({
          name,
          nik,
          isActive: false,
          role: ENUM.USER_ROLE.PATIENT,
          phone,
          provinceId,
          sudinKabKotaId,
          upkId,
          createdBy: req.user.id
        });
        const query = `INSERT INTO patients (
          fullname, nik, "address_KTP", address_domicile, date_birth, gender, phone,
          status_patient, weight, height, nama_pmo, hubungan_pmo, no_hp_pmo,
          upk_id, kelompok_populasi, lsm_penjangkau, user_id
        ) VALUES (
          '${name}', '${nik}', '${addressKTP}', '${addressDomicile}', '${dateBirth}', '${gender}', '${phone}',
          '${statusPatient}', ${weight}, ${height}, '${namaPmo}', '${hubunganPmo}', '${noHpPmo}',
          ${upkId}, '${kelompokPopulasi}', '${lsmPenjangkau}', ${user.id}
        );`;
        await sequelize.query(query, {
          type: sequelize.QueryTypes.INSERT
        });
      }
      record = await Patient.findOne({
        where: {
          nik
        }
      });

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deletePatient = async (req, res, next) => {
    const recordId = req.params.patientId;
    try {
      const result = await Patient.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getPatient = async (req, res, next) => {
    const recordId = req.params.patientId;
    try {
      const record = await Patient.findByPk(recordId, {
        include: [{ model: Upk }]
      });
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllPatients = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              nik: {
                [Op.startsWith]: keyword
              }
            },
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const results = await Patient.findAndCountAll({
        where: whereParam,
        include: [{ model: Upk }],
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  const addOrUpdateProducer = async (req, res, next) => {
    const body = req.body;
    const recordId = req.params.producerId;
    try {
      let dataParams = {
        name: body.name,
        medicineType: body.medicineType,
      };
      let record;
      if (recordId) {
        record = await Producer.findByPk(recordId);
        if (!record) {
          throw {
            status: 400,
            message: "record_not_found"
          };
        }
        dataParams.updatedBy = req.user.id;
        Object.assign(record, dataParams);
        await record.save();
      } else {
        dataParams.createdBy = req.user.id;
        record = await Producer.create(dataParams);
      }

      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const deleteProducer = async (req, res, next) => {
    const recordId = req.params.producerId;
    try {
      const result = await Producer.destroy({
        where: {
          id: recordId
        }
      });
      res.status(200).json({
        data: result
      });
    } catch (error) {
      next(error);
    }
  };

  const getProducer = async (req, res, next) => {
    const recordId = req.params.producerId;
    try {
      const record = await Producer.findByPk(recordId, {});
      if (!record) {
        throw {
          status: 400,
          message: "record_not_found"
        };
      }
      res.status(200).json({
        data: record
      });
    } catch (error) {
      next(error);
    }
  };

  const getAllProducers = async (req, res, next) => {
    let { page, limit, keyword } = req.query;
    page = page ? page : 0;
    limit = limit ? limit : 20;
    try {
      let whereParam = {};
      if (keyword) {
        Object.assign(whereParam, {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: "%" + keyword + "%"
              }
            }
          ]
        });
      }
      const results = await Producer.findAndCountAll({
        include: [].concat(QueryService.includeCreated()),
        where: whereParam,
        limit,
        offset: limit * page,
        order: [["updatedAt", "DESC"]]
      });

      res.send({
        status: 200,
        data: results.rows,
        paging: {
          page: Number(page),
          size: results.rows.length,
          total: results.count
        }
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    //medicine ops
    addOrUpdateMedicine, //done
    deleteMedicine, //done
    getMedicine, //done
    getAllMedicines, //done
    getAllMedicineBrands, //done
    //brands ops
    addOrUpdateBrand, //done
    getBrand, //done
    deleteBrand, //done
    getAllBrands, //done
    // //regiment ops
    addOrUpdateRegiment, //done
    deleteRegiment, //done
    getRegiment, //done
    getAllRegiments, //done
    // //Prescription ops
    addOrUpdatePrescription, //done
    deletePrescription, //done
    getPrescription, //done
    getAllPrescriptions, //done
    // //province ops
    addOrUpdateProvince, //done
    deleteProvince, //done
    getProvince, //done
    getAllProvinces, //done
    // //sudin kab/kota ops
    addOrUpdateSudin, //done
    deleteSudin, //done
    getSudin, //done
    getAllSudin, //done
    // //upk ops
    addOrUpdateUpk, //done
    deleteUpk, //done
    getUpk, //done
    getAllUpk, //done
    // //patient ops
    addOrUpdatePatient,
    deletePatient,
    getPatient,
    getAllPatients,
    // //user ops
    addOrUpdateUser, //done
    deleteUser, //done
    getUser, //done
    getAllUsers, //done
    getAllUsersByRole,
    //setting ops
    // //Producer ops
    addOrUpdateProducer, //done
    deleteProducer, //done
    getProducer, //done
    getAllProducers, //done
  };
};

module.exports = MasterDataController;
