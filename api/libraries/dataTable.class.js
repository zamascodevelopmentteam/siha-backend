const _ = require("lodash");
const moment = require("moment");

const Sequelize = require("sequelize");
const { Op } = require("sequelize");

module.exports = class DataTable {
  constructor(model, req) {
    const { page, limit, order, keyword, where } = req.query;

    this.model = model;
    this.page = page ? page : 0;
    this.limit = limit ? limit : 20;
    this.offset = this.page * this.limit;
    this.keyword = keyword;
    this.isFiltered = keyword ? true : false;

    this.modelIdColumnName = "*";
    this.where = {};
    this.whereFilter = {};
    this.include = [];
    this.attributes = [];
    if (order) {
      this.order = typeof order === "string" ? JSON.parse(order) : order;
    } else {
      this.order = [["updatedAt", "DESC"]];
    }
    if (where) {
      this.where = typeof where === "string" ? JSON.parse(where) : where;
    }

    this.result = {
      paging: {
        page: 0,
        size: 0,
        totalFiltered: 0,
        total: 0
      },
      data: []
    };
  }

  setLogging(logging) {
    if (logging === true) {
      this.logging = console.log;
    } else if (logging) {
      this.logging = logging;
    }
  }

  setPage(page) {
    this.page = page ? page : 0;
  }

  setLimit(limit) {
    this.limit = limit ? limit : 20;
  }

  setKeyword(keyword) {
    this.keyword = keyword;
    this.isFiltered = keyword ? true : false;
  }

  setModelIdColumnName(columnName) {
    this.modelIdColumnName = columnName;
  }

  addWhereParams(params) {
    Object.assign(this.where, params);
  }

  setFilterParams(params) {
    Object.assign(this.whereFilter, params);
  }

  setColumnsToSearch(arrColumnNames) {
    if (!_.isArray(arrColumnNames) || arrColumnNames.length === 0) return;
    Object.assign(this.whereFilter, {
      [Op.or]: arrColumnNames.map(itemName => ({
        [itemName]: {
          [Op.iLike]: "%" + this.keyword + "%"
        }
      }))
    });
  }

  setIncludeParams(params) {
    this.include = params;
  }

  setAttributeParams(params) {
    this.attributes = params;
  }

  setOrderParams(params) {
    this.order = params;
  }

  async buildResult() {
    let whereFinal = Object.assign({}, this.where);
    if (this.isFiltered) {
      whereFinal = Object.assign(whereFinal, this.whereFilter);
    }

    const results = await this.model.findAndCountAll({
      where: whereFinal,
      limit: this.limit,
      include: this.include,
      offset: this.offset,
      order: this.order,
      logging: this.logging ? this.logging : undefined
    });

    let countTotal = results.count;
    if (this.isFiltered) {
      countTotal = await this.model.count({
        where: this.where,
        include: this.include,
        col: this.modelIdColumnName
      });
    }

    this.result = {
      paging: {
        keyword: this.keyword ? this.keyword : undefined,
        page: Number(this.page),
        size: results.rows.length,
        total: this.isFiltered ? countTotal : results.count,
        totalFiltered: this.isFiltered ? results.count : undefined
      },
      data: results.rows
    };
    return this.result;
  }

  getResult() {
    return this.result;
  }
};
