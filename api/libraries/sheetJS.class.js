const _ = require("lodash");
const moment = require("moment");
const path = require("path");
const XLSX = require("xlsx");

const ENUM = require("../../config/enum");
const VariableService = require("../services/variable.service")();

module.exports = class SheetJS {
  constructor(payload) {
    const { filePath, title } = payload;

    this.ws = {};
    this.title = title || "No-title";
    this.fileName = `${title}_${moment()
      .local()
      .format("DD-MM-YYYY HH_mm")}.xlsx`;
    this.filePath =
      filePath ||
      path.join(VariableService.getTmpDownloadPath(), this.fileName);
  }

  setCellValue(cellLocation, value, type) {
    // please read at https://github.com/SheetJS/sheetjs for more
    this.ws[cellLocation] = {
      v: value,
      t: type || "s"
    };
  }

  setFilePath(filePath) {
    this.filePath = filePath;
  }

  getRawWorksheet() {
    return this.ws;
  }

  addWorksheetData(wsObject) {
    Object.assign(this.ws, wsObject);
  }

  getXLSXUtils() {
    return XLSX.utils;
  }

  getMaxCellLocation() {
    let keys = _.keys(this.ws);
    keys = keys.sort();
    const maxColumnIndex = keys[keys.length - 1].replace(/[0-9]/g, "");
    const maxRowIndex = Math.max(
      ...keys.map(i => parseInt(i.replace(/\D/g, "")))
    );
    return `${maxColumnIndex}${maxRowIndex}`;
  }

  generate(sheetName) {
    var wb = XLSX.utils.book_new();

    if (!this.ws["!ref"]) {
      if (_.keys(this.ws).length > 1) {
        this.ws["!ref"] = `A1:${this.getMaxCellLocation()}`;
      } else {
        this.ws["!ref"] = "A1:Z50";
      }
    }

    XLSX.utils.book_append_sheet(wb, this.ws, sheetName || "Sheet1");
    XLSX.writeFile(wb, this.filePath);
    return {
      filePath: this.filePath,
      fileName: this.fileName
    };
  }
};
