CREATE TYPE "populate_type_ims" AS ENUM (
  'WPS',
  'PENASUN',
  'IBU_HAMIL'
  'PPS',
  'WARIA',
  'LSL',
  'ODHA',
  'LAIN'
);

CREATE TYPE "reason_visit_ims" AS ENUM (
  'SKRINING',
  'SAKIT',
  'FOLLOWUP'
);

CREATE TABLE "ims_visits" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "populate_type" populate_type_ims,
  "lsm_referral" varchar,
  "reason_visit" reason_visit_ims,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

ALTER TYPE role ADD VALUE 'DOCTOR';

CREATE TYPE "complaint_ims" AS ENUM (
  'DUH_TUBUH',
  'GATAL',
  'KENCING_SAKIT',
  'NYERI_PERUT',
  'LECET',
  'BINTIL_SAKIT',
  'LUKA_ULKUS',
  'JENGGER',
  'BENJOLAN',
  'TIDAK_ADA'
);

ALTER TABLE ims_visits ADD COLUMN complaint complaint_ims;

CREATE TYPE "pregnancy_stage" AS ENUM (
  'TRIMESTER_1',
  'TRIMESTER_2',
  'TRIMESTER_3'
);

ALTER TABLE ims_visits
ADD COLUMN is_pregnant boolean default FALSE,
ADD COLUMN pregnancy_stage pregnancy_stage,
ADD COLUMN upk_id integer,
ADD COLUMN physical_examination jsonb default '{}'::jsonb;

CREATE TYPE "ims_lab_rn" AS ENUM (
  'REAKTIF',
  'NON_REAKTIF',
  'INVALID'
);

CREATE TYPE "ims_lab_rpr_vdrl_titer" AS ENUM (
  '0',
  '1/2',
  '1/4',
  '1/8',
  '1/16',
  '1/32'
);

CREATE TABLE "ims_visit_labs" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "pmn_uretra_serviks" boolean DEFAULT FALSE,
  "diplokokus_intrasel_uretra_serviks" boolean DEFAULT FALSE,
  "pmn_anus" boolean DEFAULT FALSE,
  "diplokokus_intrasel_anus" boolean DEFAULT FALSE,
  "t_vaginalis" boolean DEFAULT FALSE,
  "kandida" boolean DEFAULT FALSE,
  "ph" boolean DEFAULT FALSE,
  "sniff_test" boolean DEFAULT FALSE,
  "clue_cells" boolean DEFAULT FALSE,
  "rpr_vdrl" jsonb default '{}'::jsonb,
  "tpha_tppa" jsonb default '{}'::jsonb,
  "rpr_vdrl_titer" ims_lab_rpr_vdrl_titer,
  "etc" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

CREATE TABLE "ims_prescriptions" (
  "id" BIGSERIAL PRIMARY KEY,
  "patient_id" integer,
  "visit_id" integer,
  "note" varchar,
  "date_given_medicine" date,
  "condom" integer,
  "lubricant" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_at" timestamp
);

CREATE TABLE "ims_prescription_medicines" (
  "id" BIGSERIAL PRIMARY KEY,
  "prescription_id" integer,
  "medicine_id" integer,
  "medicine_name" varchar,
  "total_qty" integer,
  "total_days" integer,
  "note" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

ALTER TABLE ims_visit_labs
ADD COLUMN diagnosis_syndrome jsonb default '{}'::jsonb,
ADD COLUMN diagnosis_klinis jsonb default '{}'::jsonb,
ADD COLUMN diagnosis_lab jsonb default '{}'::jsonb;

ALTER TABLE ims_visits DROP COLUMN populate_type;
DROP TYPE populate_type_ims;
ALTER TYPE ims_lab_rpr_vdrl_titer ADD VALUE 'EMPTY';
ALTER TABLE ims_visits ADD COLUMN reason_referral varchar;
ALTER TABLE ims_visit_labs ALTER COLUMN pmn_uretra_serviks DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN diplokokus_intrasel_uretra_serviks DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN pmn_anus DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN diplokokus_intrasel_anus DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN t_vaginalis DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN kandida DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN ph DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN sniff_test DROP DEFAULT;
ALTER TABLE ims_visit_labs ALTER COLUMN clue_cells DROP DEFAULT;

ALTER TABLE ims_visit_labs ADD COLUMN is_tested boolean;

CREATE TYPE "lsm" AS ENUM (
  'SPIRITIA',
  'IAC',
  'UNFPA',
  'IPPI',
  'YPI',
  'PKVHI',
  'PKBI',
  'KADER KESEHATAN',
  'DOTS/TB',
  'LAPAS RUTAN',
  'HEPATITIS',
  'TB',
  'KIA',
  'POLI_LAINNYA'
);

ALTER TABLE patients
ADD COLUMN lsm_penjangkau lsm,
ADD COLUMN kelompok_populasi kelompok_populasi;
ALTER TABLE ims_visits DROP COLUMN lsm_referral;
ALTER TABLE ims_visits ALTER COLUMN reason_visit TYPE varchar;
ALTER TABLE ims_visits
ADD COLUMN is_iva_tested boolean default FALSE,
ADD COLUMN iva_test boolean;
ALTER TABLE ims_prescriptions ADD COLUMN counseling varchar;

CREATE TABLE "alkes_medicines" (
  "id" BIGSERIAL PRIMARY KEY,
  "medicine_id" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

ALTER TABLE arv_medicines ADD COLUMN is_ims boolean;
ALTER TABLE patients DROP COLUMN kelompok_populasi;
ALTER TABLE patients ADD COLUMN kelompok_populasi kelompok_populasi ARRAY;
ALTER TABLE ims_prescriptions ADD COLUMN is_draft BOOLEAN;

ALTER TABLE ims_prescriptions ADD COLUMN order_id INTEGER;
ALTER TABLE prescriptions ADD COLUMN order_id INTEGER;
ALTER TABLE orders
ADD COLUMN general_order_id INTEGER,
ADD COLUMN total_patient INTEGER;
alter table inventory_usage drop constraint inventory_usage_test_ims_id_fkey;
alter table inventory_usage add constraint inventory_usage_test_ims_id_fkey foreign key (test_ims_id) REFERENCES ims_visits(id);
