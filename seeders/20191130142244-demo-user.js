"use strict";

const fsPromises = require("fs").promises;
const path = require("path");
const sequelize = require("../config/database");

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return Promise.all([
      fsPromises.readFile(
        path.resolve("dummy", "01_01_central_ministries.sql"),
        "utf8"
      ),
      fsPromises.readFile(path.resolve("dummy", "01_02_provinces.sql"), "utf8"),
      fsPromises.readFile(
        path.resolve("dummy", "01_03_sudin_kab_kota.sql"),
        "utf8"
      ),
      fsPromises.readFile(path.resolve("dummy", "01_04_upk.sql"), "utf8"),
      fsPromises.readFile(path.resolve("dummy", "01_05_users.sql"), "utf8")
    ]).then(arrFiles => {
      var fileObjects = {
        centralMinistry: arrFiles[0],
        provinces: arrFiles[1],
        sudinKabKota: arrFiles[2],
        upk: arrFiles[3],
        users: arrFiles[4]
      };
      return Promise.resolve(true)
        .then(data => {
          return sequelize.query(fileObjects.centralMinistry, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.provinces, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.sudinKabKota, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.upk, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.users, {
            type: Sequelize.QueryTypes.RAW
          });
        });
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return Promise.resolve(true)
      .then(data => {
        return queryInterface.bulkDelete("users", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("upk", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("sudin_kab_kota", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("provinces", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("central_ministry", null, {});
      });
  }
};
