"use strict";

const fsPromises = require("fs").promises;
const path = require("path");
const sequelize = require("../config/database");

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return Promise.all([
      fsPromises.readFile(path.resolve("dummy", "02_01_medicines.sql"), "utf8"),
      fsPromises.readFile(path.resolve("dummy", "02_02_brands.sql"), "utf8"),
      fsPromises.readFile(
        path.resolve("dummy", "02_03_arv_medicines.sql"),
        "utf8"
      ),
      fsPromises.readFile(
        path.resolve("dummy", "02_04_non_arv_medicines.sql"),
        "utf8"
      ),
      fsPromises.readFile(path.resolve("dummy", "02_05_regiments.sql"), "utf8"),
      fsPromises.readFile(
        path.resolve("dummy", "02_06_regiment_medicines.sql"),
        "utf8"
      )
    ]).then(arrFiles => {
      var fileObjects = {
        medicines: arrFiles[0],
        brands: arrFiles[1],
        arvMedicines: arrFiles[2],
        nonArvMedicines: arrFiles[3],
        regiments: arrFiles[4],
        regimentMedicines: arrFiles[5]
      };
      return Promise.resolve(true)
        .then(data => {
          return sequelize.query(fileObjects.medicines, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.brands, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.arvMedicines, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.nonArvMedicines, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.regiments, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.regimentMedicines, {
            type: Sequelize.QueryTypes.RAW
          });
        });
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return Promise.resolve(true)
      .then(data => {
        return queryInterface.bulkDelete("regiment_medicines", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("regiments", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("non_arv_medicines", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("arv_medicines", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("brands", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("medicines", null, {});
      });
  }
};
