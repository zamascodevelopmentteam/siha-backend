"use strict";

const fsPromises = require("fs").promises;
const path = require("path");
const sequelize = require("../config/database");

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return Promise.all([
      fsPromises.readFile(path.resolve("dummy", "04_01_patients.sql"), "utf8"),
      fsPromises.readFile(path.resolve("dummy", "04_02_visits.sql"), "utf8"),
      fsPromises.readFile(path.resolve("dummy", "04_03_tes_hiv.sql"), "utf8"),
      fsPromises.readFile(path.resolve("dummy", "04_04_tes_ims.sql"), "utf8")
    ]).then(arrFiles => {
      var fileObjects = {
        patients: arrFiles[0],
        visits: arrFiles[1],
        test_hiv: arrFiles[2],
        test_ims: arrFiles[3]
      };
      return Promise.resolve(true)
        .then(data => {
          return sequelize.query(fileObjects.patients, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.visits, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.test_hiv, {
            type: Sequelize.QueryTypes.RAW
          });
        })
        .then(data => {
          return sequelize.query(fileObjects.test_ims, {
            type: Sequelize.QueryTypes.RAW
          });
        });
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return Promise.resolve(true)
      .then(data => {
        return queryInterface.bulkDelete("test_ims", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("test_hiv", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("visits", null, {});
      })
      .then(data => {
        return queryInterface.bulkDelete("patients", null, {});
      });
  }
};
