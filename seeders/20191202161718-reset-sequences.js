"use strict";

const fsPromises = require("fs").promises;
const path = require("path");
const sequelize = require("../config/database");

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return Promise.all([
      fsPromises.readFile(
        path.resolve("dummy", "05_01_reset_sequences.sql"),
        "utf8"
      )
    ]).then(arrFiles => {
      var fileObjects = {
        reset_sequences: arrFiles[0]
      };
      return Promise.resolve(true).then(data => {
        return sequelize.query(fileObjects.reset_sequences, {
          type: Sequelize.QueryTypes.RAW
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return Promise.resolve(true);
  }
};
