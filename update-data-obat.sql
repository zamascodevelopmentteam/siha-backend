INSERT INTO "medicines"
(name, code_name, medicine_type, stock_unit_type, package_unit_type, package_multiplier) VALUES
('Benzatin Penicilin 2,4 juta IU', 'Benzatin Penicilin 2,4 juta IU', 'ARV', 'VIAL', 'BOX', 10),
('Fluconazol 150mg', 'Fluconazol 150mg', 'ARV', 'KAPSUL', 'BOX', 10),
('Azithromicyn 1000mg+ Cefixime 400mg / Kombipak', 'Azithromicyn 1000mg+ Cefixime 400mg / Kombipak', 'ARV', 'PAKET', 'BOX', 10),
('Cotrimoxazole 960 mg', 'Cotrimoxazole 960 mg', 'ARV', 'KAPSUL', 'BOX', 10),
('Micafungin/micamin 50mg', 'Micafungin/micamin 50mg', 'ARV', 'VIAL', 'VIAL', null),
('Isoniazid 300mg / INH', 'Isoniazid 300mg / INH', 'ARV', 'TABLET', 'BOX', 100),
('Vitamin B6 25mg/ Piridoksin', 'Vitamin B6 25mg/ Piridoksin', 'ARV', 'TABLET', 'BOX', 100),
('Amphotericin B', 'Amphotericin B', 'ARV', 'VIAL', 'VIAL', null),
('Rapid test HIV 1 SD Bioline', 'Rapid test HIV 1 SD Bioline', 'NON_ARV', 'TEST', 'BOX', 25),
('Rapid HIV 2 KHB Diagnostik', 'Rapid HIV 2 KHB Diagnostik', 'NON_ARV', 'TEST', 'BOX', 50),
('Rapid tes HIV 3 Diagnostar', 'Rapid tes HIV 3 Diagnostar', 'NON_ARV', 'TEST', 'BOX', 25),
('Rapid Test Sifilis Bio Sensor', 'Rapid Test Sifilis Bio Sensor', 'NON_ARV', 'TEST', 'BOX', 25),
('Rapid Test Sifilis Trepocheck', 'Rapid Test Sifilis Trepocheck', 'NON_ARV', 'TEST', 'BOX', 25),
('RPR Sifilis', 'RPR Sifilis', 'NON_ARV', 'TEST', 'BOX', 100),
('DBS Collection', 'DBS Collection', 'NON_ARV', 'TEST', 'KIT', 20),
('Cryptococcus', 'Cryptococcus', 'NON_ARV', 'TEST', 'PAKET', 50),
('Tritest/Trucount CD4 Open System', 'Tritest/Trucount CD4 Open System', 'NON_ARV', 'TEST', 'KIT', 50),
('FacsCount CD4 Close System', 'FacsCount CD4 Close System', 'NON_ARV', 'TEST', 'KIT', 50),
('FacsCount Control kit', 'FacsCount Control kit', 'NON_ARV', 'KIT', 'KIT', null),
('BD Facs Clean 5L', 'BD Facs Clean 5L', 'NON_ARV', 'KIT', 'KIT', null),
('BD Facs Rinse 5L', 'BD Facs Rinse 5L', 'NON_ARV', 'KIT', 'KIT', null),
('BD FacsFlow Fluid 20L', 'BD FacsFlow Fluid 20L', 'NON_ARV', 'KIT', 'KIT', null),
('Facs Lysing Solution', 'Facs Lysing Solution', 'NON_ARV', 'KIT', 'KIT', null),
('Calibrite BD', 'Calibrite BD', 'NON_ARV', 'KIT', 'KIT', null),
('Sarung Tangan', 'Sarung Tangan', 'ALKES', 'BUAH', 'BOX', 100),
('Kondom', 'Kondom', 'ALKES', 'BUAH', 'BOX', 144),
('Lubricant', 'Lubricant', 'ALKES', 'BUAH', 'BOX', 20),
('Alat Suntik Tuberculin / steril', 'Alat Suntik Tuberculin / steril', 'ALKES', 'BUAH', 'BOX', 100),
('Alcohol Swab (BD Swab)', 'Alcohol Swab (BD Swab)', 'ALKES', 'BUAH', 'BOX', 100);

/*Ini untuk masukin obat di atas yg kategori nya ARV ke table arv*/
INSERT INTO arv_medicines (medicine_id) VALUES
(15026),
(15027),
(15028),
(15029),
(15030),
(15031),
(15032),
(15033);

/*Ini untuk masukin obat di atas yg kategori nya NON ARV ke table non arv*/
INSERT INTO non_arv_medicines (medicine_id) VALUES
(15034),
(15035),
(15036),
(15037),
(15038),
(15039),
(15040),
(15041),
(15042),
(15043),
(15044),
(15045),
(15046),
(15047),
(15048),
(15049);

/*Ini untuk masukin obat di atas yg kategori nya ALKES ke table alkes*/
INSERT INTO alkes_medicines (medicine_id) VALUES
(15050),
(15051),
(15052),
(15053),
(15054);
