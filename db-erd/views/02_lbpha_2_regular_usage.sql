CREATE OR REPLACE VIEW lbpha2_regular_usage AS
SELECT
	upk_medicines.upk_id,
	upk_medicines.sudin_kab_kota_id,
	upk_medicines.province_id,
	upk_medicines.id AS medicine_id,
	upk_medicines.code_name,
	upk_medicines.name,
	(
		COALESCE(table_regular_usage.stock_usage_regular, (0)::bigint))::integer AS stok_keluar_reguler,
	upk_medicines.package_multiplier,
	upk_medicines.package_unit_type,
	upk_medicines.stock_unit_type,
	(
		COALESCE(table_regular_usage.package_usage_regular, (0)::bigint))::integer AS paket_keluar_reguler,
    date_part('month'::text, now()) as indeks_bulan,
    date_part('year'::text, now()) as tahun
FROM (
									SELECT
										medicines.id,
										medicines.code_name,
										medicines.name,
										medicines.package_multiplier,
										medicines.package_unit_type,
										medicines.stock_unit_type,
										upk.id AS upk_id,
										upk.sudin_kab_kota_id,
										upk.province_id
									FROM (medicines
									CROSS JOIN upk)) upk_medicines
							LEFT JOIN (
								SELECT
									prescriptions.medicine_id AS id,
									count(patient_visits.patient_id) AS package_usage_regular,
									(count(patient_visits.patient_id) * prescriptions.package_multiplier) AS stock_usage_regular,
									patient_visits.upk_id
								FROM (((
											SELECT
												visits.id AS visit_id,
												visits.patient_id,
												visits.upk_id
											FROM (visits
												JOIN (
													SELECT
														patients_1.id
													FROM
														patients patients_1) patients ON ((patients.id = visits.patient_id)))) patient_visits
											JOIN (
												SELECT
													treatments_1.id,
													treatments_1.visit_id,
													treatments_1.prescription_id,
													treatments_1.is_on_transit
												FROM
													treatments treatments_1
												WHERE ((treatments_1.prescription_id IS NOT NULL)
													AND(date_part('month'::text, treatments_1.tgl_pemberian_obat) = date_part('month'::text, now()))
													AND(date_part('year'::text, treatments_1.tgl_pemberian_obat) = date_part('year'::text, now()))
													AND(treatments_1.is_on_transit IS NOT TRUE))) treatments ON ((patient_visits.visit_id = treatments.visit_id)))
										JOIN (
											SELECT
												prescriptions_1.id, prescription_medicines.amount, prescription_medicines.medicine_id, medicines.package_multiplier
											FROM ((prescriptions prescriptions_1
													JOIN prescription_medicines ON ((prescriptions_1.id = prescription_medicines.prescription_id)))
												JOIN medicines ON ((medicines.id = prescription_medicines.medicine_id)))
										WHERE ((prescriptions_1.is_draft IS NOT TRUE)
											AND(prescription_medicines.jumlah_hari <= 30))) prescriptions ON ((prescriptions.id = treatments.prescription_id)))
							GROUP BY
								prescriptions.medicine_id,
								prescriptions.package_multiplier,
								patient_visits.upk_id) table_regular_usage ON (((upk_medicines.id = table_regular_usage.id)
									AND(table_regular_usage.upk_id = upk_medicines.upk_id)))
					ORDER BY
						upk_medicines.id,
						upk_medicines.upk_id;
