CREATE OR REPLACE VIEW lbpha2_upk AS

SELECT lbpha2_regular_usage.upk_id,
   lbpha2_regular_usage.medicine_id AS medicine_id,
   lbpha2_regular_usage.code_name,
   lbpha2_regular_usage.name,
   (COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer AS stok_awal,
   (COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer AS stok_diterima,
   (COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer AS stok_keluar,
   (COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer AS stok_kadaluarsa,
   ((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * lbpha2_regular_usage.package_multiplier) AS stok_selisih_fisik,
   ((((COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer + (COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer) - ((COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer + (COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer)) + ((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * lbpha2_regular_usage.package_multiplier)) AS stok_akhir,
   (COALESCE(table_column_g.package_qty_col_g, (0)::bigint))::integer AS stok_akhir_botol,
   (COALESCE(table_inv_on_hand.stock_qty_on_hand, (0)::bigint))::integer AS stock_on_hand,
   (COALESCE(table_inv_on_hand.package_qty_on_hand, (0)::bigint))::integer AS package_on_hand,
   (COALESCE(lbpha2_regular_usage.stok_keluar_reguler, (0)::bigint))::integer AS stok_keluar_reguler,
   (COALESCE(lbpha2_regular_usage.paket_keluar_reguler, (0)::bigint))::integer AS paket_keluar_reguler,
   lbpha2_regular_usage.package_multiplier,
   lbpha2_regular_usage.package_unit_type,
   lbpha2_regular_usage.stock_unit_type,
   table_inv_on_hand.inv_string_ed_on_hand
  FROM
    lbpha2_regular_usage as lbpha2_regular_usage
    LEFT JOIN ( SELECT inventory_logs.medicine_id AS id,
           sum(inventory_logs.after_stock_qty) AS stock_qty_col_a,
           sum(inventory_logs.after_pkg_quantity) AS package_qty_col_a,
           inventory_logs.upk_id
          FROM inventory_logs
         WHERE ((inventory_logs.medicine_id IS NOT NULL) AND (inventory_logs.upk_id IS NOT NULL) AND (inventory_logs.id IN ( SELECT max(inventory_logs_1.id) AS max
                  FROM inventory_logs inventory_logs_1
                 WHERE (inventory_logs_1.activity_date <= (((date_trunc('month'::text, now()) - '1 mon'::interval) + '1 mon'::interval))::date)
                 GROUP BY inventory_logs_1.inventory_id)))
         GROUP BY inventory_logs.medicine_id, inventory_logs.upk_id) table_column_a ON (((lbpha2_regular_usage.medicine_id = table_column_a.id) AND (table_column_a.upk_id = lbpha2_regular_usage.upk_id)))
    LEFT JOIN ( SELECT inventory_logs.medicine_id AS id,
           sum(inventory_logs.after_stock_qty) AS stock_qty_col_b,
           sum(inventory_logs.after_pkg_quantity) AS package_qty_col_b,
           inventory_logs.upk_id
          FROM inventory_logs
         WHERE (((inventory_logs.activity)::text = 'INSERT'::text) AND (inventory_logs.medicine_id IS NOT NULL) AND (inventory_logs.upk_id IS NOT NULL) AND (inventory_logs.activity_date >= (((date_trunc('month'::text, now()) - '1 mon'::interval) + '1 mon'::interval))::date) AND (inventory_logs.activity_date <= (((date_trunc('month'::text, now()) + '1 mon'::interval) - '1 day'::interval))::date))
         GROUP BY inventory_logs.medicine_id, inventory_logs.upk_id) table_column_b ON (((lbpha2_regular_usage.medicine_id = table_column_b.id) AND (table_column_b.upk_id = lbpha2_regular_usage.upk_id)))
    LEFT JOIN ( SELECT prescriptions.medicine_id AS id,
           sum(prescriptions.amount) AS stock_qty_col_c,
           patient_visits.upk_id
          FROM ((( SELECT visits.id AS visit_id,
                   visits.patient_id,
                   visits.upk_id
                  FROM (visits
                    JOIN ( SELECT patients_1.id
                          FROM patients patients_1) patients ON ((patients.id = visits.patient_id)))) patient_visits
            JOIN ( SELECT treatments_1.id,
                   treatments_1.visit_id,
                   treatments_1.prescription_id,
                   treatments_1.is_on_transit
                  FROM treatments treatments_1
                 WHERE ((treatments_1.prescription_id IS NOT NULL) AND (date_part('month'::text, treatments_1.tgl_pemberian_obat) = date_part('month'::text, now())) AND (date_part('year'::text, treatments_1.tgl_pemberian_obat) = date_part('year'::text, now())))) treatments ON ((patient_visits.visit_id = treatments.visit_id)))
            JOIN ( SELECT prescriptions_1.id,
                   prescription_medicines.amount,
                   prescription_medicines.medicine_id
                  FROM (prescriptions prescriptions_1
                    JOIN prescription_medicines ON ((prescriptions_1.id = prescription_medicines.prescription_id)))
                 WHERE (prescriptions_1.is_draft IS NOT TRUE)) prescriptions ON ((prescriptions.id = treatments.prescription_id)))
         GROUP BY prescriptions.medicine_id, patient_visits.upk_id) table_column_c ON (((lbpha2_regular_usage.medicine_id = table_column_c.id) AND (table_column_c.upk_id = lbpha2_regular_usage.upk_id)))
    LEFT JOIN ( SELECT inventories.medicine_id AS id,
           sum(inventories.stock_qty) AS stock_qty_col_d,
           sum(inventories.package_quantity) AS package_qty_col_d,
           inventories.upk_id
          FROM inventories
         WHERE ((date_part('month'::text, inventories.expired_date) = date_part('month'::text, now())) AND (date_part('year'::text, inventories.expired_date) = date_part('year'::text, now())) AND (inventories.upk_id IS NOT NULL))
         GROUP BY inventories.medicine_id, inventories.upk_id) table_column_d ON (((lbpha2_regular_usage.medicine_id = table_column_d.id) AND (table_column_d.upk_id = lbpha2_regular_usage.upk_id)))
    LEFT JOIN ( SELECT inventories.medicine_id AS id,
           sum(inventory_adjustment.package_quantity) AS package_qty_col_e,
           inventories.upk_id
          FROM (inventories
            JOIN inventory_adjustment ON ((inventory_adjustment.inventory_id = inventories.id)))
         WHERE ((date_part('month'::text, inventory_adjustment.created_at) = date_part('month'::text, now())) AND (date_part('year'::text, inventory_adjustment.created_at) = date_part('year'::text, now())) AND (inventories.upk_id IS NOT NULL))
         GROUP BY inventories.medicine_id, inventories.upk_id) table_column_e ON (((lbpha2_regular_usage.medicine_id = table_column_e.id) AND (table_column_e.upk_id = lbpha2_regular_usage.upk_id)))
    LEFT JOIN ( SELECT inventory_logs.medicine_id AS id,
           sum(inventory_logs.after_stock_qty) AS stock_qty_col_g,
           sum(inventory_logs.after_pkg_quantity) AS package_qty_col_g,
           inventory_logs.upk_id
          FROM inventory_logs
         WHERE ((inventory_logs.medicine_id IS NOT NULL) AND (inventory_logs.upk_id IS NOT NULL) AND (inventory_logs.id IN ( SELECT max(inventory_logs_1.id) AS max
                  FROM inventory_logs inventory_logs_1
                 GROUP BY inventory_logs_1.inventory_id)) AND (inventory_logs.activity_date <= (((date_trunc('month'::text, now()) + '1 mon'::interval) - '1 day'::interval))::date))
         GROUP BY inventory_logs.medicine_id, inventory_logs.upk_id) table_column_g ON (((lbpha2_regular_usage.medicine_id = table_column_g.id) AND (table_column_g.upk_id = lbpha2_regular_usage.upk_id)))
    LEFT JOIN ( SELECT inventories.medicine_id AS id,
           sum(inventories.stock_qty) AS stock_qty_on_hand,
           sum(inventories.package_quantity) AS package_qty_on_hand,
           inventories.upk_id,
           string_agg(to_char(inventories.expired_date, 'DD/MM/YYYY'::text), ','::text) AS inv_string_ed_on_hand
          FROM inventories
         WHERE (inventories.upk_id IS NOT NULL)
         GROUP BY inventories.medicine_id, inventories.upk_id) table_inv_on_hand ON (((lbpha2_regular_usage.medicine_id = table_inv_on_hand.id) AND (table_inv_on_hand.upk_id = lbpha2_regular_usage.upk_id)))
 ORDER BY lbpha2_regular_usage.medicine_id, lbpha2_regular_usage.upk_id;
