CREATE OR REPLACE VIEW lbpha2_province AS

SELECT
	lbpha2_regular_usage.province_id AS province_id,
	lbpha2_regular_usage.medicine_id AS medicine_id,
	lbpha2_regular_usage.code_name,
	lbpha2_regular_usage.name,
	(COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer AS stok_awal,
	(COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer AS stok_diterima,
	(COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer AS stok_keluar,
	(COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer AS stok_kadaluarsa,
	((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * lbpha2_regular_usage.package_multiplier) AS stok_selisih_fisik,
	((((COALESCE(table_column_a.stock_qty_col_a, (0)::bigint))::integer + (COALESCE(table_column_b.stock_qty_col_b, (0)::bigint))::integer) - ((COALESCE(table_column_c.stock_qty_col_c, (0)::bigint))::integer + (COALESCE(table_column_d.stock_qty_col_d, (0)::bigint))::integer)) + ((COALESCE(table_column_e.package_qty_col_e, (0)::bigint))::integer * lbpha2_regular_usage.package_multiplier)) AS stok_akhir,
	(COALESCE(table_column_g.package_qty_col_g, (0)::bigint))::integer AS stok_akhir_botol,
	(COALESCE(table_inv_on_hand.stock_qty_on_hand, (0)::bigint))::integer AS stock_on_hand,
	(COALESCE(table_inv_on_hand.package_qty_on_hand, (0)::bigint))::integer AS package_on_hand,
	(COALESCE(lbpha2_regular_usage.stok_keluar_reguler, (0)::bigint))::integer AS stok_keluar_reguler,
	(COALESCE(lbpha2_regular_usage.paket_keluar_reguler, (0)::bigint))::integer AS paket_keluar_reguler,
	lbpha2_regular_usage.package_multiplier,
	lbpha2_regular_usage.package_unit_type,
	lbpha2_regular_usage.stock_unit_type,
	table_inv_on_hand.inv_string_ed_on_hand
FROM (
	SELECT
		medicine_id,
		province_id,
		code_name,
		lbpha2_regular_usage.name,
		lbpha2_regular_usage.package_multiplier,
		lbpha2_regular_usage.package_unit_type,
		lbpha2_regular_usage.stock_unit_type,
		sum(stok_keluar_reguler) stok_keluar_reguler,
		sum(paket_keluar_reguler) paket_keluar_reguler
	FROM
		lbpha2_regular_usage
	GROUP BY
		medicine_id,
		province_id,
		code_name,
		lbpha2_regular_usage.name,
		lbpha2_regular_usage.package_multiplier,
		lbpha2_regular_usage.package_unit_type,
		lbpha2_regular_usage.stock_unit_type) AS lbpha2_regular_usage
	JOIN provinces ON provinces.id = lbpha2_regular_usage.province_id
	LEFT JOIN (
		SELECT
			inventory_logs.medicine_id AS id,
			sum(inventory_logs.after_stock_qty) AS stock_qty_col_a,
			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_a,
			inventory_logs.province_id
		FROM
			inventory_logs
		WHERE ((inventory_logs.medicine_id IS NOT NULL)
			AND(inventory_logs.logistic_role = 'PROVINCE_ENTITY')
			AND(inventory_logs.province_id IS NOT NULL)
			AND(inventory_logs.id IN(
					SELECT
						max(inventory_logs_1.id) AS max FROM inventory_logs inventory_logs_1
					WHERE (inventory_logs_1.activity_date <= (((date_trunc('month'::text, now()) - '1 mon'::interval) + '1 mon'::interval))::date)
		GROUP BY
			inventory_logs_1.inventory_id)))
	GROUP BY
		inventory_logs.medicine_id,
		inventory_logs.province_id) table_column_a ON (((lbpha2_regular_usage.medicine_id = table_column_a.id)
			AND(table_column_a.province_id = lbpha2_regular_usage.province_id)))
	LEFT JOIN (
		SELECT
			inventory_logs.medicine_id AS id,
			sum(inventory_logs.after_stock_qty) AS stock_qty_col_b,
			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_b,
			inventory_logs.province_id
		FROM
			inventory_logs
		WHERE (((inventory_logs.activity)::text = 'INSERT'::text)
			AND(inventory_logs.logistic_role = 'PROVINCE_ENTITY')
			AND(inventory_logs.medicine_id IS NOT NULL)
			AND(inventory_logs.province_id IS NOT NULL)
			AND(inventory_logs.activity_date >= (((date_trunc('month'::text, now()) - '1 mon'::interval) + '1 mon'::interval))::date)
			AND(inventory_logs.activity_date <= (((date_trunc('month'::text, now()) + '1 mon'::interval) - '1 day'::interval))::date))
	GROUP BY
		inventory_logs.medicine_id,
		inventory_logs.province_id) table_column_b ON (((lbpha2_regular_usage.medicine_id = table_column_b.id)
			AND(table_column_b.province_id = lbpha2_regular_usage.province_id)))
	LEFT JOIN (
		SELECT
			distribution_plans_items.medicine_id AS id,
			sum(distribution_plans_items.package_quantity * brands.package_multiplier) AS stock_qty_col_c,
			sum(distribution_plans_items.package_quantity) AS package_qty_col_c,
			distribution_plans.province_id_sender AS province_id
		FROM
			distribution_plans
			JOIN distribution_plans_items ON distribution_plans.id = distribution_plans_items.distribution_plan_id
			JOIN brands ON brands.id = distribution_plans_items.brand_id
		WHERE (distribution_plans.logistic_role_sender = 'PROVINCE_ENTITY')
		AND(distribution_plans_items.brand_id IS NOT NULL)
		AND(distribution_plans.province_id_sender IS NOT NULL)
		AND(distribution_plans.created_at >= (((date_trunc('month'::text, now()) - '1 mon'::interval) + '1 mon'::interval))::date)
		AND(distribution_plans.created_at <= (((date_trunc('month'::text, now()) + '1 mon'::interval) - '1 day'::interval))::date)
	GROUP BY
		distribution_plans_items.medicine_id,
		distribution_plans.province_id_sender) table_column_c ON (((lbpha2_regular_usage.medicine_id = table_column_c.id)
			AND(table_column_c.province_id = lbpha2_regular_usage.province_id)))
	LEFT JOIN (
		SELECT
			inventories.medicine_id AS id,
			sum(inventories.stock_qty) AS stock_qty_col_d,
			sum(inventories.package_quantity) AS package_qty_col_d,
			inventories.province_id
		FROM
			inventories
		WHERE ((date_part('month'::text, inventories.expired_date) = date_part('month'::text, now()))
			AND(date_part('year'::text, inventories.expired_date) = date_part('year'::text, now()))
			AND(inventories.logistic_role = 'PROVINCE_ENTITY')
			AND(inventories.province_id IS NOT NULL))
	GROUP BY
		inventories.medicine_id,
		inventories.province_id) table_column_d ON (((lbpha2_regular_usage.medicine_id = table_column_d.id)
			AND(table_column_d.province_id = lbpha2_regular_usage.province_id)))
	LEFT JOIN (
		SELECT
			inventories.medicine_id AS id,
			sum(inventory_adjustment.package_quantity) AS package_qty_col_e,
			inventories.province_id
		FROM (inventories
			JOIN inventory_adjustment ON ((inventory_adjustment.inventory_id = inventories.id)))
	WHERE ((date_part('month'::text, inventory_adjustment.created_at) = date_part('month'::text, now()))
		AND(date_part('year'::text, inventory_adjustment.created_at) = date_part('year'::text, now()))
		AND(inventories.province_id IS NOT NULL)
		AND(inventories.logistic_role = 'PROVINCE_ENTITY'))
GROUP BY
	inventories.medicine_id,
	inventories.province_id) table_column_e ON (((lbpha2_regular_usage.medicine_id = table_column_e.id)
			AND(table_column_e.province_id = lbpha2_regular_usage.province_id)))
	LEFT JOIN (
		SELECT
			inventory_logs.medicine_id AS id,
			sum(inventory_logs.after_stock_qty) AS stock_qty_col_g,
			sum(inventory_logs.after_pkg_quantity) AS package_qty_col_g,
			inventory_logs.province_id
		FROM
			inventory_logs
		WHERE ((inventory_logs.medicine_id IS NOT NULL)
			AND(inventory_logs.logistic_role = 'PROVINCE_ENTITY')
			AND(inventory_logs.province_id IS NOT NULL)
			AND(inventory_logs.id IN(
					SELECT
						max(inventory_logs_1.id) AS max FROM inventory_logs inventory_logs_1
					GROUP BY
						inventory_logs_1.inventory_id))
			AND(inventory_logs.activity_date <= (((date_trunc('month'::text, now()) + '1 mon'::interval) - '1 day'::interval))::date))
	GROUP BY
		inventory_logs.medicine_id,
		inventory_logs.province_id) table_column_g ON (((lbpha2_regular_usage.medicine_id = table_column_g.id)
			AND(table_column_g.province_id = lbpha2_regular_usage.province_id)))
	LEFT JOIN (
		SELECT
			inventories.medicine_id AS id,
			sum(inventories.stock_qty) AS stock_qty_on_hand,
			sum(inventories.package_quantity) AS package_qty_on_hand,
			inventories.province_id,
			string_agg(to_char(inventories.expired_date, 'DD/MM/YYYY'::text), ','::text) AS inv_string_ed_on_hand
		FROM
			inventories
		WHERE (inventories.province_id IS NOT NULL)
		AND(inventories.logistic_role = 'PROVINCE_ENTITY')
	GROUP BY
		inventories.medicine_id,
		inventories.province_id) table_inv_on_hand ON (((lbpha2_regular_usage.medicine_id = table_inv_on_hand.id)
			AND(table_inv_on_hand.province_id = lbpha2_regular_usage.province_id)))
ORDER BY
	lbpha2_regular_usage.medicine_id,
	lbpha2_regular_usage.province_id
