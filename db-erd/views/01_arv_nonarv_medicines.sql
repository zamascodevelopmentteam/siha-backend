
-- VIEW: medicine with arv & non arv properties
select medicines.id medicine_id, medicines.name medicine_name, code_name, medicine_type,
		medicines.stock_unit_type, medicines.package_unit_type, medicines.package_multiplier,
		ingredients, sediaan, image_url,
		is_lini_1, is_lini_2,
		is_r1, is_r2, is_r3, is_sifilis, is_alkes, is_io_ims, is_cd_vl,is_preventif
	from medicines
	left join (select * from brands where is_generic is true) as brands
		on medicines.id=brands.medicine_id
	left join arv_medicines
		on medicines.id = arv_medicines.medicine_id
	left join non_arv_medicines
		on medicines.id = non_arv_medicines.medicine_id
	where medicines.deleted_at is null
