CREATE TABLE "central_ministry" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "provinces" (
  "id" SERIAL PRIMARY KEY,
  "central_ministry_id" integer,
  "order_multiplier" SMALLINT DEFAULT 9,
  "name" VARCHAR,
  "code_id" VARCHAR,
  "is_active" boolean DEFAULT true,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "sudin_kab_kota" (
  "id" SERIAL PRIMARY KEY,
  "province_id" integer,
  "order_multiplier" SMALLINT DEFAULT 6,
  "name" VARCHAR,
  "code_id" VARCHAR,
  "is_active" boolean DEFAULT true,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "lsm_penjangkau" (
  "id" SERIAL PRIMARY KEY,
  "sudin_kab_kota_id" integer,
  "name" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "upk" (
  "id" SERIAL PRIMARY KEY,
  "sudin_kab_kota_id" integer,
  "province_id" integer,
  "code_id" VARCHAR,
  "order_multiplier" SMALLINT DEFAULT 3,
  "name" VARCHAR,
  "is_active" boolean DEFAULT true,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "is_active" boolean DEFAULT true,
  "nik" VARCHAR UNIQUE NOT NULL,
  "password" VARCHAR,
  "name" VARCHAR,
  "role" role NOT NULL,
  "logistic_role" VARCHAR,
  "avatar" VARCHAR,
  "upk_id" integer,
  "sudin_kab_kota_id" integer,
  "province_id" integer,
  "email" VARCHAR,
  "phone" VARCHAR,
  "refresh_token" VARCHAR,
  "fcm_token" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "patients" (
  "id" SERIAL PRIMARY KEY,
  "user_id" integer,
  "upk_id" integer,
  "nik" VARCHAR,
  "fullname" VARCHAR,
  "address_KTP" VARCHAR,
  "address_domicile" VARCHAR,
  "date_birth" date,
  "tgl_meninggal" date,
  "gender" VARCHAR,
  "phone" VARCHAR,
  "status_patient" VARCHAR,
  "email" VARCHAR,
  "weight" integer,
  "height" integer,
  "nama_pmo" VARCHAR,
  "hubungan_pmo" VARCHAR,
  "no_hp_pmo" VARCHAR,
  "meta" jsonb default '{}'::jsonb,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "lsm_penjangkau" lsm,
  "kelompok_populasi" kelompok_populasi ARRAY,
);

CREATE TABLE "visits" (
  "id" BIGSERIAL PRIMARY KEY,
  "user_rr_id" integer,
  "upk_id" integer,
  "patient_id" integer,
  "is_generated" boolean DEFAULT false,
  "ordinal" integer,
  "visit_type" VARCHAR,
  "visit_date" timestamp,
  "check_out_date" timestamp,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "test_hiv" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "ordinal" integer,
  "kelompok_populasi" VARCHAR,
  "lsm_penjangkau" VARCHAR,
  "alasan_test" VARCHAR,
  "tanggal_test" timestamp,
  "jenis_test" VARCHAR,
  "nama_reagen_r1" integer,
  "qty_reagen_r1" SMALLINT DEFAULT 1,
  "hasil_test_r1" VARCHAR,
  "nama_reagen_r2" integer,
  "qty_reagen_r2" SMALLINT DEFAULT 1,
  "hasil_test_r2" VARCHAR,
  "nama_reagen_r3" integer,
  "qty_reagen_r3" SMALLINT DEFAULT 1,
  "hasil_test_r3" VARCHAR,
  "nama_reagen_dna" integer,
  "qty_reagen_dna" SMALLINT DEFAULT 0,
  "hasil_test_dna" VARCHAR,
  "nama_reagen_nat" integer,
  "qty_reagen_nat" SMALLINT DEFAULT 0,
  "hasil_test_nat" VARCHAR,
  "nama_reagen_rna" integer,
  "qty_reagen_rna" SMALLINT DEFAULT 0,
  "hasil_test_rna" VARCHAR,
  "rujukan_lab_lanjut" VARCHAR,
  "nama_reagen_elisa" integer,
  "qty_reagen_elisa" SMALLINT DEFAULT 0,
  "hasil_test_elisa" VARCHAR,
  "nama_reagen_wb" integer,
  "qty_reagen_wb" SMALLINT DEFAULT 0,
  "hasil_test_wb" VARCHAR,
  "kesimpulan_hiv" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "test_ims" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "ordinal" integer,
  "ditest_ims" boolean DEFAULT false,
  "nama_reagen" integer,
  "qty_reagen" SMALLINT NOT NULL DEFAULT 1,
  "hasil_test_ims" VARCHAR(32) DEFAULT null,
  "ditest_sifilis" boolean DEFAULT null,
  "hasil_test_sifilis" VARCHAR(32) DEFAULT null,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "test_vl_cd4" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "ordinal" integer,
  "test_vl_cd4_type" VARCHAR,
  "nama_reagen" integer,
  "qty_reagen" SMALLINT NOT NULL DEFAULT 1,
  "hasil_test_vl_cd4" VARCHAR(32),
  "hasil_test_in_number" NUMERIC(8,2),
  "hasil_lab_satuan" VARCHAR DEFAULT null,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "profilaksis" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "ordinal" integer,
  "tgl_profilaksis" timestamp,
  "status_profilaksis" VARCHAR,
  "obat_diberikan" integer,
  "qty_obat" SMALLINT NOT NULL DEFAULT 1,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "treatments" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer NOT NULL,
  "prescription_id" integer,
  "ordinal" integer,
  "is_generated" boolean DEFAULT false,
  "treatment_start_date" timestamp,
  "no_reg_nas" VARCHAR,
  "lsm_penjangkau" VARCHAR,
  "tgl_konfirmasi_hiv_pos" timestamp,
  "tgl_kunjungan" timestamp,
  "tgl_rujuk_masuk" timestamp,
  "upk_sebelumnya" integer,
  "kelompok_populasi" VARCHAR,
  "status_tb" VARCHAR,
  "tgl_pengobatan_tb" timestamp,
  "status_fungsional" VARCHAR,
  "stadium_klinis" VARCHAR,
  "ppk" boolean DEFAULT null,
  "tgl_pemberian_ppk" timestamp,
  "status_tb_tpt" VARCHAR,
  "tgl_pemberian_tb_tpt" timestamp,
  "paduan_obat_arv" VARCHAR,
  "status_paduan" VARCHAR,
  "tgl_pemberian_obat" timestamp,
  "obat_arv_1" integer,
  "jml_hr_obat_arv_1" SMALLINT,
  "sisa_obat_arv_1" SMALLINT,
  "obat_arv_2" integer,
  "jml_hr_obat_arv_2" SMALLINT,
  "sisa_obat_arv_2" SMALLINT,
  "obat_arv_3" integer,
  "jml_hr_obat_arv_3" SMALLINT,
  "sisa_obat_arv_3" SMALLINT,
  "tes_lab" VARCHAR,
  "nama_reagen" integer,
  "jml_reagen" SMALLINT,
  "hasil_lab" integer,
  "hasil_lab_satuan" VARCHAR,
  "notifikasi_pasangan" VARCHAR,
  "akhir_follow_up" VARCHAR,
  "tgl_rujuk_keluar" timestamp,
  "tgl_meninggal" timestamp,
  "tgl_berhenti_arv" timestamp,
  "tgl_rencana_kunjungan" timestamp,
  "tgl_permintaan_kunjungan" timestamp,
  "bulan_periode_laporan" SMALLINT,
  "is_on_transit" boolean,
  "weight" integer,
  "height" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "couples" (
  "id" BIGSERIAL PRIMARY KEY,
  "name" VARCHAR,
  "age" integer,
  "gender" VARCHAR,
  "relationship" VARCHAR,
  "treatment_id" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "upk_treatment_logs" (
  "id" BIGSERIAL PRIMARY KEY,
  "patient_id" integer,
  "upk_id" integer,
  "treatment_start_date" timestamp,
  "treatment_end_date" timestamp,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "medicines" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR,
  "code_name" VARCHAR,
  "medicine_type" VARCHAR,
  "stock_unit_type" VARCHAR,
  "package_unit_type" VARCHAR,
  "package_multiplier" SMALLINT DEFAULT 1,
  "ingredients" VARCHAR,
  "unit_price" float4 DEFAULT 0,
  "fund_source" VARCHAR,
  "sediaan" VARCHAR,
  "image_url" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "brands" (
  "id" SERIAL PRIMARY KEY,
  "medicine_id" integer,
  "name" VARCHAR,
  "is_generic" boolean DEFAULT false,
  "unit_price" float4 DEFAULT 0,
  "fund_source" real,
  "notes" VARCHAR,
  "package_unit_type" VARCHAR,
  "package_multiplier" SMALLINT DEFAULT 1,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "arv_medicines" (
  "id" SERIAL PRIMARY KEY,
  "medicine_id" integer,
  "is_lini_1" boolean DEFAULT null,
  "is_lini_2" boolean DEFAULT null,
  "is_ims" boolean DEFAULT null,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "non_arv_medicines" (
  "id" SERIAL PRIMARY KEY,
  "medicine_id" integer,
  "is_r1" boolean DEFAULT null,
  "is_r2" boolean DEFAULT null,
  "is_r3" boolean DEFAULT null,
  "is_sifilis" boolean DEFAULT null,
  "is_anak" boolean DEFAULT null,
  "is_alkes" boolean DEFAULT null,
  "is_io_ims" boolean DEFAULT null,
  "is_vl" boolean DEFAULT null,
  "is_cd4" boolean DEFAULT null,
  "is_preventif" boolean DEFAULT null,
  "is_eid" boolean DEFAULT null,
  "test_type" varchar(32) DEFAULT null,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "regiments" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR,
  "combination_ids" VARCHAR NOT NULL,
  "combination_str_info" VARCHAR NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "regiment_medicines" (
  "id" SERIAL PRIMARY KEY,
  "regiment_id" integer,
  "medicine_id" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "prescriptions" (
  "id" BIGSERIAL PRIMARY KEY,
  "patient_id" integer,
  -- "treatment_id" integer,
  "visit_id" integer,
  "regiment_id" integer,
  "prescription_number" VARCHAR,
  "paduan_obat_ids" VARCHAR,
  "paduan_obat_str" VARCHAR,
  "notes" VARCHAR,
  "is_draft" boolean DEFAULT false,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "prescription_medicines" (
  "id" BIGSERIAL PRIMARY KEY,
  "prescription_id" integer,
  "medicine_id" integer,
  "amount" integer,
  "jumlah_hari" integer,
  "dosis_per_hari" integer,
  "notes" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "orders" (
  "id" BIGSERIAL PRIMARY KEY,
  "order_code" VARCHAR UNIQUE NOT NULL,
  "order_number" integer NOT NULL,
  "last_order_status" VARCHAR,
  "last_approval_status" VARCHAR,
  "order_notes" VARCHAR,
  "approval_notes" VARCHAR,
  "is_regular" boolean,
  "pic_id" integer,
  "logistic_role" VARCHAR NOT NULL,
  "province_id_owner" integer,
  "sudin_kab_kota_id_owner" integer,
  "upk_id_owner" integer,
  "province_id_for" integer,
  "sudin_kab_kota_id_for" integer,
  "upk_id_for" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "order_items" (
  "id" BIGSERIAL PRIMARY KEY,
  "order_id" integer NOT NULL,
  "medicine_id" integer,
  "package_unit_type" VARCHAR,
  "package_quantity" integer NOT NULL,
  "expired_date" timestamp,
  "notes" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "order_logs" (
  "id" BIGSERIAL PRIMARY KEY,
  "order_id" integer NOT NULL,
  "actor_id" integer,
  "order_status" VARCHAR,
  "approval_status" VARCHAR,
  "prev_order_status" VARCHAR,
  "prev_approval_status" VARCHAR,
  "activity_type" VARCHAR,
  "notes" VARCHAR,
  "prev_notes" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "distribution_plans" (
  "id" BIGSERIAL PRIMARY KEY,
  "dropping_number" VARCHAR UNIQUE NOT NULL,
  "order_id" integer,
  "pic_id" integer,
  "fund_source" VARCHAR,
  "plan_status" VARCHAR,
  "logistic_role" VARCHAR NOT NULL,
  "approval_status" VARCHAR,
  "notes" VARCHAR,
  "approval_notes" VARCHAR,
  "entity_name" VARCHAR,
  "province_id" integer,
  "sudin_kab_kota_id" integer,
  "upk_id" integer,
  "total_price" BIGINT DEFAULT 0,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "distribution_plans_items" (
  "id" BIGSERIAL PRIMARY KEY,
  "distribution_plan_id" integer NOT NULL,
  "order_item_id" integer,
  "medicine_id" integer,
  "package_unit_type" VARCHAR,
  "expired_date" timestamp,
  "notes" VARCHAR,
  "package_quantity" integer NOT NULL,
  "actual_recieved_package_quantity" integer,
  "unit_price" float4 DEFAULT 0,
  "total_price" float8 DEFAULT 0,
  "fund_source" VARCHAR,
  "inventory_id" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "distribution_logs" (
  "id" BIGSERIAL PRIMARY KEY,
  "distribution_plan_id" integer NOT NULL,
  "activity" VARCHAR,
  "activity_date" timestamp,
  "actor_id" integer,
  "prev_order_id" integer,
  "prev_pic_id" integer,
  "prev_plan_status" VARCHAR,
  "prev_approval_status" VARCHAR,
  "order_id" integer,
  "plan_status" VARCHAR,
  "approval_status" VARCHAR,
  "notes" VARCHAR,
  "prev_notes" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "receipts" (
  "id" BIGSERIAL PRIMARY KEY,
  "distribution_plan_id" integer,
  "receipt_number" VARCHAR UNIQUE,
  "receipt_status" VARCHAR,
  "notes" VARCHAR,
  "logistic_role" VARCHAR NOT NULL,
  "province_id" integer,
  "sudin_kab_kota_id" integer,
  "upk_id" integer,
  "received_at" timestamp,
  "received_by" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "receipt_items" (
  "id" BIGSERIAL PRIMARY KEY,
  "receipt_id" integer NOT NULL,
  "receipt_item_status" VARCHAR,
  "inventory_id" integer,
  "batch_code" VARCHAR,
  "brand_id" integer,
  "received_expired_date" timestamp NOT NULL,
  "received_pkg_unit_type" VARCHAR,
  "received_pkg_quantity" integer NOT NULL DEFAULT 0,
  "notes" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "inventories" (
  "id" BIGSERIAL PRIMARY KEY,
  "receipt_item_id" integer,
  "batch_code" VARCHAR,
  "medicine_id" integer,
  "brand_id" integer,
  "expired_date" timestamp NOT NULL,
  "package_unit_type" VARCHAR,
  "package_quantity" integer NOT NULL DEFAULT 0,
  "stock_unit" VARCHAR,
  "stock_qty" integer NOT NULL DEFAULT 0,
  "is_adjustment" boolean DEFAULT false,
  "logistic_role" VARCHAR NOT NULL,
  "province_id" integer,
  "sudin_kab_kota_id" integer,
  "upk_id" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "inventory_adjustment" (
  "id" BIGSERIAL PRIMARY KEY,
  "inventory_id" integer NOT NULL,
  "adjustment_type" VARCHAR,
  "package_unit_type" VARCHAR,
  "package_quantity" integer NOT NULL DEFAULT 0,
  "stock_unit" VARCHAR,
  "stock_qty" integer NOT NULL DEFAULT 0,
  "reportCode" VARCHAR,
  "notes" VARCHAR,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "inventory_logs" (
  "id" BIGSERIAL PRIMARY KEY,
  "inventory_id" integer NOT NULL,
  "activity" VARCHAR,
  "activity_date" timestamp,
  "actor_id" integer,
  "is_adjustment" boolean DEFAULT false,
  "medicine_id" integer,
  "brand_id" integer,
  "logistic_role" VARCHAR NOT NULL,
  "province_id" integer,
  "sudin_kab_kota_id" integer,
  "upk_id" integer,
  "prev_expired_date" timestamp NULL,
  "prev_pkg_unit_type" VARCHAR,
  "prev_pkg_quantity" integer NULL,
  "pkg_qty_changes" integer NULL,
  "after_pkg_quantity" integer NULL,
  "prev_stock_unit" VARCHAR,
  "prev_stock_qty" integer NULL,
  "stock_qty_changes" integer NULL,
  "after_stock_qty" integer NULL,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "inventory_usage" (
  "id" BIGSERIAL PRIMARY KEY,
  "patient_id" integer,
  "upk_id" integer,
  "visit_id" integer,
  "inventory_id" integer,
  "treatment_id" integer,
  "test_hiv_id" integer,
  "test_ims_id" integer,
  "test_vl_cd4_id" integer,
  "profilaksis_id" integer,
  "brand_id" integer,
  "used_stock_unit" integer NOT NULL,
  -- "used_pkg_unit" integer NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "patient_logs" (
  "id" SERIAL PRIMARY KEY,

  "patient_id" integer,
  "upk_id" integer,
  "visit_id" integer,
  "start_date" date,
  "end_date" date,
  "tgl_rujuk_masuk" date,
  "tgl_rujuk_keluar" date,

  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);


CREATE TABLE "settings" (
  "name" VARCHAR UNIQUE NOT NULL,
  "meta" jsonb default '{}'::jsonb,
  "is_active" boolean DEFAULT true,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "lbpha2" (
  "id" SERIAL PRIMARY KEY,

  "tanggal_laporan" date,

  "upk_id" integer,
  "sudin_kab_kota_id" integer,
  "province_id" integer,

  "tabel_stok" jsonb default '{}'::jsonb,
  "tabel_rejimen_standar" jsonb default '{}'::jsonb,
  "tabel_rejimen_lain" jsonb default '{}'::jsonb,
  "tabel_fdc_junior" jsonb default '{}'::jsonb,

  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer
);

CREATE TABLE "notifications" (
    "id" BIGSERIAL NOT NULL,
    "title" varchar(128),
    "message" varchar,
    "user_id" int4,
    "sms" varchar(24),
    "fcm" varchar,
    "email" varchar(128),
    "meta" varchar,
    "sent_at" timestamp,
    "received_at" timestamp,
    "read_at" timestamp,
    "notification_type" varchar(24),

    "created_at" timestamp,
    "updated_at" timestamp,
    "deleted_at" timestamp,
    "created_by" int4,
    "updated_by" int4,
    CONSTRAINT "notifications_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users"("id"),
    PRIMARY KEY ("id")
);

CREATE TABLE "ims_visits" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "reason_visit" reason_visit_ims,
  "complaint" complaint_ims,
  "is_pregnant" boolean DEFAULT FALSE,
  "is_iva_tested" boolean DEFAULT FALSE,
  "iva_test" boolean,
  "pregnancy_stage" pregnancy_stage,
  "upk_id" integer,
  "physical_examination" jsonb default '{}'::jsonb,
  "reason_referral" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

CREATE TABLE "ims_visit_labs" (
  "id" BIGSERIAL PRIMARY KEY,
  "visit_id" integer,
  "pmn_uretra_serviks" boolean NULL,
  "diplokokus_intrasel_uretra_serviks" boolean NULL,
  "pmn_anus" boolean NULL,
  "diplokokus_intrasel_anus" boolean NULL,
  "t_vaginalis" boolean NULL,
  "kandida" boolean NULL,
  "ph" boolean NULL,
  "sniff_test" boolean NULL,
  "clue_cells" boolean NULL,
  "rpr_vdrl" jsonb default '{}'::jsonb,
  "tpha_tppa" jsonb default '{}'::jsonb,
  "rpr_vdrl_titer" ims_lab_rpr_vdrl_titer,
  "etc" varchar NULL,
  "diagnosis_syndrome" jsonb default '{}'::jsonb,
  "diagnosis_klinis" jsonb default '{}'::jsonb,
  "diagnosis_lab" jsonb default '{}'::jsonb,
  "is_tested" boolean,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

CREATE TABLE "ims_prescriptions" (
  "id" BIGSERIAL PRIMARY KEY,
  "patient_id" integer,
  "visit_id" integer,
  "note" varchar,
  "date_given_medicine" date,
  "condom" integer,
  "lubricant" integer,
  "counseling" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_at" timestamp
);

CREATE TABLE "ims_prescription_medicines" (
  "id" BIGSERIAL PRIMARY KEY,
  "prescription_id" integer,
  "medicine_id" integer,
  "medicine_name" varchar,
  "total_qty" integer,
  "total_days" integer,
  "note" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);

CREATE TABLE "alkes_medicines" (
  "id" BIGSERIAL PRIMARY KEY,
  "medicine_id" integer,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "created_by" integer,
  "updated_by" integer,
  "deleted_by" integer
);
