CREATE TYPE "gender" AS ENUM (
  'LAKI-LAKI',
  'PEREMPUAN'
);

CREATE TYPE "rujukan_lab_lanjut" AS ENUM (
  'WESTERN BLOT',
  'ELISA'
);

CREATE TYPE "status_fungsional" AS ENUM (
  'KERJA',
  'AMBULATORI',
  'BARING'
);

CREATE TYPE "stadium_klinis" AS ENUM (
  'STADIUM 1',
  'STADIUM 2',
  'STADIUM 3',
  'STADIUM 4'
);

CREATE TYPE "status_paduan" AS ENUM (
  'ORISINAL',
  'SUBSTITUSI',
  'SWITCH'
);

CREATE TYPE "tes_lab" AS ENUM (
  'VL (KOPI/ml)',
  'CD4 (SEL/m3)'
);

CREATE TYPE "hasil_lab_satuan" AS ENUM (
  'KOPI/ml',
  'SEL/m3'
);

CREATE TYPE "alasan_test_hiv" AS ENUM (
  'RECOMMENDED_BY_DOCTOR',
  'SECOND_OPINION'
);

CREATE TYPE "akhir_follow_up" AS ENUM (
  'RUJUK KELUAR',
  'MENINGGAL',
  'BERHENTI ARV'
);

CREATE TYPE "role" AS ENUM (
  'PATIENT',
  'LAB_STAFF',
  'RR_STAFF',
  'PHARMA_STAFF',
  'SUDIN_STAFF',
  'PROVINCE_STAFF',
  'MINISTRY_STAFF',
  'DOCTOR'
);

CREATE TYPE "patient_type" AS ENUM (
  'HIV NEGATIV',
  'HIV POSITIF',
  'ODHA'
);

CREATE TYPE "visit_type" AS ENUM (
  'TEST',
  'TREATMENT',
  'CONSULTATION',
  'PROFILAKSIS'
);

CREATE TYPE "test_result_type" AS ENUM (
  'REAKTIF',
  'INKONKLUSIF',
  'NON_REAKTIF'
);

CREATE TYPE "kelompok_populasi" AS ENUM (
  'WPS',
  'Waria',
  'Penasun',
  'LSL',
  'Bumil',
  'Pasien TB',
  'Pasien IMS',
  'Pasien Hepatitis',
  'Pasangan ODHA',
  'Anak ODHA',
  'WBP',
  'Lainnya'
);

CREATE TYPE "notifikasi_pasangan" AS ENUM (
  'menerima',
  'tidak menerima',
  'klien tidak memiliki pasangan',
  'tidak ditawarkan'
);

CREATE TYPE "status_tb" AS ENUM (
  'tidak ada gejala tb',
  'suspek tb',
  'dalam pengobatan tb',
  'tidak dilkakukan skrining tb',
  'tidak dilakukan tb'
);

CREATE TYPE "status_tb_tpt" AS ENUM (
  'INH',
  'RIFAMPETINE',
  'TIDAK DIBERIKAN'
);

CREATE TYPE "test_vl_cd4_type" AS ENUM (
  'VL',
  'CD4'
);

CREATE TYPE "test_type" AS ENUM (
  'HIV',
  'IMS',
  'VL'
);

CREATE TYPE "regiment_type" AS ENUM (
  'LINI I',
  'LINI I TAMBAHAN',
  'LINI II',
  'PEDIATRIK'
);

CREATE TYPE "medicine_type" AS ENUM (
  'ARV',
  'NON_ARV'
);

CREATE TYPE "stock_unit_type" AS ENUM (
  'TABLET',
  'TEST',
  'KAPSUL',
  'PAKET',
  'VIAL',
  'PCS',
  'BUAH',
  'KIT',
  'BOTOL',
  'BOX'
);

CREATE TYPE "sediaan_type" AS ENUM (
  'TUNGGAL',
  'BOOSTED',
  'KDT_FDC',
  'FDC_PEDIATRIK'
);

CREATE TYPE "logistic_role" AS ENUM (
  'LAB_ENTITY',
  'PHARMA_ENTITY',
  'UPK_ENTITY',
  'SUDIN_ENTITY',
  'PROVINCE_ENTITY',
  'MINISTRY_ENTITY'
);

CREATE TYPE "fund_source" AS ENUM (
  'APBN',
  'NON_APBN'
);

CREATE TYPE "activity_type" AS ENUM (
  'CREATE',
  'UPDATE',
  'UPDATE_INFO',
  'UPDATE_ITEM',
  'DELETE'
);

CREATE TYPE "order_status" AS ENUM (
  'DRAFT',
  'EVALUATION',
  'PROCESSED',
  'EXECUTED',
  'REJECTED'
);

CREATE TYPE "distribution_plans_status" AS ENUM (
  'DRAFT',
  'EVALUATION',
  'PROCESSED',
  'ACCEPTED_FULLY',
  'ACCEPTED_DIFFERENT',
  'LOST',
  'REJECTED'
);

CREATE TYPE "receipt_status" AS ENUM (
  'ACCEPTED_FULLY',
  'ACCEPTED_DIFFERENT',
  'LOST'
);

CREATE TYPE "receipt_item_status" AS ENUM (
  'NOT_CHANGED',
  'MORE_THAN_DEMAND',
  'LESS_THAN_DEMAND'
);

CREATE TYPE "approval_status" AS ENUM (
  'DRAFT',
  'IN_EVALUATION',
  'APPROVED_INTERNAL',
  'APPROVED_FULL',
  'REJECTED_INTERNAL',
  'REJECTED_UPPER',
  'REJECTED'
);

CREATE TYPE "adjustment_type" AS ENUM (
  'ADDITION',
  'DEDUCTION'
);

CREATE TYPE "reason_visit_ims" AS ENUM (
  'SKRINING',
  'SAKIT',
  'FOLLOWUP'
);

CREATE TYPE "complaint_ims" AS ENUM (
  'DUH_TUBUH',
  'GATAL',
  'KENCING_SAKIT',
  'NYERI_PERUT',
  'LECET',
  'BINTIL_SAKIT',
  'LUKA_ULKUS',
  'JENGGER',
  'BENJOLAN',
  'TIDAK_ADA'
);

CREATE TYPE "pregnancy_stage" AS ENUM (
  'TRIMESTER_1',
  'TRIMESTER_2',
  'TRIMESTER_3'
);

CREATE TYPE "ims_lab_rn" AS ENUM (
  'REAKTIF',
  'NON_REAKTIF',
  'INVALID'
);

CREATE TYPE "ims_lab_rpr_vdrl_titer" AS ENUM (
  '0',
  '1/2',
  '1/4',
  '1/8',
  '1/16',
  '1/32',
  '1/64',
  'EMPTY'
);

CREATE TYPE "lsm" AS ENUM (
  'SPIRITIA',
  'IAC',
  'UNFPA',
  'IPPI',
  'YPI',
  'PKVHI',
  'PKBI',
  'KADER KESEHATAN',
  'DOTS/TB',
  'LAPAS RUTAN',
  'HEPATITIS',
  'TB',
  'KIA',
  'POLI_LAINNYA'
);
