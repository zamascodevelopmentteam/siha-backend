CREATE OR REPLACE FUNCTION patient_new_record()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO patient_logs (patient_id, start_date, end_date, upk_id, created_at)
      VALUES (NEW.id, now(), now() + interval '50 year',NEW.upk_id, now());

  RETURN NEW;
  END;
  $$;

CREATE OR REPLACE FUNCTION inventory_new_item()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO inventory_logs (inventory_id, activity, activity_date, actor_id, is_adjustment, prev_expired_date, prev_pkg_unit_type, pkg_qty_changes, prev_stock_unit, stock_qty_changes, prev_pkg_quantity, prev_stock_qty, after_pkg_quantity, after_stock_qty, medicine_id, brand_id, logistic_role, province_id, sudin_kab_kota_id, upk_id)
      VALUES (NEW.id, TG_OP, now(), NEW.created_by, NEW.is_adjustment, NEW.expired_date, NEW.package_unit_type, NEW.package_quantity, NEW.stock_unit, NEW.stock_qty, 0, 0, NEW.package_quantity, NEW.stock_qty, NEW.medicine_id, NEW.brand_id, NEW.logistic_role, NEW.province_id, NEW.sudin_kab_kota_id, NEW.upk_id);

  RETURN NEW;
  END;
  $$;


CREATE OR REPLACE FUNCTION inventory_update_item()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO inventory_logs (inventory_id, activity, activity_date, actor_id, is_adjustment, prev_expired_date, prev_pkg_unit_type, pkg_qty_changes, prev_stock_unit, stock_qty_changes, prev_pkg_quantity, prev_stock_qty, after_pkg_quantity, after_stock_qty, medicine_id, brand_id, logistic_role, province_id, sudin_kab_kota_id, upk_id)
      VALUES (OLD.id, TG_OP, now(), NEW.updated_by, NEW.is_adjustment, OLD.expired_date, OLD.package_unit_type, NEW.package_quantity - OLD.package_quantity, OLD.stock_unit, NEW.stock_qty - OLD.stock_qty, OLD.package_quantity, OLD.stock_qty, NEW.package_quantity, NEW.stock_qty, NEW.medicine_id, NEW.brand_id, NEW.logistic_role, NEW.province_id, NEW.sudin_kab_kota_id, NEW.upk_id);

  RETURN NEW;
  END;
  $$;

CREATE OR REPLACE FUNCTION order_new_item()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO order_logs (order_id, activity_type, created_at, order_status, approval_status, notes, actor_id)
      VALUES (NEW.id, TG_OP, now(), NEW.created_at, NEW.last_order_status, NEW.last_approval_status, NEW.notes, NEW.created_by);

  RETURN NEW;
  END;
  $$;


CREATE OR REPLACE FUNCTION order_update_item()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO order_logs (order_id, activity_type, created_at, updated_at, order_status, approval_status, notes, prev_order_status, prev_approval_status, prev_notes, actor_id)
      VALUES (OLD.id, TG_OP, now(), OLD.created_at, NEW.updated_at, NEW.last_order_status, NEW.last_approval_status, NEW.notes, OLD.last_order_status, OLD.last_approval_status, OLD.notes, NEW.updated_by);

  RETURN NEW;
  END;
  $$;

CREATE OR REPLACE FUNCTION distribution_plan_new_item()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO distribution_logs (distribution_plan_id, order_id, activity, activity_date, created_at, plan_status, approval_status, notes, actor_id)
      VALUES (NEW.id, NEW.order_id, TG_OP, now(), NEW.created_at, NEW.plan_status, NEW.approval_status, NEW.notes, NEW.created_by);

  RETURN NEW;
  END;
  $$;


CREATE OR REPLACE FUNCTION distribution_plan_update_item()
  RETURNS TRIGGER
  SET SCHEMA 'public'
  LANGUAGE plpgsql
  AS $$
  BEGIN

  INSERT INTO distribution_logs (distribution_plan_id, order_id, activity, activity_date, created_at, updated_at, plan_status, approval_status, notes, prev_plan_status, prev_approval_status, prev_notes, actor_id)
      VALUES (OLD.id, NEW.order_id, TG_OP, now(), OLD.created_at, NEW.updated_at, NEW.plan_status, NEW.approval_status, NEW.notes, OLD.plan_status, OLD.approval_status, OLD.notes, NEW.updated_by);

  RETURN NEW;
  END;
  $$;
  
