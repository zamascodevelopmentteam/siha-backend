start from brand new DB, NEVER USE SEQUELIZE MIGRATION:

- stop your development server
- Install npm dotenv-cli
- create db "new_db_name"
- execute SQL DDL: m-siha-backend/db-erd/01_schema_enum.sql
- execute SQL DDL: m-siha-backend/db-erd/02_schema_table.sql
- execute SQL DDL: m-siha-backend/db-erd/03_schema_keys_fk.sql
- execute SQL DDL: m-siha-backend/db-erd/04_functions.sql
- execute SQL DDL: m-siha-backend/db-erd/05_triggers.sql
- execute: npm run runseeder (to remove all seeded data: npm run undoseeder)
- change your ENV's database config to "new_db_name"
- now you can start your development server with "npm start" with new db data
