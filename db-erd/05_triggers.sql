CREATE TRIGGER logging_patient_new
      AFTER INSERT ON patients
      FOR EACH ROW EXECUTE PROCEDURE patient_new_record();

CREATE TRIGGER logging_inventory_new
      AFTER INSERT ON inventories
      FOR EACH ROW EXECUTE PROCEDURE inventory_new_item();

CREATE TRIGGER logging_inventory_update
      BEFORE UPDATE ON inventories
      FOR EACH ROW EXECUTE PROCEDURE inventory_update_item();

CREATE TRIGGER logging_order_new
      AFTER INSERT ON orders
      FOR EACH ROW EXECUTE PROCEDURE order_new_item();

CREATE TRIGGER logging_ordery_update
      BEFORE UPDATE ON orders
      FOR EACH ROW EXECUTE PROCEDURE order_update_item();

CREATE TRIGGER logging_distribution_plan_new
      AFTER INSERT ON distribution_plans
      FOR EACH ROW EXECUTE PROCEDURE distribution_plan_new_item();

CREATE TRIGGER logging_distribution_plan_update
      BEFORE UPDATE ON distribution_plans
      FOR EACH ROW EXECUTE PROCEDURE distribution_plan_update_item();
