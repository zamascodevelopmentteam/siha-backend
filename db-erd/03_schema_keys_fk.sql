
ALTER TABLE "provinces" ADD FOREIGN KEY ("central_ministry_id") REFERENCES "central_ministry" ("id");

ALTER TABLE "provinces" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "provinces" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "sudin_kab_kota" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "sudin_kab_kota" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "sudin_kab_kota" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "lsm_penjangkau" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "lsm_penjangkau" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "lsm_penjangkau" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "upk" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "upk" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "upk" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "upk" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "users" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "users" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "users" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "users" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "users" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "patients" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "patients" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "patients" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "patients" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "visits" ADD FOREIGN KEY ("user_rr_id") REFERENCES "users" ("id");

ALTER TABLE "visits" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "visits" ADD FOREIGN KEY ("patient_id") REFERENCES "patients" ("id");

ALTER TABLE "visits" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "visits" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

-- ALTER TABLE "test_hiv" ADD FOREIGN KEY ("lsm_penjangkau_id") REFERENCES "lsm_penjangkau" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_r1") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_r2") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_r3") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_nat") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_dna") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_rna") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_elisa") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("nama_reagen_wb") REFERENCES "medicines" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "test_hiv" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "test_ims" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "test_ims" ADD FOREIGN KEY ("nama_reagen") REFERENCES "medicines" ("id");

ALTER TABLE "test_ims" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "test_ims" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "test_vl_cd4" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "test_vl_cd4" ADD FOREIGN KEY ("nama_reagen") REFERENCES "medicines" ("id");

ALTER TABLE "test_vl_cd4" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "test_vl_cd4" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "profilaksis" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "profilaksis" ADD FOREIGN KEY ("obat_diberikan") REFERENCES "medicines" ("id");

ALTER TABLE "profilaksis" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "profilaksis" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("prescription_id") REFERENCES "prescriptions" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("upk_sebelumnya") REFERENCES "upk" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("obat_arv_1") REFERENCES "medicines" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("obat_arv_2") REFERENCES "medicines" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("obat_arv_3") REFERENCES "medicines" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("nama_reagen") REFERENCES "medicines" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "treatments" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "couples" ADD FOREIGN KEY ("treatment_id") REFERENCES "treatments" ("id");

ALTER TABLE "upk_treatment_logs" ADD FOREIGN KEY ("patient_id") REFERENCES "patients" ("id");

ALTER TABLE "upk_treatment_logs" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "upk_treatment_logs" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "upk_treatment_logs" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "medicines" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "medicines" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "brands" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "brands" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "brands" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "arv_medicines" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "arv_medicines" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "arv_medicines" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "non_arv_medicines" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "non_arv_medicines" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "non_arv_medicines" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "regiments" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "regiments" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "regiment_medicines" ADD FOREIGN KEY ("regiment_id") REFERENCES "regiments" ("id");

ALTER TABLE "regiment_medicines" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "regiment_medicines" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "regiment_medicines" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "prescriptions" ADD FOREIGN KEY ("patient_id") REFERENCES "patients" ("id");

ALTER TABLE "prescriptions" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "prescriptions" ADD FOREIGN KEY ("regiment_id") REFERENCES "regiments" ("id");

ALTER TABLE "prescriptions" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "prescriptions" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "prescription_medicines" ADD FOREIGN KEY ("prescription_id") REFERENCES "prescriptions" ("id");

ALTER TABLE "prescription_medicines" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "prescription_medicines" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "prescription_medicines" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("pic_id") REFERENCES "users" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("province_id_owner") REFERENCES "provinces" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("sudin_kab_kota_id_owner") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("upk_id_owner") REFERENCES "upk" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("province_id_for") REFERENCES "provinces" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("sudin_kab_kota_id_for") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("upk_id_for") REFERENCES "upk" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "orders" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "order_items" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

ALTER TABLE "order_items" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "order_items" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "order_logs" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

ALTER TABLE "order_logs" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("pic_id") REFERENCES "users" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "distribution_plans" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "distribution_plans_items" ADD FOREIGN KEY ("distribution_plan_id") REFERENCES "distribution_plans" ("id");

ALTER TABLE "distribution_plans_items" ADD FOREIGN KEY ("order_item_id") REFERENCES "order_items" ("id");

ALTER TABLE "distribution_plans_items" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "distribution_plans_items" ADD FOREIGN KEY ("inventory_id") REFERENCES "inventories" ("id");

ALTER TABLE "distribution_plans_items" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "distribution_plans_items" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "distribution_logs" ADD FOREIGN KEY ("distribution_plan_id") REFERENCES "distribution_plans" ("id");

ALTER TABLE "distribution_logs" ADD FOREIGN KEY ("actor_id") REFERENCES "users" ("id");

ALTER TABLE "distribution_logs" ADD FOREIGN KEY ("prev_order_id") REFERENCES "orders" ("id");

ALTER TABLE "distribution_logs" ADD FOREIGN KEY ("prev_pic_id") REFERENCES "users" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("distribution_plan_id") REFERENCES "distribution_plans" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("received_by") REFERENCES "users" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "receipts" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "receipt_items" ADD FOREIGN KEY ("receipt_id") REFERENCES "receipts" ("id");

ALTER TABLE "receipt_items" ADD FOREIGN KEY ("inventory_id") REFERENCES "inventories" ("id");

ALTER TABLE "receipt_items" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");

ALTER TABLE "receipt_items" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("receipt_item_id") REFERENCES "receipt_items" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "inventories" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "inventory_adjustment" ADD FOREIGN KEY ("inventory_id") REFERENCES "inventories" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("inventory_id") REFERENCES "inventories" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("actor_id") REFERENCES "users" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("medicine_id") REFERENCES "medicines" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("province_id") REFERENCES "provinces" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("sudin_kab_kota_id") REFERENCES "sudin_kab_kota" ("id");

ALTER TABLE "inventory_logs" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("patient_id") REFERENCES "patients" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("inventory_id") REFERENCES "inventories" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("treatment_id") REFERENCES "treatments" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("test_hiv_id") REFERENCES "test_hiv" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("test_ims_id") REFERENCES "test_ims" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("test_vl_cd4_id") REFERENCES "test_vl_cd4" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("profilaksis_id") REFERENCES "profilaksis" ("id");

ALTER TABLE "inventory_usage" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");

CREATE UNIQUE INDEX ON "upk_treatment_logs" ("patient_id", "upk_id");

CREATE UNIQUE INDEX ON "regiment_medicines" ("regiment_id", "medicine_id");

ALTER TABLE "patient_logs" ADD FOREIGN KEY ("patient_id") REFERENCES "patients" ("id");

ALTER TABLE "patient_logs" ADD FOREIGN KEY ("visit_id") REFERENCES "visits" ("id");

ALTER TABLE "patient_logs" ADD FOREIGN KEY ("upk_id") REFERENCES "upk" ("id");

ALTER TABLE "patient_logs" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "patient_logs" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "settings" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "settings" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

CREATE UNIQUE INDEX ON "patients" ("nik");
CREATE UNIQUE INDEX ON "users" ("nik");
CREATE UNIQUE INDEX ON "treatments" ("visit_id");
CREATE UNIQUE INDEX ON "test_hiv" ("visit_id");
CREATE UNIQUE INDEX ON "test_ims" ("visit_id");
CREATE UNIQUE INDEX ON "test_vl_cd4" ("visit_id");
