const development = {
  BASE_URL: "http://" + (process.env.BASE_URL || "54.169.195.5:1601"),
  SENTRY: {
    DNS: "https://3a9f4f2020534087abe458aa059807ae@sentry.io/3630916",
    ENVIRONMENT: process.env.SENTRY_ENV || "localhost"
  },
  SENDGRID: {
    API_KEY:
      "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"
  },
  MAILGUN: {
    API_KEY:
      "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",
    DOMAIN: "kemkes.go.id"
  },
  MONGODB: {
    CONNECTION_STRING:
      "mongodb://msiha:msiha2020_GRANmelia@202.149.71.42:27017/msiha-development"
  },
  RAJASMS: {
    API_KEY: "ba495b3f5f275d9ded9f379b16ba44d0",
    USERNAME: "histudiodevs",
    URL_REGULER: "http://sms114.xyz/sms/smsreguler.php",
    URL_REGULER_JSON: "http://sms114.xyz/sms/api_sms_reguler_send_json.php",
    URL_MASKING: "http://sms114.xyz/sms/smsmasking.php",
    URL_MASKING_OTP:
      "http://sms114.xyz/sms/http://sms114.xyz/sms/smsmasking_otp.php",
    URL_CALLBACK: "http://202.149.71.42:8001/public/webhook/sms"
  }
};

const production = {
  BASE_URL: "http://" + (process.env.BASE_URL || "127.0.0.1:9090"),
  SENTRY: {
    DNS: "https://f9b6455946284aa5892aef38a5f12a56@sentry.io/1848774",
    ENVIRONMENT: "production"
  },
  SENDGRID: {
    API_KEY:
      "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"
  },
  MAILGUN: {
    API_KEY:
      "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",
    DOMAIN: "kemkes.go.id"
  },
  MONGODB: {
    CONNECTION_STRING:
      (process.env.MONGODB_URL || "mongodb://simongoms:51hapims$$$@192.168.48.153:27017/sihapimondb")
  },
  RAJASMS: {
    API_KEY: "caf441a1f4a26fb4e13539d50ebb6b81",
    USERNAME: "zamascodev",
    URL_REGULER: "http://sms114.xyz/sms/smsreguler.php",
    URL_MASKING: "http://sms114.xyz/sms/smsmasking.php",
    URL_MASKING_OTP:
      "http://sms114.xyz/sms/http://sms114.xyz/sms/smsmasking_otp.php",
    URL_CALLBACK: "http://202.149.71.42:8001/public/webhook/sms"
  }
};

let generalConfig;

switch (process.env.NODE_ENV) {
  case "development":
    generalConfig = development;
    break;
  case "production":
    generalConfig = production;
    break;
  default:
    generalConfig = development;
}

module.exports = generalConfig;
