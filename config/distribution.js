module.exports = {
  PLAN_STATUS: {
    DRAFTING: "DRAFT",
    CONFIRMED: "CONFIRMED",
    EXECUTED: "EXECUTED"
  }
};
