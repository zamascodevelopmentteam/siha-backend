module.exports = {
  GENDER: {
    LAKI_LAKI: "LAKI-LAKI",
    PEREMPUAN: "PEREMPUAN"
  },
  // STATUS_PATIENT: {
  //   BLM_TAHU: "BELUM TAHU",
  //   HIV_NEGATIF: "HIV NEGATIF",
  //   HIV_POSITIF: "HIV POSITIF",
  //   ODHA: "ODHA"
  // },
  STATUS_PATIENT: {
    // BLM_TAHU: "Bukan ODHA",
    BLM_TAHU: "BELUM TAHU",
    HIV_NEGATIF: "HIV NEGATIF",
    HIV_POSITIF: "HIV POSITIF",
    ODHA: "ODHA",
    ODHA_LAMA: "ODHA LAMA"
  },
  // catatan
  // LABEL_STATUS_PATIENT: {
  //   "BELUM TAHU": "Belum Tahu",
  //   "HIV NEGATIF": "HIV NEGATIF",
  //   "HIV POSITIF": "ODHA",
  //   ODHA: "ODHA Dalam Pengobatan",
  //   "ODHA LAMA": "ODHA Masuk Perawatan"
  // },
  // end catatan
  VISIT_TYPE: {
    PRESC_ONLY: "PRESCRIPTION",
    IN_TRANSIT: "IN TRANSIT",
    TEST: "TEST",
    TREATMENT: "TREATMENT"
  },
  VISIT_ACT_TYPE: {
    HIV: "HIV",
    IMS: "IMS",
    VL_CD4: "VL_CD4",
    TREATMENT: "TREATMENT",
    PROFILAKSIS: "PROFILAKSIS",
    LAB_USAGE_NONPATIENT: "LAB_USAGE_NONPATIENT"
  },
  TEST_TYPE: {
    HIV: "HIV",
    IMS: "IMS",
    VL_CD4: "VL_CD4"
  },
  KEL_POPULASI: {
    WPS: "WPS",
    WARIA: "Waria",
    PENASUN: "Penasun",
    LSL: "LSL",
    BUMIL: "Bumil",
    PASIEN_TB: "Pasien TB",
    PASIEN_IMS: "Pasien IMS",
    PASIEN_HEPATITIS: "Pasien Hepatitis",
    PASANGAN_ODHA: "Pasangan ODHA",
    ANAK_ODHA: "Anak ODHA",
    WBP: "WBP",
    POPULASI_UMUM: "Populasi Umum"
  },
  HIV_TEST_TYPE: {
    RDT: "RDT",
    PCR_DNA: "PCR-DNA",
    PCR_RNA: "PCR-RNA",
    PCR_RNA_EID: "PCR-DNA (EID)",
    NAT: "NAT",
    WB: "WESTERN BLOT",
    ELISA: "ELISA"
  },
  STATUS_TB: {
    NEGATIF_TB: "NEGATIF TB",
    TIDAK_TAU_STATUS: "TIDAK TAU STATUS",
    POSITIF_TB: "POSITIF TB",
  },
  PERIKSA_TB: {
    NEGATIF_TB: "NEGATIF TB",
    POSITIF_TB: "POSITIF TB",
    TIDAK_DIPERIKSA: "TIDAK DIPERKSA"
  },
  STATUS_FUNGSIONAL: {
    KERJA: "KERJA",
    AMBULATORI: "AMBULATORI",
    BARING: "BARING"
  },
  STADIUM_KLINIS: {
    STADIUM_1: "STADIUM 1",
    STADIUM_2: "STADIUM 2",
    STADIUM_3: "STADIUM 3",
    STADIUM_4: "STADIUM 4"
  },
  STATUS_TB_TPT: {
    TDK_DIBERIKAN: "TIDAK DIBERIKAN",
    INH: "INH",
    "3HP": "3HP",
    RIFAMPETINE: "RIFAMPETINE"
  },
  RUJUK_LAB_LANJUT: {
    WB: "WESTERN BLOT",
    ELISA: "ELISA"
  },
  STATUS_PADUAN: {
    ORISINAL: "ORISINAL",
    SUBSTITUSI: "SUBSTITUSI",
    SWITCH: "SWITCH"
  },
  TES_LAB: {
    VL: "VL (KOPI/ml)",
    CD4: "CD4 (SEL/m3)"
  },
  VL_TEST_TYPE: {
    VL: "VL",
    CD4: "CD4"
  },
  HASIL_LAB_SATUAN: {
    KOPI: "KOPI/ml",
    SEL: "SEL/m3"
  },
  ALASAN_TEST: {
    BY_DOCTOR: "Rekomendasi Dokter",
    SECOND_OPINION: "Second Opinion",
    SUKARELA: "Sukarela",
    NOTIF_COUPLE: "Notifikasi Pasangan",
    SKRINING: "SKRINING"
  },
  NOTIF_COUPLE: {
    TDK_MENERIMA: "tidak menerima",
    MENERIMA: "menerima",
    MENOLAK: "menolak",
    TDK_MEMENUHI: "tidak memenuhi syarat"
  },
  TEST_RESULT: {
    REAKTIF: "REAKTIF",
    INKONKLUSIF: "INKONKLUSIF",
    NON_REAKTIF: "NON_REAKTIF",
    INVALID: "INVALID",
    NOT_DETECTED: "NOT DETECTED",
    DETECTED: "DETECTED",
    ERROR: "ERROR",
    NO_RESULT: "NO RESULT",
    KALIBRASI: "KALIBRASI",
    TERBUANG: "TERBUANG",
    GANGGUAN_TEKNIS: "GANGGUAN TEKNIS",
    SEBAB_LAIN: "SEBAB LAINNYA",
    KONTROL: "KONTROL",
    LT_200: "< 200",
    BTW_200_350: "200 - 350",
    GT_350: "> 350",
    LTE_1000: "<= 1000",
    GT_1000: "> 1000",
    20: "< 20",
    34: "< 34",
    40: "< 40",
    50: "< 50",
    INVALIDERROR: "INVALID / ERROR"
  },
  LAST_ORDER_STATUS: {
    DRAFT: "DRAFT",
    EVALUATION: "EVALUATION",
    PROCESSED: "PROCESSED",
    EXECUTED: "EXECUTED",
    REJECTED: "REJECTED"
  },
  LAST_APPROVAL_STATUS: {
    DRAFT: "DRAFT",
    IN_EVALUATION: "IN_EVALUATION",
    APPROVED_INTERNAL: "APPROVED_INTERNAL",
    APPROVED_FULL: "APPROVED_FULL",
    REJECTED_INTERNAL: "REJECTED_INTERNAL",
    REJECTED_UPPER: "REJECTED_UPPER",
    REJECTED: "REJECTED"
  },
  PLAN_STATUS: {
    DRAFT: "DRAFT",
    EVALUATION: "EVALUATION",
    PROCESSED: "PROCESSED",
    ACCEPTED_FULLY: "ACCEPTED_FULLY",
    ACCEPTED_DIFFERENT: "ACCEPTED_DIFFERENT",
    LOST: "LOST",
    REJECTED: "REJECTED"
  },
  RECEIPT_STATUS: {
    ACCEPTED_FULLY: "ACCEPTED_FULLY",
    ACCEPTED_DIFFERENT: "ACCEPTED_DIFFERENT",
    LOST: "LOST"
  },
  RECEIPT_ITEM_STATUS: {
    NOT_CHANGED: "NOT_CHANGED",
    MORE_THAN_DEMAND: "MORE_THAN_DEMAND",
    LESS_THAN_DEMAND: "LESS_THAN_DEMAND"
  },
  ORDER_STATUS_STATE: {
    DRAFT: "DRAFT",
    IN_EVALUATION: "EVALUATION",
    APPROVED_INTERNAL: "EVALUATION",
    APPROVED_FULL: "PROCESSED",
    REJECTED_INTERNAL: "REJECTED",
    REJECTED_UPPER: "REJECTED"
  },
  AKHIR_FOLLOW_UP: {
    RUJUK_KELUAR: "RUJUK KELUAR",
    MENINGGAL: "MENINGGAL",
    BERHENTI_ARV: "BERHENTI ARV"
  },
  DIST_STATUS_STATE: {
    DRAFT: "DRAFT",
    IN_EVALUATION: "EVALUATION",
    APPROVED_FULL: "PROCESSED",
    REJECTED: "REJECTED"
  },
  fUND_SOURCE: {
    APBN: "APBN",
    NON_APBN: "NON_APBN"
  },
  LOGISTIC_ROLE: {
    LAB_ENTITY: "LAB_ENTITY",
    PHARMA_ENTITY: "PHARMA_ENTITY",
    UPK_ENTITY: "UPK_ENTITY",
    SUDIN_ENTITY: "SUDIN_ENTITY",
    PROVINCE_ENTITY: "PROVINCE_ENTITY",
    MINISTRY_ENTITY: "MINISTRY_ENTITY"
  },
  USER_ROLE: {
    ADMIN: "ADMIN",
    LAB_STAFF: "LAB_STAFF",
    PATIENT: "PATIENT",
    PHARMA_STAFF: "PHARMA_STAFF",
    RR_STAFF: "RR_STAFF",
    SUDIN_STAFF: "SUDIN_STAFF",
    PROVINCE_STAFF: "PROVINCE_STAFF",
    MINISTRY_STAFF: "MINISTRY_STAFF",
    DOCTOR: "DOCTOR"
  },
  PKG_UNIT_TYPE: {
    TABLET: "TABLET",
    TEST: "TEST",
    KAPSUL: "KAPSUL",
    PAKET: "PAKET",
    VIAL: "VIAL",
    PCS: "PCS",
    BUAH: "BUAH",
    KIT: "KIT",
    BOTOL: "BOTOL",
    BOX: "BOX",
    SACHET: "SACHET"
  },
  MEDICINE_TYPE: {
    ARV: "ARV",
    NON_ARV: "NON_ARV"
  },
  MEDICINE_SEDIAAN: {
    TUNGGAL: "TUNGGAL",
    BOOSTED: "BOOSTED",
    KDT_FDC: "KDT/FDC",
    FDC_PEDIATRIK: "FDC_PEDIATRIK"
  },
  REGIMENT: {
    LINI1: "LINI I",
    LINI1_PEDIATRIK: "LINI I PEDIATRIK",
    LINI2: "LINI II",
    LINI2_PEDIATRIK: "LINI II PEDIATRIK",
    PROFILAKSIS: "PROFILAKSIS"
  },
  LBPHA2: {
    LINI1: "LINI I",
    LINI1_PEDIATRIK: "LINI I PEDIATRIK",
    LINI2: "LINI II",
    LINI2_PEDIATRIK: "LINI II PEDIATRIK",
    PROFILAKSIS: "PROFILAKSIS"
  },
  STATUS_KETERSEDIAAN: {
    STOCKOUT: "STOCKOUT",
    SHORTAGE: "SHORTAGE",
    NORMAL: "NORMAL",
    OVERSTOCK: "OVERSTOCK"
  },
  ADJUSTMENT_TYPE: {
    ADDITION: "ADDITION",
    DEDUCTION: "DEDUCTION"
  },
  DIST_TYPE: {
    HIBAH: "HIBAH",
    PEMBELIAN_APBN: "PEMBELIAN_APBN",
    PEMBELIAN_APBD: "PEMBELIAN_APBD",
    LUAR_NEGERI: "LUAR_NEGERI",
    BINA_FARMA: "BINA_FARMA"
  },
  LAB_PHARMA: {
    LAB_ENTITY: "Gudang Laboratorium",
    PHARMA_ENTITY: "Gudang Farmasi"
  },
  REAGEN_TYPE: {
    VIRAL_LOAD: "VIRAL_LOAD",
    EID: "EID",
    CD4: "CD4"
  },
  NOTIFICATION: {
    R_1WEEK: {
      type: "1WEEK_REMINDER",
      title: "1 Minggu lagi obat anda habis!",
      message:
        "Jangan lupa untuk berkunjung kembali ke faskes dimana anda berobat, segera hubungi petugas untuk konfirmasi kehadiran anda."
    },
    R_1DAY: {
      type: "1DAY_REMINDER",
      title: "1 Hari lagi obat anda habis!",
      message:
        "Jangan lupa untuk berkunjung kembali ke faskes dimana anda berobat, segera hubungi petugas untuk konfirmasi kehadiran anda."
    },
    R_0DAY: {
      type: "DDAY_REMINDER",
      title: "Jangan lupa berkunjung hari ini!",
      message:
        "Jangan lupa untuk berkunjung kembali ke faskes dimana anda berobat, segera hubungi petugas untuk konfirmasi kehadiran anda."
    },
    R_1WEEK_AFT: {
      type: "1WEEK_REMINDER_DAILY",
      title: "Obat anda akan habis!",
      message:
        "Jangan lupa untuk berkunjung kembali ke faskes dimana anda berobat, segera hubungi petugas untuk konfirmasi kehadiran anda."
    },
    R_LATE: {
      type: "LATE_PRESENCE",
      title: "Obat anda sudah habis!",
      message: "Segera berkunjung kembali ke faskes dimana anda berobat!"
    },
    C_SCH_CHANGED: {
      type: "SCHEDULE_CHANGED",
      title: "telah mengubah jadwal kedatangan!",
      message:
        "Mohon segera konfirmasi perubahan jadwal kedatangan pasien melalui web atau aplikasi mobile android."
    },
    C_SCH_CONFIRMED: {
      type: "SCHEDULE_CONFIRMED",
      title: "telah menyetujui jadwal kedatangan anda!",
      message:
        "Terimakasih telah menggunakan Aplikasi M-SIHA Android, kami tunggu kehadiran anda."
    },
    SMS_OTP: {
      type: "SMS_OTP",
      title: "-",
      message:
        "Silahkan gunakan Kode OTP berikut untuk keperluan autentikasi di Aplikasi SIHA Android:  "
    }
  },
  HUBUNGAN_NOTIF_COUPLE: {
    SUAMI_ISTRI: "Suami/Istri",
    ANAK_KANDUNG: "Anak Kandung",
    ORANG_TUA: "Orang Tua Kandung",
    MITRA_SEKS: "Mitra Seks",
    MITRA_JARUM: "Mitra Berbagi Jarum"
  },
  REASON_VISIT_IMS: [
    "SKRINING",
    "SAKIT",
    "FOLLOWUP"
  ],
  COMPLAINT_IMS: [
    "DUH_TUBUH",
    "GATAL",
    "KENCING_SAKIT",
    "NYERI_PERUT",
    "LECET",
    "BINTIL_SAKIT",
    "LUKA_ULKUS",
    "JENGGER",
    "BENJOLAN",
    "TIDAK_ADA"
  ],
  PHYSICAL_EXAMINATION: [
    "dtv",
    "dts",
    "nyeri_perut",
    "lecet",
    "bintil_sakit",
    "luka_ulkus",
    "jengger",
    "bubo",
    "nyeri_goyang_serviks",
    "dtu",
    "scortum_bengkak",
    "dta",
    "dtm",
    "menstruasi",
    "tidak_ada"
  ],
  PREGNANCY_STAGE: [
    "TRIMESTER_1",
    "TRIMESTER_2",
    "TRIMESTER_3"
  ],
  REAKTIF: [
    'REAKTIF',
    'NON_REAKTIF',
    'INVALID'
  ],
  RPR_VDRL_TITER: [
    '0',
    '1/2',
    '1/4',
    '1/8',
    '1/16',
    '1/32'
  ],
  LSM_PENJANGKAU: [
    'SPIRITIA',
    'IAC',
    'UNFPA',
    'IPPI',
    'YPI',
    'PKVHI',
    'PKBI',
    'KADER KESEHATAN',
    'DOTS/TB',
    'LAPAS RUTAN',
    'HEPATITIS',
    'TB',
    'KIA',
    'POLI_LAINNYA'
  ],
  KELOMPOK_POPULASI: [
    'WPS',
    'Waria',
    'Penasun',
    'LSL',
    'Bumil',
    'Pasien TB',
    'Pasien IMS',
    'Pasien Hepatitis',
    'Pasangan ODHA',
    'Anak ODHA',
    'WBP',
    'Lainnya'
  ],
  VARIABLE_TYPE: {
    newOdhaHIV:
      "1.1 Jumlah ODHA baru yang masuk perawatan HIV selama bulan ini",
    odhaReferIn:
      "1.2 Jumlah ODHA yang dirujuk masuk selama perawatan HIV bulan ini",
    odhaReferOut:
      "1.3 Jumlah ODHA yang dirujuk keluar selama perawatan HIV bulan ini",
    cumulativeOdhaHIV:
      "1.4 Jumlah kumulatif ODHA pernah masuk perawatan HIV s/d akhir bulan ini",
    odhaNonArtNotVisit:
      "1.5 Jumlah ODHA yang belum mulai ART dan tidak hadir pada bulan ini",
    cumulativeOdhaNonArtDeath:
      "1.6 Jumlah kumulatif ODHA yang belum mulai ART meninggal sampai dengan bulan ini",
    totalVisitHiv:
      "1.7 Jumlah ODHA berkunjung ke perawatan HIV selama bulan ini",
    newOdhaArt: "2.1 Jumlah ODHA baru yang memulai ART selama bulan ini",
    odhaReferInArt:
      "2.2 Jumlah ODHA yang dirujuk masuk dengan ART selama bulan ini",
    odhaReferOutArt:
      "2.3 Jumlah ODHA yang dirujuk keluar dengan ART selama bulan ini",
    cumulativeOdhaArt:
      "2.4 Jumlah kumulatif ODHA pernah memulai ART s/d akhir bulan ini",

    odhaDeath:
      "3.1 Jumlah ODHA yang dilaporkan meninggal dunia selama bulan ini",
    odhaStopArt: "3.2 Jumlah ODHA yang pasti menghentikan ART selama bulan ini",
    odhaNotVisit: "3.3 Jumlah ODHA yang tidak hadir selama akhir bulan ini",
    failFollowUp3Month:
      "3.4 Jumlah ODHA yang gagal follow-up > 3 bulan selama akhir bulan ini",
    cumulativeOdhaArt_3:
      "3.5 Jumlah kumulatif ODHA dengan ART s/d akhir bulan ini",
    odhaRejimen1Original:
      "3.5.1 Jumlah yang masih dengan rejimen lini ke-1 orisinal",
    odhaRejimen1: "3.5.2 Jumlah yang substitusi dalam rejimen lini ke-1",
    odhaRejimen2: "3.5.3 Jumlah switch ke rejimen lini ke-2",

    patients_tb: "4.1 Jumlah ODHA yang dipreiksa status TB selama bulan ini",
    patients_t_hiv:
      "4.2 Jumlah kasus baru orang dengan koinfeksi TB-HIV selama bulan ini",
    patients_tb_hiv_arv:
      "4.3 Jumlah kasus baru orang dengan koinfeksi TB-HIV dan mendapatkan OAT dan ARV selama bulan ini",

    new_patients_PPK:
      "5.1 Jumlah orang baru yang mendapat PPK selama bulan ini",
    patients_PPK: "5.2 Jumlah orang yang sedang mendapat PPK selama bulan ini",

    new_patients_PP_INH:
      "6.1 Jumlah ODHA baru masuk perawatan dan memulai PP INH selama bulan ini",
    old_patients_PP_INH:
      "6.2 Jumlah ODHA lama masuk perawatan (>30 hari) dan memulai PP INH selama bulan ini",
    patients_rifampentin:
      "6.3 Jumlah ODHA memulai Rifampentin selama bulan ini",

    patients_VL: "7.1 Jumlah ODHA yang dites VL selama bulan ini",
    patients_over_VL:
      "7.2 Jumlah ODHA yang jumlah virusnya tersupresi (<1.000 kopi/ml) selama bulan ini",

    patients_ARV:
      "8.1 Jumlah ODHA yang mengambil ARV >1 bulan selama bulan ini",
    patients_profilaksis:
      "8.2 Jumlah orang yang mendapat profilaksis (pasca pajanan dan bayi baru lahir)",

    patients_offer_couple_notif:
      "9.1 Jumlah ODHA yang ditawarkan notifikasi pasangan selama bulan ini",
    patients_accept_offer_couple_notif:
      "9.2 Jumlah ODHA yang menerima tawaran notifikasi pasangan selama bulan ini",
    couple:
      "9.3 Jumlah pasangan/anak ODHA yang didata oleh ODHA selama bulan ini",

    odhaIms: "10.1 Jumlah ODHA yang Dites IMS",
    odhaImsPositive: "10.2 Jumlah ODHA yang Dites IMS dan Hasilnya Positif",
    odhaSifilis: "10.3 Jumlah ODHA yang Dites Sifilis",
    odhaSifilisPositive: "10.4 Jumlah ODHA yang Dites Sifilis dan Hasilnya Positif",
    
    code: "name"
  },
  MEDICINE_CATEGORIES: {
    IO_IMS: "IO/IMS", 
    ALKES: "ALKES (BHP/LAINNYA)", 
    REAGEN_LAB: "REAGEN/LAB" 
  }
};
