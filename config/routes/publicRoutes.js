const publicRoutes = {
  "POST /auth/register": "AuthController.register",
  "POST /auth/login": "AuthController.login",
  "POST /auth/refresh": "AuthController.refresh",
  "GET /webhook/sms": "HookController.rajasmsHook",
  "POST /webhook/sms": "HookController.rajasmsHook",
  // 'POST /auth/reset': 'AuthController.reset',
  "POST /auth/forgot-password": "AuthController.forgotPassword",
};

module.exports = publicRoutes;
