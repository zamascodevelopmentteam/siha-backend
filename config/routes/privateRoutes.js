const rolePolicy = require("../../api/policies/role.policy");
const visitPolicy = require("../../api/policies/visit.policy");
const patientPolicy = require("../../api/policies/patient.policy");
const medicinePolicy = require("../../api/policies/medicine.policy");
const roles = require("../roles");

const masterdataMedicineValidation = require("../../api/middlewares/validation/masterdata.medicine");
const masterdataArrMedicineValidation = require("../../api/middlewares/validation/masterdata.arrayofmedicines");
const visitArrMedicineValidation = require("../../api/middlewares/validation/visit.arrayofmedicines");
const masterdataUserValidation = require("../../api/middlewares/validation/masterdata.user");
const masterdataPatientValidation = require("../../api/middlewares/validation/masterdata.patient");
const reportPolicy = require("../../api/middlewares/report/report.policy");

const util = {
  "GET /utils/enum": "UtilController.getEnum",
  "GET /utils/download_xlsx_example": "UtilController.downloadSheetJS",
  "GET /utils/download_xlsx_lbpha2": "UtilController.downloadSheetJS"
};

const cronjob = {
  "GET /cronjob/permintaan_reguler_arv_upk":
    "CronJobController.generatePermintaanRegulerARV_upk",
  "GET /cronjob/permintaan_reguler_arv_sudin":
    "CronJobController.generatePermintaanRegulerARV_sudin",
  "GET /cronjob/permintaan_reguler_arv_province":
    "CronJobController.generatePermintaanRegulerARV_province",
  "GET /cronjob/notify_patients_h7": "CronJobController.notifyPatientsH_7",
  "GET /cronjob/notify_patients_h1": "CronJobController.notifyPatientsH_1",
  "GET /cronjob/notify_patients_h0": "CronJobController.notifyPatientsH_0"
};

const masterData = {
  //Medicine Ops
  "POST /master-data/medicines": {
    path: "MasterDataController.addOrUpdateMedicine",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataMedicineValidation().add()
    ]
  },
  "PUT /master-data/medicines/:medicineId": {
    path: "MasterDataController.addOrUpdateMedicine",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataMedicineValidation().add()
    ]
  },
  "DELETE /master-data/medicines/:medicineId": {
    path: "MasterDataController.deleteMedicine",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF]), medicinePolicy()]
  },
  "GET /master-data/medicines": {
    path: "MasterDataController.getAllMedicines",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.LAB_STAFF
      ])
    ]
  },
  "GET /master-data/medicines/:medicineId": {
    path: "MasterDataController.getMedicine",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.LAB_STAFF
      ]),
      medicinePolicy()
    ]
  },
  "GET /master-data/medicines/:medicineId/brands": {
    path: "MasterDataController.getAllMedicineBrands",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.LAB_STAFF
      ]),
      medicinePolicy()
    ]
  },
  //End of Medicine ops

  //Brand ops
  "POST /master-data/brands": {
    path: "MasterDataController.addOrUpdateBrand",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF]), medicinePolicy()]
  },
  "PUT /master-data/brands/:brandId": {
    path: "MasterDataController.addOrUpdateBrand",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "DELETE /master-data/brands/:brandId": {
    path: "MasterDataController.deleteBrand",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/brands": {
    path: "MasterDataController.getAllBrands",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /master-data/brands/:brandId": {
    path: "MasterDataController.getBrand",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  //End of Brand ops

  //Regiment ops
  "POST /master-data/regiments": {
    path: "MasterDataController.addOrUpdateRegiment",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataArrMedicineValidation().add()
    ]
  },
  "PUT /master-data/regiments/:regimentId": {
    path: "MasterDataController.addOrUpdateRegiment",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataArrMedicineValidation().add()
    ]
  },
  "DELETE /master-data/regiments/:regimentId": {
    path: "MasterDataController.deleteRegiment",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/regiments": {
    path: "MasterDataController.getAllRegiments",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/regiments/:regimentId": {
    path: "MasterDataController.getRegiment",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  //End of Regiment ops

  //Prescription ops
  "POST /master-data/prescriptions": {
    path: "MasterDataController.addOrUpdatePrescription",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataArrMedicineValidation().add()
    ]
  },
  "PUT /master-data/prescriptions/:prescriptionId": {
    path: "MasterDataController.addOrUpdatePrescription",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataArrMedicineValidation().add()
    ]
  },
  "DELETE /master-data/prescriptions/:prescriptionId": {
    path: "MasterDataController.deletePrescription",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/prescriptions": {
    path: "MasterDataController.getAllPrescriptions",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/prescriptions/:prescriptionId": {
    path: "MasterDataController.getPrescription",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  //End of Prescription ops

  //Province ops
  "POST /master-data/provinces": {
    path: "MasterDataController.addOrUpdateProvince",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "PUT /master-data/provinces/:provinceId": {
    path: "MasterDataController.addOrUpdateProvince",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "DELETE /master-data/provinces/:provinceId": {
    path: "MasterDataController.deleteProvince",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/provinces": {
    path: "MasterDataController.getAllProvinces",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ])
    ]
  },
  "GET /master-data/provinces/:provinceId": {
    path: "MasterDataController.getProvince",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /master-data/provinces/:provinceId/sudin": {
    path: "MasterDataController.getAllSudin",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  //End of Province ops

  //Sudin ops
  "POST /master-data/sudin": {
    path: "MasterDataController.addOrUpdateSudin",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "PUT /master-data/sudin/:sudinId": {
    path: "MasterDataController.addOrUpdateSudin",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "DELETE /master-data/sudin/:sudinId": {
    path: "MasterDataController.deleteSudin",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/sudin": {
    path: "MasterDataController.getAllSudin",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /master-data/sudin/:sudinId": {
    path: "MasterDataController.getSudin",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ])
    ]
  },
  "GET /master-data/sudin/:sudinId/upk": {
    path: "MasterDataController.getAllUpk",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ])
    ]
  },
  //End of Sudin ops

  //UPK ops
  "POST /master-data/upk": {
    path: "MasterDataController.addOrUpdateUpk",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "PUT /master-data/upk/:upkId": {
    path: "MasterDataController.addOrUpdateUpk",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "DELETE /master-data/upk/:upkId": {
    path: "MasterDataController.deleteUpk",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/upk": {
    path: "MasterDataController.getAllUpk",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ])
    ]
  },
  "GET /master-data/upk/:upkId": {
    path: "MasterDataController.getUpk",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  //End of UPK ops

  //User ops
  "POST /master-data/users": {
    path: "MasterDataController.addOrUpdateUser",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataUserValidation().add()
    ]
  },
  "PUT /master-data/users/:userId": {
    path: "MasterDataController.addOrUpdateUser",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataUserValidation().add()
    ]
  },
  "DELETE /master-data/users/:userId": {
    path: "MasterDataController.deleteUser",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/users": {
    path: "MasterDataController.getAllUsers",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/users/:userId": {
    path: "MasterDataController.getUser",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },

  "GET /master-data/users-by-role": {
    path: "MasterDataController.getAllUsersByRole",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  //End of UPK ops

  //Patient ops
  "POST /master-data/patients": {
    path: "MasterDataController.addOrUpdatePatient",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      masterdataPatientValidation().add()
    ]
  },
  "PUT /master-data/patients/:patientId": {
    path: "MasterDataController.addOrUpdatePatient",
    middlewares: [
      rolePolicy([roles.MINISTRY_STAFF]),
      patientPolicy(),
      masterdataPatientValidation().add()
    ]
  },
  "DELETE /master-data/patients/:patientId": {
    path: "MasterDataController.deletePatient",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF]), patientPolicy()]
  },
  "GET /master-data/patients": {
    path: "MasterDataController.getAllPatients",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/patients/:patientId": {
    path: "MasterDataController.getPatient",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF]), patientPolicy()]
  },
  //End of Patient ops

  //producer ops
  "POST /master-data/producers": {
    path: "MasterDataController.addOrUpdateProducer",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "PUT /master-data/producers/:producerId": {
    path: "MasterDataController.addOrUpdateProducer",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "DELETE /master-data/producers/:producerId": {
    path: "MasterDataController.deleteProducer",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/producers/:producerId": {
    path: "MasterDataController.getProducer",
    middlewares: [rolePolicy([roles.MINISTRY_STAFF])]
  },
  "GET /master-data/producers": {
    path: "MasterDataController.getAllProducers",
    middlewares: [
      rolePolicy([
        roles.ADMIN,
        roles.LAB_STAFF,
        roles.PATIENT,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.UPK_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF,
        roles.DOCTOR
      ])
    ]
  },
  //End of Province ops
};

const report = {
  "GET /report/test-and-treat/cohort": {
    path: "ReportIntraController.getCohortReport",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/test-and-treat/hiv": {
    path: "ReportIntraController.getMasterHivReport2",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/test-and-treat/hivById": {
    path: "ReportIntraController.getMasterHivReportById",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/test-and-treat/hivExcel": {
    path: "ReportIntraController.getMasterHivReportExcel",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  // new
  "GET /report/test-report/hiv": {
    path: "ReportIntraController.getTestHivReport",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/test-report/hiv-individual": {
    path: "ReportIntraController.getTestHivReportIndividual",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/rekap-rencana-kunjungan-odha": {
    path: "ReportIntraController.getRekapRencanaKunjunganOdha",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/rekap-tes-vl": {
    path: "ReportIntraController.getTestVl",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/rekap-tes-vl-cd4": {
    path: "ReportIntraController.getVlcd4Report",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF
      ])
    ]
  },
  // end new
  "GET /report/test-and-treat/ims": {
    path: "ReportIntraController.getMasterImsReport",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/lbpha2": {
    path: "ReportLBPHA2Controller.getLbpha2Report",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/lbpha2Excel": {
    path: "ReportLBPHA2Controller.getLbpha2ReportExcel",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/ArvNonArvExcel": {
    path: "ReportLBPHA2Controller.getArvNonArvExcel",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/regimenExcel": {
    path: "ReportLBPHA2Controller.getRegimenExcel",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/register-lab": {
    path: "ReportRegisterLabController.getRegisterLabReport",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/lbadb-detail": {
    path: "ReportRegisterLabController.getLbadbDetailReport",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /report/lbpha1/meta": {
    path: "ReportLBPHA1Controller.getMetaReport",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/masuk-perawatan-hiv": {
    path: "ReportLBPHA1Controller.getOnHivDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/masuk-dengan-art": {
    path: "ReportLBPHA1Controller.getOnArtDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/dampak-art": {
    path: "ReportLBPHA1Controller.getAfterArtDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/hiv-ims": {
    path: "ReportLBPHA1Controller.getHivImsDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/koinfeksi-tb-hiv": {
    path: "ReportLBPHA1Controller.getCoenfectionTBDasboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/pengobatan-ppk": {
    path: "ReportLBPHA1Controller.getTreatmentPPKDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/tpt": {
    path: "ReportLBPHA1Controller.getTherapyTPTDasboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/viral-load": {
    path: "ReportLBPHA1Controller.getViralLoadDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/penggunaan-arv-khusus": {
    path: "ReportLBPHA1Controller.getArvSpecialDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/notifikasi-pasangan": {
    path: "ReportLBPHA1Controller.getNotificationCoupleDashboard",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  },
  "GET /report/lbpha1/excel": {
    path: "ReportLBPHA1Controller.getExcel",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.PHARMA_STAFF,
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR
      ]),
      reportPolicy().defaultPolicy()
    ]
  }
};

const auth = {
  "POST /auth/logout": "AuthController.logout",
  //   "POST /auth/change-password": "AuthController.changePassword",
  "GET /users/:userId": "UserController.getDetails"
  //   "PUT /users/:userId": {
  //     path: "UserController.updateProfile",
  //     middlewares: [
  //       rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
  //     ]
  //   },
};

const patient = {
  "GET /patients": {
    path: "PatientController.getPatients",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "POST /patient/create": {
    path: "PatientController.create",
    middlewares: [rolePolicy([roles.LAB_STAFF, roles.RR_STAFF])]
  },
  "PUT /patient/:patientId": {
    path: "PatientController.updatePatient",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF]),
      patientPolicy()
    ]
  },
  "POST /patient/:patientId/odha": {
    path: "PatientController.convertODHAWithAccount",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF]),
      patientPolicy()
    ]
  },
  "POST /patient/:patientId/odha-no-account": {
    path: "PatientController.convertODHAWithoutAccount",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF]),
      patientPolicy()
    ]
  },
  "GET /patient/:patientId": {
    path: "PatientController.getDetail",
    middlewares: [patientPolicy()]
  },
  "GET /patient/:patientId/otp": {
    path: "PatientController.getOtp",
    middlewares: [rolePolicy([roles.RR_STAFF]), patientPolicy()]
  },
  "POST /patient/:patientId/otp": {
    path: "PatientController.submitOtp",
    middlewares: [rolePolicy([roles.RR_STAFF]), patientPolicy()]
  },
  "GET /patient/:patientId/visits": {
    path: "VisitController.getVisitByPatient",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF, roles.DOCTOR]),
      patientPolicy()
    ]
  },
  "GET /patient/:patientId/visit-today": {
    path: "PatientController.getTodayVisit",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF]),
      patientPolicy()
    ]
  },
  "GET /patient/:nik/nik": {
    path: "PatientController.getPatientByNik",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /patient/visit/:type": {
    path: "PatientController.getVisit",
    middlewares: [rolePolicy([roles.PATIENT])]
  },
  "GET /patient/:patientId/prescriptions": {
    path: "VisitPrescriptionController.getPrescriptions",
    middlewares: [
      rolePolicy([roles.PATIENT, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /visit/:visitId/prescription-by-visit": {
    path: "VisitPrescriptionController.getPrescriptionsByVisitId",
    middlewares: [
      rolePolicy([roles.PATIENT, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /patient/:patientId/last-prescription": {
    path: "VisitPrescriptionController.getLastPrescription",
    middlewares: [
      rolePolicy([roles.PATIENT, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /patient/:patientId/last-given-prescription": {
    path: "VisitPrescriptionController.getLastGivenPrescription",
    middlewares: [
      rolePolicy([roles.PATIENT, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "PUT /patient/:visitId/medicines-given": {
    path: "PatientController.updateSisa",
    middlewares: [rolePolicy([roles.PATIENT]), visitPolicy()]
  }
};

const examination = {
  "POST /examination/hiv/:visitId": {
    path: "ExaminationController.updateHiv",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]), visitPolicy()]
  },
  "POST /examination/ims/:visitId": {
    path: "ExaminationController.updateIms",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]), visitPolicy()]
  },
  "POST /examination/vl/:visitId": {
    path: "ExaminationController.updateVl",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]), visitPolicy()]
  },
  "POST /examination/profilaksis/:visitId": {
    path: "ExaminationController.updateProfilaksis",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /examination/:visitId/visit": {
    path: "ExaminationController.getExamByVisit",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /examination/hiv/:visitId": {
    path: "ExaminationController.getHiv",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /examination/ims/:visitId": {
    path: "ExaminationController.getIms",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /examination/vl/:visitId": {
    path: "ExaminationController.getVl",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /examination/profilaksis/:visitId": {
    path: "ExaminationController.getProfilaksis",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "PUT /examination/hiv/:visitId": {
    path: "ExaminationController.updateHiv",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]), visitPolicy()]
  },
  "PUT /examination/ims/:visitId": {
    path: "ExaminationController.updateIms",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]), visitPolicy()]
  },
  "PUT /examination/vl/:visitId": {
    path: "ExaminationController.updateVl",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]), visitPolicy()]
  },
  "PUT /examination/profilaksis/:visitId": {
    path: "ExaminationController.updateProfilaksis",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /examination/hiv/patient/:patientId": {
    path: "ExaminationController.getHivHistoryByPatient",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /examination/ims/patient/:patientId": {
    path: "ExaminationController.getImsHistoryByPatient",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /examination/vl/patient/:patientId": {
    path: "ExaminationController.getVlHistoryByPatient",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /examination/profilaksis/patient/:patientId": {
    path: "ExaminationController.getProfilakasisHistoryByPatient",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "POST /examination/lab-usage-nonpatient": {
    path: "ExaminationController.createLabUsage",
    middlewares: [rolePolicy([roles.LAB_STAFF])]
  },
  "GET /examination/lab-usage-nonpatient": {
    path: "ExaminationController.getLabUsageList",
    middlewares: [rolePolicy([roles.LAB_STAFF])]
  }
};

const upk = {
  "GET /hospitals/:upkId/statistic": {
    path: "UpkController.getStatistic",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /hospitals/:upkId/today-visits": {
    path: "UpkController.getVisitsToday",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /upk/:upkId/statistic": {
    path: "UpkController.getStatistic",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /upk/visits": {
    path: "VisitController.getVisitListByUpk",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF, roles.DOCTOR])
    ]
  },
  "GET /upk/patients": {
    path: "PatientController.getPatients",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF, roles.DOCTOR])
    ]
  },
  "GET /upk/:upkId/visits/today": {
    path: "UpkController.getVisitsToday",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  }
};

const medicine = {
  "GET /medicines/stock/:medicineType(all|NON_ARV|ARV)": {
    path: "MedicineController.getMedicineStocks",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.DOCTOR,
      ])
    ]
  },
  "GET /medicines/stock-detail/:medicineId": {
    path: "MedicineController.getMedicineStockById",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /medicines": {
    path: "MedicineController.getMedicines",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /medicines/regiments": {
    path: "MedicineController.getRegiments",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /medicines/regiment/by-combination-ids": {
    path: "MedicineController.getRegimentByCombinationIds",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /medicines/regiment/:regimentId": {
    path: "MedicineController.getRegiment",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  },
  "GET /medicines/inventory/:inventoryId": {
    path: "MedicineController.detailInventory",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  }
};

const prescription = {
  "POST /prescription": {
    path: "VisitPrescriptionController.createPrescription",
    middlewares: [rolePolicy([roles.RR_STAFF, roles.PHARMA_STAFF])]
  },
  "GET /prescription/:prescriptionId": {
    path: "VisitPrescriptionController.getDetailById",
    middlewares: [
      rolePolicy([roles.PATIENT, roles.RR_STAFF, roles.PHARMA_STAFF])
    ]
  }
};

const visit = {
  "POST /visits": {
    path: "VisitController.create",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.DOCTOR
      ]),
      patientPolicy()
    ]
  },
  "POST /visits/:patientId": {
    path: "VisitController.create",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF]),
      patientPolicy()
    ]
  },
  "DELETE /visits/:id": {
    path: "VisitController.deleteVisit",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.DOCTOR])
    ]
  },
  "GET /visits/:visitId": {
    path: "VisitController.getVisitDetail",
    middlewares: [
      rolePolicy([
        roles.RR_STAFF,
        roles.PATIENT,
        roles.LAB_STAFF,
        roles.PHARMA_STAFF,
        roles.DOCTOR
      ]),
      visitPolicy()
    ]
  },
  "GET /visits/:patientId/patient": {
    path: "VisitController.getVisitByPatient",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.DOCTOR
      ])
    ]
  },
  "GET /visit/:visitId/previous-treatment": {
    path: "VisitController.getPreviousTreatmentVisitDetail",
    middlewares: [
      rolePolicy([
        roles.RR_STAFF,
        roles.PATIENT,
        roles.LAB_STAFF,
        roles.PHARMA_STAFF,
        roles.DOCTOR
      ]),
      visitPolicy()
    ]
  },
  "GET /visit/:visitId/treatment": {
    path: "TreatmentController.getTreatment",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.RR_STAFF,
        roles.PHARMA_STAFF
      ]),
      visitPolicy()
    ]
  },
  "POST /visit/:visitId/prescription": {
    path: "VisitPrescriptionController.createPrescription",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy(),
      visitArrMedicineValidation().add()
    ]
  },
  "GET /visit/:visitId/prescription": {
    path: "VisitPrescriptionController.getDetailByVisitId",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.PHARMA_STAFF, roles.LAB_STAFF]),
      visitPolicy()
    ]
  },
  "PUT /visit/:visitId/prescription": {
    path: "VisitPrescriptionController.updatePrescription",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy(),
      visitArrMedicineValidation().add()
    ]
  },
  "POST /visit/:visitId/treatment": {
    path: "TreatmentController.create",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "PUT /visit/:visitId/treatment": {
    path: "TreatmentController.updateTreatment",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "POST /visit/:visitId/checkout": {
    path: "VisitController.endVisit",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "POST /visit/:visitId/checkout-ims": {
    path: "VisitController.endVisitIms",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "POST /visit/:visitId/give-prescription": {
    path: "VisitPrescriptionController.givePrescription",
    middlewares: [
      rolePolicy([roles.RR_STAFF, roles.LAB_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "PUT /visit/:visitId/ikhtishar": {
    path: "VisitController.ikhtisar",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /visit/:visitId/ikhtishar": {
    path: "VisitController.getIkhtisar",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "PUT /visit/:visitId/sisa-obat": {
    path: "VisitController.sisaObat",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /visit/:visitId/sisa-obat": {
    path: "VisitController.getSisaObat",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "GET /visit/:visitId/status-paduan": {
    path: "VisitPrescriptionController.getStatusPaduanObat",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.RR_STAFF, roles.PHARMA_STAFF]),
      visitPolicy()
    ]
  },
  "POST /visit/ims/:visitId": {
    path: "VisitController.createIms",
    middlewares: [
      rolePolicy([roles.RR_STAFF]),
      visitPolicy()
    ]
  },
  "PUT /visit/ims/:visitId": {
    path: "VisitController.updateIms",
    middlewares: [
      rolePolicy([roles.DOCTOR, roles.RR_STAFF]),
      visitPolicy()
    ]
  },
  "GET /visit/ims/:visitId/": {
    path: "VisitController.getIms",
    middlewares: [
      rolePolicy([roles.DOCTOR, roles.RR_STAFF]),
      visitPolicy()
    ]
  },
  "POST /visit/ims/examination/:visitId/": {
    path: "VisitController.createExaminationIms",
    middlewares: [
      rolePolicy([roles.DOCTOR]),
      visitPolicy()
    ]
  },
  "GET /visit/ims/examination/:visitId/": {
    path: "VisitController.getExaminationIms",
    middlewares: [
      rolePolicy([roles.DOCTOR]),
      visitPolicy()
    ]
  },
  "POST /visit/ims/lab/:visitId/": {
    path: "VisitController.createLabIms",
    middlewares: [
      rolePolicy([roles.LAB_STAFF]),
      visitPolicy()
    ]
  },
  "GET /visit/ims/lab/:visitId/": {
    path: "VisitController.fetchImsLab",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.DOCTOR]),
      visitPolicy()
    ]
  },
  "POST /visit/ims/prescription/:visitId/": {
    path: "VisitController.prescriptionIms",
    middlewares: [
      rolePolicy([roles.PHARMA_STAFF, roles.DOCTOR, roles.RR_STAFF]),
      visitPolicy()
    ]
  },
  "GET /visit/ims/prescription/:visitId/": {
    path: "VisitController.fetchPrescriptionIms",
    middlewares: [
      rolePolicy([roles.PHARMA_STAFF, roles.DOCTOR, roles.RR_STAFF]),
      visitPolicy()
    ]
  },
  "POST /visit/ims/diagnosis/:visitId": {
    path: "VisitController.updateDiagnosis",
    middlewares: [
      rolePolicy([roles.DOCTOR]),
      visitPolicy()
    ]
  },
  "GET /visit/ims-reagen": {
    path: "VisitController.fetchReagenIms",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.DOCTOR])
    ]
  },
  "GET /visit/ims-reagen-real": {
    path: "VisitController.fetchReagenImsReal",
    middlewares: [
      rolePolicy([roles.LAB_STAFF, roles.DOCTOR, roles.PHARMA_STAFF])
    ]
  },
  "GET /visit/ims-medicines": {
    path: "VisitController.fetchMedicineIms",
    middlewares: [
      rolePolicy([roles.PHARMA_STAFF, roles.DOCTOR, roles.RR_STAFF])
    ]
  },
  "GET /visit/ims-laporan-by-age": {
    path: "VisitController.reportByAge2",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.DOCTOR,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /visit/ims-laporan-by-all": {
    path: "VisitController.reportByAll",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.DOCTOR,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /visit/ims-laporan-by-all-new": {
    path: "VisitController.reportByAllNew",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.DOCTOR,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /visit/ims-laporan-by-age-old": {
    path: "VisitController.reportByAge",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.DOCTOR,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /visit/ims-laporan-by-populate": {
    path: "VisitController.reportByPopulate2",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.DOCTOR,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
  "GET /visit/ims-laporan-by-populate-old": {
    path: "VisitController.reportByPopulate",
    middlewares: [
      rolePolicy([
        roles.MINISTRY_STAFF,
        roles.DOCTOR,
        roles.RR_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF
      ])
    ]
  },
};

const order = {
  "POST /orders/create-reguler": {
    path: "OrderController.createOrderReguler",
    middlewares: [
      rolePolicy([roles.PHARMA_STAFF, roles.SUDIN_STAFF, roles.PROVINCE_STAFF])
    ]
  },
  "POST /orders/regular-demand": {
    path: "OrderController.createReguler",
    middlewares: [
      rolePolicy([roles.PHARMA_STAFF, roles.SUDIN_STAFF, roles.PROVINCE_STAFF])
    ]
  },
  "POST /orders-logistic/create": {
    path: "OrderController.createOrdersLog",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "PUT /orders-logistic/:orderId": {
    path: "OrderController.updateOrder",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /orders-logistic": {
    path: "OrderController.getOrdersLog",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /orders-logistic/in": {
    path: "OrderController.getOrdersLogIn",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /orders-logistic/availableFor": {
    path: "OrderController.getOrdersLogAvailable",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /orders-logistic/availableForDown": {
    path: "OrderController.getOrdersLogAvailableDown",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  // new
  "GET /orders-logistic/availableForUp": {
    path: "OrderController.getOrdersLogAvailableUp",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /orders-logistic/report-relokasi": {
    path: "OrderController.reportRelokasi",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  // end new
  "GET /orders/:orderId": {
    path: "OrderController.getOrderDetail",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "POST /order-confirm/:orderId": {
    path: "OrderController.confirmOrder",
    middlewares: [
      rolePolicy([
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  }
};

const inventory = {
  "GET /inventories/all": {
    path: "InventoryController.getAllInventories",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /inventories/:medicineId": {
    path: "InventoryController.getAllInventories",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /inventory": {
    path: "InventoryController.getInventoryListStock",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /inventory/picking-list/:medicineId/:expiredDate": {
    path: "InventoryController.getInventoryPickingList",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PHARMA_STAFF,
        roles.SUDIN_STAFF,
        roles.PROVINCE_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "POST /inventory/adjustment": {
    path: "InventoryController.updateAdjustment",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /inventory/adjustment/:medicineId": {
    path: "InventoryController.getStockAdjustment",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "POST /inventory/kalibration": {
    path: "InventoryController.stockKalibration",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  }
};

const distribution = {
  "GET /pengiriman": {
    path: "DistributionController.getDelivery",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "POST /pengiriman": {
    path: "DistributionController.createDistribution",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "GET /penerimaan": {
    path: "DistributionController.getPlans",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "POST /penerimaan": {
    path: "DistributionController.createPlan",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "POST /penerimaan/all-upk": {
    path: "DistributionController.createPlanForAllUpk",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "GET /penerimaan/:id": {
    path: "DistributionController.getPlanDetails",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "POST /penerimaan-bycode": {
    path: "DistributionController.getPlanDetailsByCode",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "POST /penerimaan/accept/:distributionPlanId": {
    path: "DistributionController.confirmDistribution",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF,
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "POST /penerimaan/:distributionPlanId": {
    path: "DistributionController.approvalDistribution",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },

  "PUT /penerimaan/:distributionPlanId": {
    path: "DistributionController.updateDistribution",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/jumlah-ims": {
    path: "ReportLogisticController.getJumlahIms",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  }
};

const reportLogistic = {
  "GET /report/ketersediaan-obat": {
    path: "ReportLogisticController.getKetersediaanObat",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/layanan-aktif": {
    path: "ReportLogisticController.getLayananAktif",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/layanan-desentralisasi": {
    path: "ReportLogisticController.getLayananDesentralisasi",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/penerimaan-obat": {
    path: "ReportLogisticController.getPenerimaanObat",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/pengunaan-obat": {
    path: "ReportLogisticController.getPengunaanObat",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/relokasi": {
    path: "ReportLogisticController.getRelokasi",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/adjustment": {
    path: "ReportLogisticController.getAdjustment",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  },
  "GET /report/pergerakan-stok": {
    path: "ReportLogisticController.getPergerakanStok",
    middlewares: [
      rolePolicy([
        roles.PROVINCE_STAFF,
        roles.SUDIN_STAFF,
        roles.PHARMA_STAFF,
        roles.MINISTRY_STAFF
      ])
    ]
  }
};

const lab = {
  "GET /lab/penggunaan-vlcd4": {
    path: "LabController.getData",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF
      ])
    ]
  },
  "POST /lab/penggunaan-vlcd4": {
    path: "LabController.saveData",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF
      ])
    ]
  },
  "PUT /lab/penggunaan-vlcd4": {
    path: "LabController.saveData",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF
      ])
    ]
  },
  "DELETE /lab/penggunaan-vlcd4": {
    path: "LabController.deleteData",
    middlewares: [
      rolePolicy([
        roles.LAB_STAFF
      ])
    ]
  },
};

const privateRoutes = {
  ...cronjob,
  ...util,
  ...masterData,
  ...report,
  ...reportLogistic,
  ...auth,
  ...patient,
  ...upk,
  ...visit,
  ...examination,
  ...prescription,
  ...order,
  ...distribution,
  ...inventory,
  ...medicine,
  ...lab
};

module.exports = privateRoutes;
