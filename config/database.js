const Sequelize = require("sequelize");

const connection = require("./connection");

let database;

switch (process.env.NODE_ENV) {
  case "production":
    database = new Sequelize(
      connection.production.database,
      connection.production.username,
      connection.production.password,
      {
        host: connection.production.host,
        dialect: connection.production.dialect,
        timezone: "Asia/Jakarta",
        pool: {
          max: 5,
          min: 0,
          idle: 10000
        },
        logging: false
      }
    );
    break;
  default:
    database = new Sequelize(
      connection.development.database,
      connection.development.username,
      connection.development.password,
      {
        host: connection.development.host,
        dialect: connection.development.dialect,
        timezone: "Asia/Jakarta",
        pool: {
          max: 5,
          min: 0,
          idle: 10000
        },
        logging: false,
        // logging: function(msg, benchmark) {
        //   console.log(msg);
        //   console.log("benchmark: ", benchmark, " ms");
        // },
        benchmark: true
        // storage: path.join(process.cwd(), 'db', 'database.sqlite'),
      }
    );
}

module.exports = database;
